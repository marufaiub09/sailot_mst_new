<!-- Link list items -->
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<!-- <link rel="stylesheet" href="<?php echo base_url() ?>dist/angularJS/demo-framework.css">
<link rel="stylesheet" href="<?php echo base_url() ?>dist/angularJS/simple.css"> -->
<!-- Script list -->
<!-- <script src="<?php echo base_url() ?>dist/angularJS/angular.min.js"></script>
<script src="<?php echo base_url() ?>dist/angularJS/angular-drag-and-drop-lists.min.js"></script>
<script src="<?php echo base_url() ?>dist/angularJS/angular-route.min.js"></script>
<script src="<?php echo base_url() ?>dist/angularJS/demo-framework.js"></script>
<script src="<?php echo base_url() ?>dist/angularJS/simple.js"></script> -->
<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<div class="row">
    <form class="form-horizontal frmContent" id="MainForm" method="post">
        <span class="frmMsg"></span>
        <div class="col-md-12 panel panel-base" id="tabs">
            <div class="panel-heading">
                <div class="row">                    
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Course Roster Setting</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-12 panel-body">                
                <div class="col-md-5">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Training Type <span class="text-danger"> * </span></label>
                        <div class="col-md-8" >
                            <select class="select2 form-control TRAINING_TYPE_ID required" name="TRAINING_TYPE_ID" id="TRAINING_TYPE_ID" data-placeholder="Select Training type" aria-hidden="true" data-allow-clear="true">
                                <option value="">Select One</option>
                                <?php
                                foreach ($trainingType as $row):
                                    ?>
                                    <option value="<?php echo $row->NAVYTrainingID ?>"><?php echo "[ ".$row->Code." ] ".$row->Name ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>                          
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-3 control-label"  style="padding-left: 0px;">Course Name <span class="text-danger"> * </span></label>
                        <div class="col-md-8">
                            <select class="select2 form-control required courseName" name="COURSE_NAME" id="courseName" data-placeholder="Select Course Name" aria-hidden="true" data-allow-clear="true">
                                <option value="">Select Course Name</option>
                            </select>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active "><a href="form-layouts-tabbed.html#tab-first" role="tab" data-toggle="tab">Branch & Rank</a></li>
                    <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Exam/Training/part ll </a></li>
                    <li><a href="form-layouts-tabbed.html#tab-third" role="tab" data-toggle="tab">Order by & Seniority Setting</a></li>
                </ul> 
                <div class="tab-content">
                    <div class="tab-pane active col-md-12" id="tab-first">
                        <div class="col-md-12">                                
                            <label class="col-md-1 control-label">Branch</label>
                            <div class="col-md-3" >
                                <select class="select2 form-control required" name="Branch_ID" id="Branch_ID" data-placeholder="Select Branch Name" aria-hidden="true" data-allow-clear="true">
                                    <option value="">Select One</option>
                                    <?php
                                    foreach ($branch as $row):
                                        ?>
                                        <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                        <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Branch Name">
                                <i class="fa fa-question-circle"></i>
                            </a>
                            <label class="col-md-1 control-label">Rank</label>
                            <div class="col-md-4">
                                <select class="select2 form-control"  multiple="multiple" name="rankId[]" id="rankName" data-tags="true" data-placeholder="Select Rank Name" data-allow-clear="true" >                                    
                                </select>
                            </div>
                        </div>    
                        <hr style="height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">                    
                    </div>
                    <div class="tab-pane col-md-12" id="tab-second">
                        <div class="col-md-9">
                            <div class="col-md-12">
                                <label class="col-sm-8" style="font-size: small;font-weight: bold;">Exam
                                </label>
                                <table id="sailorTable" class="table table-striped table-bordered trainTable"
                                       cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="35%">Code/Name</th>
                                            <th width="16%">Gain</th>
                                            <th width="14%">Is Mandatory</th>
                                            <th width="35%">Ouput Column</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>          
                                            <td>
                                                <?php echo form_dropdown('codeExam[]', $codeExam, 'default', 'style ="width: 100%; HEIGHT: 20%" class = "select2 form-control" id="codeExam_1"'); ?>
                                            </td>
                                            <td>
                                                <input type="text" value="" name="examGain[]" id="examGain_1"  class="form-control" placeholder="Gain Value" width="50%">
                                            </td>
                                            <td>
                                                <input type="checkbox" name="examIsMandatory[]" id="examIsMandatory_1"  class="form-control" >
                                            </td>
                                            <td>
                                                <?php 
                                                    $examOutputColumn = array(
                                                                        '' => "Select One",
                                                                        1 => "Exam-1(Date)",
                                                                        2 => "Exam-2(Date)",
                                                                        3 => "Exam-3(Date)",
                                                                    );
                                                    echo form_dropdown('examOutputColumn[]', $examOutputColumn, 'default', 'style ="width: 100%; HEIGHT: 20%" class = "select2 form-control" id="examoutputColumn_1"'); 
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="xh" style="text-align: right;">
                                    <span class="btn btn-xs btn-success" id="add_record">
                                        <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                    </span>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <label class="col-sm-4" style="font-size: small;font-weight: bold;">Training
                                </label>
                                <table id="sailorTable2"
                                       class="table table-striped table-bordered trainTable"
                                       cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="35%">Name</th>
                                            <th width="16%">Gain</th>
                                            <th width="14%">Is Mandatory</th>
                                            <th width="35%">Ouput Column</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>          
                                            <td>
                                                <?php echo form_dropdown('trainingCode[]', $codeFirst, 'default', 'style ="width: 100%; HEIGHT: 20%" class = "select2 form-control" id="trainingCode_1"'); ?>
                                            </td>
                                            <td>
                                                <input type="text" value="" name="trainingGain[]" id="trainingGain_1"  class="form-control" placeholder="Gain Value" width="50%">
                                            </td>
                                            <td>
                                                <input type="checkbox" name="trainingIsMandatory[]" id="trainingIsMandatory_1"  class="form-control" >
                                            </td>
                                            <td>
                                                <?php 
                                                    $trainingOutputColumn = array(
                                                                        '' => "Select One",
                                                                        1 => "Course-1(Date)",
                                                                        2 => "Course-2(Date)",
                                                                        3 => "Course-3(Date)",
                                                                    );
                                                    echo form_dropdown('trainingOutputColumn[]', $trainingOutputColumn, 'default', 'style ="width: 100%; HEIGHT: 20%" class = "select2 form-control" id="examoutputColumn_1"'); 
                                                ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <div class="xh" style="text-align: right;">
                                    <span class="btn btn-xs btn-success" id="add_record2">
                                        <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <table class="table" style="margin-bottom: 0px ! important;"><tr><th>Code &nbsp;&nbsp;&nbsp;&nbsp;</th><th>Part Name</th></tr></table>
                            <table id="ReportTable"  class="table table-striped table-bordered hover noHeader" width="100%">
                                <tbody>
                                    <?php foreach ($partlist as $partlistData) { ?>
                                        <tr>

                                            <td><input type="checkbox" class="check"  value="<?php echo $partlistData->PartIIID; ?>" name="partII[]"> <?php echo $partlistData->Code; ?></td>

                                            <td>
                                               <?php echo $partlistData->Name; ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-third">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="legend">Order by</label>
                                <table class="table  table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <td>
                                        <div class="checkbox checkbox-inline checkbox-primary"
                                             style="overflow:scroll;height:200px;width:222px">
                                            <input class="styled sequData" id="entryDate" type="checkbox"
                                                   value="0"
                                                   name="orderBy[]">
                                            <label for="ACTIVE_STATUS">Total Point</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="equialRank" type="checkbox"
                                                   value="1"
                                                   name="orderBy[]">
                                            <label for="ACTIVE_STATUS">Branch Position</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="examLevel" type="checkbox" value="2"
                                                   name="orderBy[]">
                                            <label for="ACTIVE_STATUS">Rank Position</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="batch" type="checkbox"
                                                   value="3"
                                                   name="orderBy[]">
                                            <label for="ACTIVE_STATUS">Seniority Date</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="dob" type="checkbox" value="4"
                                                   name="orderBy[]">
                                            <label for="ACTIVE_STATUS">Promotion Date</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="fullName" type="checkbox"
                                                   value="5"
                                                   name="orderBy[]">
                                            <label for="ACTIVE_STATUS">Official Number</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="examResult" type="checkbox"
                                                   value="6"
                                                   name="orderBy[]">
                                            <label for="ACTIVE_STATUS">Red Recom Seniority Date</label>
                                            <br/><br/>
                                        </div>
                                    </td>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-9">
                                    <fieldset class="scheduler-border">
                                        <legend class="scheduler-border">Seniority Base</legend>
                                        <div class="from-group">
                                            <input name="navi" id="navi" type="radio" checked="che" value="Navy"> Month &nbsp;&nbsp;
                                            <input name="navi" id="navi" type="radio" value="ISMODC"> Point
                                        </div>
                                    </fieldset>                                    
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-8">Sea Service Point (month)</label>
                                        <div class="col-md-4">
                                            <input type="number" name="seaPoint" style="width:100%" value="0">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-8">Req. Due By Time (Year)</label>
                                        <div class="col-md-4">
                                            <input type="number" name="dueByTime" style="width:100%" value="0">
                                        </div>
                                    </div>                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-12" >
                                    <div class="col-md-2">
                                        <input type="checkbox" name="CalcRankSenio" value="Yes" />
                                    </div>
                                    <label class="col-md-10">Calculate Rank Seniority</label>                                    
                                </div>
                                <fieldset class="scheduler-border" style="padding-top: 10px ! important;">
                                        <label class="col-md-9">Gain Value (per month)</label>
                                        <div class="col-md-3">
                                            <input type="text" class="integerNumbersOnly" name="gainValue" style="width:100%" value="0">
                                        </div> 
                                </fieldset>
                                <div class="col-md-12 form-group">                                    
                                    <label class="control-label" style="padding-left: 15px;">Seniority of red recom</label>
                                    <div class="col-md-12" >
                                        <select class="select2 form-control" name="seniorityRecom" id="seniority" data-tags="true" data-placeholder="Select seniority" data-allow-clear="true" style="width:100%">
                                            <option value="">Select seniority</option>
                                            <option value="0">D(Seniority Date)</option>
                                            <option value="1">E(Exam Date)</option>
                                            <option value="2">T(Training Date)</option>
                                            <option value="3">A(Art4 Rank Join Date)</option>
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <legend style="font-size: small;font-weight: bold;">Seniority Count
                                        For Red Recomm
                                    </legend>
                                    <table id="sailorTable3" class="table table-striped table-bordered"
                                           cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Recom.No</th>
                                                <th>Gain</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><input type="text" name="recomnNo[]" class="form-control"></td>
                                                <td><input type="text" name="recomnGain[]" class="form-control"></td>
                                            </tr>
                                        </tbody>
                                    </table> 
                                    <div class="xh" style="text-align: right;">
                                        <span class="btn btn-xs btn-success" id="add_record3">
                                            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <label class="legend"></label>
                <div class="col-md-12">
                    <hr style="margin-top: 0px;">
                    <div class="col-md-9">
                        <table class="table table-striped hovor display" id="courseBranch" cellspacing="0" width="80%">
                            <thead>
                                <tr>
                                    <th width="35%">Branch</th>
                                    <th>Rank</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="height: 15px;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="height: 15px;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="height: 15px;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="height: 15px;"></td>
                                    <td></td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <button type="button" id="trainingCourseAdd" class="btn pull-left btn-primary btn-sm">Add</button> <span class="loadingImg"></span>   &nbsp; <button type="button" class="btn btn-primary btn-sm">Delete</button>
                    </div> 
                </div>
                <div class="col-md-12" style="padding-left: 0 !important;">
                    <hr style="margin-top: 0px;">
                    <div class="col-sm-9">

                    </div>
                    <div class="col-md-3">
                        <input type="button" id = "trainingCourseSave" class="btn pull-left btn-primary btn-sm" value="Save"><span class="loadingImg"></span>                      
                    </div>
                </div>
            </div>
        </div>
</div>
</form>
</div>
<style>
    .tab-pane{
        padding-top: 10px !important;
    }
    .dataTables_scrollHead {
        width: 100% 
    }
    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 0 1.4em 1.4em 1.4em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
                box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 15px !important;
        font-weight: bold !important;
        text-align: left !important;

    }
    
    .noHeader thead { display:none;}
</style>
<script>    
   $('#courseBranch').DataTable({
        "bPaginate": false,
        "bFilter": false,
        "ordering": false,
        "bInfo": false,
   });
    $('#examTable').DataTable({
        //"scrollY": '3vh',
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {"sZeroRecords": "",
            "sEmptyTable": ""},
        "columnDefs": [
            {width: '100%', targets: 1}
        ]
    });

    $('#trainingTable').DataTable({
        //"scrollY": '3vh',
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {"sZeroRecords": "",
            "sEmptyTable": ""},
        "columnDefs": [
            {width: '100%', targets: 1}
        ]
    });
    $('#ReportTable').DataTable({
        "bPaginate": false,
        "ordering": false,
        "bFilter": false,
        "bInfo": false,
        "scrollY": '200px',
        "sScrollX": "100%",
        "bScrollCollapse": true,
        "sScrollXInner": "100%", 
        //"scrollX": false,
        "scrollCollapse": true,
        "paging": false,
        //"fixedHeader": true,
        fixedHeader: true,
        "columnDefs": [
            {width: '90px', targets: 0},
            {width: '100px', targets: 1}
        ],
        "responsive": true,
    });

    $("#checkAll").click(function () {
        $(".check").prop('checked', $(this).prop('checked'));

    });

    $(document).on('change', '#Branch_ID', function (event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>report/RpTrainCourSet/rankByBranch',
            type: 'POST',
            dataType: 'html',
            data: {branch_id: id},
            beforeSend: function () {
                $(".training_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankName').html(data);
                $('#rankName').select2('val', '');
            }
        });

    });

    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#sailorTable tbody").append(' <tr>' +
            '<td id="prexam_' + counter + '" style="display:none">' +
            ' <input type="hidden"  value="" name="prexam[]"   class="form-control" placeholder="Gain Value" >' +
            '</td>' +
            '<td style="width:30%" id="codeExam_' + counter + '">' +
            '<select class="select2 form-control" data-live-search= "true"  name="codeExam[]">' +
            '<option value="0">SELECT CODE</option>' +
            <?php foreach ($exam as $row) { ?>
                    "<option value='<?php echo $row->EXAM_ID ?>'><?php echo htmlspecialchars($row->NAME) ?></option>" +
            <?php } ?>
            '</select> ' +
            '</td>' +
            '<td id="examGain_' + counter + '">' +
            ' <input type="text"  value="" name="examGain[]"   class="form-control" placeholder="Gain Value" >' +
            '</td>' +
            '<td id="examIsMandatory_' + counter + '">' +
            ' <input type="checkbox" value = "0" name="examIsMandatory[]"   class="tempCheck form-control " >' +
            '</td>' +
            '<td style="width:28%" id="examOutputColumn_' + counter + '">' +
            '<select class="select2 form-control" data-live-search = "true" name="examOutputColumn[]">' +
            '<option value="">Select One</option>' +
            '<option value="1">Exam-1(Date)</option>' +
            '<option value="2">Exam-2(Date)</option>' +
            '<option value="3">Exam-3(Date)</option>' +
            '</select> ' +
            '</td>' +
            '<td class="text-center">' +
            '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" ></i></span>' +
            '</td>' +
            '</tr>'
        );
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    var counter = 1;
    $(document).on('click', '#add_record2', function () {
        counter++;
        $("#sailorTable2 tbody").append(' <tr>' +
            '<td id="prtrain_' + counter + '" style="display:none">' +
            ' <input type="hidden"  value="" name="prtrain_[]"   class="form-control" placeholder="Gain Value" >' +
            '</td>' +
            '<td style="width:30%" id="trainingCode_' + counter + '">' +
            '<select class="select2 form-control" data-live-search= "true"  name="trainingCode[]">' +
            '<option value="0">SELECT CODE</option>' +
            <?php foreach ($code as $row) { ?>
                    "<option value='<?php echo $row->NAVYTrainingID ?>'><?php echo htmlspecialchars($row->Name) ?></option>" +
            <?php } ?>
            '</select> ' +
            '</td>' +
            '<td id="trainingGain_' + counter + '">' +
            ' <input type="text"  value="" name="trainingGain[]"   class="form-control" placeholder="Gain Value" >' +
            '</td>' +
            '<td id="trainingIsMandatory_' + counter + '">' +
            ' <input type="checkbox" value = "0" name="trainingIsMandatory[]"   class="tempCheck form-control " >' +
            '</td>' +
            '<td style="width:28%" id="trainingOutputColumn_' + counter + '">' +
            '<select class="select2 form-control" data-live-search = "true" name="trainingOutputColumn[]">' +
            '<option value="">Select One</option>' +
            '<option value="1">Course-1(Date)</option>' +
            '<option value="2">Course-2(Date)</option>' +
            '<option value="3">Course-3(Date)</option>' +
            '</select> ' +
            '</td>' +
            '<td class="text-center">' +
            '<span class="btn btn-xs btn-danger" id="remove_tr2"><i style="cursor:pointer" class="fa fa-times" ></i></span>' +
            '</td>' +
            '</tr>'
        );
    });
    $(document).on('click', '#remove_tr2', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    var counter = 1;
    $(document).on('click', '#add_record3', function () {
        counter++;
        $("#sailorTable3 tbody").append(' <tr>' +
            '<td id="prrecomn_' + counter + '" style="display:none">' +
            ' <input type="hidden" name="prrecomn[]"   class="form-control "  >' +
            '</td>' +
            '<td  id="recomnRec_' + counter + '">' +
            ' <input type="text"  value="" name="recomnNo[]"  class="form-control" >' +
            '</td>' +
            '<td id="recomnGain_' + counter + '">' +
            ' <input type="text" name="recomnGain[]"   class="form-control " >' +
            '</td>' +
            '<td class="text-center">' +
            '<span class="btn btn-xs btn-danger" id="remove_tr2"><i style="cursor:pointer" class="fa fa-times" ></i></span>' +
            '</td>' +
            '</tr>'
        );

    });
    $(document).on('click', '#remove_tr3', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    $(document).on("click", "#trainingCourseAdd", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                //alert(label + " Is Empty");
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
                //return false;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm("Are You Sure?")) {
                var frmContent = $(".frmContent").serialize();
                console.log(frmContent);
                $.ajax({
                    type: "post",
                    data: frmContent,
                    url: "<?php echo site_url(); ?>report/RpTrainCourSet/courseDataSave",
                    beforeSend: function () {
                        $(".loadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".loadingImg").html("");
                        $(".frmMsg").html(data);
                        
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });
    $(document).on("click", "#trainingCourseSave", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                //alert(label + " Is Empty");
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
                //return false;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm("Are You Sure?")) {
                var frmContent = $(".frmContent").serialize();
                console.log(frmContent);
                $.ajax({
                    type: "post",
                    data: frmContent,
                    url: "<?php echo site_url(); ?>report/RpTrainCourSet/courseDataSave",
                    beforeSend: function () {
                        $(".loadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".loadingImg").html("");
                        $(".frmMsg").html(data);
                        
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });

    $(document).on('change', '#TRAINING_TYPE_ID', function (event) {
        event.preventDefault();
        var type_id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>report/RpTrainCourSet/course_by_trainingType',
            type: 'POST',
            dataType: 'html',
            data: {TRAINING_TYPE: type_id},
            success: function (data) {
                $('.courseName').html(data);
            }
        });
    });

    $(document).on('change', '#courseName', function(event) {
        event.preventDefault();
        var courseId = $(this).val();
        if(courseId != ''){
            $.ajax({
                url: '<?php echo base_url() ?>report/rpTrainCourSet/searchCourseRoster',
                type: 'POST',
                dataType: 'json',
                data: {CourseId: courseId},
                success: function(data){
                    var branchId = 0;
                    var temp = 1;
                    var row;
                    if(data != ''){
                        $('#courseBranch tbody tr').empty();
                        $.each(data, function(key, value){
                            branchId = value.BRANCH_ID;
                            if(branchId == temp){
                                $('#courseBranch tbody tr td:last').append(", " + value.RANK_NAME);
                            }else{
                                row =   '<tr>' +
                                        '<td>' + value.BRANCH_NAME +'</td>'+
                                        '<td>' + value.RANK_NAME +'</td>'+
                                        '</tr>';                                
                                $('#courseBranch tbody').before().append(row);
                            }
                            temp = branchId;        
                            //alert(key + ": "+ value.CourseRosterBranchID  + ", "+ value.BRANCH_NAME +", "+ value.RANK_NAME);
                        });
                    }
                }
            });            
        }

    });

</script>
