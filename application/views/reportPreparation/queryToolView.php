
<div class="panel-heading">
    <div class="row">
        <div class="col-md-11 col-sm-10 col-xs-8 ">
            <h3 class="panel-title">
                <center>Query Tool</center>
            </h3>
        </div>
    </div>
</div>
<div class="panel-body">
    <form class="" id="MainForm" >
    <span class="frmMsg"></span>
    <div class="col-lg-12">
        <div class="col-lg-8" >
            <table style="white-space: nowrap; overflow-y: auto;   overflow-x: scroll;">
                <div class="panel panel-default tabs">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="active "><a href="form-layouts-tabbed.html#tab-first" role="tab" data-toggle="tab">General</a></li>
                        <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Reports</a></li>
                        <li><a href="form-layouts-tabbed.html#tab-third" role="tab" data-toggle="tab">Foreign Visit, Make Run</a></li>
                        <li><a href="form-layouts-tabbed.html#tab-forth" role="tab" data-toggle="tab">Engagement,Assessment, Branch Change</a></li>
                        <li><a href="form-layouts-tabbed.html#tab-fifth" role="tab" data-toggle="tab">Course, Specialization</a></li>
                        <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Reports</a></li>
                        <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Reports</a></li>
                    </ul>
                    <div class="panel-body tab-content">
                        <div class="tab-pane active" id="tab-first">
                        <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Personal Information</legend>
                            <div class="col-md-12">
                                <div class="col-md-6">

                                    <label class="col-md-4" style="width:100px;margin-left:-13px">Full Name</label>

                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fullName','id'=>'fullName','type' => 'text', "class" => "form-control serial", 'style' =>'width:153px;margin-left:6px', 'placeholder' => 'Enter Full Name')); ?>
                                        </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4" style="width:112px">Short Name</label>

                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'shortName','id'=>'shortName','type' => 'text', "class" => "form-control serial", 'style' =>'width:160px', 'placeholder' => 'Enter Short Name')); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-md-4">Father's Name</label>

                                    <div class="col-md-4">
                                        <?php echo form_input(array('name' => 'fatherName','id'=>'fatherName','type' => 'text', "class" => "form-control serial",'style' =>'margin-left:-75px', 'placeholder' => 'Enter Father Name')); ?>
                                    </div>

                                </div>
                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2">Sex</label>

                                        <div class="col-md-4" style="margin-left: 15px">
                                            <select name="sex" id="sex" class="select2 serial"
                                                    data-live-search="true" data-placeholder="Select Sex" data-width="150px" >
                                                <option value="">Select Sex</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>
                                            </select>
                                        </div>
                                    </div>
                            </div>
                            <br/><br/>
                            <div class="col-lg-8">

                                <div class="form-group">
                                <label class="col-md-4">Birth Date</label>

                                    <div class="col-md-2" style="margin-left:-12px">
                                        <select name="filterBirth" id="filterBirtha" class="select2" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                            <option value="">No Filter</option>
                                            <option value="1">=</option>
                                            <option value="2"><</option>
                                            <option value="3">></option>
                                            <option value="4">Between</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDatea', "class" => "datePicker form-control",'value' => date('d-m-Y'),
                                        'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo form_input(array('name' => 'endDate', 'id'=>'endDatea', "class" => "datePicker form-control",'value' => date('d-m-Y'),
                                        'style' => 'width:100px; margin-left:110px','placeholder' => 'End Date')); ?>
                                    </div>

                                </div>

                            </div>
                            <br/><br/>
                            <div class="col-md-12">

                                <div class="form-group">
                                <label class="col-md-4">Present Hight</label>

                                    <div class="col-md-4" style="margin-left:-75px">
                                        <select name="filterHeight" id="filterHeight" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                            <option value="1">No Filter</option>
                                            <option value="2">=</option>
                                            <option value="3"><</option>
                                            <option value="4">></option>
                                            <option value="5">Between</option>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo form_input(array('name' => 'height', 'id'=>'height', "class" => "form-control serial", 'style' => 'width:100px; margin-left:60px','placeholder' => '0.00')); ?>
                                    </div>


                                </div>

                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="col-md-4" style="width:100px;margin-left:-13px">Religion</label>

                                        <div class="col-md-2" style="margin-left:6px">
                                            <select class="select2 form-control serial"  multiple="multiple" name="religion[]" id="religion" data-tags="true" data-placeholder="Select Religion" data-width="150px" data-allow-clear="true" >
                                                <option value="">Select Religion</option>
                                                <option value="1">Islam</option>
                                                <option value="2">Hindu</option>
                                                <option value="3">Buddhist</option>
                                                <option value="4">Christian</option>
                                                <option value="5">Other</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4" style="width:120px">Blood Group</label>

                                        <div class="col-md-2" style="margin-left:-10px">
                                            <select class="select2 form-control serial"  multiple="multiple" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="160px" data-allow-clear="true" >
                                                <option value="">Select Blood Group</option>

                                                <option value="1">APositive</option>
                                                <option value="2">ANegative</option>
                                                <option value="3">BPositive</option>
                                                <option value="4">BNegative</option>
                                                <option value="5">OPositive</option>
                                                <option value="6">ONegative</option>
                                                <option value="7">ABPositive</option>
                                                <option value="8">ABNegative</option>


                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="checkbox" class="serial" name="freedomFighter" id="freedomFighter" value="1">&nbsp;&nbsp;<label>FreeDom Fighter</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="checkbox" class="serial" name="freedomFighterChild" id="freedomFighterChild" value="2">&nbsp;&nbsp;<label>FreeDom Fighter's Child</label>
                                    </div>
                                </div>
                            </div>
                            </fieldset>
                            <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Official Information</legend>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="col-md-4" style="width:100px;margin-left:-13px">O NO</label>

                                        <div class="col-md-2">
                                            <textarea class="serial" rows="2" cols="20" id="officialNumber" name="officialNumber[]"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4" style="width:112px">Entry Type</label>

                                        <div class="col-md-2" style="margin-left:-2px">
                                            <select class="select2 form-control serial"  multiple="multiple" name="entryType[]" id="entryType" data-tags="true" data-placeholder="Select Entry Type" data-width="160px" data-allow-clear="true" >
                                                <option value="">Select Entry Type</option>
                                                <?php
                                                foreach ($entryType as $value) {
                                                ?>
                                                <option value="<?php echo $value->CODE?>"><?php echo $value->NAME?></option>
                                                <?php
                                                 }
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <br/><br/>

                            <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="col-md-4" style="width:115px;margin-left:-13px">O No(Range)</label>

                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromONo', 'id'=>'fromONo', 'type' => 'text', "class" => "form-control serial",  'style' =>'width:163px;margin-left:-14px', 'placeholder' => 'Enter O No')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4" style="width:112px">To</label>

                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'toNo', 'id' =>'toNo', 'type' => 'text', "class" => "form-control serial", 'style' =>'width:160px; margin-left:-2px', 'placeholder' => 'Enter O No')); ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="col-md-4" style="width:124px;margin-left:-13px">Local Number</label>

                                        <div class="col-md-2" style="margin-left:-23px">
                                            <?php echo form_input(array('name' => 'localNumber', 'id'=>'localNumber', 'type' => 'text', "class" => "form-control serial", 'style' =>'width:163px', 'placeholder' => 'Enter Local Number')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="col-md-4" style="width:100px;margin-left:-13px">Personal Number</label>

                                        <div class="col-md-2">
                                            <textarea class="serial"  rows="2" cols="20" name="personelNumber" id="personelNumber"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/><br/><br/><br/>
                            <div class="col-md-12">

                                    <div class="form-group">
                                        <label class="col-md-4" style="width:112px">Batch No</label>

                                        <div class="col-md-2" style="margin-left:-9px">
                                            <select class="select2 form-control serial"  multiple="multiple" name="batchNumber[]" id="batchNumber" data-tags="true" data-placeholder="Select Batch No" data-width="160px" data-allow-clear="true" >
                                                <option value="">Select Batch No</option>
                                                <?php
                                                foreach ($batchNumber as $value) {
                                                ?>
                                                <option value="<?php echo $value->BATCH_ID?>"><?php echo $value->BATCH_NUMBER.'--'.$value->YEAR_OF_BATCH?></option>
                                                <?php
                                                 }
                                                ?>
                                            </select>
                                        </div>

                                    </div>

                            </div>
                            </fieldset>
                            <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Status Information</legend>
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="col-md-4" style="width:120px">Sailor Status</label>

                                    <div class="col-md-2" style="margin-left:-2px">
                                        <select class="select2 form-control serial"  multiple="multiple" name="sailorStatus[]" id="sailorStatus" data-tags="true" data-placeholder="Select Sailor Status" data-width="160px" data-allow-clear="true" >
                                            <option value="">Select Sailor Status</option>

                                            <option value="1">Active</option>
                                            <option value="2">Not Active</option>



                                        </select>
                                    </div>

                                </div>


                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Is Acting</label>&nbsp;&nbsp;<input type="checkbox" class="serial" name="isActing" id="isActing" value="1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Is NQ</label>&nbsp;&nbsp;
                                            <input type="checkbox" class="serial" id="isNq" name="isNq" value="1">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Ty Status</label>&nbsp;&nbsp;
                                            <input type="checkbox" class="serial" name="tyStatus" id="tyStatus" value="1">
                                    </div>
                                </div>
                            </div>
                            <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label>FreeDom Fighter</label>&nbsp;&nbsp;
                                        <input type="checkbox" class="serial" name="freedomFighter2" id="freedomFighter2" value="1">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>FreeDom Fighter' Child</label>&nbsp;&nbsp;
                                            <input type="checkbox" class="serial" name="freedomFighterChild2" id="freedomFighterChild2" value="1">
                                    </div>
                                </div>
                            </div>
                            </fieldset>

                        </div>
                        <div class="tab-pane" id="tab-second">


                        zxxzasasasa

                        </div>
                        <div class="tab-pane" id="tab-third">
                            <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Foreign Visit Information</legend>
                                <div class="col-md-12">
                                    <div class="col-md-6">

                                        <label class="col-md-4" style="width:100px;margin-left:-13px">Visit Class</label>

                                            <div class="col-md-4" style="margin-left: 5px">
                                                <select class="selectpicker form-control serial"  multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="160px" data-allow-clear="true" >
                                                  <?php
                                                    foreach ($visit as  $row) {
                                                      # code...

                                                  ?>
                                                    <option value="<?php echo $row->VISIT_CLASSIFICATIONID?>"><?php echo $row->CODE.'--'.$row->NAME?></option>
                                                    <?php
                                                      }
                                                     ?>
                                                </select>
                                            </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-4">Visit</label>

                                            <div class="col-md-2" style="margin-left:-40px">
                                                <select class="select2 form-control serial" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                    <option value="1">In</option>
                                                    <option value="2">Out</option>

                                                </select>
                                            </div>
                                            <div class="col-md-2" style="margin-left:85px">
                                                <select class="selectpicker form-control serial"  multiple data-actions-box="true" data-width="100px" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                  <?php
                                                    foreach ($visitInfo as $visit) {
                                                    ?>
                                                    <option value="<?php echo $visit->VISIT_INFO_ID?>"><?php echo $visit->CODE.'--'.$visit->NAME?></option>
                                                    <?php
                                                      }
                                                     ?>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <br/><br/>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-2">Country</label>

                                      <div class="col-md-2" style="margin-left:15px">
                                           <select class="selectpicker form-control serial"  multiple data-actions-box="true" data-width="220px" name="bloodGroup[]" id="bloodGroup" data-tags="true"  data-allow-clear="true" >
                                                  <?php
                                                    foreach ($country as $key) {

                                                   ?>
                                                    <option value="<?php echo $key->COUNTRY_ID; ?>"><?php echo $key->CODE.'--'. $key->NAME; ?></option>
                                                    <?php
                                                      }
                                                    ?>

                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <br/><br/>

                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-4">Start Date</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                                <br/><br/>
                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-4">End Date</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                                <br/><br/>
                                    <div class="col-lg-8">

                                        <div class="form-group">
                                        <label class="col-md-4">Duration</label>
                                            <div class="col-md-2" style="margin-left:-12px">
                                                <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                                'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                            </div>


                                        </div>

                                    </div>


                            </fieldset>
                            <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Mark Run Information</legend>
                                <div class="col-md-12">
                                    <div class="col-md-6">
                                         <input type="checkbox" class="serial" name="freedomFighter2" id="freedomFighter2" value="1"> &nbsp;&nbsp;<label>Absent But Not Run</label>
                                         <br>
                                         <input type="checkbox" class="serial" name="freedomFighter2" id="freedomFighter2" value="1"> &nbsp;&nbsp;<label>Run But Not Surrender</label>
                                         <br>
                                         <input type="checkbox" class="serial" name="freedomFighter2" id="freedomFighter2" value="1"> &nbsp;&nbsp;<label>Surrender But Not 'R' Remove</label>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="col-md-6" style="margin-left:-20px">Run Status</label>

                                        <div class="col-md-2" style="margin-left:5px">
                                           <select class="select2 form-control serial" data-width="165px" multiple="multiple" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Run Status"  data-allow-clear="true" >
                                                    <option value="">Select Run Status</option>

                                                    <option value="1">APositive</option>
                                                    <option value="2">ANegative</option>
                                                    <option value="3">BPositive</option>
                                                    <option value="4">BNegative</option>
                                                    <option value="5">OPositive</option>
                                                    <option value="6">ONegative</option>
                                                    <option value="7">ABPositive</option>
                                                    <option value="8">ABNegative</option>
                                            </select>
                                        </div>

                                    </div>
                                    </div>
                                </div>
                                <br/><br/>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-4">R Remove Authority</label>

                                        <div class="col-md-2">
                                           <select class="select2 form-control serial" data-width="180px" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                    <option value=""></option>

                                                    <option value="1">APositive</option>
                                                    <option value="2">ANegative</option>
                                                    <option value="3">BPositive</option>
                                                    <option value="4">BNegative</option>
                                                    <option value="5">OPositive</option>
                                                    <option value="6">ONegative</option>
                                                    <option value="7">ABPositive</option>
                                                    <option value="8">ABNegative</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <br/><br/><br/><br/>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="col-md-4">Run Ship</label>

                                        <div class="col-md-2">
                                           <select class="select2 form-control serial" data-width="180px" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Run Ship"  data-allow-clear="true" >
                                                    <option value=""></option>

                                                    <option value="1">APositive</option>
                                                    <option value="2">ANegative</option>
                                                    <option value="3">BPositive</option>
                                                    <option value="4">BNegative</option>
                                                    <option value="5">OPositive</option>
                                                    <option value="6">ONegative</option>
                                                    <option value="7">ABPositive</option>
                                                    <option value="8">ABNegative</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <br/><br/>

                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-4">Absent Date</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                                <br/><br/>
                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-4">Run Date</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                                <br/><br/>
                                    <div class="col-lg-8">

                                        <div class="form-group">
                                        <label class="col-md-4">Surrender Date</label>
                                            <div class="col-md-2" style="margin-left:-12px">
                                                <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                                'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                            </div>


                                        </div>

                                    </div>
                                    <br/><br/>
                                    <div class="col-lg-8">

                                        <div class="form-group">
                                        <label class="col-md-4">Run Remove</label>
                                            <div class="col-md-2" style="margin-left:-12px">
                                                <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                                'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                            </div>


                                        </div>

                                    </div>


                            </fieldset>



                        </div>
                        <div class="tab-pane" id="tab-forth">
                            <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Engagement Information</legend>
                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-6">Engagement No</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                                <br/><br/>
                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-6">Engagement Date</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                                <br/><br/>
                                    <div class="col-lg-8">

                                        <div class="form-group">
                                        <label class="col-md-6">Expiry Date</label>
                                            <div class="col-md-2" style="margin-left:-12px">
                                                <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                                'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                            </div>


                                        </div>

                                    </div>
                                    <br/><br/>
                                    <div class="col-lg-8">

                                        <div class="form-group">
                                        <label class="col-md-6">Re. Eng. Due On</label>
                                            <div class="col-md-2" style="margin-left:-12px">
                                                <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                                'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                            </div>


                                        </div>

                                    </div>
                                    <br/><br/>
                                    <div class="col-lg-12">

                                        <div class="form-group">
                                        <label class="col-md-4">Engagement Type</label>
                                            <div class="col-md-2" style="margin-left:-18px">
                                                <select name="filterBirth" multiple="multiple" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="150px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                            <div class="col-md-4" style="margin-left: 65px">
                                                <input type="checkbox" class="serial" name="freedomFighter2" id="freedomFighter2" value="1"> &nbsp;&nbsp;<label>Is Ncs To Cs</label>
                                            </div>
                                        </div>

                                    </div>
                                    <br><br>
                                    <div class="col-lg-12">

                                        <div class="form-group">
                                        <label class="col-md-4">Applied Ship</label>
                                            <div class="col-md-2" style="margin-left:-18px">

                                                <select name="filterBirth" multiple="multiple" id="filterBirth" class="select2 serial"  data-live-search="true" data-placeholder="Select Filter" data-width="200px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                            </fieldset>
                            <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Assessment</legend>
                                <div class="col-lg-12">

                                        <div class="form-group">
                                        <label class="col-md-4">Ship/Establishment</label>
                                            <div class="col-md-2" style="margin-left:-18px">

                                                <select name="filterBirth" id="filterBirth"       class="selectpicker serial" multiple data-actions-box="true" data-live-search="true" data-placeholder="Select Filter" data-width="200px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                <br/><br/>
                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-6">Year</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                                <br/><br/>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label class="col-md-4" style="width:100px;margin-left:-13px">Character</label>

                                        <div class="col-md-2" style="margin-left:6px">
                                            <select class="select2 form-control serial"  multiple="multiple" name="religion[]" id="religion" data-tags="true" data-placeholder="Select Religion" data-width="150px" data-allow-clear="true" >
                                                <option value="">Select Religion</option>
                                                <option value="1">Islam</option>
                                                <option value="2">Hindu</option>
                                                <option value="3">Buddhist</option>
                                                <option value="4">Christian</option>
                                                <option value="5">Other</option>


                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4" style="width:120px">Efficiency</label>

                                        <div class="col-md-2" style="margin-left:-10px">
                                            <select class="select2 form-control serial"  multiple="multiple" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="160px" data-allow-clear="true" >
                                                <option value="">Select Blood Group</option>

                                                <option value="1">APositive</option>
                                                <option value="2">ANegative</option>
                                                <option value="3">BPositive</option>
                                                <option value="4">BNegative</option>
                                                <option value="5">OPositive</option>
                                                <option value="6">ONegative</option>
                                                <option value="7">ABPositive</option>
                                                <option value="8">ABNegative</option>


                                            </select>
                                        </div>

                                    </div>
                                </div>
                            </div>
                                <br/><br/>
                                    <div class="col-lg-8">

                                        <div class="form-group">
                                        <label class="col-md-6">No. Of VG Super</label>
                                            <div class="col-md-2" style="margin-left:-12px">
                                                <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                                'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                            </div>


                                        </div>

                                    </div>

                            </fieldset>
                            <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Branch Change Information</legend>
                                <div class="col-lg-12">

                                        <div class="form-group">
                                        <label class="col-md-4">Previous Branch</label>
                                            <div class="col-md-2" style="margin-left:-18px">

                                                <select name="filterBirth" id="filterBirth" class="selectpicker serial" multiple data-actions-box="true" data-live-search="true" data-placeholder="Select Filter" data-width="200px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                <br/><br/>
                                <div class="col-lg-12">

                                        <div class="form-group">
                                        <label class="col-md-4">Current Branch</label>
                                            <div class="col-md-2" style="margin-left:-18px">

                                                <select name="filterBirth" id="filterBirth"       class="selectpicker serial" multiple data-actions-box="true" data-live-search="true" data-placeholder="Select Filter" data-width="200px">
                                                    <option value="1">No Filter</option>
                                                    <option value="2">=</option>
                                                    <option value="3"><</option>
                                                    <option value="4">></option>
                                                    <option value="5">Between</option>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                    <br><br>
                                <div class="col-lg-8">

                                    <div class="form-group">
                                    <label class="col-md-6">Change Date</label>

                                        <div class="col-md-2" style="margin-left:-12px">
                                            <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                <option value="1">No Filter</option>
                                                <option value="2">=</option>
                                                <option value="3"><</option>
                                                <option value="4">></option>
                                                <option value="5">Between</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                            'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                        </div>


                                    </div>

                                </div>
                            </fieldset>
                        </div>
                        <div class="tab-pane" id="tab-fifth">
                          <fieldset class="scheduler-border">
                          <legend class="scheduler-border">Course Information</legend>
                              <div class="col-md-12">
                                  <div class="col-md-6">

                                      <label class="col-md-4" style="width:100px;margin-left:-13px">Name</label>

                                          <div class="col-md-4" style="margin-left: 5px">
                                              <select class="selectpicker serial" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="160px" data-allow-clear="true" >
                                                  <option value="1">APositive</option>
                                                  <option value="2">ANegative</option>
                                                  <option value="3">BPositive</option>
                                                  <option value="4">BNegative</option>
                                                  <option value="5">OPositive</option>
                                                  <option value="6">ONegative</option>
                                                  <option value="7">ABPositive</option>
                                                  <option value="8">ABNegative</option>
                                              </select>
                                          </div>

                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="col-md-4">Institute</label>

                                          <div class="col-md-2" style="margin-left:-20px">
                                              <select class="select2 form-control serial" data-width="90px"  name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                  <option value="">Select Status</option>

                                                  <option value="1">In</option>
                                                  <option value="2">Out</option>

                                              </select>
                                          </div>
                                          <div class="col-md-2" style="margin-left:55px">
                                              <select class="selectpicker form-control serial" data-width="120px" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                  <option value="1">APositive</option>
                                                  <option value="2">ANegative</option>
                                                  <option value="3">BPositive</option>
                                                  <option value="4">BNegative</option>
                                                  <option value="5">OPositive</option>
                                                  <option value="6">ONegative</option>
                                                  <option value="7">ABPositive</option>
                                                  <option value="8">ABNegative</option>
                                              </select>
                                          </div>

                                      </div>
                                  </div>
                              </div>
                              <br/><br/>
                              <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4" style="margin-left:-15px">Result</label>

                                        <div class="col-md-2" style="margin-left:-20px">
                                            <select class="select2 form-control serial" data-width="90px"  name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                <option value="">Select Status</option>

                                                <option value="1">In</option>
                                                <option value="2">Out</option>

                                            </select>
                                        </div>
                                        <div class="col-md-2" style="margin-left:55px">
                                            <select class="selectpicker form-control serial" data-width="120px" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                <option value="1">APositive</option>
                                                <option value="2">ANegative</option>
                                                <option value="3">BPositive</option>
                                                <option value="4">BNegative</option>
                                                <option value="5">OPositive</option>
                                                <option value="6">ONegative</option>
                                                <option value="7">ABPositive</option>
                                                <option value="8">ABNegative</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4">Grade</label>

                                        <div class="col-md-2" style="margin-left:-20px">
                                            <select class="select2 form-control serial" data-width="90px"  name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                <option value="">Select Status</option>

                                                <option value="1">In</option>
                                                <option value="2">Out</option>

                                            </select>
                                        </div>
                                        <div class="col-md-2" style="margin-left:55px">
                                            <select class="selectpicker form-control serial" data-width="120px" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                <option value="1">APositive</option>
                                                <option value="2">ANegative</option>
                                                <option value="3">BPositive</option>
                                                <option value="4">BNegative</option>
                                                <option value="5">OPositive</option>
                                                <option value="6">ONegative</option>
                                                <option value="7">ABPositive</option>
                                                <option value="8">ABNegative</option>
                                            </select>
                                        </div>

                                    </div>
                                </div>
                              </div>
                              <br/><br/>
                              <div class="col-md-12">
                                  <div class="col-md-6">

                                      <label class="col-md-4" style="width:100px;margin-left:-13px">Local/Foreign</label>

                                          <div class="col-md-4" style="margin-left: 5px">
                                              <select class="selectpicker serial" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="160px" data-allow-clear="true" >
                                                  <option value="1">APositive</option>
                                                  <option value="2">ANegative</option>
                                                  <option value="3">BPositive</option>
                                                  <option value="4">BNegative</option>
                                                  <option value="5">OPositive</option>
                                                  <option value="6">ONegative</option>
                                                  <option value="7">ABPositive</option>
                                                  <option value="8">ABNegative</option>
                                              </select>
                                          </div>

                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="col-md-4">Batch Number</label>

                                          <div class="col-md-2" style="margin-left:-20px">
                                              <select class="select2 form-control serial" data-width="90px"  name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                  <option value="">Select Status</option>

                                                  <option value="1">In</option>
                                                  <option value="2">Out</option>

                                              </select>
                                          </div>
                                          <div class="col-md-2" style="margin-left:55px">
                                              <select class="selectpicker form-control serial" data-width="120px" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group"  data-allow-clear="true" >
                                                  <option value="1">APositive</option>
                                                  <option value="2">ANegative</option>
                                                  <option value="3">BPositive</option>
                                                  <option value="4">BNegative</option>
                                                  <option value="5">OPositive</option>
                                                  <option value="6">ONegative</option>
                                                  <option value="7">ABPositive</option>
                                                  <option value="8">ABNegative</option>
                                              </select>
                                          </div>

                                      </div>
                                  </div>
                              </div>
                              <br><br>
                              <div class="col-lg-8">

                                  <div class="form-group">
                                  <label class="col-md-4">Start Date</label>

                                      <div class="col-md-2" style="margin-left:-12px">
                                          <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                              <option value="1">No Filter</option>
                                              <option value="2">=</option>
                                              <option value="3"><</option>
                                              <option value="4">></option>
                                              <option value="5">Between</option>
                                          </select>
                                      </div>
                                      <div class="col-md-2">
                                          <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                          'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                      </div>


                                  </div>

                              </div>
                              <br/><br/>
                              <div class="col-lg-8">

                                  <div class="form-group">
                                  <label class="col-md-4">End Date</label>

                                      <div class="col-md-2" style="margin-left:-12px">
                                          <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                              <option value="1">No Filter</option>
                                              <option value="2">=</option>
                                              <option value="3"><</option>
                                              <option value="4">></option>
                                              <option value="5">Between</option>
                                          </select>
                                      </div>
                                      <div class="col-md-2">
                                          <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                          'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                      </div>


                                  </div>

                              </div>
                              <br/><br/>
                                  <div class="col-lg-8">

                                      <div class="form-group">
                                      <label class="col-md-4">Duration</label>
                                          <div class="col-md-2" style="margin-left:-12px">
                                              <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                  <option value="1">No Filter</option>
                                                  <option value="2">=</option>
                                                  <option value="3"><</option>
                                                  <option value="4">></option>
                                                  <option value="5">Between</option>
                                              </select>
                                          </div>
                                          <div class="col-md-2">
                                              <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                              'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                          </div>


                                      </div>

                                  </div>
                                  <br><br>
                                  <div class="col-lg-8">

                                      <div class="form-group">
                                      <label class="col-md-4">Marks</label>

                                          <div class="col-md-2" style="margin-left:-12px">
                                              <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                  <option value="1">No Filter</option>
                                                  <option value="2">=</option>
                                                  <option value="3"><</option>
                                                  <option value="4">></option>
                                                  <option value="5">Between</option>
                                              </select>
                                          </div>
                                          <div class="col-md-2">
                                              <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "form-control serial",
                                              'style' => 'width:100px; margin-left:65px','placeholder' => '0.00')); ?>
                                          </div>


                                      </div>

                                  </div>
                                  <br><br>
                                  <div class="col-lg-8">

                                      <div class="form-group">
                                      <label class="col-md-4">Percentage</label>

                                          <div class="col-md-2" style="margin-left:-12px">
                                              <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                  <option value="1">No Filter</option>
                                                  <option value="2">=</option>
                                                  <option value="3"><</option>
                                                  <option value="4">></option>
                                                  <option value="5">Between</option>
                                              </select>
                                          </div>
                                          <div class="col-md-2">
                                              <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "form-control serial",
                                              'style' => 'width:100px; margin-left:65px','placeholder' => '0.00')); ?>
                                          </div>


                                      </div>

                                  </div>
                                  <br><br>
                                  <div class="col-lg-8">

                                      <div class="form-group">
                                      <label class="col-md-4">Seniority</label>

                                          <div class="col-md-2" style="margin-left:-12px">
                                              <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                                  <option value="1">No Filter</option>
                                                  <option value="2">=</option>
                                                  <option value="3"><</option>
                                                  <option value="4">></option>
                                                  <option value="5">Between</option>
                                              </select>
                                          </div>
                                          <div class="col-md-2">
                                              <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "form-control serial",
                                              'style' => 'width:100px; margin-left:65px','placeholder' => '0.00')); ?>
                                          </div>


                                      </div>

                                  </div>


                          </fieldset>
                          <fieldset class="scheduler-border">
                          <legend class="scheduler-border">Ongoing Course</legend>
                              <div class="col-md-12">
                                  <div class="col-md-6">

                                      <label class="col-md-4" style="width:100px;margin-left:-13px">Name</label>

                                          <div class="col-md-4" style="margin-left: 5px">
                                              <select class="selectpicker serial" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="160px" data-allow-clear="true" >
                                                  <option value="1">APositive</option>
                                                  <option value="2">ANegative</option>
                                                  <option value="3">BPositive</option>
                                                  <option value="4">BNegative</option>
                                                  <option value="5">OPositive</option>
                                                  <option value="6">ONegative</option>
                                                  <option value="7">ABPositive</option>
                                                  <option value="8">ABNegative</option>
                                              </select>
                                          </div>

                                  </div>
                                  <div class="col-md-6">

                                      <label class="col-md-4" style="width:100px;margin-left:-13px">Batch No.</label>

                                          <div class="col-md-4" style="margin-left: 5px">
                                              <select class="selectpicker serial" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="160px" data-allow-clear="true" >
                                                  <option value="1">APositive</option>
                                                  <option value="2">ANegative</option>
                                                  <option value="3">BPositive</option>
                                                  <option value="4">BNegative</option>
                                                  <option value="5">OPositive</option>
                                                  <option value="6">ONegative</option>
                                                  <option value="7">ABPositive</option>
                                                  <option value="8">ABNegative</option>
                                              </select>
                                          </div>

                                  </div>
                              </div>
                          </fieldset>
                          <fieldset class="scheduler-border">
                          <legend class="scheduler-border">Special Qualification</legend>
                              <div class="col-md-12">
                                  <div class="col-md-6">

                                      <label class="col-md-4" style="width:100px;margin-left:-13px">Name</label>

                                          <div class="col-md-4" style="margin-left: 5px">
                                              <select class="selectpicker serial" multiple data-actions-box="true" name="bloodGroup[]" id="bloodGroup" data-tags="true" data-placeholder="Select Blood Group" data-width="220px" data-allow-clear="true" >
                                                  <option value="1">APositive</option>
                                                  <option value="2">ANegative</option>
                                                  <option value="3">BPositive</option>
                                                  <option value="4">BNegative</option>
                                                  <option value="5">OPositive</option>
                                                  <option value="6">ONegative</option>
                                                  <option value="7">ABPositive</option>
                                                  <option value="8">ABNegative</option>
                                              </select>
                                          </div>

                                  </div>
                              </div>
                              <br><br>
                              <div class="col-lg-8">

                                  <div class="form-group">
                                  <label class="col-md-4">Date</label>

                                      <div class="col-md-2" style="margin-left:-12px">
                                          <select name="filterBirth" id="filterBirth" class="select2 serial" data-live-search="true" data-placeholder="Select Filter" data-width="120px">
                                              <option value="1">No Filter</option>
                                              <option value="2">=</option>
                                              <option value="3"><</option>
                                              <option value="4">></option>
                                              <option value="5">Between</option>
                                          </select>
                                      </div>
                                      <div class="col-md-2">
                                        <?php echo form_input(array('name' => 'fromDate', 'id'=>'fromDate', "class" => "datePicker form-control serial",'value' => date('d-m-Y'),
                                        'style' => 'width:100px; margin-left:65px','placeholder' => 'From Date')); ?>
                                      </div>


                                  </div>

                              </div>
                          </fieldset>
                        </div>
                    </div>
                </div>
            </table>
        </div>
        </form>

        <div class="col-md-4">
            <table id="datatable" class="table table-striped table-bordered"
                                   cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Rank</th>
                    <th>Posting Unit</th>
                    <th>Posting Date</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <div class="col-md-12">
            <div class="col-md-2">
                <input type="button"  class="btn btn-success search" name="submit" Value="Search"/>
                <textarea id="avc" style="display: none"></textarea>
            </div>
        </div>

    </div>
    <!-- <div class="col-md-12">
        <div class="col-md-2">
            <input type="submit"  class="btn btn-success search" name="submit" Value="Search"/>
        </div>
    </div> -->

</div>
<style type="text/css">
    .nav-tabs {
  overflow-x: auto;
  overflow-y: hidden;
  display: -webkit-box;
  display: -moz-box;
  height: 60px ;

}
.nav-tabs>li {
  float: none;
}
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
    }
</style>
<script>
function x(){
    return $('.serial').filter(function(){
        return $(this).val();
    }).serialize();
}
$( ".search" ).click(function() {
$('#avc').text(x());
var qrValue = $('#avc').val();
var filterBirth = $('#filterBirtha').val();
var fromDate = $('#fromDatea').val();
var endDate =$('#endDatea').val();
  $.ajax({
            type: "post",
            url: "<?php echo site_url('report/QueryTool/Sailor'); ?>/",
            data: {qrValue: qrValue, filterBirth : filterBirth, fromDate : fromDate, endDate : endDate},
            dataType: "json",
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
               //console.log(data);
            }
        });

});
</script>
