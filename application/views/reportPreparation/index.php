<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">

<div class="row">
    <form class="form-horizontal frmContent" id="MainForm" method="post">
        <span class="frmMsg"></span>
        <div class="col-md-12 panel panel-base" id="tabs">
            <div class="panel-heading">
                <div class="row">                    
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Course Roster Sating</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-12 panel-body">                
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-4">Course</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Course ">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-7" >
                            <select class="select2 form-control required" name="EXAM_TYPE_ID" id="course" data-placeholder="Select exam type" aria-hidden="true" data-allow-clear="true">
                                <option value="">Select One</option>
                                <?php
                                foreach ($subjectType as $row):
                                    ?>
                                    <option value="<?php echo $row->EXAM_ID ?>"><?php echo $row->NAME ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>                       
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-5">Course Type</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Course Type">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <select class="select2 form-control required" name="EXAM_NAME_ID" id="courseType" data-placeholder="Select Exam Name" aria-hidden="true" data-allow-clear="true">
                                <option value="">Select Exam Name</option>
                            </select>               
                        </div>
                    </div>
                </div>                            
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-5">Course Name</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Course Name">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <select class="select2 form-control required" name="EXAM_NAME_ID" id="courseName" data-placeholder="Select Exam Name" aria-hidden="true" data-allow-clear="true">
                                <option value="">Select Exam Name</option>
                            </select>
                        </div>
                    </div>
                </div>                
            </div>
            <div class="tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active "><a href="form-layouts-tabbed.html#tab-first" role="tab" data-toggle="tab">Branch & Rank</a></li>
                    <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Exam/Training/part ll </a></li>
                    <li><a href="form-layouts-tabbed.html#tab-third" role="tab" data-toggle="tab">Order by & Seniority Setting</a></li>
                </ul> 
                <div class="tab-content">
                    <div class="tab-pane active col-md-12" id="tab-first">
                        <div>
                            <div class="col-md-6">                                
                                    <label class="col-sm-2">Branch</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="EXAM_TYPE_ID" id="branchid" data-placeholder="Select exam type" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select One</option>
                                            <?php
                                            foreach ($branch as $row):
                                                ?>
                                                <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Branch">
                                        <i class="fa fa-question-circle"></i>
                                    </a>                                                              
                            </div>   
                            <div class="col-md-6">
                                <div>
                                    <label class="col-sm-2">Rank</label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control"  multiple="multiple" name="rankId[]" 
                                                id="rankName" data-tags="true" data-placeholder="Select Columns" data-allow-clear="true" >
                                            <option value="">Select Exam Name</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>    
                            <hr style="height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">
                        </div>
                    </div>
                    <div class="tab-pane col-md-12" id="tab-second">
                        <div class="col-md-9">
                            <div class="col-md-12">
                                <label class="col-sm-8" style="font-size: small;font-weight: bold;">Exam
                                </label>
                                <table id="sailorTable" class="table table-striped table-bordered trainTable"
                                       cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Code/Name</th>
                                            <th>Gain</th>
                                            <th>Is Mandatory</th>
                                            <th>Ouput Column</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                                <div class="xh" style="text-align: right;">
                                    <span class="btn btn-xs btn-success" id="add_record">
                                        <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                    </span>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <label class="col-sm-4" style="font-size: small;font-weight: bold;">Training
                                </label>
                                <table id="sailorTable2"
                                       class="table table-striped table-bordered trainTable"
                                       cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Code/Name</th>
                                            <th>Gain</th>
                                            <th>Is Mandatory</th>
                                            <th>Ouput Column</th>

                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>

                                <div class="xh" style="text-align: right;">
                                    <span class="btn btn-xs btn-success" id="add_record2">
                                        <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <table id="ReportTable"  class="table table-striped table-bordered hover" width="100%">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="checkAll"></th>
                                        <th>Part Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($partlist as $partlistData) { ?>
                                        <tr>

                                            <td><input type="checkbox" class="check"  value="<?php echo $partlistData->PartIIID; ?>" name="check[]"></td>

                                            <td>
                                                <input type="text" value="<?php echo $partlistData->Name; ?>" name="name[]">
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="xh" style="text-align: right;">
                            <span class="btn btn-xs btn-danger" id="add_record3">
                                <i style="cursor:pointer" class="fa fa-plus"> Add Check </i>
                            </span>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-third">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label class="legend">Order by</label>
                                <table class="table  table-bordered dataTable" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <td>
                                        <div class="checkbox checkbox-inline checkbox-primary"
                                             style="overflow:scroll;height:200px;width:222px">
                                            <input class="styled sequData" id="entryDate" type="checkbox"
                                                   value="sailor.ENTRYDATE"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">Total Point</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="equialRank" type="checkbox"
                                                   value="sailor.EQUIVALANTRANKID"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">Branch Position</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="examLevel" type="checkbox" value="sailor.LASTEDUCATIONID"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">Rank Position</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="batch" type="checkbox"
                                                   value="sailor.BATCHNUMBER"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">Seniority Date</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="dob" type="checkbox" value="sailor.BIRTHDATE"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">Promotion Date</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="fullName" type="checkbox"
                                                   value="sailor.FULLNAME"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">Official Number</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="examResult" type="checkbox"
                                                   value="a.ExamGradeID"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">Red Recom Seniority Date</label>
                                            <br/><br/>                                           
                                            <input class="styled sequData" id="examResult" type="checkbox"
                                                   value="a.ExamGradeID"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">LET Exam</label>
                                            <br/><br/>
                                            <input class="styled sequData" id="examResult" type="checkbox"
                                                   value="a.ExamGradeID"
                                                   name="sequData[]">
                                            <label for="ACTIVE_STATUS">SGC 'Q'</label>
                                            <br/><br/>
                                        </div>
                                    </td>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-5">
                                <label class="legend"></label>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-5">Seniority Date</label>
                                    <div class="col-md-4">
                                        <input name="navi" id="navi" type="radio" checked="che" value="Navy">Month
                                    </div>
                                    <div class="col-md-3">
                                        <input name="navi" id="navi" type="radio" value="ISMODC">Point
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-8 control-label">See Service Point (month)</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'code', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="col-sm-8 control-label">Req Due By Time (Year)</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'code', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required")); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="legend"></label>
                                <div class=" col-md-12">                                   
                                    <div class=" col-md-2">
                                        <input type="checkbox" name="CalcRankSenio" value="Yes" />
                                    </div>
                                    <label class="col-md-10">Calculate Rank Seniority</label>                                    
                                </div>
                                <div class=" col-md-12">
                                    <label class="col-md-7 control-label">Gain Value (per month)</label>
                                    <div class="col-md-5">
                                        <?php echo form_input(array('name' => 'code', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required")); ?>
                                    </div>
                                </div>
                                <div class=" col-md-12">
                                    <label class="col-md-7 control-label">Req Due By Time (Year)</label>
                                    <div class="col-md-5">
                                        <?php echo form_input(array('name' => 'code', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required")); ?>
                                    </div>
                                </div>


                                <div class=" col-md-12">                                    
                                    <label class="col-sm-7 control-label">Sn of red recom</label>
                                    <div class="col-sm-5">
                                        <select class="select2 form-control required" name="Gender" id="Gender" data-tags="true" data-placeholder="Select Gender" data-allow-clear="true">
                                            <option value="">Select Gender</option>
                                            <option value="0">D(Seniority Date)</option>
                                            <option value="1">E(Exam Date)</option>
                                            <option value="2">T(Training Date)</option>
                                            <option value="2">A(Art4 Rank Join Date)</option>
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <legend style="font-size: small;font-weight: bold;">Seniority Count
                                        For Red Recomm
                                    </legend>
                                    <table id="sailorTable3" class="table table-striped table-bordered"
                                           cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Recom.No</th>
                                                <th>Gain</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table> 
                                    <div class="xh" style="text-align: right;">
                                        <span class="btn btn-xs btn-success" id="add_record3">
                                            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <label class="legend"></label>
                <div class="col-md-8">                    
                    <table class="table table-striped table-bordered trainTable"
                           cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Branch</th>
                                <th>Rank</th>
                                <th>Action</th>                                    

                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>                
                </div>
                <div class="col-md-12" style="padding-left: 0 !important;">
                    <div class="col-sm-12">
                        <input type="button" class="btn pull-right btn-primary btn-sm formSubmitWithRedirect"
                               data-action="SpecialTransection/CapturingNewRecrutiInfo/save"
                               data-redirect-action="SpecialTransection/CapturingNewRecrutiInfo/index"
                               data-type="list" value="Save">
                    </div>
                </div>
            </div>
        </div>
</div>
</form>
</div>
<script>
    $(document).on('change', '#course', function (event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>report/RpTrainCourSet/course_by_trainingType',
            type: 'POST',
            dataType: 'html',
            data: {COURSE_TYPE: id},
            beforeSend: function () {
                $(".training_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#courseType').html(data);
                $('#courseType').select2('val', '');
            }
        });

    });
    $(document).on('change', '#courseType', function (event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>report/RpTrainCourSet/course_by_trainingName',
            type: 'POST',
            dataType: 'html',
            data: {COURSE_TYPE: id},
            beforeSend: function () {
                $(".training_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#courseName').html(data);
                $('#courseName').select2('val', '');
            }
        });
    });
    $('#examTable').DataTable({
        //"scrollY": '3vh',
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {"sZeroRecords": "",
            "sEmptyTable": ""},
        "columnDefs": [
            {width: '100%', targets: 1}
        ]
    });

    $('#trainingTable').DataTable({
        //"scrollY": '3vh',
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {"sZeroRecords": "",
            "sEmptyTable": ""},
        "columnDefs": [
            {width: '100%', targets: 1}
        ]
    });
    $('#ReportTable').DataTable({
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "scrollY": '200px',
        //"scrollX": false,
        "scrollCollapse": true,
        "paging": false,
        //"fixedHeader": true,
        "columnDefs": [
            {width: '100%', targets: 1},
            {width: '100%', targets: 0}
        ],
        "responsive": true,
    });


    $("#checkAll").click(function () {
        $(".check").prop('checked', $(this).prop('checked'));

    });


    $(document).on('change', '#branchid', function (event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>report/RpTrainCourSet/rankByBranch',
            type: 'POST',
            dataType: 'html',
            data: {branch_id: id},
            beforeSend: function () {
                $(".training_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankName').html(data);
                $('#rankName').select2('val', '');
            }
        });

    });


    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#sailorTable tbody").append(' <tr>' +
                '<td id="prexam_' + counter + '" style="display:none">' +
                ' <input type="hidden"  value="" name="prexam[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td style="width:30%" id="examcode_' + counter + '">' +
                '<select class="select2 form-control" data-live-search= "true"  name="codeExam[]">' +
                '<option value="0">SELECT CODE</option>' +
<?php foreach ($exam as $row) { ?>
            "<option value='<?php echo $row->EXAM_ID ?>'><?php echo htmlspecialchars($row->NAME) ?></option>" +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td id="examgain_' + counter + '">' +
                ' <input type="text"  value="" name="examgain[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td id="examisMandatory' + counter + '">' +
                ' <input type="checkbox" value = "0" name="examisMandatory[]"   class="tempCheck form-control " >' +
                '</td>' +
                '<td style="width:28%" id="examoutputColumn' + counter + '">' +
                '<select class="select2 form-control" data-live-search = "true" name="examoutputColumn[]">' +
                '<option value="1">Course-1(Date)</option>' +
                '<option value="2">Course-2(Date)</option>' +
                '<option value="3">Course-3(Date)</option>' +
                '</select> ' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });



    var counter = 1;
    $(document).on('click', '#add_record2', function () {
        counter++;
        $("#sailorTable2 tbody").append(' <tr>' +
                '<td id="prtrain_' + counter + '" style="display:none">' +
                ' <input type="hidden"  value="" name="prtrain_[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td style="width:30%" id="code_' + counter + '">' +
                '<select class="select2 form-control" data-live-search= "true"  name="code[]">' +
                '<option value="0">SELECT CODE</option>' +
<?php foreach ($code as $row) { ?>
            "<option value='<?php echo $row->NAVYTrainingID ?>'><?php echo htmlspecialchars($row->Name) ?></option>" +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td id="gain_' + counter + '">' +
                ' <input type="text"  value="" name="gain[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td id="isMandatory' + counter + '">' +
                ' <input type="checkbox" value = "0" name="isMandatory[]"   class="tempCheck form-control " >' +
                '</td>' +
                '<td style="width:28%" id="outputColumn' + counter + '">' +
                '<select class="select2 form-control" data-live-search = "true" name="outputColumn[]">' +
                '<option value="1">Course-1(Date)</option>' +
                '<option value="2">Course-2(Date)</option>' +
                '<option value="3">Course-3(Date)</option>' +
                '</select> ' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr2"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
    });
    $(document).on('click', '#remove_tr2', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    var counter = 1;
    $(document).on('click', '#add_record3', function () {
        counter++;
        $("#sailorTable3 tbody").append(' <tr>' +
                '<td id="prrecomn_' + counter + '" style="display:none">' +
                ' <input type="hidden" name="prrecomn[]"   class="form-control "  >' +
                '</td>' +
                '<td  id="recomnRec_' + counter + '">' +
                ' <input type="text"  value="" name="recomnRec[]"  class="form-control" >' +
                '</td>' +
                '<td id="recomnGain_' + counter + '">' +
                ' <input type="text" name="recomnGain[]"   class="form-control " >' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr2"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );

    });
    $(document).on('click', '#remove_tr3', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

</script>