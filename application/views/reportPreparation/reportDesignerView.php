<link href="<?php echo base_url(); ?>dist/styles/jquery-ui.css" rel="stylesheet">
<div class="" id="myWizard">

   <h3>Report Designer</h3>

   <hr>

   <div class="progress">
     <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="1" aria-valuemin="1" aria-valuemax="5" style="width: 20%;">
       Step 1 of 5
     </div>
   </div>

   <div class="navbar">
      <div class="navbar-inner">
            <ul class="nav nav-pills">
               <li class="active"><a href="#step1" data-toggle="tab" data-step="1">Basic Report Setup</a></li>
               <li><a href="#step2" data-toggle="tab" data-step="2">Select Columns</a></li>
               <li><a href="#step3" data-toggle="tab" data-step="3">Order BY</a></li>
               <li><a href="#step4" data-toggle="tab" data-step="4">Coulmn Positioning And Group By</a></li>
               <li><a href="#step5" data-toggle="tab" data-step="5">Setup Font And Page</a></li>
            </ul>
      </div>
   </div>
   <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
   <div class="tab-content">
      <div class="tab-pane fade in active" id="step1">

        <div class="well">
          <fieldset class="scheduler-border">
          <legend class="scheduler-border">Header</legend>

              <div class="col-md-12">
                  <div class="form-group">
                      <label class="col-md-2">Report Name</label>

                      <div class="col-md-6">
                          <?php echo form_input(array('name' => 'reportName','id'=>'reportName','type' => 'text', "class" => "form-control", 'style' =>'width:350px', 'placeholder' => 'Enter Report Name')); ?>
                      </div>

                  </div>
              </div>
                <br><br>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2">Report Title</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'reporttile','id'=>'reporttile','type' => 'text', "class" => "form-control", 'style' =>'width:350px', 'placeholder' => 'Enter Report Title')); ?>
                        </div>

                    </div>
                </div>
                <br><br>
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-md-2">Report Sub Title</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'reportSubtile','id'=>'reportSubtile','type' => 'text', "class" => "form-control", 'style' =>'width:350px', 'placeholder' => 'Enter Report Sub Title')); ?>
                        </div>

                    </div>
                </div>
                <br><br>
                <div class="col-md-12">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-md-4" style="margin-left:-15px">Report Header</label>
                                <div class="col-md-2">
                                    <?php echo form_input(array('name' => 'reportHeader','id'=>'reportHeader','type' => 'text', "class" => "form-control", 'style' =>'width:300px;margin-left:10px', 'placeholder' => 'Enter Report Header')); ?>
                                </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-md-4">Report Footer</label>

                              <div class="col-md-2">
                                  <?php echo form_input(array('name' => 'reportFooter','id'=>'reportFooter','type' => 'text', "class" => "form-control", 'style' =>'width:300px', 'placeholder' => 'Enter Report Footer')); ?>
                              </div>

                          </div>
                      </div>
                </div>
                <br><br>
                <div class="col-md-12">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-md-4" style="margin-left:-15px">Report Type</label>
                                <div class="col-md-2" style="margin-left:10px">
                                  <select name="reportType" id="reportType" class="select2"
                                          data-live-search="true" data-placeholder="Select Report Type" data-width="200px" >
                                      <option value="">Select Report Type</option>
                                      <?php
                                          foreach ($reporttype as $key) {
                                      ?>
                                      <option value="<?php echo $key->ReportTypeID?>"><?php echo $key->ReportTypeName; ?></option>
                                    <?php
                                        }
                                    ?>
                                  </select>
                                </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="col-md-4">Sub Report Type</label>

                              <div class="col-md-2">
                                <select name="subReportType" id="subReportType" disabled class="select2"
                                        data-live-search="true" data-placeholder="Select Sub Report Type" data-width="200px" >

                                </select>
                              </div>
                          </div>
                      </div>
                </div>
          </fieldset>

        </div>

         <a class="btn btn-default btn-lg next" href="#">Next</a>
      </div>
      <div class="tab-pane fade" id="step2">
         <div class="well">
         <fieldset class="scheduler-border">
           <legend class="scheduler-border">Add Columns</legend>
                  <!-- <div style="height: 350px; overflow: auto;"> -->
                     <table id="datatable" class="table table-striped table-bordered tbl"
                            cellspacing="0" width="100%">
                         <thead>
                             <tr>
                                 <th>SL</th>
                                 <th></th>
                                 <th>Table Name</th>
                                 <th>Column Name</th>
                                 <th>Column Caption</th>
                            </tr>
                         </thead>
                         <tbody>
                           <?php
                           $i=1;
                              foreach ($report as $value) {
                                // .' '.$value->ColumnAlias
                              ?>
                              <tr>
                                <td><?php echo $i++ ?></td>
                                <td><input type="checkbox" id="Check" class="checkbox" name="check[]" value="<?php echo $value->ActualTableName.'.'.$value->ActualColumnName ?>"></td>
                                <td class="tableClass"><input type="text" class="form-control tabler" id="actualTableName<?php echo $i;?>" name="actualTableName[]" value="<?php echo $value->ActualTableName ?>"  readonly  style="border: none"></td>
                                <td><input type="text" class="form-control column" id="actualColumnName<?php echo $i;?>" name="actualColumnName[]" value="<?php echo $value->ColName ?>"  readonly  style="border: none"></td>
                                <td class="aliasClass"><input type="text" class="form-control alias" id="columnAlias<?php echo $i;?>" name="columnAlias[]" value="<?php echo $value->ColumnAlias ?>"></td>
                              </tr>
                              <?php
                                }
                               ?>
                         </tbody>
                     </table>
                 <!-- </div> -->
         </fieldset>
       </div>
         <a class="btn btn-default next" href="#">Next</a>
      </div>
      <div class="tab-pane fade" id="step3">
        <div class="well">
          <fieldset class="scheduler-border">
            <legend class="scheduler-border">OrderBy</legend>

                      <table id="tbl2" class="table table-striped table-bordered trainTable tbl2"
                             cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>SL</th>
                                  <th></th>
                                  <th>Table Name</th>
                                  <th>Column Name</th>
                                  <th>Column Caption</th>
                             </tr>
                          </thead>
                          <tbody id="tbl2tbody">

                          </tbody>
                      </table>

          </fieldset>
        </div>
         <a class="btn btn-default next" href="#">Next</a>
      </div>
      <div class="tab-pane fade" id="step4">
        <div class="well">
          <fieldset class="scheduler-border">
            <legend class="scheduler-border">Column Positing And Group By</legend>

                   <div class="col-md-12">
                     <div class="col-md-8">
                      <table id="tbl3" class="table table-striped table-bordered trainTable tbl3"
                             cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>SL</th>
                                  <th></th>
                                  <th>Table Name</th>
                                  <th>Column Name</th>
                                  <th>Column Caption</th>
                             </tr>
                          </thead>
                          <tbody id="tbl3tbody">

                          </tbody>
                      </table>
                    </div>
                    <div class="col-md-4">
                      <table id="tbl4" class="table table-striped table-bordered trainTable tbl4"
                             cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                <th>SL</th>
                                <th></th>
                                <th>Column Name</th>
                              </tr>
                          </thead>
                          <tbody id="tbl4tbody">
                            <!-- <tr>
                            </tr> -->
                          </tbody>
                      </table>
                    </div>
                   </div>

          </fieldset>
        </div>
         <a class="btn btn-default next" href="#">Next</a>
        </div>
      <div class="tab-pane fade" id="step5">
        <div class="well">
          <fieldset class="scheduler-border">
            <legend class="scheduler-border">Page Setup</legend>
                  <div class="col-md-12">
                      <div class="form-group">
                           <label class="col-sm-2">Page Size</label>

                           <div class="col-sm-8">
                               <select class="select2" name="pageSize" id="" data-tags="true"
                                       data-placeholder="Select Page Size" data-allow-clear="true">
                                   <option value="Default">Default</option>
                                   <option value="B5">B5</option>
                                   <option value="B5-L">B5 Landscape</option>
                                   <option value="A6">A6</option>
                                   <option value="A6-L">A6 Landscape</option>
                                   <option value="A5">A5</option>
                                   <option value="A5-L">A5 Landscape</option>
                                   <option value="A4">A4</option>
                                   <option value="A4-L">A4 Landscape</option>
                                   <option value="A3">A3</option>
                                   <option value="A3-L">A3 Landscape</option>
                                   <option value="A2">A2</option>
                                   <option value="A2-L">A2 Landscape</option>
                                   <option value="A1">A1</option>
                                   <option value="A1-L">A1 Landscape</option>
                                   <option value="US Letter">US Letter</option>
                                   <option value="US Legal">US Legal</option>
                                   <option value="US Ledger">US Ledger</option>
                               </select>
                           </div>
                       </div>
                    </div>
                    <div class="col-md-12">
                      <div class="col-md-6">
                        <label class="col-sm-4">Top Margin</label>
                          <div class="col-sm-6">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                                    data-field="topMargin">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                    <input type="text" name="topMargin" class="form-control input-number" value="1cm" min="0"
                                           max="100">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus"
                                                    data-field="topMargin">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                </div>

                            </div>
                            <br>
                            <br>
                            <label class="col-sm-4">Bottom Margin</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                                    data-field="bottomMargin">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                    <input type="text" name="bottomMargin" class="form-control input-number" value="1cm" min="0"
                                           max="100">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus"
                                                    data-field="bottomMargin">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                </div>

                            </div>

                      </div>
                      <div class="col-md-6">
                        <label class="col-sm-4">Right Margin</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                                    data-field="rightMargin">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                    <input type="text" name="rightMargin" class="form-control input-number" value="1cm" min="0"
                                           max="100">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus"
                                                    data-field="rightMargin">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                  </div>
                            </div>
                        <br/><br/>
                        <label class="col-sm-4">Left Margin</label>
                          <div class="col-sm-6">
                                <div class="input-group">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                                    data-field="leftMargin">
                                                <span class="glyphicon glyphicon-minus"></span>
                                            </button>
                                        </span>
                                    <input type="text" name="leftMargin" class="form-control input-number" value="1cm" min="0"
                                           max="100">
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-success btn-number" data-type="plus"
                                                    data-field="leftMargin">
                                                <span class="glyphicon glyphicon-plus"></span>
                                            </button>
                                        </span>
                                </div>
                          </div>
                        </div>
                    </div>
          </fieldset>
          <fieldset class="scheduler-border">
            <legend class="scheduler-border">Setup Font</legend>
                   <div class="col-md-12">
                      <div class="col-md-6">
                          <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Report Title</legend>
                            <label class="col-sm-4">Font Name</label>
                              <div class="col-sm-6">
                                <select name="rtfontName" id="rtfontName" class="select2"
                                        data-live-search="true" data-placeholder="Select Font Name" >
                                        <option value="">Select Font Name</option>
                                        <option value="Arial">Arial</option>
                                        <option value="Helvetica Neue">Helvetica Neue</option>
                                        <option value="Helvetica">Helvetica</option>
                                        <option value="sans-serif">sans-serif</option>
                                </select>
                              </div>
                              <br><br>
                              <label class="col-sm-4">Font Size</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control" name="rtfontSize" value="10.00">
                              </div>
                              <br><br>
                              <div class="col-sm-6">
                                <input type="checkbox" name="rtbold" value="1">&nbsp;&nbsp;<label>Bold</label>
                                <input type="checkbox" name="rtItalic" value="1">&nbsp;&nbsp;<label>Italic</label>
                                <br>
                                <input type="checkbox" name="rtUnderline" value="1">&nbsp;&nbsp;<label>UnderLine</label>
                              </div>
                          </fieldset>
                      </div>
                      <div class="col-md-6">
                        <fieldset class="scheduler-border">
                          <legend class="scheduler-border">Report Sub Title</legend>
                          <label class="col-sm-4">Font Name</label>
                              <div class="col-sm-6">
                                <select name="rstfontName" id="rstfontName" class="select2"
                                        data-live-search="true" data-placeholder="Select Font Name" >
                                        <option value="">Select Font Name</option>
                                        <option value="Arial">Arial</option>
                                        <option value="Helvetica Neue">Helvetica Neue</option>
                                        <option value="Helvetica">Helvetica</option>
                                        <option value="sans-serif">sans-serif</option>
                                </select>
                              </div>
                              <br><br>
                              <label class="col-sm-4">Font Size</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control" name="rstfontSize" value="10.00">
                              </div>
                              <br><br>
                              <div class="col-sm-6">
                                <input type="checkbox" name="rstbold" value="1">&nbsp;&nbsp;<label>Bold</label>
                                <input type="checkbox" name="rstItalic" value="1">&nbsp;&nbsp;<label>Italic</label>
                                <br>
                                <input type="checkbox" name="rstUnderline" value="1">&nbsp;&nbsp;<label>UnderLine</label>
                              </div>
                        </fieldset>
                      </div>
                   </div>
                   <div class="col-md-12">
                      <div class="col-md-6">
                          <fieldset class="scheduler-border">
                            <legend class="scheduler-border">Column Header</legend>
                            <label class="col-sm-4">Font Name</label>
                              <div class="col-sm-6">
                                <select name="chfontName" id="chfontName" class="select2"
                                        data-live-search="true" data-placeholder="Select Font Name" >
                                        <option value="">Select Font Name</option>
                                        <option value="Arial">Arial</option>
                                        <option value="Helvetica Neue">Helvetica Neue</option>
                                        <option value="Helvetica">Helvetica</option>
                                        <option value="sans-serif">sans-serif</option>
                                </select>
                              </div>
                              <br><br>
                              <label class="col-sm-4">Font Size</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control" name="chfontSize" value="10.00">
                              </div>
                              <br><br>
                              <div class="col-sm-6">
                                <input type="checkbox" name="chbold" value="1">&nbsp;&nbsp;<label>Bold</label>
                                <input type="checkbox" name="chItalic" value="1">&nbsp;&nbsp;<label>Italic</label>
                                <br>
                                <input type="checkbox" name="chUnderline" value="1">&nbsp;&nbsp;<label>UnderLine</label>
                              </div>
                          </fieldset>
                      </div>
                      <div class="col-md-6">
                        <fieldset class="scheduler-border">
                          <legend class="scheduler-border">Column Body</legend>
                          <label class="col-sm-4">Font Name</label>
                              <div class="col-sm-6">
                                <select name="cbfontName" id="cbfontName" class="select2"
                                        data-live-search="true" data-placeholder="Select Font Name" >
                                        <option value="">Select Font Name</option>
                                        <option value="Arial">Arial</option>
                                        <option value="Helvetica Neue">Helvetica Neue</option>
                                        <option value="Helvetica">Helvetica</option>
                                        <option value="sans-serif">sans-serif</option>
                                </select>
                              </div>
                              <br><br>
                              <label class="col-sm-4">Font Size</label>
                              <div class="col-sm-6">
                                <input type="text" class="form-control" name="cbfontSize" value="10.00">
                              </div>
                              <br><br>
                              <div class="col-sm-6">
                                <input type="checkbox" name="cbbold" value="1">&nbsp;&nbsp;<label>Bold</label>
                                <input type="checkbox" name="cbItalic" value="1">&nbsp;&nbsp;<label>Italic</label>
                                <br>
                                <input type="checkbox" name="cbUnderline" value="1">&nbsp;&nbsp;<label>UnderLine</label>
                              </div>
                        </fieldset>
                      </div>
                   </div>
          </fieldset>

        </div>
        <a class="btn btn-success first" href="#">Start over</a>
        <input type="button" class="btn btn-primary btn-md formSubmitWithRedirect"
               data-action="report/reportDesigner/save" data-type="list" value="submit">

      </div>
   </div>
   </form>
</div>
<style>
.dataTables_filter{
display: none;
}
fieldset.scheduler-border {
    border: 1px groove #ddd !important;
    padding: 0 1.4em 1.4em 1.4em !important;
    margin: 0 0 1.5em 0 !important;
    -webkit-box-shadow:  0px 0px 0px 0px #000;
            box-shadow:  0px 0px 0px 0px #000;
}

legend.scheduler-border {
        font-size: 1.2em !important;
        font-weight: bold !important;
        text-align: left !important;
        width:auto;
        padding:0 10px;
        border-bottom:none;
    }
</style>
<script>
// Start Report Type Wise Sub Report Type Return
$(document).on("change", "#reportType", function () {
    $("#subReportType").prop('disabled', false);
    var id = $(this).val();
    $.ajax({
        type: "post",
        url: "<?php echo site_url('report/reportDesigner/reportype'); ?>/",
        data: {id: id},
        beforeSend: function () {
            $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
        },
        success: function (data) {
            $('#subReportType').html(data);
        }
    });
});
// End Report Type Wise Sub Report Type Return

/* Start Clone Table column */
$('.tbl tbody tr td input.checkbox').on('change', function (e) {
    if( $(this).is(':not(:checked)')){
    return false;
    }
     var row = $(this).closest('tr').html();
     $('.tbl2 tbody').append('<tr>'+row+'</tr');
     $('#tbl2 tbody tr td input:checkbox').attr('name','orderBy[]');
     $('#tbl2 .tabler').attr('name', 'tableOrderBy[]');
     $('#tbl2 .column').attr('name', 'columnOrderBy[]');
     $('#tbl2 .alias').attr('name', 'aliasOrderBy[]');
});
$('.tbl tbody tr td input.checkbox').on('change', function (e) {
    if( $(this).is(':not(:checked)')){
    return false;
    }
     var row = $(this).closest('tr').clone();
     $('.tbl3 tbody').append(row);
     $('#tbl3 tbody tr td input:checkbox').attr('name','positionBy[]');
     $('#tbl3 .tabler').attr('name', 'tablePositionBy[]');
     $('#tbl3 .column').attr('name', 'columnPositionBy[]');
     $('#tbl3 .alias').attr('name', 'aliasPositionBy[]');



});

$('.tbl tbody tr td input.checkbox').on('change', function (e) {
    if( $(this).is(':not(:checked)')){
    return false;
    }
     var row = $(this).closest('tr').html();
     $('.tbl4 tbody').append('<tr>'+row+'</tr>');
     $('#tbl4 tbody tr td input:checkbox').attr('name','groupBy[]');
     $('#tbl4 .column').attr('name', 'columnGroupBy[]');
     $('#tbl4 .tableClass').attr('style', 'display:none');
     $('#tbl4 .aliasClass').attr('style', 'display:none');

});
/* End: Clone Table column */
//Step Wise Report Generation
$('.next').click(function(){

  var nextId = $(this).parents('.tab-pane').next().attr("id");
  $('[href=#'+nextId+']').tab('show');
  return false;

});

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

  //update progress
  var step = $(e.target).data('step');
  var percent = (parseInt(step) / 5) * 100;

  $('.progress-bar').css({width: percent + '%'});
  $('.progress-bar').text("Step " + step + " of 5");

  //e.relatedTarget // previous tab

});

$('.first').click(function(){

  $('#myWizard a:first').tab('show')

});

//Start Drag And Drop
$('#tbl2tbody').sortable({
    helper: fixWidthHelper
}).disableSelection();

function fixWidthHelper(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
}
//End Drag and Drop
//Start Drag And Drop
$('#tbl3tbody').sortable({
    helper: fixWidthHelper
}).disableSelection();

function fixWidthHelper(e, ui) {
    ui.children().each(function() {
        $(this).width($(this).width());
    });
    return ui;
}
//End Drag and Drop
//Start btn-number Plus And Minus
$('.btn-number').click(function (e) {
    e.preventDefault();

    fieldName = $(this).attr('data-field');
    type = $(this).attr('data-type');
    var input = $("input[name='" + fieldName + "']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if (type == 'minus') {

            if (currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            }
            if (parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if (type == 'plus') {

            if (currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if (parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
//End btn-number Plus Minus
</script>
