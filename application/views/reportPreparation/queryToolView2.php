<script src="<?php echo base_url(); ?>dist/scripts/owl.carousel.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/styles/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>dist/styles/owl.theme.default.min.css">



<div class="panel-heading">
    <div class="row">
        <div class="col-md-11 col-sm-10 col-xs-8 ">
            <h3 class="panel-title">
                <center>Query Tool</center>
            </h3>
        </div>
    </div>
</div>
<div class="panel-body">
    <!-- <form class="form-horizontal frmContent" id="MainForm" method="post"> -->
    <span class="frmMsg"></span>
    <div class="col-md-12">

        <div class="col-md-6" >
            <ul class="tabs">
              <li class="item"><a class="acon" data-target="tab-one">General</a></li>
              <li class="item"><a class="acon" data-target="tab-two">Reports</a></li>
              <li class="item"><a class="acon" data-target="tab-three">Foreign</a></li>
              <li class="item"><a class="acon" data-target="tab-four">Engagement,assessment,Branch Change</a></li>
              <li class="item"><a class="acon" data-target="tab-five">Course</a></li>
              <li class="item"><a class="acon" data-target="tab-six">Tab Six</a></li>
              <li class="item"><a class="acon" data-target="tab-seven">Tab Seven</a></li>
              <li class="item"><a class="acon" data-target="tab-eight">Tab Eight</a></li>
            </ul>

            <div class="tab-content">
              <div class="tab tab-one active">One Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, iusto!</div>
              <div class="tab tab-two">Two ipsum dolor sit amet, consectetur</div>
              <div class="tab tab-three">Three</div>
              <div class="tab tab-four">Four</div>
              <div class="tab tab-five">Five</div>
              <div class="tab tab-six">Six</div>
              <div class="tab tab-seven">Seven</div>
              <div class="tab tab-eight">Eight</div>
            </div>

        </div>


        <div class="col-md-6">
            <table id="sailorTable" class="table table-striped table-bordered"
                                   cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Rank</th>
                    <th>Posting Unit</th>
                    <th>Posting Date</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <!-- </form> -->
    </div>
</div>
<style type="text/css">
.acon {
  color: black;

  text-decoration: none;
  font-size: 12px;
  text-transform: uppercase;
}

.tabs {
  list-style-type: none;
  max-width: 500px;
  margin: 2px auto;
  background: #DBEAF9;
  position: relative;
}

.tab-content {
  max-width: 700px;

  margin: 0 auto;
}

.item {
  padding: 10px 20px;
  white-space: nowrap;
  transition: all 0.3s ease-in;
  border-bottom: 4px solid ;
  border-top: 4px solid ;
}

.item:hover {
  border-bottom: 4px solid white;
  opacity: 0.7;
  cursor: pointer;

}

.tab-content > div {
  display: none;
}

.tab-content > div.active {
  display: block;
}

.owl-theme .owl-controls .owl-nav {
  position: absolute;
  width: 95%;
  top: 0;
}

.owl-theme .owl-controls .owl-nav [class*="owl-"] {
  position: absolute;
  background: none;
  color: black;
}

.owl-theme .owl-controls .owl-nav [class*="owl-"]:hover {
  background: none;
  color: black;
}

.owl-theme .owl-controls .owl-nav .owl-next {
  right: 0;
  transform: translate(100%);

}

.owl-theme .owl-controls .owl-nav .owl-prev {
  left: 0;
  transform: translate(-100%);
  margin-left: -50px;
}

</style>
<script>
    //OWL Carousel
$('.tabs').owlCarousel({
    loop: true,
    nav: true,
    navText: [
    '<i class="fa fa-chevron-left"></i>',
    '<i class="fa fa-chevron-right"></i>'
    ],
    dots: false
});

//Tabs

$('.tabs li a').click(function() {
    var activeLink = $(this).data('target');
  var targetTab = $('.tab.'+activeLink);

  targetTab.siblings().removeClass('active');
    targetTab.addClass('active');
});

</script>
