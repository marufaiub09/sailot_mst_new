<?php
header("Content-type: text/css");
$white = '#fff';
$dkgray = '#333';
$dkgreen = '#008400';
?>
<style>

    body {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        margin-top: <?php echo $_POST['topMargin'] ?>;
        margin-bottom: <?php echo $_POST['bottomMargin'] ?>;
        margin-left: <?php echo $_POST['leftMargin'] ?>;
        margin-right: <?php echo $_POST['rightMargin'] ?>;

    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table td, .table th {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        border: 1px <?php echo $_POST['borderType'] ?> <?php echo $_POST['borderColour'] ?>;
    }

    table.table-condensed {
        border: 1px solid <?php echo $dkgreen ?>;
    }
</style>


<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php echo base_url('dist/img/navy_logo2.jpg'); ?>"/>
    <h5 style="font-size: <?php echo $_POST['fontSize'] ?>">Sailor Management System.</h5>
    <span style=" text-align: center"><b><?php
foreach ($_POST['reportTitle'] as $reportTitle) {
    $reportTitle;
}
echo $reportTitle;
?></b><hr></span>
</div>
<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <?php
            error_reporting('0');
            foreach ($_POST['columnName'] as $columnName) {
                $columnName;
            }
            foreach ($_POST['tableName'] as $tableName) {
                $tableName;
            }

            if (empty($columnName)) {
                $data1['result'] = $this->db->list_fields($tableName);
                foreach ($data1['result'] as $value) {
                    ?>
                    <th><?php echo $value; ?></th>
                    <?php
                }
            } else {
                foreach ($columnName as $value) {
                    ?>
                    <th><?php echo $value; ?></th>
                    <?php
                }
            }
            ?>
        </tr>
    </thead>

    <tbody>
        <?php
        foreach ($_POST['joinType'] as $joinType) {
            $joinType;
        }
        ?>
        <?php
        /* $regEx = '/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|1‌​1)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468]‌​[048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(0‌​2)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02‌​)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/'; */
        switch (!empty($joinType)) {
            case ($joinType == 'JOIN'):
                foreach ($_POST['tableName'] as $tableName) {
                    $tableName;
                }
                foreach ($_POST['columnName'] as $columnName) {
                    $columnName;
                }
                foreach ($_POST['reportTitle'] as $reportTitle) {
                    $reportTitle;
                }
                foreach ($_POST['genQuery'] as $genQuery) {
                    $genQuery;
                }
                foreach ($_POST['columnName'] as $columnName) {
                    $columnName;
                }


                if (empty($_POST['columnName'])) {


                    $data = $this->db->query('SELECT * FROM ' . $tableName . ' ' . $genQuery . ' ');
                } else {
                    $column = implode(",", $_POST['columnName']);

                    $data = $this->db->query('SELECT ' . $column . ' FROM ' . $tableName . ' ' . $genQuery . ' ');
                }
                break;
        }

        /* echo '<pre>';
          print_r($data);
          exit(); */
        ?>



        <?php
        foreach ($data->result() as $row) {
            ?>
            <tr>
            <?php
            if (empty($columnName)) {
                $data1['result'] = $this->db->list_fields($tableName);
                foreach ($data1['result'] as $value) {
                    ?>
                        <td><?php echo $row->$value; ?></td>
                        <?php
                    }
                } else {
                    foreach ($columnName as $value) {
                        ?>
                        <td><?php echo $row->$value; ?></td>
                        <?php
                    }
                }
                ?>
            </tr>
                <?php
            }
            ?>

    </tbody>
</table>

