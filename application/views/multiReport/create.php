<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Assesment Year</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Year">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'assesment_year',  'type'=>'text', "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Enter Year')); ?>
        </div>
    </div>
  
		<div class="form-group">
				<label class="col-sm-3 control-label">Zone</label>
				<a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select zone">
					<i class="fa fa-question-circle"></i>
				</a>
				<div class="col-sm-6">
					<select class="select2 form-control required" name="ZONE_ID" id="ZONE_ID" data-tags="true" data-placeholder="Select Zone" data-allow-clear="true">
						<option value="">Select Zone</option>
						<?php
						foreach ($zone as $row):
							?>
							<option value="<?php echo $row->ADMIN_ID ?>"><?php echo $row->NAME ?></option>
						<?php
						endforeach; 
						?>
					</select>
				</div>
		</div>
	

		<div class="form-group">
					<label class="col-sm-3 control-label">Area</label>
					<a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Area">
						<i class="fa fa-question-circle"></i>
					</a>
					<div class="col-sm-6">
						<select class="select2 form-control required" name="AREA_ID" id="AREA_ID" data-tags="true" data-placeholder="Select Area" data-allow-clear="true">
							<option value="">Select Area</option>                        
						</select>
					</div>
		</div>
		
		<div class="form-group">
					<label class="col-sm-3 control-label">Ship Establishment</label>
					<a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Area">
						<i class="fa fa-question-circle"></i>
					</a>
					<div class="col-sm-6">
						<select class="select2 form-control required" name="SHIP_TYPEID" id="SHIP_TYPEID" data-tags="true" data-placeholder="Select Ship Establishment" data-allow-clear="true">
							<option value="">Select Ship Establishment</option>                        
						</select>
					</div>
		</div>
	<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Official Number</th>
            <th>Full Name</th>
			<th>Rank</th>
            <th>Character</th>
			<th>Efficiency</th>
            <th>Assessment Ship</th>
			<th>Last yr. Character</th>
			<th>Last yr. Efficiency</th>
			<th>Last yr. Ship</th>
			
        </tr>
    </thead>
    <tfoot>
        <tr>
		    <th>SN</th>
            <th>Official Number</th>
            <th>Full Name</th>
			<th>Rank</th>
            <th>Character</th>
            <th>Efficiency</th>
            <th>Assessment Ship</th>
			<th>Last yr. Character</th>
			<th>Last yr. Efficiency</th>
			<th>Last yr. Ship</th>
			
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php // echo $row->VISIT_INFO_ID ?>">
                <td><?php  echo $key + 1 ?></td>
                <td><input type="text" name="official_number" class="form-control required"></td>
                <td><input type="text" name="official_number" class="form-control required"></td>
                <td><input type="text" name="official_number" class="form-control required"></td>
                <td><input type="text" name="official_number" class="form-control required"></td>
				<td><input type="text" name="official_number" class="form-control required"></td>
				<td><input type="text" name="official_number" class="form-control required"></td>
				<td><input type="text" name="official_number" class="form-control required"></td>
				<td><input type="text" name="official_number" class="form-control required"></td>
				<td><input type="text" name="official_number" class="form-control required"></td>
				
            </tr>
    <?php } ?>
    </tbody>
</table>
	
    <!--div class="form-group">
        <label class="col-sm-3 control-label">Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Visit Information Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php //echo form_input(array('name' => 'name', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Visit Information name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Description</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Description">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php //echo form_textarea(array('name' => 'desc', "cols" => "3", "rows" => "4", "class" => "form-control ", 'placeholder' => '')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Visit Class</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Visit Class Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="VISIT_CI_ID" id="VISIT_CI_ID" data-tags="true" data-placeholder="Select Visit Class" data-allow-clear="true">
                <option value="">Select Trade</option>
                <?php
               // foreach ($visitClass as $row):
                 //   ?>
                 /  <option value="<?php //echo $row->VISIT_CLASSIFICATIONID ?>"><?php //echo $row->NAME ?></option>
                <?php
                //endforeach; 
                ?>
            </select>
        </div>
    </div-->
    <!--div class="form-group">
        <label class="col-sm-3 control-label"><?php //echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php //echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div-->
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/visitinformation/saveVI" data-su-action="setup/visitinformation/viList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script>
    $(document).ready(function() {
        $("#ZONE_ID").on('change', function(){
           var zone = this.value;
           $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>/sailorsInfo/assessmentInfo/areaByZone",
                data: {zoneId: zone},
                success: function (data) {
                    $("#AREA_ID").html(data);
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function() {
        $("#AREA_ID").on('change', function(){
			var area = this.value;
		
           $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>/sailorsInfo/assessmentInfo/shipbyarea",
                data: {areaId: area},
                success: function (data) {
                    $("#SHIP_TYPEID").html(data);
                }
            });
        });
    });
</script>


