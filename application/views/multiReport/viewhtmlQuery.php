<script type="text/javascript">
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
            tmpColDelim = String.fromCharCode(11), // vertical tab character
            tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
            colDelim = '","',
            rowDelim = '"\r\n"',
            // Grab text from table into CSV formatted string
            csv = '"' + $rows.map(function (i, row) {
                var $row = $(row),
                $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                    text = $col.text();

                    return text.replace(/"/g, '""'); // escape double quotes

                }).get().join(tmpColDelim);

            }).get().join(tmpRowDelim)
            .split(tmpRowDelim).join(rowDelim)
            .split(tmpColDelim).join(colDelim) + '"',
            // Data URI
            csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
            .attr({
                'download': filename,
                'href': csvData,
                'target': '_blank'
            });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // CSV
            exportTableToCSV.apply(this, [$('#datatable'), 'export.csv']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>
<a href="#" class="export">
    <button>CSV/EXCEL</button>
</a>
<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <?php
            error_reporting('0');
            //echo $_POST['tableName'];
            //exit();
            foreach ($_POST['joinType'] as $joinType) {
                $joinType;
            }
            foreach ($_POST['tableName'] as $tableName) {
                $tableName;
            }
            if (empty($_POST['columnName'])) {
                $data1['result'] = $this->db->list_fields($tableName);
                foreach ($data1['result'] as $value) {
                    ?>
                    <th><?php echo $value; ?></th>
                    <?php
                }
            } else {
                foreach ($_POST['columnName'] as $value) {
                    ?>
                    <th><?php echo $value; ?></th>
                    <?php
                }
            }
            ?>
        </tr>
    </thead>

    <tbody>
        <?php
        //error_reporting('0');




        foreach ($_POST['joinType'] as $joinType) {
            // echo  $value1;
            $joinType;
        }
        ?>
        <?php
        /* $regEx = '/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|1‌​1)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468]‌​[048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(0‌​2)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02‌​)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/'; */

        switch (!empty($joinType)) {

            case ($joinType == 'JOIN'):
                foreach ($_POST['tableName'] as $tableName) {
                    $tableName;
                }
                foreach ($_POST['columnName'] as $columnName) {

                    $columnName;
                    //$str = "Name [varchar]";
//                $str = explode(".", $value7);
//                $value7 = $str[0];;
                }
                foreach ($_POST['genQuery'] as $genQuery) {
                    $genQuery;
                }


                if (empty($_POST['columnName'])) {
                    // $this->db->query("SELECT * FROM' . '  ' . $tableName . ' ' . $genQuery");
                    //  $this->db->query('SELECT * FROM ' . $table_name . ' ORDER BY '.$colName.' '.$sort_type.' LIMIT '.$start.', ' . $length . ' ');
                    $data = $this->db->query('SELECT * FROM ' . $tableName . ' ' . $genQuery . ' ');

                    //var_dump($data);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    // $this->db->query("SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $tableName . ' ' . $genQuery");
                    $data = $this->db->query('SELECT ' . $column . ' FROM ' . $tableName . ' ' . $genQuery . ' ');
                    // var_dump($data);
                }
                break;
        }
        ?>
        <?php
        foreach ($data->result() as $row) {
            ?>
            <tr>
                <?php
                if (empty($column)) {
                    $data1['result'] = $this->db->list_fields($tableName);
                    foreach ($data1['result'] as $value) {
                        ?>
                        <td><?php echo $row->$value; ?></td>
                        <?php
                    }
                } else {
                    $column = $_POST['columnName'];

                    foreach ($column as $value) {
                        ?>
                        <td><?php echo $row->$value; ?></td>
                        <?php
                    }
                }
                ?>
            </tr>
            <?php
        }
        ?>

    </tbody>
</table>

