<script>
    $(function () {
        $("#tabs").tabs();
    });
</script>

<!-- <iframe src="http://free.timeanddate.com/clock/i58b6lm5/n73/fn11/fs19/fc360/tt0/ts1" frameborder="0" width="358" height="25"></iframe> -->
<form action="createQuery" id="form1" method="post" target="_blank" enctype="multipart/form-data">
    <div class="form-group">
        <label class="col-sm-4 control-label">Report Title</label>
        <div class="col-sm-5">
            <?php
            error_reporting('0');
            foreach ($_POST['reportTitle'] as $reportTitle) {
                ?>
                <input class="form-control" type="text"  name="reportTitle[]" readonly="" value="<?php echo $reportTitle ?>" placeholder="Give a title for Report"/>
                <?php
            }
            ?>
<!--        <input type="hidden" class="form-control" name="hd_tbl_name" id="hd_id"  />-->
        </div>
    </div>
    <br/><br/><br/>
    <table id="" class="table table-striped table-bordered " width="100%" cellspacing="0">
        <thead>
            <tr>

                <th>Tables Name</th>
                <th>Columns Name</th>


            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    <?php
                    foreach ($_POST['tableName'] as $table) {
                        ?>
                        <input type="text" id="tables" name="tableName[]" value="<?php echo $table; ?>" style="border: none; width: 100%" readonly>
                        <?php
                    }
                    ?>
                </td>
                <td>
                    <?php
                    foreach ($_POST['columnName'] as $columnName) {
                        ?>
                        <input type="text" id="column" name="columnName[]" value="<?php echo $columnName; ?>" style="border: none" readonly>
                    <?php }
                    ?>

                </td>

            </tr>
        </tbody>
    </table> 

    <table id="" class="table table-striped table-bordered " width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Operations</th>

            </tr>
        </thead>

        <tbody>
            <tr>
                <td>

                    <div id="tabs">
                        <ul>

                            <li><a href="#tabs-2">JOIN</a></li>
                            <!-- <li><a href="#tabs-3">Write Sql</a></li> -->

                        </ul>


                        <div class="form-group">
                            <script type="text/javascript">

                                function deleteRow(row)
                                {
                                    var i = row.parentNode.parentNode.rowIndex;
                                    document.getElementById('POITable').deleteRow(i);
                                }
                                function insasRow()
                                {
                                    var x = document.getElementById('POITable');
                                    // deep clone the targeted row
                                    var new_row = x.rows[1].cloneNode(true);
                                    // get the total number of rows
                                    var len = x.rows.length;
                                    // set the innerHTML of the first row 
                                    new_row.cells[0].innerHTML = len;

                                    // grab the input from the first cell and update its ID and value //inp1.value = '';
                                    var inp1 = new_row.cells[1].getElementsByTagName('select')[0];
                                    inp1.id += len;
                                    inp1.value = '';
                                    var inp2 = new_row.cells[2].getElementsByTagName('select')[0];
                                    inp2.id += len;
                                    //inp2.value = '';
                                    var inp3 = new_row.cells[3].getElementsByTagName('select')[0];
                                    inp3.id += len;
                                    //inp3.value = '';
                                    var inp4 = new_row.cells[4].getElementsByTagName('select')[0];
                                    inp4.id += len;
                                    //inp4.value = '';
                                    var inp5 = new_row.cells[5].getElementsByTagName('select')[0];
                                    inp5.id += len;
                                    // inp5.value = '';
                                    var inp6 = new_row.cells[6].getElementsByTagName('select')[0];
                                    inp6.id += len;
                                    // inp5.value = '';
                                    var inp7 = new_row.cells[7].getElementsByTagName('select')[0];
                                    inp7.id += len;
                                    // inp5.value = '';
                                    var inp8 = new_row.cells[8].getElementsByTagName('select')[0];
                                    inp8.id += len;
                                    var inp9 = new_row.cells[9].getElementsByTagName('select')[0];
                                    inp9.id += len;
                                    var inp10 = new_row.cells[10].getElementsByTagName('select')[0];
                                    inp10.id += len;


                                    x.appendChild(new_row);
                                }
                            </script>
                            <div id="POItablediv" class="data-fluid">
                                <table id="POITable" class="table " width="100%" border="1" cellspacing="0">
                                    <thead>
                                    <th>SL</th>
                                    <th>Join Type</th>
                                    <th>Table Name</th>
                                    <th>Operator</th>
                                    <th>Table Name</th>
                                    <th>Operator</th>
                                    <th>Column Name</th>
                                    <th>Operator</th>
                                    <th>Table Name</th>
                                    <th>Operator</th>
                                    <th>Column Name</th>
                                    </thead>

                                    <tbody>
                                        <?php for ($i = 1; $i <= 1; $i++): ?>
                                            <tr>
                                                <td>
                                                    <?php echo $i; ?>
                                                </td>
                                                <td>
                                                    <select class="form-control " name="joinOption[]"
                                                            id="joinOption" data-tags="true" data-placeholder="Operator"
                                                            data-allow-clear="true" style="width: 80px">
                                                        <option value=" JOIN ">Join</option>
                                                        <option value=" LEFT JOIN ">Left Join</option>
                                                        <option value=" RIGHT JOIN ">Right Join</option>
                                                        <option value=" INNER JOIN ">Inner Join</option>
                                                        <option value=" FULL OUTER JOIN ">Full Join</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control joinTable" name="joinTable[]" id="joinTable1" data-placeholder="Table"  data-allow-clear="true">
                                                        <option value="">Table</option>
                                                        <?php
                                                        foreach ($result as $row):
                                                            ?>
                                                            <option value="<?php echo $row ?>"><?php echo $row ?></option>
                                                            <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="select form-control " name="joinOption[]"
                                                            id="joinOption" data-tags="true" data-placeholder="Operator"
                                                            data-allow-clear="true" style="width: 80px" >
                                                        <option value=" ON ">ON</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control joinTable" name="joinTable[]" id="joinTable1" data-placeholder="Table"  data-allow-clear="true">
                                                        <option value="">Table</option>
                                                        <?php
                                                        foreach ($result as $row):
                                                            ?>
                                                            <option value="<?php echo $row ?>"><?php echo $row ?></option>
                                                            <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="select form-control " name="joinOption[]"
                                                            id="joinOption" data-tags="true" data-placeholder="Operator"
                                                            data-allow-clear="true" >
                                                        <option value=".">.</option>
                                                    </select>
                                                </td>

                                                <td>  
                                                    <select class="form-control" name="joinColumns[]"
                                                            id="joinColumns1" data-tags="true" data-placeholder="Column"
                                                            data-allow-clear="true" style="width: 100px" >
                                                        <option value="">Columns</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="select form-control " name="joinOption[]"
                                                            id="joinOption" data-tags="true" data-placeholder="Operator"
                                                            data-allow-clear="true" >
                                                        <option value="=">=</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control joinTables" name="joinTables[]" id="joinTables1" data-tags="true" data-placeholder="Table"  data-allow-clear="true">
                                                        <option value="">Table</option>
                                                        <?php
                                                        foreach ($result as $row):
                                                            ?>
                                                            <option value="<?php echo $row ?>"><?php echo $row ?></option>
                                                            <?php
                                                        endforeach;
                                                        ?>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="select form-control " name="joinOption[]"
                                                            id="joinOption" data-tags="true" data-placeholder="Operator"
                                                            data-allow-clear="true" >
                                                        <option value=".">.</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <select class="form-control"  name="joinColumnName[]" 
                                                            id="joinColumnName1" data-tags="true" data-placeholder="Columns" style="width: 100px" data-allow-clear="true" >
                                                        <option value="">Columns</option>
                                                    </select>
                                                </td>  
                                            </tr>
                                        <?php endfor; ?>
                                    </tbody>
                                </table> 
                                <input type="button" class="btn btn-primary" 
                                       id="addmorePOIbutton" value="Add More" onclick="insasRow()"/>
                            </div>
                            <br/><br/><br/>
                            <table class="table table-striped table-bordered" style="width: 60%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Join Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <input type="checkbox" id="a[1][]" name="joinType[]" value="JOIN" >
                                            <label>Join</label>
                                            <input type="checkbox" id="a[1][]" name="joinType[]" value="">
                                            <label> Left Join</label>
                                            <input type="checkbox" id="a[1][]" name="joinType[]" value="">
                                            <label>Right Join</label>
                                            <input type="checkbox" id="a[1][]" name="joinType[]" value="">
                                            <label>Inner Join</label>
                                            <input type="checkbox" id="a[1][]" name="joinType[]" value="">
                                            <label>Full Outer Join</label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                            <input type="button" id="genSql" class="btn btn-success" name="b" value="Generate SQL  For Selection" />
                            <br/><br/><br/>
                            <div class="col-sm-9">
                                <textarea rows="5" cols="80"  name="genQuery[]"></textarea>
                            </div>
                            <br/><br/><br/>
                            <br/><br/><br/>
                            <br/><br/><br/>
                        </div>

                    </div>
                    <br/><br/><br/>
                    <div class="checkbox checkbox-inline checkbox-primary">
                        <input class="styled" id="html" type="checkbox" value="" name="">
                        <label for="ACTIVE_STATUS">HTML/CSV View</label>
                        <br/><br/>
                        <input class="styled" id="pdf" type="checkbox" value="" name="">
                        <label for="ACTIVE_STATUS">PDF View</label>

                    </div>
                    <br/><br/><br/>
                    <input type="submit" class="btn btn-success" id="htmlbutton" disabled="disabled" style="margin-left: 100px;"
                           onclick="submitForm('createHtml')" name="submit" Value="Submit"/>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="table table-striped table-bordered" style="width: 60%" cellspacing="0">
        <thead>
            <tr>
                <th>Create PDF</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Page Size(pdf)</label>
                        <div class="col-sm-6">
                            <select class="select2 form-control" name="pageSize" id="" data-tags="true"
                                    data-placeholder="Select Page Size" style="width: 200px"  data-allow-clear="true">
                                <option value="">Select Page Size</option>
                                <option value="Default">Default</option>
                                <option value="B5">B5</option>
                                <option value="B5-L">B5 Landscape</option>
                                <option value="A6">A6</option>
                                <option value="A6-L">A6 Landscape</option>
                                <option value="A5">A5</option>
                                <option value="A5-L">A5 Landscape</option>
                                <option value="A4">A4</option>
                                <option value="A4-L">A4 Landscape</option>
                                <option value="A3">A3</option>
                                <option value="A3-L">A3 Landscape</option>
                                <option value="A2">A2</option>
                                <option value="A2-L">A2 Landscape</option>
                                <option value="A1">A1</option>
                                <option value="A1-L">A1 Landscape</option>
                                <option value="US Letter">US Letter</option>
                                <option value="US Legal">US Legal</option>
                                <option value="US Ledger">US Ledger</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Border Type(pdf)</label>
                        <div class="col-sm-6">
                            <select class="select2 form-control" name="borderType" style="width: 200px" id="" data-tags="true"
                                    data-placeholder="Select Table"  data-allow-clear="true">
                                <option value="">Select Border Type</option>
                                <option value="solid">Default</option>
                                <option value="dotted">Dotted</option>
                                <option value="double">Double</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Border Color(pdf)</label>
                        <div class="col-sm-6">
                            <select class="select2 form-control" name="borderColour" style="width: 200px" id="" data-tags="true"
                                    data-placeholder="Select Table"  data-allow-clear="true">
                                <option value="">Select Border Color</option>
                                <option value="white">Default</option>
                                <option value="red">RED</option>
                                <option value="orange">Orange</option>
                                <option value="yellow">Yellow</option>
                                <option value="blue">Blue</option>
                                <option value="cyan">Cyan</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Top Margin(pdf)</label>
                        <div class="col-sm-6">
                            <div class="input-group" style="width: 120px">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                            data-field="topMargin">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="topMargin" style="width: 120px" class="form-control input-number" value="1cm" min="0"
                                       max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus"
                                            data-field="topMargin">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Bottom Margin(pdf)</label>
                        <div class="col-sm-6">
                            <div class="input-group" style="width: 120px">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                            data-field="bottomMargin">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="bottomMargin" style="width: 120px" class="form-control input-number" value="1cm" min="0"
                                       max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus"
                                            data-field="bottomMargin">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Right Margin(pdf)</label>
                        <div class="col-sm-6">
                            <div class="input-group" style="width: 120px">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                            data-field="rightMargin">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="rightMargin" class="form-control input-number" style="width: 120px" value="1cm" min="0"
                                       max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus"
                                            data-field="rightMargin">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Left Margin(pdf)</label>
                        <div class="col-sm-6">
                            <div class="input-group" style="width: 200px">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-danger btn-number" data-type="minus"
                                            data-field="leftMargin">
                                        <span class="glyphicon glyphicon-minus"></span>
                                    </button>
                                </span>
                                <input type="text" name="leftMargin" class="form-control input-number" style="width: 123px" value="1cm" min="0"
                                       max="100">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-success btn-number" data-type="plus"
                                            data-field="leftMargin">
                                        <span class="glyphicon glyphicon-plus"></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Font(pdf)</label>
                        <div class="col-sm-6">
                            <select class="select2 form-control" style="width: 200px" name="fontType" id="" data-tags="true"
                                    data-placeholder="Select Font" data-allow-clear="true">
                                <option selected="" label="" value=""></option>
                                <option label="aealarabiya" value="aealarabiya">aealarabiya</option>
                                <option label="aefurat" value="aefurat">aefurat</option>
                                <option label="cid0cs" value="cid0cs">cid0cs</option>
                                <option label="cid0ct" value="cid0ct">cid0ct</option>
                                <option label="cid0jp" value="cid0jp">cid0jp</option>
                                <option label="cid0kr" value="cid0kr">cid0kr</option>
                                <option label="comic" value="comic">comic</option>
                                <option label="courier" value="courier">courier</option>
                                <option label="courierb" value="courierb">courierb</option>
                                <option label="courierbi" value="courierbi">courierbi</option>
                                <option label="courieri" value="courieri">courieri</option>
                                <option label="dejavusans" value="dejavusans">dejavusans</option>
                                <option label="dejavusansb" value="dejavusansb">dejavusansb</option>
                                <option label="dejavusansbi" value="dejavusansbi">dejavusansbi</option>
                                <option label="dejavusanscondensed" value="dejavusanscondensed">dejavusanscondensed</option>
                                <option label="dejavusanscondensedb" value="dejavusanscondensedb">dejavusanscondensedb</option>
                                <option label="dejavusanscondensedbi" value="dejavusanscondensedbi">dejavusanscondensedbi
                                </option>
                                <option label="dejavusanscondensedi" value="dejavusanscondensedi">dejavusanscondensedi</option>
                                <option label="dejavusansextralight" value="dejavusansextralight">dejavusansextralight</option>
                                <option label="dejavusansi" value="dejavusansi">dejavusansi</option>
                                <option label="dejavusansmono" value="dejavusansmono">dejavusansmono</option>
                                <option label="dejavusansmonob" value="dejavusansmonob">dejavusansmonob</option>
                                <option label="dejavusansmonobi" value="dejavusansmonobi">dejavusansmonobi</option>
                                <option label="dejavusansmonoi" value="dejavusansmonoi">dejavusansmonoi</option>
                                <option label="dejavuserif" value="dejavuserif">dejavuserif</option>
                                <option label="dejavuserifb" value="dejavuserifb">dejavuserifb</option>
                                <option label="dejavuserifbi" value="dejavuserifbi">dejavuserifbi</option>
                                <option label="dejavuserifcondensed" value="dejavuserifcondensed">dejavuserifcondensed</option>
                                <option label="dejavuserifcondensedb" value="dejavuserifcondensedb">dejavuserifcondensedb
                                </option>
                                <option label="dejavuserifcondensedbi" value="dejavuserifcondensedbi">dejavuserifcondensedbi
                                </option>
                                <option label="dejavuserifcondensedi" value="dejavuserifcondensedi">dejavuserifcondensedi
                                </option>
                                <option label="dejavuserifi" value="dejavuserifi">dejavuserifi</option>
                                <option label="freemono" value="freemono">freemono</option>
                                <option label="freemonob" value="freemonob">freemonob</option>
                                <option label="freemonobi" value="freemonobi">freemonobi</option>
                                <option label="freemonoi" value="freemonoi">freemonoi</option>
                                <option label="freesans" value="freesans">freesans</option>
                                <option label="freesansb" value="freesansb">freesansb</option>
                                <option label="freesansbi" value="freesansbi">freesansbi</option>
                                <option label="freesansi" value="freesansi">freesansi</option>
                                <option label="freeserif" value="freeserif">freeserif</option>
                                <option label="freeserifb" value="freeserifb">freeserifb</option>
                                <option label="freeserifbi" value="freeserifbi">freeserifbi</option>
                                <option label="freeserifi" value="freeserifi">freeserifi</option>
                                <option label="helvetica" value="helvetica">helvetica</option>
                                <option label="helveticab" value="helveticab">helveticab</option>
                                <option label="helveticabi" value="helveticabi">helveticabi</option>
                                <option label="helveticai" value="helveticai">helveticai</option>
                                <option label="hysmyeongjostdmedium" value="hysmyeongjostdmedium">hysmyeongjostdmedium</option>
                                <option label="kozgopromedium" value="kozgopromedium">kozgopromedium</option>
                                <option label="kozminproregular" value="kozminproregular">kozminproregular</option>
                                <option label="msungstdlight" value="msungstdlight">msungstdlight</option>
                                <option label="pdfacourier" value="pdfacourier">pdfacourier</option>
                                <option label="pdfacourierb" value="pdfacourierb">pdfacourierb</option>
                                <option label="pdfacourierbi" value="pdfacourierbi">pdfacourierbi</option>
                                <option label="pdfacourieri" value="pdfacourieri">pdfacourieri</option>
                                <option label="pdfahelvetica" value="pdfahelvetica">pdfahelvetica</option>
                                <option label="pdfahelveticab" value="pdfahelveticab">pdfahelveticab</option>
                                <option label="pdfahelveticabi" value="pdfahelveticabi">pdfahelveticabi</option>
                                <option label="pdfahelveticai" value="pdfahelveticai">pdfahelveticai</option>
                                <option label="pdfasymbol" value="pdfasymbol">pdfasymbol</option>
                                <option label="pdfatimes" value="pdfatimes">pdfatimes</option>
                                <option label="pdfatimesb" value="pdfatimesb">pdfatimesb</option>
                                <option label="pdfatimesbi" value="pdfatimesbi">pdfatimesbi</option>
                                <option label="pdfatimesi" value="pdfatimesi">pdfatimesi</option>
                                <option label="pdfazapfdingbats" value="pdfazapfdingbats">pdfazapfdingbats</option>
                                <option label="stsongstdlight" value="stsongstdlight">stsongstdlight</option>
                                <option label="symbol" value="symbol">symbol</option>
                                <option label="times" value="times">times</option>
                                <option label="timesb" value="timesb">timesb</option>
                                <option label="timesbi" value="timesbi">timesbi</option>
                                <option label="timesi" value="timesi">timesi</option>
                                <option label="trebuchet" value="trebuchet">trebuchet</option>
                                <option label="uni2cid_ac15" value="uni2cid_ac15">uni2cid_ac15</option>
                                <option label="uni2cid_ag15" value="uni2cid_ag15">uni2cid_ag15</option>
                                <option label="uni2cid_aj16" value="uni2cid_aj16">uni2cid_aj16</option>
                                <option label="uni2cid_ak12" value="uni2cid_ak12">uni2cid_ak12</option>
                                <option label="verdana" value="verdana">verdana</option>
                                <option label="zapfdingbats" value="zapfdingbats">zapfdingbats</option>
                            </select>
                        </div>
                    </div>
                    <br/><br/>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Font Size(pdf)</label>
                        <div class="col-sm-6">
                            <input class="form-control" style="width: 200px" type="text" name="fontSize" value="" placeholder="Default : 11px"/>
                        </div>
                    </div>
                    <br/><br/>
                    <input type="submit" class="btn btn-primary" id="pdfbutton" disabled style="margin-left: 100px;"
                           onclick="submitForm" name="submit" Value="Create PDF" target="_blank"/>
                </td>
        </tbody>
    </table>
</form>
<?php
$this->load->view('multiReport/listScript');
?>
<script>
    $(document).ready(function () {
        $("#tableName").on('change', function () {
            // $('#likeOption').select2('val', '');
            var name = this.value;
            // alert(name);
            //$("textarea#fromTable").val(name);
            $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>report/ReportSetup/checkColumn",
                data: {Tablename: name},
                success: function (data) {
                    $("#groupOption").html(data);
                }
            });
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#POITable").on('change','.joinTable', function () {
            //alert('assa');
            $('#joinColumns').select('val', '');
            var name = this.value;
            var name = this.value;
            var srtingId = this.id;
            var thenum = srtingId.replace ( /[^\d.]/g, '' ); 
            //  alert(name);
            // $("textarea#fromTable").val(name);
            $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>report/ReportSetup/checkColumn",
                data: {Tablename: name},
                success: function (data) {

                    $("#joinColumns"+thenum).html(data);
                }
            });
            // $("textarea").val(name);
        });
    });
</script>
<script>
    $(document).ready(function () {
        /* var inputId = $('#joinTables').prop('tagName');
                alert(inputId); */
        $("#POITable").on('change','.joinTables', function () {
            
            //alert('assa');
            $('#joinColumnName').select('val', '');
            var name = this.value;
            var srtingId = this.id;
            var thenum = srtingId.replace ( /[^\d.]/g, '' ); 
            //alert(thenum);
            //  alert(name);
            // $("textarea#fromTable").val(name);
            $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>report/ReportSetup/checkColumn",
                data: {Tablename: name},
                success: function (data) {

                    $("#joinColumnName"+thenum).html(data);
                }
            });
            // $("textarea").val(name);
                
       
        });
        
    });
</script>
<script>
    $("#genSql").click(function () {
        var values = $('#POITable').val();
        $("select").each(function (i) {
            values += (i > 0 ? "" : "") + this.value;
        });
        $("textarea").val(values);
    });
</script>
<script>
    //$("input:checkbox").click(function(){
    //    var group = "input:checkbox[id='"+$(this).attr("id")+"']";
    //    $(group).attr("checked",false);
    //    $(this).attr("checked",true);
    //});
</script>



