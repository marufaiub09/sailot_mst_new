<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">
<form class="form-horizontal frmContent" id="frmOrderSec" method="post">
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-base">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-md-11 col-sm-10 col-xs-8">
                            <h3 class="panel-title"><?php echo $this->lang->line('title'); ?></h3>
                        </div>
                    </div>
                    <span class="pull-right clickable">
                        <i class="glyphicon glyphicon-chevron-up"></i>
                    </span>
                </div>
                <div class="widget widget-heading-simple widget-body-gray col-md-12">
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="col-md-6">
                                <input name="navi" id="navi" type="radio" checked="che" value="Navy">NAVY
                            </div>
                            <div class="col-md-6">
                                <input name="navi" id="navi" type="radio" value="ISMODC">MODC
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="margin-left:40px;">

                        <table class="table  table-bordered dataTable" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Order Sequence</th>
                                </tr>
                            </thead>
                            <tbody>
                            <td>
                                <div class="checkbox checkbox-inline checkbox-primary"
                                     style="overflow:scroll;height:130px;width:222px">
                                    <input class="styled sequData" id="entryDate" type="checkbox"
                                           value="sailor.ENTRYDATE"
                                           name="sequData[]">
                                    <label for="ACTIVE_STATUS">Entry Date</label>
                                    <br/><br/>
                                    <input class="styled sequData" id="equialRank" type="checkbox"
                                           value="sailor.EQUIVALANTRANKID"
                                           name="sequData[]">
                                    <label for="ACTIVE_STATUS">Equivalent Rank</label>
                                    <br/><br/>
                                    <input class="styled sequData" id="examLevel" type="checkbox" value="sailor.LASTEDUCATIONID"
                                           name="sequData[]">
                                    <label for="ACTIVE_STATUS">Exam Level</label>
                                    <br/><br/>
                                    <input class="styled sequData" id="batch" type="checkbox"
                                           value="sailor.BATCHNUMBER"
                                           name="sequData[]">
                                    <label for="ACTIVE_STATUS">Batch</label>
                                    <br/><br/>
                                    <input class="styled sequData" id="dob" type="checkbox" value="sailor.BIRTHDATE"
                                           name="sequData[]">
                                    <label for="ACTIVE_STATUS">Date of Birth</label>
                                    <br/><br/>
                                    <input class="styled sequData" id="fullName" type="checkbox"
                                           value="sailor.FULLNAME"
                                           name="sequData[]">
                                    <label for="ACTIVE_STATUS">Full Name</label>
                                    <br/><br/>
                                    <input class="styled sequData" id="examResult" type="checkbox"
                                           value="a.ExamGradeID"
                                           name="sequData[]">
                                    <label for="ACTIVE_STATUS">Exam Result</label>
                                    <br/><br/>
                                </div>
                            </td>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="widget widget-heading-simple widget-body-gray col-md-12">
                    <div class="col-md-8" style="margin-top:-142px">
                        <div class="form-group">
                            <label class="col-sm-3">Batch Number</label>

                            <div class="col-sm-4">
                                <?php echo form_dropdown('batch', $batch, '', 'id="batchid" required="required" class="form-control select2" data-placeholder="Select Branch Number" aria-hidden="true" data-allow-clear="true"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="widget widget-heading-simple widget-body-gray col-md-8" style="margin-right:120px;">
                    <div class="col-md-8" style="margin-left: 612px;margin-top:-194px;">
                        <div class="col-md-10" >
                            <div style="margin-bottom:1px">
                                <label class="col-sm-4 control-label"></label>

                                <div class="col-sm-4"> &nbsp; Year</div>
                                <div class="col-sm-4"> Serial</div>
                            </div>
                            <div>
                                <label class="col-sm-4 control-label">Starting O No</label>

                                <div class="col-sm-4">
                                    <?php echo form_input(array('name' => 'alocationYear', 'id' => 'setyear', 'min' => '1970', 'max' => '2099', 'type' => 'number', "class" => "form-control required", 'required' => 'required', 'value' => Date('Y'))); ?>
                                </div>
                                <div class="col-sm-4">

                                    <?php echo form_input(array('name' => 'startNo', "id" => "setmonth", "class" => "form-control required", "value" => "00", "maxlength" => "4")); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class=" col-md-6">

                            <!-- <input type="button" class="btn btn-primary btn-sm pull-right" id="Val" value="Validate"> -->
                            </div>
                            <div class=" col-md-6">
                                <input type="button" class="btn btn-primary btn-sm pull-right" id="gen" value="Generate">
                            </div>
                            <br/><br/>
                        </div>

                        <div class="col-md-10">
                            <div class=" col-md-6">
                                <input type="button" id="refresh" class="btn btn-success btn-sm pull-right"
                                       value="Refresh" >
                            </div>
                            <div class=" col-md-6">
<!--                               <input type="button" class="btn btn-danger btn-sm pull-right formSubmit" data-action="process/FinalAllocationOfOfficialNumber/genONo" data-su-action="process/FinalAllocationOfOfficialNumber/index" data-type="list" value="Final O No">-->
                                <input type="button" id="o_no" class="btn btn-danger btn-sm pull-right" value="Final O No">
                            </div>
                            <br/><br/>
                        </div>

                    </div>
                </div>

                <div class="panel-body">
                    <table id="" class="table table-striped table-bordered selected_sailor" width="100%"
                           cellspacing="0">
                        <thead>
                            <tr>
                                <th> O No</th>
                                <th> Sailor ID </th>
                                <th> Local Number</th>
                                <th> Name</th>
                                <th> Rank</th>
                                <th> Birth Date</th>
                                <th> Entry Date</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
<script>
    $(document).on("click", "#refresh", function (e) {

        $(".selected_sailor tbody").empty();
        //var isValid = 0;

        var navi = new Array();
        var data = {'navi[]': []};
        $("#navi:checked").each(function () {
            navi.push($(this).val());

        });

        // var modc = $("#modc").val();
        //alert(modc);
        var batchid = $("#batchid").val();
        var sequData = new Array();
        var data = {'sequData[]': []};
        $(".sequData:checked").each(function () {
            sequData.push($(this).val());

        });
        //var seqData = $("#sequData").val();
        if (batchid == "") {
            alert("Select Batch Number");
            /* isValid = 1;
             return false;*/
        }

        $.ajax({
            type: "POST",
            url: '<?php echo site_url('process/FinalAllocationOfOfficialNumber/getSailorData') ?>',
            data: {navi: navi, sequData: sequData, batchid: batchid},
            dataType: 'html',
            success: function (data) {
                $(".selected_sailor tbody").append(data);

            }

        });
    })


    $(document).on("click", "#o_no", function () {
        if (confirm('Are you want to Generate Official Number?')) {
          var data = $("#frmOrderSec").serialize();
          var officialNo = $('.copy').val();          
           $.ajax({
                type: "POST",
                data: data,officialNo:officialNo,
                url: "<?php echo site_url('process/FinalAllocationOfOfficialNumber/genONo'); ?>",
                success: function (data) {
                    $(".msg").html(data);
                }
            });
        } else {
            return false;
        }
    });


</script>

<!-- Auto Increment Official Number for Sailor -->
<script type="text/javascript">
    $(document).on("click", "#gen", function (e) {
        $(".copy").empty();
        var year = $('#setyear').val();
        var Serial = $('#setmonth').val();
        var result = $('#setyear').val() + $('#setmonth').val();
        var rowCount = $('.selected_sailor tbody tr').length;
        var count = 0;
        var current = result;
        $(".copy").each(function () {
            $(this).data(current);
            $("#copy_" + count).append(current);
            $("#data_" + count).html("<input Name='officialNumber[]' type ='hidden'  value='"+ current +"'>");
            count++;
            current++;
        });
        
        
    });
</script>
