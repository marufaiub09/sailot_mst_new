

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Data Table</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <table class="datatableByClass table table-striped table-bordered dt-responsive nowrap" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Action</th>
                    </tr>

                    </thead>

                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="View">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Edit">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="#"  data-tooltip="tooltip" data-placement="top" title="Remove">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Garrett Winters</td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>63</td>
                        <td>2011/07/25</td>
                        <td>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="View">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Edit">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="#"  data-tooltip="tooltip" data-placement="top" title="Remove">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Ashton Cox</td>
                        <td>Junior Technical Author</td>
                        <td>San Francisco</td>
                        <td>66</td>
                        <td>2009/01/12</td>
                        <td>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="View">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Edit">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="#"  data-tooltip="tooltip" data-placement="top" title="Remove">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Data Table</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">
                <table class="datatableByClass table table-striped table-bordered dt-responsive nowrap" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Action</th>
                    </tr>

                    </thead>

                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office</th>
                        <th>Age</th>
                        <th>Start date</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <tr>
                        <td>Tiger Nixon</td>
                        <td>System Architect</td>
                        <td>Edinburgh</td>
                        <td>61</td>
                        <td>2011/04/25</td>
                        <td>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="View">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Edit">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="#"  data-tooltip="tooltip" data-placement="top" title="Remove">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Garrett Winters</td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>63</td>
                        <td>2011/07/25</td>
                        <td>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="View">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Edit">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="#"  data-tooltip="tooltip" data-placement="top" title="Remove">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Ashton Cox</td>
                        <td>Junior Technical Author</td>
                        <td>San Francisco</td>
                        <td>66</td>
                        <td>2009/01/12</td>
                        <td>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="View">
                                <span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Edit">
                                <span class="glyphicon glyphicon-edit"></span>
                            </a>
                            <a href="#"  data-tooltip="tooltip" data-placement="top" title="Remove">
                                <span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>