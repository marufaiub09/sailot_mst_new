<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade" id="dualListboxModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">

                <form id="demoform" action="#" method="post">
                    <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                        <option value="option1">Option 1</option>
                        <option value="option2">Option 2</option>
                        <option value="option3" selected="selected">Option 3</option>
                        <option value="option4">Option 4</option>
                        <option value="option5">Option 5</option>
                        <option value="option6" selected="selected">Option 6</option>
                        <option value="option7">Option 7</option>
                        <option value="option8">Option 8</option>
                        <option value="option9">Option 9</option>
                        <option value="option0">Option 10</option>
                    </select>
                    <br>
                    <button type="submit" class="btn btn-primary btn-block">Submit data</button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Dual Select</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-tooltip="tooltip" data-target="#dualListboxModal" data-placement="top" title="Create Module">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <form id="demoform" action="#" method="post">
                    <select multiple="multiple" size="10" name="duallistbox_demo1[]">
                        <option value="option1">Option 1</option>
                        <option value="option2">Option 2</option>
                        <option value="option3" selected="selected">Option 3</option>
                        <option value="option4">Option 4</option>
                        <option value="option5">Option 5</option>
                        <option value="option6" selected="selected">Option 6</option>
                        <option value="option7">Option 7</option>
                        <option value="option8">Option 8</option>
                        <option value="option9">Option 9</option>
                        <option value="option0">Option 10</option>
                    </select>
                    <br>
                    <button type="submit" class="btn btn-primary btn-block">Submit data</button>
                </form>

            </div>
        </div>

    </div>

</div>