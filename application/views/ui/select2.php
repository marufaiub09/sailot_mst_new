
<?php

echo '<pre>'.print_r($_POST,1).'</pre>';
?>
<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Basic Form with Validation</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-tooltip="tooltip" data-target="#basicFormModal" data-placement="top" title="Create Module">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <!-- form start -->
                <form id="bootstrapSelectForm" class="form-horizontal" method="post">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Select2 Group</label>

                        <div class="col-sm-6">
                            <select name="colors[]" class="form-control select2-select"
                                    multiple>
                                <option value="black">Black</option>
                                <option value="blue">Blue</option>
                                <option value="green">Green</option>
                                <option value="orange">Orange</option>
                                <option value="red">Red</option>
                                <option value="yellow">Yellow</option>
                                <option value="white">White</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form><!-- form end -->

            </div>
        </div>

    </div>

</div>

<script>
    $(document).ready(function() {
        $('#bootstrapSelectForm')
            .find('[name="colors[]"]')
            .select2()
            // Revalidate the color when it is changed
            .change(function(e) {
                $('#select2Form').formValidation('revalidateField', 'colors');
            })
            .end()
            .formValidation({
                framework: 'bootstrap',
                excluded: ':disabled',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                },

                fields: {
                    colors: {
                        row: '.col-xs-4',
                        validators: {
                            notEmpty: {
                                message: 'The first name is required'
                            }
                        }
                    }
                }
            });
    });
</script>