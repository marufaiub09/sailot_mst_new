<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">DataTable Multi Table</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">

                <table id="dataTableJoin" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Actor ID</th>
                        <th>First name</th>
                        <th>Film ID</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Actor ID</th>
                        <th>First name</th>
                        <th>Film ID</th>
                        <th>Title</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>

    </div>

</div>


<script type="text/javascript">

    $(document).ready(function() {
        $('#dataTableJoin').DataTable( {
            //"pageLength": 50,
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "ajaxDataTableJoin",
                "type": "POST"
            },
            responsive: true,
            aoColumnDefs: [
                {
                    bSortable: false,
                    aTargets: [ -1 ]
                }
            ]
        } );
    } );

</script>