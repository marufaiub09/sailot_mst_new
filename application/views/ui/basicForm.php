

<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Basic Form with Validation</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-tooltip="tooltip" data-target="#basicFormModal" data-placement="top" title="Create Module">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <!-- form start -->
                <form id="formHorizontal" class="form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">First Name</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="first_name" placeholder="First Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Last Name</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>

                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Text Area</label>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="textarea" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Checkbox</label>

                        <div class="col-sm-6">
                            <div class="checkbox checkbox-primary">
                                <input class="styled" type="checkbox" name="checkbox" />
                                <label>
                                    Default
                                </label>
                            </div>
                            <div class="checkbox checkbox-primary">
                                <input class="styled" type="checkbox" name="checkbox" />
                                <label>
                                    Primary
                                </label>
                            </div>
                            <div class="checkbox checkbox-primary">
                                <input class="styled" type="checkbox" name="checkbox" />
                                <label>
                                    Success
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Inline Checkbox</label>

                        <div class="col-sm-6">
                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" name="inline_checkbox" value="option1" />
                                <label for="inlineCheckbox1"> Inline One </label>
                            </div>

                            <div class="checkbox checkbox-inline">
                                <input type="checkbox" class="styled" name="inline_checkbox" value="option1" />
                                <label for="inlineCheckbox2"> Inline Two </label>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Radio Vertical</label>
                        <div class="col-sm-6">
                            <div class="radio">
                                <input type="radio" name="radio" value="option1" />
                                <label for="radio3">
                                    One
                                </label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="radio" value="option2">
                                <label for="radio4">
                                    Two
                                </label>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Radio Inline</label>
                        <div class="col-sm-6">
                            <div class="radio radio-info radio-inline">
                                <input type="radio" value="option1" name="radioInline" />
                                <label for="inlineRadio1"> Inline One </label>
                            </div>

                            <div class="radio radio-inline">
                                <input type="radio" value="option2" name="radioInline">
                                <label for="inlineRadio2"> Inline Two </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bootstrap Select</label>

                        <div class="col-sm-6">
                            <div class="selectContainer">
                                <select name="bs_select" class="form-control" />
                                <option value="">Select an Option</option>
                                <option value="mustard">Mustard</option>
                                <option value="ketchup">Ketchup</option>
                                <option value="relish">Relish</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Bootstrap Select Multiple</label>
                        <div class="col-sm-6">
                            <div class="selectContainer">
                                <select name="colors" class="form-control" multiple title="Choose 2-4 colors">
                                    <option value="black">Black</option>
                                    <option value="blue">Blue</option>
                                    <option value="green">Green</option>
                                    <option value="orange">Orange</option>
                                    <option value="red">Red</option>
                                    <option value="yellow">Yellow</option>
                                    <option value="white">White</option>
                                </select>
                            </div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Select2 Group</label>

                        <div class="col-sm-6">
                            <select name="colors2" class="form-control select2-select"
                                    multiple data-placeholder="Choose 2-4 colors">
                                <option value="black">Black</option>
                                <option value="blue">Blue</option>
                                <option value="green">Green</option>
                                <option value="orange">Orange</option>
                                <option value="red">Red</option>
                                <option value="yellow">Yellow</option>
                                <option value="white">White</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">DatePicker</label>

                        <div class="col-sm-6 date">
                            <div class="selectContainer">
                                <div class="input-group input-append date" id="datePicker">
                                    <input type="text" class="form-control" name="date" />
                                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Date Range</label>

                        <div class="col-sm-6 date">
                            <div class="input-group input-daterange">
                                <input type="text" class="form-control" value="2012-04-05">
                                <span class="input-group-addon">to</span>
                                <input type="text" class="form-control" value="2012-04-19">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Date Time</label>
                        <div class="col-sm-6 date">
                            <div class="input-group input-append date datetimepicker">
                                <input type="text" class="form-control" name="time" />
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form><!-- form end -->

            </div>
        </div>

    </div>

</div>
