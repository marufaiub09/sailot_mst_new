

<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Panel Title</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">



                <form id="surveyForm" action="" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Question</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="question" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Options</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="option[]" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Radio Inline</label>
                        <div class="col-sm-6">
                            <div class="radio radio-info radio-inline">
                                <input type="radio" value="yes" name="radioInline" />
                                <label for="inlineRadio1"> Yes </label>
                            </div>

                            <div class="radio radio-inline">
                                <input type="radio" value="no" name="radioInline">
                                <label for="inlineRadio2"> No </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group conditional-input">
                        <label class="col-xs-3 control-label">Conditional Input</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="conditional" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-5 col-xs-offset-3">
                            <button type="submit" class="btn btn-default">Validate</button>
                        </div>
                    </div>

                </form>

                <script>
                    $(document).ready(function() {

                        $('.conditional-input').hide();
                        $("input[name$='radioInline']").click(function() {

                            var condition = $(this).val();

                            if(condition == 'yes'){
                                $('.conditional-input').show();

                            }else{
                                $('.conditional-input').hide();
                            }
                            //alert(test);

                        });

                        $('#surveyForm')
                            .formValidation({
                                framework: 'bootstrap',
                                icon: {
                                    valid: 'glyphicon glyphicon-ok',
                                    invalid: 'glyphicon glyphicon-remove',
                                    validating: 'glyphicon glyphicon-refresh'
                                },
                                fields: {
                                    question: {
                                        validators: {
                                            notEmpty: {
                                                message: 'The question required and cannot be empty'
                                            }
                                        }
                                    },
                                    'option[]': {
                                        validators: {
                                            notEmpty: {
                                                message: 'The option required and cannot be empty'
                                            },
                                            stringLength: {
                                                max: 100,
                                                message: 'The option must be less than 100 characters long'
                                            }
                                        }
                                    },
                                    radioInline: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Inline radio button is required and cannot be empty'
                                            }
                                        }
                                    },
                                    conditional: {
                                        validators: {
                                            notEmpty: {
                                                message: 'Inline radio button is required and cannot be empty'
                                            }
                                        }
                                    }
                                }
                            })
                    });
                </script>



            </div>
        </div>

    </div>

</div>