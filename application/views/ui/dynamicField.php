

<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">

                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Basic CRUD</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="modalLink btn btn-primary btn-xs"  title="Create Dynamic" data-tooltip="tooltip" data-placement="top" href="<?php echo site_url("UI/dynamicFormCreate"); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">


            </div>
        </div>

    </div>

</div>
$(document).ready(function(){

$('.commentBox').hide();

$("#back_btn").click(function(){
$("div.commentBox").slideDown(400);
$("i.reqSymbol").show();
$('#reqStat').val("");
});

$(".go_btn1").click(function(){
$("div.commentBox").slideDown(400);
$("i.reqSymbol").show();
$('#reqStat').val("");
});

$(".go_btn").click(function(){
$("div.commentBox").slideUp(400);
$('#reqStat').val("");
});

$('.dgdpMainForm').formValidation({

fields: {
COMMENTS: {
validators: {
notEmpty: {
message: 'Comment field is required and cannot be empty'
}
}
}
}

});

});