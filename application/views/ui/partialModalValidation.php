<?php
    $this->util->modalPartial('ui/partialModalValidationForm');
?>

<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">

                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Partial Modal Validation</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a id="modalPartial" type="button" class="btn btn-primary btn-xs" data-title="Partial Modal Form Example" data-toggle="modal" data-tooltip="tooltip" data-target="#partialModal" data-placement="top" >
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">



            </div>
        </div>

    </div>

</div>

<?php

$code = "$('#partialModalForm').formValidation();";
$this->util->addJsCode($code);

