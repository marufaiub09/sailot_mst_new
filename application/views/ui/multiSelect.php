<div class="row">
    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Multi Select</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-xs-5">
                        <select name="from[]" id="multiselect" class="form-control" size="8" multiple="multiple">
                            <option value="1">Item 1</option>
                            <option value="3">Item 3</option>
                            <option value="2">Item 2</option>
                        </select>
                    </div>

                    <div class="col-xs-2">
                        <button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
                        <button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
                        <button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
                        <button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
                    </div>

                    <div class="col-xs-5">
                        <select name="to[]" id="multiselect_to" class="form-control" size="8" multiple="multiple"></select>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Multi Select with Search</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-xs-5">
                        <select name="from[]" id="search" class="form-control" size="8" multiple="multiple">
                            <option value="1">Item 1</option>
                            <option value="2">Item 5</option>
                            <option value="2">Item 2</option>
                            <option value="2">Item 4</option>
                            <option value="3">Item 3</option>
                        </select>
                    </div>

                    <div class="col-xs-2">
                        <button type="button" id="search_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-forward"></i></button>
                        <button type="button" id="search_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-right"></i></button>
                        <button type="button" id="search_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-left"></i></button>
                        <button type="button" id="search_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-backward"></i></button>
                    </div>

                    <div class="col-xs-5">
                        <select name="to[]" id="search_to" class="form-control" size="8" multiple="multiple"></select>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>




