<span id="validationResult"></span>
<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Template Generator</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">

                <!-- form start -->
                <?php echo form_open("TemplateGenerator/create", "class='dgdpMainForm form-horizontal'"); ?>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Template Name</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter a template name. i.e. Registration Confirmation ">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="template_name" placeholder="Template Name" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Select Category</label>

                        <div class="col-sm-6">
                            <select id="template_category" name="category" class="selectpicker form-control" required>
                                <option value="">Select Category</option>
                                <option value="E">Email</option>
                                <option value="L">Letter/Forwarding Letter</option>
                            </select>
                        </div>

                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Select Type</label>
                        <div class="col-sm-6">
                            <select name="type" class="selectpicker form-control" required>
                                <option value="">Select Type</option>
                                <option value="I">Individual</option>
                                <option value="G">Global</option>
                            </select>
                        </div>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>

                    </div>

                    <div class="form-group" id="template_subject">
                        <label class="col-sm-3 control-label">Subject</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="subject" placeholder="Subject" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Body</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="body" rows="10"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                <?php echo form_close(); ?>

            </div>
        </div>

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function(){

        $("#template_subject").hide();
        $("#template_category").change(function () {
            if ($("#template_category").val() == 'E')
                $("#template_subject").show();
            else
                $("#template_subject").hide();
        });

    })
</script>