<?php if($this->session->flashdata('flashMessage')): ?>
    <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('flashMessage'); ?></div>
<?php endif; ?>

<?php

$formError = validation_errors();

if(!empty($formError)): ?>
    <div class="alert alert-danger" role="alert"><?php echo $formError; ?></div>
<?php endif; ?>

<span id="resultNotification"></span>
<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Template Generator</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="modalLink btn btn-primary btn-xs"  title="Create New" data-tooltip="tooltip" data-placement="top" href="<?php echo site_url("TemplateGenerator/create"); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table class="dtsp table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Template Name</th>
                        <th>Template Category</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Template Name</th>
                        <th>Template Category</th>
                        <th>Type</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>

    </div>

</div>

<script type="text/javascript">

    $(document).ready(function() {


        var table = $('.dtsp').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "TemplateGenerator/ajaxTplGenIndex",
                "type": "POST"
            },
            responsive: true,
            aoColumnDefs: [
                {
                    bSortable: false,
                    aTargets: [ -1 ]
                }
            ]
        } );


        $('#showDetaildModal').on('shown.bs.modal', function (e) {

            $('.dgdpMainForm').formValidation()
                .on('success.form.fv', function(e) {
                    // Prevent form submission
                    e.preventDefault();

                    var $form = $(e.target),
                        fv    = $form.data('formValidation');

                    // Use Ajax to submit form data
                    $.ajax({
                        url: $form.attr('action'),
                        type: 'POST',
                        dataType:'JSON',
                        data: $form.serialize(),
                        success: function(data) {

                            $("#validationResult").html(data.msg);

                            if(data.result=='success'){
                                table.ajax.reload();
                            }


                        }
                    });
                });

            // for edit

            $(".tag-check").keydown(function (event) {
                var tb = $(this).get(0);
                var start = tb.selectionStart;

                var end = tb.selectionEnd;
                var reg = new RegExp(/\[(.*?)\]/g);
                //var reg = new RegExp("(<.+?>)", "g");
                var amatch = null;
                while ((amatch = reg.exec(tb.value)) != null) {
                    var thisMatchStart = amatch.index-1;
                    var thisMatchEnd = amatch.index + amatch[0].length+1;
                    if (start <= thisMatchStart && end > thisMatchStart) {
                        event.preventDefault();
                        return false;
                    }
                    else if (start > thisMatchStart && start < thisMatchEnd) {
                        event.preventDefault();
                        return false;
                    }
                }
            });



            var category = $( "#template_category option:selected" ).attr( "value" );

            if(category == 'L'){
                $("#template_subject").hide();
            }

            $("#template_category").change(function () {
                if ($("#template_category").val() == 'E')
                    $("#template_subject").show();
                else
                    $("#template_subject").hide();
            });

            // end edit

        });


    } );

</script>