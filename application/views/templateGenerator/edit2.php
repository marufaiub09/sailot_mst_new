<?php

$this->load->library('Util');

$id = 3;
$salutation = 'Mr.';
$first_name = 'MD';
$middle_name = 'Akhirul';
$last_name = 'Islam';
$code = '123456';
$action_url = 'http://google.com';


$strToReplace = array($salutation, $first_name, $middle_name, $last_name, $code, $action_url);
$template = $this->util->getTemplateText($id,$strToReplace);


//echo '<pre>'.print_r($template,1).'</pre>';


?>

<?php if($this->session->flashdata('flashMessage')): ?>
    <div class="alert alert-success" role="alert"><?php echo $this->session->flashdata('flashMessage'); ?></div>
<?php endif; ?>

<?php

$formError = validation_errors();

if(!empty($formError)): ?>
    <div class="alert alert-danger" role="alert"><?php echo $formError; ?></div>
<?php endif; ?>


<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Template Generator</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">

                <!-- form start -->
                <form method="post" action="" id="templateGenerator" class="dgdpMainForm form-horizontal">

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Template Name</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter a template name. i.e. Registration Confirmation ">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="template_name" value="<?php echo $tpl_details->TEMPL_NAME ?>" placeholder="Template Name" >
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Select Category</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <div class="selectContainer">
                                <?php echo form_dropdown('category', array(''=>'Select Category','E'=>'Email','L'=>'Letter/Forwarding Letter'),
                                    $tpl_details->TEMPL_CAT,'id="template_category" class="selectpicker form-control" required'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Select Type</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <div class="selectContainer">

                                <?php echo form_dropdown('type', array(''=>'Select Type','I'=>'Individual','G'=>'Global'),
                                    $tpl_details->TEMPL_TYPE,'id="template_category" class="selectpicker form-control" required'); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" id="template_subject">
                        <label class="col-sm-3 control-label">Subject</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="subject" value="<?php echo $tpl_details->TEMPL_SUBJECT ?>" placeholder="Subject" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Body</label>
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                            <i class="fa fa-question-circle"></i>
                        </a>
                        <div class="col-sm-6">
                            <textarea id="t" class="tag-check form-control" name="body" rows="10"><?php echo $tpl_details->TEMPL_BODY ?></textarea>
                            <!--                            <textarea class="dgdpTextAreaEditor tag-check form-control" name="body" rows="10">--><?php //echo $tpl_details->TEMPL_BODY ?><!--</textarea>-->
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form><!-- form end -->

            </div>
        </div>

    </div>

</div>

<script type="text/javascript">
    $(document).ready(function(){

        $(".tag-check").keydown(function (event) {
            var tb = $(this).get(0);
            var start = tb.selectionStart;

            var end = tb.selectionEnd;
            var reg = new RegExp(/\[(.*?)\]/g);
            //var reg = new RegExp("(<.+?>)", "g");
            var amatch = null;
            while ((amatch = reg.exec(tb.value)) != null) {
                var thisMatchStart = amatch.index-1;
                var thisMatchEnd = amatch.index + amatch[0].length+1;
                if (start <= thisMatchStart && end > thisMatchStart) {
                    event.preventDefault();
                    return false;
                }
                else if (start > thisMatchStart && start < thisMatchEnd) {
                    event.preventDefault();
                    return false;
                }
            }
        });



        var category = $( "#template_category option:selected" ).attr( "value" );

        if(category == 'L'){
            $("#template_subject").hide();
        }

        $("#template_category").change(function () {
            if ($("#template_category").val() == 'E')
                $("#template_subject").show();
            else
                $("#template_subject").hide();
        });

    })
</script>