
<table class="table table-bordered">
    <thead>
        <tr>
            <th>column Name</th>
            <th>Info</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Template Name</td>
            <td><?=$tpl_details->TEMPL_NAME?></td>
        </tr>
        <tr>
            <td>Category</td>
            <td><?=$tpl_details->TEMPL_CAT=='E'?'Email':'Letter/Forwarding Letter'?></td>
        </tr>
        <tr>
            <td>Type</td>
            <td><?=$tpl_details->TEMPL_TYPE=='I'?'Individual':'Global'?></td>
        </tr>
        <tr>
            <td>Status</td>
            <td><?=$tpl_details->ACTIVE_FLAG==1?'Active':'Inaction'?></td>
        </tr>

    </tbody>
</table>