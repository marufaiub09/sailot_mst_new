<span id="validationResult"></span>
<!-- form start -->
<?php echo form_open("", "class='dgdpMainForm form-horizontal'"); ?>

<div class="form-group">
    <label class="col-sm-3 control-label">Template Name</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter a template name. i.e. Registration Confirmation ">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="template_name" placeholder="Template Name" value="<?php echo $tpl_details->TEMPL_NAME ?>" required>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Select Category</label>

    <div class="col-sm-6">

            <?php echo form_dropdown('category', array(''=>'Select Category','E'=>'Email','L'=>'Letter/Forwarding Letter'),
                $tpl_details->TEMPL_CAT,'id="template_category" class="selectpicker form-control" required'); ?>

    </div>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Select Type</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <div class="selectContainer">

            <?php echo form_dropdown('type', array(''=>'Select Type','I'=>'Individual','G'=>'Global'),
                $tpl_details->TEMPL_TYPE,'id="template_category" class="selectpicker form-control" required'); ?>
        </div>
    </div>
</div>

<div class="form-group" id="template_subject">
    <label class="col-sm-3 control-label">Subject</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <input type="text" class="form-control" name="subject" value="<?php echo $tpl_details->TEMPL_SUBJECT ?>" placeholder="Subject" />
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Body</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
    
    <div class="col-sm-6">
        <textarea id="t" class="tag-check form-control" name="body" rows="10"><?php echo $tpl_details->TEMPL_BODY ?></textarea>
    </div>
</div>


<div class="form-group">
    <div class="col-sm-offset-3">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('save') ?></button>
    </div>
</div>

<?php echo form_close(); ?>

