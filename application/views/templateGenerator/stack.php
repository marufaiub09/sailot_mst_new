<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Panel Title</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">

                <?php

                $product_list =
                    array(
                        array(
                            'product_id' => 1,
                            'product_name' => 'product 1'
                        ),
                        array(
                            'product_id' => 2,
                            'product_name' => 'product 2'
                        ),
                        array(
                            'product_id' => 3,
                            'product_name' => 'product 3'
                        ),
                        array(
                            'product_id' => 4,
                            'product_name' => 'product 4'
                        ),
                        array(
                            'product_id' => 5,
                            'product_name' => 'product 5'
                    )
                    );

                //echo '<pre>'.print_r($product_list,1).'</pre>';
                ?>

                <table class="table table-bordered table-hover">
                	<thead>
                		<tr>
                            <th>Name</th>
                            <th>Action</th>
                		</tr>
                	</thead>
                	<tbody>
                    <?php if (is_array($product_list)) {
                        foreach ($product_list as $item): ?>
                            <tr>
                                <td>
                                    <?=$item['product_name']?></td>
                                <td>
                                    <a href="#" class="btn btn-small z-depth-0" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" id="mts" data-id="<?=$item['product_id']?>" data-name="<?=$item['product_id']?>">
                                        <span class="glyphicon glyphicon-edit"></span>sid
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach;} ?>
                	</tbody>
                </table>


            </div>
        </div>

    </div>

</div>