<form class="form-horizontal frmContent unitMainForm" method="post">
<table id="sailorTable"  class="table table-striped table-bordered dataTable" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Rank Name</th>
            <th>Part II Name</th>
            <th>Sanction Number</th>
            <th>Remarks</th>
            <th>Action</th>
            
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($unitData as $row) { ?>
        <tr id="row_<?php echo $row->UnitWiseSanctionID ?>">

             <td class="hidden"><input type="text" class="form-control" placeholder="Sanction Number" Name="UnitWiseSanctionID[]" id="SanctionId" value="<?php echo $row->UnitWiseSanctionID ?>">
            </td>
            <td>
                <select class="select2 form-control required" name="RankName[]" id="RankId" data-placeholder="Select Rank" aria-hidden="true" data-allow-clear="true">
                    <option value="">Select Rank</option>
                    <?php
                    foreach ($rankData as $row1):
                        ?>
                        <option value="<?php echo $row1->RANK_ID ?>"" <?php echo ($row->RankID == $row1->RANK_ID) ? 'selected' : '' ?>><?php echo "[".$row1->RANK_CODE."] ".$row1->RANK_NAME ?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
            </td>

            <td>
                <select class="select2 form-control required" name="PartName[]" id="partId" data-placeholder="Select Name" aria-hidden="true" data-allow-clear="true">
                    <option value="">Select partII</option>
                    <?php
                    foreach ($partII as $row1):
                        ?>
                        <option value="<?php echo $row1->PartIIID ?>" <?php echo ($row->PartIIID == $row1->PartIIID) ? 'selected' : '' ?>><?php echo "[".$row1->Code."] ".$row1->Name ?></option>
                    <?php
                    endforeach;
                    ?>
                </select>
            </td>
            

            <td><input type="text" class="form-control numbersOnly" placeholder="Sanction Number" Name="SanctionNo[]" id="SanctionId" value="<?php echo $row->SanctionNo ?>">
            </td>

            <td><input type="text" class="form-control" placeholder="Remarks" Name="Remarks[]" id="Remarks" value="<?php echo $row->Remarks ?>">
            </td>

            <td>
                <center>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->UnitWiseSanctionID; ?>" title="Click For Delete" data-type="delete" data-field="UnitWiseSanctionID" data-tbl="unitwisesanction"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
            </td>
        </tr>
        <?php }?>
        <?php
        ?>
    </tbody>
</table>
 <div class="xh" style="text-align: right;">
        <span class="btn btn-xs btn-success" id="add_record">
            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
        </span>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-12">
            <input type="button" class="btn btn-primary btn-sm formSSS"  data-type="list" value="Save">

            <input type="button" class="btn btn-primary btn-sm formDeleteAll" value="Delete All">
        </div>
    </div>


<script type="text/javascript" language="javascript">
    $(document).on("click", ".formDeleteAll", function () {
        if (confirm('Are you want to Delete?')) {
            var data = $(".unitMainForm").serialize();
            // console.log(data);
            $.ajax({
                type: "POST",
                data: data,
                url: "<?php echo site_url('othersInfo/OrganizationWiseSanction/deleteUnitData'); ?>",
                success: function (data) {
                    $(".msg").html(data);   
                    $('.msg').delay(1000).fadeOut();                    
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });
    
</script>