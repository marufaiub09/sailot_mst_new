<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><?php echo 'DAO Amendment' ?></h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs" href="<?php echo site_url('othersInfo/DAOAmendment/create'); ?>" title="<?php ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>
                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>               
            </div>
            <div class="panel-body">
                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th> SN </th>
                            <th>Official No </th> 
                            <th>Previous DAO </th> 
                            <th>Amendad DAO</th> 
                            <th>Authority Number</th>
                            <th>Authority Date</th>                                                         
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th> SN </th>
                            <th>Official No </th> 
                            <th>Previous DAO </th> 
                            <th>Amendad DAO</th> 
                            <th>Authority Number</th>
                            <th>Authority Date</th>                                                         
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody id="officialNoData">
                        <?php foreach ($daoAmendment as $key => $row) { ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo $row->OFFICIALNUMBER ?></td>
                                <td><?php echo $row->PRE_DAO ?></td>
                                <td><?php echo $row->AMEND_DAO ?></td>
                                <td><?php echo $row->AuthorityNumber ?></td>
                                <td><?php echo $row->AuthorityDate ?></td>                            
                                <td>
                         <center>
                            <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('othersInfo/DAOAmendment/view/' . $row->DAOAmendID); ?>"  title="View DAO Amendment" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a class="btn btn-warning btn-xs cm" href="<?php echo site_url('othersInfo/DAOAmendment/edit/' . $row->DAOAmendID); ?>" title="Edit DAO Amendment" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->DAOAmendID; ?>" title="Click For Delete" data-type="delete" data-field="DAOAmendID" data-tbl="daoamend"><span class="glyphicon glyphicon-trash"></span></a>
                        </center>
                        </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
