<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('othersInfo/DAOAmendment/index'); ?>" title="List leave informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Add DAO Amendment</center></h3>

                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="sailorMainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Official No <span class="text-danger"> * </span> </label>        
                                <div class="col-sm-6" >
                                    <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                    <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                </div>
                            </div>                                
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Full Name</label>
                                <div class="col-sm-8">
                                    <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Rank</label>                                            
                                <div class="col-sm-8">
                                    <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5 control-label">Current DAO <span class="text-danger"> * </span></label>
                                <div class="col-md-6" >
                                    <select class="select2 form-control required" name="currentDao"  data-placeholder="Current DAO" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Current DAO</option>
                                        <?php
                                        foreach ($dao as $row):
                                            ?>
                                            <option value="<?php echo $row->DAO_ID."_".$row->DAO_NO; ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="padding-left: 0px;">Pre DAO <span class="text-danger">*</span></label>
                                <div class="col-sm-7" >
                                    <select class="select2 form-control required" name="previousDAO"  data-placeholder="Previous DAO" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Previous DAO</option>
                                        <?php
                                        foreach ($dao as $row):
                                            ?>
                                            <option value="<?php echo $row->DAO_ID."_".$row->DAO_NO; ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend" style="font-size: 12px;  margin-bottom: 10px;">Posting Unit</legend>
                            <div class="col-md-6">
                                <div class="col-md-12">                            
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Zone<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <?php echo form_dropdown("zone", $zone, set_value("zone"), "class='form-control select2' id='zone'"); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Area<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <select  id="area" name="area" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ship/Est<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <select  id="shift" name="shiftEstablishment" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Posting Unit<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <select  id="postingUnit" name="postingUnit" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority No<span class="text-danger"> * </span></label>
                                        <div class="col-sm-6 control-labe">
                                            <?php echo form_input(array('name' => 'authorityNumber', "id" => "authorityNumber", "class" => "form-control required", 'required' => 'required')); ?>              
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority Date</label>
                                        <div class="col-sm-6 control-labe">
                                            <?php echo form_input(array('name' => 'authorityDate', "id" => "authorityDate", "class" => "datePicker form-control required", 'required' => 'required', "value" => date("d-m-Y"))); ?>              
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Amend text</label>
                                        <div class="col-sm-9 control-labe">
                                            <?php echo form_textarea(array('name' => 'amendTest', "class" => "redactor form-control", 'placeholder' => 'Amend Text', 'rows' => '2')); ?>
                                            
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-2">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect pull-left" data-action="othersInfo/DAOAmendment/save" data-redirect-action="othersInfo/DAOAmendment/index" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    .line-behind-text span {
        font-size: 14px !important;
    }
    .line-behind-text::before {
        top: 60%;
        border-top: 1px solid #e5e5e5 !important;
    }
</style>
<?php $this->load->view("common/sailors_info"); ?>

<script type="text/javascript">
    $(document).ready(function () {    
        $('#zone').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_area'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#area').html(data);
                }
            });
        });
        $('#area').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_shift'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shift').html(data);
                }
            });
        });
        $('#shift').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_postingUnit'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#postingUnit').html(data);
                }
            });
        });
    });
</script>