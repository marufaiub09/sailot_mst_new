<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('othersInfo/DAOAmendment/index'); ?>" title="List leave informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Edit DAO Amendment</center></h3>

                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="sailorMainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-5 control-label">Official No <span class="text-danger"> * </span> </label>        
                                <div class="col-sm-6" >
                                    <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number', 'value' => ($ac_type == "edit") ? $result->OFFICIALNUMBER : '', 'readonly' => "($ac_type == 'edit')? 'readonly':'' ")); ?>           
                                    <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                </div>
                            </div>                                
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Full Name</label>
                                <div class="col-sm-8">
                                    <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => ($ac_type == "edit") ? $result->FULLNAME : set_value('fullName'))); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Rank</label>                                            
                                <div class="col-sm-8">
                                    <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => ($ac_type == "edit") ? $result->RANK_NAME : set_value('rank'))); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-md-5 control-label">Current DAO <span class="text-danger"> * </span></label>
                                <div class="col-md-6" >
                                    <select class="select2 form-control required" name="currentDao"  data-placeholder="Select Current DAO" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Current DAO</option>
                                        <?php
                                        foreach ($dao as $row):
                                            ?>
                                            <option value="<?php echo $row->DAO_ID . "_" . $row->DAO_NO; ?>" <?php echo ($result->CurrentDAOID == $row->DAO_ID) ? 'selected' : '' ?>><?php echo $row->DAO_NO ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="padding-left: 0px;">Pre DAO <span class="text-danger">*</span></label>
                                <div class="col-sm-7" >
                                    <select class="select2 form-control required" name="previousDAO"  data-placeholder="Select Previous DAO" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select ship area</option>
                                        <?php
                                        foreach ($dao as $row):
                                            ?>
                                            <option value="<?php echo $row->DAO_ID . "_" . $row->DAO_NO; ?>" <?php echo ($result->CurrentDAOID == $row->DAO_ID) ? 'selected' : '' ?>><?php echo $row->DAO_NO ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend" style="font-size: 12px;  margin-bottom: 10px;">Posting Unit</legend>
                            <div class="col-md-6">
                                <div class="col-md-12">                            
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Zone<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <select class="select2 form-control required" name="" id="zone" data-placeholder="Select ship establishment" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Zone</option>
                                                <?php
                                                foreach ($zone as $row):
                                                    ?>
                                                    <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->ADMIN_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Area<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <select class="select2 form-control required" name="" id="area" data-placeholder="Select ship establishment" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Zone</option>
                                                <?php
                                                foreach ($area as $row):
                                                    ?>
                                                    <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->AREA_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Ship/Est<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <select class="select2 form-control required" name="shiftEstablishment" id="ship" data-placeholder="Select ship establishment" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select ship establishment</option>
                                                <?php
                                                foreach ($establishment as $row):
                                                    ?>
                                                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->SHIP_ESTABLISHMENTID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Posting Unit<span class="text-danger"> * </span></label>
                                        <div class="col-sm-7" >
                                            <select class="select2 form-control required" name="postingUnit" id="postingUnit" data-placeholder="Select ship establishment" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Zone</option>
                                                <?php
                                                foreach ($PostingUnit as $row):
                                                    ?>
                                                    <option value="<?php echo $row->POSTING_UNITID ?>" <?php echo ($result->POSTING_UNITID == $row->POSTING_UNITID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority No<span class="text-danger"> * </span></label>
                                        <div class="col-sm-6 control-labe">
                                            <?php echo form_input(array('name' => 'authorityNumber', 'value' => $result->AuthorityNumber, "id" => "authorityNumber", "class" => "form-control required", 'required' => 'required')); ?>              
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority Date</label>
                                        <div class="col-sm-6 control-labe">
                                            <?php echo form_input(array('name' => 'authorityDate', 'value' => $result->AuthorityDate, "id" => "authorityDate", "class" => "datePicker form-control required", 'required' => 'required')); ?>              
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-12"> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" style="padding-left: 0px;">Amend text</label>
                                        <div class="col-sm-7 control-labe">
                                            <?php echo form_textarea(array('name' => 'amendTest', 'value' => $result->AmendText, "class" => "form-control", 'placeholder' => 'Amend Text', 'rows' => '2')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    </fieldset>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-2">
                        <input type="hidden" name="id" value="<?php echo $result->DAOAmendID; ?>">
                        <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect pull-right" data-action="othersInfo/DAOAmendment/update" data-redirect-action="othersInfo/DAOAmendment/index" value="Update">
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

<style>
    .line-behind-text span {
        font-size: 14px !important;
    }
    .line-behind-text::before {
        top: 60%;
        border-top: 1px solid #e5e5e5 !important;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        
        $('#zone').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_area'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#area').html(data);
                    $('#area').select2('val', '');

                }
            });
        });
        $('#area').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_shift'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#ship').html(data);
                    $('#ship').select2('val', '');                    
                }
            });
        });
        $('#ship').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_postingUnit'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#postingUnit').html(data);
                    $('#postingUnit').select2('val', '');

                }
            });
        });
    });
</script>

<?php $this->load->view("common/sailors_info"); ?>
