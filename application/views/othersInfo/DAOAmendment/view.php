<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Posting Unit Name</th>
        <td><?php echo $viewdetails->POSTING_UNIT_NAME ?></td>
    </tr>
    <tr>
        <th>Authority Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->AuthorityDate)) ?></td>
    </tr>
    <tr>
        <th>Previous DAO Number</th>
        <td><?php echo $viewdetails->PreviousDAONumber ?></td>
    </tr>
    <tr>
        <th>Current DAO Number</th>
        <td><?php echo $viewdetails->CurrentDAONumber ?></td>
    </tr>
    <tr>
        <th>Authority No</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Amend Text</th>
        <td><?php echo $viewdetails->AmendText ?></td>
    </tr>
</table>