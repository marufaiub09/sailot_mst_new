<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                    
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Branch And Rank Wise Section</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body custom_panel">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <div class="row ">
                        <div class="col-md-4 ">
                            <div class="row   ">
                                <div class="col-md-12 btn btn-primary btn-block">
                                    <div class="col-md-12">
                                        <div class="controls" style="margin-top: 2px;">
                                            <select class="select2 form-control formSearch" name="branch_id" id="branch_id" data-tags="true" data-placeholder="Select Branch" data-allow-clear="true">
                                                <option value="">Select Branch</option>
                                                <?php
                                                foreach ($branch as $row):
                                                    ?>
                                                    <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-primary">Clear</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row col-sm-6">
            <div class="box box-warning">
                <div class="contentArea">
                    <table id="employee-grid" class="display table-striped " width="100%" cellspacing="0">
                        <thead>
                            <tr>                               
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.frmContent').change(function () {
        var branchId = $(' .frmContent option:selected').val();
        if (branchId != '') {
            var allData = $(".frmContent").serialize();
            $.ajax({
                type: "post",
                data: allData,
                dataType: "html",
                url: "<?php echo site_url(); ?>/othersInfo/branchAndRankWiseSection/searchBranchAndRankData",
                beforeSend: function () {
                    $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".contentArea").html(data);
                }
            });
        } else {
            $(".contentArea").html('');
        }
    });
</script>