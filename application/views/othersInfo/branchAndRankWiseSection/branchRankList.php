<form class="form-horizontal frmContent" id="branchForm" method="post">
<table id="dataTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>Sl No.</th>
            <th>Rank Name</th>            
            <th>Sanction Number</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $i = 1;
        foreach ($branchRank as $row) { ?>
        <tr id="row_<?php echo $row->RANK_ID ?>">
            <td><?php echo $i++; ?></td>
            <td><?php echo $row->RANK_NAME ?>
            <td class="hidden"><input Name="rank_id[]"  value="<?php echo $row->RANK_ID ?>"></td>            
            <td><input Name="sectionNumber[]" value="<?php echo $row->SANCTION_NUMBER ?>"></td>
        </tr>
        <?php }?>      
        <?php
        ?>
    </tbody>
</table>
 <div class="form-actions pull-right">
    <!-- <button type="submit" class="btn btn-primary" ><i class="fa fa-check-circle"></i> Save</button> -->    
    <input type="button" class="btn btn-primary btn-sm " id="formSave" value="Save">        
            <input type="button" class="btn btn-primary btn-sm " id="formDelete" value="Delete">
        </div>
</form>
<script type="text/javascript" language="javascript">
    $(document).on("click", "#formDelete", function () {
        if (confirm('Are you want to Delete?')) {
            var data = $("#branchForm").serialize();
            //console.log(data);
            $.ajax({
                type: "POST",
                data: data,
                url: "<?php echo site_url('othersInfo/BranchAndRankWiseSection/deleteBranchAndRank'); ?>",
                success: function (data) {
                    $(".msg").html(data);
                    window.location.replace('<?php echo site_url('othersInfo/BranchAndRankWiseSection/index'); ?>');
                }
            });
        } else {
            return false;
        }
    });


        $(document).on("click", "#formSave", function () {
        if (confirm('Are you want to Save?')) {
            var data = $("#branchForm").serialize();
            //console.log(data);
            $.ajax({
                type: "POST",
                data: data,
                url: "<?php echo site_url('othersInfo/BranchAndRankWiseSection/saveBranchAndRank'); ?>",
                success: function (data) {
                    $(".msg").html(data);
                   window.location.replace('<?php echo site_url('othersInfo/BranchAndRankWiseSection/index'); ?>');
                }
            });
        } else {
            return false;
        }
    });


    $('#dataTable').removeAttr('width').DataTable( {
        "scrollX": true,
        "scrollX": true,
        "bPaginate": false,
        "bFilter": false, 
        "bInfo": false,
        "columnDefs": [
            { width: '100px', targets: 0 },
            { width: '100px', targets: 1 },
            { width: '100px', targets: 2 },
            { width: '80px', targets: 3 },
            { width: '100px', targets: 5 },
            { width: '90px', targets: 8 },
            { width: '100px', targets: 9 },
        ],
        "fixedColumns": true

    } );

    
</script>