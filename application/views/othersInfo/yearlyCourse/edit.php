<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-3 control-label">Course/Training</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Training/Course">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="Course" id="Course" data-tags="true" data-placeholder="Select Training/Course" data-allow-clear="true">
                <option value="">Select Training/Course</option>
                <?php
                foreach ($course as $row):
                    ?>
                    <option value="<?php echo $row->NAVYTrainingID ?>" <?php echo ($result->CourseID == $row->NAVYTrainingID) ? 'selected' : '' ?>><?php echo $row->Name ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Batch Number</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Batch Number">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="batch_number" id="Course" data-tags="true" data-placeholder="Select Batch Number" data-allow-clear="true">
                <option value="">Select Batch Number</option>
                <?php
                foreach ($batch_number as $row):
                    ?>
                    <option value="<?php echo $row->BATCH_ID ?>" <?php echo ($result->BatchNumber == $row->BATCH_ID) ? 'selected' : '' ?>><?php echo $row->BATCH_NUMBER ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Duration (In Week)</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Duration">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'duration', 'value' => $result->Duration, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Duration')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Starting Date</label>
        <div class="col-sm-3 date">
            <div class="selectContainer">
                <div class="input-group input-append date datePicker">
                    <?php echo form_input(array('name' => 'starting_date', 'value' => date('d-m-Y', strtotime($result->StartDate)), "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Date')); ?>
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Completion Date</label>
        <div class="col-sm-3 date">
            <div class="selectContainer">
                <div class="input-group input-append date datePicker">
                    <?php echo form_input(array('name' => 'completion_date', 'value' => date('d-m-Y', strtotime($result->EndDate)), "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Date')); ?>
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">No of Trainees</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter No of Trainees">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'trainees_no', 'value' => $result->BatchSize, "class" => "form-control required", 'id' => 'trainees_no', 'required' => 'required', 'placeholder' => 'No of Trainees')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Institute Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Institute Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <select class="select2 form-control required" name="institute_name" id="Course" data-tags="true" data-placeholder="Select Institute Name" data-allow-clear="true">
                <option value="">Select Institute Name</option>
                <?php
                foreach ($institute_name as $row):
                    ?>
                    <option value="<?php echo $row->Training_Institute_ID; ?>" <?php echo ($result->InstituteID == $row->Training_Institute_ID) ? 'selected' : '' ?>><?php echo $row->Name; ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->YearlyCoursePlanID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="othersInfo/YearlyCourse/updatePart" data-su-action="othersInfo/YearlyCourse/partList" data-type="list" value="submit">
        </div>
    </div>
</form>


