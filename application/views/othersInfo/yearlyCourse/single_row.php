<td><?php echo $sn; ?></td>
<td><?php echo $row->COURSE_NAME ?></td>
<td><?php echo $row->Duration ?></td>
<td><?php echo date('d-M-Y', strtotime($row->StartDate)); ?></td>
<td><?php echo date('d-M-Y', strtotime($row->EndDate)); ?></td>
<td><?php echo $row->BatchSize ?></td>
<td><?php echo $row->Training_Institute ?></td>
<td>
<center>
    <a class="itemStatus" id="<?php echo $row->YearlyCoursePlanID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="YearlyCoursePlanID" data-field="ACTIVE_STATUS" data-tbl="bn_yearlycourseplan" data-su-url="othersInfo/yearlyCourse/examNameById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
   <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('othersInfo/yearlyCourse/view/' . $row->YearlyCoursePlanID); ?>" title="View Course Plan" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('othersInfo/yearlyCourse/edit/' . $row->YearlyCoursePlanID); ?>"  title="Edit Course Plan" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->YearlyCoursePlanID; ?>" title="Click For Delete" data-type="delete" data-field="YearlyCoursePlanID" data-tbl="bn_yearlycourseplan"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>