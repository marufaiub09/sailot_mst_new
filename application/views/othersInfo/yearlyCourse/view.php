
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">

    <tr>
        <th width="20%">Course Name</th>
        <td><?php echo $viewdetails->COURSE_NAME ?></td>
    </tr>
    <tr>
        <th width="20%">Batch Name</th>
        <td><?php echo $viewdetails->Batch_name ?></td>
    </tr>
    <tr>
        <th>Duration in Weeks</th>
        <td><?php echo $viewdetails->Duration ?></td>
    </tr> 
    <tr>
        <th>Starting Date</th>
        <td><?php echo date('d-M-Y', strtotime($viewdetails->StartDate)); ?></td>
    </tr> 
    <tr>
        <th>Completion Date</th>
        <td><?php echo date('d-M-Y', strtotime($viewdetails->EndDate)); ?></td>
    </tr> 
    <tr>
        <th>No of Trainees</th>
        <td><?php echo $viewdetails->BatchSize ?></td>
    </tr> 
    <tr>
        <th>Institute Name</th>
        <td><?php echo $viewdetails->Training_Institute ?></td>
    </tr> 
</table>