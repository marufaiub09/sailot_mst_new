<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                    
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Unit Wise Section</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body custom_panel">
                <form class="form-horizontal frmContent unitMainForm" method="post">
                    <div class="row ">
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <label class="col-sm-5 control-label">Zone</label>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Zone">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                                <div class="col-sm-6" >
                                    <?php echo form_dropdown("zone", $zone, set_value("zone"), "class='select2 form-control' id='zone' data-tags='true' data-allow-clear='true' data-placeholder='Select zone'"); ?>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-sm-3 control-label">Area</label>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select First Zone" >
                                    <i class="fa fa-question-circle"></i>
                                </a>
                                <div class="col-sm-6" >
                                    <select  id="area" name="area" class="select2 form-control" data-tags="true" data-allow-clear="true" data-placeholder="Select Area">
                                        <option value="">Select One</option>
                                    </select>
                                </div>
                            </div>                            
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-group col-md-6">
                                <label class="col-sm-5 control-label">Ship/Establishment</label>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select First Area">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                                <div class="col-sm-6" >
                                    <select  id="shift" name="shiftEstablishment" class="select2 form-control" data-tags="true" data-allow-clear="true" data-placeholder="Select Ship/Establishment">
                                        <option value="">Select One</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="col-sm-3 control-label">Posting Unit</label>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select First Shift/Establishment">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                                <div class="col-sm-6" >
                                    <select  id="postingUnit" name="postingUnitName" class="select2 form-control" data-tags="true" data-allow-clear="true" data-placeholder="Select posting unit">
                                        <option value="">Select One</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="row col-sm-12">
            <div class="box box-warning">                
                <div class="contentArea">
                    <table id="employee-grid" class="display table-striped " width="100%" cellspacing="0">
                        <thead>
                            <tr>
                               

                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#postingUnit").on('change', function () {
            var posting = this.value;
            if (posting != '') {
                $.ajax({
                    type: "post",
                    url: "<?php echo site_url(); ?>/othersInfo/UnitWaiseSanction/searchPostingUnitID",
                    data: {posting: posting},
                    beforeSend: function () {
                        $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".contentArea").html(data);
                    }
                });

            } else {
                $(".contentArea").html('');
            }
        });
    });

    $('#zone').change(function () {
        var selectedValue = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_area'); ?>",
            data: {selectedValue: selectedValue},
            dataType: 'html',
            success: function (data) {
                $('#area').html(data);
                $('#area').select2('val', '');
            }
        });
    });

    $('#area').change(function () {
        var selectedValue = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_shift'); ?>",
            data: {selectedValue: selectedValue},
            dataType: 'html',
            success: function (data) {
                $('#shift').html(data);
                $('#shift').select2('val', '');
            }
        });
    });
    $('#shift').change(function () {
        var selectedValue = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_postingUnit'); ?>",
            data: {selectedValue: selectedValue},
            dataType: 'html',
            success: function (data) {
                $('#postingUnit').html(data);
                $('#postingUnit').select2('val', '');
            }
        });
    });


    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#sailorTable tbody").append(' <tr>' +
                '<td>' +
                '<select class="select2 form-control required" id="RankId' + counter + '" name="RankName[]">' +
                '<option value="">-Select-</option>' +
<?php foreach ($rankData as $row) { ?>
            '<option value="<?php echo $row->RANK_ID ?>"><?php echo "[" . $row->RANK_CODE . "]" . "-" . $row->RANK_NAME ?></option>' +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td>' +
                '<select class="select2 form-control required" id="PartIIID' + counter + '" name="PartName[]">' +
                '<option value="">-Select-</option>' +
<?php foreach ($partiiData as $row) { ?>
            '<option value="<?php echo $row->PartIIID ?>"><?php echo "[" . $row->Code . "]" . $row->Name ?></option>' +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td>' +
                ' <input type="text" name="SanctionNo[]" id="SanctionId' + counter + '"  class="form-control numbersOnly" placeholder="Sanction Number" >' +
                '</td>' + '<td>' +
                ' <input type="text" name="Remarks[]" id="Remarks' + counter + '"  class="form-control " placeholder="Remarks" >' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'

        );
    });


    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });



    $(document).on("click", ".formSSS", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm('Are you want to Save?')) {
                var data = $(".unitMainForm").serialize();
                console.log(data);
                $.ajax({
                    type: "POST",
                    data: data,
                    url: "<?php echo site_url('/othersInfo/UnitWaiseSanction/saveData'); ?>",
                    success: function (data) {
                        $(".msg").html(data);
                        $('.msg').delay(1000).fadeOut();
                        location.reload();
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });

    $(document).on('keyup', '.numbersOnly', function () {
        var val = $(this).val();
        if (isNaN(val)) {
            val = val.replace(/[^0-9\.]/g, '');
            if (val.split('.').length > 2) {
                val = val.replace(/\.+$/, "");
            }
        }
        $(this).val(val);
    });

</script>