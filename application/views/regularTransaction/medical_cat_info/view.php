<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th width="20%">Medical category</th>
        <td>
            <?php
            if ($viewdetails->MedicalCategory == '1') {
                echo 'A (AYE)';
            } elseif ($viewdetails->MedicalCategory == '2') {
                echo 'B (BEE)';
                } elseif ($viewdetails->MedicalCategory == '3') {
                echo 'C (CEE)';
                } elseif ($viewdetails->MedicalCategory == '4') {
                echo 'D (DEE)';
            } else {
                echo 'E (EEE)';
            }
            ?>
        </td>
    </tr>
    <tr>
        <th>Duration</th>
        <td><?php echo $viewdetails->Duration ?></td>
    </tr>
    <tr>
        <th>DAO Number</th>
        <td><?php echo $viewdetails->DAONumber ?></td>
    </tr>
    <tr>
        <th>Effective Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->Effectivedate)) ?></td>
    </tr>
    <tr>
        <th>Authority Number</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Authority Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->Authoritydate)) ?></td>
    </tr>
    <tr>
        <th>Description</th>
        <td><?php echo $viewdetails->Description ?></td>
    </tr>
</table>