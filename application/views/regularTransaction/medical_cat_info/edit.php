<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/medicalCatInfo/index') : site_url('regularTransaction/medicalCatInfo/index') ?>" title="medical category informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Edit medical category information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <?php $medicalCat = array("1" => "A (AYE)", "2" => "B (BEE)", "3" => "C (CEE)", "4" => "D (DEE)", "5" => "E (EEE)"); ?>
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Current Info</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'value' => $result->OFFICIALNUMBER, 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number', 'readonly' => 'readonly')); ?>
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                            <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Medical Category</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'medical_Cat', "class" => "form-control PostingUnit", 'placeholder' => 'medical category', 'readonly' => 'readonly', 'value' => $medicalCat[$result->MedicalCategory])); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => $result->FULLNAME)); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => $result->RANK_NAME)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">ship/Estabishment</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'shipEstablishment', "class" => "form-control SHIP_ESTALISHMENT required", 'required' => 'required', 'placeholder' => 'ship/establishment', 'readonly' => 'readonly', 'value' => $result->SHIP_EST)); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Authority Information</legend>    
                            <div class="col-md-6">                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Medical category</label>
                                    <div class="col-sm-5">
                                        <select class="select2 form-control required" name="medicalCat" id="medicalCat" data-tags="true" data-placeholder="Select Gender Type" data-allow-clear="true">
                                            <option value="">Select Category Type</option>
                                            <option value="1"<?php echo ($result->MedicalCategory == 1) ? 'selected' : '' ?>>A (AYE)</option>
                                            <option value="2"<?php echo ($result->MedicalCategory == 2) ? 'selected' : '' ?>>B (BEE)</option>
                                            <option value="3"<?php echo ($result->MedicalCategory == 3) ? 'selected' : '' ?>>C (CEE)</option>
                                            <option value="4"<?php echo ($result->MedicalCategory == 4) ? 'selected' : '' ?>>D (DEE)</option>
                                            <option value="5"<?php echo ($result->MedicalCategory == 5) ? 'selected' : '' ?>>E (EEE)</option>
                                        </select>
                                    </div>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Duration</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'Duration', "id" => "Duration", "class" => "form-control", 'value' => $result->Duration)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Effective Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'EffectiveDate', "id" => "EffectiveDate", "class" => "datePicker form-control required", 'value' => Date('d-m-Y', strtotime($result->Effectivedate)))); ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id" => "authorityNumber", "class" => "form-control required", "value" => $result->AuthorityNumber)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Description</label>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'remarks', "id" => "remarks", "class" => "form-control", "rows" => 2, "cols" => 2, "value" => $result->Description)); ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Category Type</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"><input type="radio" value="1" name="categoryType" class="categoryType"> Permanent</label>
                                        <label class="col-sm-3 control-label"><input type="radio" value="2" name="categoryType" class="categoryType" checked> Temporary</label>
                                    </div> 
                                </fieldset>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number</label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"<?php echo ($result->DAOID == $row->DAO_ID) ? 'selected' : '' ?>><?php echo $row->DAO_NO ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id" => "authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>                                
                            </div>
                        </fieldset>
                    </div>                      
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="<?php echo $result->MedicalID; ?>">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/medicalCatInfo/update':'regularTransaction/medicalCatInfo/update' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/medicalCatInfo/index':'regularTransaction/medicalCatInfo/index'?>" value="Update">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(".categoryType").click(function(event) {
        var catType = $('input[name=categoryType]:checked', '#MainForm').val();
        if (catType == 1) {
            $("#Duration").prop('disabled', true);            
        }else{
            $("#Duration").prop('disabled', false);
        }
    });
    $("#medicalCat").on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        var cat = $(this).find('option:selected').val();
        if(cat ==1 ){
            $("#Duration").prop('disabled', true);
            $(".categoryType").prop('disabled', true);
        }else{
            $("#Duration").prop('disabled', false);
            $(".categoryType").prop('disabled', false);
        }
    });
</script>