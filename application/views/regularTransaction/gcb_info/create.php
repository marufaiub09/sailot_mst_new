<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/gcbAwardRestore/index') : site_url('regularTransaction/gcbAwardRestore/index') ?>" title="List GCB Ward Restore">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Add GCB Award/Restore</center></h3>                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Current Info</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumberGCB', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ship/Establishment</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'SHIP_ESTALISHMENT', "class" => "form-control SHIP_ESTALISHMENT required", 'required' => 'required', 'placeholder' => 'ship/Establishment', 'readonly' => 'readonly', 'value' => set_value('SHIP_ESTALISHMENT'))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Award/Restore</label>
                                    <div class="col-md-8">
                                        <label class="radio-inline"> <?php echo  form_radio(array('name' => 'GCBType',"id" => "award", 'value' =>  1, "checked" => TRUE));?>Award</label>
                                        <label class="radio-inline"> <?php echo  form_radio(array('name' => 'GCBType',"id" => "restore", 'value' =>  2))?>Restore</label>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Award/Restore Info</legend>
                            <div class="col-md-6">                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">GCB NO</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'gcbNo', "id"=>"gcbNo", "class" => "form-control required", 'readonly' => 'readonly', "value" => set_value("") )); ?>
                                        <input type="hidden" name="GCB_NO" id="GCB_NO" value="">
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                            <span class="smloadingImg"></span><span class="alertSMSgcv label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number</label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Award Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Award date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'awardDate', "id"=>"awardDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="AUTHO_SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                        </select>               
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                
                            </div>
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/gcbAwardRestore/save':'regularTransaction/gcbAwardRestore/save' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/gcbAwardRestore/index':'sailorsInfo/gcbAwardRestore/index'?>" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $("#officialNumberGCB").on('blur', function(){
        var flag = "<?php echo $flag; ?>";
        var sailorStatus = (flag >= 1 ? 3 : 1 ); /*sailor status 1 means active and 3 means retirement*/
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus: sailorStatus},
                dataType: "json",
                url: "<?php echo site_url(); ?>regularTransaction/GcbAwardRestore/searchSailorInfoByOfficalNo",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data !== null){
                        $(".alertSMSgcv").html('');
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);  
                        $(".SHIP_ESTALISHMENT").val(data['SHIP_ESTABLISHMENT']);  

                        $.ajax({
                            type: "post",
                            data: {officeNumber: officeNumber},
                            dataType: "json",
                            url: "<?php echo site_url(); ?>regularTransaction/GcbAwardRestore/gcbCountByOfficalNo",
                            success: function (datagcb) {
                                if(datagcb['gcb'] == 0){
                                    $("#gcbNo").val("1st");
                                    $("#GCB_NO").val("1");
                                }else if(datagcb['gcb'] == 1){
                                    $("#gcbNo").val("2nd");
                                    $("#GCB_NO").val("2");
                                }else if(datagcb['gcb'] == 2){
                                    $("#gcbNo").val("3rd");
                                    $("#GCB_NO").val("3");
                                }
                                else if(datagcb['gcb'] == 3){
                                    $(".alertSMSgcv").html('Limit Exists');
                                }
                            }
                        });

                        $(".alertSMS").html('');
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');      
                        $(".SHIP_ESTALISHMENT").val(''); 
                        $("#gcbNo").val('');
                        $("#GCB_NO").val('');
                        $(".alertSMSgcv").html('');
                        $(".alertSMS").html('Invalid Officer Number');      
                    }
                }
            });
        }else{
            $(".alertSMS").html(''); 
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');      
            $(".SHIP_ESTALISHMENT").val(''); 
            $("#gcbNo").val('');
            $("#GCB_NO").val('');
            $(".alertSMSgcv").html('');
        }
    });
</script>