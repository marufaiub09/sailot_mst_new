<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th>Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Authority ship Establishment</th>
        <td><?php echo $viewdetails->AUTHO_SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Award/Restore Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->EffectDate)) ?></td>
    </tr>
     <tr>
        <th width="20%">Award/Restore</th>
        <td>
            <?php
            if ($viewdetails->GCBType == '1') {
                echo 'Award';
            } else  {
                echo 'Restore';
            }
            ?>
        </td>
    </tr>
    <tr>
        <th>Authority Number</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Authority Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->AuthorityDate)) ?></td>
    </tr>
    <tr>
        <th>DAO Number</th>
        <td><?php echo $viewdetails->DAONumber ?></td>
    </tr>
</table>