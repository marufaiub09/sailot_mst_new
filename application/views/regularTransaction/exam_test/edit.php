<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
     <?php 
        /*echo "<pre>";
        print_r($result);
        exit();*/
    ?>
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/examTestInfo/index') : site_url('regularTransaction/examTestInfo/index') ?>" title="List exam/test informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Edit Exam/Test</center></h3>
                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <span class="frmMsg"></span>
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Exam Type</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="EXAM_TYPE_ID" id="EXAM_TYPE_ID" data-placeholder="Select exam type" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select One</option>
                                            <?php
                                            foreach ($trainingType as $row):
                                                ?>
                                                <option value="<?php echo $row->EXAM_ID ?>" <?php echo ($result->PARENT_ID == $row->EXAM_ID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select exam type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Exam Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select exam name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control required" name="EXAM_NAME_ID" id="EXAM_NAME_ID" data-placeholder="Select Exam Name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select One</option>
                                            <?php
                                            foreach ($examName as $row):
                                                if($result->PARENT_ID == $row->PARENT_ID ):
                                                ?>
                                                <option value="<?php echo $row->EXAM_ID ?>" <?php echo ($result->ExamID == $row->EXAM_ID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                                endif;
                                            endforeach; 
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Exam/Test Date</label>
                                    <div class="col-sm-4">
                                       <?php echo form_input(array('name' => 'examDate', "class" => "datePicker form-control required", 'placeholder' => 'Exam/Test date', 'value' => date("d-m-Y", strtotime($result->ExamDate)))); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend"></legend>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Zone</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control" name="AUTHORITY_ZONE_ID" id="AUTHORITY_ZONE_ID" data-placeholder="Select Authority zone" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Authority zone</option>
                                            <?php
                                            foreach ($authorityZone as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->AUTHO_ZONE == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority zone">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Area</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select visit information">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control" name="AUTHORITY_AREA_ID" id="AUTHORITY_AREA_ID" data-placeholder="Select Authority area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select authority area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                if($row->PARENT_ID == $result->AUTHO_ZONE):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->AuthorityAreaID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                                endif;
                                            endforeach; 
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"<?php echo ($result->SHIP_AREA == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select visit information">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="SHIP_ESTABLISHMENT_ID" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                            <?php
                                            foreach ($shipEstablish as $row):
                                                if ($row->AREA_ID == $result->SHIP_AREA):
                                                ?>
                                                <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->AuthorityShipID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                                endif;
                                            endforeach; 
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Name</label>
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'authorityName', "id"=>"authorityName", "class" => "form-control", "value"=> $result->AuthorityName)); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select visit information">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required", "value"=> $result->AuthorityNumber)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number</label>
                                    <div class="col-sm-4" >
                                        <select class="select2 form-control required" name="DAO_ID" id="DAO_ID" data-placeholder="Select DAO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($daoNumber as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"<?php echo ($result->DAOID == $row->DAO_ID) ? 'selected' : '' ?>><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y", strtotime($result->AuthorityDate)))); ?>              
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                    </div>
                    
                    <div class="col-md-12">
                        <hr style="margin-top: 0px;">
                        <table id="sailorTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Official Number</th>
                                    <th>Full Name</th>
                                    <th>Rank</th>
                                    <th>Grade</th>
                                    <th>Marks</th>
                                    <th>Percentage</th>
                                    <th>Result</th>
                                    <th>No of Attempt</th>
                                    <th>Seniority(In Months)</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>          
                                    <td>
                                        <input type="text" value="<?php echo $result->OFFICIALNUMBER ?>" name="OFFICIAL_NO[]" id="OFFICIAL_NO_1"  class="form-control numbersOnly OFFICIAL_NO required" placeholder="Official No" >
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $result->FULLNAME ?>" name="FULLNAME[]" id="FULLNAME_1"  class="form-control required" placeholder="Full Name" >
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $result->RANK_NAME ?>" name="RANK[]" id="RANK_1"  class="form-control required" placeholder="Rank" >
                                    </td> 
                                    <td>
                                        <?php 
                                            echo form_dropdown('GRADE[]', $examGrade, $result->ExamGradeID, 'class = "select2 form-control" id="GRADE_1"');
                                        ?>
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $result->Mark ?>" name="MARKS[]" id="MARKS_1"  class="form-control " placeholder="mark" >
                                    </td>
                                    <td>
                                        <input type="text" value="<?php echo $result->Percentage ?>" name="PERCENTAGE[]" id="PERCENTAGE_1"  class="form-control " placeholder="percentage" >
                                    </td>
                                    <td>
                                        <?php 
                                            echo form_dropdown('RESULT[]', $examResult, $result->ExamResultID, 'class = "select2 form-control" id="RESULT_1"');
                                        ?>
                                    </td>
                                     <td>
                                        <input type="text" value ="<?php echo $result->AttemptNo ?>"name="attempt[]" id="ATTEMPT_1"  class="col-sm-1 form-control required" value="1" >
                                    </td>
                                    <td>
                                        <input type="text" value ="<?php echo $result->ExamSeniority ?>"name="SENIORITY[]" id="SENIORITY_1"  class="form-control " placeholder="seniority" >
                                    </td>
                                    <td class="text-center">                       
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="xh" style="text-align: right;">
                        <span class="btn btn-xs btn-default">
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                            <input type="hidden" name="id" value="<?php echo $result->TranID; ?>">
                            <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/examTestInfo/save':'regularTransaction/examTestInfo/update' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/examTestInfo/index':'regularTransaction/examTestInfo/index'?>" value="Update">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>

<?php $this->load->view("common/sailors_info"); ?>
<script>  
    var authoName = '<?php echo $result->AuthorityName; ?>';
    var authoArea = '<?php echo $result->AuthorityAreaID; ?>';
    var authoShipEst = '<?php echo $result->AuthorityShipID; ?>';
    if(authoName != ''){
        $("#AUTHORITY_ZONE_ID").prop("disabled", true);
        $("#AUTHORITY_AREA_ID").prop("disabled", true);
        
        $("#SHIP_AREA_ID").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID").prop("disabled", true);   
    }
    else if(authoArea != ''){
        $("#authorityName").prop("disabled", true); 
        
        $("#SHIP_AREA_ID").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID").prop("disabled", true);           
    }
    else if(authoShipEst != ''){
        $("#authorityName").prop("disabled", true);  

        $("#AUTHORITY_ZONE_ID").prop("disabled", true);
        $("#AUTHORITY_AREA_ID").prop("disabled", true);          
    }
    $(document).on('change', '#TRAINING_TYPE_ID', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/trainingName_by_trainingType',
            type: 'POST',
            dataType: 'html',
            data: {TRAINING_TYPE: id},
            beforeSend: function () {
                $(".training_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('.training_dropdown').html(data);
            }
        });
        
    });


    $('#sailorTable').removeAttr('width').DataTable( {  
        "scrollX": true,
        "scrollX": true,
        "bPaginate": false,
        "bFilter": false, 
        "bInfo": false,
        "columnDefs": [
            { width: '100px', targets: 0 },
            { width: '100px', targets: 1 },
            { width: '100px', targets: 2 },
            { width: '80px', targets: 3 },
            { width: '80px', targets: 5 },
            { width: '90px', targets: 6 },
            { width: '70px', targets: 7 },
            { width: '90px', targets: 8 },
            { width: '90px', targets: 9 },
        ],
        "fixedColumns": true

    } );
    
    /*$("#authorityDate").prop("disabled", true);
    $("#DAO_ID").prop("disabled", true);*/
</script>