<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var flag = "<?php echo $flag; ?>";
        var tergatUrl;
        if(flag == 1){
            tergatUrl = "Retirement/HistoryTran/examTestInfo/ajaxExamTestList";   /*Retirement sailor url*/ 
        }else{
            tergatUrl = "regularTransaction/examTestInfo/ajaxExamTestList"; /*Active sailor url*/ 
        }
        var dataTable = $('#employee-grid').DataTable({
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>"+tergatUrl, // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }
            },
            /*id attribute add into dataTable*/
            createdRow: function(row, data, dataIndex) {
                var a = $(row).find('td:eq(0)').html();
                $(row).attr("id", 'row_'+a);
            }
        });
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Exam/Test list</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/examTestInfo/create') : site_url('regularTransaction/examTestInfo/create') ?>" title="Add exam/test Informaion">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body contentArea">
                <table id="employee-grid" class="display table-striped " width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Official NO</th>
                            <th>Exam/Test Name</th>
                            <th>Grade</th>
                            <th>Marks</th>
                            <th>Percentage</th>
                            <th>Result</th>
                            <th>Seni-mon</th>
                            <th>Action</th>
                            <th>No. of Attempt</th>
                            <th>Authority info</th>
                            <th>DAO Number</th>
                        </tr>
                    </thead>
                    <tbody>  

                    </tbody>
                </table>             
            </div>
        </div>

    </div>
</div>
<script>
    $(document).on('change', '#VISIT_CI_ID', function(event) {
        event.preventDefault();
        $('#VISIT_ID').select2('val', '');
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/visitInfo_by_visitClass_search',
            type: 'POST',
            dataType: 'html',
            data: {VISIT_CLASS_ID: id},
            beforeSend: function () {
                $(".vistInfo_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('.vistInfo_dropdown').html(data);
            }
        });
        
    });
    /*date picker*/
    $(".jsDatePicker").datepicker();
    /*start date between disabled / enable part*/
    $("#startDate").click(function(event) {
        if ($(this).is(':checked')) {
            $("#fromDate").prop("disabled", false);
            $("#toDate").prop("disabled", false);
        }else{
            $("#fromDate").prop("disabled", true);
            $("#toDate").prop("disabled", true);
        }
    });
    $("#fromDate").prop("disabled", true);
    $("#toDate").prop("disabled", true);
    /*end*/
    $(".formSearch").click(function(event) {
        var allData = $(".frmContent").serialize();
        $.ajax({
            type: "post",
            data: allData,
            dataType: "html",
            url: "<?php echo site_url(); ?>sailorsInfo/foreignVisitInfo/searchForeignVisitInfo",
            beforeSend: function () {
                $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
            },
            success: function (data) {
                $(".contentArea").html(data);                
            }
        });
    });

</script>