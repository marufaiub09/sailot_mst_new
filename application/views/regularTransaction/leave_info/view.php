<div class="panel-body">
    <form class="form-horizontal frmContent" >
        <span class="frmMsg"></span>    
        <div class="col-md-12">
            <fieldset class="">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-6 control-label"><b>Official No</b></label>        
                        <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;"><?php echo $leave->OFFICIALNUMBER ?></label>        
                        
                    </div>                                
                </div>                
            </fieldset>
        </div>
        <div class="col-md-12">
            <fieldset class="">
                <legend  class="legend"></legend>                            
                <div class="col-md-6">
                    
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                        <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;"><b><?php echo $leave->SHIP_NAME; ?></b></label>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="padding-left: 0px;">Leave Option</label>
                        <?php 
                            $leaveOption = array('1' => 'Leave', '2' => 'Only Allowance'); 
                        ?>
                        <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;"><b><?php echo array_key_exists($leave->LeaveOption, $leaveOption)? $leaveOption[$leave->LeaveOption] : ''; ?></b></label>
                        
                    </div> 
                    <div class="form-group">
                       <label class="col-sm-6 control-label" style="padding-left: 0px;">Leave Start Date</label>
                       <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;"><b><?php echo date("d-m-Y", strtotime($leave->StartDate)) ?></b></label>
                       
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="padding-left: 0px;">Leave End Date</label>
                        <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;"><b><?php echo ($leave->EndDate != null )? date("d-m-Y", strtotime($leave->EndDate)) : '' ?></b></label>
                        
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="padding-left: 0px;">Authority Number</label>
                        <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;"><b><?php echo $leave->AuthorityNumber; ?></b></label>
                        
                    </div> 

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Leave Type</label>
                        <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;">
                            <?php 
                               $leaveType = array('0' => 'PL','1' => 'RL','2' => 'ML','3' => 'APL','4' => 'AL');
                               
                           ?>
                          <b><?php echo array_key_exists($leave->LeaveType, $leaveType)? $leaveType[$leave->LeaveType] : ''; ?></b>
                        </label>
                    </div>                                  
                    <div class="form-group">
                        <label class="col-sm-6 control-label">Leave Category</label>
                        <label class="col-sm-6 " style="padding-left: 0px; padding-top: 7px;">
                            <?php 
                               $leavecat = array('0' => 'Normal','1' => 'Advance','2' => 'Extension');
                               
                           ?>
                            <b><?php echo array_key_exists($leave->LeaveCategory, $leavecat)? $leavecat[$leave->LeaveCategory] : ''; ?></b>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="padding-left: 0px;">Leave Cycle Year</label>
                        <label class="col-sm-6" style="padding-left: 0px; padding-top: 7px;">
                            <b><?php echo $leave->CycleYear; ?></b>
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="padding-left: 0px;">Total Leave (Days)</label>
                        <label class="col-sm-6" style="padding-left: 0px; padding-top: 7px;">
                            <b><?php echo $leave->TotalDays; ?></b>
                        </label>
                    </div> 
                    <div class="form-group">
                        <label class="col-sm-6 control-label" style="padding-left: 0px;">Authority Date</label>
                        <label class="col-sm-6" style="padding-left: 0px; padding-top: 7px;">
                            <b><?php echo date("d-m-Y", strtotime($leave->AuthorityDate)); ?></b>
                        </label>
                    </div> 

                </div>
            </fieldset>
        </div>        
        <div class="col-md-12">
            <fieldset class="">
                <div class="col-md-6">                                
                    <h6 class="line-behind-text"><span style="font-size: 14px;">Recall Info</span></h6>
                    <div class="form-group">
                        <label class="col-sm-5 control-label" style="padding-left: 0px;">Recall From Leave</label>
                        <label class="col-sm-6" style="padding-left: 0px; padding-top: 7px;">
                            asd
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label" style="padding-left: 0px;">Recall Date</label>
                        <label class="col-sm-6" style="padding-left: 0px; padding-top: 7px;">
                            asd
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label" style="padding-left: 0px;">Authority Number</label>
                        <label class="col-sm-6" style="padding-left: 0px; padding-top: 7px;">
                            asd
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-5 control-label" style="padding-left: 0px;">Authority Date</label>
                        <label class="col-sm-6" style="padding-left: 0px; padding-top: 7px;">
                            asd
                        </label>
                    </div>

                </div>
                <div class="col-md-6">
                    <h6 class="line-behind-text"><span style="font-size: 14px;">Year Leave Info</span></h6>
                    <table class="table table-striped table-bordered" width="100%" cellspacing="0">
                        <tr>
                            <th>Year</th>
                            <th>Leave (Days)</th>
                        </tr>
                        <tr>
                            <td>                                
                                
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td></td>
                        </tr>
                    </table>
                </div>
            </fieldset>
        </div> 
    </form>
</div>