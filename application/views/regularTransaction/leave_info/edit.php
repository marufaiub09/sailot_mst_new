<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <?php
            //echo $leaveDetails[0]->LeaveYear;
            //echo $leaveDetails[1]->LeaveYear;
            //echo "<pre>";
            //print_r($leaveDetails);
            //exit();
        ?>
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('regularTransaction/leaveInfo/index'); ?>" title="List leave informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Edit Leave Information</center></h3>                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="sailorMainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Official No</label>        
                                    <div class="col-sm-6" >
                                       <?php echo form_input(array('name' => 'officialNo', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number', 'readonly'=>'readonly', 'value' => $result->OFFICIALNUMBER)); ?>           
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Details</label>
                                    <div class="col-sm-5">
                                       <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly', 'value' =>  $result->FULLNAME)); ?>
                                    </div>                                          
                                    <div class="col-sm-4">
                                       <?php echo form_input(array('name' => 'rank',  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly', 'value' =>  $result->RANK_NAME)); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend"></legend>                            
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"  <?php echo ($result->AREA_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <div class="col-sm-6">
                                        <select class="select2 vistInfo_dropdown form-control" name="SHIP_ESTABLISHMENT_ID" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                            <?php
                                            foreach ($shipEstablish as $row):
                                                if ($row->AREA_ID == $result->AREA_ID):
                                                ?>
                                                <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->AuthorityShipID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                                endif;
                                            endforeach; 
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Leave Option</label>
                                    <div class="col-sm-6" >
                                        <?php 
                                           $leaveOption = array('1' => 'Leave','2' => 'Only Allowance');
                                           echo form_dropdown('leaveOption', $leaveOption, $result->LeaveOption, 'class="select2 form-control required" id ="leaveOption"'); 
                                       ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                   <label class="col-sm-5 control-label" style="padding-left: 0px;">Leave Start Date</label>
                                   <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'lvStartDate', "id"=>"lvStartDate", "class" => "datePicker form-control", "value" => date("d-m-Y", strtotime($result->StartDate)))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="padding-left: 0px;">Leave End Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'lvEndDate', "id"=>"lvEndDate", "class" => "form-control", "readonly" => "readonly", "value" => Date("d-m-Y", strtotime("-1 days"))))?>              
                                    </div>
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Leave Type</label>
                                    <div class="col-sm-4" >
                                        <?php 
                                           $leaveType = array('0' => 'PL','1' => 'RL','2' => 'ML','3' => 'APL','4' => 'AL');
                                           echo form_dropdown('leaveType', $leaveType, $result->LeaveType, 'class="select2 form-control required" id ="leaveType"'); 
                                       ?>
                                    </div>
                                </div>                                  
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Leave Category</label>
                                    <div class="col-sm-4" >
                                        <?php 
                                           $leavecat = array('0' => 'None', '1' => 'Normal','2' => 'Advance','3' => 'Extension');
                                           echo form_dropdown('leaveCat', $leavecat, $result->LeaveCategory, 'class="select2 form-control required" id ="leavecat"'); 
                                       ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Leave Cycle Year</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'lvCycleYear', 'type'=> 'number', 'min'=>'1947','max'=>'2099', "id"=>"lvCycleYear", "class" => "required form-control", 'required'=>'required', "value" => $result->CycleYear)); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Total Leave(Days)</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'totalLeave', "id"=>"totalLeave", "class" => "form-control", 'value' => $result->TotalDays)); ?>              
                                    </div>
                                </div>                                                              
                            </div>
                        </fieldset>
                    </div>        
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-5">                                
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="padding-left: 0px;">Authority Number</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required",'required'=>'required', 'value' => $result->AuthorityNumber)); ?>              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control required", 'required'=>'required',"value" => date("d-m-Y", strtotime($result->AuthorityDate)))); ?>              
                                    </div>
                                </div> 
                            </div>
                        </fieldset>
                    </div> 
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-5">                                
                                <h3 class="line-behind-text"><span>Recall Info</span></h3>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="padding-left: 0px;">Recall From Leave</label>
                                    <div class="col-sm-1">                                        
                                        <?php echo  form_checkbox(array('name' => 'recallLeave', "id"=>"recallLeave", 'value'=> TRUE, "class" => "form-control", "checked"=> ($result->IsRecalled == 1)? 'checked': ''));?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="padding-left: 0px;">Recall Date</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'recallDate', "id"=>"recallDate", "class" => "datePicker form-control", "value" => ($result->IsRecalled == 1)? date("d-m-Y", strtotime($result->RecallDate)): date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="padding-left: 0px;">Authority Number</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'reAuthoNo', "id"=>"reAuthoNo", "class" => "form-control", "value" => $result->RecallAuthorityNumber)); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'reAuthoDate', "id"=>"reAuthoDate", "class" => "datePicker form-control", "value" => ($result->IsRecalled == 1)? date("d-m-Y", strtotime($result->RecallAuthorityDate)): date("d-m-Y"))); ?>              
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <h3 class="line-behind-text"><span>Year Leave Info</span></h3>
                                <table class="table table-striped table-bordered" width="100%" cellspacing="0">
                                    <tr>
                                        <th>Year</th>
                                        <th>Leave (Days)</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span id="leaveIntervel1"></span>
                                            <input type="hidden" name="firstYear" class="leaveIntervel1" value="">
                                        </td>
                                        <td><input type="text" name="leaveOne" id="leaveOne" size="5%" value="<?php echo ($result->LeaveType == 0 || $result->LeaveType == 3 )? $leaveDetails[0]->LeaveDays : ''; ?>"></td>
                                    </tr>                                    
                                    <tr>
                                        <td>
                                            <span id="leaveIntervel2"></span>
                                            <input type="hidden" name="secondYear" class="leaveIntervel2" value="">
                                        </td>
                                        <td><input type="text" name="leaveTwo" id="leaveTwo" size="5%"  value="<?php echo ($result->LeaveType == 0 || $result->LeaveType == 3 )? $leaveDetails[1]->LeaveDays : ''; ?>"></td>
                                    </tr>                                    
                                </table>
                            </div>
                        </fieldset>
                    </div> 

                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                            <input type="hidden" name="id" value="<?php echo $result->LeaveID; ?>">
                            <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/leaveInfo/update" data-redirect-action="regularTransaction/leaveInfo/index" value="Update">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
   .line-behind-text span {
        font-size: 14px !important;
    }
    .line-behind-text::before {
        top: 60%;
        border-top: 1px solid #e5e5e5 !important;
    }
</style>

<?php $this->load->view("common/sailors_info"); ?>
<script> 
    var leaveType = "<?php echo $result->LeaveType; ?>";
    var isRecalled = "<?php echo $result->IsRecalled; ?>";
    if( leaveType == 0 || leaveType == 3){
        $("#leaveOne").prop("disabled", false);
        $("#leaveTwo").prop("disabled", false);
    }else{
        $("#leaveOne").prop("disabled", true);
        $("#leaveTwo").prop("disabled", true);
    }
    $(document).ready(function($) {
        var dateStr = $("#lvStartDate").val();
        dArr = dateStr.split("-"); 
        var currentYear = dArr[2];
        var preYear = parseInt(dArr[2]) - 1;
        $("#leaveIntervel1").text(preYear);
        $("#leaveIntervel2").text(currentYear);
        $(".leaveIntervel1").val(preYear);
        $(".leaveIntervel2").val(currentYear);  
    });

    $(document).on('change', '#lvStartDate', function(event) {
        event.preventDefault();
        var leaveType = $("#leaveType").val();
        var dateStr = $(this).val();
        dArr = dateStr.split("-"); 
        var currentYear = dArr[2];
        if(leaveType == 0){
            var preYear = parseInt(dArr[2]) - 1;
            $("#leaveIntervel1").text(preYear);
            $("#leaveIntervel2").text(currentYear);  
            $(".leaveIntervel1").val(preYear);
            $(".leaveIntervel2").val(currentYear);  

        }else if(leaveType == 3){
            var nextYear = parseInt(dArr[2]) + 1;
            $("#leaveIntervel1").text(currentYear);
            $("#leaveIntervel2").text(nextYear);
            $(".leaveIntervel1").val(currentYear);
            $(".leaveIntervel2").val(nextYear);    
        }
    });

    $(document).on('change', '#VISIT_CI_ID', function(event) {
        event.preventDefault();
        $('#VISIT_ID').select2('val', '');
        var id = $(this).val();
    });
    /*default disable LeaveType equal to RL*/
    $("#leaveOption").prop("disabled", true);
    $("#totalLeave").prop("disabled", true);
    $("#lvCycleYear").prop("disabled", true);
    /*end disable part*/
    $("#leaveOption").change(function(event) {
        if ($(this).val() == 1) {
            $("#totalLeave").prop("disabled", false);
        }else{
            $("#totalLeave").prop("disabled", true);
            $("#totalLeave").val('');
        }
    });
    $("#leaveType").change(function(event) {
        if ($(this).val() == 0 || $(this).val() == 3 ) {
            $("#leaveOption").prop("disabled", true);
            $("#totalLeave").prop("disabled", true);
            $("#lvCycleYear").prop("disabled", true);
            $("#leavecat").prop("disabled", false);
            
            $("#leaveOne").prop("disabled", false);
            $("#leaveTwo").prop("disabled", false);
            $("#leaveOne").val('');
            $("#leaveTwo").val('');

            var leaveType = $(this).val();
            var dateStr = $("#lvStartDate").val();
            dArr = dateStr.split("-"); 
            var currentYear = dArr[2];
            if(leaveType == 0){
                var preYear = parseInt(dArr[2]) - 1;
                $("#leaveIntervel1").text(preYear);
                $("#leaveIntervel2").text(currentYear); 
                $(".leaveIntervel1").val(preYear);
                $(".leaveIntervel2").val(currentYear); 

            }else if(leaveType == 3){
                var nextYear = parseInt(dArr[2]) + 1;
                $("#leaveIntervel1").text(currentYear);
                $("#leaveIntervel2").text(nextYear);   
                $(".leaveIntervel1").val(currentYear);
                $(".leaveIntervel2").val(nextYear);   

            }
        }else if($(this).val() == 1){
            $("#leavecat").prop("disabled", true);
            $("#leaveOption").prop("disabled", false);
            $("#totalLeave").prop("disabled", false);
            $("#lvCycleYear").prop("disabled", false);
            $("#leaveOne").prop("disabled", true);
            $("#leaveTwo").prop("disabled", true);
            $("#leaveOne").val('');
            $("#leaveTwo").val('');
        }else if($(this).val() == 2 || $(this).val() == 4 ){
            $("#leavecat").prop("disabled", true);
            $("#leaveOption").prop("disabled", true);
            $("#lvCycleYear").prop("disabled", true);
            $("#totalLeave").prop("disabled", false);
            $("#leaveOne").prop("disabled", true);
            $("#leaveTwo").prop("disabled", true);
            $("#leaveOne").val('');
            $("#leaveTwo").val('');
        }
    });

    $("#recallLeave").click(function(event) {
        if ($(this).is(':checked')) {
            $("#recallDate").prop("disabled", false);
            $("#reAuthoNo").prop("disabled", false);
            $("#reAuthoDate").prop("disabled", false);
        }else{
            $("#recallDate").prop("disabled", true);
            $("#reAuthoNo").prop("disabled", true);
            $("#reAuthoDate").prop("disabled", true);
        }
    });
    if(isRecalled == 0){
        $("#recallDate").prop("disabled", true);
        $("#reAuthoNo").prop("disabled", true);
        $("#reAuthoDate").prop("disabled", true);        
    }

    //$("#lvEndDate").prop("disabled", true);
    /*leave calculation*/
    $(document).on('blur', '#totalLeave', function(event) {
        event.preventDefault();
        var sDate = $("#lvStartDate").val();
        dArr = sDate.split("-"); 
        var startDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];  /*Date formate change*/
        var days = $(this).val();
        if(days != ''){
            var someDate = new Date(startDate);
            someDate.setDate((someDate.getDate() - 1)+ parseInt(days)); 
            var dd = someDate.getDate();
            var mm = someDate.getMonth() + 1;
            var y = someDate.getFullYear();
            if(dd.toString().length == 1){
                dd = "0"+dd;
            }if(mm.toString().length == 1){
                mm = "0"+mm;
            }
            var someFormattedDate = dd + '-'+ mm + '-'+ y;
            $('#lvEndDate').val(someFormattedDate);            
        }
    });
    $(document).on('blur', '#lvStartDate', function(event) {
        event.preventDefault();

        var days = $("#totalLeave").val();        
        var firstLeave = $("#leaveOne").val();
        var secondLeave = $("#leaveTwo").val();
        if(firstLeave == ''){
            firstLeave = 0;
        }if(secondLeave == ''){
            secondLeave = 0;
        }if(days == ''){
            days = 0;
        }
        var sDate = $("#lvStartDate").val();
        dArr = sDate.split("-"); 
        var startDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];  /*Date formate change*/
        
        var someDate = new Date(startDate);
        someDate.setDate((someDate.getDate() - 1) + parseInt(days) + parseInt(firstLeave) + parseInt(secondLeave)); 
        var dd = someDate.getDate();
        var mm = someDate.getMonth() + 1;
        var y = someDate.getFullYear();
        if(dd.toString().length == 1){
            dd = "0"+dd;
        }if(mm.toString().length == 1){
            mm = "0"+mm;
        }
        var someFormattedDate = dd + '-'+ mm + '-'+ y;
        if(firstLeave != ''){
            $('#lvEndDate').val(someFormattedDate);                        
        }
    });

    $(document).on('blur', '#leaveOne', function(event) {
        event.preventDefault();
        var firstLeave = $("#leaveOne").val();
        var secondLeave = $("#leaveTwo").val();
        if(secondLeave == ''){
            secondLeave = 0;
        }
        var sDate = $("#lvStartDate").val();
        dArr = sDate.split("-"); 
        var startDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];  /*Date formate change*/
        
        var someDate = new Date(startDate);
        someDate.setDate((someDate.getDate() - 1) + parseInt(firstLeave) + parseInt(secondLeave)); 
        var dd = someDate.getDate();
        var mm = someDate.getMonth() + 1;
        var y = someDate.getFullYear();
        if(dd.toString().length == 1){
            dd = "0"+dd;
        }if(mm.toString().length == 1){
            mm = "0"+mm;
        }
        var someFormattedDate = dd + '-'+ mm + '-'+ y;
        if(firstLeave != ''){
            $('#lvEndDate').val(someFormattedDate);                        
        }
        
    });
    $(document).on('blur', '#leaveTwo', function(event) {
        event.preventDefault();
        var firstLeave = $("#leaveOne").val();
        var secondLeave = $("#leaveTwo").val();
        if(firstLeave == ''){
            firstLeave = 0;
        }
        var sDate = $("#lvStartDate").val();
        dArr = sDate.split("-"); 
        var startDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];  /*Date formate change*/
        
        var someDate = new Date(startDate);
        someDate.setDate((someDate.getDate() - 1) + parseInt(firstLeave) + parseInt(secondLeave)); 
        var dd = someDate.getDate();
        var mm = someDate.getMonth() + 1;
        var y = someDate.getFullYear();
        if(dd.toString().length == 1){
            dd = "0"+dd;
        }if(mm.toString().length == 1){
            mm = "0"+mm;
        }
        var someFormattedDate = dd + '-'+ mm + '-'+ y;
        if(secondLeave != ''){
            $('#lvEndDate').val(someFormattedDate);            
        }
        
    });
    

</script>