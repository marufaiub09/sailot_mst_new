<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Leave Info Search </h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <div class="col-md-12">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Official Number</label>
                                <div class="col-sm-3">
                                    <?php echo form_input(array('name' => 'officialNumber', 'id' => 'officialNoLeaveInfo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>
                                </div>
                                <div class="col-md-1"><span class="smloadingImg"></span></div> 
                                <label class="col-md-5 control-label"> <div class="text-denger"><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div><span class="fullName"></span><span class="rank"></span></label>
                            </div>
                        </div>                      
                    </div>
                </form>              
            </div>
        </div>
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Leave Information</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs "  href="<?php echo site_url('regularTransaction/leaveInfo/create'); ?>" title="Add leave Informaion">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">
                <table id="leaveInfo" class="stripe row-border order-column" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Official NO</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Total Days</th>
                            <th>Leave Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="contentArea">  

                    </tbody>
                </table>             
            </div>
        </div>

    </div>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        
        $('#leaveInfo').removeAttr('width').DataTable( {  
            "bPaginate": false,
            "bFilter": false, 
            "bInfo": false,
            "columnDefs": [
                { width: '20px', targets: 0 },
                { width: '100px', targets: 1 },
                { width: '100px', targets: 2 },
                { width: '80px', targets: 3 },
                { width: '100px', targets: 4 },
                { width: '90px', targets: 5 },
                { width: '100px', targets: 6 },
            ],
            "fixedColumns": true,
            "ordering": false

        } );
        $('.dataTables_empty').empty();
    });
    /*start search sailorInfo, rank_name by Official Number */
    $("#officialNoLeaveInfo").on('blur', function(){
        var officeNumber = $(this).val();
        var flag = "<?php echo $flag; ?>";
        var sailorStatus = (flag == 0 ? '1' : '3'); 
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:sailorStatus},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailor",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").text(data['SAILORID']);
                        $(".fullName").text(data['FULLNAME']);
                        $(".rank").text(', Rank: '+data['RANK_NAME']);  
                        $(".alertSMS").html('');
                        
                        $.ajax({
                            type: "post",
                            data: {sailorId: data['SAILORID'], sailorStatus:1},
                            dataType: "json",
                            url: "<?php echo site_url(); ?>regularTransaction/leaveInfo/searchLeaveInfo",
                            beforeSend: function () {
                                $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                            },
                            success: function (data) {
                                $(".smloadingImg").html("");
                                var TableRow;
                                var sn = 1;
                                $.each(data, function(index, leave) {
                                    var cat ;
                                    var view = '<?php echo site_url("regularTransaction/leaveInfo/view")?>/'+leave.LeaveID;
                                    var edit = '<?php echo site_url("regularTransaction/leaveInfo/edit")?>/'+leave.LeaveID;                                    
                                    var link = '<a class="btn btn-success btn-xs modalLink" href="'+view+'" title="View leave info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ';                                    
                                    var link2 = '<a class="btn btn-warning btn-xs" href="'+edit+'" title="Edit Leave Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ';
                                    var link3 = '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'+leave.LeaveID+'" sn="'+sn+'" title="Click For Delete" data-type="delete" data-field="LeaveID" data-tbl="leave"><span class="glyphicon glyphicon-trash"></span></a>';
                                    if(leave.LeaveType == 0){
                                        cat = "PL";
                                    }else if (leave.LeaveType == 1) {
                                        cat = "RL";
                                    }else if (leave.LeaveType == 2) {
                                        cat = "ML";
                                    }else if (leave.LeaveType == 3) {
                                        cat = "APL";
                                    }else if (leave.LeaveType == 4) {
                                        cat = "AL";
                                    }


                                    TableRow += "<tr id =row_"+sn+">";
                                    TableRow += "<td>" + sn++ + "</td>"+
                                                "<td>" + leave.OFFICIALNUMBER + "</td>"+
                                                "<td>" + leave.startDT + "</td>"+
                                                "<td>" + leave.endDT + "</td>"+
                                                "<td>" + leave.TotalDays + "</td>"+
                                                "<td>" + cat + "</td>"+
                                                "<td>" + link + link2 + link3+ "</td>";
                                    TableRow += "</tr>";
                                    
                                });
                                $(".contentArea").html(TableRow);
                            }
                        });             
                    }else{
                        $(".sailorId").text('');
                        $(".fullName").text('');
                        $(".rank").text('');      
                        $(".alertSMS").html('Invalid Officer Number');  
                        $("#leaveInfo tbody tr").empty();    
                    }
                }
            });            
        }else{
            $(".sailorId").text('');
            $(".fullName").text('');
            $(".rank").text(''); 
            $(".alertSMS").html(''); 
            $("#leaveInfo tbody tr").empty();    
        }     
       
    });
    /*leave deails*/
   /* $.ajax({
        type: "post",
        data: {leaveId: leave.LeaveID},
        dataType: "json",
        url: "<?php echo site_url(); ?>regularTransaction/leaveInfo/searchLeaveDetails",
        success: function (data2) {
            var count = data2.length;
            
            $.each(data2, function(index, leaveDT) {
                if(count == 1){
                    yr = leaveDT.LeaveYear;
                }else if(count == 2){
                    yr += leaveDT.LeaveYear;
                    if(index == 0)
                        yr += "-";
                }
                //alert(yr);  
            });
        }
    });*/
    /*end sailor details*/
</script>
