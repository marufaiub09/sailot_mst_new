<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/movementHistory/index') : site_url('regularTransaction/movementHistory/index') ?>" title="List Movement informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Add Movement Information</center></h3>                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Current Info</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php $id = ($flag == 1)? 'retofficialNumber':'officialNumber'; ?>
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => $id, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Posting Unit</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'PostingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Draft Info</legend>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ship Establishment</label>
                                    <div class="col-sm-7" >
                                        <select class="select2 form-control required" name="SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT" data-placeholder="Select ship establishment" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship establishment</option>
                                            <?php
                                            foreach ($shipEst as $row):
                                                ?>
                                                <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship establishment">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Posting Unit</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select posting unit">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 vistInfo_dropdown form-control" name="POSTING_UNIT_ID" id="POSTING_UNIT_ID" data-placeholder="Select posting unit" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select posting unit</option>
                                        </select>               
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Appointment Type</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="APPOINTMENT_TYPE" id="APPOINTMENT_TYPE" data-placeholder="Select Appointment type" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select appointment type</option>
                                            <?php
                                            foreach ($appointment as $row):
                                                ?>
                                                <option value="<?php echo $row->APPOINT_TYPEID ?>"><?php echo $row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select appointment type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Draft in Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Draft in date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'draftInDate', "id"=>"draftInDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Draft Out Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Draf out date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'draftOutDate', "id"=>"draftOutDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Authority Information</legend>    
                            <div class="col-md-6">                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Order Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'orderNumber', "id"=>"orderNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Remarks</label>
                                   <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'remarks', "id"=>"remarks", "class" => "form-control", "rows" =>2, "cols" =>2)); ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="AUTHO_SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                        </select>               
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Order Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'orderDate', "id"=>"orderDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/movementHistory/save':'regularTransaction/movementHistory/save' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/movementHistory/index':'regularTransaction/movementHistory/index'?>" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>