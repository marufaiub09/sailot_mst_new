<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "
                           href="<?php echo site_url('regularTransaction/AcademicInfo/index'); ?>"
                           title="List Sailor informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Edit Academic Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>
                    <div class="form-group col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Current Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Official Number</label>        
                                    <div class="col-sm-5" >
                                        <?php echo form_input(array('name' => 'officialNo', 'value' => $results->OFFICIALNUMBER, "class" => "form-control", 'placeholder' => 'Official Number', 'readonly' => 'readonly')); ?>
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                            <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Posting Unit</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control PostingUnit",  'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => $results->postingUnit)); ?>
                                    </div>
                                </div>
                                <div class="form-group hidden">
                                    <label class="col-sm-5 control-label">Sailor Id</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'sailorId', "class" => "form-control sailorId",  'readonly' => 'readonly', 'value' => set_value('sailorId'))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName ",  'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => $results->FULLNAME)); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank ", 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => $results->RANK_NAME)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => $results->POSTINGDATE)); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="form-group col-md-12">
                        <legend class="legend">Academic Information:</legend>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-md-4">Exam System</label>
                                <div class="col-md-6">
                                    <select name="examSystem" id="examSystem"
                                            class="form-control required select2"
                                            data-placeholder="Select Exam System" aria-hidden="true"
                                            data-allow-clear="true" style="width: 100%;">
                                        <option value="">Select Exam System</option>
                                        <option value="1" <?php echo ($results->EvaluationSystem == 1) ? 'selected' : '' ?>>Traditional</option>
                                        <option value="2" <?php echo ($results->EvaluationSystem == 2 ) ? 'selected' : '' ?>>GPA</option>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Exam System">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-3">Exam Name</label>
                                <div class="col-md-8">
                                    <select class="select2 form-control required" name="examName" id="examId" data-placeholder="Select Exam Name" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Exam Name</option>
                                        <?php
                                        foreach ($exam_nameData as $row):
                                            ?>
                                            <option value="<?php echo $row->GOVT_EXAMID ?>" <?php echo ($results->ExamID == $row->GOVT_EXAMID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Exam Name">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-md-4">Subject/Group</label>
                                <div class="col-md-6">
                                    <select class="select2 form-control required" name="subjectGroup" id="subjectGroupId" data-placeholder="Select Subject/Group" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Exam Name</option>
                                        <?php
                                        foreach ($subject_group as $row):
                                            ?>
                                            <option value="<?php echo $row->SUBJECT_GROUPID ?>" <?php echo ($results->SubjectID == $row->SUBJECT_GROUPID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Subject/Group Name">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-3">Passing Year</label>

                                <div class="col-md-8">
                                    <select name="passingYear" id="passingYearid"
                                            class="form-control required select2"
                                            data-placeholder="Select Passing Year" aria-hidden="true"
                                            data-allow-clear="true" style="width: 100%;">
                                        <option>Select Year</option>
                                        <?php
                                        for ($i = date("Y"); $i >= 1950; $i--) {
                                            ?>
                                            <option value="<?php echo $i; ?>" <?php echo ($results->PassingYear == $i) ? 'selected' : '' ?>><?php echo $i ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Passing Year">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-md-4">Board/University </label>

                                <div class="col-md-6">
                                    <select class="select2 form-control required" name="boardUniversity" id="boardUniversity" data-placeholder="Select Board/University" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Exam Name</option>
                                        <?php
                                        foreach ($board_university as $row):
                                            ?>
                                            <option value="<?php echo $row->BOARD_CENTERID ?>" <?php echo ($results->BoardID == $row->BOARD_CENTERID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Exam Name">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-3">Result</label>
                                <div class="col-md-8">
                                    <select class="select2 form-control required" name="result" id="resultId" data-placeholder="Select Result" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Exam Name</option>
                                        <?php
                                        foreach ($result as $row):
                                            ?>
                                            <option value="<?php echo $row->EXAM_GRADEID ?>" <?php echo ($results->ExamGradeID == $row->EXAM_GRADEID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                            <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Result">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6" id="gpadiv">
                                <label class="col-md-4">GPA</label>

                                <div class="col-md-4">
                                    <?php echo form_input(array('name' => 'gpa', 'id' => 'gpa', 'type' => 'text', "class" => "form-control numbersOnly ", 'placeholder' => 'Enter GPA', 'value' => $results->TotalMarks)); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select GPA">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div id="traditional1" class="col-md-6" style="display:none">
                                <label class="col-md-4">Traditional</label>

                                <div class="col-md-6">
                                    <?php echo form_input(array('name' => 'tradition', 'id' => 'traditional', 'type' => 'text', "class" => "form-control numbersOnly ", 'placeholder' => 'Enter Traditional', 'value' => $results->TotalMarks)); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select GPA">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>


                            <div class="col-md-6">
                                <label class="col-md-3">Percentage</label>

                                <div class="col-md-5">
                                    <?php echo form_input(array('name' => 'percent', 'id' => 'percent', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Percentage', 'value' => $results->Percentage)); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Percentage">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-sm-4">Result Description</label>

                                <div class="col-md-6">
                                    <?php echo form_input(array('name' => 'resultdesc', 'id' => 'resultDesc', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Result Description', 'value' => $results->ResultDescription)); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please Enter Result Description">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-3">Education Status</label>

                                <div class="col-md-8">
                                    <select id="eduStatusDesc" name="eduStatusDesc" class="select2 form-control"
                                            data-placeholder="Select Education Status" aria-hidden="true"
                                            data-allow-clear="true" style="width: 100%;">
                                        <option value="">Select Education Status</option>
                                        <option value="1" <?php echo ($results->EducationStatus == 1) ? 'selected' : '' ?>>Entry Education</option>
                                        <option value="2" <?php echo ($results->EducationStatus == 2) ? 'selected' : '' ?>>Entry Period Highest</option>
                                        <option value="3" <?php echo ($results->EducationStatus == 3) ? 'selected' : '' ?>>Last</option>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Select Education Status">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="<?php echo $results->AcademicID; ?>">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/AcademicInfo/update"  value="Update">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('keyup', '.numbersOnly', function () {
            var val = $(this).val();
            if (isNaN(val)) {
                val = val.replace(/[^0-9\.]/g, '');
                if (val.split('.').length > 2) {
                    val = val.replace(/\.+$/, "");
                }
            }
            $(this).val(val);
        });

    });
    $(document).ready(function () {
        $("#examSystem").change(function () {
            $(this).find("option:selected").each(function () {
                if ($(this).attr("value") == "1") {
                    $("#gpadiv").hide();
                    $("#traditional1").show();
                }
                else if ($(this).attr("value") == "2") {
                    $("#traditional1").hide();
                    $("#gpadiv").show();
                }
                else {
                    $("#traditional1").hide();
                }
            });
        }).change();
    });
</script>