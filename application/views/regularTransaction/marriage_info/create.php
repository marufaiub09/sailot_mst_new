<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/marriageInfo/index') : site_url('regularTransaction/marriageInfo/index') ?>" title="List marriage informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Marriage Information</center></h3>
                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php $id = ($flag == 1)? 'retofficialNumber':'officialNumber'; ?>
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => $id, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Posting Unit</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'PostingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend"></legend>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Spouse Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter spouse name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'spouseName', "id"=>"spouseName", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Spouse Name (বাংলা)</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter spouse name bangla">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'spouseNameBN', "id"=>"spouseNameBN", "class" => "form-control")); ?>
                                    </div>
                                </div>      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Marriage Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter marriage date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'marriageDate', "id"=>"marriageDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="padding-left: 0px;"></label>
                                <div class="col-sm-1">
                                    <?php echo  form_checkbox(array('name' => 'marriageRate', "id"=>"marriageRate", "class" => "form-control", 'value' => 1));?>              
                                </div>
                                <label class="col-sm-4 control-label" style="padding-left: 0px;">Marriage Rate of Pay</label>                                    
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <?php echo  form_checkbox(array('name' => 'marriageNextOfKin', "id"=>"marriageNextOfKin", "class" => "form-control", 'value' => 1));?>              
                                </div>
                                <label class="col-sm-5 control-label" style="padding-left: 0px;">Is this Marriage Next of Kin</label>                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend"></legend>    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="SHIP_ESTABLISHMENT_ID" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">DAO Number</label>
                                    <div class="col-sm-4" >
                                        <select class="select2 form-control required" name="DAO_ID" id="DAO_ID" data-placeholder="Select DAO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($daoNumber as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/marriageInfo/save':'regularTransaction/marriageInfo/save' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/marriageInfo/index':'regularTransaction/marriageInfo/index'?>" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>