<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('regularTransaction/marriageInfo/index'); ?>" title="List marriage informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Marriage Information</center></h3>

                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'value' => $result->OFFICIALNUMBER, 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number', 'readonly' => 'readonly')); ?>
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                            <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Posting Unit</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'PostingUnit', 'value' => $result->POSTING_UNIT_NAME, "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', 'value' => $result->FULLNAME, "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly')); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', 'value' => $result->RANK_NAME, "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'PostingDate', 'value' => date('d-m-Y', strtotime($result->POSTINGDATE)), "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend"></legend>
                            <div class="form-group" id="marriageStatusDiv">
                                <label class="col-md-2 control-label">Merital Status</label>
                                <div class="col-md-8">
                                    <label class="radio-inline"> &nbsp;&nbsp; <?php echo form_radio(array('name' => 'marriageStatus', "id" => "EditMarriage", 'value' => 1, "checked" => ($result->MaritalStatus == 1) ? TRUE : FALSE)); ?>  Edit Marriage</label>
                                    <label class="radio-inline"> <?php echo form_radio(array('name' => 'marriageStatus', "id" => "separation", 'value' => 2, "checked" => ($result->MaritalStatus == 2) ? TRUE : FALSE)); ?>  Separation</label>
                                    <label class="radio-inline"> <?php echo form_radio(array('name' => 'marriageStatus', "id" => "death", 'value' => 3, "checked" => ($result->MaritalStatus == 3) ? TRUE : FALSE)); ?>  Death</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Spouse Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter spouse name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'spouseName', "id" => "spouseName", "class" => "form-control required", "value" => $result->SpouseName)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Spouse Name (বাংলা)</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter spouse name bangla">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'spouseNameBN', "id" => "spouseNameBN", "class" => "form-control", "value" => $result->SpouseNameBangla)); ?>
                                    </div>
                                </div>      
                            </div>
                            <div class="col-md-6">
                                <div class="form-group marriageInfo">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Marriage Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter marriage date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'marriageDate', "id" => "marriageDate", "class" => "datePicker form-control", "value" => date("d-m-Y", strtotime($result->MarriageDate)))); ?>              
                                    </div>
                                </div>
                                <div class="form-group separationInfo">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Separation Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter separation date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'separationDate', "id" => "separationDate", "class" => "datePicker form-control", "value" => ($result->MaritalStatus == 2) ? date("d-m-Y", strtotime($result->Date)) : date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group deathInfo">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Death Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter death date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'deathDate', "id" => "deathDate", "class" => "datePicker form-control", "value" => ($result->MaritalStatus == 3) ? date("d-m-Y", strtotime($result->Date)) : date("d-m-Y"))); ?>              
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label" style="padding-left: 0px;"></label>
                                <div class="col-sm-1">
                                    <?php echo form_checkbox(array('name' => 'marriageRate', "id" => "marriageRate", "class" => "form-control", 'value' => $result->IsMarriageRatesofPay, 'checked' => (!empty($result->IsMarriageRatesofPay)) ? TRUE : FALSE)); ?>
                                </div>
                                <label class="col-sm-4 control-label" style="padding-left: 0px;">Marriage Rate of Pay</label>                                    
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-sm-1">
                                    <?php echo form_checkbox(array('name' => 'marriageNextOfKin', "id" => "marriageNextOfKin", "class" => "form-control", 'value' => $result->IsThisMarriageNextOfKin, 'checked' => (!empty($result->IsThisMarriageNextOfKin)) ? TRUE : FALSE)); ?>
                                </div>
                                <label class="col-sm-5 control-label" style="padding-left: 0px;">Is this Marriage Next of Kin</label>                                    
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 marriageInfo" >
                        <fieldset class="">
                            <legend  class="legend"></legend>    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id" => "authorityNumber", "class" => "form-control required", "value" => $result->MarriageAuthorityNumber)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->AREA_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo "[" . $row->CODE . "] " . $row->NAME ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="SHIP_ESTABLISHMENT_ID" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                            <?php
                                            foreach ($authorityShipEst as $row):
                                                if ($row->AREA_ID == $result->AREA_ID):
                                                    ?>
                                                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->MarriageShipEstablishmentID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo "[" . $row->CODE . "] " . $row->NAME ?></option>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id" => "authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y", strtotime($result->MarriageAuthorityDate)))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">DAO Number</label>
                                    <div class="col-sm-4" >
                                        <select class="select2 form-control required" name="DAO_ID" id="DAO_ID" data-placeholder="Select DAO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($daoNumber as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>" <?php echo ($result->MarriageDAOID == $row->DAO_ID) ? 'selected' : '' ?>><?php echo $row->DAO_NO ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </fieldset>
                    </div>  
                    <div class="col-md-12 separationInfo" >
                        <fieldset class="">
                            <legend  class="legend"></legend>    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'authorityNumber_SEP', "id" => "authorityNumber_SEP", "class" => "form-control required", "value" => ($result->MaritalStatus == 2) ? $result->AuthorityNumber : '')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control" name="SHIP_AREA_ID_SEP" id="SHIP_AREA_ID_SEP" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->MaritalStatus == 2) ? (($result->AREA_ID == $row->ADMIN_ID) ? 'selected' : '') : '' ?>><?php echo "[" . $row->CODE . "] " . $row->NAME ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="SHIP_ESTABLISHMENT_ID_SEP" id="SHIP_ESTABLISHMENT_ID_SEP" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                            <?php
                                            foreach ($authorityShipEst as $row):
                                                if ($row->AREA_ID == $result->AREA_ID):
                                                    ?>
                                                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->MaritalStatus == 2) ? (($result->MarriageShipEstablishmentID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '') : '' ?>><?php echo "[" . $row->CODE . "] " . $row->NAME ?></option>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Separation Cause</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter separation cause">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <?php echo form_textarea(array('name' => 'separationCause', "id" => "separationCause", "rows" => 2, "cols" => 2, "class" => "form-control", "value" => ($result->MaritalStatus == 2) ? $result->SeparationCause : '')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate_SEP', "id" => "authorityDate_SEP", "class" => "datePicker form-control", "value" => ($result->MaritalStatus == 2) ? date("d-m-Y", strtotime($result->AuthorityDate)) : date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">DAO Number</label>
                                    <div class="col-sm-4" >
                                        <select class="select2 form-control required" name="DAO_ID_SEP" id="DAO_ID_SEP" data-placeholder="Select DAO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($daoNumber as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>" <?php echo ($result->MaritalStatus == 2) ? (($result->MarriageDAOID == $row->DAO_ID) ? 'selected' : '') : '' ?>><?php echo $row->DAO_NO ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </fieldset>
                    </div>  
                    <div class="col-md-12 deathInfo" >
                        <fieldset class="">
                            <legend  class="legend"></legend>    
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'authorityNumber_D', "id" => "authorityNumber_D", "class" => "form-control required", "value" => ($result->MaritalStatus == 3) ? $result->AuthorityNumber : '')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID_D" id="SHIP_AREA_ID_D" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->MaritalStatus == 3) ? (($result->AREA_ID == $row->ADMIN_ID) ? 'selected' : '') : '' ?>><?php echo "[" . $row->CODE . "] " . $row->NAME ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="SHIP_ESTABLISHMENT_ID_D" id="SHIP_ESTABLISHMENT_ID_D" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                            <?php
                                            foreach ($authorityShipEst as $row):
                                                if ($row->AREA_ID == $result->AREA_ID):
                                                    ?>
                                                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->MaritalStatus == 3) ? (($result->MarriageShipEstablishmentID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '') : '' ?>><?php echo "[" . $row->CODE . "] " . $row->NAME ?></option>
                                                    <?php
                                                endif;
                                            endforeach;
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate_D', "id" => "authorityDate_D", "class" => "datePicker form-control", "value" => ($result->MaritalStatus == 3) ? date("d-m-Y", strtotime($result->AuthorityDate)) : date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">DAO Number</label>
                                    <div class="col-sm-4" >
                                        <select class="select2 form-control required" name="DAO_ID_D" id="DAO_ID_D" data-placeholder="Select DAO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($daoNumber as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>" <?php echo ($result->MaritalStatus == 3) ? (($result->MarriageDAOID == $row->DAO_ID) ? 'selected' : '') : '' ?>><?php echo $row->DAO_NO ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </fieldset>
                    </div>  

                    <div class="col-md-12 ">   
                        <div class="col-sm-2"></div>
                        <div class="form-group">                             
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="<?php echo $result->MarriageID; ?>">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/marriageInfo/update':'regularTransaction/marriageInfo/update' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/marriageInfo/index':'regularTransaction/marriageInfo/index'?>" value="Update">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    /*authority area ship search by authority area: ADMIN_TYPE: 2*/
    $(document).on('change', '#SHIP_AREA_ID_SEP', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/shipEstablishment_by_area',
            type: 'POST',
            dataType: 'html',
            data: {SHIP_AREA_ID: id},
            beforeSend: function () {
                $("#SHIP_ESTABLISHMENT_ID_SEP").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#SHIP_ESTABLISHMENT_ID_SEP').html(data);
                $('#SHIP_ESTABLISHMENT_ID_SEP').select2('val', '');
            }
        });
        
    });
    /*authority area ship search by authority area: ADMIN_TYPE: 2*/
    $(document).on('change', '#SHIP_AREA_ID_D', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/shipEstablishment_by_area',
            type: 'POST',
            dataType: 'html',
            data: {SHIP_AREA_ID: id},
            beforeSend: function () {
                $("#SHIP_ESTABLISHMENT_ID_D").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#SHIP_ESTABLISHMENT_ID_D').html(data);
                $('#SHIP_ESTABLISHMENT_ID_D').select2('val', '');
            }
        });
        
    });
    $(document).ready(function() {
        $("#marriageStatusDiv").change(function(){
            var selectedVal = "";
            var selected = $("#marriageStatusDiv input[type='radio'][name = 'marriageStatus']:checked");
            selectedVal = selected.val();
            if(selectedVal == 1){
                marriageInfo();
            }else if(selectedVal == 2){
                separationInfo();
            }else{
                deathInfo();
            }

        });
    });
    var marriageStatus = "<?php echo $result->MaritalStatus; ?>";
    if(marriageStatus == 1){
        marriageInfo();
    }else if(marriageStatus == 2){
        separationInfo();
    }else{
        deathInfo();
    }
    function marriageInfo(){
        $(".select2-container").css({"width":"100%"});
        $(".marriageInfo").show();
        $(".separationInfo").hide();
        $(".deathInfo").hide();
        
        $("#marriageDate").prop("disabled", false);
        $("#separationDate").prop("disabled", true);
        $("#deathDate").prop("disabled", true);

        $("#authorityNumber").prop("disabled", false);
        $("#authorityNumber").addClass('required')
        $("#SHIP_AREA_ID").prop("disabled", false);
        $("#SHIP_AREA_ID").addClass('required')
        $("#SHIP_ESTABLISHMENT_ID").prop("disabled", false);
        $("#SHIP_ESTABLISHMENT_ID").addClass('required')
        $("#authorityDate").prop("disabled", false);
        $("#authorityDate").addClass('required')
        $("#DAO_ID").prop("disabled", false);
        $("#DAO_ID").addClass('required')

        $("#authorityNumber_SEP").prop("disabled", true);
        $("#authorityNumber_SEP").removeClass('required');
        $("#SHIP_AREA_ID_SEP").prop("disabled", true);
        $("#SHIP_AREA_ID_SEP").removeClass('required');
        $("#SHIP_ESTABLISHMENT_ID_SEP").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID_SEP").removeClass('required');
        $("#authorityDate_SEP").prop("disabled", true);
        $("#authorityDate_SEP").removeClass('required');
        $("#DAO_ID_SEP").prop("disabled", true);
        $("#DAO_ID_SEP").removeClass('required');
        $("#separationCause").prop("disabled", true);
        $("#separationCause").removeClass('required');

        $("#authorityNumber_D").prop("disabled", true);
        $("#authorityNumber_D").removeClass('required')
        $("#SHIP_AREA_ID_D").prop("disabled", true);
        $("#SHIP_AREA_ID_D").removeClass('required')
        $("#SHIP_ESTABLISHMENT_ID_D").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID_D").removeClass('required')
        $("#authorityDate_D").prop("disabled", true);
        $("#authorityDate_D").removeClass('required')
        $("#DAO_ID_D").prop("disabled", true);
        $("#DAO_ID_D").removeClass('required')
    }
    function separationInfo() {
        $(".select2-container").css({"width":"100%"});
        $(".separationInfo").show();
        $(".marriageInfo").hide();
        $(".deathInfo").hide();

        $("#marriageDate").prop("disabled", true);
        $("#separationDate").prop("disabled", false);
        $("#deathDate").prop("disabled", true);

        $("#authorityNumber").prop("disabled", true);
        $("#authorityNumber").removeClass('required')
        $("#SHIP_AREA_ID").prop("disabled", true);
        $("#SHIP_AREA_ID").removeClass('required')
        $("#SHIP_ESTABLISHMENT_ID").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID").removeClass('required')
        $("#authorityDate").prop("disabled", true);
        $("#authorityDate").removeClass('required')
        $("#DAO_ID").prop("disabled", true);
        $("#DAO_ID").removeClass('required')

        $("#authorityNumber_SEP").prop("disabled", false);
        $("#authorityNumber_SEP").addClass('required')
        $("#SHIP_AREA_ID_SEP").prop("disabled", false);
        $("#SHIP_AREA_ID_SEP").addClass('required')
        $("#SHIP_ESTABLISHMENT_ID_SEP").prop("disabled", false);
        $("#SHIP_ESTABLISHMENT_ID_SEP").addClass('required')
        $("#authorityDate_SEP").prop("disabled", false);
        $("#authorityDate_SEP").addClass('required')
        $("#DAO_ID_SEP").prop("disabled", false);
        $("#DAO_ID_SEP").addClass('required')
        $("#separationCause").prop("disabled", false);
        $("#separationCause").addClass('required');

        $("#authorityNumber_D").prop("disabled", true);
        $("#authorityNumber_D").removeClass('required')
        $("#SHIP_AREA_ID_D").prop("disabled", true);
        $("#SHIP_AREA_ID_D").removeClass('required')
        $("#SHIP_ESTABLISHMENT_ID_D").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID_D").removeClass('required')
        $("#authorityDate_D").prop("disabled", true);
        $("#authorityDate_D").removeClass('required')
        $("#DAO_ID_D").prop("disabled", true);
        $("#DAO_ID_D").removeClass('required')
    }
    function deathInfo(){
        $(".select2-container").css({"width":"100%"});
        $(".deathInfo").show();
        $(".marriageInfo").hide();
        $(".separationInfo").hide();
        $("#marriageDate").prop("disabled", true);
        $("#separationDate").prop("disabled", true);
        $("#deathDate").prop("disabled", false);

        $("#authorityNumber").prop("disabled", true);
        $("#authorityNumber").removeClass('required');
        $("#SHIP_AREA_ID").prop("disabled", true);
        $("#SHIP_AREA_ID").removeClass('required');
        $("#SHIP_ESTABLISHMENT_ID").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID").removeClass('required');
        $("#authorityDate").prop("disabled", true);
        $("#authorityDate").removeClass('required');
        $("#DAO_ID").prop("disabled", true);
        $("#DAO_ID").removeClass('required');

        $("#authorityNumber_SEP").prop("disabled", true);
        $("#authorityNumber_SEP").removeClass('required');
        $("#SHIP_AREA_ID_SEP").prop("disabled", true);
        $("#SHIP_AREA_ID_SEP").removeClass('required');
        $("#SHIP_ESTABLISHMENT_ID_SEP").prop("disabled", true);
        $("#SHIP_ESTABLISHMENT_ID_SEP").removeClass('required');
        $("#authorityDate_SEP").prop("disabled", true);
        $("#authorityDate_SEP").removeClass('required');
        $("#DAO_ID_SEP").prop("disabled", true);
        $("#DAO_ID_SEP").removeClass('required');
        $("#separationCause").prop("disabled", true);
        $("#separationCause").removeClass('required');

        $("#authorityNumber_D").prop("disabled", false);
        $("#authorityNumber_D").addClass('required')
        $("#SHIP_AREA_ID_D").prop("disabled", false);
        $("#SHIP_AREA_ID_D").addClass('required')
        $("#SHIP_ESTABLISHMENT_ID_D").prop("disabled", false);
        $("#SHIP_ESTABLISHMENT_ID_D").addClass('required')
        $("#authorityDate_D").prop("disabled", false);
        $("#authorityDate_D").addClass('required')
        $("#DAO_ID_D").prop("disabled", false);
        $("#DAO_ID_D").addClass('required')
    }

</script>