<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th>Officical NO</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Spouse Name</th>
        <td><?php echo $viewdetails->SpouseName ?></td>
    </tr>
    <tr>
        <th>Spouse Name (বাংলা)</th>
        <td><?php echo $viewdetails->SpouseNameBangla ?></td>
    </tr>
    <tr>
        <th>Marriage Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->MarriageDate)) ?></td>
    </tr>
    <tr>
        <th>Marriage No</th>
        <td><?php echo $viewdetails->MarriageNo ?></td>
    </tr>
    <tr>
        <th>Marriage Authority Ship</th>
        <td><?php echo $viewdetails->MG_SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Marriage Authority Number</th>
        <td><?php echo $viewdetails->MarriageAuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Marriage Authority Date</th>
        <td><?php echo $viewdetails->MarriageAuthorityDate ?></td>
    </tr>
    <tr>
        <th>Marriage Authority DAO</th>
        <td><?php echo $viewdetails->MarriageDAONo ?></td>
    </tr>
    <?php if($viewdetails->MaritalStatus == 2) :?>
    <tr>
        <th>Separation Date</th>
        <td><?php echo $viewdetails->Date ?></td>
    </tr>
    <tr>
        <th>Separation Authority Ship</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Separation Authority No</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Separation Authority Date</th>
        <td><?php echo $viewdetails->AuthorityDate ?></td>
    </tr>
    <tr>
        <th>Separation Causes</th>
        <td><?php echo $viewdetails->SeparationCause ?></td>
    </tr>
    <?php elseif ($viewdetails->MaritalStatus == 2) : ?>
    <tr>
        <th>Death Date</th>
        <td><?php echo $viewdetails->Date ?></td>
    </tr>
    <tr>
        <th>Death Authority Ship</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Death Authority No</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Death Authority Date</th>
        <td><?php echo $viewdetails->AuthorityDate ?></td>
    </tr>
    <?php endif; ?>
    
</table>