<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('regularTransaction/transferInfo/index'); ?>" title="Transfer information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Transfer Edit</center></h3>                        
                    </div>  
                </div>
            </div>       
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left:0px;">Ship Establishment <span class="text-danger">*</span> </label>
                                    <div class="col-sm-7" >
                                        <?php echo form_dropdown("shipEstablishment", $shipEstablishment, $result->tShipId, "class='select2 form-control' id='shipEstablishment'  data-placeholder='Select Ship Stablishment' aria-hidden='true' data-allow-clear='true'"); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Ship/Establishment">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Posting Unit<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control" id="postingUnit" name="postingUnit" data-placeholder="Select Posting Unit" data-tags="true"  data-allow-clear="true">
                                            <?php foreach ($postingUnit_one as $row) { ?>
                                                <?php if($result->tShipId == $row->SHIP_ESTABLISHMENTID): ?>
                                                    <option value="<?php echo $row->POSTING_UNITID ?>" <?php echo ($row->POSTING_UNITID == $result->PostingUnitID)? 'selected' : ''; ?>><?php echo $row->NAME ?></option>
                                                <?php endif; ?>
                                            <?php } ?>
                                        </select> 
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select posting unit">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left:0px;">Appointment Type <span class="text-danger">*</span></label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="appointment" id="appointment" data-placeholder="Select Appointment Type" data-tags="true"  data-allow-clear="true">
                                            <option value="">Select Appointment Type</option>
                                            <?php
                                            foreach ($appointmentType as $row):
                                                ?>
                                                <option value="<?php echo $row->APPOINT_TYPEID ?>" <?php echo ($row->APPOINT_TYPEID == $result->AppointmentTypeID)? 'selected' : ''; ?>><?php echo $row->NAME ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Select Appointment type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                              
                            </div>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-4">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 15px;  margin-bottom: 10px;">Transfer Authority</legend>                                
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" style="padding-left:0px;">Order Number <span class="text-danger">*</span></label>
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name' => 'orderNumber', "id" => "orderNumber", "class" => "form-control", "value" => $result->TONumber)); ?>
                                        </div> 
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please write Order Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>                                  
                                    </div>                                
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" style="padding-left: 0px;">Order Date <span class="text-danger">*</span></label>                                  
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name' => 'orderDate', "id" => "orderDate", "class" => "datePicker form-control", "value" => date("d-m-Y", strtotime($result->TODate)))); ?>              
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select order date">
                                            <i class="fa fa-question-circle"></i>
                                        </a>                                  
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label"><input type="checkbox" value ="1" class="d_canTemporary" name="read" id="canEffect"> Effect Within</label>
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name' => 'effectDate', "id" => "d_canEffect", "class" => "datePicker form-control", "value" => date("d-m-Y", strtotime($result->EWDate)))); ?>              
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select effect date">
                                            <i class="fa fa-question-circle"></i>
                                        </a>                                  
                                    </div>
                                    <hr>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 15px;  margin-bottom: 10px;">Transfer for Training Course </legend>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" style="padding-right: 0px; padding-left: 0px;"><input type="checkbox" value ="" name="canTraining" id="canTraining" <?php echo ($result->CourseID != '') ? "checked":'' ?>></label>
                                        <div class="col-sm-6 control-label"> Is Training Course</div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Appointment type">
                                            <i class="fa fa-question-circle control-label"></i>
                                        </a>                                  
                                    </div>              
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" style="padding-right: 0px; padding-left: 0px;">Training Type</label>
                                        <div class="col-sm-7" >
                                            <?php echo form_dropdown("traiiningType", $traiiningType, $result->TrainningType, "class='select2 form-control d_canTraining' id='trainingType' data-placeholder='Select Training Type' aria-hidden='true' data-allow-clear='true'"); ?>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Appointment type">
                                            <i class="fa fa-question-circle"></i>
                                        </a>                                  
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" style="padding-right: 0px; padding-left: 0px;">Training Name</label>
                                        <div class="col-sm-7" >
                                            <select  id="trainingName" name="" class="select2 form-control" data-placeholder="Select Training Name" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Course</option>
                                                <?php foreach ($courseTraining_one as $row) { ?>
                                                    <?php if($row->ParentID == $result->TrainningType): ?>
                                                    <option value="<?php echo $row->NAVYTrainingID ?>" <?php echo ($row->NAVYTrainingID == $result->CourseID)? "selected" : '' ?> ><?php echo $row->Name ?></option>
                                                    <?php endif; ?>
                                                <?php } ?>
                                            </select>                                            
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Appointment type">
                                            <i class="fa fa-question-circle"></i>
                                        </a>                                  
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label" style="padding-right: 0px; padding-left: 0px;">Batch Number</label>
                                        <div class="col-sm-7" >
                                            <select  id="d_canBatchNumber" name="canBatchNumber" class="select2 form-control" data-placeholder="Select Batch Name"  aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Course</option>
                                                <?php foreach ($batchNumber_one as $row) { ?>
                                                    <option value="<?php echo $row->BATCH_ID ?>" <?php echo ($row->BATCH_ID == $batchId->BATCH_ID)? "selected" : '' ?> ><?php echo $row->BATCH_NUMBER ?></option>
                                                <?php } ?>
                                            </select> 
                                        </div>                                            
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Appointment type">
                                            <i class="fa fa-question-circle"></i>
                                        </a>                                  
                                    </div>
                                    <hr>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 15px;  margin-bottom: 10px;">Temporary Transfer</legend>
                                    <div class="form-group">
                                        <label class="col-sm-6 control-label"><input type="checkbox" value ="0" id="canTemporary" <?php echo ($result->IsTemp ==1)? "checked" : "" ?>> Is Temporary</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please SelectTemporary Transfer">
                                            <i class="fa fa-question-circle"></i>
                                        </a>                                            
                                    </div>     
                                    <hr>
                                </fieldset>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend" style="font-size: 15px;  margin-bottom: 1px;">Transfer Entry Information</legend>
                        </fieldset>
                    </div>                    
                    <div class="col-md-12">
                        <hr style="margin-top: 0px;">
                        <table id="sailorTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Official Number</th>
                                    <th>Name</th>
                                    <th>Rank</th>
                                    <th>Current Posting Unit</th>
                                    <th>Posting Date</th>
                                    <th>Drafted Posting Unit</th>
                                    <th>Appointment</th>
                                    <th>Transfer Order No</th>
                                    <th>Order Date</th>
                                    <th>Effect Within</th>
                                    <th>Temporary</th>
                                    <th>Course</th>
                                    <th>Batch No</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>          
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result->OFFICIALNUMBER; ?>" name="OFFICIAL_NO[]" id="OFFICIAL_NO_1"  class="form-control required" placeholder="Official No" >
                                    </td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result->FULLNAME; ?>" name="FULLNAME[]" id="FULLNAME_1"  class="form-control required" placeholder="Full Name" >
                                    </td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result->RANK_NAME; ?>" name="RANK[]" id="RANK_1"  class="form-control required" placeholder="Rank" >
                                    </td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo $result->POSTING_UNIT_NAME; ?>" name="POST_UNIT[]" id="POST_UNIT_1"  class="form-control required" placeholder="Posting Unit" >
                                    </td>
                                    <td>
                                        <input type="text" readonly="readonly" value="<?php echo date('d-m-Y', strtotime($result->POSTINGDATE)); ?>" name="POST_UNIT_DATE[]" id="POST_UNIT_DATE_1"  class="form-control required" placeholder="posting date" >
                                    </td>
                                    <td>
                                        <select class="select2 form-control" id="d_PostingUnit_1" name="d_PostingUnit[]">
                                            <option value="">--select--</option>
                                            <?php foreach ($postingUnit_one as $row) { ?>
                                                <option value="<?php echo $row->POSTING_UNITID ?>" <?php echo ($row->POSTING_UNITID==$result->PostingUnitID)? "selected":"" ?>><?php echo $row->NAME ?></option>
                                            <?php } ?>
                                        </select> 
                                    </td>
                                    <td>
                                        <?php 
                                            echo form_dropdown('Appointment[]', $Appointment, $result->AppointmentTypeID, 'class = "select2 form-control" id="Appointment_1"');
                                        ?>
                                    </td>

                                    <td>
                                        <input type="text" name="ORDER_NO[]" id="ORDER_NO_1"  value="<?php echo $result->TONumber; ?>" class="form-control " placeholder="Order No" >
                                    </td>
                                    <td>
                                        <input type="text" name="ORDER_DATE[]" id="ORDER_DATE_1", value="<?php echo date('d-m-Y', strtotime($result->TODate)); ?>" class="datePicker form-control",  placeholder="Date" >
                                    </td>
                                    <td>
                                        <input type="text" name="Effect[]" id="Effect_1",  value="<?php echo date('d-m-Y', strtotime($result->EWDate)); ?>" class="datePicker form-control",  placeholder="Effect With" >
                                    </td>
                                    <td>
                                        <input type="checkbox" name="temporary_1" id="temporary_1"  class="form-control tempCheck" value="0" <?php echo ($result->IsTemp ==1)? "checked" : "" ?>>
                                    </td>
                                    <td>
                                        <?php 
                                            echo form_dropdown('course[]', $courseTraining, $result->CourseID, 'class = "select2 form-control" id="course_1"');
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                            echo form_dropdown('batchNumber[]', $batchNumber, $batchId->BATCH_ID, 'class = "select2 form-control" id="BatchNo_1"');
                                        ?>
                                    </td>
                                    <td>
                                        <input type="text" name="REMARKS[]" id="REMARKS_1" value="<?php echo $result->Remarks; ?>" class="form-control" placeholder="Remarks" >
                                    </td>
                                    <td class="text-center">                       
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br/>
                        <br/>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="<?php echo $result->TransferID; ?>">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/transferInfo/transfer_update" data-redirect-action="regularTransaction/transferInfo/index" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>

<?php $this->load->view("common/sailors_info"); ?>

<script>

    var IsTemp = "<?php echo $result->IsTemp; ?>";
    if(IsTemp == 1){
        $("#temporary_1").val(1);
    }
    var courseId = "<?php echo $result->CourseID; ?>";
    if(courseId != ''){        
        $("#trainingType").prop("disabled", true);
        $("#trainingName").prop("disabled", true);
        $("#d_canBatchNumber").prop("disabled", true);
    }else{
        $("#trainingType").prop("disabled", false);
        $("#trainingName").prop("disabled", false);
        $("#d_canBatchNumber").prop("disabled", false);
    }
        
    $("#d_canEffect").prop("disabled", true);
    $(document).ready(function() {
        $("#canEffect").on('click', function(){
            $("#d_canEffect").attr('disabled', !this.checked);            
        });
        $("#canTraining").on('click', function(){
            $("#trainingType").attr('disabled', !this.checked);            
            $("#trainingName").attr('disabled', !this.checked);            
            $("#d_canBatchNumber").attr('disabled', !this.checked);         
        });
        $("#canTemporary").on('click', function(){

            var status = ($(this).is(':checked') ) ? 1 : 0;
            $(this).val(status);

            $("#canEffect").attr('disabled', this.checked);            
            var isDisabled = $('#d_canEffect').prop('disabled');
            if(isDisabled == false){
                $("#d_canEffect").attr('disabled', this.checked);                            
            }
            if($('#canEffect').is(':checked')) {
                $('#canEffect').prop( "checked", false );
            };
        });
    });
    $(document).ready(function () {
        $('#shipEstablishment').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/TransferInfo/ajax_get_postingUnit'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#postingUnit').html(data);
                    $('#postingUnit').select2('val', '');
                }
            });
        });
        $('#trainingType').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/TransferInfo/ajax_get_trainingName'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#trainingName').html(data);
                    $('#trainingName').select2('val', '');                    
                }
            });
        });
    });

    /*Transfer Entry table for each sailor*/

    // append data info table
    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#sailorTable tbody").append(' <tr>' +
            '<td>' +
            ' <input type="text" name="OFFICIAL_NO[]" id="OFFICIAL_NO_' + counter + '"  class="form-control numbersOnly OFFICIAL_NO_Trans required" placeholder="Official No" >' +
            '</td>' +
            '<td>' +
            ' <input type="text" value="" name="FULLNAME[]" id="FULLNAME_' + counter + '"  class="form-control" placeholder="Full Name" >' +
            '</td>' +            
            '<td>' +
            ' <input type="text" name="RANK[]" id="RANK_' + counter + '"  class="form-control " placeholder="Rank" >' +
            '</td>' +  
            '<td>' +
            ' <input type="text" name="POST_UNIT[]" id="POST_UNIT_' + counter + '"  class="form-control " placeholder="posting unit" >' +
            '</td>' +  
            '<td>' +
            ' <input type="text" name="POST_UNIT_DATE[]" id="POST_UNIT_DATE_' + counter + '"  class="form-control " placeholder="posting unit" >' +
            '</td>' +  
            '<td>' +
                '<select class="select2 form-control" data-live-search= "true" id="d_PostingUnit_' + counter + '" name="d_PostingUnit[]">' +
                    <?php foreach ($postingUnit_one as $row) { ?>
                    '<option value="<?php echo $row->POSTING_UNITID ?>"><?php echo $row->NAME ?></option>' +
                    <?php } ?>
                '</select> ' +
            '</td>' +
            '<td>' +
                '<select class="select2 form-control" data-live-search= "true" id="Appointment_' + counter + '" name="Appointment[]">' +
                    '<option value="">-Select-</option>' +
                    <?php foreach ($Appointment_one as $row) { ?>
                    '<option value="<?php echo $row->APPOINT_TYPEID ?>"><?php echo $row->NAME ?></option>' +
                    <?php } ?>
                '</select> ' +
            '</td>' +            
            '<td>' +
            ' <input type="text" name="ORDER_NO[]" id="ORDER_NO_' + counter + '"  class="form-control " placeholder="Order No" >' +
            '</td>' + 
            '<td>' +
            ' <input type="text" name="ORDER_DATE[]" value = "<?php echo date('d-m-Y') ?>" id="ORDER_DATE_' + counter + '"  class="form-control " placeholder="Date" >' +
            '</td>' + 
            '<td>' +
            ' <input type="text" name="Effect[]" value = "" id="Effect_' + counter + '"  class=" form-control " placeholder="Effect date" >' +
            '</td>' + 
            '<td>' +
            ' <input type="checkbox" value = "0" name="temporary_' + counter + '" id="temporary_' + counter + '"  class="tempCheck form-control " >' +
            '</td>' + 
            '<td>' +
                '<select class="select2 form-control" data-live-search= "true" id="course_' + counter + '" name="course[]">' +
                    '<option value="">-Select-</option>' +
                    <?php foreach ($courseTraining_one as $row) { ?>
                    "<option value='<?php echo $row->NAVYTrainingID ?>'><?php echo $row->Name ?></option>" +
                    <?php } ?>
                '</select> ' +
            '</td>' +
            '<td>' +
                '<select class=" form-control" data-live-search= "true" id="BatchNo_' + counter + '" name="batchNumber[]">' +
                    '<option value="">-Select-</option>' +
                    <?php foreach ($batchNumber_one as $row) { ?>
                    '<option value="<?php echo $row->BATCH_ID ?>"><?php echo $row->BATCH_NUMBER ?></option>' +
                    <?php } ?>
                '</select> ' +
            '</td>' +

            '<td>' +
            ' <input type="text" name="REMARKS[]" id="REMARKS_' + counter + '"  class="form-control " placeholder="Remarks" >' +
            '</td>' +

            '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
            '</td>' +
            '</tr>'
        );
        
        $("#d_PostingUnit_"+counter).select2();
        $("#Appointment_"+counter).select2();
        $("#course_"+counter).select2();
        $("#BatchNo_"+counter).select2();
        $("#ORDER_DATE_"+counter).datepicker();
        $("#Effect_"+counter).datepicker();
            
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    $('#sailorTable').removeAttr('width').DataTable( {  
        "scrollX": true,
        "bPaginate": false,
        "bFilter": false, 
        "bInfo": false,
        "columnDefs": [
            { width: '100px', targets: 0 },
            { width: '100px', targets: 1 },
            { width: '100px', targets: 2 },
            { width: '80px', targets: 3 },
            { width: '80px', targets: 4 },
            { width: '80px', targets: 5 },
            { width: '90px', targets: 6 },
            { width: '70px', targets: 7 },
            { width: '90px', targets: 8 },
            { width: '90px', targets: 11 },
            { width: '90px', targets: 12 },
        ],
        "fixedColumns": true

    } );

    /*end*/

    /*Search Official Number in a sailor & duplicate check official number*/
    $(document).on('click', '.tempCheck', function () {
         var tempCh = $(this).attr('id');
        var len = tempCh.length;
        var lastD = tempCh.charAt(len-1); /*find id attribute last string*/
        var status = ($(this).is(':checked') ) ? 1 : 0;
        $("#temporary_" + lastD).val(status);
    });

    $(document).on('blur', '.OFFICIAL_NO_Trans', function () {
        var officialNoId = $(this).attr('id');
        var officialNo = $(this).val();
        var len = officialNoId.length;
        var lastD = officialNoId.charAt(len-1); /*find id attribute last string*/
        // already selected
        if(officialNo != ''){
            if(inputsHaveDuplicateValues() !== false){
                $("#OFFICIAL_NO_"+lastD).val('');
                alert("This official number is already ");
            }else{
                /*upper get data*/
                var postingUnit = $("#postingUnit").val();
                var appointment = $("#appointment").val();
                var orderNumber = $("#orderNumber").val();
                var orderDate = $("#orderDate").val();
                var d_canEffect = $("#d_canEffect").val();
                var canTemporary = 0;
                if($('#canTemporary').is(':checked')) {
                    canTemporary = 1;
                };

                var trainingName = $("#trainingName").val();
                var d_canBatchNumber = $("#d_canBatchNumber").val();
                /*end*/
                var url = '<?php echo site_url('setup/common/checkOfficialNo'); ?>';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: {officialNo: officialNo},
                    success: function (data) {
                        if(data == "Y"){
                            $.ajax({
                                type: 'POST',
                                url: '<?php echo site_url('setup/common/searchSailor'); ?>',
                                data: {officeNumber: officialNo},
                                dataType: 'json',
                                success: function (data1) {
                                    $("#FULLNAME_"+lastD).val(data1['FULLNAME']);                        
                                    $("#RANK_"+lastD).val(data1["RANK_NAME"]);                        
                                    $("#POST_UNIT_"+lastD).val(data1["POSTING_UNIT_NAME"]);                        
                                    $("#POST_UNIT_DATE_"+lastD).val(data1["POSTING_DATE"]);
                                    
                                    $("#ORDER_NO_"+ lastD).val(orderNumber);
                                    $("#ORDER_DATE_"+ lastD).val(orderDate);
                                    $("#Effect_"+ lastD).val(d_canEffect);

                                    if(canTemporary == 1){
                                        $("#temporary_" + lastD).prop( "checked", true );
                                        $("#temporary_" + lastD).val('1');
                                    }

                                    //$("#d_PostingUnit_"+ lastD + " option:selected").val(postingUnit);
                                    //$("#d_PostingUnit_"+ lastD + " option:selected").val(postingUnit);
                                    //$("#d_PostingUnit_"+ lastD + " option[value='"+postingUnit +"']").attr('selected', 'selected');                                
                                    
                                    $("#d_PostingUnit_"+ lastD).val(postingUnit).trigger("change");
                                    $("#Appointment_"+ lastD).val(appointment).trigger("change");
                                    $("#course_"+ lastD).val(trainingName).trigger("change");
                                    $("#BatchNo_"+ lastD).val(d_canBatchNumber).trigger("change");
                                }
                            });
                        }else{
                            alert("Please enter valid official number");
                            $("#OFFICIAL_NO_"+lastD).val('');
                            $("#FULLNAME_"+lastD).val('');                       
                            $("#RANK_"+lastD).val('');
                            $("#POST_UNIT_"+lastD).val('');
                            $("#POST_UNIT_DATE_"+lastD).val('');
                            $("#SHIP_"+ lastD).val('');
                        }
                    }
                });                
            }            
        }
         // check dublicate selection value
        function inputsHaveDuplicateValues() {
            var hasDuplicates = false;
            $('.OFFICIAL_NO').each(function () {
                var inputsWithSameValue = $(this).val();
                hasDuplicates = $('.OFFICIAL_NO').not(this).filter(function () {
                    return $(this).val() === inputsWithSameValue;
                }).length > 0;
                if (hasDuplicates) return false;
            });
            return hasDuplicates;
        }
        
    });
</script>
