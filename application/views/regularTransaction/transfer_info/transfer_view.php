<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr> 
    <tr>
        <th>Order Number</th>
        <td><?php echo $viewdetails->TONumber ?></td>
    </tr>
    <tr>
        <th>Order Date</th>
        <td><?php echo (date('Y-m-d', strtotime($viewdetails->TODate))); ?></td>
    </tr>
    <tr>
        <th>Posting Unit</th>
        <td><?php echo $viewdetails->POSTING_UNIT ?></td>
    </tr>
    <tr>
        <th>Appointment Type</th>
        <td><?php echo $viewdetails->AP_TYPE_NAME ?></td>
    </tr>
    <tr>
        <th>Effect Within</th>
        <td><?php echo (date('Y-m-d', strtotime($viewdetails->EWDate))); ?></td>
    </tr>
    <tr>
        <th>Temporary</th>
        <td><?php echo ($viewdetails->IsTemp == 1)? "TRUE" : "FALSE" ?></td>
    </tr>
    <tr>
        <th>Course</th>
        <td><?php echo $viewdetails->Training_Name ?></td>
    </tr> 
    <tr>
        <th>batch No</th>
        <td><?php echo $viewdetails->BatchNumber ?></td>
    </tr> 
    <tr>
        <th>Remarks</th>
        <td><?php echo $viewdetails->Remarks ?></td>
    </tr> 
    
</table>