<form class="form-horizontal frmContent" id="MainForm" method="post">
    <div class="col-lg-12">
        <span class="frmMsg"></span> 
        <div class="form-group col-md-12">
            <legend class="legend">Academic Information:</legend>
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <label class="col-md-5">Exam System <b style="color: red"> * </b></label>
                    <div class="col-md-6">
                        <select name="examSystem" id="examSystemEdit"
                                class="form-control required select2"
                                data-placeholder="Select Exam System" aria-hidden="true"
                                data-allow-clear="true" style="width: 100%;">
                            <option value="">Select Exam System</option>
                            <option value="1" <?php echo ($results->EvaluationSystem == 1) ? 'selected' : '' ?>>Traditional</option>
                            <option value="2" <?php echo ($results->EvaluationSystem == 2 ) ? 'selected' : '' ?>>GPA</option>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select Exam System">
                        <i class="fa fa-question-circle"></i>
                    </a>

                    <div class="col-sm-5 hidden">
                        <?php echo form_input(array('name' => 'sailorId', 'id' => 'sailor_id_edit', "class" => "form-control", 'readonly' => 'readonly', 'value' => $results->SailorID)); ?>
                    </div>

                </div>
                <div class="col-md-6">
                    <label class="col-md-5">Exam Name <b style="color: red"> * </b></label>
                    <div class="col-md-6">
                        <select class="select2 form-control required" name="examName" id="examIdEdit" data-placeholder="Select Exam Name" aria-hidden="true" data-allow-clear="true">
                            <option value="">Select Exam Name</option>
                            <?php
                            foreach ($exam_nameData as $row):
                                ?>
                                <option value="<?php echo $row->GOVT_EXAMID ?>" <?php echo ($results->ExamID == $row->GOVT_EXAMID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select Exam Name">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <label class="col-md-5">Subject/Group <b style="color: red"> * </b></label>
                    <div class="col-md-6">
                        <select class="select2 form-control required" name="subjectGroup" id="subjectGroupIdEdit" data-placeholder="Select Subject/Group" aria-hidden="true" data-allow-clear="true">
                            <option value="">Select Exam Name</option>
                            <?php
                            foreach ($subject_group as $row):
                                ?>
                                <option value="<?php echo $row->SUBJECT_GROUPID ?>" <?php echo ($results->SubjectID == $row->SUBJECT_GROUPID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select Subject/Group Name">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <label class="col-md-5">Passing Year <b style="color: red"> * </b></label>

                    <div class="col-md-6">
                        <select name="passingYear" id="passingYearidEdit"
                                class="form-control required select2"
                                data-placeholder="Select Passing Year" aria-hidden="true"
                                data-allow-clear="true" style="width: 100%;">
                            <option>Select Year</option>
                            <?php
                            for ($i = date("Y"); $i >= 1950; $i--) {
                                ?>
                                <option value="<?php echo $i; ?>" <?php echo ($results->PassingYear == $i) ? 'selected' : '' ?>><?php echo $i ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select Passing Year">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <label class="col-md-5">Board/University <b style="color: red"> * </b></label>

                    <div class="col-md-6">
                        <select class="select2 form-control required" name="boardUniversity" id="boardUniversityEdit" data-placeholder="Select Board/University" aria-hidden="true" data-allow-clear="true">
                            <option value="">Select Exam Name</option>
                            <?php
                            foreach ($board_university as $row):
                                ?>
                                <option value="<?php echo $row->BOARD_CENTERID ?>" <?php echo ($results->BoardID == $row->BOARD_CENTERID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select Exam Name">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <label class="col-md-5">Result <b style="color: red"> * </b></label>
                    <div class="col-md-6">
                        <select class="select2 form-control required" name="result" id="resultIdEdit" data-placeholder="Select Result" aria-hidden="true" data-allow-clear="true">
                            <option value="">Select Exam Name <b style="color: red"> * </b></option>
                            <?php
                            foreach ($result as $row):
                                ?>
                                <option value="<?php echo $row->EXAM_GRADEID ?>" <?php echo ($results->ExamGradeID == $row->EXAM_GRADEID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select Result">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-6" id="gpaEdit">
                    <label class="col-md-5">GPA</label>
                    <div class="col-md-5">
                        <?php echo form_input(array('name' => 'gpa', 'id' => 'gpaEdit', 'type' => 'text', "class" => "form-control numbersOnly ", 'placeholder' => 'Enter GPA', 'value' => $results->TotalMarks)); ?>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select GPA">
                        <i class="fa fa-question-circle"></i>
                    </a>                    
                </div>
                <div id="traditionaldiv" class="col-md-6" style="display:none">
                    <label class="col-md-5">Total Mark's</label>

                    <div class="col-md-5">
                        <?php echo form_input(array('name' => 'tradition', 'id' => 'traditionalEdit', 'type' => 'text', "class" => "form-control numbersOnly ", 'placeholder' => 'Enter Traditional', 'value' => $results->TotalMarks)); ?>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select GPA">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <label class="col-md-5">Percentage</label>

                    <div class="col-md-6">
                        <?php echo form_input(array('name' => 'percent', 'id' => 'percentEdit', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Percentage', 'value' => $results->Percentage)); ?>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please select Percentage">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <label class="col-sm-5">Result Description</label>

                    <div class="col-md-6">
                        <?php echo form_input(array('name' => 'resultdesc', 'id' => 'resultDescEdit', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Result Description', 'value' => $results->ResultDescription)); ?>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Please Enter Result Description">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
                <div class="col-md-6">
                    <label class="col-md-5">Education Status <b style="color: red"> * </b></label>

                    <div class="col-md-6">
                        <select id="eduStatusDescEdit" name="eduStatusDesc" class="select2 form-control"
                                data-placeholder="Select Education Status" aria-hidden="true"
                                data-allow-clear="true" style="width: 100%;">
                            <option value="">Select Education Status</option>
                            <option value="1" <?php echo ($results->EducationStatus == 1) ? 'selected' : '' ?>>Entry Education</option>
                            <option value="2" <?php echo ($results->EducationStatus == 2) ? 'selected' : '' ?>>Entry Period Highest</option>
                            <option value="3" <?php echo ($results->EducationStatus == 3) ? 'selected' : '' ?>>Last</option>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover"
                       data-placement="right" data-content="Select Education Status">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <label class="col-sm-3 control-label">&nbsp;</label>
            <div class="col-sm-6">
                <input type="hidden" name="idEdit" value="<?php echo $results->AcademicID; ?>">
                <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="regularTransaction/AcademicInfo/update" data-su-action="regularTransaction/AcademicInfo/updateSailorList/<?php echo $results->SailorID; ?>" data-type="list" value="update">
            </div>
        </div>    
    </div>
</form>
<script type="text/javascript">
    $(document).on('keyup', '.numbersOnly', function () {
        var val = $(this).val();
        if (isNaN(val)) {
            val = val.replace(/[^0-9\.]/g, '');
            if (val.split('.').length > 2) {
                val = val.replace(/\.+$/, "");
            }
        }
        $(this).val(val);
    });
    $(document).ready(function () {
        $("#examSystemEdit").change(function () {
            $(this).find("option:selected").each(function () {
                if ($(this).attr("value") == "1") {
                    $("#gpaEdit").hide();
                    $("#traditionaldiv").show();
                }
                else if ($(this).attr("value") == "2") {
                    $("#traditionaldiv").hide();
                    $("#gpaEdit").show();
                }
                else {
                    $("#traditionaldiv").hide();
                }
            });
        }).change();
    });
</script>
