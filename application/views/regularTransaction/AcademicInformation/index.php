<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                    
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Academic Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>
                    <div class="form-group col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Current Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-5 control-label">Official Number</label>        
                                    <div class="col-sm-5" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNoAcademic', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                            <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-5 control-label ">Posting Unit</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>

                                    </div>
                                </div>
                                <div class="form-group hidden ">
                                    <label class="col-sm-5 control-label">Sailor Id</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'sailorId', 'id' => 'sailor_id', "class" => "form-control sailorId ", 'readonly' => 'readonly', 'value' => set_value('sailorId'))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="col-sm-8">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'postingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="form-group col-md-12">
                        <legend class="legend">Academic Information:</legend>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-md-5">Exam System <span class="text-danger"> * </span></label>
                                <div class="col-md-6">
                                    <select name="examSystem[]" id="examSystem"
                                            class="form-control select2"
                                            data-placeholder="Select Exam System" aria-hidden="true"
                                            data-allow-clear="true" style="width: 100%;">
                                        <option value="">Select Exam System</option>
                                        <option value="1">Traditional</option>
                                        <option value="2">GPA</option>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Exam System">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-5">Exam Name <span class="text-danger"> * </span></label>

                                <div class="col-md-5">
                                    <?php echo form_dropdown('examNameDesc[]', $exam_name, '', 'id="examNameDesc" required="required" class="form-control select2" data-placeholder="Select Exam Name" aria-hidden="true" data-allow-clear="true" style="width: 120px;"'); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Exam Name">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-md-5">Subject/Group <span class="text-danger"> * </span></label>

                                <div class="col-md-6">
                                    <?php echo form_dropdown('subjectGroupDesc[]', $subject_group, '', 'id="subjectGroupDesc" required="required" class="form-control select2" data-placeholder="Select Subject Group" aria-hidden="true" data-allow-clear="true" style="width: 120px;"'); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Subject/Group Name">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-5">Passing Year <span class="text-danger"> * </span></label>
                                <div class="col-md-5">
                                    <select name="passingYear[]" id="passingYearid"
                                            class="form-control required select2"
                                            data-placeholder="Select Passing Year" aria-hidden="true"
                                            data-allow-clear="true" style="width: 110%;">
                                        <option>Select Year</option>
                                        <?php
                                        for ($i = 1995; $i <= date("Y"); $i++) {
                                            ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Passing Year">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-md-5">Board/University <span class="text-danger"> * </span></label>

                                <div class="col-md-6">
                                    <?php echo form_dropdown('baordUniversityDesc[]', $board_university, '', 'id="baordUniversityDesc" required="required" class="form-control select2" data-placeholder="Select Board/University" aria-hidden="true" data-allow-clear="true" style="width: 120px;"'); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Exam Name">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-5">Result <span class="text-danger"> * </span></label>

                                <div class="col-md-5">
                                    <?php echo form_dropdown('result[]', $result, '', 'id="results" required="required" class="form-control select2" data-placeholder="Select Result" aria-hidden="true" data-allow-clear="true" style="width: 120px;"'); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Result">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6" id="gpadiv">
                                <label class="col-md-5">GPA</label>

                                <div class="col-md-6">
                                    <?php echo form_input(array('name' => 'gpa[]', 'id' => 'gpa', 'type' => 'text', "class" => "form-control numbersOnly required", 'required' => 'required', 'placeholder' => 'Enter GPA')); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select GPA">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div id="traditional1" class="col-md-6" style="display:none">
                                <label class="col-md-5">Total Mark's</label>

                                <div class="col-md-6">
                                    <?php echo form_input(array('name' => 'tradition[]', 'id' => 'traditional', 'type' => 'text', "class" => "form-control numbersOnly required", 'required' => 'required', 'placeholder' => 'Enter Traditional')); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select GPA">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>


                            <div class="col-md-6">
                                <label class="col-md-5">Percentage</label>

                                <div class="col-md-5">
                                    <?php echo form_input(array('name' => 'percent[]', 'id' => 'percent', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Percentage')); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please select Percentage">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6">
                                <label class="col-sm-5">Result Description</label>

                                <div class="col-md-6">
                                    <?php echo form_input(array('name' => 'resultdesc[]', 'id' => 'resultDesc', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Result Description')); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Please Enter Result Description">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-6">
                                <label class="col-md-5">Education Status <span class="text-danger"> * </span></label>

                                <div class="col-md-5">
                                    <select id="eduStatusDesc" name="eduStatusDesc[]" class="select2 form-control"
                                            data-placeholder="Select Education Status" aria-hidden="true"
                                            data-allow-clear="true" style="width: 100%;">
                                        <option value="">Select Education Status</option>
                                        <option value="1">Entry Education</option>
                                        <option value="2">Entry Period Highest</option>
                                        <option value="3">Last</option>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover"
                                   data-placement="right" data-content="Select Education Status">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <label class="col-md-1"><span
                                        class="glyphicon glyphicon-arrow-right"></span></label>

                                <div class="xh col-md-6 " style="text-align: right;">
                                    <span class="btn btn-xs btn-success" id="add">
                                        <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <table id="selected_pc" class="table table-striped table-bordered " width="100%"
                               cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Exam System</th>
                                    <th>Exam Name</th>
                                    <th>Group</th>
                                    <th>Passing Year</th>
                                    <th>Board</th>
                                    <th>Result</th>
                                    <th>GPA</th>
                                    <th>Total Marks</th>
                                    <th>Percentage</th>
                                    <th>Result Description</th>
                                    <th>Education Status Desc</th>
                                    <th>Action</th>                                    
                                </tr>
                            </thead>
                            <tbody id="fiExit" class="contentArea">
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-8">
                        <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect"
                               data-action="regularTransaction/AcademicInfo/save"
                               data-redirect-action="regularTransaction/AcademicInfo/index"
                               data-type="list" value="submit">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>

<script type="text/javascript">
    $("#officialNoAcademic").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:1},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailor",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);
                        $(".PostingUnit").val(data['POSTING_UNIT_NAME']);
                        $(".PostingDate").val(data['POSTING_DATE']);  
                        $(".alertSMS").html('');
                        
                        var sailor_id = data['SAILORID'];
                        $.ajax({
                            type: "post",
                            url: '<?php echo site_url('regularTransaction/AcademicInfo/ifExistData') ?>',
                            data: {sailor_id: sailor_id},
                            beforeSend: function () {
                                $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                            },
                            success: function (data) {
                                $("#fiExit").html(data);
                            }
                        });                 
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val(''); 
                        $(".PostingUnit").val(''); 
                        $(".PostingDate").val('');                       
                        $(".alertSMS").html('Invalid Officer Number');      
                    }
                }
            });            
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val(''); 
            $(".alertSMS").html(''); 
        }
    });
    $(document).ready(function () {
        $(document).on("click", "#add", function (e) {
            var isValid = 0;
            var a = 0;
            var examSystem = $("#examSystem").val();
            var examSystem_name = $("#examSystem :selected").text().trim();
            var exam_id = $("#examNameDesc").val();
            var exam_name = $("#examNameDesc").text().trim();
            var examNameDesc = $("#examNameDesc :selected").val();
            var examNameDesc_name = $("#examNameDesc :selected").text().trim();
            var subjectGroupDesc = $("#subjectGroupDesc :selected").val();
            var subjectGroupDesc_name = $("#subjectGroupDesc :selected").text().trim();
            var subjectValidation = $("#subjectGroupDesc").val();
            var passingYear = $("#passingYearid").val();
            var baordUniversityDesc = $("#baordUniversityDesc :selected").val();
            var baordUniversityDesc_name = $("#baordUniversityDesc :selected").text().trim();
            var boardValidation = $("#baordUniversityDesc").val();
            var result = $("#results :selected").val();
            var result_n = $("#results :selected").text().trim();
            var resultValidation = $("#results").val();
            var resultDesc = $("#resultDesc").val();
            var percent = $("#percent").val();
            var gpa = $("#gpa").val();
            var traditional = $("#traditional").val();
            var eduStatusDesc = $("#eduStatusDesc :selected").val();
            var eduStatusDesc_name = $("#eduStatusDesc :selected").text().trim();
            if (examSystem == "") {
                alert("Select Exam System");
                isValid = 1;
                return false;
            } else if (exam_id == "") {
                alert("Select Exam Name");
                isValid = 1;
                return false;
            } else if (subjectValidation == "") {
                alert("Select Subject/Group");
                isValid = 1;
                return false;
            } else if (passingYear == "") {
                alert("Select Passing Year");
                isValid = 1;
                return false;
            } else if (boardValidation == "") {
                alert("Select Board/University");
                isValid = 1;
                return false;
            } else if (resultValidation == "") {
                alert("Select Result");
                isValid = 1;
                return false;
            }
            if (isValid == 0) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo site_url('regularTransaction/AcademicInfo/getEduData') ?>',
                    data: {
                        examSystem: examSystem,
                        examSystem_name: examSystem_name,
                        examNameDesc: examNameDesc,
                        examNameDesc_name: examNameDesc_name,
                        subjectGroupDesc: subjectGroupDesc,
                        subjectGroupDesc_name: subjectGroupDesc_name,
                        passingYear: passingYear,
                        baordUniversityDesc: baordUniversityDesc,
                        baordUniversityDesc_name: baordUniversityDesc_name,
                        result: result,
                        result_n: result_n,
                        resultDesc: resultDesc,
                        eduStatusDesc: eduStatusDesc,
                        eduStatusDesc_name: eduStatusDesc_name,
                        percent: percent,
                        traditional: traditional,
                        gpa: gpa


                    },
                    success: function (data) {
                        $(".eximId").each(function () {
                            if ($(this).val() == examNameDesc) {
                                a = 1;
                            }
                        });
                        if (a == 1) {
                            alert("You Already selected this Exam");
                            return false;
                        } else {
                            $("#selected_pc").append(data);
                        }
                    }
                });
            }
        })

        $(document).on("click", ".removeEdu", function () {
            $(this).parent().parent("tr").remove();
        });
        $(document).on('keyup', '.numbersOnly', function () {
            var val = $(this).val();
            if (isNaN(val)) {
                val = val.replace(/[^0-9\.]/g, '');
                if (val.split('.').length > 2) {
                    val = val.replace(/\.+$/, "");
                }
            }
            $(this).val(val);
        });
        $(document).ready(function () {
            $("#examSystem").change(function () {
                $(this).find("option:selected").each(function () {
                    if ($(this).attr("value") == "1") {
                        $("#gpadiv").hide();
                        $("#traditional1").show();
                    }
                    else if ($(this).attr("value") == "2") {
                        $("#traditional1").hide();
                        $("#gpadiv").show();
                    }
                    else {
                        $("#traditional1").hide();
                    }
                });
            }).change();
        });
        $('#selected_pc').removeAttr('width').DataTable({
            "scrollX": true,           
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
            "columnDefs": [
                {width: '80px', targets: 0},
                {width: '80px', targets: 1},
                {width: '60px', targets: 2},
                {width: '80px', targets: 3},
                {width: '60px', targets: 4},                
                {width: '20px', targets: 5},
                {width: '20px', targets: 6},              
                {width: '70px', targets: 7},
                
                {width: '60px', targets: 8},
                {width: '150px', targets: 9},
                {width: '150px', targets: 10},
                {width: '80px', targets: 11},
                
            ],
            "fixedColumns": true

        });
    });
</script>