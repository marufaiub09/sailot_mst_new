<?php foreach ($academicInfo as $row) { ?>
    <tr id="row_<?php echo $row->AcademicID ?>">
        <td class="text-center"><?php echo $row->Exam_System ?></td>
        <td class="text-center"><?php echo $row->Exam_Name ?></td>
        <td class="text-center"><?php echo $row->Subject ?></td>
        <td class="text-center"><?php echo $row->PassingYear ?></td>        
        <td class="text-center"><?php echo $row->Board_Name ?></td>        
        <td class="text-center"><?php echo $row->Exam_Gred ?></td>        
        <td class="text-center"><?php echo $row->Exam_Gred ?></td>      
        <td class="text-center"><?php echo $row->TotalMarks ?></td>       
        <td class="text-center"><?php echo $row->Percentage ?></td>
        <td class="text-center"><?php echo $row->ResultDescription ?></td>
        <td class="text-center"><?php echo $row->EDUST ?></td>        
        <td>
    <center>
        <a class="btn btn-warning btn-xs modalLink"  data-modal-size="modal-lg" href="<?php echo site_url('regularTransaction/AcademicInfo/edit/' . $row->AcademicID); ?>"  title="Edit Academic Information" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
        <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->AcademicID; ?>" title="Click For Delete" data-type="delete" data-field="AcademicID" data-tbl="academic"><span class="glyphicon glyphicon-trash"></span></a>
    </center>
    </td>
    </tr>
<?php } ?>