<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/punishmentActivity/punishment_index') : site_url('regularTransaction/punishmentActivity/punishment_index') ?>" title="Punishment information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Edit punishment Information</center></h3>                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span> 
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'value' => $result->OFFICIALNUMBER,'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number' , 'readonly' => 'readonly')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Punishment Cause</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter punishment causes">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'punishmentCauses', "id"=>"punishmentCauses", "rows" => "2","cols" => "2", "class" => "form-control", "value" => $result->Cause)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => $result->FULLNAME)); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => $result->RANK_NAME)); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class=""> 
                            <div class="col-md-6">                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Punishment No</label>
                                    <div class="col-sm-7" >
                                        <select class="select2 form-control required" name="PUNISHMENT_NO" id="PUNISHMENT_NO" data-placeholder="Select punishment type" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select punishment type</option>
                                            <?php
                                            foreach ($punishmentType as $row):
                                                ?>
                                                <option value="<?php echo $row->PunishmentTypeID ?>" <?php echo ($result->PunishmentTypeID == $row->PunishmentTypeID) ? 'selected' : '' ?>><?php echo $row->Name ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required", "value" => $result->AuthorityNumber)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"  <?php echo ($result->AREA_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number</label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>" <?php echo ($result->DAOID == $row->DAO_ID) ? 'selected' : '' ?>><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Punishment Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Award date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'punishmentDate', "id"=>"punishmentDate", "class" => "datePicker form-control", "value" => date("d-m-Y" , strtotime($result->EffectDate)))); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y", strtotime($result->AuthorityDate)))); ?>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="AUTHO_SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                            <?php
                                            foreach ($shipEst as $row):
                                                if($row->AREA_ID == $result->AREA_ID):
                                                ?>
                                                <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->ShipID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                                endif;
                                            endforeach; 
                                            ?>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-11">
                            <label class="col-sm-2 control-label">Offence Category</label>
                            <div class="form-group">
                                <div class="col-sm-8" >
                                    <table id="example" class="display" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Description</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($offenceCat as $key => $row): ?>
                                            <tr>
                                                <?php $offCat = $this->utilities->findByAttribute("punishmentoffence", array("OffenceCatagoryID" =>$row->OFFENCE_CATAGORYID, "PunishmentID" => $result->PunishmentID)); ?>
                                                <td><input type="checkbox" name="offenceCat[]" value="<?php echo $row->OFFENCE_CATAGORYID; ?>" <?php echo !empty($offCat->OffenceCatagoryID) ? 'checked' : '' ?>> &nbsp;&nbsp;<?php echo $row->CODE ?></td>
                                                <td><?php echo $row->NAME ?></td>
                                            </tr>
                                            <?php endforeach; ?>
                                            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="<?php echo $result->PunishmentID; ?>">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/punishmentActivity/punishment_update':'regularTransaction/punishmentActivity/punishment_update' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/punishmentActivity/punishment_index':'regularTransaction/punishmentActivity/punishment_index'?>" value="Update">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(document).ready(function() {
    $('#example').DataTable( {
        "scrollY":        "200px",
        "scrollCollapse": false,
        "paging":         false,
        "bFilter" : false,
        "bInfo" : false
    } );
} );
</script>