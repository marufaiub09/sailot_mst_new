<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/honorActivity/medal_index') : site_url('regularTransaction/honorActivity/medal_index') ?>" title="Medal information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Add Medal Information</center></h3>                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span> 
                    <div class="col-md-12">
                        <fieldset class=""> 
                            <div class="col-md-6">                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Medal Name</label>
                                    <div class="col-sm-7" >
                                        <select class="select2 form-control required" name="MEDAL_NAME" id="MEDAL_NAME" data-placeholder="Select Medal Name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Medal Name</option>
                                            <?php
                                            foreach ($medalName as $row):
                                                ?>
                                                <option value="<?php echo $row->MEDAL_ID ?>"><?php echo $row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Medal Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityName', "id"=>"authorityName", "class" => "form-control required")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Award Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Award date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'awardDate', "id"=>"awardDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number</label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="AUTHO_SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                        </select>               
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>  
                    <div class="col-md-10">
                        <div class="col-md-1"></div>
                        <div class="col-md-11">
                            <hr style="margin-top: 0px;">
                            <table id="sailorTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Official Number</th>
                                        <th>Full Name</th>
                                        <th>Rank</th>
                                        <th>Posting Unit</th>                                    
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>          
                                        <td>
                                            <input type="text" name="OFFICIAL_NO[]" id="OFFICIAL_NO_1"  class="<?php echo ($flag == 1)? 'RET_OFFICIAL_NO':'OFFICIAL_NO' ?> form-control numbersOnly required" placeholder="Official No" >
                                        </td>
                                        <td>
                                            <input type="text" value="" name="FULLNAME[]" id="FULLNAME_1"  class="form-control required" placeholder="Full Name" >
                                        </td>
                                        <td>
                                            <input type="text" name="RANK[]" id="RANK_1"  class="form-control required" placeholder="Rank" >
                                        </td>
                                        <td>
                                            <input type="text" name="POST_UNIT[]" id="POST_UNIT_1"  class="col-sm-1 form-control required" placeholder="post unit" >
                                        </td>                                    
                                        <td class="text-center">                       
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <div class="xh" style="text-align: right;">
                                <span class="btn btn-xs btn-success" id="add_record">
                                    <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/honorActivity/medal_save' : 'regularTransaction/honorActivity/medal_save' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/honorActivity/medal_index' : 'regularTransaction/honorActivity/medal_index' ?>" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    // append academic info table
    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#sailorTable tbody").append(' <tr>' +           
            '<td>' +
            ' <input type="text" name="OFFICIAL_NO[]" id="OFFICIAL_NO_' + counter + '"  class="form-control numbersOnly OFFICIAL_NO required" placeholder="Official No" >' +
            '</td>' +
            '<td>' +
            ' <input type="text" value="" name="FULLNAME[]" id="FULLNAME_' + counter + '"  class="form-control" placeholder="Full Name" >' +
            '</td>' +            
            '<td>' +
            ' <input type="text" name="RANK[]" id="RANK_' + counter + '"  class="form-control " placeholder="Rank" >' +
            '</td>' +
             '<td>' +
            ' <input type="text" name="POST_UNIT[]" id="POST_UNIT_' + counter + '"  class="col-sm-1 form-control required" placeholder="post unit" >' +
            '</td>' + 
            '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" ></i></span>' +
            '</td>' +
            '</tr>'
        );
        
        $("#END_DATE_"+counter).datepicker();
            
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });
</script>