<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr>
    <tr>
        <th>Medal Name</th>
        <td><?php echo $viewdetails->MEDAL_NAME ?></td>
    </tr>
    <tr>
        <th>Award Date</th>
        <td><?php echo (date('Y-m-d', strtotime($viewdetails->AwardDate))); ?></td>
    </tr>
    <tr>
        <th>Authority Ship</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Authority Number</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Authority Date</th>
        <td><?php echo (date('Y-m-d', strtotime($viewdetails->AuthorityDate))); ?></td>
    </tr>
    <tr>
        <th>Authority Name</th>
        <td><?php echo $viewdetails->AuthorityName ?></td>
    </tr>
    <tr>
        <th>DAO Number</th>
        <td><?php echo $viewdetails->DAONumber ?></td>
    </tr> 
</table>