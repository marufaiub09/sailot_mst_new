<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('regularTransaction/reEngagementInfo/index'); ?>" title="Re-Engagement Information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Add Re-Engagement Information</center></h3>                        
                    </div>  
                </div>

            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Current Info</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'preEngagementNo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ship/Establishment</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'SHIP_ESTALISHMENT', "class" => "form-control SHIP_ESTALISHMENT required", 'required' => 'required', 'placeholder' => 'ship/establishment', 'readonly' => 'readonly', 'value' => set_value('SHIP_ESTALISHMENT'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Engagement No</label>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'EngagementNo', "id"=>"EngagementNo", "class" => "form-control required", 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Entry Type</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'entryType', "class" => "form-control entryType", 'placeholder' => 'Entry type', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Authority Information </legend>
                            <div style="margin-right: 5%; text-align: right;" id="convert">
                                <input type="checkbox" name="NCSTOCS" value="1" id="ncsToCs"> NCS TO CS
                            </div> 
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Engagement No</label>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'engagementNoNext', "id"=>"engagementNoNext", "class" => "form-control required", 'readonly' => 'readonly')); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Engagement Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Engagement date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'engagementDate', "id"=>"engagementDate", "value" => date("d-m-Y"),  "class" => "datePicker form-control required", "placeholder" => "Engagement date")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Engage For (Year)</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'engageFor', "id"=>"engageFor", "class" => "form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Ship Area</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-placeholder="Select ship area" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship area</option>
                                            <?php
                                            foreach ($authorityArea as $row):
                                                ?>
                                                <option value="<?php echo $row->ADMIN_ID ?>"><?php echo "[".$row->CODE."] ".$row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select authority ship area">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Authority Ship Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select authority ship name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-7">
                                        <select class="select2 vistInfo_dropdown form-control" name="AUTHO_SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT_ID" data-placeholder="Select ship name" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship name</option>
                                        </select>               
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number</label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">                                
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Engagement Period</legend>
                                    <div class="form-group" style="margin-bottom:1px">
                                        <label class="col-sm-4 control-label"></label>
                                        <div class="col-sm-2"> &nbsp; Year </div>
                                        <div class="col-sm-2"> Month </div>
                                        <div class="col-sm-2"> &nbsp; Day </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Period</label>
                                        <div class="col-sm-2">
                                            <?php echo form_input(array('name' => 'priodYY', "id"=>"priodYY", "class" => "form-control required", "value" => "00", "maxlength"=>"2")); ?>
                                        </div>
                                        <div class="col-sm-2">
                                            <?php echo form_input(array('name' => 'priodMM', "id"=>"priodMM", "class" => "form-control required", "value" => "00", "maxlength"=>"2", "max" => "12")); ?>
                                        </div>
                                        <div class="col-sm-2">
                                            <?php echo form_input(array('name' => 'priodDD', "id"=>"priodDD", "class" => "form-control required", "value" => "00", "maxlength"=>"2", "max" => "31")); ?>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Expire Date</label>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'expireDate', "id"=>"expireDate", "class" => "form-control required", "readonly" => "readonly", "placeholder" =>"Expire date")); ?>
                                        </div>
                                    </div>                                    
                                    <hr style=" margin-bottom: 5px; margin-top: 0;">
                                </fieldset>  
                                
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>                               
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Engagement Type</label>
                                    <div class="col-sm-6" >
                                        <?php 
                                         $engageType = array(
                                            '1' => "Normal",
                                            '2' => "Extension",
                                            '3' => "Compulsary Extension",
                                            '4' => "Punishment Extension",
                                            '5' => "Hospital Extension"
                                            );
                                        echo form_dropdown('engagement_type', $engageType, '', 'class="select2 form-control required" id="engagement_type"')
                                        ?>
                                        
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Entry Type</label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control " name="ENTRY_TYPE" id="ENTRY_TYPE" data-placeholder="Select entry type" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Entry type</option>
                                            <?php
                                            foreach ($entryType as $row):
                                                ?>
                                                <option value="<?php echo $row->ENTRY_TYPEID ?>"><?php echo $row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Select Entry Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <input type="hidden" id="engPeriod" value="">
                            </div>
                        </fieldset>
                    </div>                       
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/reEngagementInfo/save" data-redirect-action="regularTransaction/reEngagementInfo/index" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    /*start search sailorInfo, rank_name, ship_establishment, Posting_unit, Posting_date by Official Number */
    $("#preEngagementNo").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber},
                dataType: "json",
                url: "<?php echo site_url(); ?>regularTransaction/ReEngagementInfo/searchSailorInfoByOfficalNo",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $.ajax({
                            type: "post",
                            data: {officeNumber: officeNumber},
                            dataType: "html",
                            url: "<?php echo site_url(); ?>regularTransaction/ReEngagementInfo/countEngagementInfo",
                            success: function (data1) {
                                if( data1 != 0){
                                    $.ajax({
                                        type: "post",
                                        data: {officeNumber: officeNumber},
                                        dataType: "json",
                                        url: "<?php echo site_url(); ?>regularTransaction/ReEngagementInfo/engagementCurrentInfo",
                                        success: function (data2) { 
                                            $EntryTypeID = data2['EntryTypeID'];
                                            if($EntryTypeID == 2){
                                                $("#ncsToCs").prop("disabled", true);
                                                $("#ENTRY_TYPE").prop("disabled", true);
                                                $("#convert").css({
                                                    "background-color": "WHITE",
                                                    "opacity": "0.5"                                                    
                                                });
                                            }else{
                                                $("#ncsToCs").prop("disabled", false);
                                                $("#ENTRY_TYPE").prop("disabled", false);
                                                $("#convert").css({
                                                    "opacity": "1"                                                    
                                                });
                                            }
                                            $("#engagementDate").val(data2['ExpiryDate']);

                                            dArr = data2['ExpiryDate'].split("-");
                                            var expDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];
                                            var d = new Date(expDate);
                                            var dt = new Date(d.setDate(d.getDate() - 1));                                            
                                            var year = dt.getFullYear();
                                            var date = dt.getDate(dt);
                                            var month = (dt.getMonth() + 1);

                                            if(month.toString().length == 1){
                                                month = "0"+month;
                                            }
                                            if(date.toString().length == 1){
                                                date = "0"+date;
                                            }
                                            var exp_date = year+"/"+month+"/"+date;
                                            $("#expireDate").val(exp_date);
                                        }
                                    });
                                }
                                $("#EngagementNo").val(data1);
                                var nextNumber = parseInt(data1) + 1;
                                $("#engagementNoNext").val(nextNumber);
                            }
                        });
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);  
                        $(".SHIP_ESTALISHMENT").val(data['SHIP_ESTABLISHMENT']);  
                        $(".PostingUnit").val(data['POSTING_UNIT_NAME']);
                        $(".PostingDate").val(data['POSTING_DATE']);                  
                        $(".entryType").val(data['ENTRY_TYPE_NAME']);                  
                        $("#engageFor").val(data['ENGAGEMENTPERIOD']);                  
                        $("#engPeriod").val(data['ENGAGEMENTPERIOD']);                  
                        $(".alertSMS").html('');
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');      
                        $(".SHIP_ESTALISHMENT").val('');      
                        $(".PostingUnit").val('');
                        $(".PostingDate").val('');
                        $(".entryType").val('');
                        $(".alertSMS").html('Invalid Officer Number');      
                    }
                }
            });
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');      
            $(".SHIP_ESTALISHMENT").val('');      
            $(".PostingUnit").val('');
            $(".PostingDate").val('');
            $(".entryType").val('');
            $(".alertSMS").html(''); 
        }
    });
    /*end Official Number search part*/

    function AddDate(oldDate, offset, offsetType) {

        var year = parseInt(oldDate.getFullYear());
        var month = parseInt(oldDate.getMonth());
        var date = parseInt(oldDate.getDate());
        var hour = parseInt(oldDate.getHours());
        var newDate;
        switch (offsetType) {

            case "Y":
            case "y":

            newDate = new Date(year + offset, month, date, hour);
            break;
            case "M":
            case "m":

               newDate = new Date(year, month + offset, date, hour);
               break;    

            case "D":
            case "d":

               newDate = new Date(year, month, date + offset, hour);
               break;
  
            case "H":
            case "h":
               newDate = new Date(year, month, date, hour + offset);
               break;

        }
        return newDate;            

    } 
    $("#priodDD").on('blur', function(){
        var dateStr = $("#engagementDate").val();
        var hiddenDate = $("#engPeriod").val();

        dArr = dateStr.split("-"); 
        var EngageDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val()) ;  
        if(month > 12 || date > 31){
            $("#priodMM").val('00');
            $("#priodDD").val('00') ; 
            $("#expireDate").val('');
            $("#engageFor").val('');
        }else{
            if($("#priodYY").val().length == 0){
                year = 0;
            }if($("#priodMM").val().length == 0){
                month = 0;
            }if($("#priodDD").val().length == 0){
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if(exp_day.toString().length == 1){
                exp_day = "0"+exp_day;
            }
            var exp_month = exp_date.getMonth()+1;
            if(exp_month.toString().length == 1){
                exp_month = "0"+exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year+"/"+exp_month+"/"+exp_day);

            /*start Engage for Year*/
            var end = new Date(exp_year+"-"+exp_month+"-"+exp_day);
            var cDate = new Date(EngageDate);
            var diff = new Date(end - cDate);
            var totalDays = diff/1000/60/60/24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if(totalYears.toString().length == 1){
                totalYears = "0"+totalYears;
            }
            if(totalMonths.toString().length == 1){
                totalMonths = "0"+totalMonths;
            }
            if(remainingDays.toString().length == 1){
                remainingDays = "0"+remainingDays;
            }
            $("#engageFor").val(totalYears+""+totalMonths+""+remainingDays);

            /*end Engage for Year*/
        }

    });
    $("#priodMM").on('blur', function(){
        var dateStr = $("#engagementDate").val();
        dArr = dateStr.split("-"); 
        var EngageDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val()) ;  
        if(month > 12 || date > 31){
            $("#priodMM").val('00');
            $("#priodDD").val('00') ;  
            $("#expireDate").val('');
            $("#engageFor").val('');
        }else{
            if($("#priodYY").val().length == 0){
                year = 0;
            }if($("#priodMM").val().length == 0){
                month = 0;
            }if($("#priodDD").val().length == 0){
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if(exp_day.toString().length == 1){
                exp_day = "0"+exp_day;
            }
            var exp_month = exp_date.getMonth()+1;
            if(exp_month.toString().length == 1){
                exp_month = "0"+exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year+"/"+exp_month+"/"+exp_day);

            /*start Engage for Year*/
            var end = new Date(exp_year+"-"+exp_month+"-"+exp_day);
            var cDate = new Date(EngageDate);
            var diff = new Date(end - cDate);
            var totalDays = diff/1000/60/60/24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if(totalYears.toString().length == 1){
                totalYears = "0"+totalYears;
            }
            if(totalMonths.toString().length == 1){
                totalMonths = "0"+totalMonths;
            }
            if(remainingDays.toString().length == 1){
                remainingDays = "0"+remainingDays;
            }
            $("#engageFor").val(totalYears+""+totalMonths+""+remainingDays);

            /*end Engage for Year*/
        }
    });
    $("#priodYY").on('blur', function(){
        var dateStr = $("#engagementDate").val();
        dArr = dateStr.split("-"); 
        var EngageDate = dArr[2]+ "-" +dArr[1]+ "-" +dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val());

        if(month > 12 || date > 31){
            $("#priodMM").val('00');
            $("#priodDD").val('00') ;  
            $("#expireDate").val('');
            $("#engageFor").val('');
        }else{
            if($("#priodYY").val().length == 0){
                year = 0;
            }if($("#priodMM").val().length == 0){
                month = 0;
            }if($("#priodDD").val().length == 0){
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if(exp_day.toString().length == 1){
                exp_day = "0"+exp_day;
            }
            var exp_month = exp_date.getMonth()+1;
            if(exp_month.toString().length == 1){
                exp_month = "0"+exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year+"/"+exp_month+"/"+exp_day);

            /*start Engage for Year*/
            var cDate = new Date(EngageDate);
            var end = new Date(exp_year+"-"+exp_month+"-"+exp_day);
            var diff = new Date(end - cDate);
            var totalDays = diff/1000/60/60/24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if(totalYears.toString().length == 1){
                totalYears = "0"+totalYears;
            }
            if(totalMonths.toString().length == 1){
                totalMonths = "0"+totalMonths;
            }
            if(remainingDays.toString().length == 1){
                remainingDays = "0"+remainingDays;
            }
            
            $("#engageFor").val(totalYears+""+totalMonths+""+remainingDays);
            /*end Engage for Year*/
        }
    });

</script>