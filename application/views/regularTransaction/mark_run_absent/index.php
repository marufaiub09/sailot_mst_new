<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                   
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Mark Run/Absent Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <form class="form-horizontal" id="MainForm" method="post">   
                        <fieldset class="">
                            <legend  class="legend">Current Info</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'runAbsentONo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Ship/Establishment</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'SHIP_ESTALISHMENT', "class" => "form-control SHIP_ESTALISHMENT required", 'required' => 'required', 'placeholder' => 'ship/establishment', 'readonly' => 'readonly', 'value' => set_value('SHIP_ESTALISHMENT'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Posting Unit</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'postingUnit', "class" => "form-control required PostingUnit", 'readonly' => 'readonly', 'placeholder' => 'posting unit')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Sailor Status</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'sailorStatus', "class" => "form-control sailorStatus", 'placeholder' => 'sailor status', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'postingDate', "class" => "form-control PostingDate", 'placeholder' => 'posting date', 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-12">
                    <hr style="margin-top: 0px;">
                    <legend  class="legend">Mark Run History</legend>
                    <div class="bs-example pull-right" data-example-id="split-button-dropdown" >
                        <div class="btn-group">
                            <button id="" class="btn btn-danger btn-sm deleteItem" title="Click For Delete" data-type="delete" data-field="MarkRunID" data-tbl="markrun" type="button">Delete</button>
                        </div>
                        <div class="btn-group">
                            <button id="markAbsent" class="btn btn-primary btn-sm markAbsent run_modalLink" data-modal-size="modal-md" href="<?php echo site_url('regularTransaction/markRunAbsent/mark_absent/1'); ?>" data-no ="" mark-run ="" title="Mark absent entry" type="button">Mark Absent</button>
                        </div>
                        <div class="btn-group">
                            <button id="markRun" class="btn btn-primary btn-sm markRun run_modalLink" data-modal-size="modal-md" href="<?php echo site_url('regularTransaction/markRunAbsent/mark_absent/2'); ?>" data-no ="" mark-run ="" title="Mark Run entry"  type="button">Mark Run</button>
                        </div>
                        <div class="btn-group">
                            <button id="surrender" class="btn btn-primary btn-sm surrender run_modalLink" data-modal-size="modal-md" href="<?php echo site_url('regularTransaction/markRunAbsent/mark_absent/3'); ?>" data-no ="" mark-run ="" title="Surrender entry"  type="button">Surrender</button>
                        </div>
                        <div class="btn-group">
                            <button id="runRemove" class="btn btn-danger btn-sm runRemove run_modalLink" data-modal-size="modal-md" href="<?php echo site_url('regularTransaction/markRunAbsent/mark_absent/4'); ?>" data-no ="" mark-run ="" title="Run remove entry"  type="button">Run Remove</button>
                        </div>
                        <div class="btn-group">
                            <button id="depot" class="btn btn-warning btn-sm deport run_modalLink" data-modal-size="modal-lg" href="<?php echo site_url('regularTransaction/markRunAbsent/mark_absent/5'); ?>" data-no ="" mark-run ="" title="Books of depot Information"  type="button">Books of Depot</button>
                        </div>
                    </div>
                    <br/>
                    <br/>
                    <table id="sailorTable"  class="table table-striped table-bordered " cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Absent Date</th>
                                <th>Absent DAO</th>
                                <th>Run Date</th>
                                <th>Run/Absent Ship</th>
                                <th>Run DAO</th>
                                <th>Surrender Date</th>
                                <th>Surrender DAO</th>
                                <th>Run Remove Date</th>
                                <th>Run Remove By</th>
                                <th>Run Remove Ship</th>
                                <th>Run Remove DAO</th>
                            </tr>
                        </thead>
                        <tbody class="contentArea">
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $('.deleteItem').prop('disabled', true);
    $('.markAbsent').prop('disabled', true);
    $('.markRun').prop('disabled', true);
    $('.surrender').prop('disabled', true);
    $('.runRemove').prop('disabled', true);
    $('.deport').prop('disabled', true);
     $("#sailorTable  tbody").empty();
    /*start search sailorInfo, rank_name, ship_establishment, Posting_unit, Posting_date by Official Number */
    $("#runAbsentONo").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber},
                dataType: "json",
                url: "<?php echo site_url(); ?>regularTransaction/markRunAbsent/searchSailorInfoByOfficalNo",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $.ajax({
                            type: "post",
                            data: {sailorId: data['SAILORID'] },
                            dataType: "json",
                            url: "<?php echo site_url(); ?>regularTransaction/markRunAbsent/searchMarkRunAbsentInfo",
                            success: function (data1) {
                                $("#sailorTable  tbody").empty();
                                var newRowContent = '';
                                $.each(data1, function(i, item) {
                                    var MarkRunID = item.MarkRunID;
                                    var AbsentDate = (item.AbsentDate == null ) ? '' : item.AbsentDate;
                                    var RunDate = (item.RunDate == null ) ? '' : item.RunDate;
                                    var SurrenderDate = (item.SurrenderDate == null ) ? '' : item.SurrenderDate;
                                    var RemoveDate = (item.RemoveDate == null ) ? '' : item.RemoveDate;
                                    var REMOVE_SHIP = (item.REMOVE_SHIP == null ) ? '' : item.REMOVE_SHIP;
                                    var checkbox = '<input type="checkbox" status="'+ item.Status +'" class="checkboxMarkRun" name="markRun" value="'+ item.MarkRunID +'">';
                                    newRowContent += '<tr class = common id= row_'+ MarkRunID +'><td>' + 
                                                        checkbox  + '</td><td>' + 
                                                        AbsentDate  + '</td><td>' + 
                                                        item.AbsentDAONumber + '</td><td>' + 
                                                        RunDate + '</td><td>' + 
                                                        item.RUN_SHIP + '</td><td>' + 
                                                        item.RunDAONumber + '</td><td>' + 
                                                        SurrenderDate + '</td><td>' + 
                                                        item.SurrenderDAONumber + '</td><td>' + 
                                                        RemoveDate + '</td><td>' + 
                                                        item.RemoveBy + '</td><td>' + 
                                                        REMOVE_SHIP + '</td><td>' + 
                                                        item.RemoveDAONumber + '</td></tr>';
                                    
                                    $('.deleteItem').prop('disabled', true);
                                    $('.markAbsent').prop('disabled', false);
                                    $('.markRun').prop('disabled', false);                                    
                                });
                                $("#sailorTable tbody").append(newRowContent);
                            }
                        });

                        /*Test button href*/
                        $( "button" ).attr( "data-no",  data['SAILORID']);

                        /*end*/

                       /* $('.markAbsent').prop('disabled', false);
                        $('.markRun').prop('disabled', false);*/

                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);  
                        $(".SHIP_ESTALISHMENT").val(data['SHIP_ESTABLISHMENT']);  
                        $(".PostingUnit").val(data['POSTING_UNIT_NAME']);
                        $(".PostingDate").val(data['POSTING_DATE']); 
                        var status = data['SAILORSTATUS'];
                        var statusName; 
                        if(status == 0 ){
                            statusName = "None";
                        }else if(status == 1){
                            statusName = "Active";
                        }else if(status == 2){
                            statusName = "Retirement In Process";
                        }else if(status == 3){
                            statusName = "Released";
                        }else if(status == 4){
                            statusName = "Books of Depot";
                        }
                        $(".sailorStatus").val(statusName);                 
                        $(".alertSMS").html('');
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');      
                        $(".SHIP_ESTALISHMENT").val('');      
                        $(".PostingUnit").val('');
                        $(".PostingDate").val('');
                        $(".sailorStatus").val('');
                        $(".alertSMS").html('Invalid Officer Number');      
                        $("#sailorTable  tbody").empty();
                    }
                }
            });
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');      
            $(".SHIP_ESTALISHMENT").val('');      
            $(".PostingUnit").val('');
            $(".PostingDate").val('');
            $(".entryType").val('');
            $(".alertSMS").html('');
            $("#sailorTable  tbody").empty();
        }
    });
    
    $('#sailorTable').removeAttr('width').DataTable( {  
        "scrollY": 100,
        "scrollX": true,
        "bPaginate": false,
        "bFilter": false, 
        "bInfo": false,
        "bSort" : false,
        "columnDefs": [
            { width: '10px', targets: 0 },
            { width: '100px', targets: 1 },
            { width: '100px', targets: 2 },
            { width: '80px', targets: 3 },
            { width: '80px', targets: 5 },
            { width: '90px', targets: 6 },
            { width: '70px', targets: 7 },
            { width: '90px', targets: 8 },
            { width: '90px', targets: 9 },
        ],
        "fixedColumns": true,
        "oLanguage": {"sZeroRecords": "", "sEmptyTable": ""}

    } );

    // dynamic modal
    $(document).on("click", ".run_modalLink", function (e) { 
        e.preventDefault();
        var officialNo = $(".sailorId").val();
        var markRunId = $(this).attr("id");
        var sailorId = $(this).attr('data-no');
        var href = $(this).attr('href');
        if(!isNaN(markRunId)){
                href =  href+"/"+sailorId+"/"+markRunId;
        }else{
                href =  href+"/"+sailorId;
        }
        if(officialNo != ''){

            var modal_size = $(this).attr('data-modal-size');
            if ( modal_size!=='' && typeof modal_size !== typeof undefined && modal_size !== false ) {
                $("#modalSize").addClass(modal_size);
            }
            else{
                $("#modalSize").addClass('modal-md');
            }
            var title = $(this).attr('title');
            $("#showDetaildModalTile").text(title);

            var data_title = $(this).attr('data-original-title');
            $("#showDetaildModalTile").text(data_title);

            $.ajax({
                type: "POST",
                url: href ,
                //data: $.parseJSON($(this).attr('mmh_data')),
                success: function (data) {
                    $("#showDetaildModalBody").html(data);
                    $("#showDetaildModal").modal('show');
                }
            });
        }else{
            alert("Please enter valid official number");
        }
    });
    $(document).on('click', '.checkboxMarkRun', function(e) {
        var isChecked = $(this).is(':checked');
        var status = $(this).attr('status'); /*mark run status 1= mark Absent, 2 = mark Run, 3 = surrender, 4 = Run remove, 5 = Books of Deport*/        
        var id = $(this).val();
        if(isChecked){
            $("button" ).attr( "id",  id );
            $(".common").css("background-color", "");
            $('input:checkbox').not(this).prop('checked', false);   
            $("#row_" + id).css("background-color", "#0381A8");
            if(status == 1 || status == 2){
                $('.deleteItem').prop('disabled', false);     
                $('.markAbsent').prop('disabled', false);
                $('.markRun').prop('disabled', false);
                $('.surrender').prop('disabled', false);
                $('.runRemove').prop('disabled', true);
                if(status == 2){
                    $('.deport').prop('disabled', false);                    
                }
            }else if(status == 3){
                $('.deleteItem').prop('disabled', false);     
                $('.markAbsent').prop('disabled', true);
                $('.markRun').prop('disabled', true);
                $('.surrender').prop('disabled', false);
                $('.runRemove').prop('disabled', false);
                $('.deport').prop('disabled', true);
            }else if(status == 4){
                $('.deleteItem').prop('disabled', false);     
                $('.markAbsent').prop('disabled', true);
                $('.markRun').prop('disabled', true);
                $('.surrender').prop('disabled', true);
                $('.runRemove').prop('disabled', false);
                $('.deport').prop('disabled', true);
            }
        }else{
            $("#row_" + id).css("background-color", "");
            $("button" ).attr( "id",  '' );

            if(status == 1){

            }else if(status == 2){

            }else if(status == 3){

            }else if(status == 4){

            }
            $('.markAbsent').prop('disabled', false);
            $('.markRun').prop('disabled', false);

            $('.deleteItem').prop('disabled', true);     
            $('.surrender').prop('disabled', true);
            $('.runRemove').prop('disabled', true);
            $('.deport').prop('disabled', true);
        }
    });
    $(document).on("click", ".formSubmitMarkRun", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                //alert(label + " Is Empty");
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
                //return false;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm("Are You Sure?")) {
                var frmContent = $(".frmContent").serialize();
                var action_uri = $(this).attr("data-action");
                var type = $(this).attr("data-type");
                var success_action_uri = $(this).attr("data-su-action");
                var param = $(this).attr("sailorId");
                var ac_type = $(this).attr("");
                $.ajax({
                    type: "post",
                    data: frmContent,
                    url: "<?php echo site_url(); ?>/" + action_uri,
                    beforeSend: function () {
                        $(".loadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".loadingImg").html("");
                        $(".frmMsg").html(data);
                        $.ajax({
                            type: "post",
                            data: {sailorId: param},
                            url: "<?php echo site_url(); ?>/" + success_action_uri,
                            dataType: "json",
                            beforeSend: function () {
                                if (type != "list") {
                                    $("#loader_" + param).removeClass("hidden").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' style='width:10px;' />").siblings("span").addClass("hidden");
                                }
                            },
                            success: function (data1) {
                               //$(".contentArea").html(data1);
                                $("#sailorTable  tbody").empty();
                                var newRowContent = '';
                                $.each(data1, function(i, item) {
                                    var MarkRunID = item.MarkRunID;
                                    var AbsentDate = (item.AbsentDate == null ) ? '' : item.AbsentDate;
                                    var RunDate = (item.RunDate == null ) ? '' : item.RunDate;
                                    var SurrenderDate = (item.SurrenderDate == null ) ? '' : item.SurrenderDate;
                                    var RemoveDate = (item.RemoveDate == null ) ? '' : item.RemoveDate;
                                    var REMOVE_SHIP = (item.REMOVE_SHIP == null ) ? '' : item.REMOVE_SHIP;
                                    var checkbox = '<input type="checkbox" status="'+ item.Status +'" class="checkboxMarkRun" name="markRun" value="'+ item.MarkRunID +'">';
                                    newRowContent += '<tr class = common id= row_'+ MarkRunID +'><td>' + 
                                                        checkbox  + '</td><td>' + 
                                                        AbsentDate  + '</td><td>' + 
                                                        item.AbsentDAONumber + '</td><td>' + 
                                                        RunDate + '</td><td>' + 
                                                        item.RUN_SHIP + '</td><td>' + 
                                                        item.RunDAONumber + '</td><td>' + 
                                                        SurrenderDate + '</td><td>' + 
                                                        item.SurrenderDAONumber + '</td><td>' + 
                                                        RemoveDate + '</td><td>' + 
                                                        item.RemoveBy + '</td><td>' + 
                                                        REMOVE_SHIP + '</td><td>' + 
                                                        item.RemoveDAONumber + '</td></tr>';
                                });
                                $("#sailorTable tbody").append(newRowContent);
                            }
                        });
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });

</script>