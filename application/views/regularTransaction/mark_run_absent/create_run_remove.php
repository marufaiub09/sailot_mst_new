<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="col-md-12">        
        <div class="col-md-6">
            <legend  class="legend">Surrender Information</legend>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">Surrender Date</label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'surrenderDate',  "class" => "datePicker surrenderDate form-control required", 'required'=>'required', 'value' => ($markRunId == 0)? Date('d-m-Y'): (($markRun->SurrenderDate == null )? date('d-m-Y') : date('d-m-Y', strtotime($markRun->SurrenderDate))))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">Authority No</label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'suAuthorityNo',  "class" => "suAuthorityNo form-control required", 'required'=>'required', 'value' => ($markRunId == 0)? '': $markRun->SurrenderAuthorityNo)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">Authority Date</label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'suAuthorityDate',  "class" => "datePicker suAuthorityDate form-control required", 'required'=>'required', 'value' => ($markRunId == 0)? Date('d-m-Y'): (($markRun->SurrenderAuthorityDate == null )? date('d-m-Y') : date('d-m-Y', strtotime($markRun->SurrenderAuthorityDate))))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">DAO Number</label>     
                <div class="col-sm-6">
                    <select class="select2 form-control required" name="su_DAO_ID" id="su_DAO_ID" data-tags="true" data-placeholder="Select DAO " data-allow-clear="true">
                        <option value="">Select DAO Number</option>
                        <?php
                        foreach ($dao as $row):
                            ?>
                            <option value="<?php echo $row->DAO_ID ?>" <?php echo  ($markRunId != 0)? ( ($markRun->SurrenderDAOID == $row->DAO_ID) ? 'selected' : '') : '' ?>><?php echo $row->DAO_NO ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>                
            </div>
            <legend  class="legend"></legend>
        </div>
        <div class="col-md-6">
            <legend  class="legend">'R' remove Information</legend>
            <div class="form-group">
                <label class="col-sm-5 control-label">'R' remove Date</label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'runRemoveDate',  "class" => "datePicker runDate form-control required", 'required'=>'required', 'value' => Date('d-m-Y'))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Authority No</label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'runRemoveAuthoNo',  "class" => "runAuthoNo form-control required", 'required'=>'required', 'value' => '')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Authority Date</label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'runRemoveAuthoDate',  "class" => "datePicker runAuthoDate form-control required", 'required'=>'required', 'value' => Date('d-m-Y'))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">DAO Number</label>     
                <div class="col-sm-6">
                    <select class="select2 form-control required" name="run_remove_DAO_ID" id="run_DAO_ID" data-tags="true" data-placeholder="Select DAO " data-allow-clear="true">
                        <option value="">Select DAO Number</option>
                        <?php
                        foreach ($dao as $row):
                            ?>
                            <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>                
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label">Run Remove By</label>     
                <div class="col-sm-6">
                    <select class="select2 form-control required" name="run_remove_by" id="run_DAO_ID" data-tags="true" data-placeholder="Select One " data-allow-clear="true">
                        <option value="1">CO</option>
                        <option value="2">CNS</option>                        
                    </select>
                </div>                
            </div>
            <legend  class="legend"></legend>
        </div>
    </div>
	<div class="form-group">
		<label class="col-sm-3 control-label">'R' Remove Ship</label>
		<div class="col-sm-5">
			<select class="select2 form-control required" name="remove_ship" id="remove_ship" data-tags="true" data-placeholder="Select ship" data-allow-clear="true">
                <option value="">Select Ship</option> 
                <?php
                foreach ($shipEst as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo  ($markRunId != 0)? ( ($markRun->RunShipID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '') : '' ?> ><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
		</div>
	</div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="sailorId" id="sailorId" value="<?php echo $sailorId; ?>">
            <input type="hidden" name="Status" id="Status" value="<?php echo $flag; ?>">
            <input type="hidden" name="MarkRunID" id="MarkRunID" value="<?php echo ($markRunId == 0)? '' : $markRun->MarkRunID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmitMarkRun" sailorId="<?php echo $sailorId; ?>" data-action="regularTransaction/markRunAbsent/runRemoveSave" data-su-action="regularTransaction/markRunAbsent/searchMarkRunAbsentInfo" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php if($flag == 4){ ?>
    <style>
        .runTransfarent {
            background-color: white;
            opacity: 0.5;        
        }
    </style>
<?php }?>
<script>
    var flag = "<?php echo $flag; ?>";

    if(flag == 4){
        $('.surrenderDate').prop('disabled', true);
        $('.suAuthorityNo').prop('disabled', true);
        $('.suAuthorityDate').prop('disabled', true);
        $('#su_DAO_ID').prop('disabled', true);
        $('#SHIP_ESTABLISHMENT').prop('disabled', true);
        $('#su_DAO_ID').removeClass('required');
        $('.suAuthorityNo ').removeClass('required');
        $('#SHIP_ESTABLISHMENT').removeClass('required');
    }
</script>