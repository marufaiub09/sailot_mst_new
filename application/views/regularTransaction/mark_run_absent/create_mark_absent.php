<form class="form-horizontal frmContent" id="MainForm" method="post">
    <span class="frmMsg"></span>
    <div class="col-md-12">
        <div class="col-md-6">
            <legend  class="legend"> <?php echo ($flag == 1)? 'Mark absent information':'Run mark information'?> </legend>
            <div class="form-group">
                <label class="col-sm-5 control-label absTransfarent">Absent Date <span class="text-danger">*</span></label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'absentDate',  "class" => "datePicker absentDate form-control required absInput", 'required'=>'required', 'value' => ($markRunId == 0)? Date('d-m-Y'): date('d-m-Y', strtotime($markRun->AbsentDate)))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label absTransfarent">Authority No <span class="text-danger">*</span></label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'abAuthorityNo',  "class" => "abAuthorityNo form-control required absInput", 'required'=>'required', 'value' => ($markRunId == 0)? '': $markRun->AbsentAuthorityNo)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label absTransfarent">Authority Date <span class="text-danger">*</span></label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'abAuthorityDate',  "class" => "datePicker abAuthorityDate form-control required absInput", 'required'=>'required', 'value' =>($markRunId == 0)? Date('d-m-Y'): date('d-m-Y', strtotime($markRun->AbsentDate)))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label absTransfarent">DAO Number <span class="text-danger">*</span></label>     
                <div class="col-sm-6">
                    <select class="select2 form-control required absInput" name="ab_DAO_ID" id="ab_DAO_ID" data-tags="true" data-placeholder="Select DAO " data-allow-clear="true">
                        <option value="">Select DAO Number</option>
                        <?php
                        foreach ($dao as $row):
                            ?>
                            <option value="<?php echo $row->DAO_ID ?>" <?php echo  ($markRunId != 0)? ( ($markRun->AbsentDAOID == $row->DAO_ID) ? 'selected' : '') : '' ?> ><?php echo $row->DAO_NO ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>                
            </div>
            <legend  class="legend"></legend>
        </div>
        <div class="col-md-6">
            <legend  class="legend">Run Information</legend>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">Run Date <span class="text-danger">*</span></label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'runDate',  "class" => "datePicker runDate form-control required runInput", 'required'=>'required', 'value' => ($markRunId == 0)? Date('d-m-Y'): (($markRun->RunDate == null )? date('d-m-Y') : date('d-m-Y', strtotime($markRun->RunDate))))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">Authority No <span class="text-danger">*</span></label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'runAuthoNo',  "class" => "runAuthoNo form-control required runInput", 'required'=>'required', 'value' => ($markRunId == 0)? '': $markRun->RunAuthorityNo)); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">Authority Date <span class="text-danger">*</span></label>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'runAuthoDate',  "class" => "datePicker runAuthoDate form-control required runInput", 'required'=>'required', 'value' => ($markRunId == 0)? Date('d-m-Y'): (($markRun->RunAuthorityDate == null )? date('d-m-Y') : date('d-m-Y', strtotime($markRun->RunAuthorityDate))))); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-5 control-label runTransfarent">DAO Number <span class="text-danger">*</span></label>     
                <div class="col-sm-6">
                    <select class="select2 form-control required runInput" name="run_DAO_ID" id="run_DAO_ID" data-tags="true" data-placeholder="Select DAO " data-allow-clear="true">
                        <option value="">Select DAO Number</option>
                        <?php
                        foreach ($dao as $row):
                            ?>
                            <option value="<?php echo $row->DAO_ID ?>" <?php echo  ($markRunId != 0)? ( ($markRun->RunDAOID == $row->DAO_ID) ? 'selected' : '') : '' ?>><?php echo $row->DAO_NO ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>                
            </div>
            <legend  class="legend"></legend>
        </div>
    </div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Authority Ship <span class="text-danger">*</span></label>
		<div class="col-sm-5">
			<select class="select2 form-control required" name="SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT" data-tags="true" data-placeholder="Select ship establishment" data-allow-clear="true">
                <option value="">Select Authority Ship</option> 
                <?php
                foreach ($shipEst as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo  ($markRunId != 0)? ( ($markRun->RunShipID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '') : '' ?> ><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
		</div>
	</div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="sailorId" id="sailorId" value="<?php echo $sailorId; ?>">
            <input type="hidden" name="Status" id="Status" value="<?php echo $flag; ?>">
            <input type="hidden" name="MarkRunID" id="MarkRunID" value="<?php echo ($markRunId == 0)? '' : $markRun->MarkRunID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmitMarkRun" sailorId="<?php echo $sailorId; ?>" data-action="regularTransaction/markRunAbsent/markAbsentSave" data-su-action="regularTransaction/markRunAbsent/searchMarkRunAbsentInfo" data-type="list" value="submit">
            <?php if($markRunId != 0 && $markRun->Status == 2){  ?>
                <input type="button" class="btn btn-primary btn-sm formSubmitMarkRun" sailorId="<?php echo $sailorId; ?>" data-action="regularTransaction/markRunAbsent/runDelete" data-su-action="regularTransaction/markRunAbsent/searchMarkRunAbsentInfo" data-type="list" value="Delete">
            <?php } ?>
        </div>
    </div>
</form>
<?php if($flag == 1){ ?>
    <style>
        .runTransfarent {
            background-color: white;
            opacity: 0.5;        
        }
        .runInput{
            opacity: 0.5 !important;
        }
    </style>
<?php } else { ?>
    <style>
        .absTransfarent {
            background-color: white;
            opacity: 0.5;        
        }
        .absInput{
            opacity: 0.5 !important;
        }
    </style>
<?php } ?>
<script>
    var flag = "<?php echo $flag; ?>";
    if(flag == 2){
        $('.absentDate').prop('disabled', true);
        $('.abAuthorityNo').prop('disabled', true);
        $('.abAuthorityDate').prop('disabled', true);
        $('#ab_DAO_ID').prop('disabled', true);
        $('.abAuthorityNo').removeClass('required');
        $('#ab_DAO_ID').removeClass('required');
    }else {
        $('.runDate').prop('disabled', true);
        $('.runAuthoNo').prop('disabled', true);
        $('#run_DAO_ID').prop('disabled', true);
        $('.runAuthoDate').prop('disabled', true);
        $('.runAuthoNo').removeClass('required');
        $('#run_DAO_ID').removeClass('required');
    }
</script>