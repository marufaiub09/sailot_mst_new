<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Posting Unit Name</th>
        <td><?php echo $viewdetails->POSTING_UNIT_NAME ?></td>
    </tr>
    <tr>
        <th>Posting Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->POSTINGDATE)) ?></td>
    </tr>
    <tr>
        <th>Appointment Type</th>
        <td><?php echo $viewdetails->Appoint_Type ?></td>
    </tr>
    <tr>
        <th>Authority ship Establishment</th>
        <td><?php echo $viewdetails->AUTHO_SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Draft In Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->DraftInDate)) ?></td>
    </tr>
    <tr>
        <th>Draft Out Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->DraftOutDate)) ?></td>
    </tr>
    <tr>
        <th>Transfer Order No</th>
        <td><?php echo $viewdetails->TransferOrderNo ?></td>
    </tr>
    <tr>
        <th>Transfer Order Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->TransferOrderDate)) ?></td>
    </tr>
    <tr>
        <th>Order Number</th>
        <td><?php echo $viewdetails->GenFormNo ?></td>
    </tr>
    <tr>
        <th>Order Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->GenFormDate)) ?></td>
    </tr>
    <tr>
        <th>Remarks</th>
        <td><?php echo $viewdetails->Remarks ?></td>
    </tr>
</table>