<form class="form-horizontal frmContent" id="MainForm" method="post">
    <span class="frmMsg"></span>
        <div class="col-md-12">
            <fieldset class="">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Official Number</label>        
                        <div class="col-sm-6" >
                            <?php echo form_input(array('name' => 'officialNo',  'value' => $sailorInfo->OFFICIALNUMBER, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number', 'readonly' => 'readonly')); ?>           
                            <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="<?php echo $sailorInfo->SAILORID ?>">           
                            <input type="hidden" name="SHIP_EST" id="SHIP_EST" class="sailorId" value="<?php echo $sailorInfo->SHIPESTABLISHMENTID ?>">           
                            <input type="hidden" name="POSTING_UNIT" id="POSTING_UNIT" class="sailorId" value="<?php echo $sailorInfo->POSTINGUNITID ?>">          
                        </div>
                        <div class="col-sm-1">
                            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                <i class="fa fa-question-circle"></i>
                            </a>
                        </div>
                        <div class="col-md-12" >
                            <div class="col-sm-4"></div>
                            <div class="col-sm-8 danger">
                            <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Posting Unit</label>
                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => $sailorInfo->POSTING_NAME)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Sailor status</label>
                        <div class="col-sm-6">
                            <?php 
                                if($sailorInfo->SAILORSTATUS ==1) {
                                    $status = "Active";
                                }else if($sailorInfo->SAILORSTATUS == 3){
                                    $status = "Released";
                                }else {
                                    $status = "Books of Depot";
                                }
                                echo form_input(array('name' => 'sailorStatus', "class" => "form-control  required", 'required' => 'required', 'placeholder' => 'Sailor Status', 'readonly' => 'readonly', 'value' => $status)); 
                            ?>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Details</label>
                        <div class="col-sm-4">
                            <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => $sailorInfo->FULLNAME)); ?>
                        </div>
                        <div class="col-sm-4">
                            <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => $sailorInfo->RANK_NAME)); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Posting Date</label>
                        <div class="col-sm-5">
                            <?php echo form_input(array('name' => 'PostingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly', 'value' => date('d-m-Y', strtotime($sailorInfo->POSTINGDATE)))); ?>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="col-md-12">        
            <legend  class="legend">Transfer Execute Information</legend>
            <div class="form-group">
                <label class="col-sm-2 control-label">Effecting Date</label>
                <div class="col-sm-3">
                   <?php echo form_input(array('name' => 'effectingDate',  "class" => "datePicker runDate form-control required", 'required'=>'required', 'value' => Date('d-m-Y'))); ?>
                </div>
                <label class="col-sm-2 control-label">Authority No</label>
                <div class="col-sm-3">
                   <?php echo form_input(array('name' => 'bookAuthoNo',  "class" => "runAuthoNo form-control required", 'required'=>'required', 'value' => '')); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Authority Date</label>
                <div class="col-sm-3">
                   <?php echo form_input(array('name' => 'bookAuthoDate',  "class" => "datePicker runAuthoDate form-control required", 'required'=>'required', 'value' => Date('d-m-Y'))); ?>
                </div>
        		<label class="col-sm-2 control-label">Authority Ship</label>
        		<div class="col-sm-3">
        			<select class="select2 form-control required" name="SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT" data-tags="true" data-placeholder="Select ship establishment" data-allow-clear="true">
                        <option value="">Select Authority Ship</option> 
                        <?php
                        foreach ($shipEst as $row):
                            ?>
                            <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"><?php echo $row->NAME ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
        		</div>
        	</div>            
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">&nbsp;</label>
            <div class="col-sm-2">&nbsp;&nbsp;
                <input type="hidden" name="sailorId" id="sailorId" value="<?php echo $sailorId; ?>">
                <input type="hidden" name="Status" id="Status" value="<?php echo $flag; ?>">
                <input type="hidden" name="MarkRunID" id="MarkRunID" value="<?php echo ($markRunId == 0)? '' : $markRun->MarkRunID; ?>">
                <input type="button" class="btn btn-primary btn-sm formSubmitMarkRun" sailorId="<?php echo $sailorId; ?>" data-action="regularTransaction/markRunAbsent/booksDeportSave" data-su-action="regularTransaction/markRunAbsent/searchMarkRunAbsentInfo" data-type="list" value="submit">
            </div>
        </div>
</form>
<script>
    var flag = "<?php echo $flag; ?>";
    if(flag == 3){
        $('.runDate').prop('disabled', true);
        $('.runAuthoNo').prop('disabled', true);
        $('.runAuthoDate').prop('disabled', true);
        $('#run_DAO_ID').prop('disabled', true);
        $('#SHIP_ESTABLISHMENT').prop('disabled', true);
    }
</script>