<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Re-Engagement Information</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs "  href="<?php echo site_url('regularTransaction/reEngagementInfo/create'); ?>" title="Add Re-Engagement Informaion">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>
                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" style="margin-top: 5px;"><b>Official Number</b></label>
                        <div class="col-sm-3" >
                            <?php echo form_input(array('name' => 'officialNumber', "id"=>"searchOfficialNo", "class" => "form-control")); ?>
                        </div>
                        <label class="col-sm-6 control-label" style="margin-top: 5px;  padding-left: 0 !important;"> 
                            <span class="smloadingImg"></span>
                            <span class="alertSMS label label-danger"></span> <span class="fullName"></span><span class="rank"></span>
                        </label>
                    </div>                                
                </div>
            </div>
            <div class="panel-body" id="contentArea">
            </div>
        </div>
    </div>
</div>
<script>
    $("#contentArea").hide();
    /*start search sailorInfo, rank_name by Official Number */
    $("#searchOfficialNo").on('blur', function(){
        var flag = "<?php echo $flag; ?>";
        var sailorStatus = (flag >=1 ? '3' : '1');
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:sailorStatus},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailor",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        var sailorId = data['SAILORID'];
                        $(".fullName").text(data['FULLNAME']+', Rank: ');
                        $(".rank").text(data['RANK_NAME']);  
                        $(".alertSMS").html(''); 
                        $("#contentArea").show();
                         $.ajax({
                            type: "post",
                            data: {sailorId: sailorId},
                            dataType: "html",
                            url: "<?php echo site_url(); ?>regularTransaction/reEngagementInfo/searchEngagementInfo",
                            success: function(dataSailor){
                                $("#contentArea").html(dataSailor);
                            }
                        });
                    }else{
                        $(".sailorId").text('');
                        $(".fullName").text('');
                        $(".rank").text('');      
                        $(".alertSMS").html('Invalid Officer Number');      
                        $("#contentArea").hide();
                    }
                }
            });            
        }else{
            $(".sailorId").text('');
            $(".fullName").text('');
            $(".rank").text(''); 
            $(".alertSMS").html(''); 
             $("#contentArea").hide();
        }
    });
    /*end Official Number search part*/
</script>
