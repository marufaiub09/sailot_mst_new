<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Engagement NO</th>
        <td><?php echo $viewdetails->EngagementNo ?></td>
    </tr>
    <tr>
        <th>Engagement Date</th>
        <td><?php  echo date("d-m-Y", strtotime($viewdetails->EngagementDate)) ?></td>
    </tr>
    <tr>
        <th>Engage For (Year)</th>
        <td><?php echo $viewdetails->EngagementYear ?></td>
    </tr>
    <tr>
        <th>Engagement Period</th>
        <td><?php echo $viewdetails->EngagementYear.''.$viewdetails->EngagementMonth.''.$viewdetails->EngagementDay ?></td>
    </tr>
    <tr>
        <th>Expire Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->ExpiryDate)) ?></td>
    </tr>
    <!-- <tr>
        <th>Authority Ship Area</th>
        <td><?php echo $viewdetails->AUTHO_SHIP_EST ?></td>
    </tr> -->
   <!--  <tr>
        <th>Authority Number</th>
        <td><?php //echo $viewdetails->TransferOrderNo ?></td>
    </tr> -->
    <tr>
        <th>Authority Ship Name</th>
        <td><?php  echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Engagement Type</th>
        <td><?php  echo $viewdetails->entryName ?></td>
    </tr>
    <tr>
        <th>DAO Number</th>
        <td><?php echo $viewdetails->DAONo ?></td>
    </tr>
    <tr>
        <th>Entry Type</th>
        <td><?php  echo $viewdetails->Appoint_Type ?></td>
    </tr>
</table>
