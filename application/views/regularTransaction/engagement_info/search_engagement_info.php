<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var dataTable = $('.dataTable').DataTable({
            "bFilter": false, 
            "bInfo": false,
            "bPaginate": false,
        });
    });
</script>
<table id="dataTable" class="dataTable display table-striped " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Engagement Type</th>
            <th>Date</th>
            <th>Period</th>
            <th>Expire Date</th>
            <th>Engage For</th>
            <th>DAO NO</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
	<?php
    $sn = 1;
    $temp = 0;
    $engageType = array(
                '1' => "Normal",
                '2' => "Extension",
                '3' => "Compulsary Extension",
                '4' => "Punishment Extension",
                '5' => "Hospital Extension"
                );
	foreach ($engagement as $row) { ?>
		<tr id="row_<?php echo $row->EagagementID ?>">
            <td> <?php echo $sn; ?> </td>
            <td><?php echo array_key_exists($row->EngagementType, $engageType)? $engageType[$row->EngagementType] : ''; ?></td>
            <td><?php echo date("d-m-Y", strtotime($row->EngagementDate)); ?></td>
            <td>
                <?php 
                    if(strlen($row->EngagementYear) == 1){
                        $year = "0".$row->EngagementYear;
                    }else{
                        $year = $row->EngagementYear;
                    }
                    if(strlen($row->EngagementMonth) == 1){
                        $month = "0".$row->EngagementMonth;
                    }else{
                        $month = $row->EngagementMonth;
                    }
                    if(strlen($row->EngagementDay) == 1){
                        $day = "0".$row->EngagementDay;
                    }else{
                        $day = $row->EngagementDay;
                    }
                    
                    echo $year.$month.$day;
                    $engagementFor =  (int)$year.$month.$day;
                ?>
            </td>
            <td><?php echo date("d-m-Y", strtotime($row->ExpiryDate)); ?></td>
            <td>
                <?php 
                    echo $engNext = $engagementFor + $temp;
                    $temp = $engNext;
                ?>
                
            </td>
            <td><?php echo $row->DAONo ?></td>
            <td>
                <?php echo '<a class="btn btn-success btn-xs modalLink" href="'.site_url('regularTransaction/reEngagementInfo/view/' . $row->EagagementID) .'" title="View Re-engagement Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                                '<a class="btn btn-warning btn-xs" href="'.site_url('regularTransaction/reEngagementInfo/edit/' . $row->EagagementID) .'" title="Edit Re-engagement Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                                '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->EagagementID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="MarriageID" data-tbl="marriage"><span class="glyphicon glyphicon-trash"></span></a>';
                ?>                
            </td>
        </tr>
	<?php }?>
	</tbody>
</table>
