<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <p><u>Current Information</u></p>
    <div class="form-group">
        <label class="col-sm-3 control-label">Official No</label>        
        <div class="col-sm-3" >
            <?php $id = ($flag == 1)? 'retofficialNumber':'officialNumber'; ?>
            <?php echo form_input(array('name' => 'officialNumber', 'id' => $id, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
            <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
        <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Full Name</label>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>

        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Posting Unit</label>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
        </div>
        <label class="col-sm-2 control-label">Posting Date</label>

        <div class="col-sm-3">

            <?php echo form_input(array('name' => 'PostingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly')); ?>
        </div>
    </div>
    <p><u>Add/Edit Information</u></p>
    <div class="form-group">
        <label class="col-sm-3 control-label">Child Name</label>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'ChildName', "class" => "form-control  required", 'required' => 'required', 'placeholder' => 'Child Name', 'value' => set_value('ChildName'))); ?>
        </div>
        <label class="col-sm-2 control-label">Birth Date</label>

        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'birthDate', "class" => " datePicker form-control  required", 'required' => 'required', 'placeholder' => 'Birth Date', 'value' => date('d-m-Y'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Gender</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Gender">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="Gender" id="Gender" data-tags="true" data-placeholder="Select Gender" data-allow-clear="true">
                <option value="">Select Gender</option>
                <option value="0">Male</option>
                <option value="1">Female</option>
                <option value="2">Others</option>
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Child Name Bangla</label>
        <div class="col-sm-4">
            <?php echo form_input(array('name' => 'ChildNameBangla', "class" => "form-control", 'placeholder' => 'Child Name Bangla', 'value' => set_value('ChildNameBangla'))); ?>
        </div>

    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Authority Number</label>
        <div class="col-sm-4">
            <?php echo form_input(array('name' => 'authorityNumber', "class" => "form-control  required", 'required' => 'required', 'placeholder' => 'Authority Number', 'value' => set_value('authrityNumber'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Authority Date</label>

        <div class="col-sm-3 date">
            <?php echo form_input(array('name' => 'AuthorityDate', "class" => "datePicker form-control required", 'required' => 'required', 'value' => date('d-m-Y'), 'placeholder' => 'Authority date')); ?>
        </div>
        <label class="col-sm-2 control-label">DAO</label>

        <div class="col-sm-3">
            <select class="select2 form-control required" name="DAO_NUMBER" id="DAO_NUMBER" data-tags="true" data-placeholder="Select DAO" data-allow-clear="true">
                <option value="">Select DAO</option>
                <?php
                foreach ($daoNumber as $row):
                    ?>
                    <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <p><u>Authority Ship</u><p>
    <div class="form-group">
        <label class="col-sm-3 control-label">Zone</label>

        <div class="col-sm-3 date">
            <?php echo form_dropdown("zone", $zone, set_value("zone"), "class='select2 form-control' id='zone'  data-placeholder='Select Zone' aria-hidden='true' data-allow-clear='true'" ); ?>
        </div>
        <label class="col-sm-2 control-label">Area</label>

        <div class="col-sm-3" >
            <select  id="area" name="" class="select2 form-control" data-placeholder="Select Area" aria-hidden="true" data-allow-clear="true">
                <option value="">Select Area</option>
            </select>
        </div>  
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Ship Establishment</label>
        <div class="col-sm-4" >
            <select  id="ship" name="shipEstId" class="select2 form-control" data-placeholder="Select Ship establishment" aria-hidden="true" data-allow-clear="true">
                <option value="">Select Ship Est.</option>
            </select>
        </div>    
    </div>
    <div class="form-group" >
        <div id="shipNumber">
        </div>    

        <div id="shipName">

        </div>                                    
    </div>
<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/childrenInfo/save':'regularTransaction/childrenInfo/save' ?>" data-su-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/childrenInfo/childList':'regularTransaction/childrenInfo/childList'?> data-type="list" value="submit">
    </div>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('#zone').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_area'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#area').html(data);
                    $('#area').select2('val', '');

                }
            });
        });



        $('#area').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_shift'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#ship').html(data);
                    $('#ship').select2('val', '');
                    
                }
            });
        });
        $('#ship').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_shipEstablishmentNumber'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shipNumber').html(data);
                }
            });
        });
        $('#ship').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_shipEstablishmentName'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shipName').html(data);
                }
            });
        });
    });
</script>
<?php $this->load->view("common/sailors_info"); ?>


