<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <p><u>Current Information</u></p>
<div class="form-group">
    <label class="col-sm-3 control-label">Official No</label>        
    <div class="col-sm-3" >
        <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'value' => $result->OFFICIALNUMBER)); ?>           
        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
    </div>
    <div class="col-sm-1">
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
            <i class="fa fa-question-circle"></i>
        </a>
    </div>
    <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Full Name</label>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => $result->FULLNAME)); ?>
    </div>
    <label class="col-sm-2 control-label">Rank</label>

    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => $result->RANK_NAME)); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Posting Unit</label>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => $result->NAME)); ?>
    </div>
    <label class="col-sm-2 control-label">Posting Date</label>

    <div class="col-sm-3">

        <?php echo form_input(array('name' => 'PostingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly', 'value' => $result->POSTING_DATE)); ?>
    </div>
</div>
<p><u>Add/Edit Information</u></p>
<div class="form-group">
    <label class="col-sm-3 control-label">Child Name</label>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'ChildName', "class" => "form-control  required", 'required' => 'required', 'placeholder' => 'Child Name', 'value' => $result->Name)); ?>
    </div>
    <label class="col-sm-2 control-label">Birth Date</label>

    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'birthDate', "class" => "datePicker form-control required", 'required' => 'required', 'value' => date('d-m-Y', strtotime($result->BirthDate)), 'placeholder' => 'Birth Date')); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Gender</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Gender">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
        <select class="select2 form-control required" name="Gender" id="Gender" data-tags="true" data-placeholder="Select Gender Type" data-allow-clear="true">
            <option value="">Select Gender Type</option>
            <option value="0"<?php echo ($result->Gender == 0) ? 'selected' : '' ?>>Male</option>
            <option value="1"<?php echo ($result->Gender == 1) ? 'selected' : '' ?>>Female</option>
            <option value="2"<?php echo ($result->Gender == 2) ? 'selected' : '' ?>>Others</option>
        </select>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Child Name Bangla</label>
    <div class="col-sm-4">
        <?php echo form_input(array('name' => 'ChildNameBangla', 'value' => $result->ChildNameBangla, 'id' => 'ChildNameBangla', 'type' => 'text', "class" => "form-control", 'placeholder' => 'Child Name Bangla')); ?>
    </div>

</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Authority Number</label>
    <div class="col-sm-4">
        <?php echo form_input(array('name' => 'authorityNumber', 'value' => $result->AuthorityNumber, 'id' => 'AuthorityNumber', 'type' => 'text', "class" => "form-control required", 'placeholder' => 'Authority  Number')); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Authority Date</label>

    <div class="col-sm-3 date">
        <?php echo form_input(array('name' => 'AuthorityDate', "class" => "datePicker form-control required", 'required' => 'required', 'value' => date('d-m-Y', strtotime($result->AuthorityDate)), 'placeholder' => 'Authority date')); ?>
    </div>
    <label class="col-sm-2 control-label">DAO</label>

    <div class="col-sm-3">
        <select class="select2 form-control required" name="DAO_NUMBER" id="DAO_NUMBER" data-tags="true" data-placeholder="Select DAO" data-allow-clear="true">
            <option value="">Select DAO</option>
            <?php
            foreach ($daoNumber as $row):
                ?>
                <option value="<?php echo $row->DAO_ID ?>"<?php echo ($result->DAOID == $row->DAO_ID) ? 'selected' : '' ?>><?php echo $row->DAO_NO ?></option>
                <?php
            endforeach;
            ?>
        </select>
    </div>
</div>
<p><u>Authority Ship</u><p>
<div class="form-group">
    <label class="col-sm-3 control-label">Zone</label>

    <div class="col-sm-3 date">
        <?php echo form_dropdown("zone", $zone, $result->AUTHO_ZONE, "class='select2 form-control' id='zone'  aria-hidden='true' data-allow-clear='true'"); ?>
    </div>
    <label class="col-sm-2 control-label">Area</label>

    <div class="col-sm-3" >
        <select  id="area" name="" class="select2 form-control" aria-hidden="true" data-allow-clear="true">
            <option value="">Select Area</option>
            <?php
            foreach ($authorityArea as $row):
                if ($row->PARENT_ID == $result->AUTHO_ZONE):
                    ?>
                    <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->SHIP_AREA == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo "[" . $row->CODE . "] " . $row->NAME ?></option>
                    <?php
                endif;
            endforeach;
            ?>
        </select>
    </div>  
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">Ship Establishment</label>
    <div class="col-sm-4" >
        <select  id="ship" name="shipEstId" class="select2 form-control" aria-hidden="true" data-allow-clear="true">
            <option value="">Select Ship Est.</option>
            <?php
            foreach ($shipEstablishment as $row):
                ?>
                <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->ShipEstablishmentID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                <?php
            endforeach;
            ?>
        </select>
    </div>    
</div>
<div class="form-group" >
    <div id="shipNumber">
        <label class="col-sm-3 control-label">Authority Ship</label> 
        <div class="col-sm-2" >
            <input type="text" class="form-control" value="<?php echo $result->CODE ?>" />
        </div>
    </div>    

    <div id="shipName">
        <div class="col-sm-4" >
            <input type="text" class="form-control" value=" <?php echo $result->NAME ?>">
        </div>
    </div>                                    
</div>
<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <input type="hidden" name="id" value="<?php echo $result->ChildernID; ?>">
        <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/childrenInfo/update':'regularTransaction/childrenInfo/update' ?>" data-su-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/childrenInfo/childList':'regularTransaction/childrenInfo/childList'?> data-type="list" value="Update">  
    </div>
</div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('#zone').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_area'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#area').html(data);
                    $('#area').select2('val', '');

                }
            });
        });



        $('#area').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_shift'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#ship').html(data);
                    $('#ship').select2('val', '');
                    
                }
            });
        });
        $('#ship').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_shipEstablishmentNumber'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shipNumber').html(data);
                }
            });
        });
        $('#ship').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",          
                url: "<?php echo site_url('regularTransaction/ChildrenInfo/ajax_get_shipEstablishmentName'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shipName').html(data);
                }
            });
        });
    });
</script>
<?php $this->load->view("common/sailors_info"); ?>


