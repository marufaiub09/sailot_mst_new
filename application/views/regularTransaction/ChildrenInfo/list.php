<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var flag = "<?php echo $flag; ?>";
        var tergatUrl;
        if(flag == 1){
            tergatUrl = "Retirement/HistoryTran/childrenInfo/ajaxChildTranList";   /*Retirement sailor url*/ 
        }else{
            tergatUrl = "regularTransaction/childrenInfo/ajaxChildTranList"; /*Active sailor url*/ 
        }
        var dataTable = $('#employee-grid').DataTable({
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>"+tergatUrl, // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }

            }

        });
    });
</script>

<table id="employee-grid" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Child Name</th>
            <th>Gender</th>
            <th>Birth Date</th>
            <th>Issuing Authority</th>
            <th>Authority</th>
            <th>Sailor Info</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>  
    </tbody>
</table>        