<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var flag = "<?php echo $flag; ?>";
        var tergatUrl;
        if(flag == 1){
            tergatUrl = "Retirement/HistoryTran/childrenInfo/ajaxChildTranList";   /*Retirement sailor url*/ 
        }else{
            tergatUrl = "regularTransaction/childrenInfo/ajaxChildTranList"; /*Active sailor url*/ 
        }
        var dataTable = $('#employee-grid').DataTable({
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>"+tergatUrl, // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }
            },
            /*id attribute add into dataTable*/
            createdRow: function(row, data, dataIndex) {
            //console.log(data);
            
                var a = $(row).find('td:eq(0)').html();
                $(row).attr("id", 'row_'+a);
            }
        });
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Children Information</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink" data-modal-size="modal-md" href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/childrenInfo/create') : site_url('regularTransaction/childrenInfo/create') ?>"  title="Add Children Information">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>


                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body contentArea">
                <table id="employee-grid" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Child Name</th>
                            <th>Gender</th>
                            <th>Birth Date</th>
                            <th>Issuing Authority</th>
                            <th>Authority</th>
                            <th>Sailor Info</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>             
            </div>
        </div>
    </div>
</div>
