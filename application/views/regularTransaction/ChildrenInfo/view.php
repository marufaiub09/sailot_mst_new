<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">  
    <tr>
        <th width="20%">Child Name</th>
        <td><?php echo $viewdetails->Name ?></td>
    </tr>
    <tr>
        <th width="20%">Gender</th>
        <td>
            <?php
            if ($viewdetails->Gender == '0') {
                echo 'Male';
            } elseif ($viewdetails->Gender == '1') {
                echo 'Female';
            } else {
                echo 'Others';
            }
            ?>
        </td>
    </tr>
    <tr>
        <th width="20%">Birth Date</th>
        <td><?php echo date("d/m/Y", strtotime($viewdetails->BirthDate)) ?></td>
    </tr>

    <tr>
        <th width="20%">Issuing Authority</th>
        <td><?php echo $viewdetails->SHIP_ESTABLISHMENT ?></td>
    </tr>
</tr><tr>
    <th>Authority Number</th>
    <td><?php echo $viewdetails->AuthorityNumber ?></td>
</tr>
<tr>
    <th>Authority Date</th>
    <td><?php echo date("d/m/Y", strtotime($viewdetails->AuthorityDate)) ?></td>
</tr>
<tr>
    <th>Official Number</th>
    <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
</tr>        
<tr>
    <th>Full Name</th>
    <td><?php echo $viewdetails->FULLNAME ?></td>
</tr>        
<tr>
    <th>Rank</th>
    <td><?php echo $viewdetails->RANK_NAME ?></td>
</tr>        
<tr>
    <th>Posting Unit</th>
    <td><?php echo $viewdetails->NAME ?></td>
</tr>  
<tr>
    <th>Posting Date</th>
    <td><?php echo date("d/m/Y", strtotime($viewdetails->POSTING_DATE)) ?></td>
</tr> 
<tr>
    <th>Child Name Bangla</th>
    <td><?php echo $viewdetails->ChildNameBangla ?></td>
</tr> 
<tr>
    <th>Dao Number</th>
    <td><?php echo $viewdetails->DAONo ?></td>
</tr> 

</table>