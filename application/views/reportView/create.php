<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-3 control-label">Assesment Year</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Year">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-2">
            <?php echo form_input(array('name' => 'assesment_year', 'min' => '1970', 'max' => '2099', 'type' => 'number', "class" => "form-control required", 'required' => 'required', 'value' => Date('Y'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Ship Establishment</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Ship Establishment">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT" data-tags="true" data-placeholder="Select ship establishment" data-allow-clear="true">
                <option value="">Select ship establishment</option> 
                <?php
                foreach ($shipEstablishment as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"><?php echo $row->NAME ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <table id="sailorTable" class="table table-striped table-bordered " width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Official Number</th>
                <th>Full Name</th>
                <th>Rank</th>
                <th>Character</th>
                <th>Efficiency</th>
                <th>Assessment Ship</th>
                <th>Last yr. Character</th>
                <th>Last yr. Efficiency</th>
                <th>Last yr. Ship</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>          
                <td>
                    <input type="text" name="OFFICIAL_NO[]" id="OFFICIAL_NO_1"  class="form-control numbersOnly OFFICIAL_NO required" placeholder="Official No" >
                </td>
                <td>
                    <input type="text" value="" name="FULLNAME[]" id="FULLNAME_1"  class="form-control required" placeholder="Full Name" >
                </td>
                <td>
                    <input type="text" name="RANK[]" id="RANK_1"  class="form-control required" placeholder="Rank" >
                </td>
                <td>
                    <?php
                    $options = array(
                        0 => "VG",
                        1 => "GOOD",
                        2 => "FAIR",
                        3 => "INDIF",
                        4 => "BAD"
                    );
                    echo form_dropdown('CHAR[]', $options, 'default', 'class = "select2 form-control required" id="CHAR_1"');
                    ?>

                </td>
                <td>
                    <?php
                    $options = array(
                        0 => "SUPER",
                        1 => "SAT",
                        2 => "MOD",
                        3 => "INFER",
                        4 => "UT"
                    );
                    echo form_dropdown('EFFICIANCY[]', $options, 'default', 'class = "select2 form-control required" id="EFFICIANCY_1"');
                    ?>
                </td>
                <td>
                    <input type="text" name="SHIP[]" id="SHIP_1"  class="form-control " placeholder="Ship" >
                </td>
                <td>
                    <input type="text" name="LAST_YR_CHAR[]" id="LAST_YR_CHAR_1"  class="form-control " placeholder="Last yr. Char" >
                </td>
                <td>
                    <input type="text" name="LAST_YR_EFF[]" id="LAST_YR_EFF_1"  class="form-control " placeholder="Last yr. eff." >
                </td>
                <td>
                    <input type="text" name="LAST_YR_SHIP[]" id="LAST_YR_SHIP_1"  class="form-control " placeholder="Last yr. ship" >
                </td>

                <td class="text-center">

                </td>
            </tr>
        </tbody>
    </table>
    <div class="xh" style="text-align: right;">
        <span class="btn btn-xs btn-success" id="add_record">
            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
        </span>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/assessmentInfo/saveVI" data-su-action="sailorsInfo/assessmentInfo/viList" data-type="list" value="submit">
        </div>
    </div>
</form>