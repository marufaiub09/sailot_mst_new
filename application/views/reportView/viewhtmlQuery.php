<script type="text/javascript">
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',
            // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row),
                            $cols = $row.find('td');

                        return $cols.map(function (j, col) {
                            var $col = $(col),
                                text = $col.text();

                            return text.replace(/"/g, '""'); // escape double quotes

                        }).get().join(tmpColDelim);

                    }).get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim) + '"',
            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // CSV
            exportTableToCSV.apply(this, [$('#datatable'), 'export.csv']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>
<a href="#" class="export">
    <button>CSV/EXCEL</button>
</a>
<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <?php
        error_reporting('0');

        $this->db->where('id', $_POST['reportName']);
        $query = $this->db->get('report_view');
        foreach ($query->result() as $row) {
            $row->report_title;
            $data = $row->report_sql;
            $tableName = $row->table_name;
            $columnName = $row->column_name;
            $str = $columnName;

            $arr1 = explode(",", $str);

        }


        if (empty($columnName)) {
            $data1['result'] = $this->db->list_fields($tableName);
            foreach ($data1['result'] as $value) {
                ?>
                <th><?php echo $value; ?></th>
                <?php
            }
        } else {
            foreach ($arr1 as $value) {
                ?>
                <th><?php echo $value; ?></th>
                <?php
            }
        }

        ?>
    </tr>
    </thead>

    <tbody>
    <?php
    if (!empty($row->report_sql)) {
        $Strsql = $row->report_sql;
        $ar = str_split($Strsql, 7);
        if (in_array("sailor.", $ar, true)) {
            $sql = $row->report_sql . ' WHERE sailor.SAILORID' . ' =' . $_POST['SAILOR_ID'];
        } else {
            $sql = $row->report_sql . ' WHERE SAILORID' . ' =' . $_POST['SAILOR_ID'];
        }
        $data = $this->db->query($sql)->result();


    }
    ?>



    <?php
    foreach ($data as $row) {
        ?>
        <tr>
            <?php
            if (empty($columnName)) {
                $data1['result'] = $this->db->list_fields($tableName);
                foreach ($data1['result'] as $value) {
                    ?>
                    <td><?php echo $row->$value; ?></td>
                    <?php
                }
            } else {

                foreach ($arr1 as $value) {
                    ?>
                    <td><?php echo $row->$value; ?></td>
                    <?php
                }
            }
            ?>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>

