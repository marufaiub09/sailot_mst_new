<?php
header("Content-type: text/css");
$white = '#fff';
$dkgray = '#333';
$dkgreen = '#008400';
?>
<style>

    body {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        margin-top: <?php echo $_POST['topMargin'] ?>;
        margin-bottom: <?php echo $_POST['bottomMargin'] ?>;
        margin-left: <?php echo $_POST['leftMargin'] ?>;
        margin-right: <?php echo $_POST['rightMargin'] ?>;

    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table td, .table th {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        border: 1px <?php echo $_POST['borderType'] ?> <?php echo $_POST['borderColour'] ?>;
    }

    table.table-condensed {
        border: 1px solid <?php echo $dkgreen ?>;
    }
</style>
<?php
error_reporting('0');

$this->db->where('id', $_POST['reportName']);
$query = $this->db->get('report_view');
foreach ($query->result() as $row) {
    $title = $row->report_title;
    $row->report_sql;
    $tableName = $row->table_name;
    $columnName = $row->column_name;
    $str = $columnName;

    $arr1 = explode(",", $str);
}
?>

<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php echo base_url('dist/img/navy_logo2.jpg'); ?>"/>
    <h5 style="font-size: <?php echo $_POST['fontSize'] ?>">Sailor Management System.</h5>
    <span style=" text-align: center"><b><?php echo $title; ?></b><hr></span>
</div>
<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <?php
        if (empty($columnName)) {
            $data1['result'] = $this->db->list_fields($tableName);
            foreach ($data1['result'] as $value) {
                ?>
                <th><?php echo $value; ?></th>
                <?php
            }
        } else {
            foreach ($arr1 as $value) {
                ?>
                <th><?php echo $value; ?></th>
                <?php
            }
        }
        ?>
    </tr>
    </thead>

    <tbody>
    <?php
    if (!empty($row->report_sql)) {
        $Strsql = $row->report_sql;
        $ar = str_split($Strsql, 7);
        if (in_array("sailor.", $ar, true)) {
            $sql = $row->report_sql . ' WHERE sailor.SAILORID' . ' =' . $_POST['SAILOR_ID'];
        } else {
            $sql = $row->report_sql . ' WHERE SAILORID' . ' =' . $_POST['SAILOR_ID'];
        }
        $data = $this->db->query($sql)->result();

    }
    ?>



    <?php
    foreach ($data as $row) {
        ?>
        <tr>
            <?php
            if (empty($columnName)) {
                $data1['result'] = $this->db->list_fields($tableName);
                foreach ($data1['result'] as $value) {
                    ?>
                    <td><?php echo $row->$value; ?></td>
                    <?php
                }
            } else {

                foreach ($arr1 as $value) {
                    ?>
                    <td><?php echo $row->$value; ?></td>
                    <?php
                }
            }
            ?>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>