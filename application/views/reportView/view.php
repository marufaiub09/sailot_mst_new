<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">  
    <tr>
        <th width="20%">Official Numver</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER?></td>
    </tr>
    <tr>
        <th width="20%">Full Name</th>
        <td><?php echo $viewdetails->FULLNAME?></td>
    </tr>
    <tr>
        <th width="20%">Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME?></td>
    </tr>

    <tr>
        <th width="20%">Asses year</th>
        <td><?php echo $viewdetails->AssessYear?></td>
    </tr>
    <tr>
        <?php 
            $char = array("VG", "GOOD", "FAIR", "INDIF", "BAD");
            $efficiency = array("SUPER","SAT","MOD","INFER","UT");
        ?>
        <th>Assess Ship</th>
        <td><?php echo $viewdetails->SHIP_ESTABLISHMENT?></td>
    </tr><tr>
        <th>Character</th>
        <td><?php echo array_key_exists($viewdetails->CharacterType, $char) ? $char[$viewdetails->CharacterType] : ''; ?></td>
    </tr>
    <tr>
        <th>Efficiency</th>
        <td><?php echo array_key_exists($viewdetails->EfficiencyType, $efficiency) ? $efficiency[$viewdetails->EfficiencyType] : ''; ?></td>
    </tr>		 
        
</table>