<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Official No</label>        
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number')); ?>           
           <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
        <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Full Name</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly', 'value' => set_value('fullName'))); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>
                
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'rank',  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly', 'value' => set_value('rank'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Ship/Establishment</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Ship/Establishment">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
           <?php echo form_input(array('name' => 'ship_establishment', 'type'=>'text', "class" => "SHIP_ESTALISHMENT form-control required", 'required'=>'required', 'readonly'=>'readonly',  'value' => set_value('SHIP_ESTALISHMENT'))); ?>
        </div>
    </div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Language</label>
		<a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Language">
			<i class="fa fa-question-circle"></i>
		</a>
		<div class="col-sm-4">
            <select class="select2 form-control required" name="LANGUAGE_ID" id="LANGUAGE_ID" data-tags="true" data-placeholder="Select Language" data-allow-clear="true">
                <option value="">Select Language</option>
                <?php
                foreach ($language as $row):
                    ?>
                    <option value="<?php echo $row->LANGUAGE_ID ?>"><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
	</div>
	<div class="form-group">
        <label class="col-sm-3 control-label"><input type="checkbox" value ="1" name="read" id="canRead"> Can Read</label>
        <div class="col-sm-3">
            <?php 
                $status = array('1' => 'Excellent', '2' => 'Good', '3' => 'Moderate', '4' => 'Poor' );
                echo form_dropdown('read_status', $status, '2', 'class="select2 form-control required" id ="d_canRead"');
            ?>
        </div>
        <label class="col-sm-2 control-label"><input type="checkbox" value ="1" name="speck" id="canSpeck"> Can Speak</label>     
        <div class="col-sm-3">
            <?php 
                echo form_dropdown('specking_status', $status, '2', 'class="select2 form-control required" id ="d_canSpeck"');
            ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><input type="checkbox" value ="1" name="write" id="canWrite"> Can Write</label>
        <div class="col-sm-3">
            <?php 
                echo form_dropdown('writing_status', $status, '2', 'class="select2 form-control required" id ="d_canWrite"');
            ?>
        </div>
        <label class="col-sm-2 control-label"><input type="checkbox" value ="1" name="listen" id="canListen"> Can Listen</label>     
        <div class="col-sm-3">
            <?php 
                echo form_dropdown('listen_status', $status, '2', 'class="select2 form-control required" id ="d_canListen"');
            ?>
        </div>
    </div>
	
	
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/languageInfo/save" data-su-action="sailorsInfo/languageInfo/languageList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script>
    $("#d_canRead").prop("disabled", true);
    $("#d_canSpeck").prop("disabled", true);
    $("#d_canWrite").prop("disabled", true);
    $("#d_canListen").prop("disabled", true);
    $(document).ready(function() {
        $("#canRead").on('click', function(){
            $("#d_canRead").attr('disabled',! this.checked);            
        });
        $("#canSpeck").on('click', function(){
            $("#d_canSpeck").attr('disabled',! this.checked);            
        });
        $("#canWrite").on('click', function(){
            $("#d_canWrite").attr('disabled',! this.checked);            
        });
        $("#canListen").on('click', function(){
            $("#d_canListen").attr('disabled',! this.checked);            
        });
        
    });
   
</script>
<?php $this->load->view("common/sailors_info"); ?>