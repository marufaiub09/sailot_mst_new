<?php 
    $status = array('1' => 'Excellent', '2' => 'Good', '3' => 'Moderate', '4' => 'Poor' ); 
?>
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">  
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER?></td>
    </tr>
    <tr>
        <th width="20%">Full Name</th>
        <td><?php echo $viewdetails->FULLNAME?></td>
    </tr>
    <tr>
        <th width="20%">Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME?></td>
    </tr>

    <tr>
        <th width="20%">Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_ESTABLISHMENT?></td>
    </tr>
    <tr>
        <th>Can Read</th>
        <td><?php echo ($viewdetails->CanRead == 0) ? 'No' : 'Yes'; ?></td>
    </tr>
    <tr>
        <th>Read Efficiency</th>
        <td><?php echo array_key_exists($viewdetails->ReadEfficiency, $status) ? $status[$viewdetails->ReadEfficiency] : ''; ?></td>
    </tr>
    <tr>
        <th>Can Write</th>
        <td><?php echo ($viewdetails->CanWrite == 0) ? 'No' : 'Yes'; ?></td>
    </tr>
    <tr>
        <th>Write Efficiency</th>
        <td><?php echo array_key_exists($viewdetails->WriteEfficiency, $status) ? $status[$viewdetails->WriteEfficiency] : '';?></td>
    </tr>
    <tr>
        <th>Can Speak</th>
        <td><?php echo ($viewdetails->CanSpeak == 0) ? 'No' : 'Yes'; ?></td>
    </tr>
    <tr>
        <th>Speak Efficiency</th>
        <td><?php echo array_key_exists($viewdetails->SpeakEfficiency, $status) ? $status[$viewdetails->SpeakEfficiency] : '';?></td>
    </tr>
    <tr>
        <th>Can Listen</th>
        <td><?php echo ($viewdetails->CanLeassening == 0) ? 'No' : 'Yes'; ?></td>
    </tr>
    <tr>
        <th>Listen Efficiency</th>
        <td><?php echo array_key_exists($viewdetails->LeasseningEfficiency, $status) ? $status[$viewdetails->LeasseningEfficiency] : '';?></td>
    </tr>
      
       
</table>