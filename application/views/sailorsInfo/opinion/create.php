<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group">
        <label class="col-sm-3 control-label">Event Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Ship Establishment">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-5">
            <select class="select2 form-control required" name="MISSION_ID" id="MISSION_ID" data-tags="true" data-placeholder="Select event name" data-allow-clear="true">
                <option value="">Select event name</option> 
                <?php
                foreach ($mission as $row):
                    ?>
                    <option value="<?php echo $row->CODE.'-'.$row->MISSION_ID ?>"><?php echo $row->CODE." - ".$row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <table id="sailorTable" class="table table-striped table-bordered " width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Official Number</th>
                <th>Full Name</th>
                <th>Rank</th>
                <th>Ship</th>
                <th>action</th>
            </tr>
        </thead>
        <tbody>
            <tr>          
                <td>
                    <input type="text" name="OFFICIAL_NO[]" id="OFFICIAL_NO_1"  class="form-control numbersOnly OFFICIAL_NO required" placeholder="Official No" >
                </td>
                <td>
                    <input type="text" value="" name="FULLNAME[]" id="FULLNAME_1"  class="form-control required" placeholder="Full Name" >
                </td>
                <td>
                    <input type="text" name="RANK[]" id="RANK_1"  class="form-control required" placeholder="Rank" >
                </td>              
                <td>
                    <input type="text" name="SHIP[]" id="SHIP_1"  class="form-control required" placeholder="ship" >
                </td>              
                
                <td class="text-center">
                   
                </td>
            </tr>
        </tbody>
    </table>
    <div class="xh" style="text-align: right;">
        <span class="btn btn-xs btn-success" id="add_record">
            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
        </span>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/opinion/save" data-su-action="sailorsInfo/opinion/opinionList" data-type="list" value="submit">
        </div>
    </div>
</form>