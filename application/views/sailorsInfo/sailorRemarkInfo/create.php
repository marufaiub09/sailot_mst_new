<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-3 control-label">Official No</label>        
        <div class="col-sm-3" >
            <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
            <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
        <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Full Name</label>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>

        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Remarks</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Remarks ">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <?php echo form_textarea(array('name' => 'remarks', "id" => "remarks", "rows" => "2", "cols" => "2", "class" => "form-control remarks required")); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/sailorRemark/remarkUpdate" data-su-action="sailorsInfo/sailorRemark/SailorRemarkList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script>
    
    /*start search sailorInfo, rank_name, ship_establishment, Posting_unit, Posting_date by Official Number */
    $("#officialNumber").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber},
                dataType: "json",
                url: "<?php echo site_url(); ?>sailorsInfo/sailorRemark/searchSailorInfoByOfficalNo",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php // echo base_url();   ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);  
                        $(".SHIP_ESTALISHMENT").val(data['SHIP_ESTABLISHMENT']); 
                        $(".remarks").val(data['REMARKS']); 
                        $(".PostingUnit").val(data['POSTING_UNIT_NAME']);
                        $(".PostingDate").val(data['POSTING_DATE']);                  
                        $(".entryType").val(data['ENTRY_TYPE_NAME']);                  
                        $(".alertSMS").html('');
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val(''); 
                        $(".remarks").val('');
                        $(".SHIP_ESTALISHMENT").val('');      
                        $(".PostingUnit").val('');
                        $(".PostingDate").val('');
                        $(".entryType").val('');
                        $(".alertSMS").html('Invalid Officer Number');      
                    }
                }
            });
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');      
            $(".SHIP_ESTALISHMENT").val('');  
            $(".remarks").val('');      
            $(".PostingUnit").val('');
            $(".PostingDate").val('');
            $(".entryType").val('');
            $(".alertSMS").html(''); 
        }
    });
    /*end Official Number search part*/


</script>

