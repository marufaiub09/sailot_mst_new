<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->CURR_RANK ?></td>
    </tr>
    <tr>
        <th>Last Rank</th>
        <td><?php echo $viewdetails->LAST_RANK ?></td>
    </tr>
    <tr>
        <th>Posting Unit</th>
        <td><?php echo $viewdetails->POSTING_UNIT_NAME ?></td>
    </tr>
    <tr>
        <th>Posting Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->POSTING_DATE)) ?></td>
    </tr>
    <tr>
        <th>Change Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->CHANGE_DATE)) ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>DAO Number </th>
        <td><?php echo $viewdetails->DAO_NUMBER ?></td>
    </tr>
    <tr>
        <th>Authority Number</th>
        <td><?php echo $viewdetails->AUTHORITY_NO ?></td>
    </tr>
    <tr>
        <th>Authority Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->AUTHORITY_DATE)) ?></td>
    </tr>
</table>