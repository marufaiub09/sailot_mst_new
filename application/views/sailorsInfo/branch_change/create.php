<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>    
    <div class="col-md-12">
        <fieldset class="">
            <legend  class="legend">Current Info</legend>   
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-sm-4 ">Official Number</label>        
                    <div class="col-sm-6" >
                        <?php echo form_input(array('name' => 'officialNo', 'id' => "officialNumber", "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                        <input type="hidden" name="CURR_BRANCH_ID" class="branchId" value="">           
                        <input type="hidden" name="CURR_RANK_ID" class="rankId" value="">           
                    </div>
                    <div class="col-sm-1">
                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                            <i class="fa fa-question-circle"></i>
                        </a>
                    </div>
                    <div class="col-md-12" >
                        <div class="col-sm-4"></div>
                        <div class="col-sm-8 danger">
                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4">Posting Unit</label>
                    <div class="col-sm-6">
                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-sm-3">Details</label>
                    <div class="col-sm-4">
                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                    </div>
                    <div class="col-sm-4">
                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3">Posting Date</label>
                    <div class="col-sm-4">
                        <?php echo form_input(array('name' => 'PostingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly')); ?>
                    </div>
                </div>
            </div>
        </fieldset>
    </div>
    <div class="col-md-12">
        <fieldset class="">
            <legend  class="legend">New Information</legend>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4">Branch <span class="text-danger">*</span></label>
                    <div class="col-md-6" >
                       <select class="select2 form-control required" name="BRANCH_ID" id="BRANCH_ID" data-tags="true" data-placeholder="Select Branch" data-allow-clear="true">
                            <option value="">Select Branch</option>
                            <?php
                            foreach ($branch as $row):
                                ?>
                                <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                            <?php
                            endforeach; 
                            ?>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Branch Name">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
                <div class="form-group">
                    <label class="col-md-4">Rank <span class="text-danger">*</span></label>
                    <div class="col-md-6" >
                       <select class="select2 form-control required" name="RANK_ID" id="RANK_ID" data-tags="true" data-placeholder="Select Rank" data-allow-clear="true">
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-4 ">Area <span class="text-danger">*</span></label>
                    <div class="col-md-6" >
                       <select class="select2 form-control required" name="SHIP_AREA_ID" id="SHIP_AREA_ID" data-tags="true" data-placeholder="Select Area" data-allow-clear="true">
                            <option value="">Select Area</option>
                            <?php
                            foreach ($area as $row):
                                ?>
                                <option value="<?php echo $row->ADMIN_ID ?>"><?php echo $row->NAME ?></option>
                            <?php
                            endforeach; 
                            ?>
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Area Name">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
                <div class="form-group">
                    <label class="col-md-4" style="padding-right: 0px;">Ship/Establishment <span class="text-danger">*</span></label>
                    <div class="col-md-7" >
                       <select class="select2 form-control required" name="SHIP_ESTABLISHMENT_ID" id="SHIP_ESTABLISHMENT_ID" data-tags="true" data-placeholder="Select Ship/Establishment" data-allow-clear="true">
                        </select>
                    </div>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Ship/Establishment">
                        <i class="fa fa-question-circle"></i>
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-sm-4">Authority Number <span class="text-danger">*</span></label>
                    <div class="col-sm-6" >
                       <?php echo form_input(array('name' => 'authorityNo', "class" => "form-control required", 'required'=>'required','placeholder' => 'Authority Number')); ?>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="col-sm-4">Authority Date <span class="text-danger">*</span></label>
                    <div class="col-sm-4" >
                       <?php echo form_input(array('name' => 'authorityDate', "class" => "datePicker form-control required", 'required'=>'required','placeholder' => 'Authority Date', 'value' => date('d-m-Y'))); ?>
                    </div>
                </div> 
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-sm-4">DAO Number <span class="text-danger">*</span></label>
                    <div class="col-sm-6" >
                       <select class="select2 form-control required" name="DAO_ID" id="DAO_ID" data-tags="true" data-placeholder="Select DAO Number" data-allow-clear="true">
                            <option value="">Select DAO Number</option>
                            <?php
                            foreach ($dao as $row):
                                ?>
                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                            <?php
                            endforeach; 
                            ?>
                        </select>
                    </div>
                </div>   
                <div class="form-group">
                    <label class="col-sm-4">Change Date <span class="text-danger">*</span></label>
                    <div class="col-sm-4" >
                       <?php echo form_input(array('name' => 'changeDate', "class" => "form-control datePicker", 'required'=>'required','placeholder' => 'Changing date', 'value' => date('d-m-Y'))); ?>
                    </div>
                </div>   

            </div>
        </fieldset>
        <div class="form-group">
            <label class="col-sm-2"></label>
            <div class="col-sm-6">
                &nbsp; <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/branchChange/saveBranch" data-su-action="sailorsInfo/branchChange/branchList" data-type="list" value="submit">
            </div>
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>