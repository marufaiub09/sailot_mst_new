<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>    
    <div class="form-group">
        <label class="col-sm-2 control-label">Official Number</label>
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'officialNo', 'readonly'=>'readonly', 'id' => 'officialNumber', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number','value' => $result->OFFICIALNUMBER)); ?>           
           <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">
        </div>
        <div class="col-sm-1" >
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
         <label class="col-sm-2 control-label">Full Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Enter Sailor Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly','value' => $result->FULLNAME)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Rank</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'rank',  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly', 'value' => $result->RANK_NAME)); ?>
        </div>
        <label class="col-sm-3 control-label">Ship/Establishment</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'ship_establishment', 'type'=>'text', "class" => "SHIP_ESTALISHMENT form-control required", 'required'=>'required', 'readonly'=>'readonly',  'value' => $result->SE_ESTABLISHMENT)); ?>
        </div>
    </div>
    <hr>
    <div class="form-group">
        <label class="col-sm-2 control-label">Fraudulent category</label>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="FRADULENT_ID" id="FRADULENT_ID" data-tags="true" data-placeholder="Select One" data-allow-clear="true">
                <option value="">Select fraudulent category</option>
                <?php
                foreach ($fraudulentCat as $row):
                    ?>
                    <option value="<?php echo $row->CATEGORY_ID ?>" <?php echo ($result->CategoryID == $row->CATEGORY_ID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
        <label class="col-sm-3 control-label">Effective Date</label>
        <div class="col-sm-2 date">
           <?php echo form_input(array('name' => 'effectiveDate', "class" => "datePicker form-control required input-form-padding-right-empty",'required'=>'required', 'value' => date('m-d-Y', strtotime($result->TranDate)))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Status</label>
        <div class="col-sm-3">
           <?php 
               $status = array('1' => 'Record','2' => 'Disposed','3' => 'Under Disposal');
               echo form_dropdown('TranStatus', $status, $result->TranStatus, 'class="select2 form-control required" id ="d_canRead"'); 
           ?>
        </div>
        <label class="col-sm-3 control-label">Ship/Establishment</label>
        <div class="col-sm-4">
           <select class="select2 form-control required" name="SHIP_ESTABLISHMENTID" id="SHIP_ESTABLISHMENTID" data-tags="true" data-placeholder="Select One" data-allow-clear="true">
                <option value="">Select Ship/Establishment</option>
                <?php
                foreach ($shipEstablish as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->ShipID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Authority Name</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'authoName', 'id' => 'authorityName', "class" => "form-control required",  'placeholder' => 'Authority name', 'value' => $result->AuthorityName)); ?>
        </div>
        <label class="col-sm-3 control-label">Authority Date</label>
        <div class="col-sm-2 date">
           <?php echo form_input(array('name' => 'authorityDate',  "class" => "datePicker form-control input-form-padding-right-empty required",'required'=>'required', 'placeholder' => 'Date', 'value' => date('m-d-Y', strtotime($result->AuthorityDate)))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Authority Number</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'authoNumber', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Authority number', 'value' => $result->AuthorityNumber)); ?>
        </div>
        <label class="col-sm-3 control-label">Remarks</label>
        <div class="col-sm-4">
           <?php echo form_textarea(array('name' => 'remarks', "class" => "form-control required", 'required'=>'required','placeholder' => 'Remarks', 'cols' => '12','rows' => '4', 'value' => $result->Remarks)); ?>
        </div>        
    </div>  
    <div class="form-group">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->DetailID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/FraudulentInfo/update" data-su-action="sailorsInfo/FraudulentInfo/fraudulentList" data-type="list" value="Update">
        </div>
    </div>
</form>
<script>
    var ship = "<?php echo $result->ShipID; ?>";
    var authorityName = "<?php echo $result->AuthorityName; ?>";
    if(authorityName == ''){
        $("#authorityName").prop("disabled", true);        
    }
    if(ship == ''){
        $("#SHIP_ESTABLISHMENTID").prop("disabled", true);        
    }

    $("#authorityName").on('blur', function(){
        var name = $(this).val();
        if(name == ''){
            $("#SHIP_ESTABLISHMENTID").prop("disabled", false);
            $("#SHIP_ESTABLISHMENTID").addClass('required');
        }else{
            $("#SHIP_ESTABLISHMENTID").prop("disabled", true);
            $("#SHIP_ESTABLISHMENTID").removeClass('required');
        }
    });
    $("#SHIP_ESTABLISHMENTID").on('change', function(){
        var ship = $(this).val();
        if(ship == ''){
            $("#authorityName").prop("disabled", false);
            $("#authorityName").addClass('required');
        }else{
            $("#authorityName").prop("disabled", true);
            $("#authorityName").removeClass('required');
        }
    });
</script>
<?php $this->load->view("common/sailors_info"); ?>