<?php 
    $status = array('1' => 'Record','2' => 'Disposed','3' => 'Under Disposal');
?>
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">  
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER?></td>
    </tr>
    <tr>
        <th width="20%">Full Name</th>
        <td><?php echo $viewdetails->FULLNAME?></td>
    </tr>
    <tr>
        <th width="20%">Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME?></td>
    </tr>

    <tr>
        <th width="20%">Ship/Establishment</th>
        <td><?php echo $viewdetails->SHIP_ESTABLISHMENT?></td>
    </tr>
    <tr>
        <th>Effective Date</th>
        <td><?php echo date('d/m/Y', strtotime($viewdetails->TranDate)); ?></td>
    </tr>
    <tr>
        <th>Status</th>
        <td><?php echo array_key_exists($viewdetails->TranStatus, $status) ? $status[$viewdetails->TranStatus] : ''; ?></td>
    </tr>
    <tr>
        <th>Authority name</th>
        <td><?php echo $viewdetails->AuthorityName?></td>
    </tr>        
    <tr>
        <th>Authority Date</th>
        <td><?php echo date('d/m/Y', strtotime($viewdetails->AuthorityDate))?></td>
    </tr>        
    <tr>
        <th>Authority Number</th>
        <td><?php echo $viewdetails->AuthorityNumber?></td>
    </tr>        
</table>