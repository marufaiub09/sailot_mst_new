<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Assesment Year</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Assessment year">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-2">
           <?php echo form_input(array('name' => 'assesment_year', 'min' => '1970','max' => '2099',  'type'=>'number', "class" => "form-control required", 'required'=>'required', 'value' => Date('Y'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Ship Establishment</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Ship Establishment">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="SHIP_ESTABLISHMENT" id="SHIP_ESTABLISHMENT" data-tags="true" data-placeholder="Select ship establishment" data-allow-clear="true">
                <option value="">Select ship establishment</option> 
                <?php
                foreach ($shipEstablishment as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"  <?php echo ($result->ShipEstablishmentID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-3 control-label">Official No</label>        
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'officialNo','value'=>$result->OFFICIALNUMBER, 'id' => 'officialNo', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number', 'readonly'=>'readonly')); ?>           
           <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
        </div>        
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Full Name</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'fullName','value'=>$result->FULLNAME, "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly')); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>
                
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'rank','value'=>$result->RANK_NAME,  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Character</label>        
        <div class="col-sm-3" >
           <?php 
                $options = array(
                        0 => "VG",
                        1 => "GOOD",
                        2 => "FAIR",
                        3 => "INDIF",
                        4 => "BAD"
                    );
                $selected = $result->CharacterType;
                echo form_dropdown('CHAR', $options, $selected, 'class = "select2 form-control required" id="CHAR_1"');
            ?>           
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Character Select">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>        
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Efficiency</label>        
        <div class="col-sm-3" >
           <?php 
                $options = array(
                        0 => "SUPER",
                        1 => "SAT",
                        2 => "MOD",
                        3 => "INFER",
                        4 => "UT"
                    );
                $selected = $result->EfficiencyType;
                echo form_dropdown('EFFICIANCY', $options, $selected, 'class = "select2 form-control required" id="EFFICIANCY_1"');
            ?>         
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Efficiency Select">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->AssessID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/assessmentInfo/updateAssess':'sailorsInfo/assessmentInfo/updateAssess' ?>" data-su-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran//assessmentInfo/assessList':'sailorsInfo/assessmentInfo/assessList'?>" data-type="list" value="Update">
        </div>
    </div>
</form>