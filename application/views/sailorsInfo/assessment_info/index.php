<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var flag = "<?php echo $flag; ?>";
        var tergatUrl;
        if(flag == 1){
            tergatUrl = "Retirement/HistoryTran/assessmentInfo/ajaxAssessmentList";   /*Retirement sailor url*/ 
        }else{
            tergatUrl = "sailorsInfo/assessmentInfo/ajaxAssessmentList"; /*Active sailor url*/ 
        }
        var dataTable = $('#employee-grid').DataTable({
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>"+tergatUrl, // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }
            },
            /*id attribute add into dataTable*/
            createdRow: function(row, data, dataIndex) {
                var a = $(row).find('td:eq(0)').html();
                $(row).attr("id", 'row_'+a);
            }
        });
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Assessment Information</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink" data-modal-size="modal-lg"  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/assessmentInfo/create') : site_url('sailorsInfo/assessmentInfo/create') ?>" title="Create Assessment Information">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>
                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body contentArea">
                <table id="employee-grid" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Official No</th>
                            <th>Name</th>
                            <th>Rank</th>
                            <th>Assess Year</th>
                            <th>Assess Ship</th>
                            <th>Character</th>
                            <th>Efficiency</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>  
                    </tbody>
                </table>             
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(document).ready(function() {
        $("#sailorTable tbody").empty();        
    });
    // append academic info table
    var counter = 1;
    $(document).on('click', '#add_record', function () {
        var flag = "<?php echo $flag ?>";
        counter++;
        $("#sailorTable tbody").append(' <tr>' +           
            '<td>' +
            ' <input type="text" name="OFFICIAL_NO[]' + counter + '" id="OFFICIAL_NO_' + counter + '"  class="'+ (flag >= 1 ? 'RET_OFFICIAL_NO' : 'OFFICIAL_NO') +' form-control numbersOnly required" placeholder="Official No" >' +
            '</td>' +
            '<td>' +
            ' <input type="text" value="" name="FULLNAME[]' + counter + '" id="FULLNAME_' + counter + '"  class="form-control" placeholder="Full Name" >' +
            '</td>' +
            '<td>' +
            ' <input type="text" name="RANK[]' + counter + '" id="RANK_' + counter + '"  class="form-control " placeholder="Rank" >' +
            '</td>' +
            '<td>' +
                '<select class="select2 form-control required" name="CHAR[]" id= "CHAR_' + counter + '" >' +
                <?php 
                    $options = array(
                        0 => "VG",
                        1 => "GOOD",
                        2 => "FAIR",
                        3 => "INDIF",
                        4 => "BAD"
                    );
                    ?>
                    <?php for ($i = 0; $i<5; $i++) { ?>
                    '<option value="<?php echo $i ?>"><?php echo $options[$i] ?></option>' +
                    <?php } ?>
                '</select>' +
            '</td>' +
            '<td>' +
                '<select class="select2 form-control required" name="EFFICIANCY[]" id= "EFFICIANCY_' + counter + '" >' +
                <?php 
                    $options = array(
                        0 => "SUPER",
                        1 => "SAT",
                        2 => "MOD",
                        3 => "INFER",
                        4 => "UT"
                    );
                    ?>
                    <?php for ($i = 0; $i<5; $i++) { ?>
                    '<option value="<?php echo $i ?>"><?php echo $options[$i] ?></option>' +
                    <?php } ?>
                '</select>' +
            '</td>' +
            '<td>' +
            ' <input type="text" name="SHIP[]' + counter + '" id="SHIP_' + counter + '"  class="form-control " placeholder="Ship" >' +
            '</td>' +'<td>' +
            ' <input type="text" name="LAST_YR_CHAR[]' + counter + '" id="LAST_YR_CHAR_' + counter + '"  class="form-control " placeholder="Last yr. Char" >' +
            '</td>' +'<td>' +
            ' <input type="text" name="LAST_YR_EFF[]' + counter + '" id="LAST_YR_EFF_' + counter + '"  class="form-control " placeholder="Last yr. eff." >' +
            '</td>' +'<td>' +
            ' <input type="text" name="LAST_YR_SHIP[]' + counter + '" id="LAST_YR_SHIP_' + counter + '"  class="form-control " placeholder="Last yr. ship" >' +
            '</td>' +

            '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
            '</td>' +
            '</tr>'
        );
        $(".select2").select2();
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });
    $(document).on('click', '.formSubmit', function(){
        var ship = $('option:selected', "#SHIP_ESTABLISHMENT").val();
        if(ship == ''){
            alert(" Please Ship Establishment select");
        }
    });

    

</script>
<style>
    .modal-lg {
        width: 90% !important;
    }
</style>