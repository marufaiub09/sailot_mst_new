<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var flag = "<?php echo $flag; ?>";
        var tergatUrl;
        if(flag == 1){
            tergatUrl = "Retirement/HistoryTran/foreignVisitInfo/ajaxForeignVisitList";   /*Retirement sailor url*/ 
        }else{
            tergatUrl = "sailorsInfo/foreignVisitInfo/ajaxForeignVisitList"; /*Active sailor url*/ 
        }
        var dataTable = $('#employee-grid').DataTable({
            /*"scrollY":        "200px",
            "scrollCollapse": true,
            "paging":         false,
            "scrollY": true,*/
            /*"scrollY": "400px",
            "bPaginate": false,
            "bFilter": false, 
            "bInfo": false,*/
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>"+tergatUrl, // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }
            },
            "columnDefs": [
                { "width": "12%", "targets": 7 }
            ],
            /*id attribute add into dataTable*/
            createdRow: function(row, data, dataIndex) {
                var a = $(row).find('td:eq(0)').html();
                $(row).attr("id", 'row_'+a);
            }
        });
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Foreign Visit Search </h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Visit Classification</label>
                                <div class="col-sm-6" >
                                    <select class="select2 form-control" name="VISIT_CI_ID" id="VISIT_CI_ID" data-tags="true" data-placeholder="Select Classification" data-allow-clear="true">
                                        <option value="">Select Classification</option>
                                        <?php
                                        foreach ($visitClass as $row):
                                            ?>
                                            <option value="<?php echo $row->VISIT_CLASSIFICATIONID ?>"><?php echo $row->NAME ?></option>
                                        <?php
                                        endforeach; 
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Visit Information</label>
                                <div class="col-sm-6">
                                    <select class="select2 vistInfo_dropdown form-control required" name="VISIT_ID" id="VISIT_ID" data-placeholder="Select Visit Info" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Visit Informatio</option>
                                        <?php
                                        foreach ($visitInfo as $row):
                                            ?>
                                            <option value="<?php echo $row->VISIT_INFO_ID ?>"><?php echo $row->NAME ?></option>
                                        <?php
                                        endforeach; 
                                        ?>
                                    </select>            
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <fieldset>
                                <legend class="legend" style="font-weight: normal !important; "><input type="checkbox" id="startDate" name="startDate"> Start Date Between:</legend>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="col-md-3 control-label">From:</label>
                                        <div class="col-md-9" >
                                            <?php echo form_input(array('name' => 'fromDate', "id"=>"fromDate", "class" => "jsDatePicker form-control")); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-1 control-label">To:</label>
                                        <div class="col-md-9" >
                                            <?php echo form_input(array('name' => 'toDate', "id"=>"toDate", "class" => "jsDatePicker form-control")); ?>
                                        </div>
                                    </div>                                
                                </div>
                            </fieldset>
                        </div>                        
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-6">
                                <input type="button" class="btn btn-primary btn-sm formSearch" value="Search">                                                     
                            </div>
                        </div>                        
                    </div>
                </form>              
            </div>
        </div>
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Foreign Visit list</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs "  href="<?php echo site_url('sailorsInfo/foreignVisitInfo/create'); ?>" title="Add Foreign visit Informaion">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body contentArea">
                <table id="employee-grid" class="display table-striped " width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Visit Info Name</th>
                            <th>Description</th>
                            <th>Countries</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Durations (Days)</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>  

                    </tbody>
                </table>             
            </div>
        </div>

    </div>
</div>
<script>
    $(document).on('change', '#VISIT_CI_ID', function(event) {
        event.preventDefault();
        $('#VISIT_ID').select2('val', '');
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/visitInfo_by_visitClass_search',
            type: 'POST',
            dataType: 'html',
            data: {VISIT_CLASS_ID: id},
            beforeSend: function () {
                $(".vistInfo_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('.vistInfo_dropdown').html(data);
            }
        });
        
    });
    /*date picker*/
    $(".jsDatePicker").datepicker();
    /*start date between disabled / enable part*/
    $("#startDate").click(function(event) {
        if ($(this).is(':checked')) {
            $("#fromDate").prop("disabled", false);
            $("#toDate").prop("disabled", false);
        }else{
            $("#fromDate").prop("disabled", true);
            $("#toDate").prop("disabled", true);
        }
    });
    $("#fromDate").prop("disabled", true);
    $("#toDate").prop("disabled", true);
    /*end*/
    $(".formSearch").click(function(event) {
        var allData = $(".frmContent").serialize();
        $.ajax({
            type: "post",
            data: allData,
            dataType: "html",
            url: "<?php echo site_url(); ?>sailorsInfo/foreignVisitInfo/searchForeignVisitInfo",
            beforeSend: function () {
                $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
            },
            success: function (data) {
                $(".contentArea").html(data);                
            }
        });
    });

</script>