<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/foreignVisitInfo/index') : site_url('sailorsInfo/foreignVisitInfo/index') ?>" title="List Foreign visit Informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Add Foreign visit Informaion</center></h3>
                        
                    </div>  
                </div>
             </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
                    <span class="frmMsg"></span>    

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Visit Classification</label>
                            <div class="col-sm-6" >
                                <select class="select2 form-control required" name="VISIT_CI_ID" id="VISIT_CI_ID" data-placeholder="Select Visit Class" aria-hidden="true">
                                    <option value="">Select Classification</option>
                                    <?php
                                    foreach ($visitClass as $row):
                                        ?>
                                        <option value="<?php echo $row->VISIT_CLASSIFICATIONID ?>"><?php echo $row->NAME ?></option>
                                    <?php
                                    endforeach; 
                                    ?>
                                </select>
                            </div>
                            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select visit classification">
                                <i class="fa fa-question-circle"></i>
                            </a>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Visit Information</label>
                            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select visit information">
                                <i class="fa fa-question-circle"></i>
                            </a>
                            <div class="col-sm-6">
                                <select class="select2 vistInfo_dropdown form-control required" name="VISIT_ID" id="VISIT_ID" data-placeholder="Select Visit Info" aria-hidden="true">
                                    <option value="">Select Visit Informatio</option>
                                </select>               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Remarks</label>
                            <div class="col-sm-6">
                               <?php echo form_textarea(array('name' => 'remarks', "class" => "form-control",'placeholder' => 'Remarks', 'cols' => '12','rows' => '4')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-5">
                            <fieldset class="form-group">
                                <legend  class="legend">Visited Country:</legend>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Country</label>
                                    <div class="col-sm-6" >
                                       <select class="select2 form-control required" name="COUNTRY_ID" id="COUNTRY_ID" data-tags="true" data-placeholder="Select coutry" data-allow-clear="true">
                                            <option value="">Select Country</option>
                                            <?php
                                                foreach ($country as $row):
                                                    ?>
                                                    <option value="<?php echo $row->COUNTRY_ID ?>"><?php echo $row->NAME ?></option>
                                                <?php
                                                endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please sleect country name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Start Date</label>
                                    <div class="col-sm-4" >
                                       <?php echo form_input(array('name' => 'startDate', "id" =>"startDate", "class" => "form-control required", 'required'=>'required')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select start date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">End Date</label>
                                    <div class="col-sm-4" >
                                       <?php echo form_input(array('name' => 'endDate', "id" =>"endDate", "class" => "form-control required", 'required'=>'required')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select end date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Duration</label>
                                    <div class="col-sm-3" >
                                       <?php echo form_input(array('name' => 'durations', 'id' => 'durations', "class" => "form-control", 'value' => set_value('durations'))); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter durations">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div> 

                            </fieldset>
                        </div>
                        <div class="col-md-6">
                            <fieldset class="form-group">
                                <legend class="legend">Course Information:</legend>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Training Type</label>
                                    <div class="col-sm-6" >
                                       <select class="select2 form-control required" name="TRAINING_TYPE_ID" id="TRAINING_TYPE_ID" data-placeholder="Select Training Type" >
                                            <option value="">Select Training Type</option>
                                            <?php
                                                foreach ($trainingType as $row):
                                                    ?>
                                                    <option value="<?php echo $row->EXAM_ID ?>"><?php echo $row->NAME ?></option>
                                                <?php
                                                endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Training Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>  
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Training Name</label>
                                    <div class="col-sm-6" >
                                        <select class="select2 training_dropdown form-control required" name="TRAINING_NAME" id="TRAINING_NAME" data-placeholder="Select Training Name" >
                                            <option value="">Select Training Name</option>
                                        </select>  
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select training name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>  

                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number</label>
                                    <div class="col-sm-6" >
                                       <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control",'placeholder' => 'Authority Number')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>    
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Date</label>
                                    <div class="col-sm-4" >
                                       <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "class" => "datePicker form-control")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>    
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number</label>
                                    <div class="col-sm-6" >
                                       <select class="select2 form-control" name="DAO_ID" id="DAO_ID" data-tags="true" data-placeholder="Select DAO" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                                foreach ($daoNumber as $row):
                                                    ?>
                                                    <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                                <?php
                                                endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select DAO number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>    
                                
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr style="margin-top: 0px;">
                        <table id="sailorTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Official Number</th>
                                    <th>Full Name</th>
                                    <th>Rank</th>
                                    <th>End Date</th>
                                    <th>Duration (Days)</th>
                                    <th>Grade</th>
                                    <th>Marks</th>
                                    <th>Percentage</th>
                                    <th>Result</th>
                                    <th>Seniority</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>          
                                    <td>
                                        <input type="text" name="OFFICIAL_NO[]" id="OFFICIAL_NO_1"  class="<?php echo ($flag == 1)? 'RET_OFFICIAL_NO':'OFFICIAL_NO' ?> form-control numbersOnly required" placeholder="Official No" >
                                    </td>
                                    <td>
                                        <input type="text" value="" name="FULLNAME[]" id="FULLNAME_1"  class="form-control required" placeholder="Full Name" >
                                    </td>
                                    <td>
                                        <input type="text" name="RANK[]" id="RANK_1"  class="form-control required" placeholder="Rank" >
                                    </td>
                                    <td>
                                        <input type="text" name="s_endDate[]" id="END_DATE_1"  class="endDate col-sm-1 form-control required" placeholder="End date" >
                                    </td>
                                    <td>
                                        <input type="text" name="s_durations[]" id="S_DURATIONS_1"  class="form-control" placeholder="duration" >
                                    </td>
                                    
                                    <td>
                                        <?php 
                                            echo form_dropdown('GRADE[]', $examGrade, 'default', 'class = "select2 form-control" id="GRADE_1"');
                                        ?>
                                    </td>
                                    <td>
                                        <input type="text" name="MARKS[]" id="MARKS_1"  class="form-control " placeholder="mark" >
                                    </td>
                                    <td>
                                        <input type="text" name="PERCENTAGE[]" id="PERCENTAGE_1"  class="form-control " placeholder="percentage" >
                                    </td>
                                    <td>
                                        <?php 
                                            echo form_dropdown('RESULT[]', $examResult, 'default', 'class = "select2 form-control" id="RESULT_1"');
                                        ?>
                                    </td>
                                    <td>
                                        <input type="text" name="SENIORITY[]" id="SENIORITY_1"  class="form-control " placeholder="seniority" >
                                    </td>
                                    <td class="text-center">                       
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="xh" style="text-align: right;">
                        <span class="btn btn-xs btn-success" id="add_record">
                            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/foreignVisitInfo/save':'sailorsInfo/foreignVisitInfo/save' ?>" data-su-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/foreignVisitInfo/fvList':'sailorsInfo/foreignVisitInfo/fvList'?>" data-type="list" value="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>

<?php $this->load->view("common/sailors_info"); ?>
<script>
    var flag = "<?php echo $flag ?>";
    var dateToday = new Date();
     $(document).on('blur', '.s_duration', function () {
        var endDateId = $(this).attr('id');
        var len = endDateId.length;
        var lastD = endDateId.charAt(len-1); /*find id attribute last string*/
        var startDate = $("#startDate").val();
        var start = new Date(startDate);
        var endDate = $("#END_DATE_"+lastD).val();
        var end = new Date(endDate);
        var diff = new Date(end - start);
        var days = diff/1000/60/60/24;
        if(!isNaN(days)){
            if(0<=days){
                $("#S_DURATIONS_" + lastD).val(days);                                    
            }else{                
                alert("Start date must be less or equal to end date");     
                $("#S_DURATIONS_" + lastD).val('');
                $("#END_DATE_" + lastD).val('');
            }
        }
    });
    $("#startDate").datepicker({
        dateFormat: 'mm/dd/yy',        
        onSelect: function(dateText, inst) {
            var start = new Date(dateText);
            var endDate = $("#endDate").val();
            var end = new Date(endDate);
            var diff = new Date(end - start);
            var days = diff/1000/60/60/24;
            if(!isNaN(days)){
                if(0<=days){
                    $('#durations').val(days);                                    
                }else{                
                    alert("Start date must be less or equal to end date");     
                    $('#durations').val('');
                    $('#startDate').val('');
                }
            }
        }
    });
    $("#endDate").datepicker({
        dateFormat: 'mm/dd/yy',
        onSelect: function(dateText, inst) {
            var startDate = $("#startDate").val();
            var start = new Date(startDate);
            var end = new Date(dateText);
            var diff = new Date(end - start);
            var days = diff/1000/60/60/24;
            if(!isNaN(days)){
                if(0<=days){
                    $('#durations').val(days);                                    
                }else{                
                    alert("Start date must be less or equal to end date");     
                    $('#durations').val('');
                    $('#endDate').val('');
                }
            }
        }
         
    });
  
    $(document).on('change', '#VISIT_CI_ID', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/visitInfo_by_visitClass_id',
            type: 'POST',
            dataType: 'html',
            data: {VISIT_CLASS_ID: id},
            beforeSend: function () {
                $(".vistInfo_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('.vistInfo_dropdown').html(data);
            }
        });
        
    });
    $(document).on('change', '#TRAINING_TYPE_ID', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/trainingName_by_trainingType',
            type: 'POST',
            dataType: 'html',
            data: {TRAINING_TYPE: id},
            beforeSend: function () {
                $(".training_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('.training_dropdown').html(data);
            }
        });
        
    });
    // append academic info table
    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#sailorTable tbody").append(' <tr>' +           
            '<td>' +
            ' <input type="text" name="OFFICIAL_NO[]" id="OFFICIAL_NO_' + counter + '"  class="'+ (flag >= 1 ? 'RET_OFFICIAL_NO' : 'OFFICIAL_NO') +' form-control numbersOnly required" placeholder="Official No" >' +
            '</td>' +
            '<td>' +
            ' <input type="text" value="" name="FULLNAME[]" id="FULLNAME_' + counter + '"  class="form-control" placeholder="Full Name" >' +
            '</td>' +            
            '<td>' +
            ' <input type="text" name="RANK[]" id="RANK_' + counter + '"  class="form-control " placeholder="Rank" >' +
            '</td>' +
             '<td>' +
            ' <input type="text" name="s_endDate[]" id="END_DATE_' + counter + '"  class="endDate col-sm-1 form-control required sEndDate" placeholder="End date" >' +
            '</td>' +
             '<td>' +
            ' <input type="text" name="s_durations[]" id="S_DURATIONS_' + counter + '"  class="form-control s_duration" placeholder="duration" >' +
            '</td>' +            
            '<td>' +
            '<select class="select2 form-control" id="GRADE_' + counter + '" name="GRADE[]">' +
                '<option value="">---Select---</option>' +
                <?php foreach ($examGrade_one as $row) { ?>
                '<option value="<?php echo $row->EXAM_GRADEID ?>"><?php echo $row->NAME ?></option>' +
                <?php } ?>
            '</select> ' +
            '</td>' +
            '<td>' +
            ' <input type="text" name="MARKS[]" id="MARKS_' + counter + '"  class="form-control " placeholder="mark" >' +
            '</td>' + 
            '<td>' +
            ' <input type="text" name="PERCENTAGE[]" id="PERCENTAGE_' + counter + '"  class="form-control " placeholder="percentage" >' +
            '</td>' + 
            '<td>' +
            '<select class="select2 form-control" id="RESULT_' + counter + '" name="RESULT[]">' +
                '<option value="">---Select---</option>' +
                <?php foreach ($examResul_one as $row) { ?>
                '<option value="<?php echo $row->EXAM_RESULT_ID ?>"><?php echo $row->NAME ?></option>' +
                <?php } ?>
            '</select> ' +
            '</td>' +
            '<td>' +
            ' <input type="text" name="SENIORITY[]" id="SENIORITY_' + counter + '"  class="form-control " placeholder="seniority" >' +
            '</td>' +

            '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
            '</td>' +
            '</tr>'
        );
        $('.select2').select2();
        $("#END_DATE_"+counter).datepicker();
            
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    $('#sailorTable').removeAttr('width').DataTable( {  
        "scrollX": true,
        "scrollX": true,
        "bPaginate": false,
        "bFilter": false, 
        "bInfo": false,
        "columnDefs": [
            { width: '100px', targets: 0 },
            { width: '100px', targets: 1 },
            { width: '100px', targets: 2 },
            { width: '80px', targets: 3 },
            { width: '100px', targets: 5 },
            { width: '90px', targets: 8 },
            { width: '100px', targets: 9 },
        ],
        "fixedColumns": true

    } );
    $(document).on('blur', '#authorityNumber', function () {
        var authorityNumber = $(this).val();
        if(authorityNumber == ''){
            $("#authorityDate").prop("disabled", true);
            $("#DAO_ID").prop("disabled", true);
            $("#authorityDate").removeClass('required');
            $("#DAO_ID").addClass('required');
        }else{
            $("#authorityDate").prop("disabled", false);
            $("#authorityDate").addClass('required');
            $("#DAO_ID").prop("disabled", false);
            $("#DAO_ID").removeClass('required');
        }
    });
    $("#authorityDate").prop("disabled", true);
    $("#DAO_ID").prop("disabled", true);

</script>