<table id="dataTable" class="display table-striped " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>Visit Info Name</th>
            <th>Description</th>
            <th>Countries</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Durations (Days)</th>
        </tr>
    </thead>
    <tbody>
	<?php
	foreach ($foreignVisit as $row) { ?>
		<tr id="row_<?php echo $row->ForeignVisitID ?>">
            <td><?php echo $row->VI_NAME ?></td>
            <td><?php echo $row->VI_DESCR ?></td>
            <td><?php echo $row->COUNTRY_NAME ?></td>
            <td><?php echo date("d/m/Y", strtotime($row->StartDate)); ?></td>
            <td><?php echo date("d/m/Y", strtotime($row->EndDate)); ?></td>
            <td><?php echo $row->Duration; ?></td>
        </tr>
	<?php }?>
	</tbody>
</table>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var dataTable = $('#dataTable').DataTable();
    });
</script>