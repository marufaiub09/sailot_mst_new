<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var flag = "<?php echo $flag; ?>";
        var tergatUrl;
        if(flag == 1){
            tergatUrl = "Retirement/HistoryTran/specializationInfo/ajaxSpecTranList";   /*Retirement sailor url*/ 
        }else{
            tergatUrl = "sailorsInfo/specializationInfo/ajaxSpecTranList"; /*Active sailor url*/ 
        }
        var dataTable = $('#employee-grid').DataTable({
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>"+tergatUrl, // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }

            }

        });
    });
</script>

<table id="employee-grid" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Sailors Info</th>
            <th>Name</th>
            <th>Rank</th>
            <th>Specialization</th>
            <th>Effect Date</th>
            <th>Authority</th>
            <th>Authority Ship</th>
            <th>DAO Number</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>  
    </tbody>
</table>        