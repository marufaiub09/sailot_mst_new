<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Official No</label>        
        <div class="col-sm-3" >
            <?php $id = ($flag == 1)? 'retofficialNumber':'officialNumber'; ?>
            <?php echo form_input(array('name' => 'officialNo', 'id' => $id, "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number')); ?>           
           <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
        <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Full Name</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly', 'value' => set_value('fullName'))); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>
                
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'rank',  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly', 'value' => set_value('rank'))); ?>
        </div>
    </div>
	<div class="form-group">
		<label class="col-sm-3 control-label">Specialization</label>
		<a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Specialization">
			<i class="fa fa-question-circle"></i>
		</a>
		<div class="col-sm-4">
            <select class="select2 form-control required" name="otherSpecialization" id="otherSpecialization" data-tags="true" data-placeholder="Select Specialization" data-allow-clear="true">
                <option value="">Select Specialization</option>
                <?php
                foreach ($otherSpecialization as $row):
                    ?>
                    <option value="<?php echo $row->OtherSpecializationID ?>"><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
	</div>
	<div class="form-group">
        <label class="col-sm-3 control-label">Effecting Date</label>
        <div class="col-sm-3" date>
           <?php echo form_input(array('name' => 'EffectingDate',  "class" => "datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Clearance date')); ?>
        </div>
        <label class="col-sm-2 control-label">Dao Number</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select  date">
            <i class="fa fa-question-circle"></i>
        </a>        
        <div class="col-sm-3">
            <select class="select2 form-control required" name="DAO_ID" id="DAO_ID" data-tags="true" data-placeholder="Select DAO " data-allow-clear="true">
                <option value="">Select DAO Number</option>
                <?php
                foreach ($daoNumber as $row):
                    ?>
                    <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-3 control-label">Authority Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Authority Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
           <?php echo form_input(array('name' => 'authorityName',  'id' =>'authorityName', 'type'=>'text', "class" => "form-control required",  'placeholder' => 'Enter Authority Name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Authority Ship</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Authority Ship">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="AUTHORITY_SHIP" id="AUTHORITY_SHIP" data-tags="true" data-placeholder="Select ship" data-allow-clear="true">
                <option value="">Select ship</option>
                <?php
                foreach ($shipEstablishment as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
	<div class="form-group">
        <label class="col-sm-3 control-label">Authority Number</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'authorityNumber', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Authority Number')); ?>
        </div>
        <label class="col-sm-2 control-label">Authority Date</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Authority date">
            <i class="fa fa-question-circle"></i>
        </a>        
        <div class="col-sm-3 date">
           <?php echo form_input(array('name' => 'authorityDate',  "class" => "datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Authority date')); ?>
        </div>
    </div>
	
	
	
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/specializationInfo/save') : site_url('sailorsInfo/specializationInfo/save') ?>" data-su-action="<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/SpecializationInfo/spacialList') : site_url('sailorsInfo/SpecializationInfo/spacialList') ?>" data-type="list" value="submit">
        </div>
    </div>
</form>
<script>
    $("#authorityName").on('blur', function(){
        var name = $(this).val();
        if(name == ''){
            $("#AUTHORITY_SHIP").prop("disabled", false);
            $("#AUTHORITY_SHIP").addClass('required');
        }else{
            $("#AUTHORITY_SHIP").prop("disabled", true);
            $("#AUTHORITY_SHIP").removeClass('required');
        }
    });
    $("#AUTHORITY_SHIP").on('change', function(){
        var ship = $(this).val();
        if(ship == ''){
            $("#authorityName").prop("disabled", false);
            $("#authorityName").addClass('required');
        }else{
            $("#authorityName").prop("disabled", true);
            $("#authorityName").removeClass('required');
        }
    });
</script>
<?php $this->load->view("common/sailors_info"); ?>


