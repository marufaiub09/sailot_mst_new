<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    
    <div class="form-group">
        <label class="col-sm-2 control-label">Official No</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'code', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Full Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Enter Sailor Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'name', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly')); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Rank">
            <i class="fa fa-question-circle"></i>
        </a>        
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'name',  "class" => "form-control required",'required'=>'required', 'value' => '', 'placeholder' => 'Rank', 'readonly'=>'readonly')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Remarks</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Over Weight">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4" >
           <?php echo form_textarea(array('name' => 'remarks', "class" => "form-control required", 'required'=>'required','placeholder' => 'Remarks', 'cols' => '12','rows' => '4')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/trade/saveTrade" data-su-action="setup/trade/tradeList" data-type="list" value="submit">
        </div>
    </div>
</form>
