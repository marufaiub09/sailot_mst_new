<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Official No</th>
            <th>Name</th>
            <th>Rank</th>
            <th>Remarks</th>
            <th>Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody><!-- 
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->TRADE_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->CODE ?></td>
                <td><?php echo $row->NAME ?></td>
                <td><?php echo $row->BRANCH_NAME ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->TRADE_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="TRADE_ID" data-field="ACTIVE_STATUS" data-tbl="bn_trade" data-su-url="setup/trade/tradeById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>
    
                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/trade/view/' . $row->TRADE_ID); ?>" title="View trade" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/trade/edit/' . $row->TRADE_ID); ?>"  title="Edit trade" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->TRADE_ID; ?>" title="Click For Delete" data-type="delete" data-field="TRADE_ID" data-tbl="bn_trade"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?> -->
        <tr id="row_<?php //echo $row->TRADE_ID ?>">
            <td>1</td>
            <td>20160001</td>
            <td>Md. Ikbal Hossain</td>
            <td>Acting Sub Lieutenant </td>
            <td>Remarks information</td>
            <td>01/05/2016</td>            

            <td>
            <center>
                <a class="btn btn-success btn-xs" href="#" title="View trade" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                <a class="btn btn-warning btn-xs"  href="#"  title="Edit trade" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            </center>
            </td>
        </tr>
    </tbody>
</table>