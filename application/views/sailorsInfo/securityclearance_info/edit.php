<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Official No</label>        
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number', 'readonly'=>'readonly','value' => $result->OFFICIALNUMBER)); ?>           
           <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
        <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Full Name</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly', 'value' => $result->FULLNAME)); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>
                
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'rank',  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly', 'value' => $result->RANK_NAME)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Ship/Establishment</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Ship/Establishment">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
           <?php echo form_input(array('name' => 'ship_establishment', 'type'=>'text', "class" => "SHIP_ESTALISHMENT form-control required", 'required'=>'required', 'readonly'=>'readonly',  'value' => $result->SHIP_ESTABLIHSMENT)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Clearance Type</label>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="clearance_ID" id="clearance_ID" data-tags="true" data-placeholder="Select Clearance Type" data-allow-clear="true">
                <option value="">Select Clearance Type</option>
                <?php
                foreach ($clearanceType as $row):
                    ?>
                    <option value="<?php echo $row->CLEARANCE_TYPEID ?>" <?php echo ($row->CLEARANCE_TYPEID == $result->ClearanceTypeID) ? 'selected': '' ?> ><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
        <a class="help-icon col-sm-1" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Clearance Type">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-1">
           <?php echo form_checkbox('CLEARED', '1', ($result->IsClearance ==1)? TRUE: FALSE, 'class="styled"'); ?>
        </div>
        <div class="col-sm-1">
           Cleared
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Clearance Date</label>
        <div class="col-sm-2" date>
           <?php echo form_input(array('name' => 'clearanceDate',  "class" => "datePicker form-control required input-form-padding-right-empty",'required'=>'required', 'value' => date('d-m-Y', strtotime($result->TranDate)), 'placeholder' => 'Clearance date')); ?>
        </div>
        <label class="col-sm-3 control-label">Valid Up To</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select  date">
            <i class="fa fa-question-circle"></i>
        </a>        
        <div class="col-sm-2 date">
           <?php echo form_input(array('name' => 'validUpTo',  "class" => "datePicker form-control required input-form-padding-right-empty",'required'=>'required', 'value' =>date('d-m-Y', strtotime($result->ValidDate)), 'placeholder' => 'Valid UpTo date')); ?>
        </div>
    </div>    
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Authority Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Authority Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
           <?php echo form_input(array('name' => 'authorityName',  'type'=>'text', "class" => "form-control required ", 'required'=>'required', 'placeholder' => 'Enter Authority Name', 'value' => $result->AuthorityName)); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Authority Number</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'authorityNumber', "class" => "form-control required input-form-padding-right-empty",'required'=>'required', 'placeholder' => 'Authority Number', 'value' => $result->AuthorityNumber)); ?>
        </div>
        <label class="col-sm-2 control-label"> Date</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Authority date">
            <i class="fa fa-question-circle"></i>
        </a>        
        <div class="col-sm-2 date">
           <?php echo form_input(array('name' => 'authorityDate',  "class" => "datePicker form-control required input-form-padding-right-empty",'required'=>'required', 'value' =>date('d-m-Y', strtotime($result->AuthorityDate)), 'placeholder' => 'Authority date')); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Remarks</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Remarks">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
           <?php echo form_textarea(array('name' => 'remarks', "class" => "form-control",'placeholder' => 'Remarks', 'cols' => '12','rows' => '3', 'value' => $result->Remarks)); ?>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->ClearanceTranID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/securityClearanceInfo/update" data-su-action="sailorsInfo/securityClearanceInfo/clearanceList" data-type="list" value="Update">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>