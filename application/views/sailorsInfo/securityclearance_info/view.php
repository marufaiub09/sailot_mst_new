<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official No</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr>
    <tr>
        <th width="20%">Clearance For</th>
        <td><?php echo $viewdetails->CLEAR_TYPE ?></td>
    </tr>
    <tr>
        <th>Clearance (Y/N)</th>
        <td><?php echo ($viewdetails->IsClearance == 1)? "Yes" : "NO"?></td>
    </tr>
    <tr>
        <th>Clearance Date</th>
        <td><?php echo date("d/m/Y", strtotime($viewdetails->TranDate))?></td>
    </tr>
    <tr>
        <th>Valid Up To</th>
        <td><?php echo date("d/m/Y", strtotime($viewdetails->ValidDate)) ?></td>
    </tr>
    <tr>
        <th>Authority Name</th>
        <td><?php echo $viewdetails->AuthorityName?></td>
    </tr>
    <tr>
        <th>Authority Number</th>
        <td><?php echo date("d/m/Y", strtotime($viewdetails->AuthorityNumber))?></td>
    </tr>
    <tr>
        <th>Authority date </th>
        <td><?php echo date("d/m/Y", strtotime($viewdetails->AuthorityDate))?></td>
    </tr>
    <tr>
        <th>Remarks</th>
        <td><?php echo $viewdetails->Remarks?></td>
    </tr>    
    
</table>