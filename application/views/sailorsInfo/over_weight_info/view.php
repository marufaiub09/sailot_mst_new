
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        
        <tr>
            <th width="20%">Official No</th>
            <td><?php echo $viewdetails->OFFICIALNUMBER?></td>
        </tr>
        <tr>
            <th>Name</th>
            <td><?php echo $viewdetails->FULLNAME?></td>
        </tr>
        <tr>
            <th>Rank</th>
            <td><?php echo $viewdetails->RANK_NAME?></td>
        </tr>
        <tr>
            <th>Over weight (kg)</th>            
            <td><?php echo $viewdetails->WEIGHT?></td>
        </tr>
        </tr><tr>
            <th>Return Auth (Ship/Estab)</th>            
            <td><?php echo $viewdetails->SHIP_ESTABLISHMENT?></td>
        </tr>
        <tr>
            <th>Return Date</th>            
            <td><?php echo date('Y-m-d',strtotime($viewdetails->DATE_OF_RETURN)); ?></td>
        </tr>
        <tr>
            <th>Authority Number</th>            
            <td><?php echo $viewdetails->AUTHORITY_NUMBER?></td>
        </tr><tr>
            <th>Authority Date</th>            
            <td><?php echo date('Y-m-d',strtotime($viewdetails->AUTHORITY_DATE)); ?></td>
        
</table>