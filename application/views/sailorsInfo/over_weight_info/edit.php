<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-2 control-label">Date of Return</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Date of Return">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3 date" >
           <?php echo form_input(array('name' => 'dateOfReturn',  "class" => "datePicker form-control required", 'required'=>'required', 'value' => date('d-m-Y', strtotime($result->DATE_OF_RETURN)), 'placeholder' => 'Date of return')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Authority No</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'authorityNo','value'=>$result->AUTHORITY_NUMBER, "class" => "form-control required",'required'=>'required', 'placeholder' => 'Number')); ?>
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Authority date">
            <i class="fa fa-question-circle"></i>
        </a>
        </div>
        <label class="col-sm-2 control-label">Authority Date</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Authority date">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3 date">
           <?php echo form_input(array('name' => 'authorityDate','value'=> date('Y-m-d', strtotime($result->AUTHORITY_DATE)),  "class" => "datePicker form-control required",'required'=>'required',  'placeholder' => 'Authority date')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Authority Ship</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select  Authority Ship">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="SHIP_ID" id="SHIP_ID" data-tags="true" data-placeholder="Select ship" data-allow-clear="true">
                <option value="">Select ship</option>
                <?php
                foreach ($shipEstablishment as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($result->SHIP_ID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Official No</label>
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'officialNo','value'=>$result->OFFICIALNUMBER,
           'id' => 'officialNo', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number', 'readonly'=>'readonly')); ?>
           <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
        <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Full Name</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'fullName','value'=>$result->FULLNAME, "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly')); ?>
        </div>
        <label class="col-sm-2 control-label">Rank</label>

        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'rank','value'=>$result->RANK_NAME,  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">OverWeight (kg)</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Over Weight">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'OverWeight', 'value' => $result->WEIGHT, "class" => "form-control required", 'required'=>'required','placeholder' => 'Weight')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->OVER_WEIGHT_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/overweightInfo/update" data-su-action="sailorsInfo/overweightInfo/overWeightList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>
