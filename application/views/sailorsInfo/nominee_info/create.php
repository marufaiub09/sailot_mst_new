<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-2 control-label">Official No</label>        
        <div class="col-sm-3" >
           <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNo', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number')); ?>           
           <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
        </div>
        <div class="col-sm-1">
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                <i class="fa fa-question-circle"></i>
            </a>
        </div>
        <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">Full Name</label>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly', 'value' => set_value('fullName'))); ?>
        </div>
        <label class="col-sm-3 control-label">Rank</label>
                
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'rank',  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly', 'value' => set_value('rank'))); ?>
        </div>
    </div>
	
	<div class="form-group">
        <label class="col-sm-2 control-label">Nominee Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Nominee Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-5">
           <?php echo form_input(array('name' => 'nomineeName',  'type'=>'text', "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Enter Nominee Name')); ?>
        </div>
    </div>
	
	<div class="form-group">
        <label class="col-sm-2 control-label">Relation</label>
        <div class="col-sm-3">
           <select class="select2 form-control required" name="RELATION_ID" id="RELATION_ID" data-tags="true" data-placeholder="Select relation" data-allow-clear="true">
                <option value="">Select Relation</option>
                <?php
                foreach ($relation as $row):
                    ?>
                    <option value="<?php echo $row->RELATION_ID ?>"><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
        <a class="col-sm-1 help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Relation with Sailor">
            <i class="fa fa-question-circle"></i>
        </a>

        <label class="col-sm-2 control-label">Percentage</label>
        <div class="col-sm-2">
           <?php echo form_input(array('name' => 'percentage', 'id' => 'percentage', 'maxlength' =>'3', 'pattern' =>'^[0-9]', 'type'=>'text', "class" => "form-control required", 'required'=>'required', 'placeholder' => 'percentage')); ?>
        </div>
        <a class="col-sm-1 help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Percentage">
            <i class="fa fa-question-circle"></i>
        </a>
    </div>
   <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6" style="margin-left: 170px">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="sailorsInfo/nomineeInfo/save" data-su-action="sailorsInfo/nomineeInfo/nomineeList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script>
    $("#percentage").on('keyup', function(){
        if(!(parseInt($(this).val()) <= 100)) {
            alert("Number is greater than 100");
            $(this).val('');
       }
    });
</script>
<?php $this->load->view("common/sailors_info"); ?>