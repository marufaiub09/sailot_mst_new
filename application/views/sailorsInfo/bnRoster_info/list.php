  
<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>OfficialNum</th>
            <th>Name</th>
            <th>Rank</th>
            <th>Marriage Date</th>
            <th>Apply Date</th>
			<th>AppliedShip/EST</th>
			<th>No Of Child</th>
			<th>Date Of Entry</th>
			<th>Date Of Promotion</th>
			<th>Date Of Birth</th>
			<th>Entry User</th>
			<th>Entry Date</th>
			<th>Update User</th>
			<th>Update Date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
		    <th>SN</th>
            <th>OfficialNum</th>
            <th>Name</th>
            <th>Rank</th>
            <th>Marriage Date</th>
            <th>Apply Date</th>
			<th>AppliedShip/EST</th>
			<th>No Of Child</th>
			<th>Date Of Entry</th>
			<th>Date Of Promotion</th>
			<th>Date Of Birth</th>
			<th>Entry User</th>
			<th>Entry Date</th>
			<th>Update User</th>
			<th>Update Date</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php  echo $row->VISIT_INFO_ID ?>">
                <td><?php  echo $key + 1 ?></td>
                <td>810249</td>
                <td><?php  echo $row->NAME ?></td>
                <td>Auth Rank</td>
				<td>02-05-1996</td>
				<td>02-05-1986</td>
                <td>02-05-2016</td>
				<td>2 </td>
				<td>02-05-1996</td>
				<td>02-05-2016 </td>
				<td>02-05-1976 </td>
				<td>Entry User </td>
				<td>02-05-2016 </td>
				<td>Update User </td>
				<td>02-05-2016 </td>

                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('sailorsInfo/BnRosterInfo/view/' . $row->VISIT_INFO_ID); ?>" title="View BnRosterInfo Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('sailorsInfo/BnRosterInfo/edit/' . $row->VISIT_INFO_ID); ?>"  title="Edit BnRosterInfo Information" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->VISIT_INFO_ID; ?>" title="Click For Delete" data-type="delete" data-field="VISIT_INFO_ID" data-tbl="bn_visitinformation"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>