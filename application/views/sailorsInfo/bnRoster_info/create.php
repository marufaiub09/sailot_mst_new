<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <fieldset class="form-group">
        <legend style="font-size: 14px; font-weight: bold; padding-left: 20px;">Current Information:</legend>
        <div class="form-group">
            <label class="col-sm-3 control-label">Official No</label>        
            <div class="col-sm-3" >
               <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required'=>'required','placeholder' => 'Official Number')); ?>           
               <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
            </div>
            <div class="col-sm-1">
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                    <i class="fa fa-question-circle"></i>
                </a>
            </div>
            <div class="col-sm-4 danger"><span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Full Name</label>
            <div class="col-sm-3">
               <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required",'required'=>'required', 'placeholder' => 'Sailor name', 'readonly'=>'readonly', 'value' => set_value('fullName'))); ?>
            </div>
            <label class="col-sm-2 control-label">Rank</label>
                    
            <div class="col-sm-3">
               <?php echo form_input(array('name' => 'rank',  "class" => "form-control rank required",'required'=>'required', 'placeholder' => 'Rank', 'readonly'=>'readonly', 'value' => set_value('rank'))); ?>
            </div>
        </div>
    	
    	<div class="form-group">
           <label class="col-sm-3 control-label">Posting Unit</label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Unit">
                <i class="fa fa-question-circle"></i>
            </a>        
            <div class="col-sm-3">
               <?php echo form_input(array('name' => 'official_number',  'type'=>'text', "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Enter Posting Unit')); ?>
            </div>
    		<label class="col-sm-2 control-label">Posting Date</label>
               <div class="col-sm-2" date>
               <?php echo form_input(array('name' => 'name',  "class" => "input-form-padding-right-empty datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Clearance date')); ?>
            </div>
        </div>
    	<div class="form-group">
            <label class="col-sm-3 control-label">Joining Date</label>
            <div class="col-sm-2">
               <?php echo form_input(array('name' => 'name',  "class" => "input-form-padding-right-empty datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Joining Date')); ?>
            </div>
            <label class="col-sm-3 control-label">Birth Date</label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Birth date">
                <i class="fa fa-question-circle"></i>
            </a>        
            <div class="col-sm-2 date">
               <?php echo form_input(array('name' => 'name',  "class" => "input-form-padding-right-empty datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Authority date')); ?>
            </div>
        </div>
    	<div class="form-group">
            <label class="col-sm-3 control-label">Promotion Date</label>
            <div class="col-sm-2">
               <?php echo form_input(array('name' => 'name',  "class" => "input-form-padding-right-empty datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Joining Date')); ?>
            </div>
            <label class="col-sm-3 control-label">Marriage Date</label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Birth date">
                <i class="fa fa-question-circle"></i>
            </a>        
            <div class="col-sm-2 date">
               <?php echo form_input(array('name' => 'name',  "class" => "input-form-padding-right-empty datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Authority date')); ?>
            </div>
        </div>
    	
    	<div class="form-group">
            <label class="col-sm-3 control-label">No of Children</label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter No of Children">
                <i class="fa fa-question-circle"></i>
            </a>
            <div class="col-sm-4">
               <?php echo form_input(array('name' => 'details',  'type'=>'text', "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Enter No of Children')); ?>
            </div>
        </div>
    </fieldset>
	<fieldset class="form-group">
        <legend style="font-size: 14px; font-weight: bold; padding-left: 20px;">New Information:</legend>
    	<div class="form-group">
           <label class="col-sm-3 control-label">Apply Ship</label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Unit">
                <i class="fa fa-question-circle"></i>
            </a>        
            <div class="col-sm-3">
               <?php echo form_input(array('name' => 'official_number',  'type'=>'text', "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Enter Posting Unit')); ?>
            </div>
    		<label class="col-sm-2 control-label">Apply Date</label>
               <div class="col-sm-2" date>
               <?php echo form_input(array('name' => 'name',  "class" => "input-form-padding-right-empty datePicker form-control required",'required'=>'required', 'value' => date('d-m-Y'), 'placeholder' => 'Clearance date')); ?>
            </div>
        </div>
	</fieldset>
	<div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/visitinformation/saveVI" data-su-action="setup/visitinformation/viList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>
<script>
   /*  $(document).ready(function() {
        $("#ZONE_ID").on('change', function(){
           var zone = this.value;
           $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>/sailorsInfo/assessmentInfo/areaByZone",
                data: {zoneId: zone},
                success: function (data) {
                    $("#AREA_ID").html(data);
                }
            });
        });
    }); */
</script>

<script>
    /* $(document).ready(function() {
        $("#AREA_ID").on('change', function(){
			var area = this.value;
		
           $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>/sailorsInfo/assessmentInfo/shipbyarea",
                data: {areaId: area},
                success: function (data) {
                    $("#SHIP_TYPEID").html(data);
                }
            });
        });
    }); */
</script>


