<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->vc_name ?></td>
<td><?php echo $row->DESCR ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->VISIT_INFO_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="VISIT_INFO_ID" data-field="ACTIVE_STATUS" data-tbl="bn_visitinformation" data-su-url="setup/visitinformation/viById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/visitinformation/view/' . $row->VISIT_INFO_ID); ?>" title="View Visit Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/visitinformation/edit/' . $row->VISIT_INFO_ID); ?>"  title="Edit Visit Information" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->VISIT_INFO_ID; ?>" title="Click For Delete" data-type="delete" data-field="VISIT_INFO_ID" data-tbl="bn_visitinformation"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>