<style type="text/css">
    .pull-right{
        margin-bottom: 5px;
        margin-top: -8px;
    }
    body{
        background-color:#F3F3F4;        
        
    }
    .container{
        background-image: url("<?php echo site_url('dist/img/body-img.jpg'); ?>" );
        background-repeat: no-repeat;      
        width: 98% !important;
    }

    .modal-content{
        opacity: 0.7;
    }
</style>
<div id="login-overlay" class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <img class="center-block img-responsive" src="<?php echo site_url('dist/img/navy_logo.jpg'); ?>" style="width: 140px; height: 140px;"/>
            <h4 class="modal-title text-center">Sailor Management System</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-2"></div>
                <div class="col-sm-8">
                    <div class="well">
                        <?php echo form_open("", "id='formSignin' "); ?>

                        <span class="error">
                            <?php echo validation_errors(); ?>
                        </span>

                        <div class="form-group">
                            <label for="txtUserName" class="control-label">Username</label>
                            <input type="text" class="form-control" id="txtUserName" name="txtUserName" value="<?php echo set_value('txtUserName'); ?>" title="Please enter you username" placeholder="Username" required autofocus>
                            <span class="help-block"></span>
                        </div>

                        <div class="form-group">
                            <label for="txtPassword" class="control-label">Password</label>
                            <input type="password" class="form-control" id="inputPassword" name="txtPassword" value="<?php echo set_value('txtPassword'); ?>" title="Please enter your password" placeholder="Password" required>
                            <span class="help-block"></span>
                        </div>
                        <div id="loginErrorMsg" class="alert alert-error hide">Wrong username og password</div>

                        <div class="checkbox checkbox-inline checkbox-primary">
                            <input type="checkbox" class="styled styled-primary" id="remember_me" value="option1">
                            <a class ="pull-right" href="<?php echo site_url('auth/forgetPasswordView'); ?>">Forgotten Password?</a>
                            <!--<label for="remember_me"> Remember me </label>
                            <p class="help-block pricom">(if this is a private computer)</p>-->
                            <p class="help-block pricom">&nbsp;</p>
                        </div>
                        <button class="btn btn-primary btn-block" type="submit">Sign in</button>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="howToRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo (empty($howToRegiInfo))?'': $howToRegiInfo->S_INFO_TITLE; ?></h4>
            </div>
            <div class="modal-body">
                <?php echo (empty($howToRegiInfo))?'': $howToRegiInfo->S_INFO_DESC; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php if(!empty($howToRegiInfo)): ?>
                    <a target='_blank' href='<?php echo site_url("auth/print_information/$howToRegiInfo->S_INFO_ID"); ?>'><button type="button" class="btn btn-primary"><?php echo $this->lang->line("msg_print"); ?></button></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="eliCriInfo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo (empty($eliCriInfo))?'': $eliCriInfo->S_INFO_TITLE; ?></h4>
            </div>
            <div class="modal-body">
                <?php echo (empty($eliCriInfo))?'': $eliCriInfo->S_INFO_DESC; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <?php if(!empty($eliCriInfo)): ?>
                    <a target='_blank' href='<?php echo site_url("auth/print_information/$eliCriInfo->S_INFO_ID"); ?>'><button type="button" class="btn btn-primary"><?php echo $this->lang->line("msg_print"); ?></button></a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $( document ).ready(function() {
        $('#formSignin').formValidation({
            framework: 'bootstrap',
            excluded: ':disabled',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            addOns: {
                mandatoryIcon: {
                    icon: 'glyphicon glyphicon-asterisk'
                }
            }
        });

    });
</script>