<?php
header("Content-type: text/css");
$white = '#fff';
$dkgray = '#333';
$dkgreen = '#008400';
?>
<style>

    body {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        margin-top: <?php echo $_POST['topMargin'] ?>;
        margin-bottom: <?php echo $_POST['bottomMargin'] ?>;
        margin-left: <?php echo $_POST['leftMargin'] ?>;
        margin-right: <?php echo $_POST['rightMargin'] ?>;

    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table td, .table th {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        border: 1px <?php echo $_POST['borderType'] ?> <?php echo $_POST['borderColour'] ?>;
    }

    table.table-condensed {
        border: 1px solid <?php echo $dkgreen ?>;
    }
</style>


<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php echo base_url('dist/img/navy_logo2.jpg'); ?>"/>
    <h5 style="font-size: <?php echo $_POST['fontSize'] ?>">Sailor Management System.</h5>
    <span style=" text-align: center"><b><?php echo $_POST['reportTitle']; ?></b><hr></span>
</div>
<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <?php
            error_reporting('0');

            foreach ($_POST['operatortype'] as $dataQuery) {
                $dataQuery;
            }
            if (empty($_POST['columnName'])) {
                $data1['result'] = $this->db->list_fields($_POST['tableName']);
                foreach ($data1['result'] as $value) {
                    ?>
                    <th><?php echo $value; ?></th>
                    <?php
                }
            } else {
                foreach ($_POST['columnName'] as $value) {
                    ?>
                    <th><?php echo $value; ?></th>
                    <?php
                }
            }
            ?>
        </tr>
    </thead>

    <tbody>
        <?php
        //error_reporting('0');
        if (!empty($_POST['operatortype'])) {

            foreach ($_POST['likeOption'] as $value3) {
                $value3;
                //$str = "Name [varchar]";
                $str = explode(".", $value3);
                $value3 = $str[0];
                $value3;
            }

            foreach ($_POST['operatortype'] as $value1) {
                // echo  $value1;
                $value1;
            }
            $value = array();
            foreach ($_POST['options'] as $key => $value2) {
                $value2;
                $value[$key] = $value2;
            }
            foreach ($_POST['join'] as $value9) {
                $value9;
            }
        }
        ?>
        <?php
        /* $regEx = '/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|1‌​1)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468]‌​[048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(0‌​2)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02‌​)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/'; */

        switch (!empty($value1)) {
            case ($value9 == 'join'):
                foreach ($_POST['ortableName'] as $value8) {
                    $value8;
                }
                foreach ($_POST['orclName'] as $value7) {

                    $value7;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value7);
                    $value7 = $str[0];
                }
                foreach ($_POST['anothertlName'] as $value78) {
                    $value78;
                }
                foreach ($_POST['anothertlName'] as $value78) {
                    $value78;
                }
                foreach ($_POST['anotherclName'] as $value79) {

                    $value79;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value79);
                    $value79 = $str[0];
                    ;
                }
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    $this->db->from($_POST['ortableName']);
                    $this->db->join($value78, "$value8.$value7 = $value78.$value79");
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    $this->db->from($_POST['ortableName']);
                    $this->db->join($value78, "$value8.$value7 = $value78.$value79");
                    $data = $this->db->get()->result();
                }
                break;
            case ($value1 == '<'):
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '<', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '<', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }
                break;

            case ($value1 == '<='):
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '<=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '<=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }
                break;

            case ($value1 == '>'):
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '>', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '>', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }

                break;

            case ($value1 == '>='):
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '>=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '>=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }

                break;
            case ($value1 == '!='):
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '!=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '!=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }
                break;

            case ($value1 == '='):
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '=', $value[$key]);
                        //var_dump($this->db->where($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }
                break;

            case ($value1 == 'Like'):
                if (empty($_POST['columnName'])) {

                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];

                        $this->db->like($value3, $value[$key]);
                        //var_dump($this->db->like($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->like($value3, $value[$key]);
                        //var_dump($this->db->like($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }

                break;

            case ($value1 == 'NotLike'):
                if (empty($_POST['columnName'])) {

                    $this->db->select('*');
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];

                        $this->db->not_like($value3, $value[$key]);
                        //var_dump($this->db->like($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->not_like($value3, $value[$key]);
                        //var_dump($this->db->like($value3,$value[$key]));
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                }

                break;

            case($value1 == 'select'):
                if (empty($_POST['columnName'])) {
                    $data1['result'] = $this->db->list_fields($_POST['tableName']);
                    foreach ($data1['result'] as $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->select($value3);
                    }
                    $this->db->from($_POST['tableName']);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    $this->db->from($_POST['tableName']);
                    //var_dump($this->db->from($_POST['tableName']));
                    $data = $this->db->get()->result();
                }

                break;

            /* case ($value1 == 'group'):
              if(empty($_POST['columnName']))
              {
              $this->db->select('*');
              $this->db->from($_POST['tableName']);
              $this->db->group_by($value[$key]);
              $data = $this->db->get()->result();
              }
              else
              {
              $this->db->select($_POST['columnName']);
              $this->db->from($_POST['tableName']);
              $this->db->group_by($value[$key]);
              $data = $this->db->get()->result();
              }

              break; */

            /* case ($value1 == 'distinct'):
              if(empty($_POST['columnName']))
              {
              $this->db->select('*');
              $this->db->from($_POST['tableName']);
              $this->db->distinct($value2);
              $data = $this->db->get()->result();
              }
              else
              {
              $this->db->select($_POST['columnName']);
              $this->db->from($_POST['tableName']);
              $this->db->distinct($value2);
              $data = $this->db->get()->result();
              }

              break; */
            case ($value1 == 'limit'):
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    $this->db->from($_POST['tableName']);
                    $this->db->limit($value2);
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    $this->db->from($_POST['tableName']);
                    $this->db->limit($value2);
                    $data = $this->db->get()->result();
                }

                break;
            case ($value1 == 'between'):
                $start = $_POST['startDate'];
                $end = $_POST['endDate'];
                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    $this->db->from($_POST['tableName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];

                        $this->db->where($value3 . '>=', date("Y-m-d", strtotime($start)));

                        $this->db->where($value3 . '<=', date("Y-m-d", strtotime($end)));
                    }
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    $this->db->from($_POST['tableName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];
                        $this->db->where($value3 . '>=', date("Y-m-d", strtotime($start)));

                        $this->db->where($value3 . '<=', date("Y-m-d", strtotime($end)));
                    }
                    $data = $this->db->get()->result();
                }

                break;

            case ($value1 == 'toDate'):
                $start = $_POST['todayDate'];

                if (empty($_POST['columnName'])) {
                    $this->db->select('*');
                    $this->db->from($_POST['tableName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        //$this->db->where($value3,date("Y-m-d",strtotime($start)));
                        $this->db->like($value3, date("Y-m-d", strtotime($start)));
                    }
                    $data = $this->db->get()->result();
                } else {
                    $this->db->select($_POST['columnName']);
                    $this->db->from($_POST['tableName']);
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $this->db->like($value3, date("Y-m-d", strtotime($start)));
                    }
                    $data = $this->db->get()->result();
                }

                break;
        }

        /* echo '<pre>';
          print_r($data);
          exit(); */
        ?>



        <?php
        foreach ($data as $row) {
            ?>
            <tr>
                <?php
                if (empty($_POST['columnName'])) {
                    $data1['result'] = $this->db->list_fields($_POST['tableName']);
                    foreach ($data1['result'] as $value) {
                        ?>
                        <td><?php echo $row->$value; ?></td>
                        <?php
                    }
                } else {
                    foreach ($_POST['columnName'] as $value) {
                        ?>
                        <td><?php echo $row->$value; ?></td>
                        <?php
                    }
                }
                ?>
            </tr>
            <?php
        }
        ?>

    </tbody>
</table>

