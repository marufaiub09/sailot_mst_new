
<!-- FOR same ID  -->
<script type="text/javascript">
    /*$(document).ready(function () {
     $("#datatable_2").dataTable();
     $("#datatable_3").dataTable();
     });
     */

</script>
<!-- End of same ID -->

<!-- START: For multiple Submit button in one Form -->
<script type="text/javascript">
    function submitForm(action)
    {
        document.getElementById('form1').action = action;
        document.getElementById('form1').submit();
    }
</script>
<!--END:For multiple Submit button in one Form -->



<!-- For on Change dropdown value -->
<script>
    $(document).ready(function () {
        $("#tableName").on('change', function () {
            $('#columnName').select2('val', '');
            var name = this.value;
            // alert(name);
            $("textarea#fromTable").val(name);
            $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>report/ReportSetup/checkColumn",
                data: {Tablename: name},
                success: function (data) {
                    $("#columnName").html(data);
                }
            });
        });
    });
</script>


<script>
    $(document).ready(function () {
        $("#tableName").on('change', function () {
            // $('#likeOption').select2('val', '');
            var name = this.value;
            // alert(name);
            //$("textarea#fromTable").val(name);
            $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>report/ReportSetup/checkTable",
                data: {Tablename: name},
                success: function (data) {
                    $("#likeOption").html(data);
                }
            });
        });
    });
</script>


<script>
    $(document).ready(function () {
        $("#likeOption").on('change', function () {
            //$('#operatorType').select2('val', '');
            var name = this.value;
            //alert(name);
            $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>report/ReportSetup/checkColumnType",
                data: {likeOption: name},
                success: function (data) {
                    $("#operatorType").html(data);
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        $("#operatorType").on('change', function () {
            //$('#bodyspan').select2('val', '');
            var name = this.value;
            //alert(name);
            $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>report/ReportSetup/express",
                data: {operatorType: name},
                success: function (data) {
                    $("#bodyspan").html(data);
                }
            });
        });
    });
</script>
<!-- FOR Click Check box and enable or disable value-->
<script>
    $(document).ready(function () {
        $("#html").click(function () {
            $("#htmlbutton").attr('disabled', !this.checked)
            $("#pdf").attr('disabled', this.checked)
        });
    });
</script>
<script>

    $(document).ready(function () {
        $("#pdf").click(function () {
            $("#pdfbutton").attr('disabled', !this.checked)
            $("#html").attr('disabled', this.checked)
        });
    });
</script>



<!-- For Select multiple value from dropdown -->
<script type="text/javascript">
    function getSelectedOptions(sel, fn) {
        var opts = [], opt;

        // loop through options in select list
        for (var i = 0, len = sel.options.length; i < len; i++) {
            opt = sel.options[i];

            // check if selected
            if (opt.selected) {
                // add to array of option elements to return from this function
                opts.push(opt);

                // invoke optional callback function if provided
                if (fn) {
                    fn(opt);
                }
            }
        }

        // return array containing references to selected option elements
        return opts;
    }

    // example callback function (selected options passed one by one)
    function callback(opt) {
        // can access properties of opt, such as...
        //alert( opt.value )
        //alert( opt.text )
        //alert( opt.form )


        // display in textarea for this example
        var display = document.getElementById('display');
        display.innerHTML += opt.value + ', ';
    }

    // anonymous function onchange for select list with id columnName
    document.getElementById('columnName').onchange = function (e) {
        // get reference to display textarea
        var display = document.getElementById('display');
        display.innerHTML = ''; // reset

        // callback fn handles selected options
        getSelectedOptions(this, callback);

        // remove ', ' at end of string
        var str = display.innerHTML.slice(0, -2);
        display.innerHTML = str;
    };

    document.getElementById('columnName').onsubmit = function (e) {
        // reference to select list using this keyword and form elements collection
        // no callback function used this time
        var opts = getSelectedOptions(this.elements['columnName[]']);

        alert('The number of options selected is: ' + opts.length); //  number of selected options

        return false; // don't return online form
    };

    window.onmousedown = function (e) {
        var el = e.target;
        if (el.tagName.toLowerCase() === 'option' && el.parentNode.hasAttribute('multiple')) {
            e.preventDefault();

            var display = document.getElementById('display');

            // toggle selection
            if (el.hasAttribute('selected'))
            {
                el.removeAttribute('selected');
                var str = display.innerHTML;
                str = str.replace(new RegExp(el.value + ",?"), '');
                display.innerHTML = str;
            }
            else {
                el.setAttribute('selected', '');
                display.innerHTML += el.value + ', ';
            }

            // hack to correct buggy behavior
            var select = el.parentNode.cloneNode(true);
            el.parentNode.parentNode.replaceChild(select, el.parentNode);


        }
    }
</script>

<script>
//plugin bootstrap minus and plus
//http://jsfiddle.net/laelitenetwork/puJ6G/
    $('.btn-number').click(function (e) {
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });

</script>




