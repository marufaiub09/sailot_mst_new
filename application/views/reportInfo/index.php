<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Report Design</h3>
                    </div>
                    <!-- <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink" href="<?php //echo site_url('sailorsInfo/AssessmentInfo/create'); ?>" title="Create Assessment Information">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div> -->
                </div>


                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">
			   <!--div class="form-group">
					<label class="col-sm-3 control-label">Official Number</label>
					<a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Official Number">
						<i class="fa fa-question-circle"></i>
					</a>
					<div class="col-sm-3">
					   <?php// echo form_input(array('name' => 'code',  'type'=>'text', "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Enter Official Number')); ?>
					</div>
				</div-->
				
                <div class="contentArea">

				
                    <?php $this->load->view("reportInfo/list.php"); ?>

                    <?php //$this->load->view("setup/visitinformation/list"); ?>

                </div>                
            </div>
        </div>
    </div>
</div>

