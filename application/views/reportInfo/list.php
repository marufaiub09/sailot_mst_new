<form action="ReportSetup/createQuery" id="form1" method="post" target="_blank" enctype="multipart/form-data">
    <table id="" class="table table-striped table-bordered "  width="100%" cellspacing="0">
        <thead>
            <tr>
                <th><p style="color: #042054;">Select Tables and Columns</p></th>
        <th><p style="color: #042054">Preview</p></th>
        </tr>
        </thead>

        <tbody>
            <tr>
                <td class="col-sm-6">
                    <div class="form-group">
                        <script type="text/javascript">
                            function test() {
                               
                                var f1 = document.getElementById("tableName");
                                var f2 = document.getElementById("tables");
                                var f3 = document.getElementById("ortableName");
                                var f4 = document.getElementById("columnName");
                                //  var f5 = document.getElementById("hd_id");

                                f2.value = f1.value;
                               f3.value = f2.value;
                            }
                           
                        </script>
                       


                        <div class="col-sm-6">
                                <select class="select2 form-control " name="tableName" id="tableName" data-tags="true" data-placeholder="Select Table"  data-allow-clear="true" onchange="test();">
                                <option value="">Select Table</option>
                                <?php
                                foreach ($result as $row):
                                    ?>
                                    <option value="<?php echo $row ?>"><?php echo $row ?></option>
                                    <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <br/><br/><br/>
                    <div class="form-group">

                        <div class="col-sm-6">
                            <select class="select2 form-control"  multiple="multiple" name="columnName[]" 
                                    id="columnName" data-tags="true" data-placeholder="Select Columns" data-allow-clear="true" >
                                <option value="">Select Columns</option>


                            </select>
                        </div>

                    </div>                                      
                </td>
                <td>
                    <table id="" class="table table-striped table-bordered " width="100%" cellspacing="0">
                        <thead>
                            <tr>

                                <th>Tables Name</th>
                                <th>Columns Name</th>


                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <input type="text" id="tables" style="border: none; width: 100%" readonly>
                                </td>
                                <td id="display">
                                 <!-- <input type="text" id="column" style="border: none" readonly> -->

                                </td>

                            </tr>
                        </tbody>
                    </table>    


                </td>

            </tr>



        </tbody>
    </table>



    <?php
//echo $value;

    $this->load->view('reportInfo/listTable');



//$this->load->view('reportInfo/listTable2');
    ?>
</form>
<?php
$this->load->view('reportInfo/listScript');
?>