<script type="text/javascript">
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',
            // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row),
                            $cols = $row.find('td');

                        return $cols.map(function (j, col) {
                            var $col = $(col),
                                text = $col.text();

                            return text.replace(/"/g, '""'); // escape double quotes

                        }).get().join(tmpColDelim);

                    }).get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim) + '"',
            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // CSV
            exportTableToCSV.apply(this, [$('#datatable'), 'export.csv']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>
<a href="#" class="export">
    <button>CSV/EXCEL</button>
</a>
<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <?php
        error_reporting('0');
        //echo $_POST['tableName'];
        //exit();
        foreach ($_POST['operatortype'] as $dataQuery) {
            $dataQuery;
        }
        if (empty($_POST['columnName'])) {
            $data1['result'] = $this->db->list_fields($_POST['tableName']);
            foreach ($data1['result'] as $value) {
                ?>
                <th><?php echo $value; ?></th>
                <?php
            }
        } else {
            foreach ($_POST['columnName'] as $value) {
                ?>
                <th><?php echo $value; ?></th>
                <?php
            }
        }
        ?>
    </tr>
    </thead>

    <tbody>
    <?php
    //error_reporting('0');
    if (!empty($_POST['operatortype'])) {

        foreach ($_POST['likeOption'] as $value3) {
            $value3;
            //$str = "Name [varchar]";
            $str = explode(".", $value3);
            $value3 = $str[0];
            $value3;
        }

        foreach ($_POST['operatortype'] as $value1) {
            // echo  $value1;
            $value1;
        }
        $value = array();
        foreach ($_POST['options'] as $key => $value2) {
            $value2;
            $value[$key] = $value2;
        }
        foreach ($_POST['join'] as $value9) {
            $value9;
        }
        foreach ($_POST['write'] as $value103) {
            $value103;
        }
    }
    ?>
    <?php
    /* $regEx = '/^(((\d{4})(-)(0[13578]|10|12)(-)(0[1-9]|[12][0-9]|3[01]))|((\d{4})(-)(0[469]|1‌​1)(-)([0][1-9]|[12][0-9]|30))|((\d{4})(-)(02)(-)(0[1-9]|1[0-9]|2[0-8]))|(([02468]‌​[048]00)(-)(02)(-)(29))|(([13579][26]00)(-)(02)(-)(29))|(([0-9][0-9][0][48])(-)(0‌​2)(-)(29))|(([0-9][0-9][2468][048])(-)(02)(-)(29))|(([0-9][0-9][13579][26])(-)(02‌​)(-)(29)))(\s([0-1][0-9]|2[0-4]):([0-5][0-9]):([0-5][0-9]))$/'; */

    switch (!empty($value1)) {
        case ($value103 == 'write'):
        foreach ($_POST['write_query'] as  $value104) {

            $value104;
        }
        if(!empty($_POST['write_query']))
        {
            $data = $this->db->query($value104)->result();
        }

        break;
        case ($value9 == 'join'):
            foreach ($_POST['ortableName'] as $value8) {
                $value8;
            }
            foreach ($_POST['orclName'] as $value7) {

                $value7;
                //$str = "Name [varchar]";
                $str = explode(".", $value7);
                $value7 = $str[0];;
            }
            foreach ($_POST['anothertlName'] as $value78) {
                $value78;
            }
            foreach ($_POST['anothertlName'] as $value78) {
                $value78;
            }
            foreach ($_POST['anotherclName'] as $value79) {

                $value79;
                //$str = "Name [varchar]";
                $str = explode(".", $value79);
                $value79 = $str[0];

            }
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                $this->db->from($_POST['ortableName']);
                $this->db->join($value78, "$value8.$value7 = $value78.$value79");
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                $this->db->from($_POST['ortableName']);
                $this->db->join($value78, "$value8.$value7 = $value78.$value79");
                $data = $this->db->get()->result();
            }
            break;

        case ($value1 == '<'):
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '<', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '<', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }
            break;

        case ($value1 == '<='):
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '<=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '<=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }
            break;

        case ($value1 == '>'):
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '>', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '>', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }

            break;

        case ($value1 == '>='):
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '>=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '>=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }
            break;

        case ($value1 == '!='):
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '!=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '!=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }

            break;

        case ($value1 == '='):
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '=', $value[$key]);
                    //var_dump($this->db->where($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }
            break;

        case ($value1 == 'Like'):
            if (empty($_POST['columnName'])) {

                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];

                    $this->db->like($value3, $value[$key]);
                    //var_dump($this->db->like($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->like($value3, $value[$key]);
                    //var_dump($this->db->like($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }

            break;

        case ($value1 == 'NotLike'):
            if (empty($_POST['columnName'])) {

                $this->db->select('*');
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];

                    $this->db->not_like($value3, $value[$key]);
                    //var_dump($this->db->like($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->not_like($value3, $value[$key]);
                    //var_dump($this->db->like($value3,$value[$key]));
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            }

            break;

        case($value1 == 'select'):
            if (empty($_POST['columnName'])) {
                $data1['result'] = $this->db->list_fields($_POST['tableName']);
                foreach ($data1['result'] as $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->select($value3);
                }
                $this->db->from($_POST['tableName']);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                $this->db->from($_POST['tableName']);
                //var_dump($this->db->from($_POST['tableName']));
                $data = $this->db->get()->result();
            }

            break;


        case ($value1 == 'limit'):
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                $this->db->from($_POST['tableName']);
                $this->db->limit($value2);
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                $this->db->from($_POST['tableName']);
                $this->db->limit($value2);
                $data = $this->db->get()->result();
            }

            break;
        case ($value1 == 'between'):
            $start = $_POST['startDate'];
            $end = $_POST['endDate'];
            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                $this->db->from($_POST['tableName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];

                    $this->db->where($value3 . '>=', date("Y-m-d", strtotime($start)));

                    $this->db->where($value3 . '<=', date("Y-m-d", strtotime($end)));
                }
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                $this->db->from($_POST['tableName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $value3;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value3);
                    $value3 = $str[0];
                    $this->db->where($value3 . '>=', date("Y-m-d", strtotime($start)));

                    $this->db->where($value3 . '<=', date("Y-m-d", strtotime($end)));
                }
                $data = $this->db->get()->result();
            }
            break;

        case ($value1 == 'toDate'):
            $start = $_POST['todayDate'];

            if (empty($_POST['columnName'])) {
                $this->db->select('*');
                $this->db->from($_POST['tableName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    //$this->db->where($value3,date("Y-m-d",strtotime($start)));
                    $this->db->like($value3, date("Y-m-d", strtotime($start)));
                }
                $data = $this->db->get()->result();
            } else {
                $this->db->select($_POST['columnName']);
                $this->db->from($_POST['tableName']);
                foreach ($_POST['likeOption'] as $key => $value3) {
                    $this->db->like($value3, date("Y-m-d", strtotime($start)));
                }
                $data = $this->db->get()->result();
            }

            break;
    }

    /* echo '<pre>';
      print_r($data);
      exit(); */
    ?>



    <?php
    foreach ($data as $row) {
        ?>
        <tr>
            <?php
            if (empty($_POST['columnName'])) {
                $data1['result'] = $this->db->list_fields($_POST['tableName']);
                foreach ($data1['result'] as $value) {
                    ?>
                    <td><?php echo $row->$value; ?></td>
                    <?php
                }
            } else {
                foreach ($_POST['columnName'] as $value) {
                    ?>
                    <td><?php echo $row->$value; ?></td>
                    <?php
                }
            }
            ?>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>

