<div class="row">

    <div class="col-md-12">

        <div class="panel panel-base">
            <div class="panel-heading">
                <h3 class="panel-title">Encryption Library</h3>
                <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
            </div>
            <div class="panel-body">

                <!-- form start -->
                <form id="attributeForm" method="post" class="form-horizontal"
                      data-fv-framework="bootstrap"
                      data-fv-message="This value is not valid"
                      data-fv-icon-valid="glyphicon glyphicon-ok"
                      data-fv-icon-invalid="glyphicon glyphicon-remove"
                      data-fv-icon-validating="glyphicon glyphicon-refresh">


                    <div class="form-group">
                        <label class="col-xs-3 control-label">Tender ID</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="tender_id" placeholder="Tender ID"
                                   data-fv-notempty="true" />
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Tender Title</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="tender_title" placeholder="Tender Title"
                                   data-fv-notempty="true" />
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Tender Type</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="tender_type" placeholder="Tender Type"
                                   data-fv-notempty="true" />
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Tender Priority</label>
                        <div class="col-xs-5">
                            <input type="text" class="form-control" name="tender_priority" placeholder="Tender Priority"
                                   data-fv-notempty="true" />
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Letter Body</label>
                        <div class="col-xs-5">
                            <input class="form-control" name="letter_body" type="text" placeholder="Letter Body"
                                   data-fv-emailaddress="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-3 control-label">Email address</label>
                        <div class="col-xs-5">
                            <input class="form-control" name="email" type="email" placeholder="Email"
                                   data-fv-emailaddress="true" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3">
                            <button type="submit" class="btn btn-primary">Exit</button>
                            <button type="submit" class="btn btn-primary">Decrypt</button>
                            <button type="submit" class="btn btn-primary">Encrypt &amp; Save</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

    </div>

</div>

