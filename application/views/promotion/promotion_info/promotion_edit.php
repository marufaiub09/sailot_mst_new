<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "   href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/promotionInfo/index') : site_url('promotion/promotionInfo/index') ?>" title="Promotion Information list">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Edit Promotion Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Current Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Official Number <span class="text-danger">*</span></label>        
                                    <div class="col-sm-5" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number', 'value' => " $result->OFFICIALNUMBER", 'readonly' => 'readonly')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="<?php echo $result->SailorID; ?>">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Promotion Date</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'promotionDate', "class" => "form-control promotionDate", 'placeholder' => 'promotion date', 'readonly' => 'readonly',  'value' =>  date('d-m-Y', strtotime($result->PROMOTIONDATE)))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Seniority Date</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'seniorityDate', "class" => "form-control seniorityDate", 'placeholder' => 'seniority date', 'readonly' => 'readonly',  'value' =>  date('d-m-Y', strtotime($result->SENIORITYDATE)) )); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' =>  " $result->FULLNAME")); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' =>  " $result->CR_RANK")); ?>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Unit</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' =>  " $result->POSTING_UNIT_NAME")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Promotion Type</label>
                                    <div class="col-sm-5">
                                        <?php //echo form_input(array('name' => 'promotionType', "class" => "form-control promotionType required", 'required' => 'required', 'placeholder' => 'promotion type', 'readonly' => 'readonly', 'value' => set_value('PromotionType'))); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Promotion Information </legend>
                            <div class="col-md-6">  
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 13px;">Rank From</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Branch<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select rank from Branch">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="branchFrom" id="branchFrom" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Branch</option>
                                                <?php
                                                foreach ($branch as $row):
                                                    ?>
                                                    <option value="<?php echo $row->BRANCH_ID ?>" <?php echo ($row->BRANCH_ID == $result->PRE_BRAMCH)? "selected": "" ?>><?php echo $row->BRANCH_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>
                                            </select>                                        
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Rank<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select promotion rank to">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="rank_from" id="rank_from" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Rank</option>
                                                <?php
                                                foreach ($rank as $row):
                                                    ?>
                                                    <option value="<?php echo $row->RANK_ID ?>" <?php echo ($row->RANK_ID == $result->LastRankID)? "selected": "" ?>><?php echo $row->RANK_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>                                        
                                            </select>                                        
                                        </div>
                                    </div>
                                    <hr>
                                </fieldset>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Promotion Date<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select promotion date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'promotionDate', "id"=>"promotionDate", 'value' => date('d-m-Y', strtotime($result->PromoDate)),  "class" => "datePicker form-control required")); ?>                              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"></label>
                                    <div class="col-sm-3">
                                         <input type="checkbox" name="isActing" value="1" <?php echo ($result->IsActing == 1)? "checked" : "" ?>> Is Acting
                                    </div>
                                    <div class="col-sm-4">
                                        <input type="checkbox" name="isNQ" value="1" <?php echo ($result->IsNQ == 1)? "checked" : "" ?>> Is NQ
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 13px;">Promotion To</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Branch<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion Branch">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="branchTo" id="branchTo" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Branch</option>
                                                <?php
                                                foreach ($branch as $row):
                                                    ?>
                                                    <option value="<?php echo $row->BRANCH_ID ?>" <?php echo ($row->BRANCH_ID == $result->CR_BRAMCH)? "selected": "" ?>><?php echo $row->BRANCH_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>
                                            </select>                                       
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Rank<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion rank to">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="rankTo" id="rankTo" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Rank</option>
                                                <?php
                                                foreach ($rank as $row):
                                                    ?>
                                                    <option value="<?php echo $row->RANK_ID ?>" <?php echo ($row->RANK_ID == $result->CurrentRankID)? "selected": "" ?>><?php echo $row->RANK_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>                                      
                                            </select>                                        
                                        </div>
                                    </div>
                                    <hr>
                                </fieldset>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Seniority Date<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Seniority Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'seniorityDate', "id"=>"seniorityDate", 'value' => date('d-m-Y', strtotime($result->PromoEffectDate)),  "class" => "datePicker form-control required")); ?>                              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Seniority-Mon</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please write Seniority in Month">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'seniorityMon', "id"=>"seniorityMon",  "value" => "$result->SeniorityMonth", "class" => "form-control")); ?>
                                    </div>
                                </div>
                            </div>                            
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Promotion Authority Information </legend>
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required", "value" => $result->AuthorityNumber)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityDate', 'value' =>  date("d-m-Y", strtotime($result->AuthorityDate)), "id"=>"authorityDate", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>                                             
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number <span class="text-danger">*</span></label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>" <?php echo ($row->DAO_ID == $result->DAOID)? "selected" : "" ?>><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px;">Ship/Establishment <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Ship/Establishment ">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control required" name="ship_establishment" id="ship_establishment" data-placeholder="Select Ship/Establishment" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship/establishment</option>
                                            <?php
                                            foreach ($shipEst as $row):
                                                ?>
                                                <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>" <?php echo ($row->SHIP_ESTABLISHMENTID == $result->ShipID)? "selected" : "" ?>><?php echo $row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>                                        
                                    </div>
                                </div>
                            </div>
                            <legend class="legend"></legend>
                        </fieldset>
                    </div>  

                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="hidden" name="id" value="<?php echo $result->PromotionID; ?>">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/promotionInfo/promotionInfo_Update':'promotion/promotionInfo/promotionInfo_Update' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/promotionInfo/index':'promotion/promotionInfo/index'?>" value="Update">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(document).on("change", "#branchFrom", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rank_from').html(data);
            }
        });
    });
    $(document).on("change", "#branchTo", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankTo').html(data);
            }
        });
    });
</script>