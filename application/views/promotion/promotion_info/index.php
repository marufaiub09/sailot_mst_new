<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">

<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        var flag = "<?php echo $flag; ?>";
        var tergatUrl;
        if(flag == 1){
            tergatUrl = "Retirement/HistoryTran/promotionInfo/ajaxPromotionInfoList";   /*Retirement sailor url*/ 
        }else{
            tergatUrl = "promotion/promotionInfo/ajaxPromotionInfoList"; /*Active sailor url*/ 
        }
        var dataTable = $('#employee-grid').DataTable({
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>"+tergatUrl, // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }
            },
            /*id attribute add into dataTable*/
            createdRow: function(row, data, dataIndex) {
                var a = $(row).find('td:eq(0)').html();
                $(row).attr("id", 'row_'+a);
            }
        });        
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Promotion Information</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/promotionInfo/create') : site_url('promotion/promotionInfo/create') ?>" title="Add Promotion Informaion">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body contentArea">
                <table id="employee-grid" class="display table-striped " width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Official NO</th>
                            <th>Previous Rank</th>
                            <th>Promoted Rank</th>
                            <th>Promotion Date</th>
                            <th>Seniority Date</th>
                            <th>Action</th>                            
                        </tr>
                    </thead>
                    <tbody>  

                    </tbody>
                </table>             
            </div>
        </div>

    </div>
</div>
<script>
    $(document).on("click", ".transfer", function () {
        if (confirm("Are You Sure?")) {
            var id = $(this).attr("id");
            var sn = $(this).attr("sn");
            $.ajax({
                type: "post",
                url: "<?php echo site_url('regularTransaction/TransferInfo/TransferInfoExecute'); ?>/",
                data: {transferId: id},
                beforeSend: function () {
                    $("#loader_" + sn).html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
                },
                success: function (data) {
                    if (data == "Y") {
                        alert("Transfer Execute Successfully");
                        $("#row_" + sn).hide();
                        
                    } else {
                        alert("Row Delete Field");
                    }
                }
            });
        } else {
            return false;
        }
    });
</script>