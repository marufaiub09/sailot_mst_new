<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Roster Prepare</center></h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>
                    <div class="col-md-8">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2 " style="padding-right: 0px; padding-left: 0px">Branch & Rank<span class="text-danger">*</span></label>
                                <div class="col-sm-4" >
                                    <select class="select2 form-control required" name="branchCurr" id="branchCurr" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Branch</option>
                                        <?php
                                        foreach ($branch as $row):
                                            ?>
                                            <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                                <div class="col-sm-4" >
                                    <select class="select2 form-control required" name="rankCurr" id="rankCurr" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Rank</option>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Promotion rank">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2" style="padding-right: 0px; padding-left: 0px">Condition<span class="text-danger">*</span></label>
                                <div class="col-sm-3" >
                                    <select class="select2 form-control required" name="condition" id="condition" data-placeholder="Select condition" aria-hidden="true" data-allow-clear="true">
                                        <option value="0">IN</option>
                                        <option value="1">NOT IN</option>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select condition type">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-2 " style="padding-right: 0px; padding-left: 0px">Promotion Rank</label>
                                <div class="col-sm-4" >
                                    <select class="select2 form-control required" name="branchPromotion" id="branchPromotion" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Branch</option>
                                        <?php
                                        foreach ($branch as $row):
                                            ?>
                                            <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>

                                </div>
                                <div class="col-sm-4" >
                                    <select class="select2 form-control required" name="rankPromotion" id="rankPromotion"  multiple="multiple" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                        <option value="">Select Rank</option>
                                    </select>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Promotion rank">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-left: -14px ! important;">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <fieldset class="">
                                        <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Promotion Date</legend>
                                        <div class="form-group" style="margin-bottom:1px">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-3">Year </div>
                                            <div class="col-sm-2"></div>
                                            <div class="col-sm-4"> &nbsp;&nbsp;Date </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <?php echo form_input(array('type' =>"number", 'name' => 'priodYY', "id"=>"priodYY", "class" => "form-control required", "value" => date("Y"), "maxlength"=>"4")); ?>
                                            </div>
                                            <div class="col-sm-6">
                                                 <select class="select2 form-control required" name="promotionDate" id="promotionDate" data-placeholder="Select">
                                                    <option value="0">01 Jan</option>
                                                    <option value="1">01 Jun</option>
                                                </select>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Roster Criteria for</legend>
                                    <div class="form-group">
                                        <table style="width: 100%; margin-top: 5%; margin-left: 6%;">
                                            <tr>
                                                <td style="padding-left:5%"><input type="radio" name="rosterCriteria" checked></td>
                                                <td>Regular</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left:5%"><input type="radio" name="rosterCriteria"></td>
                                                <td>Dockyard</td>
                                            </tr>

                                        </table>

                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-4">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Calculation Base</legend>
                                    <div class="form-group">
                                        <table style="width: 100%; margin-top: 5%; margin-left: 6%;">
                                            <tr>
                                                <td style="padding-left:5%"><input type="radio" name="calculationBase" checked></td>
                                                <td>Seniority</td>
                                            </tr>
                                            <tr>
                                                <td style="padding-left:5%"><input type="radio" name="calculationBase"></td>
                                                <td>Point</td>
                                            </tr>

                                        </table>

                                    </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-left: -14px ! important;">
                            <fieldset class="">
                                <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Show</legend>
                                <div class="form-group">
                                    <table style="width: 60%; margin-top: 3px; margin-left: 6%;">
                                        <tr>
                                            <td style="padding-left:0%;"><input type="radio" name="show" value="0" class="show"></td>
                                            <td style="padding-left:0%">Qualified</td>
                                            <td style="padding-left:0%"><input type="radio" name="show" value="1" class="show"></td>
                                            <td style="padding-left:0%">Not Qualified</td>
                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-12" style="margin-left: -14px ! important;">
                            <fieldset class="">
                                <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Not Qualified For</legend>
                                <div class="form-group">
                                    <table style="width: 100%; margin-top: 3px; margin-left: 6%;">
                                        <tr>
                                            <td style="padding-left:0%;"><input type="radio" name="notQualified" class="notQualified"></td>
                                            <td style="padding-left:0%" class="qual">Recom Not Received</td>
                                            <td style="padding-left:0%"><input type="radio" name="notQualified" class="notQualified"></td>
                                            <td style="padding-left:0%" class="qual">Not Continious VG</td>
                                            <td style="padding-left:0%"><input type="radio" name="notQualified" class="notQualified"></td>
                                            <td style="padding-left:0%" class="qual">Not Due By Time</td>

                                        </tr>
                                    </table>
                                </div>
                            </fieldset>
                        </div>
                        <div class="col-md-12">
                            <input type="button" id="process" class="btn btn-primary pull-left btn-sm Process" value="Process">
                            <input type="button" class="btn btn-primary pull-right btn-sm filter" value="Filter">
                        </div>
                        <div class="col-md-12">
                            <hr>
                            <table id="sailorTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Official No</th>
                                        <th>Name</th>
                                        <th>Present Rank</th>
                                        <!-- <th>Seniority/Point</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-3 " style="padding-right: 0px; padding-left: 0px">Report Type</label>
                            <div class="col-sm-9" >
                                <select class="select2 form-control required" name="reportTYpe" id="reportTYpe" data-placeholder="Select Report Type" aria-hidden="true" data-allow-clear="true">
                                    <option value="">Select Report</option>
                                    <?php
                                    foreach ($reporttype as $row):
                                        ?>
                                        <option value="<?php echo $row->ReportSubTypeID ?>"><?php echo $row->ReportSubTypeName ?></option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <table id="ReportTable"  class="table table-striped table-bordered hover" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Report Name</th>
                                    </tr>
                                </thead>
                                <tbody class="area">
                                    <?php foreach ($reportsetup as $key => $value) { ?>
                                        <tr><td class="report" id="<?php echo $value->ReportSetupID; ?>"><?php echo $value->Name; ?></td></tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <!-- <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="promotion/reCommendation/recomm_save" data-redirect-action="promotion/reCommendation//index" value="submit"> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    .dataTables_wrapper {
        clear: both;
        min-height: 302px;
        position: relative;
    }
</style>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(document).on('click', '.show', function(event) {
        var value = $(this).val();
        if(value == 1){
            $(".notQualified").prop('checked', false);
            $(".notQualified").prop('disabled', true);
            $(".qual").css('opacity', '0.4');
        }else{
            $(".notQualified").prop('disabled', false);
            $(".qual").css('opacity', '1');
        }
    });
    $(document).on('click', '.report', function(event) {
        event.preventDefault();
        var id = $(this).attr('id');
       // alert(id);
    });
    $(document).on('change', '#reportTYpe', function(event) {
        event.preventDefault();
        /* Act on the event */
        var reportType = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('promotion/rosterPreparate/reportByReportType'); ?>/",
            data: {reportType: reportType},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('.area').html(data);
            }
        });
    });
    $('#sailorTable').DataTable( {
        "scrollY": '50vh',

        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {"sZeroRecords": "",
                      "sEmptyTable": ""},
        "columnDefs": [
            { width: '30%', targets: 1 }
        ]
    });
    $('#ReportTable').DataTable( {
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "scrollY": '50vh',
        "scrollCollapse": true,
        "paging":         false
    });

    $(document).on("change", "#branchCurr", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankCurr').html(data);
                $('#rankCurr').select2('val', '');
            }
        });
    });
    $(document).on("change", "#branchPromotion", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankPromotion').html(data);
                $('#rankPromotion').select2('val', '');
            }
        });
    });

    $(document).on("change", "#branchRecomm", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankRecomm').html(data);
                $('#rankRecomm').select2('val', '');
            }
        });
    });
    $(document).on("click","#process",function(){
        var branch = $("#branchCurr").val();

        var rank = $("#rankCurr").val();

        $.ajax({
            type: "post",
            url: "<?php echo site_url(); ?>promotion/RosterPreparate/prepareQuery",
            data: {branch: branch, rank: rank},
            dataType: "json",
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                if (data != ''){
                        console.log(data.process);
                        var process = data.process;

                                var counter = 1;
                                $.each(process, function (obj, value) {
                                    var officialNumber = value['O_NO'];
                                    offNo = "<input type='text' name='o_no[]' class='form-control'  value='" + officialNumber + "' >";
                                    var name = value['FULLNAME'];
                                    name = "<input type='text' name='FULLNAME[]' class='form-control'  value='" + name + "' >";
                                    var rank = value['PRESENT_RANK'];
                                    rank = "<input type='text' name='PRESENT_RANK[]' class='form-control'  value='" + rank + "' >";
                                    $("#sailorTable tbody").append("<tr><td id='of_No_" + counter + "'>" + offNo + "</td><td id='name_" + counter + "'>" + name + "</td><td id='rank_" + counter + "'>" + rank + "</td></tr>");

                                });
                                counter ++;
                    }
                    else{
                        $("#sailorTable tbody").append("No Sailor Found");
                    }
            }
        });
    });

</script>
