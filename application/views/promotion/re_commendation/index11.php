<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('regularTransaction/reEngagementInfo/index'); ?>" title="Re-Engagement Information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Add Recommendation Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Recommendation Period</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px; padding-left: 0px">Recommendation Year</label>        
                                    <div class="col-sm-3" >
                                        <?php echo form_input(array('name' => 'RecommendationYear', 'type' => 'number', 'id' => 'RecommendationYear', 'value' => date('Y'), "class" => "form-control required", 'required' => 'required')); ?>        
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Recommendation Year">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 "  style="padding-right: 0px; padding-left: 0px">Recommendation Date</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'RecommendationDate', 'id' => 'RecommendationDate', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Recommendation Date')); ?>                                                              
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Recommendation Date">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Sailor Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Official Number <span class="text-danger">*</span></label>        
                                    <div class="col-sm-4" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'preEngagementNo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Unit</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'postingdate', "class" => "form-control postingdate required", 'required' => 'required', 'placeholder' => 'posting date', 'value' => set_value('PostingDate'), 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Recommended Information </legend>
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 ">Current Rank <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select current rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control required" name="currRank" id="currRank" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Rank</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>                                        
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px; padding-left: 10px;">Recommended Rank  <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Recommended Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control required" name="recommendRank" id="recommendRank" data-placeholder="Select Rank" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select rank</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>                                        
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Is NQ <span class="text-danger">*</span></label>                                
                                    <div class="col-sm-1">
                                        <?php echo form_checkbox(array('name' => 'sailorStatus', "id"=>"sailorStatus", "class" => " form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>                       
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Type <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Release type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <select class="select2 form-control required" name="type" id="type" data-placeholder="Select Type" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Type</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px;">Ship/Establishment <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Ship/Establishment ">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control required" name="ship_establishment" id="ship_establishment" data-placeholder="Select Ship/Establishment" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship/establishment</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>                                        
                                    </div>
                                </div>
                            </div>                            
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/reEngagementInfo/save" data-redirect-action="regularTransaction/reEngagementInfo/index" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
