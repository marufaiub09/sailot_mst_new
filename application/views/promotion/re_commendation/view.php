<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Current Rank  Name</th>
        <td><?php echo $viewdetails->CR_RANK ?></td>
    </tr>
    <tr>
        <th>Posting Unit</th>
        <td><?php echo $viewdetails->POSTING_UNIT_NAME ?></td>
    </tr>
    <tr>
        <th>Posting Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->POSTINGDATE)) ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment </th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Recommendation Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->RecomDate)) ?></td>
    </tr>
    <tr>
        <th>Recommendation Rank</th>
        <td><?php echo $viewdetails->PRE_RANK ?></td>
    </tr>
</table>