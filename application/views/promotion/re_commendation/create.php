<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('promotion/reCommendation/index'); ?>" title="Recommendation Information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Add Recommendation Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Recommendation Period</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px; padding-left: 0px">Recommended Year <span class="text-danger">*</span></label>        
                                    <div class="col-sm-3" >
                                        <?php echo form_input(array('name' => 'RecommendationYear', 'type' => 'number', 'id' => 'RecommendationYear', 'value' => date('Y'), "class" => "form-control required", 'required' => 'required')); ?>        
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Recommendation Year">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 "  style="padding-right: 0px; padding-left: 0px">Recommended Date <span class="text-danger">*</span></label>        
                                    <div class="col-sm-6" >
                                        <select class="select2 form-control required" name="RecommendationDate" id="RecommendationDate" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select On</option>
                                            <option value="0">15 June</option>
                                            <option value="1">15 December</option>
                                            <option value="2">25 July</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Recommendation Date">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Sailor Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Official Number <span class="text-danger">*</span></label>        
                                    <div class="col-sm-4" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Unit</label>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Date</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'postingdate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'posting date', 'value' => set_value('PostingDate'), 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Recommended Information </legend>
                            <div class="col-md-6"> 
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 13px;">Current Rank </legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Branch<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion Branch">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="branchCurr" id="branchCurr" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Branch</option>
                                                <?php
                                                foreach ($branch as $row):
                                                    ?>
                                                    <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>
                                            </select>                                       
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Rank<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion rank to">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="rankCurr" id="rankCurr" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Rank</option>                                            
                                            </select>                       
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 13px;">Recommended Rank</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Branch<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion Branch">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="branchrecom" id="branchRecomm" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Branch</option>
                                                <?php
                                                foreach ($branch as $row):
                                                    ?>
                                                    <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>
                                            </select>                                      
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Rank<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion rank to">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="rankRecomm" id="rankRecomm" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Rank</option>                                            
                                            </select>                                        
                                        </div>
                                    </div>
                                </fieldset>                                                       
                            </div> 
                            <div class="col-md-6">
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 13px;">Type, Ship Establishment </legend>                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Type <span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Release type">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-4">
                                            <select class="select2 form-control required" name="type" id="type" data-placeholder="Select Type" aria-hidden="true" data-allow-clear="true">
                                                <option value="1">Blue</option>
                                                <option value="2">Red</option>
                                            </select>                                        
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 " style="padding-right: 0px;">Ship/Establishment <span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Ship/Establishment ">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-7">
                                            <select class="select2 form-control required" name="ship_establishment" id="ship_establishment" data-placeholder="Select Ship/Establishment" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select ship/establishment</option>
                                                 <?php
                                                foreach ($shipEst as $row):
                                                    ?>
                                                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"><?php echo $row->NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>
                                            </select>                                        
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Is NQ </label>                                
                                        <div class="col-sm-1">
                                            <?php echo form_checkbox(array('name' => 'isNQ', "id"=>"sailorSisNQtatus", "value" => "1", "class" => " form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>                            
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="promotion/reCommendation/recomm_save" data-redirect-action="promotion/reCommendation//index" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(document).on("change", "#branchCurr", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankCurr').html(data);
                $('#rankCurr').select2('val', '');                
            }
        });
    });
    $(document).on("change", "#branchRecomm", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankRecomm').html(data);
                $('#rankRecomm').select2('val', '');
            }
        });
    });
</script>