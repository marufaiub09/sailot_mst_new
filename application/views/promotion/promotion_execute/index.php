<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('regularTransaction/reEngagementInfo/index'); ?>" title="Re-Engagement Information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Add Re-Entry Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Sailor Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'preEngagementNo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 ">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Present Information </legend>
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 ">Posting Unit <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Posting unit">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'postingUnit', "id"=>"postingUnit", "class" => "form-control required")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Posting Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Posting date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'prePostingDate', "id"=>"prePostingDate", 'value' => date('d-m-Y'),  "class" => "datePicker form-control required")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Sailor Status <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter sailor status">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'sailorStatus', "id"=>"sailorStatus", "class" => " form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>                       
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Release Type <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Release type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'releaseType', "id"=>"lprToken", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Release Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Release Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'releaseDate', "id"=>"releaseDate", 'value' => date('d-m-Y'), "class" => "datePicker form-control required")); ?>
                                    </div>
                                </div>
                            </div>                            
                        </fieldset>
                    </div>   
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Re-Entry Information</legend>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Re-Entry Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Re-entry Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'reEntryDate', 'value' =>  date("d-m-Y"), "id"=>"reEntryDate",  "class" => " datePicker form-control required")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">New Posting Unit <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter New posting unit">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'newPostingUnit',  "id"=>"newPostingUnit", "class" => "form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>                       
                                <div class="form-group">
                                    <label class="col-sm-4 ">Posting date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter posting date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'postingDate', 'value' =>  date("d-m-Y"), "id"=>"postingDate", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>                      
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority No <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Authority No">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNo', "id"=>"authorityNo",  "class" => " form-control required", "placeholder" => "authority no")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityDate', 'value' =>  date("d-m-Y"), "id"=>"authorityDate", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 12px;  margin-bottom: 10px;"> <input type="checkbox" id="changeId">  Official No Change</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">New Official No </label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter New Official No">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'newOfficialNo', "id"=>"newOfficialNo", "class" => "form-control required", 'readonly' => 'readonly')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">new Rank </label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter new Rank">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'availedDays', "id"=>"availedDays", "class" => "form-control required", 'readonly' => 'readonly')); ?>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Remarks </label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter re-entry information remarks">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_textarea(array('name' => 'remarks', "class" => "form-control", 'placeholder' => 'remarks ', 'cols' => '5', 'rows' => '2')); ?>
                                    </div>
                                </div>
                            </div>                    
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/reEngagementInfo/save" data-redirect-action="regularTransaction/reEngagementInfo/index" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    function StudentImg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#stdPhoto')
                    .attr('src', e.target.result)
                    .width(120);
            };

            reader.readAsDataURL(input.files[0]);
            /*upload photo specifit directory*/
            var input = document.getElementById("STD_PHOTO");
            file = input.files[0];
            if (file != undefined) {
                formData = new FormData();
                if (!!file.type.match(/image.*/)) {
                    formData.append("image", file);
                    $.ajax({
                        url: "<?php echo site_url(); ?>/portal/studentImagUpload",
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $("#STD_PHOTO").text(data).val();
                        }
                    });
                } else {
                    alert('Not a valid image!');
                }
            } else {
                alert('Input something!');
            }
        }
    }
</script>