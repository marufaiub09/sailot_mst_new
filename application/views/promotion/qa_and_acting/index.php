<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "
                           href="<?php echo site_url('regularTransaction/reEngagementInfo/index'); ?>"
                           title="Re-Engagement Information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title">
                            <center>NQ and Acting Remove</center>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>

                    <div class="col-md-12">
                        <fieldset class="">
                            <legend class="legend">Current Information</legend>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Acting Type <span class="text-danger">*</span></label>

                                    <div class="col-sm-8">
                                        <div class="col-sm-6" style="padding-left: 0px;">
                                            <?php echo form_radio(array('name' => 'RemoveType', 'id' => 'act', 'class' => 'acty', 'value' => '1', 'checked' => 'checked')); ?>
                                            Acting Remove
                                        </div>

                                        <div class="col-sm-6">
                                            <?php echo form_radio(array('name' => 'RemoveType', 'id' => 'nqd', 'class' => 'acty1', 'value' => '2')); ?>
                                            NQ Remove
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-4 ">Official Number <span class="text-danger">*</span></label>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumberact', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>
                                        <input type="hidden" name="SailorID" id="SAILOR_ID" class="sailorId" value="">
                                        <input type="hidden" name="ISACTING" id="ISACTING" class="ISACTING" value="">
                                        <input type="hidden" name="ISNOTQUALIFIED" id="ISNOTQUALIFIED"
                                               class="ISNOTQUALIFIED" value="">
                                        <input type="hidden" name="RankID" id="rankId" class="rankId" value="">
                                        <input type="hidden" name="RankID" id="rankId" class="rankId" value="">
                                        <!-- <input type="hidden" name="DAOID" id="DAOID" class="DAOID" value="">  
                                        <input type="hidden" name="DAONumber" id="DAONUMBER" class="DAONUMBER" value="">
                                        <input type="hidden" name="ShipEstablishmentID" id="SHIPESTABLISHMENTID" class="SHIPESTABLISHMENTID" value="">            -->
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                           data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                            <span class="smloadingImg"></span><span class="alertSMS label label-danger"
                                                                                    style="font-size: 89%;"></span>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Promotion Date</label>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'promotionDate', "class" => "form-control promotionDate required", 'required' => 'required', 'placeholder' => 'promotion date', 'value' => set_value('PromotionDate'), 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Seniority Date</label>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'seniorityDate', "class" => "form-control seniorityDate required", 'required' => 'required', 'placeholder' => 'seniority date', 'value' => set_value('SeniorityDate'), 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <!-- <label class="col-sm-1"><input type="checkbox" name="editNQ" id="editNQ"></label>                                    
                                    <label class="col-sm-7" style="padding-left: 0px;">Edit NQ/Acting Information</label> -->
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Details</label>

                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Unit</label>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Authority Ship</label>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'SHIP_ESTALISHMENT', "class" => "form-control SHIP_ESTALISHMENT required", 'required' => 'required', 'placeholder' => 'Authority Ship ', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                                    </div>
                                </div>

                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend class="legend">New Information</legend>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="col-sm-2 " style="padding-right: 0px;">Authority Ship <span
                                            class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="left" data-content="Please select Ship/Establishment ">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                    <div class="col-sm-3">

                                        <select class="select2 form-control required" name="ShipEstablishmentID"
                                                id="SHIP_ESTALISHMENT" data-placeholder="Select Authirity Ship"
                                                aria-hidden="true" data-allow-clear="true">
                                            <option value="0">Select Authority Ship</option>
                                            <?php
                                            foreach ($shipEst as $row):
                                                ?>
                                                <option value="<?php echo $row->SHIP_ESTABLISHMENTID; ?>"><?php echo $row->NAME; ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number <span
                                            class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'AuthorityNumber', "id" => "authorityNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please enter Authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'AuthorityDate', 'value' => date("d-m-Y"), "id" => "authorityDate", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number <span class="text-danger">*</span></label>

                                    <div class="col-sm-5">
                                        <select class="select2 form-control required" name="DAOID" id="DAO_NO"
                                                data-placeholder="Select DAO NO" aria-hidden="true"
                                                data-allow-clear="true">

                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option
                                                    value="<?php echo $row->DAO_ID . '.' . $row->DAO_NO; ?>"><?php echo $row->DAO_NO ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>

                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="left" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">NQ Remove Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please enter Authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'NQRemoveDate', 'value' => date("d-m-Y"), "id" => "authorityDate", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 ">&nbsp;</label>

                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect"
                                       data-action="promotion/NqAndActing/save" value="Save"><!-- data-redirect-action="promotion/NqAndActing/index" -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $('.acty1').on('click', function () {
        if ($("#nqd").prop("checked", true)) {
            $(".formSubmitWithRedirect").prop('disabled', false);
            var officeNumber = $("#officialNumberact").val();
            if (officeNumber != '') {
                $.ajax({
                    type: "post",
                    data: {officeNumber: officeNumber},
                    dataType: "json",
                    url: "<?php echo site_url(); ?>setup/common/searchSailorInfoByOfficalNo",
                    beforeSend: function () {
                        $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".smloadingImg").html("");
                        if (data != null) {

                            $(".ISNOTQUALIFIED").val(data['ISNOTQUALIFIED']);
                            var nq = $(".ISNOTQUALIFIED").val();
                            if (nq == 1) {
                                $(".alertSMS").html('');
                            }
                            else {
                                $(".alertSMS").html('Selected Sailor Has No Not Qualified Rules');
                                $(".formSubmitWithRedirect").prop('disabled', true);

                            }

                        } else {

                            $(".ISNOTQUALIFIED").val('');
                            $(".alertSMS").html('Invalid Officer Number');
                        }
                    }
                });
            } else {

                $(".ISNOTQUALIFIED").val('');
                $(".alertSMS").html('');
            }

        }


    });
    $('.acty').on('click', function () {
        if ($("#act").prop("checked", true)) {
            var officeNumber = $("#officialNumberact").val();
            if (officeNumber != '') {
                $.ajax({
                    type: "post",
                    data: {officeNumber: officeNumber},
                    dataType: "json",
                    url: "<?php echo site_url(); ?>setup/common/searchSailorInfoByOfficalNo",
                    beforeSend: function () {
                        $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".smloadingImg").html("");
                        if (data != null) {

                            $(".ISACTING").val(data['ISACTING']);
                            var nq = $(".ISACTING").val();
                            if (nq == 1) {
                                $(".alertSMS").html('');
                            }
                            else {
                                $(".alertSMS").html('Selected Sailor Has No Acting Rules');
                                $(".formSubmitWithRedirect").prop('disabled', true);

                            }

                        } else {

                            $(".ISACTING").val('');
                            $(".alertSMS").html('Invalid Officer Number');
                        }
                    }
                });
            } else {

                $(".ISACTING").val('');
                $(".alertSMS").html('');
            }

        }


    });
</script>
