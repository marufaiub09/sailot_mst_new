<div class="row">
    <!-- <div class="col-md-12">
        <ol class="breadcrumb" style=" background-color: #f5f5f5 !important;  ">
            <li>
                <i class="glyphicon glyphicon-th-large"></i> Promotion
            </li>
            <li class="active">
                Promotion Roster
            </li>
        </ol>
    </div> -->
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title">
                            <center>Promotion Roster</center>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>

                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-6">
                                <legend class="legend">Calculation Base</legend>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="radio" name="calculationBase" class="rad" checked id="month"
                                               value="1"> Month
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="radio" name="calculationBase" class="rad" id="point" value="2">
                                        Point
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <legend class="legend">Roster Criteria For</legend>
                                <div class="form-group">
                                    <div class="col-sm-3">
                                        <input type="radio" name="rosterCriteria" class="roasterCr" value="1" id="regular" checked> Regular
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="radio" class="roasterCr" name="rosterCriteria"  value="2" id="dockyard"> Dockyard
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Branch <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select current rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                    <div class="col-sm-6">
                                        <select class="select2 form-control" name="branch" id="branchFrom1"
                                                data-placeholder="Select Branch" aria-hidden="true"
                                                data-allow-clear="true">
                                            <option value="">Select Branch</option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Rank<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select current rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                    <div class="col-sm-6">
                                        <select class="select2 form-control rankID" name="currRank"
                                                id="rank_from1" data-placeholder="Select rank" aria-hidden="true"
                                                data-allow-clear="true">
                                            <option value="">Select Rank</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend class="legend">Promotion Roster Process</legend>
                            <div class="panel panel-default tabs">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="active "><a href="form-layouts-tabbed.html#tab-first" role="tab"
                                                           data-toggle="tab">Service</a></li>
                                    <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">All
                                            Round Qualifications</a></li>
                                    <li><a href="form-layouts-tabbed.html#tab-third" role="tab" data-toggle="tab">Recommendation</a>
                                    </li>
                                    <li><a href="form-layouts-tabbed.html#tab-four" role="tab" data-toggle="tab">Medal &
                                            Honor</a></li>
                                    <li><a href="form-layouts-tabbed.html#tab-five" role="tab"
                                           data-toggle="tab">Others</a></li>
                                </ul>
                                <div class="panel-body tab-content">
                                    <div class="tab-pane active" id="tab-first">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="padding-left: 0px;">Sea
                                                        service (month)</label>

                                                    <div class="col-md-3">
                                                        <?php echo form_input(array('name' => 'seaService', "id" => "seaService", "class" => "form-control")); ?>
                                                        <input type="hidden" id="prSeaservice" name="prSeaservice">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4"><input type="radio" name="RCSS" id="combinedSeaService">
                                                    </label>
                                                    <div class="col-sm-8" style="text-align: left;margin-left: -140px;">
                                                        Required Combined Sea Service
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-5">
                                                        <select class="select2 form-control  reSeaRan"
                                                                name="CombainSeeService" id="reSeaRank"
                                                                data-placeholder=" Rank" aria-hidden="true"
                                                                data-allow-clear="true" data-width="150px">

                                                        </select>
                                                    </div>
                                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                                       data-placement="right" data-content="Please select current rank">
                                                        <i class="fa fa-question-circle"></i>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4">Starting Rank</label>

                                                    <div class="col-sm-5">
                                                        <select class="select2 form-control"
                                                                name="StartingService" id="rankSeaService"
                                                                data-placeholder="Select Rank" aria-hidden="true"
                                                                data-allow-clear="true">

                                                        </select>
                                                    </div>
                                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                                       data-placement="right" data-content="Please select current rank">
                                                        <i class="fa fa-question-circle"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <fieldset class="">
                                                <legend class="legend" style="font-size: 12px; margin-bottom: 10px;">
                                                    'SUPER' Efficiency Point
                                                </legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Gain Value <span
                                                                class="text-danger">*</span></label>
                                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                                           data-placement="right"
                                                           data-content="Please enter authority number">
                                                            <i class="fa fa-question-circle"></i>
                                                        </a>

                                                        <div class="col-sm-5">
                                                            <?php echo form_input(array('name' => 'gainValue', "id" => "gainValue", "class" => "form-control")); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Maximum Limit <span
                                                                class="text-danger">*</span></label>
                                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                                           data-placement="right"
                                                           data-content="Please enter authority number">
                                                            <i class="fa fa-question-circle"></i>
                                                        </a>

                                                        <div class="col-sm-5">
                                                            <?php echo form_input(array('name' => 'maximumLimit', "id" => "maximumLimit", "class" => "form-control")); ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>
                                        <div class="col-md-12">
                                            <fieldset class="">
                                                <legend class="legend" style="font-size: 12px; margin-bottom: 10px;">
                                                    Each Year point
                                                </legend>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Sea Service <span
                                                                class="text-danger">*</span></label>
                                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                                           data-placement="right"
                                                           data-content="Please enter authority number">
                                                            <i class="fa fa-question-circle"></i>
                                                        </a>

                                                        <div class="col-sm-5">
                                                            <?php echo form_input(array('name' => 'RankServicePoint', "id" => "SeaSer", "class" => "form-control")); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label"
                                                               style="padding-left: 0px;">Instructor Service <span
                                                                class="text-danger">*</span></label>
                                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                                           data-placement="right"
                                                           data-content="Please enter authority number">
                                                            <i class="fa fa-question-circle"></i>
                                                        </a>

                                                        <div class="col-sm-5">
                                                            <?php echo form_input(array('name' => 'InstructorService', "id" => "InsService", "class" => "form-control")); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Rank Service <span
                                                                class="text-danger">*</span></label>
                                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                                           data-placement="right"
                                                           data-content="Please enter authority number">
                                                            <i class="fa fa-question-circle"></i>
                                                        </a>

                                                        <div class="col-sm-5">
                                                            <?php echo form_input(array('name' => 'RankService', "id" => "RankService", "class" => "form-control")); ?>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-4 control-label">Service length <span
                                                                class="text-danger">*</span></label>
                                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                                           data-placement="right"
                                                           data-content="Please enter authority number">
                                                            <i class="fa fa-question-circle"></i>
                                                        </a>

                                                        <div class="col-sm-5">
                                                            <?php echo form_input(array('name' => 'ServiceLength', "id" => "serviceLength", "class" => "form-control")); ?>
                                                        </div>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-second">
                                        <div class="col-md-12">
                                            <label class="col-sm-4" style="font-size: small;font-weight: bold;">Training/Course
                                                List</label>
                                            <table id="sailorTable"
                                                   class="table table-striped table-bordered trainTable"
                                                   cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Code/Name</th>
                                                        <th>Gain</th>
                                                        <th>Is Mandatory</th>
                                                        <th>Ouput Column</th>

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>

                                            <div class="xh" style="text-align: right;">
                                                <span class="btn btn-xs btn-success" id="add_record">
                                                    <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                                </span>
                                            </div>
                                        </div>


                                        <div class="col-md-12">
                                            <label class="col-sm-4" style="font-size: small;font-weight: bold;">Exam/Test
                                                List</label>
                                            <table id="sailorTable1" class="table table-striped table-bordered"
                                                   cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Code/Name</th>
                                                        <th>Gain</th>
                                                        <th>Is Mandatory</th>
                                                        <th>Ouput Column</th>

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>

                                            <div class="xh" style="text-align: right;">
                                                <span class="btn btn-xs btn-success" id="add_record1">
                                                    <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                                </span>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="tab-pane" id="tab-third">
                                        <div class="col-md-12">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <div class="col-sm-4">
                                                        <input type="checkbox" name="MRfPromotion" id="manRecomn"
                                                               class="form-control tempCheck" value="0">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <label style="margin-left:-105px;margin-top:10px"> Mandatory
                                                        Recommendation for Promotion</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">

                                            <div class="col-md-5">
                                                <legend style="font-size: small;font-weight: bold;">Recommendation
                                                    Form
                                                </legend>
                                                <div class="form-group">
                                                    <label class="col-sm-4">Recom. Form</label>

                                                    <div class="col-sm-6">
                                                        <select class="select2 form-control" name="RecomFrom" id="recomnForm" data-search-allow="true"
                                                                data-width="170px">
                                                            <option value="0">Select Recomn. Form</option>
                                                            <option value="1">D(Seniority Date)</option>
                                                            <option value="2">E(Exam Date)</option>
                                                            <option value="3">T (Training Date)</option>
                                                            <option value="4">A (Rank)</option>

                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4">Branch</label>

                                                    <div class="col-sm-6">
                                                        <select class="select2 form-control" name="branchFrom"
                                                                id="branchRecomn" data-placeholder="Select Branch"
                                                                aria-hidden="true" data-allow-clear="true"
                                                                data-width="170px">
                                                            <option value="">Select Branch</option>
                                                            <?php
                                                            foreach ($branch as $row):
                                                                ?>
                                                                <option
                                                                    value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                                                    <?php
                                                                endforeach;
                                                                ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-4">Rank</label>

                                                    <div class="col-sm-6">
                                                        <select class="select2 form-control" name="rank_from"
                                                                id="rankRecomn" data-placeholder="Select rank"
                                                                aria-hidden="true" data-allow-clear="true"
                                                                data-width="170px" disabled="true">
                                                            <option value="">Select Rank</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <legend style="font-size: small;font-weight: bold;">Seniority Counted
                                                    For Red Recommendation
                                                </legend>

                                                <table id="sailorTable2" class="table table-striped table-bordered"
                                                       cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>Recom.No</th>
                                                            <th>Gain</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                                <div class="xh" style="text-align: right;">
                                                    <span class="btn btn-xs btn-success" id="add_record2">
                                                        <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <legend style="font-size: small;font-weight: bold; margin-left:17px;">No. of
                                                Recommendation Point
                                            </legend>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Gain
                                                        Value</label>

                                                    <div class="col-sm-5">
                                                        <?php echo form_input(array('name' => 'recomnGaintext', "id" => "recomnGaintext", "class" => "form-control")); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-4 control-label" style="padding-left: 0px;">Mazimum
                                                        Limit</label>

                                                    <div class="col-sm-5">
                                                        <?php echo form_input(array('name' => 'recomnMax', "id" => "recomnMax", "class" => "form-control")); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-four">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <label class="col-sm-4" style="font-size: small;font-weight: bold;">Medal
                                                    List</label>
                                                <table id="sailorTable3" class="table table-striped table-bordered"
                                                       cellspacing="0" width="60%">
                                                    <thead>
                                                        <tr>
                                                            <th>Code/Name</th>
                                                            <th>Gain</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>

                                                <div class="xh" style="text-align: right;">
                                                    <span class="btn btn-xs btn-success" id="add_record3">
                                                        <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label class="col-sm-4" style="font-size: small;font-weight: bold;">Honor
                                                    List</label>

                                                <div class="form-group">
                                                    <table id="sailorTable4" class="table table-striped table-bordered"
                                                           cellspacing="0" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th>Code/Name</th>
                                                                <th>Gain</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>

                                                    <div class="xh" style="text-align: right;">
                                                        <span class="btn btn-xs btn-success" id="add_record4">
                                                            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                                        </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-five">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <div class="col-sm-2" style="margin-left:-32px;">
                                                        <input type="checkbox" name="CSPRank" id="seniorityPrRank"
                                                               class="form-control tempCheck" value="0">
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <label style="margin-left:-40px;margin-top:10px"> Calculate
                                                            Seniority Present Rank</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-6" style="padding-left: 0px;">Continuous VG(In
                                                        Month)</label>

                                                    <div class="col-sm-3">
                                                        <?php echo form_input(array('name' => 'continuousVg', "id" => "continuousVg", "class" => "form-control")); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-10" style="padding-left: 0px;">Rank Service
                                                        Point Per Month After Due Time</label>

                                                    <div class="col-sm-2"
                                                         style="padding-left: 0px;  margin-left:-20px;margin-top:-8px">
                                                             <?php echo form_input(array('name' => 'RSPPMADTime', "id" => "RSPPMADTime", "type" => "number", "class" => "form-control")); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-sm-6" style="padding-left: 0px;">Due By Time(In
                                                        Year)</label>

                                                    <div class="col-sm-3">
                                                        <?php echo form_input(array('name' => 'dueByTime', "id" => "dueByTime", "class" => "form-control")); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-10">Report Criteria Heading 1</label>

                                                <div class="col-sm-8">
                                                    <textarea rows="2" cols="80" name="RCHeading1" id="ReportCriteriaHeading1"></textarea>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-10">Report Criteria Heading 2</label>

                                                <div class="col-sm-8">
                                                    <textarea rows="3" cols="80" name="RCHeading2" id="ReportCriteriaHeading2"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">

                                            <label class="col-sm-4" style="font-size: small;font-weight: bold;">Order By
                                                List</label>


                                            <table id="sailorTable5" class="table table-striped table-bordered"
                                                   cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>OrderBy</th>

                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>

                                            <div class="xh" style="text-align: right;">
                                                <span class="btn btn-xs btn-success" id="add_record5">
                                                    <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="col-sm-2 ">&nbsp;</label>

                                                <div class="col-sm-8">
                                                    <input type="button"
                                                           class="btn btn-primary btn-sm formSubmitWithRedirect"
                                                           data-action="promotion/PromotionRoster/save"
                                                           data-redirect-action="promotion/PromotionRoster/index"
                                                           value="submit">                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<?php $this->load->view("common/sailors_info"); ?>
<!-- Start Add More Table Option -->
<script>

    function StudentImg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#stdPhoto')
                        .attr('src', e.target.result)
                        .width(120);
            };

            reader.readAsDataURL(input.files[0]);
            /*upload photo specifit directory*/
            var input = document.getElementById("STD_PHOTO");
            file = input.files[0];
            if (file != undefined) {
                formData = new FormData();
                if (!!file.type.match(/image.*/)) {
                    formData.append("image", file);
                    $.ajax({
                        url: "<?php echo site_url(); ?>/portal/studentImagUpload",
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $("#STD_PHOTO").text(data).val();
                        }
                    });
                } else {
                    alert('Not a valid image!');
                }
            } else {
                alert('Input something!');
            }
        }
    }

    var counter = 1;
    $(document).on('click', '#add_record', function () {
        var id = $('#sailorTable tr:last td:nth-last-child(3) ').attr("id");
        if(typeof id === "undefined")
        {
            var newCount = 1;
        }
        else
        {
            var arr = id.split("_");
            var newCount = parseInt(arr[1]) + 1;
        }
        //counter++;
        $("#sailorTable tbody").append(' <tr>' +
            '<td id="prtrain_' + counter + '" style="display:none">' +
                ' <input type="hidden"  value="" name="prtrain_[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td id="code_' + counter + '">' +
                '<select class="select2 form-control" data-live-search= "true"  name="codetrain[]">' +
                '<option value="0">SELECT CODE</option>' +
<?php foreach ($code as $row) { ?>
            "<option value='<?php echo $row->NAVYTrainingID ?>'><?php echo htmlspecialchars($row->Name) ?></option>" +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td id="gain_' + counter + '">' +
                ' <input type="text"  value="" name="gain[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td id="isMandatory_' + newCount + '">' +
                ' <input id="manTrain_'+newCount+'"type="checkbox" value ="0" name="isMandatory_'+newCount+'" class="tempCheck form-control manTrain" >' +
                '</td>' +
                '<td id="outputColumn' + counter + '">' +
                '<select class="select2 form-control" data-live-search = "true" name="outputColumn[]">' +
                '<option value="1">Course-1(Date)</option>' +
                '<option value="2">Course-2(Date)</option>' +
                '<option value="3">Course-3(Date)</option>' +
                '</select> ' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });


    var counter = 1;
    $(document).on('click', '#add_record1', function () {
        var id = $('#sailorTable1 tr:last td:nth-last-child(3) ').attr("id");
        if(typeof id === "undefined")
        {
            var newCount = 1;
        }
        else
        {
            var arr = id.split("_");
            var newCount = parseInt(arr[1]) + 1;
        }
        //counter++;
        $("#sailorTable1 tbody").append(' <tr>' +
            '<td id="prexam_' + counter + '" style="display:none">' +
                ' <input type="hidden"  value="" name="prexam_[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td id="examcode_' + counter + '">' +
                '<select class="select2 form-control" data-live-search= "true"  name="codeexam[]">' +
                '<option value="0">SELECT CODE</option>' +
<?php foreach ($exam as $row) { ?>
            "<option value='<?php echo $row->EXAM_ID ?>'><?php echo htmlspecialchars($row->NAME) ?></option>" +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td id="examgain_' + counter + '">' +
                ' <input type="text"  value="" name="examgain[]"   class="form-control" placeholder="Gain Value" >' +
                '</td>' +
                '<td id="examisMandatory_' + newCount + '">' +
                ' <input id="manExam_'+newCount+'"type="checkbox" value ="0" name="examisMandatory_'+newCount+'" class="tempCheck form-control manExam" >' +
                '</td>' +
                '<td id="examoutputColumn' + counter + '">' +
                '<select class="select2 form-control" data-live-search = "true" name="examoutputColumn[]">' +
                '<option value="1">Course-1(Date)</option>' +
                '<option value="2">Course-2(Date)</option>' +
                '<option value="3">Course-3(Date)</option>' +
                '</select> ' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr1"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
    });
    $(document).on('click', '#remove_tr1', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });
    var counter = 1;
    $(document).on('click', '#add_record2', function () {
        counter++;
        $("#sailorTable2 tbody").append(' <tr>' +
            '<td id="prrecomn_' + counter + '" style="display:none">' +
                ' <input type="hidden" name="prrecomn_[]"   class="form-control " placeholder="Gain" >' +
                '</td>' +
                '<td  id="recomnRec_' + counter + '">' +
                ' <input type="text"  value="" name="recomnRec[]"  class="form-control" placeholder="Recomn. No" >' +
                '</td>' +
                '<td id="recomnGain_' + counter + '">' +
                ' <input type="text" name="recomnGain[]"   class="form-control " placeholder="Gain" >' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr2"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );

    });
    $(document).on('click', '#remove_tr2', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    var counter = 1;
    $(document).on('click', '#add_record3', function () {
        counter++;
        $("#sailorTable3 tbody").append(' <tr>' +
            '<td id="prmedal_' + counter + '" style="display:none">' +
                ' <input type="hidden" name="prmedal_[]"   class="form-control" placeholder="Gain" >' +
                '</td>' +
                '<td id="medalName_' + counter + '">' +
                '<select class="select2 form-control" data-live-search= "true"  name="MedalID[]">' +
                '<option value="0">SELECT Medal</option>' +
<?php foreach ($medal as $row) { ?>
            "<option value='<?php echo $row->MEDAL_ID ?>'><?php echo htmlspecialchars($row->NAME) ?></option>" +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td id="medalGain_' + counter + '">' +
                ' <input type="text" name="medalGain[]"   class="form-control " placeholder="Gain" >' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr3"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
    });
    $(document).on('click', '#remove_tr3', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });
    var counter = 1;
    $(document).on('click', '#add_record4', function () {
        counter++;
        $("#sailorTable4 tbody").append(' <tr>' +
            '<td id="prhonor_' + counter + '" style="display:none">' +
                ' <input type="hidden" name="prhonor_[]"   class="form-control" placeholder="Gain" >' +
                '</td>' +
                '<td id="honorName_' + counter + '">' +
                '<select class="select2 form-control" data-live-search= "true"  name="HonorID[]">' +
                '<option value="0">SELECT Honor</option>' +
<?php foreach ($honor as $row) { ?>
            "<option value='<?php echo $row->HONOR_ID ?>'><?php echo htmlspecialchars($row->NAME) ?></option>" +
<?php } ?>
        '</select> ' +
                '</td>' +
                '<td id="honorGain_' + counter + '">' +
                ' <input type="text" name="honorGain[]"   class="form-control " placeholder="Gain" >' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr4"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
    });
    $(document).on('click', '#remove_tr4', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });
    var counter = 1;
    $(document).on('click', '#add_record5', function () {
        var id = $('#sailorTable5 tr:last td:nth-last-child(3) ').attr("id");
        if(typeof id === "undefined")
        {
            var newCount = 1;
        }
        else
        {
            var arr = id.split("_");
            var newCount = parseInt(arr[1]) + 1;
        }
       // counter++;
        $("#sailorTable5 tbody").append(' <tr>' +
            '<td id="prorder_' + counter + '" style="display:none">' +
                ' <input type="hidden" name="prorder_[]"   class="form-control" placeholder="Gain" >' +
                '</td>' +
                '<td id="pos_' + newCount + '">' +
                '<select class="select2 form-control" data-live-search= "true"  name="orderby[]">' +
                '<option value="0">SELECT OrderBy</option>' +
                "<option value='RedRecomSeniorityDate'>RedRecomSeniorityDate</option>" +
                "<option value='SeniorityDate'>SeniorityDate</option>" +
                "<option value='OfficialNumber'>OfficialNumber</option>" +
                "<option value='TotalPoint'>TotalPoint</option>" +
                "<option value='CourseDate1'>CourseDate1</option>" +
                "<option value='ExamDate1'>ExamDate1</option>" +
                "<option value='PromotionDate'>PromotionDate</option>" +
                "<option value='Art4JoinDate'>Art4JoinDate</option>" +
                "<option value='CourseDate3'>CourseDate3</option>" +
                "<option value='ExamDate2'>ExamDate2</option>" +
                '</select> ' +
                '</td>' +
                '<td id="prorder_' + newCount + '" style="display:none">' +
                ' <input type="hidden" name="position[]" value="'+ newCount +'"  class="form-control" placeholder="Gain" >' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr5"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );



    });
    $(document).on('click', '#remove_tr5', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });
</script>
<!-- End Add More Table Option -->

<script>
    /*Start change Branch Type(Month/Point) on change radio Button and Branch Wise Other Rank */
    $(document).ready(function () {
        var rad = $('input[class=rad]:checked').val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('promotion/PromotionRoster/branchName'); ?>/",
            data: {rad: rad},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#branchFrom1').html(data);
            }
        });

    });
    $(document).on("change", ".rad", function () {
        var rad = $('input[class=rad]:checked').val();
        /*if (rad == 1)
         {
         $("#regular").prop('checked', true);
         $("#dockyard").prop('checked', false);
         }
         if(rad == 2)
         {
         $("#dockyard").prop('checked', true);
         $("#regular").prop('checked', false);
         }*/
        $.ajax({
            type: "post",
            url: "<?php echo site_url('promotion/PromotionRoster/branchNameByIn'); ?>/",
            data: {rad: rad},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#branchFrom1').html(data);
            }
        });
    });

    $(document).on("change", "#branchRecomn", function () {
        $("#rankRecomn").prop('disabled', false);
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankRecomn').html(data);
            }
        });
    });
    $(document).on("change", "#branchFrom1", function () {
        /*$('#rank_from1').select2('val', '');
         $('#reSeaRank').select2('val', '');
         $('#rankSeaService').select2('val', '');*/
        $("#rank_from1").prop('disabled', false);
        $("#reSeaRank").prop('disabled', false);
        $("#rankSeaService").prop('disabled', false);
        var id = $(this).val();

        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rank_from1').html(data);
                $('#reSeaRank').html(data);
                $('#rankSeaService').html(data);

            }

        });
    });
    $(document).on("change", "#branchTo", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankTo').html(data);
            }
        });
    });
    /*End change Branch Type(Month/Point) on change radio Button and Branch Wise Other Rank */

    /*Rank & roaster Criteria wise Search For Promotion Roster*/
    $(".roasterCr").on('change', function () {
        $("#sailorTable tbody").empty();
        $("#sailorTable1 tbody").empty();
        $("#sailorTable2 tbody").empty();
        $("#sailorTable3 tbody").empty();
        $("#sailorTable4 tbody").empty();
        $("#sailorTable5 tbody").empty();
        var cr = $(this).val();
        var rank = $("#rank_from1").val();
        var br = $("#branchFrom1").val();
        var rad = $('input[class=rad]:checked').val();
        if (cr == 1)
        {
            /*START FOR REQULAR*/
            if (rank !== '')
            {
                $.ajax({
                    type: "post",
                    data: {rank: rank, rad: rad, cr: cr},
                    dataType: "json",
                    url: "<?php echo site_url(); ?>promotion/PromotionRoster/promotion1",
                    beforeSend: function () {
                        $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        if (data != '')
                        {
                            /*console.log(data.service);
                            console.log(data.training);
                             console.log(data.exam);
                            console.log(data.honor);
                            console.log(data.medal);
                            console.log(data.recomnSelect);
                            console.log(data.recomnBranch);
                            console.log(data.OrderBy);
                            console.log(data.trainCode);*/

                            if (data.service != null) {
                                $('#prSeaservice').val(data.service['RoasterID']);
                                $('#seaService').val(data.service['SeaService']);
                                if (data.service['IsRCSService'] != 1) {
                                    $('#combinedSeaService').prop('checked', false);
                                }
                                else {
                                    $('#combinedSeaService').prop('checked', true);
                                }
                                var abc = data.service['CSSRankID'];
                                $("#reSeaRank").val(abc).trigger("change");
                                var startRank = data.service['RankServiceID'];
                                $("#rankSeaService").val(startRank).trigger('change');
                                $("#gainValue").val(data.service['Given_Value']);
                                $("#maximumLimit").val(data.service['maximumValue']);
                                $("#SeaSer").val(data.service['See_Service']);
                                $("#InsService").val(data.service['Instructor_Service']);
                                $("#RankService").val(data.service['Rank_Service']);
                                $("#serviceLength").val(data.service['Service_Length']);

                                if (data.service['Calculate_Senority'] != 1) {
                                    $('#seniorityPrRank').prop('checked', false);
                                }
                                else {
                                    $('#seniorityPrRank').prop('checked', true);
                                }
                                $("#continuousVg").val(data.service['Continuas_VG']);
                                $("#RSPPMADTime").val(data.service['RSPPMADTime']);
                                $("#dueByTime").val(data.service['Due_by_Time']);
                                ReportCriteriaHeading1
                                $("#ReportCriteriaHeading1").val(data.service['ReportCriteriaHeading1']);
                                $("#ReportCriteriaHeading2").val(data.service['ReportCriteriaHeading2']);


                            }
                            else {
                                $('#seaService').val('');
                                $('#combinedSeaService').prop('checked', false);
                                $(".rank").val('');
                                $("#reSeaRank").val('');
                                $("#rankSeaService").val('');
                                $("#gainValue").val('');
                                $("#maximumLimit").val('');
                                $("#SeaSer").val('');
                                $("#InsService").val('');
                                $("#RankService").val('');
                                $("#serviceLength").val('');
                            }

                            if (data.training != null) {
                                var train = data.training;
                                var trainCode = data.trainCode;

                                var traincounter = 1;
                                $.each(train, function (obj, value) {

                                    var ab = value['Is_Mandatory'];
                                    var status = (ab >=1 ? 'checked' : '');
                                    var manVal = (ab >=1 ? "1" : "0");
                                    str1 = '<input type="checkbox" id="manTrain_'+traincounter+'" value='+manVal+'  name="isMandatory_'+traincounter+'"  class="tempCheck form-control manTrain" '+status+'>';
                                    var name = value['name'];

                                    code = "<input type='text' id='codes_"+traincounter+"'  name='code[]' class='form-control excode'  value='" + name + "' >";
                                    /*code2 = "<select class='select2' id='code2'><option value='"+name+"'>'"+name+"'</option></select>"*/
                                    var gain = value['Gain'];
                                    gain = "<input type='text' name='gain[]' class='form-control'  value=" + gain + " >";
                                    var column1 = value['Output_Column'];
                                    //column = "<input type='text' name='outputColumn[]' class='form-control'  value=" + column + " >";
                                    var outputColumnID = value['CourseDateColumn'];
                                    //outputColumnID = "<input type='text' name='outputColumnID[]' class='form-control'  value=" + outputColumnID + " >";
                                    column = "<select class='form-control' name='outputColumn[]'><option value='"+outputColumnID +"'>"+column1+"</option><option value='1'>Course-1(Date)</option><option value='2'>Course-2(Date)</option><option value='3'>Course-3(Date)</option></select>"
                                    var prtrain = value['NAVYTrainingID'];
                                    codetrain = "<input type='text' name='codetrain[]' id='trIDS_"+traincounter+"' class='form-control trds' type='hidden'  value=" + prtrain + " >";
                                    prtrain = "<input type='text' name='prtrain[]' class='form-control'  value=" + prtrain + " >";
                                    var valueID = value['ValueID'];
                                    values = "<input type='text'  name='valuetrain[]' class='form-control valueID'  value=" + valueID + " >";
                                        $("#sailorTable tbody").append("<tr><td id='prtrain_" + traincounter + "' style='display:none'>" + values + "</td><td id='prtrain_" + traincounter + "' style='display:none'>" + prtrain + "</td><td id='prtrain_" + traincounter + "' style='display:none'>" + codetrain + "</td><td id='code_" + traincounter + "'>" + code + "</td><td id='code_" + traincounter + "'>" + gain + "</td><td id='code_" + traincounter + "'>" + str1 + "</td><td id='code_" + traincounter + "'>" + column + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr10"><i style="cursor:pointer" class="fa fa-times deleteItem" > Remove</i></span>' + "</td></tr>");
                                    traincounter++;
                                 });
                                /*For CheckBox*/
                                $(document).on('click', '.manTrain', function () {
                                    var status = ($(this).is(':checked')) ? 1 : 0;
                                    var checkID = $(this).attr("id");

                                    $("#"+checkID).val(status);
                                });
                                /*For CheckBox*/
                                /*for POP UP*/

                                function deselect(e) {
                                  $('.pop').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.excode').on('click', function() {

                                    var cd = $(this).attr("id");
                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.pop').slideFadeToggle();
                                      $('#trainCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.pop'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#trainCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#trainCode option:selected").text();
                                    var trID = $("#trainCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#trIDS_"+arr[1]).val(trID);
                                    $(".pop").fadeOut("slow");

                                });

                                /*for POP UP*/

                                $(document).on('click', '#remove_tr10', function () {

                                    if (traincounter > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".valueID").val();

                                        $.ajax({
                                            type: "post",
                                            url: "<?php echo site_url('promotion/PromotionRoster/trainDel'); ?>/",
                                            data: {id: id},
                                            beforeSend: function () {
                                                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                            },
                                            success: function (data) {

                                            }
                                        })

                                        traincounter--;
                                    }
                                   // return false;
                                });


                            }
                            else {
                                $("#sailorTable tbody").empty();
                            }
                            if (data.exam != null) {
                                var examin = data.exam;
                                var examcounter = 1;
                                $.each(examin, function (obj, value) {

                                    var ab = value['IsMandatory'];
                                    var status = (ab >=1 ? 'checked' : '');
                                    var manVal = (ab >=1 ? "1" : "0");
                                    str1 = '<input type="checkbox" id="manExam_'+examcounter+'" value='+manVal+'  name="examisMandatory_'+examcounter+'"  class="tempCheck form-control manExam" '+status+'>';
                                    var outputColumnID = value['CourseDateColumn'];

                                    var name = value['name'];

                                    code = "<input type='text' id='excodes_"+examcounter+"' name='examcode[]' class='form-control codeExam'  value='" + name + "' >";
                                    var gain = value['GainValue'];
                                    gain = "<input type='text' name='examgain[]' class='form-control'  value=" + gain + " >";
                                    var column1 = value['Output_Column'];
                                    column = "<select class='form-control' name='examoutputColumn[]'><option value='"+outputColumnID +"'>"+column1+"</option><option value='1'>Course-1(Date)</option><option value='2'>Course-2(Date)</option><option value='3'>Course-3(Date)</option></select>";
                                    var prexam1 = value['EXAM_ID'];
                                    prexam = "<input type='text' name='prexam[]' class='form-control' type='hidden'  value=" + prexam1 + " >";
                                    codeexam = "<input type='text' name='codeexam[]' id='exIDS_"+examcounter+"' class='form-control exds' type='hidden'  value=" + prexam1 + " >";
                                    var exvalueID = value['ValueID'];
                                    exvalues = "<input type='text'  name='valueexam[]' class='form-control exvalueID'  value=" + exvalueID + " >";
                                    $("#sailorTable1 tbody").append("<tr><td id='prexam_" + examcounter + "' style='display:none'>" + exvalues + "</td><td id='prexam_" + examcounter + "' style='display:none'>" + codeexam + "</td><td id='prexam_" + examcounter + "' style='display:none'>" + prexam + "</td><td id='examcode_" + examcounter + "'>" + code + "</td><td id='examgain_" + examcounter + "'>" + gain + "</td><td id='examisMandatory_" + examcounter + "'>" + str1 + "</td><td id='examoutputColumn_" + examcounter + "'>" + column + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr11"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");
                                    examcounter++;
                                });
                                /*Start For CheckBox*/
                                $(document).on('click', '.manExam', function () {
                                    var status = ($(this).is(':checked')) ? 1 : 0;
                                    var checkID = $(this).attr("id");

                                    $("#"+checkID).val(status);
                                });
                                /*End For CheckBox*/
                                /*Start for POP UP*/

                                function deselect(e) {
                                  $('.popCode').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.codeExam').on('click', function() {

                                    var cd = $(this).attr("id");

                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.popCode').slideFadeToggle();
                                      $('#examCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.popCode'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#examCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#examCode option:selected").text();
                                    var trID = $("#examCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#exIDS_"+arr[1]).val(trID);
                                    $(".popCode").fadeOut("slow");

                                });

                                /*End for POP UP*/
                                $(document).on('click', '#remove_tr11', function () {
                                    if (confirm("Are You Sure?"))
                                    {
                                        if (examcounter > 1) {
                                            $(this).closest('tr').remove();
                                            var id = $(".exvalueID").val();

                                            $.ajax({
                                                type: "post",
                                                url: "<?php echo site_url('promotion/PromotionRoster/examDel'); ?>/",
                                                data: {id: id},
                                                beforeSend: function () {
                                                    $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                },
                                                success: function (data) {

                                                }
                                            })
                                            examcounter--;
                                        }
                                        return false;

                                    }
                                    else
                                    {
                                        return false;
                                    }
                                });


                            }
                            else {
                                $("#sailorTable1 tbody").empty();
                            }
                            if (data.recomnSelect != null || data.recomnBranch != null)
                            {
                                if (data.recomnBranch['mendatory_recommendation_for_promotion'] != 1)
                                {
                                    $('#manRecomn').prop('checked', false);
                                }
                                else
                                {
                                    $('#manRecomn').prop('checked', true);
                                }
                                $("#recomnForm").val(data.recomnBranch['RecomCriteria']).trigger("change");

                                if (data.recomnBranch['RecomCriteria'] != 4)
                                {
                                    $("#branchRecomn").prop('disabled', true);
                                }
                                $("#recomnForm").on('change', function () {
                                    var recomn = $("#recomnForm").val();

                                    if (recomn == 4)
                                    {
                                        $("#branchRecomn").prop('disabled', false);
                                    }

                                });
                                $("#recomnMax").val(data.recomnBranch['Maximum_limit']);
                                $("#recomnGaintext").val(data.recomnBranch['Gain_Value']);

                                var recomnSelect = data.recomnSelect;
                                var recomncounter = 1;

                                $.each(recomnSelect, function (obj, value) {
                                    recomncounter++;

                                    var name = value['Gain'];

                                    code = "<input type='text' name='recomnGain[]' class='form-control'  value='" + name + "' >";
                                    var gain = value['RecoNo'];
                                    gain = "<input type='text' name='recomnRec[]' class='form-control'  value=" + gain + " >";
                                    var prrecomn = value['ValueID'];
                                    prrecomn = "<input type='hidden' name='prrecomn[]' class='form-control'  value=" + prrecomn + " >";
                                    $("#sailorTable2 tbody").append("<tr><td id='prrecomn_" + recomncounter + "' style='display:none'>" + prrecomn + "</td><td id='recomnRec_" + recomncounter + "'>" + gain + "</td><td id='recomnGain_" + recomncounter + "'>" + code + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr12"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                $(document).on('click', '#remove_tr12', function () {

                                if (confirm("Are You Sure?"))
                                {
                                    if (recomncounter > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".precomnValueID").val();

                                                $.ajax({
                                                    type: "post",
                                                    url: "<?php echo site_url('promotion/PromotionRoster/recomnDel'); ?>/",
                                                    data: {id: id},
                                                    beforeSend: function () {
                                                        $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                    },
                                                    success: function (data) {
                                                       console.log(data);
                                                    }
                                                })
                                        recomncounter--;
                                    }
                                    return false;
                                }
                                else
                                {
                                    return false;
                                }
                                });
                            }
                            else
                            {
                                $("#manRecomn").val('');
                                $("#recomnForm").val('');
                                $("#recomnMax").val('');
                                $("#recomnGaintext").val('');
                                $("#sailorTable2 tbody").empty();
                            }

                            if (data.medal != null) {
                                var medal = data.medal;
                                var counter = 1;
                                $.each(medal, function (obj, value) {
                                    counter++;

                                    var name = value['NAME'];
                                    code = "<input type='text' id='medcodes_"+counter+"' name='medalName[]' class='form-control medal'  value='" + name + "' >";

                                    var gain = value['GainValue'];
                                    gain = "<input type='text' name='medalGain[]' class='form-control'  value=" + gain + " >";
                                    var prmedal1 = value['MEDAL_ID'];
                                    prmedal = "<input type='hidden' name='prmedal[]' class='form-control'  value=" + prmedal1 + " >";
                                    MedalID = "<input type='text' name='MedalID[]' id='medIDS_"+counter+"' class='form-control meds' type='hidden'  value=" + prmedal1 + " >";
                                    var medalValue = value['ValueID'];
                                    medalValue = "<input type='hidden' name='medalValue[]' class='form-control medValueID'  value=" + medalValue + " >";
                                    $("#sailorTable3 tbody").append("<tr><td id='prmedal_" + counter + "' style='display:none'>" + MedalID + "</td><td id='prmedal_" + counter + "' style='display:none'>" + medalValue + "</td><td id='prmedal_" + counter + "' style='display:none'>" + prmedal + "</td><td id='medalName_" + counter + "'>" + code + "</td><td id='medalGain_" + counter + "'>" + gain + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr13"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                /*Start for POP UP*/

                                function deselect(e) {
                                  $('.popMedal').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.medal').on('click', function() {

                                    var cd = $(this).attr("id");

                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.popMedal').slideFadeToggle();
                                      $('#MedalCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.popMedal'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#MedalCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#MedalCode option:selected").text();
                                    var trID = $("#MedalCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#medIDS_"+arr[1]).val(trID);
                                    $(".popMedal").fadeOut("slow");

                                });

                                /*End for POP UP*/
                                $(document).on('click', '#remove_tr13', function () {
                                    if (confirm("Are You Sure?"))
                                    {
                                        if (counter > 1) {
                                            $(this).closest('tr').remove();
                                            var id = $(".medValueID").val();

                                                $.ajax({
                                                    type: "post",
                                                    url: "<?php echo site_url('promotion/PromotionRoster/medalDel'); ?>/",
                                                    data: {id: id},
                                                    beforeSend: function () {
                                                        $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                    },
                                                    success: function (data) {

                                                    }
                                                })
                                            counter--;
                                        }
                                        return false;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                });


                            }

                            else {
                                $("#sailorTable3 tbody").empty();
                            }

                            if (data.honor != null) {
                                var honor = data.honor;
                                var counterhonor = 1;
                                $.each(honor, function (obj, value) {
                                    counterhonor++;

                                    var name = value['NAME'];
                                    code = "<input type='text' id='honcodes_"+counterhonor+"' name='honorName[]' class='form-control honor'  value='" + name + "' >";

                                    var gain = value['GainValue'];
                                    gain = "<input type='text' name='honorGain[]' class='form-control'  value=" + gain + " >";
                                    var prhonor1 = value['HONOR_ID'];
                                    prhonor = "<input type='hidden' name='prhonor[]' class='form-control'  value=" + prhonor1 + " >";
                                    HonorID = "<input type='text' name='HonorID[]' id='honIDS_"+counterhonor+"' class='form-control hons' type='hidden'  value=" + prhonor1 + " >";
                                    var honorValue = value['ValueID'];
                                    honorValue = "<input type='hidden' name='honorValue[]' class='form-control honValueID'  value=" + honorValue + " >";
                                    $("#sailorTable4 tbody").append("<tr><td id='prhonot_" + counterhonor + "' style='display:none'>" + HonorID + "</td><td id='prhonot_" + counterhonor + "' style='display:none'>" + honorValue + "</td><td id='prhonot_" + counterhonor + "' style='display:none'>" + prhonor + "</td><td id='honorName_" + counterhonor + "'>" + code + "</td><td id='honorGain_" + counterhonor + "'>" + gain + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr14"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                /*Start for POP UP*/

                                function deselect(e) {
                                  $('.popHonor').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.honor').on('click', function() {

                                    var cd = $(this).attr("id");

                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.popHonor').slideFadeToggle();
                                      $('#HonorCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.popHonor'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#HonorCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#HonorCode option:selected").text();
                                    var trID = $("#HonorCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#honIDS_"+arr[1]).val(trID);
                                    $(".popHonor").fadeOut("slow");

                                });

                                /*End for POP UP*/
                                $(document).on('click', '#remove_tr14', function () {
                                    if (confirm("Are You Sure?"))
                                    {
                                        if (counterhonor > 1) {
                                            $(this).closest('tr').remove();
                                            var id = $(".honValueID").val();

                                                    $.ajax({
                                                        type: "post",
                                                        url: "<?php echo site_url('promotion/PromotionRoster/honorDel'); ?>/",
                                                        data: {id: id},
                                                        beforeSend: function () {
                                                            $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                        },
                                                        success: function (data) {

                                                        }
                                                    })
                                            counterhonor--;
                                        }
                                        return false;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                });


                            }
                            else {
                                $("#sailorTable4 tbody").empty();
                            }

                            if (data.OrderBy != null)
                            {
                                var order = data.OrderBy;
                                var counterorder = 0;
                                $.each(order, function (obj, value) {
                                    counterorder++;

                                    var name = value['Description'];
                                    code = "<input type='text' name='orderby[]' class='form-control'  value='" + name + "' >";

                                    var prorder = value['ValueID'];
                                    prorder = "<input type='hidden' name='prorder[]' class='form-control pr'  value='" + prorder + "' >";
                                    var position = value['Position'];
                                    position = "<input type='hidden'  name='position[]''   class='form-control' value='"+ position + "'>";

                                    $("#sailorTable5 tbody").append("<tr><td id='pos_" + counterorder + "' style='display:none'>" + position + "</td><td id='prorder_" + counterorder + "' style='display:none'>" + prorder + "</td><td id='orderby_" + counterorder + "'>" + code + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr15"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                $(document).on('click', '#remove_tr15', function () {

                                    if (counterorder > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".pr").val();

                                            $.ajax({
                                                type: "post",
                                                url: "<?php echo site_url('promotion/PromotionRoster/orderDel'); ?>/",
                                                data: {id: id},
                                                beforeSend: function () {
                                                    $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                },
                                                success: function (data) {

                                                }
                                            })
                                        counterorder--;
                                    }
                                    return false;
                                });
                            }
                            else
                            {
                                $("#sailorTable5 tbody").empty();
                            }



                        }

                        else {
                            $(".sailorId").val('');
                            $(".fullName").val('');
                            $(".rank").val('');
                            $("#reSeaRank").val('');
                            $("#rankSeaService").val('');
                            $("#gainValue").val('');
                            $("#maximumLimit").val('');
                            $("#SeaSer").val('');
                            $("#InsService").val('');
                            $("#RankService").val('');
                            $("#serviceLength").val('');
                            $("#sailorTable tbody").empty();
                            $("#sailorTable1 tbody").empty();
                            $("#sailorTable2 tbody").empty();
                            $("#sailorTable3 tbody").empty();
                            $("#sailorTable4 tbody").empty();
                            $("#sailorTable5 tbody").empty();
                            $("#manRecomn").val('');
                            $("#recomnForm").val('');
                            $("#recomnMax").val('');
                            $("#recomnGaintext").val('');
                            $(".alertSMS").html('Invalid Officer Number');
                        }
                        $(".smloadingImg").html("");
                        //alert($('#seaService').html(data));
                    }
                });
            } else {
                $(".sailorId").val('');
                $(".fullName").val('');
                $(".rank").val('');
                $("#reSeaRank").val('');
                $("#rankSeaService").val('');
                $("#gainValue").val('');
                $("#maximumLimit").val('');
                $("#SeaSer").val('');
                $("#InsService").val('');
                $("#RankService").val('');
                $("#serviceLength").val('');
                $("#sailorTable tbody").empty();
                $("#sailorTable1 tbody").empty();
                $("#sailorTable2 tbody").empty();
                $("#sailorTable3 tbody").empty();
                $("#sailorTable4 tbody").empty();
                $("#sailorTable5 tbody").empty();
                $("#manRecomn").val('');
                $("#recomnForm").val('');
                $("#recomnMax").val('');
                $("#recomnGaintext").val('');
                $(".alertSMS").html('');
            }
            /*END REGULARE*/
        }
        else
        {
            /*Start Dockyard*/
            if (rank !== '')
            {
                $.ajax({
                    type: "post",
                    data: {rank: rank, rad: rad, cr: cr},
                    dataType: "json",
                    url: "<?php echo site_url(); ?>promotion/PromotionRoster/promotion2",
                    beforeSend: function () {
                        $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        if (data != '')
                        {
                            /*console.log(data.service);
                            console.log(data.training);
                             console.log(data.exam);
                            console.log(data.honor);
                            console.log(data.medal);
                            console.log(data.recomnSelect);
                            console.log(data.recomnBranch);
                            console.log(data.OrderBy);
                            console.log(data.trainCode);*/

                            if (data.service != null) {
                                $('#prSeaservice').val(data.service['RoasterID']);
                                $('#seaService').val(data.service['SeaService']);
                                if (data.service['IsRCSService'] != 1) {
                                    $('#combinedSeaService').prop('checked', false);
                                }
                                else {
                                    $('#combinedSeaService').prop('checked', true);
                                }
                                var abc = data.service['CSSRankID'];
                                $("#reSeaRank").val(abc).trigger("change");
                                var startRank = data.service['RankServiceID'];
                                $("#rankSeaService").val(startRank).trigger('change');
                                $("#gainValue").val(data.service['Given_Value']);
                                $("#maximumLimit").val(data.service['maximumValue']);
                                $("#SeaSer").val(data.service['See_Service']);
                                $("#InsService").val(data.service['Instructor_Service']);
                                $("#RankService").val(data.service['Rank_Service']);
                                $("#serviceLength").val(data.service['Service_Length']);

                                if (data.service['Calculate_Senority'] != 1) {
                                    $('#seniorityPrRank').prop('checked', false);
                                }
                                else {
                                    $('#seniorityPrRank').prop('checked', true);
                                }
                                $("#continuousVg").val(data.service['Continuas_VG']);
                                $("#RSPPMADTime").val(data.service['RSPPMADTime']);
                                $("#dueByTime").val(data.service['Due_by_Time']);
                                ReportCriteriaHeading1
                                $("#ReportCriteriaHeading1").val(data.service['ReportCriteriaHeading1']);
                                $("#ReportCriteriaHeading2").val(data.service['ReportCriteriaHeading2']);


                            }
                            else {
                                $('#seaService').val('');
                                $('#combinedSeaService').prop('checked', false);
                                $(".rank").val('');
                                $("#reSeaRank").val('');
                                $("#rankSeaService").val('');
                                $("#gainValue").val('');
                                $("#maximumLimit").val('');
                                $("#SeaSer").val('');
                                $("#InsService").val('');
                                $("#RankService").val('');
                                $("#serviceLength").val('');
                            }

                            if (data.training != null) {
                                var train = data.training;
                                var trainCode = data.trainCode;

                                var traincounter = 1;
                                $.each(train, function (obj, value) {

                                    var ab = value['Is_Mandatory'];
                                    var status = (ab >=1 ? 'checked' : '');
                                    var manVal = (ab >=1 ? "1" : "0");
                                    str1 = '<input type="checkbox" id="manTrain_'+traincounter+'" value='+manVal+'  name="isMandatory_'+traincounter+'"  class="tempCheck form-control manTrain" '+status+'>';
                                    var name = value['name'];

                                    code = "<input type='text' id='codes_"+traincounter+"'  name='code[]' class='form-control excode'  value='" + name + "' >";
                                    /*code2 = "<select class='select2' id='code2'><option value='"+name+"'>'"+name+"'</option></select>"*/
                                    var gain = value['Gain'];
                                    gain = "<input type='text' name='gain[]' class='form-control'  value=" + gain + " >";
                                    var column1 = value['Output_Column'];
                                    //column = "<input type='text' name='outputColumn[]' class='form-control'  value=" + column + " >";
                                    var outputColumnID = value['CourseDateColumn'];
                                    //outputColumnID = "<input type='text' name='outputColumnID[]' class='form-control'  value=" + outputColumnID + " >";
                                    column = "<select class='form-control' name='outputColumn[]'><option value='"+outputColumnID +"'>"+column1+"</option><option value='1'>Course-1(Date)</option><option value='2'>Course-2(Date)</option><option value='3'>Course-3(Date)</option></select>"
                                    var prtrain = value['NAVYTrainingID'];
                                    codetrain = "<input type='text' name='codetrain[]' id='trIDS_"+traincounter+"' class='form-control trds' type='hidden'  value=" + prtrain + " >";
                                    prtrain = "<input type='text' name='prtrain[]' class='form-control'  value=" + prtrain + " >";
                                    var valueID = value['ValueID'];
                                    values = "<input type='text'  name='valuetrain[]' class='form-control valueID'  value=" + valueID + " >";
                                        $("#sailorTable tbody").append("<tr><td id='prtrain_" + traincounter + "' style='display:none'>" + values + "</td><td id='prtrain_" + traincounter + "' style='display:none'>" + prtrain + "</td><td id='prtrain_" + traincounter + "' style='display:none'>" + codetrain + "</td><td id='code_" + traincounter + "'>" + code + "</td><td id='code_" + traincounter + "'>" + gain + "</td><td id='code_" + traincounter + "'>" + str1 + "</td><td id='code_" + traincounter + "'>" + column + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr10"><i style="cursor:pointer" class="fa fa-times deleteItem" > Remove</i></span>' + "</td></tr>");
                                    traincounter++;
                                 });
                                /*For CheckBox*/
                                $(document).on('click', '.manTrain', function () {
                                    var status = ($(this).is(':checked')) ? 1 : 0;
                                    var checkID = $(this).attr("id");

                                    $("#"+checkID).val(status);
                                });
                                /*For CheckBox*/
                                /*for POP UP*/

                                function deselect(e) {
                                  $('.pop').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.excode').on('click', function() {

                                    var cd = $(this).attr("id");
                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.pop').slideFadeToggle();
                                      $('#trainCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.pop'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#trainCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#trainCode option:selected").text();
                                    var trID = $("#trainCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#trIDS_"+arr[1]).val(trID);
                                    $(".pop").fadeOut("slow");

                                });

                                /*for POP UP*/

                                $(document).on('click', '#remove_tr10', function () {

                                    if (traincounter > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".valueID").val();

                                        $.ajax({
                                            type: "post",
                                            url: "<?php echo site_url('promotion/PromotionRoster/trainDel'); ?>/",
                                            data: {id: id},
                                            beforeSend: function () {
                                                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                            },
                                            success: function (data) {

                                            }
                                        })

                                        traincounter--;
                                    }
                                   // return false;
                                });


                            }
                            else {
                                $("#sailorTable tbody").empty();
                            }
                            if (data.exam != null) {
                                var examin = data.exam;
                                var examcounter = 1;
                                $.each(examin, function (obj, value) {

                                    var ab = value['IsMandatory'];
                                    var status = (ab >=1 ? 'checked' : '');
                                    var manVal = (ab >=1 ? "1" : "0");
                                    str1 = '<input type="checkbox" id="manExam_'+examcounter+'" value='+manVal+'  name="examisMandatory_'+examcounter+'"  class="tempCheck form-control manExam" '+status+'>';
                                    var outputColumnID = value['CourseDateColumn'];

                                    var name = value['name'];

                                    code = "<input type='text' id='excodes_"+examcounter+"' name='examcode[]' class='form-control codeExam'  value='" + name + "' >";
                                    var gain = value['GainValue'];
                                    gain = "<input type='text' name='examgain[]' class='form-control'  value=" + gain + " >";
                                    var column1 = value['Output_Column'];
                                    column = "<select class='form-control' name='examoutputColumn[]'><option value='"+outputColumnID +"'>"+column1+"</option><option value='1'>Course-1(Date)</option><option value='2'>Course-2(Date)</option><option value='3'>Course-3(Date)</option></select>";
                                    var prexam1 = value['EXAM_ID'];
                                    prexam = "<input type='text' name='prexam[]' class='form-control' type='hidden'  value=" + prexam1 + " >";
                                    codeexam = "<input type='text' name='codeexam[]' id='exIDS_"+examcounter+"' class='form-control exds' type='hidden'  value=" + prexam1 + " >";
                                    var exvalueID = value['ValueID'];
                                    exvalues = "<input type='text'  name='valueexam[]' class='form-control exvalueID'  value=" + exvalueID + " >";
                                    $("#sailorTable1 tbody").append("<tr><td id='prexam_" + examcounter + "' style='display:none'>" + exvalues + "</td><td id='prexam_" + examcounter + "' style='display:none'>" + codeexam + "</td><td id='prexam_" + examcounter + "' style='display:none'>" + prexam + "</td><td id='examcode_" + examcounter + "'>" + code + "</td><td id='examgain_" + examcounter + "'>" + gain + "</td><td id='examisMandatory_" + examcounter + "'>" + str1 + "</td><td id='examoutputColumn_" + examcounter + "'>" + column + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr11"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");
                                    examcounter++;
                                });
                                /*Start For CheckBox*/
                                $(document).on('click', '.manExam', function () {
                                    var status = ($(this).is(':checked')) ? 1 : 0;
                                    var checkID = $(this).attr("id");

                                    $("#"+checkID).val(status);
                                });
                                /*End For CheckBox*/
                                /*Start for POP UP*/

                                function deselect(e) {
                                  $('.popCode').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.codeExam').on('click', function() {

                                    var cd = $(this).attr("id");

                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.popCode').slideFadeToggle();
                                      $('#examCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.popCode'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#examCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#examCode option:selected").text();
                                    var trID = $("#examCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#exIDS_"+arr[1]).val(trID);
                                    $(".popCode").fadeOut("slow");

                                });

                                /*End for POP UP*/
                                $(document).on('click', '#remove_tr11', function () {
                                    if (confirm("Are You Sure?"))
                                    {
                                        if (examcounter > 1) {
                                            $(this).closest('tr').remove();
                                            var id = $(".exvalueID").val();

                                            $.ajax({
                                                type: "post",
                                                url: "<?php echo site_url('promotion/PromotionRoster/examDel'); ?>/",
                                                data: {id: id},
                                                beforeSend: function () {
                                                    $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                },
                                                success: function (data) {

                                                }
                                            })
                                            examcounter--;
                                        }
                                        return false;

                                    }
                                    else
                                    {
                                        return false;
                                    }
                                });


                            }
                            else {
                                $("#sailorTable1 tbody").empty();
                            }
                            if (data.recomnSelect != null || data.recomnBranch != null)
                            {
                                if (data.recomnBranch['mendatory_recommendation_for_promotion'] != 1)
                                {
                                    $('#manRecomn').prop('checked', false);
                                }
                                else
                                {
                                    $('#manRecomn').prop('checked', true);
                                }
                                $("#recomnForm").val(data.recomnBranch['RecomCriteria']).trigger("change");

                                if (data.recomnBranch['RecomCriteria'] != 4)
                                {
                                    $("#branchRecomn").prop('disabled', true);
                                }
                                $("#recomnForm").on('change', function () {
                                    var recomn = $("#recomnForm").val();

                                    if (recomn == 4)
                                    {
                                        $("#branchRecomn").prop('disabled', false);
                                    }

                                });
                                $("#recomnMax").val(data.recomnBranch['Maximum_limit']);
                                $("#recomnGaintext").val(data.recomnBranch['Gain_Value']);

                                var recomnSelect = data.recomnSelect;
                                var recomncounter = 1;

                                $.each(recomnSelect, function (obj, value) {
                                    recomncounter++;

                                    var name = value['Gain'];

                                    code = "<input type='text' name='recomnGain[]' class='form-control'  value='" + name + "' >";
                                    var gain = value['RecoNo'];
                                    gain = "<input type='text' name='recomnRec[]' class='form-control'  value=" + gain + " >";
                                    var prrecomn = value['ValueID'];
                                    prrecomn = "<input type='hidden' name='prrecomn[]' class='form-control'  value=" + prrecomn + " >";
                                    $("#sailorTable2 tbody").append("<tr><td id='prrecomn_" + recomncounter + "' style='display:none'>" + prrecomn + "</td><td id='recomnRec_" + recomncounter + "'>" + gain + "</td><td id='recomnGain_" + recomncounter + "'>" + code + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr12"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                $(document).on('click', '#remove_tr12', function () {

                                if (confirm("Are You Sure?"))
                                {
                                    if (recomncounter > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".precomnValueID").val();

                                                $.ajax({
                                                    type: "post",
                                                    url: "<?php echo site_url('promotion/PromotionRoster/recomnDel'); ?>/",
                                                    data: {id: id},
                                                    beforeSend: function () {
                                                        $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                    },
                                                    success: function (data) {
                                                       console.log(data);
                                                    }
                                                })
                                        recomncounter--;
                                    }
                                    return false;
                                }
                                else
                                {
                                    return false;
                                }
                                });
                            }
                            else
                            {
                                $("#manRecomn").val('');
                                $("#recomnForm").val('');
                                $("#recomnMax").val('');
                                $("#recomnGaintext").val('');
                                $("#sailorTable2 tbody").empty();
                            }

                            if (data.medal != null) {
                                var medal = data.medal;
                                var counter = 1;
                                $.each(medal, function (obj, value) {
                                    counter++;

                                    var name = value['NAME'];
                                    code = "<input type='text' id='medcodes_"+counter+"' name='medalName[]' class='form-control medal'  value='" + name + "' >";

                                    var gain = value['GainValue'];
                                    gain = "<input type='text' name='medalGain[]' class='form-control'  value=" + gain + " >";
                                    var prmedal1 = value['MEDAL_ID'];
                                    prmedal = "<input type='hidden' name='prmedal[]' class='form-control'  value=" + prmedal1 + " >";
                                    MedalID = "<input type='text' name='MedalID[]' id='medIDS_"+counter+"' class='form-control meds' type='hidden'  value=" + prmedal1 + " >";
                                    var medalValue = value['ValueID'];
                                    medalValue = "<input type='hidden' name='medalValue[]' class='form-control medValueID'  value=" + medalValue + " >";
                                    $("#sailorTable3 tbody").append("<tr><td id='prmedal_" + counter + "' style='display:none'>" + MedalID + "</td><td id='prmedal_" + counter + "' style='display:none'>" + medalValue + "</td><td id='prmedal_" + counter + "' style='display:none'>" + prmedal + "</td><td id='medalName_" + counter + "'>" + code + "</td><td id='medalGain_" + counter + "'>" + gain + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr13"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                /*Start for POP UP*/

                                function deselect(e) {
                                  $('.popMedal').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.medal').on('click', function() {

                                    var cd = $(this).attr("id");

                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.popMedal').slideFadeToggle();
                                      $('#MedalCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.popMedal'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#MedalCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#MedalCode option:selected").text();
                                    var trID = $("#MedalCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#medIDS_"+arr[1]).val(trID);
                                    $(".popMedal").fadeOut("slow");

                                });

                                /*End for POP UP*/
                                $(document).on('click', '#remove_tr13', function () {
                                    if (confirm("Are You Sure?"))
                                    {
                                        if (counter > 1) {
                                            $(this).closest('tr').remove();
                                            var id = $(".medValueID").val();

                                                $.ajax({
                                                    type: "post",
                                                    url: "<?php echo site_url('promotion/PromotionRoster/medalDel'); ?>/",
                                                    data: {id: id},
                                                    beforeSend: function () {
                                                        $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                    },
                                                    success: function (data) {

                                                    }
                                                })
                                            counter--;
                                        }
                                        return false;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                });


                            }

                            else {
                                $("#sailorTable3 tbody").empty();
                            }

                            if (data.honor != null) {
                                var honor = data.honor;
                                var counterhonor = 1;
                                $.each(honor, function (obj, value) {
                                    counterhonor++;

                                    var name = value['NAME'];
                                    code = "<input type='text' id='honcodes_"+counterhonor+"' name='honorName[]' class='form-control honor'  value='" + name + "' >";

                                    var gain = value['GainValue'];
                                    gain = "<input type='text' name='honorGain[]' class='form-control'  value=" + gain + " >";
                                    var prhonor1 = value['HONOR_ID'];
                                    prhonor = "<input type='hidden' name='prhonor[]' class='form-control'  value=" + prhonor1 + " >";
                                    HonorID = "<input type='text' name='HonorID[]' id='honIDS_"+counterhonor+"' class='form-control hons' type='hidden'  value=" + prhonor1 + " >";
                                    var honorValue = value['ValueID'];
                                    honorValue = "<input type='hidden' name='honorValue[]' class='form-control honValueID'  value=" + honorValue + " >";
                                    $("#sailorTable4 tbody").append("<tr><td id='prhonot_" + counterhonor + "' style='display:none'>" + HonorID + "</td><td id='prhonot_" + counterhonor + "' style='display:none'>" + honorValue + "</td><td id='prhonot_" + counterhonor + "' style='display:none'>" + prhonor + "</td><td id='honorName_" + counterhonor + "'>" + code + "</td><td id='honorGain_" + counterhonor + "'>" + gain + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr14"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                /*Start for POP UP*/

                                function deselect(e) {
                                  $('.popHonor').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.honor').on('click', function() {

                                    var cd = $(this).attr("id");

                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.popHonor').slideFadeToggle();
                                      $('#HonorCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.popHonor'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#HonorCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                   // alert(arr[1]);

                                    var name = $("#HonorCode option:selected").text();
                                    var trID = $("#HonorCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#honIDS_"+arr[1]).val(trID);
                                    $(".popHonor").fadeOut("slow");

                                });

                                /*End for POP UP*/
                                $(document).on('click', '#remove_tr14', function () {
                                    if (confirm("Are You Sure?"))
                                    {
                                        if (counterhonor > 1) {
                                            $(this).closest('tr').remove();
                                            var id = $(".honValueID").val();

                                                    $.ajax({
                                                        type: "post",
                                                        url: "<?php echo site_url('promotion/PromotionRoster/honorDel'); ?>/",
                                                        data: {id: id},
                                                        beforeSend: function () {
                                                            $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                        },
                                                        success: function (data) {

                                                        }
                                                    })
                                            counterhonor--;
                                        }
                                        return false;
                                    }
                                    else
                                    {
                                        return false;
                                    }
                                });


                            }
                            else {
                                $("#sailorTable4 tbody").empty();
                            }

                            if (data.OrderBy != null)
                            {
                                var order = data.OrderBy;
                                var counterorder = 0;
                                $.each(order, function (obj, value) {
                                    counterorder++;

                                    var name = value['Description'];
                                    code = "<input type='text' name='orderby[]' class='form-control'  value='" + name + "' >";

                                    var prorder = value['ValueID'];
                                    prorder = "<input type='hidden' name='prorder[]' class='form-control pr'  value='" + prorder + "' >";
                                    var position = value['Position'];
                                    position = "<input type='hidden'  name='position[]''   class='form-control' value='"+ position + "'>";

                                    $("#sailorTable5 tbody").append("<tr><td id='pos_" + counterorder + "' style='display:none'>" + position + "</td><td id='prorder_" + counterorder + "' style='display:none'>" + prorder + "</td><td id='orderby_" + counterorder + "'>" + code + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr15"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                                });
                                $(document).on('click', '#remove_tr15', function () {

                                    if (counterorder > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".pr").val();

                                            $.ajax({
                                                type: "post",
                                                url: "<?php echo site_url('promotion/PromotionRoster/orderDel'); ?>/",
                                                data: {id: id},
                                                beforeSend: function () {
                                                    $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                },
                                                success: function (data) {

                                                }
                                            })
                                        counterorder--;
                                    }
                                    return false;
                                });
                            }
                            else
                            {
                                $("#sailorTable5 tbody").empty();
                            }



                        }

                        else {
                            $(".sailorId").val('');
                            $(".fullName").val('');
                            $(".rank").val('');
                            $("#reSeaRank").val('');
                            $("#rankSeaService").val('');
                            $("#gainValue").val('');
                            $("#maximumLimit").val('');
                            $("#SeaSer").val('');
                            $("#InsService").val('');
                            $("#RankService").val('');
                            $("#serviceLength").val('');
                            $("#sailorTable tbody").empty();
                            $("#sailorTable1 tbody").empty();
                            $("#sailorTable2 tbody").empty();
                            $("#sailorTable3 tbody").empty();
                            $("#sailorTable4 tbody").empty();
                            $("#sailorTable5 tbody").empty();
                            $("#manRecomn").val('');
                            $("#recomnForm").val('');
                            $("#recomnMax").val('');
                            $("#recomnGaintext").val('');
                            $(".alertSMS").html('Invalid Officer Number');
                        }
                        $(".smloadingImg").html("");
                        //alert($('#seaService').html(data));
                    }
                });
            } else {
                $(".sailorId").val('');
                $(".fullName").val('');
                $(".rank").val('');
                $("#reSeaRank").val('');
                $("#rankSeaService").val('');
                $("#gainValue").val('');
                $("#maximumLimit").val('');
                $("#SeaSer").val('');
                $("#InsService").val('');
                $("#RankService").val('');
                $("#serviceLength").val('');
                $("#sailorTable tbody").empty();
                $("#sailorTable1 tbody").empty();
                $("#sailorTable2 tbody").empty();
                $("#sailorTable3 tbody").empty();
                $("#sailorTable4 tbody").empty();
                $("#sailorTable5 tbody").empty();
                $("#manRecomn").val('');
                $("#recomnForm").val('');
                $("#recomnMax").val('');
                $("#recomnGaintext").val('');
                $(".alertSMS").html('');
            }
            /*END Dockyard*/
        }
    });
    /*FOR ALL MAIN*/
    $("#rank_from1").on('change', function () {
        $("#sailorTable tbody").empty();
        $("#sailorTable1 tbody").empty();
        $("#sailorTable2 tbody").empty();
        $("#sailorTable3 tbody").empty();
        $("#sailorTable4 tbody").empty();
        $("#sailorTable5 tbody").empty();
        var rank = $(this).val();
        var br = $("#branchFrom1").val();
        var rad = $('input[class=rad]:checked').val();
        var cr = $('input[class=roasterCr]:checked').val();

        if (rad == 1)
        {

            $("#point").prop("checked", false);
            $("#month").prop("checked", true);
        }
        else
        {

            $("#point").prop("checked", true);
            $("#month").prop("checked", false);
            if (rad == 2)
            {

                if (br == 4 && rank == 37 || br == 4 && rank == 38 || br == 7 && rank == 78 || br == 5 && rank == 51 || br == 5 && rank == 52 || br == 6 && rank == 64 || br == 6 && rank == 66 || br == 8 && rank == 90 || br == 8 && rank == 91)
                {
                    $("#point").prop("checked", false);
                    $("#month").prop("checked", true);
                }
                else
                {
                    $("#point").prop("checked", true);
                    $("#month").prop("checked", false);
                }

            }
            else
            {

                $("#point").prop("checked", true);
                $("#month").prop("checked", false);
            }
        }
        if (rad == 1 && br == 4 && rank != 37 || rad == 1 && br == 4 && rank != 38 || rad == 1 && br == 7 && rank != 78 || rad == 1 && br == 5 && rank != 51 || rad == 1 && br == 5 && rank != 52 || rad == 1 && br == 6 && rank != 64 || rad == 1 && br == 6 && rank != 66 || rad == 1 && br == 8 && rank != 90 || rad == 1 && br == 8 && rank != 91)
        {

            $("#point").prop("checked", true);
            $("#month").prop("checked", false);

        }
        if (rad == 1 && br == 4 && rank == 37 || rad == 1 && br == 4 && rank == 38 || rad == 1 && br == 7 && rank == 78 || rad == 1 && br == 5 && rank == 51 || rad == 1 && br == 5 && rank == 52 || rad == 1 && br == 6 && rank == 64 || rad == 1 && br == 6 && rank == 66 || rad == 1 && br == 8 && rank == 90 || rad == 1 && br == 8 && rank == 91)
        {

            $("#point").prop("checked", false);
            $("#month").prop("checked", true);

        }

        var rad = $('input[class=rad]:checked').val();
        if (rank !== '')
        {
            $.ajax({
                type: "post",
                data: {rank: rank, rad: rad, cr: cr},
                dataType: "json",
                url: "<?php echo site_url(); ?>promotion/PromotionRoster/promotion",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    if (data != '')
                    {
                        /*console.log(data.service);
                        console.log(data.training);
                         console.log(data.exam);
                        console.log(data.honor);
                        console.log(data.medal);
                        console.log(data.recomnSelect);
                        console.log(data.recomnBranch);
                        console.log(data.OrderBy);
                        console.log(data.trainCode);*/

                        if (data.service != null) {
                            $('#prSeaservice').val(data.service['RoasterID']);
                            $('#seaService').val(data.service['SeaService']);
                            if (data.service['IsRCSService'] != 1) {
                                $('#combinedSeaService').prop('checked', false);
                            }
                            else {
                                $('#combinedSeaService').prop('checked', true);
                            }
                            var abc = data.service['CSSRankID'];
                            $("#reSeaRank").val(abc).trigger("change");
                            var startRank = data.service['RankServiceID'];
                            $("#rankSeaService").val(startRank).trigger('change');
                            $("#gainValue").val(data.service['Given_Value']);
                            $("#maximumLimit").val(data.service['maximumValue']);
                            $("#SeaSer").val(data.service['See_Service']);
                            $("#InsService").val(data.service['Instructor_Service']);
                            $("#RankService").val(data.service['Rank_Service']);
                            $("#serviceLength").val(data.service['Service_Length']);

                            if (data.service['Calculate_Senority'] != 1) {
                                $('#seniorityPrRank').prop('checked', false);
                            }
                            else {
                                $('#seniorityPrRank').prop('checked', true);
                            }
                            $("#continuousVg").val(data.service['Continuas_VG']);
                            $("#RSPPMADTime").val(data.service['RSPPMADTime']);
                            $("#dueByTime").val(data.service['Due_by_Time']);
                            ReportCriteriaHeading1
                            $("#ReportCriteriaHeading1").val(data.service['ReportCriteriaHeading1']);
                            $("#ReportCriteriaHeading2").val(data.service['ReportCriteriaHeading2']);


                        }
                        else {
                            $('#seaService').val('');
                            $('#combinedSeaService').prop('checked', false);
                            $(".rank").val('');
                            $("#reSeaRank").val('');
                            $("#rankSeaService").val('');
                            $("#gainValue").val('');
                            $("#maximumLimit").val('');
                            $("#SeaSer").val('');
                            $("#InsService").val('');
                            $("#RankService").val('');
                            $("#serviceLength").val('');
                        }

                        if (data.training != null) {
                            var train = data.training;
                            var trainCode = data.trainCode;

                            var traincounter = 1;
                            $.each(train, function (obj, value) {

                                var ab = value['Is_Mandatory'];
                                var status = (ab >=1 ? 'checked' : '');
                                var manVal = (ab >=1 ? "1" : "0");
                                str1 = '<input type="checkbox" id="manTrain_'+traincounter+'" value='+manVal+'  name="isMandatory_'+traincounter+'"  class="tempCheck form-control manTrain" '+status+'>';
                                var name = value['name'];

                                code = "<input type='text' id='codes_"+traincounter+"'  name='code[]' class='form-control excode'  value='" + name + "' >";
                                /*code2 = "<select class='select2' id='code2'><option value='"+name+"'>'"+name+"'</option></select>"*/
                                var gain = value['Gain'];
                                gain = "<input type='text' name='gain[]' class='form-control'  value=" + gain + " >";
                                var column1 = value['Output_Column'];
                                //column = "<input type='text' name='outputColumn[]' class='form-control'  value=" + column + " >";
                                var outputColumnID = value['CourseDateColumn'];
                                //outputColumnID = "<input type='text' name='outputColumnID[]' class='form-control'  value=" + outputColumnID + " >";
                                column = "<select class='form-control' name='outputColumn[]'><option value='"+outputColumnID +"'>"+column1+"</option><option value='1'>Course-1(Date)</option><option value='2'>Course-2(Date)</option><option value='3'>Course-3(Date)</option></select>"
                                var prtrain = value['NAVYTrainingID'];
                                codetrain = "<input type='text' name='codetrain[]' id='trIDS_"+traincounter+"' class='form-control trds' type='hidden'  value=" + prtrain + " >";
                                prtrain = "<input type='text' name='prtrain[]' class='form-control'  value=" + prtrain + " >";
                                var valueID = value['ValueID'];
                                values = "<input type='text'  name='valuetrain[]' class='form-control valueID'  value=" + valueID + " >";
                                    $("#sailorTable tbody").append("<tr><td id='prtrain_" + traincounter + "' style='display:none'>" + values + "</td><td id='prtrain_" + traincounter + "' style='display:none'>" + prtrain + "</td><td id='prtrain_" + traincounter + "' style='display:none'>" + codetrain + "</td><td id='code_" + traincounter + "'>" + code + "</td><td id='code_" + traincounter + "'>" + gain + "</td><td id='code_" + traincounter + "'>" + str1 + "</td><td id='code_" + traincounter + "'>" + column + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr10"><i style="cursor:pointer" class="fa fa-times deleteItem" > Remove</i></span>' + "</td></tr>");
                                traincounter++;
                             });
                            /*For CheckBox*/
                            $(document).on('click', '.manTrain', function () {
                                var status = ($(this).is(':checked')) ? 1 : 0;
                                var checkID = $(this).attr("id");

                                $("#"+checkID).val(status);
                            });
                            /*For CheckBox*/
                            /*for POP UP*/

                            function deselect(e) {
                              $('.pop').slideFadeToggle(function() {
                                e.removeClass('selected');
                              });
                            }

                            $(function() {
                              $('.excode').on('click', function() {

                                var cd = $(this).attr("id");
                                if($(this).hasClass('selected')) {
                                  deselect($(this));
                                } else {
                                  $(this).addClass('selected');
                                  $('.pop').slideFadeToggle();
                                  $('#trainCode').attr("code-id", cd);
                                }

                                return false;

                              });

                              $('.close').on('click', function() {
                                deselect($('.pop'));
                                return false;
                              });
                            });

                            $.fn.slideFadeToggle = function(easing, callback) {
                              return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                            };
                            $("#trainCode").on('change', function () {
                                var codeId = $(this).attr("code-id");

                                var arr = codeId.split('_');
                                //alert(arr[1]);

                                var name = $("#trainCode option:selected").text();
                                var trID = $("#trainCode option:selected").val();
                                $("#"+codeId).val(name);
                                $("#trIDS_"+arr[1]).val(trID);
                                $(".pop").fadeOut("slow");

                            });

                            /*for POP UP*/

                            $(document).on('click', '#remove_tr10', function () {

                                if (traincounter > 1) {
                                    $(this).closest('tr').remove();
                                    var id = $(".valueID").val();

                                    $.ajax({
                                        type: "post",
                                        url: "<?php echo site_url('promotion/PromotionRoster/trainDel'); ?>/",
                                        data: {id: id},
                                        beforeSend: function () {
                                            $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                        },
                                        success: function (data) {

                                        }
                                    })

                                    traincounter--;
                                }
                               // return false;
                            });


                        }
                        else {
                            $("#sailorTable tbody").empty();
                        }
                        if (data.exam != null) {
                            var examin = data.exam;
                            var examcounter = 1;
                            $.each(examin, function (obj, value) {

                                var ab = value['IsMandatory'];
                                var status = (ab >=1 ? 'checked' : '');
                                var manVal = (ab >=1 ? "1" : "0");
                                str1 = '<input type="checkbox" id="manExam_'+examcounter+'" value='+manVal+'  name="examisMandatory_'+examcounter+'"  class="tempCheck form-control manExam" '+status+'>';
                                var outputColumnID = value['CourseDateColumn'];

                                var name = value['name'];

                                code = "<input type='text' id='excodes_"+examcounter+"' name='examcode[]' class='form-control codeExam'  value='" + name + "' >";
                                var gain = value['GainValue'];
                                gain = "<input type='text' name='examgain[]' class='form-control'  value=" + gain + " >";
                                var column1 = value['Output_Column'];
                                column = "<select class='form-control' name='examoutputColumn[]'><option value='"+outputColumnID +"'>"+column1+"</option><option value='1'>Course-1(Date)</option><option value='2'>Course-2(Date)</option><option value='3'>Course-3(Date)</option></select>";
                                var prexam1 = value['EXAM_ID'];
                                prexam = "<input type='text' name='prexam[]' class='form-control' type='hidden'  value=" + prexam1 + " >";
                                codeexam = "<input type='text' name='codeexam[]' id='exIDS_"+examcounter+"' class='form-control exds' type='hidden'  value=" + prexam1 + " >";
                                var exvalueID = value['ValueID'];
                                exvalues = "<input type='text'  name='valueexam[]' class='form-control exvalueID'  value=" + exvalueID + " >";
                                $("#sailorTable1 tbody").append("<tr><td id='prexam_" + examcounter + "' style='display:none'>" + exvalues + "</td><td id='prexam_" + examcounter + "' style='display:none'>" + codeexam + "</td><td id='prexam_" + examcounter + "' style='display:none'>" + prexam + "</td><td id='examcode_" + examcounter + "'>" + code + "</td><td id='examgain_" + examcounter + "'>" + gain + "</td><td id='examisMandatory_" + examcounter + "'>" + str1 + "</td><td id='examoutputColumn_" + examcounter + "'>" + column + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr11"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");
                                examcounter++;
                            });
                            /*Start For CheckBox*/
                            $(document).on('click', '.manExam', function () {
                                var status = ($(this).is(':checked')) ? 1 : 0;
                                var checkID = $(this).attr("id");

                                $("#"+checkID).val(status);
                            });
                            /*End For CheckBox*/
                            /*Start for POP UP*/

                            function deselect(e) {
                              $('.popCode').slideFadeToggle(function() {
                                e.removeClass('selected');
                              });
                            }

                            $(function() {
                              $('.codeExam').on('click', function() {

                                var cd = $(this).attr("id");

                                if($(this).hasClass('selected')) {
                                  deselect($(this));
                                } else {
                                  $(this).addClass('selected');
                                  $('.popCode').slideFadeToggle();
                                  $('#examCode').attr("code-id", cd);
                                }

                                return false;

                              });

                              $('.close').on('click', function() {
                                deselect($('.popCode'));
                                return false;
                              });
                            });

                            $.fn.slideFadeToggle = function(easing, callback) {
                              return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                            };
                            $("#examCode").on('change', function () {
                                var codeId = $(this).attr("code-id");

                                var arr = codeId.split('_');
                                //alert(arr[1]);

                                var name = $("#examCode option:selected").text();
                                var trID = $("#examCode option:selected").val();
                                $("#"+codeId).val(name);
                                $("#exIDS_"+arr[1]).val(trID);
                                $(".popCode").fadeOut("slow");

                            });

                            /*End for POP UP*/
                            $(document).on('click', '#remove_tr11', function () {
                                if (confirm("Are You Sure?"))
                                {
                                    if (examcounter > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".exvalueID").val();

                                        $.ajax({
                                            type: "post",
                                            url: "<?php echo site_url('promotion/PromotionRoster/examDel'); ?>/",
                                            data: {id: id},
                                            beforeSend: function () {
                                                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                            },
                                            success: function (data) {

                                            }
                                        })
                                        examcounter--;
                                    }
                                    return false;

                                }
                                else
                                {
                                    return false;
                                }
                            });


                        }
                        else {
                            $("#sailorTable1 tbody").empty();
                        }
                        if (data.recomnSelect != null || data.recomnBranch != null)
                        {
                            if (data.recomnBranch['mendatory_recommendation_for_promotion'] != 1)
                            {
                                $('#manRecomn').prop('checked', false);
                            }
                            else
                            {
                                $('#manRecomn').prop('checked', true);
                            }
                            $("#recomnForm").val(data.recomnBranch['RecomCriteria']).trigger("change");

                            if (data.recomnBranch['RecomCriteria'] != 4)
                            {
                                $("#branchRecomn").prop('disabled', true);
                            }
                            $("#recomnForm").on('change', function () {
                                var recomn = $("#recomnForm").val();

                                if (recomn == 4)
                                {
                                    $("#branchRecomn").prop('disabled', false);
                                }

                            });
                            $("#recomnMax").val(data.recomnBranch['Maximum_limit']);
                            $("#recomnGaintext").val(data.recomnBranch['Gain_Value']);

                            var recomnSelect = data.recomnSelect;
                            var recomncounter = 1;

                            $.each(recomnSelect, function (obj, value) {
                                recomncounter++;

                                var name = value['Gain'];

                                code = "<input type='text' name='recomnGain[]' class='form-control'  value='" + name + "' >";
                                var gain = value['RecoNo'];
                                gain = "<input type='text' name='recomnRec[]' class='form-control'  value=" + gain + " >";
                                var prrecomn = value['ValueID'];
                                prrecomn = "<input type='hidden' name='prrecomn[]' class='form-control precomnValueID'  value=" + prrecomn + " >";
                                $("#sailorTable2 tbody").append("<tr><td id='prrecomn_" + recomncounter + "' style='display:none'>" + prrecomn + "</td><td id='recomnRec_" + recomncounter + "'>" + gain + "</td><td id='recomnGain_" + recomncounter + "'>" + code + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr12"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                            });
                            $(document).on('click', '#remove_tr12', function () {
                                if (confirm("Are You Sure?"))
                                {
                                    if (recomncounter > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".precomnValueID").val();

                                                $.ajax({
                                                    type: "post",
                                                    url: "<?php echo site_url('promotion/PromotionRoster/recomnDel'); ?>/",
                                                    data: {id: id},
                                                    beforeSend: function () {
                                                        $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                    },
                                                    success: function (data) {
                                                       console.log(data);
                                                       location.refresh(true);
                                                    }
                                                })
                                        recomncounter--;
                                    }
                                    return false;
                                }
                                else
                                {
                                    return false;
                                }
                            });
                        }
                        else
                        {
                            $("#manRecomn").val('');
                            $("#recomnForm").val('');
                            $("#recomnMax").val('');
                            $("#recomnGaintext").val('');
                            $("#sailorTable2 tbody").empty();
                        }

                        if (data.medal != null) {
                            var medal = data.medal;
                            var counter = 1;
                            $.each(medal, function (obj, value) {
                                counter++;

                                var name = value['NAME'];
                                code = "<input type='text' id='medcodes_"+counter+"' name='medalName[]' class='form-control medal'  value='" + name + "' >";

                                var gain = value['GainValue'];
                                gain = "<input type='text' name='medalGain[]' class='form-control'  value=" + gain + " >";
                                var prmedal1 = value['MEDAL_ID'];
                                prmedal = "<input type='hidden' name='prmedal[]' class='form-control'  value=" + prmedal1 + " >";
                                MedalID = "<input type='text' name='MedalID[]' id='medIDS_"+counter+"' class='form-control meds' type='hidden'  value=" + prmedal1 + " >";
                                var medalValue = value['ValueID'];
                                medalValue = "<input type='hidden' name='medalValue[]' class='form-control medValueID'  value=" + medalValue + " >";
                                $("#sailorTable3 tbody").append("<tr><td id='prmedal_" + counter + "' style='display:none'>" + MedalID + "</td><td id='prmedal_" + counter + "' style='display:none'>" + medalValue + "</td><td id='prmedal_" + counter + "' style='display:none'>" + prmedal + "</td><td id='medalName_" + counter + "'>" + code + "</td><td id='medalGain_" + counter + "'>" + gain + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr13"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                            });
                            /*Start for POP UP*/

                            function deselect(e) {
                              $('.popMedal').slideFadeToggle(function() {
                                e.removeClass('selected');
                              });
                            }

                            $(function() {
                              $('.medal').on('click', function() {

                                var cd = $(this).attr("id");

                                if($(this).hasClass('selected')) {
                                  deselect($(this));
                                } else {
                                  $(this).addClass('selected');
                                  $('.popMedal').slideFadeToggle();
                                  $('#MedalCode').attr("code-id", cd);
                                }

                                return false;

                              });

                              $('.close').on('click', function() {
                                deselect($('.popMedal'));
                                return false;
                              });
                            });

                            $.fn.slideFadeToggle = function(easing, callback) {
                              return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                            };
                            $("#MedalCode").on('change', function () {
                                var codeId = $(this).attr("code-id");

                                var arr = codeId.split('_');
                                //alert(arr[1]);

                                var name = $("#MedalCode option:selected").text();
                                var trID = $("#MedalCode option:selected").val();
                                $("#"+codeId).val(name);
                                $("#medIDS_"+arr[1]).val(trID);
                                $(".popMedal").fadeOut("slow");

                            });

                            /*End for POP UP*/
                            $(document).on('click', '#remove_tr13', function () {
                                if (confirm("Are You Sure?"))
                                {
                                    if (counter > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".medValueID").val();

                                            $.ajax({
                                                type: "post",
                                                url: "<?php echo site_url('promotion/PromotionRoster/medalDel'); ?>/",
                                                data: {id: id},
                                                beforeSend: function () {
                                                    $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                },
                                                success: function (data) {

                                                }
                                            })
                                        counter--;
                                    }
                                    return false;
                                }
                                else
                                {
                                    return false;
                                }
                            });


                        }

                        else {
                            $("#sailorTable3 tbody").empty();
                        }

                        if (data.honor != null) {
                            var honor = data.honor;
                            var counterhonor = 1;
                            $.each(honor, function (obj, value) {
                                counterhonor++;

                                var name = value['NAME'];
                                code = "<input type='text' id='honcodes_"+counterhonor+"' name='honorName[]' class='form-control honor'   value='" + name + "' >";

                                var gain = value['GainValue'];
                                gain = "<input type='text' name='honorGain[]' class='form-control '  value=" + gain + " >";
                                var prhonor1 = value['HONOR_ID'];
                                prhonor = "<input type='hidden' name='prhonor[]' class='form-control'  value=" + prhonor1 + " >";
                                HonorID = "<input type='text' name='HonorID[]' id='honIDS_"+counterhonor+"' class='form-control hons' type='hidden'  value=" + prhonor1 + " >";
                                var honorValue = value['ValueID'];
                                honorValue = "<input type='hidden' name='honorValue[]' class='form-control honValueID'  value=" + honorValue + " >";
                                $("#sailorTable4 tbody").append("<tr><td id='prhonot_" + counterhonor + "' style='display:none'>" + HonorID + "</td><td id='prhonot_" + counterhonor + "' style='display:none'>" + honorValue + "</td><td id='prhonot_" + counterhonor + "' style='display:none'>" + prhonor + "</td><td id='honorName_" + counterhonor + "'>" + code + "</td><td id='honorGain_" + counterhonor + "'>" + gain + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr14"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                            });
                            /*Start for POP UP*/

                                function deselect(e) {
                                  $('.popHonor').slideFadeToggle(function() {
                                    e.removeClass('selected');
                                  });
                                }

                                $(function() {
                                  $('.honor').on('click', function() {

                                    var cd = $(this).attr("id");

                                    if($(this).hasClass('selected')) {
                                      deselect($(this));
                                    } else {
                                      $(this).addClass('selected');
                                      $('.popHonor').slideFadeToggle();
                                      $('#HonorCode').attr("code-id", cd);
                                    }

                                    return false;

                                  });

                                  $('.close').on('click', function() {
                                    deselect($('.popHonor'));
                                    return false;
                                  });
                                });

                                $.fn.slideFadeToggle = function(easing, callback) {
                                  return this.animate({ opacity: 'toggle', height: 'toggle' }, 'fast', easing, callback);
                                };
                                $("#HonorCode").on('change', function () {
                                    var codeId = $(this).attr("code-id");

                                    var arr = codeId.split('_');
                                    //alert(arr[1]);

                                    var name = $("#HonorCode option:selected").text();
                                    var trID = $("#HonorCode option:selected").val();
                                    $("#"+codeId).val(name);
                                    $("#honIDS_"+arr[1]).val(trID);

                                    $(".popHonor").fadeOut("slow");

                                });

                                /*End for POP UP*/
                            $(document).on('click', '#remove_tr14', function () {
                                if (confirm("Are You Sure?"))
                                {
                                    if (counterhonor > 1) {
                                        $(this).closest('tr').remove();
                                        var id = $(".honValueID").val();

                                                $.ajax({
                                                    type: "post",
                                                    url: "<?php echo site_url('promotion/PromotionRoster/honorDel'); ?>/",
                                                    data: {id: id},
                                                    beforeSend: function () {
                                                        $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                                    },
                                                    success: function (data) {

                                                    }
                                                })
                                        counterhonor--;
                                    }
                                    return false;
                                }
                                else
                                {
                                    return false;
                                }
                            });


                        }
                        else {
                            $("#sailorTable4 tbody").empty();
                        }

                        if (data.OrderBy != null)
                        {
                            var order = data.OrderBy;
                            var counterorder = 0;
                            $.each(order, function (obj, value) {
                                counterorder++;

                                var name = value['Description'];
                                code = "<input type='text' name='orderby[]' class='form-control'  value='" + name + "' >";

                                var prorder = value['ValueID'];
                                prorder = "<input type='hidden' name='prorder[]' class='form-control pr'  value='" + prorder + "' >";
                                var position = value['Position'];
                                position = "<input type='hidden'  name='position[]''   class='form-control' value='"+ position + "'>";

                                $("#sailorTable5 tbody").append("<tr><td id='pos_" + counterorder + "' style='display:none'>" + position + "</td><td id='prorder_" + counterorder + "' style='display:none'>" + prorder + "</td><td id='orderby_" + counterorder + "'>" + code + "</td><td>" + ' ' + '<span class="btn btn-xs btn-danger" id="remove_tr15"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' + "</td></tr>");

                            });
                            $(document).on('click', '#remove_tr15', function () {

                                if (counterorder > 1) {
                                    $(this).closest('tr').remove();
                                    var id = $(".pr").val();

                                        $.ajax({
                                            type: "post",
                                            url: "<?php echo site_url('promotion/PromotionRoster/orderDel'); ?>/",
                                            data: {id: id},
                                            beforeSend: function () {
                                                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                                            },
                                            success: function (data) {

                                            }
                                        })
                                    counterorder--;
                                }
                                return false;
                            });
                        }
                        else
                        {
                            $("#sailorTable5 tbody").empty();
                        }



                    }

                    else {
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');
                        $("#reSeaRank").val('');
                        $("#rankSeaService").val('');
                        $("#gainValue").val('');
                        $("#maximumLimit").val('');
                        $("#SeaSer").val('');
                        $("#InsService").val('');
                        $("#RankService").val('');
                        $("#serviceLength").val('');
                        $("#sailorTable tbody").empty();
                        $("#sailorTable1 tbody").empty();
                        $("#sailorTable2 tbody").empty();
                        $("#sailorTable3 tbody").empty();
                        $("#sailorTable4 tbody").empty();
                        $("#sailorTable5 tbody").empty();
                        $("#manRecomn").val('');
                        $("#recomnForm").val('');
                        $("#recomnMax").val('');
                        $("#recomnGaintext").val('');
                        $(".alertSMS").html('Invalid Officer Number');
                    }
                    $(".smloadingImg").html("");
                    //alert($('#seaService').html(data));
                }
            });
        } else {
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');
            $("#reSeaRank").val('');
            $("#rankSeaService").val('');
            $("#gainValue").val('');
            $("#maximumLimit").val('');
            $("#SeaSer").val('');
            $("#InsService").val('');
            $("#RankService").val('');
            $("#serviceLength").val('');
            $("#sailorTable tbody").empty();
            $("#sailorTable1 tbody").empty();
            $("#sailorTable2 tbody").empty();
            $("#sailorTable3 tbody").empty();
            $("#sailorTable4 tbody").empty();
            $("#sailorTable5 tbody").empty();
            $("#manRecomn").val('');
            $("#recomnForm").val('');
            $("#recomnMax").val('');
            $("#recomnGaintext").val('');
            $(".alertSMS").html('');
        }
    });


    /*End Rank wise Search For Promotion Roster*/
</script>
<!-- START Pop UP FOR TRAINING -->
<div class="messagepop pop">
     <div class="modal-header">

            <h4 class="modal-title">Training List</h4>
    </div>
    <div class="modal-body">
        <div class="col-md-6">
            <div class="form-group">
                <select class="select2 form-control" name="branch" id="trainCode"
                        data-placeholder="Select Training" aria-hidden="true"
                        data-allow-clear="true" style="width: 200px" code-id = "">
                    <option value="">Select Training</option>
                    <?php
                    foreach ($code as $key ) {
                        ?>
                        <option value="<?php echo $key->NAVYTrainingID?>"><?php echo $key->Name?></option>
                   <?php
                    }
                    ?>

                </select>
            </div>
        </div>
    </div>

</div>
<!-- END Pop UP FOR TRAINING -->

<!-- START Pop UP FOR Exam -->
<div class="messagepop popCode">
     <div class="modal-header">

            <h4 class="modal-title">Exam List</h4>
    </div>
    <div class="modal-body">
        <div class="col-md-6">
            <div class="form-group">

                <select class="select2 form-control" name="branch" id="examCode"
                        data-placeholder="Select Exam" aria-hidden="true"
                        data-allow-clear="true" style="width: 200px" code-id = "">
                    <option value="">Select Exam</option>
                    <?php
                    foreach ($exam as $key ) {
                        ?>
                        <option value="<?php echo $key->EXAM_ID?>"><?php echo $key->NAME?></option>
                   <?php
                    }
                    ?>

                </select>
            </div>
        </div>
    </div>

</div>
<!-- END Pop UP FOR Exam -->
<!-- START Pop UP FOR Medal -->
<div class="messagepop popMedal">
    <div class="modal-header">

            <h4 class="modal-title">Medal List</h4>
    </div>
    <div class="modal-body">
        <div class="col-md-6">
            <div class="form-group">

                <select class="select2 form-control" name="branch" id="MedalCode"
                        data-placeholder="Select Medal" aria-hidden="true"
                        data-allow-clear="true" style="width: 200px" code-id = "">
                    <option value="">Select Medal</option>
                    <?php
                    foreach ($medal as $key ) {
                        ?>
                        <option value="<?php echo $key->MEDAL_ID?>"><?php echo $key->NAME?></option>
                   <?php
                    }
                    ?>

                </select>
            </div>
        </div>
    </div>

</div>

<!--  -->
<!-- END Pop UP FOR Medal -->
<!-- START Pop UP FOR Honor -->
<div class="messagepop popHonor">
    <div class="modal-header">

            <h4 class="modal-title">Honor List</h4>
    </div>
    <div class="modal-body">
        <div class="col-md-6">
            <div class="form-group">

            <select class="select2 form-control" name="branch" id="HonorCode"
                    data-placeholder="Select Honor" aria-hidden="true"
                    data-allow-clear="true" style="width: 200px" code-id = "">
                <option value="">Select Honor</option>
                <?php
                foreach ($honor as $key ) {
                    ?>
                    <option value="<?php echo $key->HONOR_ID?>"><?php echo $key->NAME?></option>
               <?php
                }
                ?>

            </select>
            </div>
        </div>
    </div>

</div>
<!-- END Pop UP FOR Honor -->
<style type="text/css">
    a.selected {
  background-color:#1F75CC;
  color:white;
  z-index:100;
}

.messagepop {
    border-radius: 7px;
    background-color:#f9f9f9;
    border:1px solid #999999;
    cursor:default;
    display:none;
    margin: -449px 0px 0px 302px;
    position:absolute;
    text-align:left;
    width:300px;
    height: 400px;
    z-index:50;*

}

</style>
