<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Current Rank  Name</th>
        <td><?php echo $viewdetails->CR_RANK ?></td>
    </tr>
    <tr>
        <th>Promotion Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->PromoDate)) ?></td>
    </tr>
    <tr>
        <th>Posting Unit</th>
        <td><?php echo $viewdetails->POSTING_UNIT_NAME ?></td>
    </tr>
    <tr>
        <th>Seniority Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->SENIORITYDATE)) ?></td>
    </tr>
    <tr>
        <th>Authority Number</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr>
    <tr>
        <th>Authority Date</th>
        <td><?php echo date("d-m-Y", strtotime($viewdetails->AuthorityDate)) ?></td>
    </tr>
    <tr>
        <th>DAO Number</th>
        <td><?php echo $viewdetails->DAONumber ?></td>
    </tr>
    <tr>
        <th>Ship/Establishment </th>
        <td><?php echo $viewdetails->SHIP_EST ?></td>
    </tr>
    <tr>
        <th>Previous Rank</th>
        <td><?php echo $viewdetails->PRE_RANK ?></td>
    </tr>
</table>