<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/readvancement/index') : site_url('promotion/readvancement/index') ?>" title="Re-advancement Information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Add Readvanced Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Sailor Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Official Number <span class="text-danger">*</span></label>        
                                    <div class="col-sm-5" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Promotion Date</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'promotionDate', "class" => "form-control promotionDate required", 'required' => 'required', 'placeholder' => 'promotion date', 'value' => set_value('PromotionDate'), 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Seniority Date</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'seniorityDate', "class" => "form-control seniorityDate required", 'required' => 'required', 'placeholder' => 'seniority date', 'value' => set_value('SeniorityDate'), 'readonly' => 'readonly')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>                                
                                <div class="form-group">
                                    <label class="col-sm-4">Posting Unit</label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                                    </div>
                                </div>
                                
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Readvanced Information </legend>
                            <div class="col-md-6">  
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 13px;">Rank From</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Branch<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion Branch">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="branchCurr" id="branchCurr" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Branch</option>
                                                <?php
                                                foreach ($branch as $row):
                                                    ?>
                                                    <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>
                                            </select>                                       
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Rank<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion rank to">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="rankCurr" id="rankCurr" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Rank</option>                                            
                                            </select>                                        
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Readvanced Date<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select current rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'reAdvancedDate', "id"=>"reAdvancedDate", 'value' => date('d-m-Y'),  "class" => "datePicker form-control required")); ?>                              
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 13px;">Readvance To </legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Branch<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion Branch">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="branchRecomm" id="branchRecomm" data-placeholder="Select Branch" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Branch</option>
                                                <?php
                                                foreach ($branch as $row):
                                                    ?>
                                                    <option value="<?php echo $row->BRANCH_ID ?>"><?php echo $row->BRANCH_NAME ?></option>
                                                <?php
                                                endforeach; 
                                                ?>
                                            </select>                                       
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Rank<span class="text-danger">*</span></label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select promotion rank to">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <select class="select2 form-control required" name="rankRecomm" id="rankRecomm" data-placeholder="Select rank" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Rank</option>                                            
                                            </select>                                        
                                        </div>
                                    </div>
                                </fieldset> 
                            </div>                            
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Readvanced Authority Information </legend>
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Authority Number <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter authority number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityDate', 'value' =>  date("d-m-Y"), "id"=>"authorityDate", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>                                             
                                
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">DAO Number <span class="text-danger">*</span></label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required" name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>"><?php echo $row->DAO_NO ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px;">Ship/Establishment <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Ship/Establishment ">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control required" name="ship_establishment" id="ship_establishment" data-placeholder="Select Ship/Establishment" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select ship/establishment</option>
                                            <?php
                                            foreach ($shipEst as $row):
                                                ?>
                                                <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"><?php echo $row->NAME ?></option>
                                            <?php
                                            endforeach; 
                                            ?>
                                        </select>                                        
                                    </div>
                                </div>
                            </div>                            
                        </fieldset>
                    </div>  

                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/ReAdvancement/save':'promotion/ReAdvancement/save' ?>" data-redirect-action="<?php echo ($flag == 1)? 'Retirement/HistoryTran/reAdvancement/index':'regularTransaction/promotion/reAdvancement/index'?>" value="Save">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(document).on("change", "#branchCurr", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankCurr').html(data);
            }
        });
    });
    $(document).on("change", "#branchRecomm", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('setup/common/rankName_by_branch'); ?>/",
            data: {branchId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankRecomm').html(data);
            }
        });
    });
</script>