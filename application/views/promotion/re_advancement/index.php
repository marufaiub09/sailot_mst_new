<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Readvancement Info Search </h3>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <div class="col-md-12">
                        <div class="col-md-10">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Official Number</label>
                                <div class="col-md-3">
                                    <?php echo form_input(array('name' => 'officialNumber', 'id' => 'officialInfo', "class" => "form-control required smloadingImg", 'required' => 'required', 'placeholder' => 'Official Number')); ?>
                                </div>
                                <div class="col-md-1"><span class="smloadingImg"></span></div>                                
                                <label class="col-md-5 control-label"> <div class="text-denger"><span class="alertSMS label label-danger" style="font-size: 89%;"></span></div><span class="fullName"></span><span class="rank"></span></label>
                            </div>
                        </div>                      
                    </div>
                </form>              
            </div>
        </div>
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Readvancement Information</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs "  href= "<?php echo ($flag == 1)? site_url('Retirement/HistoryTran/readvancement/create') : site_url('promotion/readvancement/create') ?>" title="Add Readvancement Informaion">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">
                <table id="recommInfo" class="stripe row-border order-column" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Authority NO</th>
                            <th>DAO NO</th>
                            <th>Last Rank</th>
                            <th>Readvanced Rank</th>
                            <th>Readvanced Date</th>
                            <th>Action</th>                            
                        </tr>
                    </thead>
                    <tbody class="contentArea">  

                    </tbody>
                </table>             
            </div>
        </div>
    </div>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }
 
    div.container {
        width: 80%;
    }
</style>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        
        $('#recommInfo').removeAttr('width').DataTable( {  
            "bPaginate": false,
            "bFilter": false, 
            "bInfo": false,
            "columnDefs": [
                { width: '20px', targets: 0 },
                { width: '100px', targets: 1 },
                { width: '100px', targets: 2 },
                { width: '80px', targets: 3 },
                { width: '100px', targets: 4 },
                { width: '90px', targets: 5 },
                { width: '100px', targets: 6 },
            ],
            "fixedColumns": true,
            "ordering": false

        } );
        $('.dataTables_empty').empty();
    });
    /*start search sailorInfo, rank_name by Official Number */
    $("#officialInfo").on('blur', function(){
        var officeNumber = $(this).val();
        var flag = "<?php echo $flag; ?>";
        var sailorStatus = (flag == 0 ? '1' : '3'); 
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus: sailorStatus},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailor",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").text(data['SAILORID']);
                        $(".fullName").text(data['FULLNAME']);
                        $(".rank").text(', Rank: '+data['RANK_NAME']);  
                        $(".alertSMS").html('');
                        
                        $.ajax({
                            type: "post",
                            data: {sailorId: data['SAILORID'], sailorStatus:1},
                            dataType: "json",
                            url: "<?php echo site_url(); ?>promotion/reAdvancement/searchReAdvancementInfo",
                            beforeSend: function () {
                                $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                            },
                            success: function (data) {
                                $(".smloadingImg").html("");
                                var TableRow;
                                var sn = 1;
                                $.each(data, function(index, recomm) {

                                    var view = '<?php echo site_url("promotion/ReAdvancement/view")?>/'+recomm.PromotionID;
                                    var edit = '<?php echo site_url("promotion/ReAdvancement/edit")?>/'+recomm.PromotionID;                                    
                                    var link = '<a class="btn btn-success btn-xs modalLink" href="'+view+'" title="View Readvancement info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ';                                    
                                    var link2 = '<a class="btn btn-warning btn-xs" href="'+edit+'" title="Edit Readvancement Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ';
                                    var link3 = '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'+recomm.PromotionID+'" sn="'+sn+'" title="Click For Delete" data-type="delete" data-field="PromotionID" data-tbl="promotion"><span class="glyphicon glyphicon-trash"></span></a>';
                                    TableRow += "<tr id =row_"+sn+">";
                                    TableRow += "<td>" + sn++ + "</td>"+
                                                "<td>" + recomm.AuthorityNumber + "</td>"+
                                                "<td>" + recomm.DAONumber + "</td>"+
                                                "<td>" + recomm.CR_RANK + "</td>"+
                                                "<td>" + recomm.LAST_RANK + "</td>"+
                                                "<td>" + recomm.PromotionDate + "</td>"+
                                                "<td>" + link + link2 + link3+ "</td>";
                                    TableRow += "</tr>";
                                    
                                });
                                $(".contentArea").html(TableRow);
                            }
                        });             
                    }else{
                        $(".sailorId").text('');
                        $(".fullName").text('');
                        $(".rank").text('');      
                        $(".alertSMS").html('Invalid Official Number');  
                        $("#recommInfo tbody tr").empty();    
                    }
                }
            });            
        }else{
            $(".sailorId").text('');
            $(".fullName").text('');
            $(".rank").text(''); 
            $(".alertSMS").html(''); 
            $("#recommInfo tbody tr").empty();    
        }     
       
    });
    /*end sailor details*/
</script>
