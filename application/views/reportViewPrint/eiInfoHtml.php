<script type="text/javascript">
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',
            // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row),
                            $cols = $row.find('td');

                        return $cols.map(function (j, col) {
                            var $col = $(col),
                                text = $col.text();

                            return text.replace(/"/g, '""'); // escape double quotes

                        }).get().join(tmpColDelim);

                    }).get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim) + '"',
            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // CSV
            exportTableToCSV.apply(this, [$('#datatable'), 'export.csv']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>
<a href="#" class="export">
    <button>CSV/EXCEL</button>
</a>
<!--<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php /*echo base_url('dist/img/navy_logo2.jpg'); */ ?>"/>
    <h5 style="font-size: <?php /*echo $_POST['fontSize'] */ ?>">Sailor Management System.</h5>

</div>-->


<div style=" text-align: center;"><p style=" text-align: right;"></p>

    <span style=" text-align: center"><b>Engagement Information</b></span>
</div>
<br/><br/>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>
<hr>

<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>SN</th>
        <th>Engage Date</th>
        <th>Duration</th>
        <th>Engage For</th>
        <th>Expiary Date</th>
        <th>Eng Type</th>
        <th>Engagement Ship</th>
        <th>DAO</th>

    </tr>
    </thead>

    <tbody>
    <?php
    $sn = 1;
    $temp = 0;
    foreach ($abs as $row) {
        ?>
        <tr>
            <td> <?php echo $sn++; ?> </td>
            <td><?php echo $row->EngageDate; ?></td>
            <td><?php
                if (strlen($row->EngagementYear == 1)) {
                    $year = "0" . $row->EngagementYear;
                } else {
                    $year = $row->EngagementYear;
                }
                if (strlen($row->EngagementMonth) == 1) {
                    $month = "0" . $row->EngagementMonth;
                } else {
                    $month = $row->EngagementMonth;
                }
                if (strlen($row->EngagementDay) == 1) {
                    $day = "0" . $row->EngagementDay;
                } else {
                    $day = $row->EngagementDay;
                }

                echo $year . $month . $day;
                $engagementFor = (int)$year . $month . $day;
                ?></td>
            <td><?php
                echo $engNext = $engagementFor + $temp;
                $temp = $engNext;
                ?></td>
            <td><?php echo $row->ExpiryDate; ?></td>

            <td><?php
                if($row->EngagementType == '1')
                {
                    echo 'Normal';
                }
                if($row->EngagementType == '2')
                {
                    echo 'Extension';
                }
                if($row->EngagementType == '3')
                {
                    echo 'Compulsary Extension';
                }
                if($row->EngagementType == '4')
                {
                    echo 'Punishment Extension';
                }
                if($row->EngagementType == '5')
                {
                    echo 'Hospital Extension';
                }
                ?></td>
            <td><?php echo $row->NAME; ?></td>
            <td><?php echo $row->DAONo; ?></td>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>
