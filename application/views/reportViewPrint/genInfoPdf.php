<?php
error_reporting('0');
header("Content-type: text/css");
$white = '#fff';
$dkgray = '#333';
$dkgreen = '#008400';
?>
<style>

    body {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        margin-top: <?php echo $_POST['topMargin'] ?>;
        margin-bottom: <?php echo $_POST['bottomMargin'] ?>;
        margin-left: <?php echo $_POST['leftMargin'] ?>;
        margin-right: <?php echo $_POST['rightMargin'] ?>;

    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table td, .table th {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        border: 1px <?php echo $_POST['borderType'] ?> <?php echo $_POST['borderColour'] ?>;
    }

    table.table-condensed {
        border: 1px solid <?php echo $dkgreen ?>;
    }
</style>

<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php echo base_url('dist/img/navy_logo2.jpg'); ?>"/>
    <h5 style="font-size: <?php echo $_POST['fontSize'] ?>">Sailor Management System.</h5>

</div>


<h1 style=" text-align: center; font:12px;"><u>Sailor Information</u></h1>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>

<hr>

<h3><u>Genaral Information</u></h3>
<div class="content row-fluid">
    <div class="span5 " style="width:34%; float: left;">
        <div class="widget">
            <table class="" style=" float: right;">
                <?php
                foreach ($abs as $row)
                {
                ?>

                <tr>
                    <td style="color: black;"><b>Dt of Birth</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->BIRTHDATE; ?></td>
                </tr>
                <tr>
                    <td><b>Dt of Engagement</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->EngagementDate; ?></td>
                </tr>
                <tr>
                    <td><b>Dt of Promotion</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->PROMOTIONDATE; ?></td>
                </tr>
                <tr>
                    <td><b>Present Billet</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;sdsddsdsdssddsddswdsdsd</td>
                </tr>

                <tr>
                    <td><b>Education at Entry</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->Name; ?></td>
                </tr>
                <tr>
                    <td><b>Marital Status</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php $mar = $row->MARITALSTATUS;
                        if ($mar == '0') {
                            echo 'Unmarried';
                        } elseif ($mar == '1') {
                            echo 'Married';
                        } elseif ($mar == '2') {
                            echo 'Separation';
                        } elseif ($mar == '3') {
                            echo 'Divorce';
                        } else {
                            echo 'Widower';
                        }
                        ?></td>
                </tr>
                <tr>
                    <td><b>Weight</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->WEIGHT; ?>Kg</td>

                </tr>
                <tr>
                    <td><b>Complexion</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->COMPLEXION; ?></td>

                </tr>
                <tr>
                    <td><b>Father's Name</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->FATHERNAME ?></td>
                </tr>
                <tr>
                    <td><b>Spouse Name</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->SpouseName; ?></td>
                </tr>
                <tr>
                    <td><b>Next of Kin</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->NextOfKin; ?></td>
                </tr>
                <tr>
                    <td><b>ID Marks</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->IDMARKS; ?></td>
                </tr>
                <tr>
                    <td><b>Permanent Address</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->PERMANENTADDRESS; ?></td>
                </tr>
                <tr>
                    <td><b>Remarks</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->REMARKS; ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="span5 " style="width:33%; float: left;">
        <div class="widget">
            <table class="" style=" float: right;">
                <tr>
                    <td><b>Dt of Entry</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->ENTRYDATE; ?></td>
                </tr>
                <tr>
                    <td style="width: 60%"><b>Engagement Expiry</b></td>
                    <td style="width: 60%">:&nbsp;&nbsp;&nbsp;<?php echo $row->EngagementExpiry; ?></td>
                </tr>
                <tr>
                    <td><b>Dt of Seniority</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->SENIORITYDATE; ?></td>
                </tr>
                <tr>
                    <td><b>Dt of Posting</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->POSTINGDATE; ?></td>
                </tr>

                <tr>
                    <td><b>Highest Education</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->Name ?></td>
                </tr>
                <tr>
                    <td><b>Medical Category</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->MEDICALCATEGORY; ?></td>
                </tr>
                <tr>
                    <td><b>Height</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->HEIGHT; ?></td>

                </tr>
                <tr>
                    <td><b>Blood Group</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->BLOODGROUP; ?></td>

                </tr>
                <tr>
                    <td><b>Mother's Name</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->MOTHERNAME; ?></td>
                </tr>
                <tr>
                    <td><b>Dt of Marriage</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->MARRIAGEDATE ?></td>
                </tr>
                <tr>
                    <td><b>Relation</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->RELATIONID; ?></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="span5 " style="width:33%; float: left;">
        <div class="widget">
            <table class="" style=" float: right; margin-top: 120px; ">
                <tr>
                    <td><b>Chest</b></td>
                    <td style="text-align: left;">:&nbsp;&nbsp;&nbsp;<?php echo $row->CHEST; ?>CM</td>
                </tr>
                <tr>
                    <td width="60%;" style=" "><b>Eye Sight</b></td>
                    <td>:&nbsp;&nbsp;&nbsp;<?php echo $row->EYESIGHT; ?></td>
                </tr>
                <?php
                }
                ?>
            </table>
        </div>
    </div>

</div>

<div style=" margin-left: 100%;"><h2 style=" text-align: center;"></h2>
    <hr>
</div>
