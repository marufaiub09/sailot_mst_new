<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2016
 * Time: 2:17 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2016
 * Time: 12:04 PM
 */
?>
<?php
header("Content-type: text/css");
$white = '#fff';
$dkgray = '#333';
$dkgreen = '#008400';
?>
<style>

    body {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        margin-top: <?php echo $_POST['topMargin'] ?>;
        margin-bottom: <?php echo $_POST['bottomMargin'] ?>;
        margin-left: <?php echo $_POST['leftMargin'] ?>;
        margin-right: <?php echo $_POST['rightMargin'] ?>;

    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table td, .table th {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        border: 1px <?php echo $_POST['borderType'] ?> <?php echo $_POST['borderColour'] ?>;
    }

    table.table-condensed {
        border: 1px solid <?php echo $dkgreen ?>;
    }
</style>


<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php echo base_url('dist/img/navy_logo2.jpg'); ?>"/>
    <h5 style="font-size: <?php echo $_POST['fontSize'] ?>">Sailor Management System.</h5>
    <span style=" text-align: center"><b>Privilege Leave Information</b></span>
</div>
<br/><br/>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>
<hr>


<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>SN</th>
        <th>Leave Year</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Total Days</th>
        <th>Leave Category</th>
        <th>Grant Authority</th>


    </tr>
    </thead>

    <tbody>
    <?php
    $sn = 1;
    $temp = 0;
    foreach ($abs as $row) {
        ?>
        <tr>
            <td><?php echo $sn++; ?></td>
            <td><?php echo $row->LeaveYear; ?></td>
            <td><?php echo $row->StartDate; ?></td>
            <td><?php echo $row->EndDate;?></td>
            <td><?php echo $row->LeaveDays; ?></td>

            <td><?php
                if($row->LeaveCategory == 0)
                {
                    echo 'None';
                }
                if($row->LeaveCategory == 1)
                {
                    echo 'Normal';
                }
                if($row->LeaveCategory == 2)
                {
                    echo 'Advance';
                }
                if($row->LeaveCategory == 3)
                {
                    echo 'Extension';
                }


                ?></td>
            <td><?php echo $row->NAME; ?></td>


        </tr>
        <?php

    }
    ?>

    </tbody>
    <tfoot>
    <tr>
        <th>Total Leave Availed:</th>
        <th><?php echo $row->LeaveAvailed;?></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>

    </tr>

    </tfoot>
</table>

