<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2016
 * Time: 12:43 PM
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2016
 * Time: 12:04 PM
 */
?>
<?php
header("Content-type: text/css");
$white = '#fff';
$dkgray = '#333';
$dkgreen = '#008400';
?>
<style>

    body {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        margin-top: <?php echo $_POST['topMargin'] ?>;
        margin-bottom: <?php echo $_POST['bottomMargin'] ?>;
        margin-left: <?php echo $_POST['leftMargin'] ?>;
        margin-right: <?php echo $_POST['rightMargin'] ?>;

    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table td, .table th {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        border: 1px <?php echo $_POST['borderType'] ?> <?php echo $_POST['borderColour'] ?>;
    }

    table.table-condensed {
        border: 1px solid <?php echo $dkgreen ?>;
    }
</style>


<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php echo base_url('dist/img/navy_logo2.jpg'); ?>"/>
    <h5 style="font-size: <?php echo $_POST['fontSize'] ?>">Sailor Management System.</h5>
    <span style=" text-align: center"><b>Assessment Information</b></span>
</div>
<br/><br/>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>
<hr>


<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>SN</th>
        <th>Assessment Year</th>
        <th>Character</th>
        <th>Efficiency</th>
        <th>Assessment Ship</th>


    </tr>
    </thead>

    <tbody>
    <?php
    $sn = 1;
    foreach ($abs as $row) {
        ?>
        <tr>
            <td><?php echo $sn++; ?></td>
            <td><?php echo $row->AssessYear; ?></td>

            <td><?php
                if($row->CharacterType == 0)
                {
                    echo 'VG';
                }
                if($row->CharacterType == 1)
                {
                    echo 'VG_';
                }
                if($row->CharacterType == 2)
                {
                    echo 'GOOD';
                }
                if($row->CharacterType == 3)
                {
                    echo 'FAIR';
                }
                if($row->CharacterType == 4)
                {
                    echo 'INDIF';
                }
                if($row->CharacterType == 5)
                {
                    echo 'BAD';
                }
                ?></td>

            <td><?php
                if($row->EfficiencyType == 0)
                {
                    echo 'SUPER';
                }
                if($row->EfficiencyType == 1)
                {
                    echo 'SAT';
                }
                if($row->EfficiencyType == 2)
                {
                    echo 'MOD';
                }
                if($row->EfficiencyType == 3)
                {
                    echo 'INFER';
                }
                if($row->EfficiencyType == 4)
                {
                    echo 'UT';
                }

                ?></td>
            <td><?php echo $row->SHIP_ESTABLISHMENT; ?></td>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>

