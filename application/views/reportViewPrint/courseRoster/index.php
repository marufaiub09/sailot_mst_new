<!-- Link list items -->
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>

<div class="row">
    <form class="form-horizontal frmContent" id="MainForm" method="post">
        <div class="col-md-12 panel panel-base" id="tabs">
            <div class="panel-heading">
                <div class="row">                    
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Prepare Course Roster.</h3>
                    </div>
                </div>
            </div>
            <div class="col-md-7 panel-body">                
                <div class="col-md-6">
                    <div class="form-group col-md-12">
                        <label for="TRAINING_TYPE_ID">Training Type <span class="text-danger"> * </span></label>
                        <select class="select2 form-control TRAINING_TYPE_ID required" name="TRAINING_TYPE_ID" id="TRAINING_TYPE_ID" data-placeholder="Select Training type" aria-hidden="true" data-allow-clear="true">
                            <option value="">Select One</option>
                            <?php
                            foreach ($trainingType as $row):
                                ?>
                                <option value="<?php echo $row->NAVYTrainingID ?>"><?php echo "[ ".$row->Code." ] ".$row->Name ?></option>
                                <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>                          
                <div class="col-md-6">
                    <div class="form-group col-sm-12">
                        <label>Course Name <span class="text-danger"> * </span></label>
                        <select class="select2 form-control required courseName" name="COURSE_NAME" id="courseName" data-placeholder="Select Course Name" aria-hidden="true" data-allow-clear="true">
                            <option value="">Select Course Name</option>
                        </select>
                    </div>
                </div>   
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">&nbsp; Filter For Only Excluding</legend>             
                    <div class="tabs">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="active "><a href="form-layouts-tabbed.html#tab-first" role="tab" data-toggle="tab">Basic Type</a></li>
                            <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Med Cat & Opinion</a></li>
                            <li><a href="form-layouts-tabbed.html#tab-third" role="tab" data-toggle="tab">BranchRank & Navy Admin Hierarchy</a></li>
                        </ul> 
                        <div class="tab-content">
                            <div class="tab-pane active col-md-12" id="tab-first">
                                <div class="col-md-6">
                                    <h5>Appointment Type</h5>
                                    <table id="ReportTable"  class="table table-striped table-bordered hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th>CODE</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($appointType as $type) { ?>
                                                <tr>

                                                    <td><input type="checkbox" class="check"  value="<?php echo $type->APPOINT_TYPEID; ?>" name="partII[]"> <?php echo $type->CODE; ?></td>

                                                    <td>
                                                       <?php echo $type->NAME; ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-6">
                                    <h5>Engagement Type</h5>
                                    <table id="ReportTable"  class="table table-striped table-bordered hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Type Name</th>                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr> <td><input type="checkbox" class="check"  value="1" name="engagementType[]"> Normal</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="2" name="engagementType[]"> Extension</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="3" name="engagementType[]"> Compulsary Extension</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="4" name="engagementType[]"> Punishment Extension</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="5" name="engagementType[]"> Hospital Extension</td> </tr>
                                        </tbody>
                                    </table>                                
                                </div>

                            </div>
                            <div class="tab-pane col-md-12" id="tab-second">
                                <div class="col-md-5">
                                    <h5>Not Willing In</h5>
                                    <table id="ReportTable"  class="table table-striped table-bordered hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th>CODE</th>
                                                <th>Name</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($mission as $miss) { ?>
                                                <tr>

                                                    <td width="20%"><input type="checkbox" class="check"  value="<?php echo $miss->MISSION_ID; ?>" name="partII[]"> <?php echo $miss->CODE; ?></td>

                                                    <td>
                                                       <?php echo $miss->NAME; ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-4">
                                    <h5>Medical Category</h5>
                                    <?php //$medicalCat = array("" =>"Select One","1" =>"A (AYE)", "2" =>"B (BEE)", "3" =>"C (CEE)", "4" =>"D (DEE)", "5" =>"E (EEE)"); ?>
                                    <table class="table" style="margin-bottom: 0px ! important;"><tr><th> Category</th></tr></table>
                                    <table id="ReportTable"  class="table table-striped table-bordered hover noHeader" width="100%">
                                        <tbody>
                                            <tr> <td><input type="checkbox" class="check"  value="1" name="medCategory[]"> A (AYE)</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="2" name="medCategory[]"> B (BEE)</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="3" name="medCategory[]"> C (CEE)</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="4" name="medCategory[]"> D (DEE)</td> </tr>
                                            <tr> <td><input type="checkbox" class="check"  value="5" name="medCategory[]"> E (EEE)</td> </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <h5></h5><br><br><br>
                                    <div><input type="checkbox" class="check"  value="1" name="foreignPunishment[]"> Foreign Visit</div>
                                    <div><input type="checkbox" class="check"  value="2" name="foreignPunishment[]"> Punishment</div>
                                    
                                </div>
                            </div>
                            <div class="tab-pane col-md-12" id="tab-third">
                            </div>
                        </div>                    
                    </div>
                    <input type="button" class="pull-right btn btn-primary btn-sm"value="Filter">
                </fieldset>
            </div>
            <div class="col-md-5 panel-body">
                <div class="col-sm-5">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">Calculation Base</legend>
                        <div class="from-group">
                            <input name="navi" id="navi" type="radio" checked="che" value="Navy"> Seniority </br>
                            <input name="navi" id="navi" type="radio" value="ISMODC"> Point
                        </div>
                    </fieldset>
                </div>
                <div class="col-sm-7">
                    <div class="form-group">
                        <label for="processDate" class="col-sm-6 control-label" style="padding-right: 0px;">Process Date</label>
                        <div class="col-sm-6" >
                            <input type="text" value="<?php echo date('Y-m-d'); ?>" name="processDate" id="processDate"  class="form-control">
                        </div>
                    </div>
                    <input type="button" class="btn btn-primary btn-sm" value="Process">
                    <input type="button" class="btn btn-warning btn-sm" value="Clear">
                </div>
                <div class="col-sm-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">&nbsp;Reports</legend>
                        <!-- <table class="table" style="margin-bottom: 0px ! important;"><tr><th>CODE</th><th>NAME</th></tr></table> -->
                        <table id="courseReportTable"  class="table table-striped table-bordered hover" width="100%">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Title</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($rosterReport as $report) { ?>
                                    <tr>
                                        <td><input type="checkbox" name="rosterReport" class="courseRoster" value="<?php echo $report->ReportSetupID; ?>"></td>
                                        <td><?php echo $report->Name; ?></td>
                                        <td><?php echo $report->Title; ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </fieldset>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="row">
    <table id="selected_pc" class="table table-striped table-bordered " width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>Official No</th>
                <th>Name</th>
                <th>Rank</th>
                <th>Seni. in Rank</th>
                <th>Red Recom</th>
                <th>Seni. With Red Recom</th>
                <th>Egg. Expire</th>
                <th>Borne</th>
                <th>Remarks</th>
                <th>Total Point</th>
            </tr>
        </thead>
        <tbody id="fiExit" class="contentArea">
        </tbody>
    </table>

</div>
<style>

    #courseReportTable div.dataTables_wrapper {
        width: 400px;
        margin: 0 auto;
    }
    .tab-pane{
        padding-top: 10px !important;
    }
    /* .dataTables_scrollHead {
        width: 100% 
    } */
    legend {
         margin-bottom: 0px !important;
    }
    fieldset.scheduler-border {
        border: 1px groove #ddd !important;
        padding: 1em 1em 1em !important;
        margin: 0 0 1.5em 0 !important;
        -webkit-box-shadow:  0px 0px 0px 0px #000;
                box-shadow:  0px 0px 0px 0px #000;
    }

    legend.scheduler-border {
        font-size: 15px !important;
        /* font-weight: bold !important; */
        text-align: left !important;

    }
    
    .noHeader thead { display:none;}
</style>
<script>
    $('#selected_pc').removeAttr('width').DataTable({
        "scrollX": true,           
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "fixedColumns": true,
        "columnDefs": [
            {width: '80px', targets: 0},
            {width: '80px', targets: 1},
            {width: '60px', targets: 2},
            {width: '80px', targets: 3},
            {width: '80px', targets: 4},                
            {width: '150px', targets: 5},
            {width: '70px', targets: 6},              
            {width: '40px', targets: 7},
            {width: '50px', targets: 8},
            {width: '60px', targets: 9},
            
        ],
    }); 
    $('#courseBranch').DataTable({
        "bPaginate": false,
        "bFilter": false,
        "ordering": false,
        "bInfo": false,
    });
    $('#examTable').DataTable({
        //"scrollY": '3vh',
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {"sZeroRecords": "",
            "sEmptyTable": ""},
        "columnDefs": [
            {width: '100%', targets: 1}
        ]
    });

    $('#trainingTable').DataTable({
        //"scrollY": '3vh',
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "oLanguage": {"sZeroRecords": "",
            "sEmptyTable": ""},
        "columnDefs": [
            {width: '100%', targets: 1}
        ]
    });
    $('#ReportTable').DataTable({
        "bPaginate": false,
        "ordering": false,
        "bFilter": false,
        "fixedColumns": true,
        "bInfo": false,
        "scrollY": '200px',
        "bScrollCollapse": true,
        "sScrollXInner": "100%", 
        "scrollX": true,
        "scrollCollapse": true,
        "paging": false,
        //"fixedHeader": true,
        fixedHeader: true,
        "columnDefs": [
            {width: '90px', targets: 0},
            {width: '200px', targets: 1},
        ],
        "responsive": true,
    });
    $('#courseReportTable').removeAttr('width').DataTable({
        "scrollY": '276px',
        "scrollX": true,           
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "fixedColumns": true,
        "columnDefs": [
            {width: '5px', targets: 0},
            {width: '120px', targets: 1}
        ]
    });


    $("#checkAll").click(function () {
        $(".check").prop('checked', $(this).prop('checked'));

    });

    $(document).on('change', '#Branch_ID', function (event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>report/RpTrainCourSet/rankByBranch',
            type: 'POST',
            dataType: 'html',
            data: {branch_id: id},
            beforeSend: function () {
                $(".training_dropdown").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#rankName').html(data);
                $('#rankName').select2('val', '');
            }
        });

    });

    

    $(document).on("click", "#trainingCourseAdd", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                //alert(label + " Is Empty");
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
                //return false;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm("Are You Sure?")) {
                var frmContent = $(".frmContent").serialize();
                console.log(frmContent);
                $.ajax({
                    type: "post",
                    data: frmContent,
                    url: "<?php echo site_url(); ?>report/RpTrainCourSet/courseDataSave",
                    beforeSend: function () {
                        $(".loadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".loadingImg").html("");
                        $(".frmMsg").html(data);
                        
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });
    $(document).on("click", "#trainingCourseSave", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                //alert(label + " Is Empty");
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
                //return false;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm("Are You Sure?")) {
                var frmContent = $(".frmContent").serialize();
                console.log(frmContent);
                $.ajax({
                    type: "post",
                    data: frmContent,
                    url: "<?php echo site_url(); ?>report/RpTrainCourSet/courseDataSave",
                    beforeSend: function () {
                        $(".loadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".loadingImg").html("");
                        $(".frmMsg").html(data);
                        
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });

    $(document).on('change', '#TRAINING_TYPE_ID', function (event) {
        event.preventDefault();
        var type_id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>report/RpTrainCourSet/course_by_trainingType',
            type: 'POST',
            dataType: 'html',
            data: {TRAINING_TYPE: type_id},
            success: function (data) {
                $('.courseName').html(data);
            }
        });
    });

    $(document).on('change', '#courseName', function(event) {
        event.preventDefault();
        var courseId = $(this).val();
        if(courseId != ''){
            $.ajax({
                url: '<?php echo base_url() ?>report/rpTrainCourSet/searchCourseRoster',
                type: 'POST',
                dataType: 'json',
                data: {CourseId: courseId},
                success: function(data){
                    var branchId = 0;
                    var temp = 1;
                    var row;
                    if(data != ''){
                        $('#courseBranch tbody tr').empty();
                        $.each(data, function(key, value){
                            branchId = value.BRANCH_ID;
                            if(branchId == temp){
                                $('#courseBranch tbody tr td:last').append(", " + value.RANK_NAME);
                            }else{
                                row =   '<tr>' +
                                        '<td>' + value.BRANCH_NAME +'</td>'+
                                        '<td>' + value.RANK_NAME +'</td>'+
                                        '</tr>';                                
                                $('#courseBranch tbody').before().append(row);
                            }
                            temp = branchId;        
                            //alert(key + ": "+ value.CourseRosterBranchID  + ", "+ value.BRANCH_NAME +", "+ value.RANK_NAME);
                        });
                    }
                }
            });            
        }

    });

</script>
