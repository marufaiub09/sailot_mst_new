<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2016
 * Time: 12:49 PM
 */
?>
<script type="text/javascript">
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',
            // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row),
                            $cols = $row.find('td');

                        return $cols.map(function (j, col) {
                            var $col = $(col),
                                text = $col.text();

                            return text.replace(/"/g, '""'); // escape double quotes

                        }).get().join(tmpColDelim);

                    }).get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim) + '"',
            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // CSV
            exportTableToCSV.apply(this, [$('#datatable'), 'export.csv']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>
<a href="#" class="export">
    <button>CSV/EXCEL</button>
</a>
<!--<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php /*echo base_url('dist/img/navy_logo2.jpg'); */?>"/>
    <h5 style="font-size: <?php /*echo $_POST['fontSize'] */?>">Sailor Management System.</h5>

</div>-->


<div style=" text-align: center;"><p style=" text-align: right;"></p>

    <span style=" text-align: center"><b>Privilege Leave Information</b></span>
</div>
<br/><br/>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>
<hr>

<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>SN</th>
        <th>Leave Year</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Total Days</th>
        <th>Leave Category</th>
        <th>Grant Authority</th>


    </tr>
    </thead>

    <tbody>
    <?php
    $sn = 1;
    $temp = 0;
    foreach ($abs as $row) {
        ?>
        <tr>
            <td><?php echo $sn++; ?></td>
            <td><?php echo $row->LeaveYear; ?></td>
            <td><?php echo $row->StartDate; ?></td>
            <td><?php echo $row->EndDate;?></td>
            <td><?php echo $row->LeaveDays; ?></td>

            <td><?php
                if($row->LeaveCategory == 0)
                {
                    echo 'None';
                }
                if($row->LeaveCategory == 1)
                {
                    echo 'Normal';
                }
                if($row->LeaveCategory == 2)
                {
                    echo 'Advance';
                }
                if($row->LeaveCategory == 3)
                {
                    echo 'Extension';
                }


                ?></td>
            <td><?php echo $row->NAME; ?></td>


        </tr>
        <?php

    }
    ?>

    </tbody>
    <tfoot>
    <tr>
         <th>Total Leave Availed:</th>
        <th><?php echo $row->LeaveAvailed;?></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>

    </tr>

    </tfoot>
</table>

