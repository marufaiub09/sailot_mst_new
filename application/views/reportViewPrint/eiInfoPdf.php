<?php
header("Content-type: text/css");
$white = '#fff';
$dkgray = '#333';
$dkgreen = '#008400';
?>
<style>

    body {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        margin-top: <?php echo $_POST['topMargin'] ?>;
        margin-bottom: <?php echo $_POST['bottomMargin'] ?>;
        margin-left: <?php echo $_POST['leftMargin'] ?>;
        margin-right: <?php echo $_POST['rightMargin'] ?>;

    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    .table td, .table th {
        font-family: <?php echo $_POST['fontType']; ?>;
        font-size: <?php echo $_POST['fontSize']; ?>;
        border: 1px <?php echo $_POST['borderType'] ?> <?php echo $_POST['borderColour'] ?>;
    }

    table.table-condensed {
        border: 1px solid <?php echo $dkgreen ?>;
    }
</style>


<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php echo base_url('dist/img/navy_logo2.jpg'); ?>"/>
    <h5 style="font-size: <?php echo $_POST['fontSize'] ?>">Sailor Management System.</h5>
    <span style=" text-align: center"><b>Engagement Information</b></span>
</div>
<br/><br/>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>
<hr>


<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>SN</th>
        <th>Engage Date</th>
        <th>Duration</th>
        <th>Engage For</th>
        <th>Expiary Date</th>
        <th>Eng Type</th>
        <th>Engagement Ship</th>
        <th>DAO</th>

    </tr>
    </thead>

    <tbody>
    <?php
    $sn = 1;
    $temp = 0;
    foreach ($abs as $row) {
        ?>
        <tr>
            <td> <?php echo $sn++; ?> </td>
            <td><?php echo $row->EngageDate; ?></td>
            <td><?php
                if (strlen($row->EngagementYear == 1)) {
                    $year = "0" . $row->EngagementYear;
                } else {
                    $year = $row->EngagementYear;
                }
                if (strlen($row->EngagementMonth) == 1) {
                    $month = "0" . $row->EngagementMonth;
                } else {
                    $month = $row->EngagementMonth;
                }
                if (strlen($row->EngagementDay) == 1) {
                    $day = "0" . $row->EngagementDay;
                } else {
                    $day = $row->EngagementDay;
                }

                echo $year . $month . $day;
                $engagementFor = (int)$year . $month . $day;
                ?></td>
            <td><?php
                echo $engNext = $engagementFor + $temp;
                $temp = $engNext;
                ?></td>
            <td><?php echo $row->ExpiryDate; ?></td>

            <td><?php
                if ($row->EngagementType == '1') {
                    echo 'Normal';
                }
                if ($row->EngagementType == '2') {
                    echo 'Extension';
                }
                if ($row->EngagementType == '3') {
                    echo 'Compulsary Extension';
                }
                if ($row->EngagementType == '4') {
                    echo 'Punishment Extension';
                }
                if ($row->EngagementType == '5') {
                    echo 'Hospital Extension';
                }
                ?></td>
            <td><?php echo $row->NAME; ?></td>
            <td><?php echo $row->DAONo; ?></td>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>