<!-- START: For multiple Submit button in one Form -->
<script type="text/javascript">
    function submitForm(action)
    {
        document.getElementById('form1').action = action;
        document.getElementById('form1').submit();
    }
</script>
<!--END:For multiple Submit button in one Form -->

<form action="SailorProfile/createView" id="form1" method="post" enctype="multipart/form-data">
<table id="" class="table table-striped table-bordered "  width="50%" cellspacing="0">
    <thead>
        <tr>
            <th style="background-color:#3b5998"><p style="color: #FFFFFF;">Search Sailor</p></th>

</tr>
</thead>

<tbody>
    <tr>
        <td class="col-sm-12">
            <div class="form-group">


                <fieldset class="">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Official Number</label>        
                            <div class="col-sm-6" >
                                <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                            </div>
                            <div class="col-sm-1">
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                            <div class="col-md-12" >
                                <div class="col-sm-4"></div>
                                <div class="col-sm-8 danger">
                                    <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                </div>
                            </div>
                        </div>
                        <br/><br/>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Posting Unit</label>
                            <div class="col-sm-6">
                                <?php echo form_input(array('name' => 'PostingUnit', "class" => "form-control PostingUnit required", 'required' => 'required', 'placeholder' => 'Posting Unit', 'readonly' => 'readonly', 'value' => set_value('PostingUnit'))); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Details</label>
                            <div class="col-sm-4">
                                <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                            </div>
                            <div class="col-sm-4">
                                <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                            </div>
                        </div>
                        <br/><br/>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Posting Date</label>
                            <div class="col-sm-4">
                                <?php echo form_input(array('name' => 'PostingDate', "class" => "form-control PostingDate required", 'required' => 'required', 'placeholder' => 'Posting Date', 'readonly' => 'readonly')); ?>
                            </div>
                        </div>
                    </div>
                </fieldset>

            </div>
        </td>
    </tr>



</tbody>
</table>
<br/><br/>

<table id="" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th style="width: 500px;background-color:#20b2aa">Name Of Information</th>
            <th style="background-color:#20b2aa;">View/Print</th>
        </tr>
    </thead>

    <tbody>
        <tr>

            <td>

                <div class="checkbox checkbox-inline checkbox-primary"v style="overflow: scroll;height: 600px; width: 500px">
                    <input class="styled" id="html"  type="checkbox"  name="checkbox[]" value="GI">
                    <label for="ACTIVE_STATUS">General Information</label>
                    <br/><br/>
                    <input class="styled" id="pdf"  type="checkbox"  value="MI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Movement Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="TMI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">TY Movement Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="FVI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Foreign Visit Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="SSI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Sea Service Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="ISI">
                    <label for="ACTIVE_STATUS">Instructor Service Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="EI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Engagement Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="TCI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Training/Course Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="SI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Specialization Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="EXI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Examination Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="PI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Promotion Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="RI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Recommendation Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="AI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Assessment Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="PLI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Privilege Leave Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="RLI">
                    <label for="ACTIVE_STATUS">Recreation Leave Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="MLI">
                    <label for="ACTIVE_STATUS">Medical Leave Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="GCI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">GCB Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="JPI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Jesthata Padak Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="MDI">
                    <label for="ACTIVE_STATUS">Medal Information</label>
                    <br/><br/> <input class="styled" id="html"  type="checkbox"  value="" name="HI">
                    <label for="ACTIVE_STATUS">Honor Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="MCI">
                    <label for="ACTIVE_STATUS">Medical Category Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="OI">
                    <label for="ACTIVE_STATUS">Overweight Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="MRI" name="checkbox[]">
                    <label for="ACTIVE_STATUS">Marriage Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="CI">
                    <label for="ACTIVE_STATUS">Children Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="NI">
                    <label for="ACTIVE_STATUS">Nominee Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="ACI">
                    <label for="ACTIVE_STATUS">Academic Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="LNI">
                    <label for="ACTIVE_STATUS">Language Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="PUI">
                    <label for="ACTIVE_STATUS">Punishment Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="MKRI">
                    <label for="ACTIVE_STATUS">Mark Run Information</label>
                    <br/><br/>
                    <input class="styled" id="html"  type="checkbox"  value="" name="WI">
                    <label for="ACTIVE_STATUS">Willing/Not Willing Information</label>
                    <br/><br/>



                </div>

            </td>

            <td>
                <input type="submit" class="btn btn-primary" style="margin-left: 80px;" id="DefaultPrint"  style="" onclick="submitForm('SailorProfile/createHtmlView')" name="HTMLVIEW" Value="HTMLVIEW"/>


                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Page Size(pdf)</label>
                    <div class="col-sm-4">
                        <select class="select2 form-control" name="pageSize" id="" data-tags="true" data-placeholder="Select Page Size"  data-allow-clear="true">
                            <option value="Default">Default</option>
                            <option value="B5">B5</option>
                            <option value="B5-L">B5 Landscape</option>
                            <option value="A6">A6</option>
                            <option value="A6-L">A6 Landscape</option>
                            <option value="A5">A5</option>
                            <option value="A5-L">A5 Landscape</option>
                            <option value="A4">A4</option>
                            <option value="A4-L">A4 Landscape</option>
                            <option value="A3">A3</option>
                            <option value="A3-L">A3 Landscape</option>
                            <option value="A2">A2</option>
                            <option value="A2-L">A2 Landscape</option>
                            <option value="A1">A1</option>
                            <option value="A1-L">A1 Landscape</option>
                            <option value="US Letter">US Letter</option>
                            <option value="US Legal">US Legal</option>
                            <option value="US Ledger">US Ledger</option>
                        </select>
                    </div>
                </div>
                <br/><br/>

                <div class="form-group">
                    <label class="col-sm-4 control-label">Border Type(pdf)</label>
                    <div class="col-sm-4">
                        <select class="select2 form-control required" name="borderType" id="" data-tags="true" data-placeholder="Select Table" required="required" data-allow-clear="true">
                            <option value="solid">Default</option>
                            <option value="dotted">Dotted</option>
                            <option value="double">Double</option>
                        </select>
                    </div>
                </div>
                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Border Colour(pdf)</label>
                    <div class="col-sm-4">
                        <select class="select2 form-control required" name="borderColour" id="" data-tags="true" data-placeholder="Select Table" required="required" data-allow-clear="true">
                            <option value="white">Default</option>
                            <option value="red">RED</option>
                            <option value="orange">Orange</option>
                            <option value="yellow">Yellow</option>
                            <option value="blue">Blue</option>
                            <option value="cyan">Cyan</option>

                        </select>
                    </div>
                </div>
                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Top Margin(pdf)</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="topMargin">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </span>
                            <input type="text" name="topMargin" class="form-control input-number" value="1cm" min="0" max="100">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="topMargin">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </span>
                        </div>
                      <!-- <input class="form-control" type="text" name="topMargin" value="" placeholder="Default : 1cm"/> -->
                    </div>
                </div>
                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Bottom Margin(pdf)</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="bottomMargin">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </span>
                            <input type="text" name="bottomMargin" class="form-control input-number" value="1cm" min="0" max="100">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="bottomMargin">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </span>
                        </div>
                   <!-- <input class="form-control" type="text" name="bottomMargin" value="" placeholder="Default : 1cm"/> -->
                    </div>
                </div>
                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Right Margin(pdf)</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="rightMargin">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </span>
                            <input type="text" name="rightMargin" class="form-control input-number" value="1cm" min="0" max="100">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="rightMargin">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </span>
                        </div> 
                       <!-- <input class="form-control" type="text" name="rightMargin" value="" placeholder="Default : 1cm"/>
                                               </div> -->
                    </div>
                </div>
                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Left Margin(pdf)</label>
                    <div class="col-sm-4">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-danger btn-number"  data-type="minus" data-field="leftMargin">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </span>
                            <input type="text" name="leftMargin" class="form-control input-number" value="1cm" min="0" max="100">
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="leftMargin">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </span>
                        </div>
                      <!-- <input class="form-control" type="text" name="leftMargin" value="" placeholder="Default : 1cm"/> -->
                    </div>
                </div>
                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Font(pdf)</label>
                    <div class="col-sm-4">
                        <select class="select2 form-control required" name="fontType" id="" data-tags="true" data-placeholder="Select Font"  data-allow-clear="true">
                            <option selected="" label="" value=""></option>
                            <option label="aealarabiya" value="aealarabiya">aealarabiya</option>
                            <option label="aefurat" value="aefurat">aefurat</option>
                            <option label="cid0cs" value="cid0cs">cid0cs</option>
                            <option label="cid0ct" value="cid0ct">cid0ct</option>
                            <option label="cid0jp" value="cid0jp">cid0jp</option>
                            <option label="cid0kr" value="cid0kr">cid0kr</option>
                            <option label="comic" value="comic">comic</option>
                            <option label="courier" value="courier">courier</option>
                            <option label="courierb" value="courierb">courierb</option>
                            <option label="courierbi" value="courierbi">courierbi</option>
                            <option label="courieri" value="courieri">courieri</option>
                            <option label="dejavusans" value="dejavusans">dejavusans</option>
                            <option label="dejavusansb" value="dejavusansb">dejavusansb</option>
                            <option label="dejavusansbi" value="dejavusansbi">dejavusansbi</option>
                            <option label="dejavusanscondensed" value="dejavusanscondensed">dejavusanscondensed</option>
                            <option label="dejavusanscondensedb" value="dejavusanscondensedb">dejavusanscondensedb</option>
                            <option label="dejavusanscondensedbi" value="dejavusanscondensedbi">dejavusanscondensedbi</option>
                            <option label="dejavusanscondensedi" value="dejavusanscondensedi">dejavusanscondensedi</option>
                            <option label="dejavusansextralight" value="dejavusansextralight">dejavusansextralight</option>
                            <option label="dejavusansi" value="dejavusansi">dejavusansi</option>
                            <option label="dejavusansmono" value="dejavusansmono">dejavusansmono</option>
                            <option label="dejavusansmonob" value="dejavusansmonob">dejavusansmonob</option>
                            <option label="dejavusansmonobi" value="dejavusansmonobi">dejavusansmonobi</option>
                            <option label="dejavusansmonoi" value="dejavusansmonoi">dejavusansmonoi</option>
                            <option label="dejavuserif" value="dejavuserif">dejavuserif</option>
                            <option label="dejavuserifb" value="dejavuserifb">dejavuserifb</option>
                            <option label="dejavuserifbi" value="dejavuserifbi">dejavuserifbi</option>
                            <option label="dejavuserifcondensed" value="dejavuserifcondensed">dejavuserifcondensed</option>
                            <option label="dejavuserifcondensedb" value="dejavuserifcondensedb">dejavuserifcondensedb</option>
                            <option label="dejavuserifcondensedbi" value="dejavuserifcondensedbi">dejavuserifcondensedbi</option>
                            <option label="dejavuserifcondensedi" value="dejavuserifcondensedi">dejavuserifcondensedi</option>
                            <option label="dejavuserifi" value="dejavuserifi">dejavuserifi</option>
                            <option label="freemono" value="freemono">freemono</option>
                            <option label="freemonob" value="freemonob">freemonob</option>
                            <option label="freemonobi" value="freemonobi">freemonobi</option>
                            <option label="freemonoi" value="freemonoi">freemonoi</option>
                            <option label="freesans" value="freesans">freesans</option>
                            <option label="freesansb" value="freesansb">freesansb</option>
                            <option label="freesansbi" value="freesansbi">freesansbi</option>
                            <option label="freesansi" value="freesansi">freesansi</option>
                            <option label="freeserif" value="freeserif">freeserif</option>
                            <option label="freeserifb" value="freeserifb">freeserifb</option>
                            <option label="freeserifbi" value="freeserifbi">freeserifbi</option>
                            <option label="freeserifi" value="freeserifi">freeserifi</option>
                            <option label="helvetica" value="helvetica">helvetica</option>
                            <option label="helveticab" value="helveticab">helveticab</option>
                            <option label="helveticabi" value="helveticabi">helveticabi</option>
                            <option label="helveticai" value="helveticai">helveticai</option>
                            <option label="hysmyeongjostdmedium" value="hysmyeongjostdmedium">hysmyeongjostdmedium</option>
                            <option label="kozgopromedium" value="kozgopromedium">kozgopromedium</option>
                            <option label="kozminproregular" value="kozminproregular">kozminproregular</option>
                            <option label="msungstdlight" value="msungstdlight">msungstdlight</option>
                            <option label="pdfacourier" value="pdfacourier">pdfacourier</option>
                            <option label="pdfacourierb" value="pdfacourierb">pdfacourierb</option>
                            <option label="pdfacourierbi" value="pdfacourierbi">pdfacourierbi</option>
                            <option label="pdfacourieri" value="pdfacourieri">pdfacourieri</option>
                            <option label="pdfahelvetica" value="pdfahelvetica">pdfahelvetica</option>
                            <option label="pdfahelveticab" value="pdfahelveticab">pdfahelveticab</option>
                            <option label="pdfahelveticabi" value="pdfahelveticabi">pdfahelveticabi</option>
                            <option label="pdfahelveticai" value="pdfahelveticai">pdfahelveticai</option>
                            <option label="pdfasymbol" value="pdfasymbol">pdfasymbol</option>
                            <option label="pdfatimes" value="pdfatimes">pdfatimes</option>
                            <option label="pdfatimesb" value="pdfatimesb">pdfatimesb</option>
                            <option label="pdfatimesbi" value="pdfatimesbi">pdfatimesbi</option>
                            <option label="pdfatimesi" value="pdfatimesi">pdfatimesi</option>
                            <option label="pdfazapfdingbats" value="pdfazapfdingbats">pdfazapfdingbats</option>
                            <option label="stsongstdlight" value="stsongstdlight">stsongstdlight</option>
                            <option label="symbol" value="symbol">symbol</option>
                            <option label="times" value="times">times</option>
                            <option label="timesb" value="timesb">timesb</option>
                            <option label="timesbi" value="timesbi">timesbi</option>
                            <option label="timesi" value="timesi">timesi</option>
                            <option label="trebuchet" value="trebuchet">trebuchet</option>
                            <option label="uni2cid_ac15" value="uni2cid_ac15">uni2cid_ac15</option>
                            <option label="uni2cid_ag15" value="uni2cid_ag15">uni2cid_ag15</option>
                            <option label="uni2cid_aj16" value="uni2cid_aj16">uni2cid_aj16</option>
                            <option label="uni2cid_ak12" value="uni2cid_ak12">uni2cid_ak12</option>
                            <option label="verdana" value="verdana">verdana</option>
                            <option label="zapfdingbats" value="zapfdingbats">zapfdingbats</option>
                        </select>
                    </div>
                </div>
                <br/><br/>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Font Size(pdf)</label>
                    <div class="col-sm-4">
                        <input class="form-control" type="text" name="fontSize" value="" placeholder="Default : 11px"/>
                    </div>
                </div>
                <br/><br/>

                <input type="submit" class="btn btn-primary" id="pdfbutton"  style="margin-left: 60px" onclick="submitForm" name="Print" Value="Print"/>
                
            </td>

        </tr>



    </tbody>
</table>
</form>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    //plugin bootstrap minus and plus
    //http://jsfiddle.net/laelitenetwork/puJ6G/
    $('.btn-number').click(function (e) {
        e.preventDefault();

        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }

            }
        } else {
            input.val(0);
        }
    });

</script>
