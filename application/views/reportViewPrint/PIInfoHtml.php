<script type="text/javascript">
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',
            // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row),
                            $cols = $row.find('td');

                        return $cols.map(function (j, col) {
                            var $col = $(col),
                                text = $col.text();

                            return text.replace(/"/g, '""'); // escape double quotes

                        }).get().join(tmpColDelim);

                    }).get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim) + '"',
            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // CSV
            exportTableToCSV.apply(this, [$('#datatable'), 'export.csv']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>
<a href="#" class="export">
    <button>CSV/EXCEL</button>
</a>
<!--<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php /*echo base_url('dist/img/navy_logo2.jpg'); */?>"/>
    <h5 style="font-size: <?php /*echo $_POST['fontSize'] */?>">Sailor Management System.</h5>

</div>-->


<div style=" text-align: center;"><p style=" text-align: right;"></p>

    <span style=" text-align: center"><b>Promotion Information</b></span>
</div>
<br/><br/>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>
<hr>

<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>Sl No</th>
        <th>Previous Rank</th>
        <th>Promoted Rank</th>
        <th>Date of Promotion</th>
        <th>Date of Seniority</th>
        <th>Acting</th>
        <th>NQ</th>
        <th>DAO</th>

    </tr>
    </thead>

    <tbody>
    <?php
    $sn = 1;
    foreach ($promotion_Data as $row) {
        ?>
        <tr>
            <td><?php echo $sn++; ?></td>
            <td><?php echo $row->PRE_RANK; ?></td>
            <td><?php echo $row->CR_RANK; ?></td>
            <td><?php echo $row->PromoDate; ?></td>
            <td><?php echo $row->PromoEffectDate; ?></td>
            <td><?php echo $row->IsActing; ?></td>
            <td><?php echo $row->IsNQ; ?></td>
            <td><?php echo $row->DAOID; ?></td>

        </tr>
        <?php
    }
    ?>

    </tbody>
</table>
