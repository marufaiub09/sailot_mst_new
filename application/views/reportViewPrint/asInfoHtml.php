<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/26/2016
 * Time: 12:22 PM
 */
?>
<script type="text/javascript">
    $(document).ready(function () {

        function exportTableToCSV($table, filename) {

            var $rows = $table.find('tr:has(td)'),
            // Temporary delimiter characters unlikely to be typed by keyboard
            // This is to avoid accidentally splitting the actual contents
                tmpColDelim = String.fromCharCode(11), // vertical tab character
                tmpRowDelim = String.fromCharCode(0), // null character

            // actual delimiter characters for CSV format
                colDelim = '","',
                rowDelim = '"\r\n"',
            // Grab text from table into CSV formatted string
                csv = '"' + $rows.map(function (i, row) {
                        var $row = $(row),
                            $cols = $row.find('td');

                        return $cols.map(function (j, col) {
                            var $col = $(col),
                                text = $col.text();

                            return text.replace(/"/g, '""'); // escape double quotes

                        }).get().join(tmpColDelim);

                    }).get().join(tmpRowDelim)
                        .split(tmpRowDelim).join(rowDelim)
                        .split(tmpColDelim).join(colDelim) + '"',
            // Data URI
                csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(csv);

            $(this)
                .attr({
                    'download': filename,
                    'href': csvData,
                    'target': '_blank'
                });
        }

        // This must be a hyperlink
        $(".export").on('click', function (event) {
            // CSV
            exportTableToCSV.apply(this, [$('#datatable'), 'export.csv']);

            // IF CSV, don't do event.preventDefault() or return false
            // We actually need this to be a typical hyperlink
        });
    });
</script>
<a href="#" class="export">
    <button>CSV/EXCEL</button>
</a>
<!--<div style=" text-align: center;"><p style=" text-align: right;"></p>
    <img src="<?php /*echo base_url('dist/img/navy_logo2.jpg'); */ ?>"/>
    <h5 style="font-size: <?php /*echo $_POST['fontSize'] */ ?>">Sailor Management System.</h5>

</div>-->


<div style=" text-align: center;"><p style=" text-align: right;"></p>

    <span style=" text-align: center"><b>Assessment Information</b></span>
</div>
<br/><br/>
<b>
    <p5>O NO. :</p5>
</b>
<p6><?php echo $_POST['officialNo'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Name:</p5>
</b>
<p6><?php echo $_POST['fullName'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    <p5>Rank:</p5>
</b>
<p6><?php echo $_POST['rank'] ?></p6>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<b>
    DATE:
</b>
<p6><?php echo date("d-M-Y"); ?></p6>
<hr>

<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>SN</th>
        <th>Assessment Year</th>
        <th>Character</th>
        <th>Efficiency</th>
        <th>Assessment Ship</th>


    </tr>
    </thead>

    <tbody>
    <?php
    $sn = 1;
    foreach ($abs as $row) {
        ?>
        <tr>
            <td><?php echo $sn++; ?></td>
            <td><?php echo $row->AssessYear; ?></td>

            <td><?php
                if($row->CharacterType == 0)
                {
                    echo 'VG';
                }
                if($row->CharacterType == 1)
                {
                    echo 'VG_';
                }
                if($row->CharacterType == 2)
                {
                    echo 'GOOD';
                }
                if($row->CharacterType == 3)
                {
                    echo 'FAIR';
                }
                if($row->CharacterType == 4)
                {
                    echo 'INDIF';
                }
                if($row->CharacterType == 5)
                {
                    echo 'BAD';
                }
                ?></td>

            <td><?php
                if($row->EfficiencyType == 0)
                {
                    echo 'SUPER';
                }
                if($row->EfficiencyType == 1)
                {
                    echo 'SAT';
                }
                if($row->EfficiencyType == 2)
                {
                    echo 'MOD';
                }
                if($row->EfficiencyType == 3)
                {
                    echo 'INFER';
                }
                if($row->EfficiencyType == 4)
                {
                    echo 'UT';
                }

                ?></td>
            <td><?php echo $row->SHIP_ESTABLISHMENT; ?></td>
        </tr>
        <?php
    }
    ?>

    </tbody>
</table>

