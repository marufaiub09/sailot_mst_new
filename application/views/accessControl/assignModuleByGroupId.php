<?php
foreach ($org_modules as $org_module) {
    //$org_module_links = $this->utilities->findAllByAttributeWithJoin("sa_org_mlinks", "sa_module_links", "LINK_ID", "LINK_ID", "LINK_NAME", array("SA_MODULE_ID" => $org_module->SA_MODULE_ID));
    $org_module_links = $this->db->query("SELECT sml.LINK_NAME, sml.LINK_ID, som.* FROM sa_org_mlinks som
                                                                                LEFT JOIN sa_module_links sml on sml.LINK_ID = som.LINK_ID
                                                                                WHERE sml.PR_LINK_ID is null AND som.SA_MODULE_ID = $org_module->SA_MODULE_ID")->result();
    ?>
    <li>
        <a id="nut3" data-value="Bootstrap_Tree" data-toggle="branch" class="tree-toggle closed" href="#" style="color: #4A8BC2;"><?php echo $org_module->SA_MODULE_NAME; ?></a>
        <ul class="branch in" style="width: 98%; padding: 5px; margin-left: 15px;">
            <?php
            foreach ($org_module_links as $org_module_link):
                $active_pages = $this->utilities->findByAttribute("sa_uglw_mlink", array("SA_MLINKS_ID" => $org_module_link->SA_MLINKS_ID, "USERGRP_ID" => $group));
                ?>
                <li style="border-bottom: 1px dashed #ccc; padding: 5px 0 5px 0; background: #f2f2f2; padding-left: 5px;">
                    <span style="color: #333;"><i class="icon-file"></i> <?php echo $org_module_link->LINK_NAME; ?></span>
                    <span style="float: right;">
                        <span style="padding: 0 19px;">
                            <?php if ($org_module_link->CREATE == 1) { ?>
                                <input type="checkbox" class="chkPage"  title="Create" id="<?php echo (!empty($active_pages) ? $active_pages->CREATE : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->CREATE == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'C'; ?>" />
                            <?php } else { ?>
                                <input type="checkbox" title="Create" disabled="disabled"  />
                            <?php } ?>
                        </span>
                        <span style="padding: 0 19px;">
                            <?php if ($org_module_link->READ == 1) { ?>
                                <input type="checkbox" class="chkPage" title="Read" id="<?php echo (!empty($active_pages) ? $active_pages->READ : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->READ == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'R'; ?>" />
                            <?php } else { ?>
                                <input type="checkbox" title="Read" disabled="disabled" />
                            <?php } ?>
                        </span>
                        <span style="padding: 0 19px;">
                            <?php if ($org_module_link->UPDATE == 1) { ?>
                                <input type="checkbox" class="chkPage" title="Update" id="<?php echo (!empty($active_pages) ? $active_pages->UPDATE : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->UPDATE == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'U'; ?>" />
                            <?php } else { ?>
                                <input type="checkbox" title="Update" disabled="disabled" />
                            <?php } ?>
                        </span>
                        <span style="padding: 0 19px;">
                            <?php if ($org_module_link->DELETE == 1) { ?>
                                <input type="checkbox" class="chkPage" title="Delete" id="<?php echo (!empty($active_pages) ? $active_pages->DELETE : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->DELETE == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'D'; ?>" />
                            <?php } else { ?>
                                <input type="checkbox" title="Delete" disabled="disabled" />
                            <?php } ?>
                        </span>
                        <span style="padding: 0 19px;">
                            <?php if ($org_module_link->STATUS == 1) { ?>
                                <input type="checkbox" class="chkPage" title="Delete" id="<?php echo (!empty($active_pages) ? $active_pages->STATUS : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->STATUS == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'S'; ?>" />
                            <?php } else { ?>
                                <input type="checkbox" title="Delete" disabled="disabled" />
                            <?php } ?>
                        </span>
                    </span>
                    <?php $org_module_link_not_null = $this->db->query("SELECT sml.LINK_NAME, sml.LINK_ID FROM sa_org_mlinks som
                                            LEFT JOIN sa_module_links sml on sml.LINK_ID = som.LINK_ID
                                            WHERE sml.PR_LINK_ID = $org_module_link->LINK_ID")->result(); ?>
                    <?php if(!empty($org_module_link_not_null)){
                    echo"<ul>";
                     foreach ($org_module_link_not_null as $org_module_link_notNull) { ?>                                                                    
                        <li style="margin-right: -5px;">
                            <span><i class="icon-leaf"></i> <?php echo $org_module_link_notNull->LINK_NAME; ?></span>
                            <span style="float: right;">
                            <span style="padding: 0 19px;">
                                <?php if ($org_module_link->CREATE == 1) { ?>
                                    <input type="checkbox" class="chkPage"  title="Create" id="<?php echo (!empty($active_pages) ? $active_pages->CREATE : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->CREATE == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'C'; ?>" />
                                <?php } else { ?>
                                    <input type="checkbox" title="Create" disabled="disabled"  />
                                <?php } ?>
                            </span>
                            <span style="padding: 0 19px;">
                                <?php if ($org_module_link->READ == 1) { ?>
                                    <input type="checkbox" class="chkPage" title="Read" id="<?php echo (!empty($active_pages) ? $active_pages->READ : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->READ == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'R'; ?>" />
                                <?php } else { ?>
                                    <input type="checkbox" title="Read" disabled="disabled" />
                                <?php } ?>
                            </span>
                            <span style="padding: 0 19px;">
                                <?php if ($org_module_link->UPDATE == 1) { ?>
                                    <input type="checkbox" class="chkPage" title="Update" id="<?php echo (!empty($active_pages) ? $active_pages->UPDATE : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->UPDATE == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'U'; ?>" />
                                <?php } else { ?>
                                    <input type="checkbox" title="Update" disabled="disabled" />
                                <?php } ?>
                            </span>
                            <span style="padding: 0 19px;">
                                <?php if ($org_module_link->DELETE == 1) { ?>
                                    <input type="checkbox" class="chkPage" title="Delete" id="<?php echo (!empty($active_pages) ? $active_pages->DELETE : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->DELETE == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'D'; ?>" />
                                <?php } else { ?>
                                    <input type="checkbox" title="Delete" disabled="disabled" />
                                <?php } ?>
                            </span>
                            <span style="padding: 0 19px;">
                                <?php if ($org_module_link->STATUS == 1) { ?>
                                    <input type="checkbox" class="chkPage" title="Delete" id="<?php echo (!empty($active_pages) ? $active_pages->STATUS : 0); ?>" <?php echo (!empty($active_pages) ? ($active_pages->STATUS == 1) ? "checked" : ""  : 0); ?> value="<?php echo $org_module->SA_MODULE_ID . ',' . $org_module_link->SA_MLINKS_ID . ',' . 'S'; ?>" />
                                <?php } else { ?>
                                    <input type="checkbox" title="Delete" disabled="disabled" />
                                <?php } ?>
                            </span>
                        </span>
                        </li>
                    <?php  } 
                    echo "</ul>";
                    }
                    ?>
                    <br clear="all" />
                </li>
            <?php endforeach; ?>
        </ul>
    </li>
    <?php
}
?>