<table id="dataTable" class="display table-striped " width="100%" cellspacing="0">
    <thead>
        <tr>            
            <th> Official No </th>
            <th> Local No </th>
            <th> Full Name </th>
            <th> Short Name </th>
            <th> Rank </th>
            <th> Date Of Birth </th>
            <th> Action </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($searchSailor as $row) { ?>
            <tr id="row_<?php echo $row->SAILORID ?>">
                <td><?php echo $row->OFFICIALNUMBER ?></td>
                <td><?php echo $row->LOCALNUMBER ?></td>
                <td><?php echo $row->FULLNAME ?></td>
                <td><?php echo $row->SHORTNAME ?></td>
                <td><?php echo $row->RANKID ?></td>
                <td><?php echo date("d/m/Y", strtotime($row->BIRTHDATE)); ?></td>
                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/appointmentType/view/' . $row->SAILORID); ?>" title="View Detail Sailor Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/edit/' . $row->SAILORID); ?>"  title="Edit Detail Sailor Information" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->SAILORID; ?>" title="Click For Delete" data-type="delete" data-field="SAILORID" data-tbl="sailor"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var dataTable = $('#dataTable').DataTable();
    });
</script>