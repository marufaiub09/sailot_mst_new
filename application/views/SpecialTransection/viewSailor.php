<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var dataTable = $('#employee-grid').DataTable({
            "responsive": true,   // enable responsive
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "rowId": 'staffId',
            "ajax": {
                url: "<?php echo base_url()?>SpecialTransection/CapturingNewRecrutiInfo/index", // json datasource
                type: "post", // method  , by default get
                error: function () {  // error handling
                    $(".employee-grid-error").html("");
                    $("#employee-grid").append('<tbody class="employee-grid-error"><tr id="row_"><th colspan="3">No data found in the server</th></tr></tbody>');
                    $("#employee-grid_processing").css("display", "none");
                }
            },
            /*id attribute add into dataTable*/
            createdRow: function(row, data, dataIndex) {
            //console.log(data);

                var a = $(row).find('td:eq(0)').html();
                $(row).attr("id", 'row_'+a);
            }
        });
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><?php echo $this->lang->line('title'); ?></h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <!-- <a class="btn btn-primary btn-xs modalLink" data-modal-size="modal-lg"  href="<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/create'); ?>" title="<?php echo $this->lang->line('create_newSailor'); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a> -->
                    </div>
                </div>
                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>                
            </div>
            <div class="widget widget-heading-simple widget-body-gray col-md-12">
                   <div class="col-md-1" style="width:12%; padding-top: 7px;"><strong><i class="fa fa-search"></i> Search</strong></div>
                    <div class="form-group col-md-5">
                        <label class="col-sm-4 control-label" style="padding-top: 7px;"><?php echo $this->lang->line('batch_name'); ?></label>
                        </a>
                        <div class="col-sm-6" style="padding-top: 7px;">
                            <?php echo form_dropdown('batchid', $batch, '', 'id="batchid" required="required" class="form-control select2"'); ?>
                        </div>
                    </div>
                    <div class="form-group col-md-5">
                        <div class="col-sm-12" style="padding-top: 11px;">
                            <input type="button" class="btn-primary btn-xs pull-right" id="searchAbsent" name="submit" value="Find Sailor"/>
                        </div>
                    </div>
                </div>
            <div class="panel-body" id="branchWise">
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
        $("#batchid").on('change', function(){
            var batch = this.value;
            if(batch != ''){
                $.ajax({
                    type: "post",
                    url: "<?php echo site_url(); ?>/SpecialTransection/CapturingNewRecrutiInfo/searchbramch",
                    data: {batch: batch},
                    beforeSend: function () {
                      $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                       },
                    success: function (data) {
                        $("#branchWise").html(data);
                    }
                });
        
            }else{
                $("#branchWise").html('');
            }
        });
    });
</script>