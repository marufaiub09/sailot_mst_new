<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 7/31/2016
 * Time: 11:47 AM
 */
?>
<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">

<div class="col-md-6">

    <div class="form-group">
        <lable class="col-md-4"">Official Number</lable>
        <div class="col-md-6">                
            <input type="text" class="form-control" id="officialNumber" name="officialNUmber" placeholder="ENTER OFFICIAL NUMBER">
        </div>
    </div>
</div>
<br><br>

<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">General Information</h3>
            <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
        <div class="panel-body form-horizontal frmContent">
            <div class="col-md-12">

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Rank:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'rank', 'type' => 'text', "class" => "form-control", 'id' => 'rank', 'placeholder' => 'RANK')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">

                        <label class="col-sm-5">Birth Date:</label>

                        <div class="col-sm-5 date">
                            <?php echo form_input(array('name' => 'birthDate', 'type' => 'text', "class" => "form-control", 'id' => 'birthDate', 'placeholder' => 'BIRHT DATE')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Full Name:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'fullName', 'type' => 'text', "class" => "form-control", 'id' => 'fullName', 'placeholder' => 'FULL NAME')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5">সম্পুর্ণ নাম বাংলায়:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'fullNameBangla', 'type' => 'text', "class" => "form-control", 'id' => 'fullNameBangla', 'placeholder' => 'FULL NAME BANGLA')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Short Name:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'shortNameBangla', 'type' => 'text', "class" => "form-control", 'id' => 'shortNameBangla', 'placeholder' => 'SHORT NAME ')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5">সংক্ষিপ্ত নাম বাংলায়:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'shortName', 'type' => 'text', "class" => "form-control", 'id' => 'shortName', 'placeholder' => 'SHORT NAME BANGLA')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Nick Name:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'nickName', 'type' => 'text', "class" => "form-control", 'id' => 'nickName', 'placeholder' => 'NICK NAME')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5">ডাক নাম বাংলায়:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'nickNameBangla', 'type' => 'text', "class" => "form-control", 'id' => 'nickNameBangla', 'placeholder' => 'NICK NAME BANGLA ')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Father's Name:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'fatherName', 'type' => 'text', "class" => "form-control", 'id' => 'fatherName', 'placeholder' => 'FATHER NAME')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5">বাবার নাম বাংলায়:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'fatherNameBangla', 'type' => 'text', "class" => "form-control", 'id' => 'fatherNameBangla', 'placeholder' => 'FATHER NAME BANGLA')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Mother's Name:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'motherName', 'type' => 'text', "class" => "form-control", 'id' => 'motherName', 'placeholder' => 'MOTHER NAME')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-5">মায়ের নাম বাংলায়:</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'motherNameBangla', 'type' => 'text', "class" => "form-control", 'id' => 'motherNameBangla', 'placeholder' => 'MOTHER NAME BANGLA')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Maritial Status:</label>

                        <div class="col-md-5">
                            <?php echo form_input(array('name' => 'maritalStatus', 'type' => 'text', "class" => "form-control", 'id' => 'maritalStatus', 'placeholder' => 'MARITIAL STATUS')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label class="col-md-4" >Religion Status:</label>

                        <div class="col-md-5"style="padding-left:60px;">
                            <?php echo form_input(array('name' => 'religionStatus', 'type' => 'text', "class" => "form-control", 'id' => 'religionStatus', 'placeholder' => 'RELIGION STATUS')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <fieldset class="">
                    <legend class="legend" style="font-size: 12px;  margin-bottom: 10px;">Freedom
                        fighter status
                    </legend>
                    <div class="col-md-3">
                        <input type="checkbox" 
                               name="fredomFighterStatus" id="fredomFighterStatus" checked="checked"
                               value="0"/>None</label>

                    </div>
                    <div class="col-md-4">
                        <label class="check"><input type="checkbox" 
                                                    name="fredomfighterStatus" value="1"/> Freedom
                            Fighter</label>
                    </div>
                    <div class="col-md-5">
                        <label class="check"><input type="checkbox" 
                                                    name="fredomfighterStatus" name="iradio"
                                                    value="2"/> Freedom Fighter Child</label>
                    </div>
                </fieldset>
            </div>
        </div>

    </div>

</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Engagement Information and Period</h3>
            <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
        <div class="panel-body">

            <div class="col-md-12">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-5">Entry Date</label>

                        <div class="col-sm-6 date">
                            <?php echo form_input(array('name' => 'EntryDate', 'type' => 'text', "class" => "form-control", 'id' => 'EntryDate', 'placeholder' => 'ENTRY DATE')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-5">Entry Type</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'entryType', 'type' => 'text', "class" => "form-control", 'id' => 'entryType', 'placeholder' => 'ENTRY TYPE')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-sm-5">Expire Date</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'expiryDate', 'type' => 'text', "class" => "form-control", 'id' => 'expiryDate', 'placeholder' => 'EXPIRE DATE')); ?>
                        </div>

                    </div>
                    <hr style=" margin-bottom: 5px; margin-top: 0;">

                </div>

            </div>


        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Posting Unit</h3>
            <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
        <div class="panel-body form-horizontal frmContent">

            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Zone</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'zone', 'type' => 'text', "class" => "form-control", 'id' => 'zone', 'placeholder' => 'ZONE')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Area</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'area', 'type' => 'text', "class" => "form-control", 'id' => 'area', 'placeholder' => 'AREA')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Ship/Establishment</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'shipestablishment', 'type' => 'text', "class" => "form-control", 'id' => 'shipestablishment', 'placeholder' => 'SHIPESTABLISHMENT')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Posting Unit</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'postingUnit', 'type' => 'text', "class" => "form-control", 'id' => 'postingUnit', 'placeholder' => 'POSTING UNIT')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Posting Date</label>

                        <div class="col-sm-4 date">
                            <?php echo form_input(array('name' => 'postingDate', 'type' => 'text', "class" => "form-control", 'id' => 'postingDate', 'placeholder' => 'POSTING DATE')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-md-4">Posting Type</label>

                        <div class="col-md-6">
                            <?php echo form_input(array('name' => 'postingType', 'type' => 'text', "class" => "form-control", 'id' => 'postingType', 'placeholder' => 'POSTING TYPE')); ?>
                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Permanent Address</h3>
            <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
        <div class="panel-body form-horizontal frmContent">

            <div class="col-md-12">

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Division </label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'division', 'type' => 'text', "class" => "form-control", 'id' => 'division', 'placeholder' => 'DIVISION')); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Select District</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'district', 'type' => 'text', "class" => "form-control", 'id' => 'district', 'placeholder' => 'DISTRICT')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Police Station</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'policeStation', 'type' => 'text', "class" => "form-control", 'id' => 'policeStation', 'placeholder' => 'POLICE STATION')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Permanent Address</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'permanentAddress', 'type' => 'text', "class" => "form-control", 'id' => 'permanentAddress', 'placeholder' => 'PERMANENT ADDRESS')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">স্থায়ী ঠিকানা বাংলায়</label>

                        <div class="col-sm-7">
                            <?php echo form_input(array('name' => 'permanentAddressBangla', 'type' => 'text', "class" => "form-control", 'id' => 'permanentAddressBangla', 'placeholder' => 'PERMANENT ADDRESS BANGLA')); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Next of kin Information And Address</h3>
            <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
        <div class="panel-body form-horizontal frmContent">

            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Next of kin</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'nextofkin', 'type' => 'text', "class" => "form-control", 'id' => 'nextofkin', 'placeholder' => 'NEXT OF KIN')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">আত্মীয়র নাম বাংলায়</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'nextofkinBangla', 'type' => 'text', "class" => "form-control", 'id' => 'nextofkinBangla', 'placeholder' => 'NEXT OF KIN BANGLA')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Relation of next kin</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'relation', 'type' => 'text', "class" => "form-control", 'id' => 'relation', 'placeholder' => 'RELATION NEXT OF KIN')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Next of kin address:</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'nextofkinAddress', 'type' => 'text', "class" => "form-control", 'id' => 'nextofkinAddress', 'placeholder' => ' NEXT OF KIN ADDRESS')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">আত্মীয়র ঠিকানা বাংলায়</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'nextofkinAddressBangla', 'type' => 'text', "class" => "form-control", 'id' => 'nextofkinAddressBangla', 'placeholder' => ' NEXT OF KIN ADDRESS BANGLA')); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Academic Information</h3>
            <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
        <div class="panel-body">
            <table id="acdemicTable" class="table table-striped table-bordered" width="100%"
                   cellspacing="0">
                <thead>
                    <tr>
                        <th>Exam System &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Exam Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subject/Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Passing Year</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Board/University&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>GPA/Result</th>
                        <th>Total Marks</th>
                        <th>Percentage</th>
                        <th>Result Description</th>
                        <th>Education Status Desc</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Physical Information</h3>
            <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
        </div>
        <div class="panel-body form-horizontal frmContent">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Height</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'height', 'type' => 'text', "class" => "form-control", 'id' => 'height', 'placeholder' => ' HEIGHT')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="col-sm-4">Weight</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'weight', 'type' => 'text', "class" => "form-control", 'id' => 'weight', 'placeholder' => ' WEIGHT')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Chest</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'chest', 'type' => 'text', "class" => "form-control", 'id' => 'chest', 'placeholder' => ' CHEST')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">  Eye Color</label>
                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'eyecolor', 'type' => 'text', "class" => "form-control", 'id' => 'eyecolor', 'placeholder' => ' EYE COLOR')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Eye Sight</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'eyesight', 'type' => 'text', "class" => "form-control", 'id' => 'eyesight', 'placeholder' => ' EYE SIGHT')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4"> Hair Color</label>
                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'haircolor', 'type' => 'text', "class" => "form-control", 'id' => 'haircolor', 'placeholder' => 'HAIR COLOR')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Blood Group</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'bloodgroup', 'type' => 'text', "class" => "form-control", 'id' => 'bloodgroup', 'placeholder' => 'BLOOD GROUP')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4"> Complexion</label>
                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'complexion', 'type' => 'text', "class" => "form-control", 'id' => 'complexion', 'placeholder' => 'COMPLEXION')); ?>
                        </div>

                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Sex</label>

                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'sex', 'type' => 'text', "class" => "form-control", 'id' => 'sex', 'placeholder' => 'SEX')); ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-4">Id Marks</label>
                        <div class="col-sm-6">
                            <?php echo form_input(array('name' => 'idmarks', 'type' => 'text', "class" => "form-control", 'id' => 'idmarks', 'placeholder' => 'ID MARKS')); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 100%;
    }
    .dataTables_empty{
        display: none;
    }
</style>
<script type="text/javascript">
    $('#acdemicTable').removeAttr('width').DataTable( {
        "scrollX": true,
        "scrollX": true,
        "bSort" : false,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "columnDefs": [
            { width: '100px', targets: 0 },
            { width: '100px', targets: 1 },
            { width: '100px', targets: 2 },
            { width: '100px', targets: 3 },
            { width: '100px', targets: 4 },
            { width: '100px', targets: 5 },
            { width: '100px', targets: 6 },
            { width: '100px', targets: 7 },
            { width: '100px', targets: 8 },
            { width: '100px', targets: 9 },

        ],
        "fixedColumns": true

    } );
    $(document).on("change", "#officialNumber", function () {
     
        var officialNumber = $(this).val();
        //alert(officialNumber);
        $.ajax({
            type: "post",
            url: "<?php echo site_url('SpecialTransection/sailorPreview/sailorDetails'); ?>/",
            data: {officialNumber: officialNumber},
            dataType: "json",
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
               
                if (data != '' && data != undefined)
                //alert(data);
                //console.log(data['FULLNAME'])
                {
                    // console.log(data.academic);
                    $('#fullName').val(data.general['FULLNAME']);
                    $('#rank').val(data.general['RANK_ID']);
                    $('#birthDate').val(data.general['BIRTHDATE']);
                    $('#fullNameBangla').val(data.general['FULLNAMEINBANGLA']);
                    $('#shortName').val(data.general['SHORTNAME']);
                    $('#shortNameBangla').val(data.general['SHORTNAMEINBANGLA']);
                    $('#nickName').val(data.general['NICKNAME']);
                    $('#nickNameBangla').val(data.general['NICKNAMEINBANGLA']);
                    $('#fatherName').val(data.general['FATHERNAME']);
                    $('#fatherNameBangla').val(data.general['FATHERNAMEINBANGLA']);
                    $('#motherName').val(data.general['MOTHERNAME']);
                    $('#motherNameBangla').val(data.general['MOTHERNAMEINBANGLA']);
                    $('#maritalStatus').val(data.general['MARITAL_STATUS']);
                    $('#religionStatus').val(data.general['RELI_GION']);
                    $('#fredomFighterStatus').val(data.general['FredomFighter']);
                    $('#EntryDate').val(data.general['ENTRYDATE1']);
                    $('#entryType').val(data.general['ENTRYTYPE_ID']);
                    $('#expiryDate').val(data.general['EXPIRYDATE']);
                    $('#zone').val(data.general['ZONE_ID']);
                    $('#area').val(data.general['AREA_ID']);
                    $('#postingUnit').val(data.general['POSTINGUNIT_ID']);
                    $('#shipestablishment').val(data.general['SHIPESTABLISHMENT_ID']);
                    $('#postingType').val(data.general['POSTING_TYPE']);
                    $('#postingDate').val(data.general['POSTING_DATE']);                 
                    $('#division').val(data.general['DIVISION_NAME']);
                    $('#district').val(data.general['DISTRICT']);
                    $('#policeStation').val(data.general['POLICESTATION']);
                    $('#permanentAddress').val(data.general['PERMANENTADDRESS']);
                    $('#permanentAddressBangla').val(data.general['PERMANENTADDRESSINBANGLA']);                 
                    $('#nextofkin').val(data.general['NEXTOFKIN']);
                    $('#nextofkinAddress').val(data.general['NEXTOFKINADDRESS']);
                    $('#nextofkinAddressBangla').val(data.general['NEXTOFKINADDRESSINBANGLA']);
                    $('#nextofkinBangla').val(data.general['NEXTOFKININBANGLA']);
                    $('#relation').val(data.general['RELATION_ID']);       
                    $('#height').val(data.general['HEIGHT']);
                    $('#weight').val(data.general['WEIGHT']);
                    $('#chest').val(data.general['CHEST']);
                    
                    if(data.general['EYECOLOR'] == 1 )
                    {
                        $('#eyecolor').val('Black');
                    }
                    else if(data.general['EYECOLOR'] == 2)
                    {
                        $('#eyecolor').val('Brown');
                    }
                    else
                    {
                        $('#eyecolor').val('None'); 
                    }
                    if(data.general['EYESIGHT'] == 1 )
                    {
                        $('#eyesight').val('Six_By_Six');
                    }
                    else if(data.general['EYESIGHT'] == 2)
                    {
                        $('#eyesight').val('Six_By_Nine');
                    }
                    else if(data.general['EYESIGHT']==3)
                    {
                        $('#eyesight').val('Six_By_Twelve');
                    }
                    else if(data.general['EYESIGHT']==4)
                    {
                        $('#eyesight').val('Six_By_Eighteen');
                    }
                    else if(data.general['EYESIGHT']==5)
                    {
                        $('#eyesight').val('Six_By_Sixty');
                    }
                    else
                    {
                        $('#eyesight').val('None'); 
                    }
                    if(data.general['HAIRCOLOR'] == 1 )
                    {
                        $('#haircolor').val('Black');
                    }
                    else if(data.general['EYESIGHT'] == 2)
                    {
                        $('#haircolor').val('Brown');
                    }
                    else if(data.general['EYESIGHT']==3)
                    {
                        $('#haircolor').val('Golden');
                    }
                    else if(data.general['EYESIGHT']==4)
                    {
                        $('#haircolor').val('White');
                    }
                    else
                    {
                        $('#haircolor').val('None'); 
                    }
                    if(data.general['BLOODGROUP'] == 1 )
                    {
                        $('#bloodgroup').val('APositive');
                    }
                    else if(data.general['BLOODGROUP'] == 2)
                    {
                        $('#bloodgroup').val('ANegative');
                    }
                    else if(data.general['BLOODGROUP']==3)
                    {
                        $('#bloodgroup').val('BPositive');
                    }
                    else if(data.general['BLOODGROUP']==4)
                    {
                        $('#bloodgroup').val('BNegative');
                    }
                    else if(data.general['BLOODGROUP']==5)
                    {
                        $('#bloodgroup').val('OPositive');
                    }
                    else if(data.general['BLOODGROUP']==6)
                    {
                        $('#bloodgroup').val('ONegative');
                    }
                    else if(data.general['BLOODGROUP']==7)
                    {
                        $('#bloodgroup').val('ABPositive');
                    }
                    else if(data.general['BLOODGROUP']==7)
                    {
                        $('#bloodgroup').val('ABNegative');
                    }
                    else
                    {
                        $('#bloodgroup').val('None'); 
                    }
                    if(data.general['COMPLEXION'] == 1 )
                    {
                        $('#complexion').val('Fair');
                    }
                    else if(data.general['COMPLEXION'] == 2)
                    {
                        $('#complexion').val('Dark');
                    }
                    else if(data.general['COMPLEXION']==3)
                    {
                        $('#complexion').val('MediumDark');
                    }
                    else if(data.general['EYESIGHT']==4)
                    {
                        $('#complexion').val('LightDark');
                    }
                    else
                    {
                        $('#complexion').val('None'); 
                    }
                    if(data.general['SEX'] == 1 )
                    {
                        $('#sex').val('Male');
                    }
                    else if(data.general['SEX'] == 2)
                    {
                        $('#sex').val('Female');
                    }
                    else
                    {
                        $('#sex').val('None'); 
                    }
                    $('#idmarks').val(data.general['IDMARKS']); 
                    
                    if(data.academic != ''&& data.academic != undefined)
                    {
                        var academic = data.academic;
                            
                        $.each(academic, function (obj, value) {
                            var examSys = value['Exam_System'];
                            examSystem = "<input type='text' name='examSystem[]' class='form-control'  value='" + examSys + "' >";
                            var examNam = value['Exam_Name'];
                            examName = "<input type='text' name='examName[]' class='form-control'  value='" + examNam + "' >";
                            var sub = value['Subject'];
                            subject = "<input type='text' name='subject[]' class='form-control'  value='" + sub + "' >";
                            var passYear = value['PassingYear'];
                            passingYear = "<input type='text' name='passingYear[]' class='form-control'  value='" + passYear + "' >";
                            var boardNam = value['Board_Name'];
                            boardName = "<input type='text' name='boardName[]' class='form-control'  value='" + boardNam + "' >";
                            var exGred = value['Exam_Gred'];
                            examGred = "<input type='text' name='examGred[]' class='form-control'  value='" + exGred + "' >";
                            var toMarks = value['TotalMarks'];
                            totalMarks = "<input type='text' name='totalMarks[]' class='form-control'  value='" + toMarks + "' >";
                            var percent = value['Percentage'];
                            percentage = "<input type='text' name='percentage[]' class='form-control'  value='" + percent + "' >";
                            var resultDes = value['ResultDescription'];
                            resultDescription = "<input type='text' name='resultDescription[]' class='form-control'  value='" + resultDes + "' >";
                            var eduStatus = value['EducationStatus'];
                            educationalStatus = "<input type='text' name='educationalStatus[]' class='form-control'  value='" + eduStatus + "' >";
                           
                           
                              
                                                                                                                                                                                                                                            
                                
                            $("#acdemicTable tbody").append("<tr><td>" + examSystem + "</td><td>" + examName + "</td><td>" + subject + "</td><td>" + passingYear + "</td><td>" + boardName + "</td><td>" + examGred + "</td><td>" + totalMarks + "</td><td>" + percentage + "</td><td>" + resultDescription + "</td><td>" + educationalStatus + "</td></tr>");


                        });
                    }
                }
            }
                    
            

        });
     
     
    });   

  

</script>