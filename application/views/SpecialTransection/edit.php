<script src="<?php echo base_url() ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url() ?>dist/styles/fixedColumns.dataTables.min.css">
<div class="row">
    <form class="form-horizontal frmContent" id="MainForm" method="post">
        <span class="frmMsg"></span>

        <div class="col-md-12" id="tabs">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "
                           href="<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/index'); ?>"
                           title="List Sailor informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">New Recruit Information</h3>
                    </div>
                </div>
            </div>
            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active "><a href="form-layouts-tabbed.html#tab-first" role="tab" data-toggle="tab">General
                            Info & Engagement Info</a></li>
                    <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Permanent Address and
                            Next of Kin Info</a></li>
                    <li><a href="form-layouts-tabbed.html#tab-third" role="tab" data-toggle="tab">Academic & Physical
                            Info</a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="tab-first">


                        <legend class="legend">General Information</legend>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Rank <span class="text-danger"> * </span></label>

                                    <div class="col-md-6">
                                        <select class="selectpicker " name="RankName"
                                                id="RankId" data-placeholder="Select Rank" data-live-search="true">
                                            <option value="<?php echo $result->R; ?>"><?php echo $result->RANK_ID; ?></option>
                                            <?php
                                            foreach ($rankData as $row):
                                                ?>
                                                <option
                                                    value="<?php echo $row->RANK_ID . "_" . $row->BRANCH_ID . "_" . $row->EQUIVALANT_RANKID; ?>"><?php echo "[" . $row->RANK_CODE . "] " . $row->RANK_NAME ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">

                                    <label class="col-sm-5">Birth Date <span class="text-danger"> *</span></label>

                                    <div class="col-sm-5 date">
                                        <?php echo form_input(array('name' => 'birthDate', "class" => "datePicker form-control ", 'value' => $result->BIRTHDATE, 'placeholder' => 'Birth Date')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select BirthDate">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Full Name <span class="text-danger"> * </span></label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fullName', 'type' => 'text', "class" => "form-control fullName ", '' => '', 'value' => $result->FULLNAME, 'placeholder' => 'Enter Full Name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Enter Full Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">সম্পুর্ণ নাম বাংলায়</label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fullNameInBn', 'type' => 'text', "class" => "form-control ", 'value' => $result->FULLNAMEINBANGLA, 'placeholder' => 'সম্পুর্ণ নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Short Name <span class="text-danger"> * </span></label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'shortName', 'type' => 'text', "class" => "form-control ", '' => '', 'placeholder' => 'Enter Short Name', 'value' => $result->SHORTNAME)); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Enter Short Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">সংক্ষিপ্ত নাম বাংলায়</label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'shortNameBangla', 'type' => 'text', "class" => "form-control ", 'value' => $result->SHORTNAMEINBANGLA, 'placeholder' => 'সংক্ষিপ্ত নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Nick Name</label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'nickName', 'type' => 'text', "class" => "form-control ", 'value' => $result->NICKNAME, 'placeholder' => 'Enter Nick Name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Enter NIck Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">ডাক নাম বাংলায়</label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'nickNameInBangla', 'type' => 'text', "class" => "form-control ", 'value' => $result->NICKNAMEINBANGLA, 'placeholder' => 'ডাক নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Father's Name <span class="text-danger"> * </span></label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fatherName', 'type' => 'text', "class" => "form-control ", 'value' => $result->FATHERNAME, '' => '', 'placeholder' => "Enter Father's Name")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Enter Father's Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">বাবার নাম বাংলায়</label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fatherNameInBangls', 'type' => 'text', "class" => "form-control ", 'value' => $result->FATHERNAMEINBANGLA, 'placeholder' => "বাবার নাম বাংলায়")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="asa">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Mother's Name <span class="text-danger"> * </span></label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'motherName', 'type' => 'text', "class" => "form-control ", 'value' => $result->MOTHERNAME, '' => '', 'placeholder' => "Enter Mother's Name")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Enter Mother's Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">মায়ের নাম বাংলায়</label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'motherNameInBangla', 'type' => 'text', "class" => "form-control ", 'value' => $result->MOTHERNAMEINBANGLA, 'placeholder' => "মায়ের নাম বাংলায়")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Maritial Status <span class="text-danger"> * </span></label>

                                    <div class="col-md-5">
                                        <select name="mariatialStatus" class=" selectpicker" data-live-search = "true" data-placeholder="Select Status" data-width="120px">
                                            <option value="<?php echo $result->MARITALSTATUS; ?>"><?php echo $result->MARITAL_STATUS; ?>
                                            <option value="0">Unmarried</option>
                                            <option value="1">Married</option>
                                            <option value="2">Separation</option>
                                            <option value="3">Divorce</option>
                                            <option value="4">Widower</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Maritial Status">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4">Religion <span class="text-danger"> * </span></label>

                                    <div class="col-md-5">
                                        <select name="religion" class=" selectpicker"
                                                data-placeholder="Select Status" aria-hidden="true"
                                                data-allow-clear="true" data-live-search="true" data-width="120px">
                                            <option value="<?php echo $result->RELIGION; ?>"><?php echo $result->RELI_GION ?></option>
                                            <option value="1">Islam</option>
                                            <option value="2">Hindu</option>
                                            <option value="3">Buddhist</option>
                                            <option value="4">Christian</option>
                                            <option value="5">Other's</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Religion">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="">
                                    <legend class="legend" style="font-size: 12px;  margin-bottom: 10px;">Freedom
                                        fighter status
                                    </legend>

                                    <div class="form-group">
                                        <?php
                                        $st = $result->FREEDOMFIGHTERSTATUS;
                                        // echo $st;
                                        if ($st == 0) {
                                            ?>
                                            <div class="col-md-3">
                                                <label class="check"><input type="radio" class="iradio"
                                                                            name="fredomfighterStatus" checked="checked"
                                                                            value="0"/>None</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="check"><input type="radio" class="iradio"
                                                                            name="fredomfighterStatus" value="1"/> Freedom
                                                    Fighter</label>
                                            </div>
                                            <div class="col-md-5">
                                                <label class="check"><input type="radio" class="iradio"
                                                                            name="fredomfighterStatus" name="iradio"
                                                                            value="2"/> Freedom Fighter Child</label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        $st = $result->FREEDOMFIGHTERSTATUS;
                                        // echo $st;
                                        if ($st == 1) {
                                            ?>
                                            <div class="col-md-3">
                                                <label class="check"><input type="radio" class="iradio"
                                                                            name="fredomfighterStatus" checked=""
                                                                            value="0"/>None</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="check"><input type="radio" class="iradio" checked="checked"
                                                                            name="fredomfighterStatus" value="1"/> Freedom
                                                    Fighter</label>
                                            </div>
                                            <div class="col-md-5">
                                                <label class="check"><input type="radio" class="iradio"
                                                                            name="fredomfighterStatus" name="iradio"
                                                                            value="2"/> Freedom Fighter Child</label>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        $st = $result->FREEDOMFIGHTERSTATUS;
                                        // echo $st;
                                        if ($st == 2) {
                                            ?>
                                            <div class="col-md-3">
                                                <label class="check"><input type="radio" class="iradio"
                                                                            name="fredomfighterStatus" checked=""
                                                                            value="0"/>None</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="check"><input type="radio" class="iradio"
                                                                            name="fredomfighterStatus" value="1"/> Freedom
                                                    Fighter</label>
                                            </div>
                                            <div class="col-md-5">
                                                <label class="check">
                                                    <input type="radio" class="iradio" name="fredomfighterStatus" name="iradio" checked="checked" value="2"/> Freedom Fighter Child</label>
                                            </div>
                                            <?php
                                        }
                                        ?>

                                    </div>

                                </fieldset>
                            </div>
                        </div>
                        <legend class="legend">Engagement Information and Period</legend>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3">Entry Date</label>

                                    <div class="col-sm-4 date">
                                        <?php echo form_input(array('name' => 'entryDate', "id" => "engagementDate", "class" => "datePicker form-control", 'value' => date('d-m-Y'), "value" => $result->ENTRYDATE1, 'placeholder' => 'Entry Date')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Enter Entry Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3">Entry Type</label>

                                    <div class="col-md-4">
                                        <select class="selectpicker " name="entryType" data-width="130px"
                                                id="batchid" data-placeholder="Select Type" data-live-search="true">
                                            <option value="<?php echo $result->ENTRYTYPEID; ?>"><?php echo $result->ENTRYTYPE_ID; ?></option>
                                            <?php
                                            foreach ($entryType1 as $row):
                                                ?>
                                                <option
                                                    value="<?php echo $row->ENTRY_TYPEID; ?>"><?php echo $row->NAME; ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                        </select>

                                        <!--  <?php echo form_dropdown('entryType', $entryType, '', 'id="batchid" ="" class="form-control select2" '); ?> -->
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Entry Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">


                                <div class="form-group" style="margin-bottom:1px">
                                    <label class="col-sm-4"></label>

                                    <div class="col-sm-2"> &nbsp; Year</div>
                                    <div class="col-sm-2"> Month</div>
                                    <div class="col-sm-2"> &nbsp; Day</div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Period</label>

                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'priodYY', "id" => "priodYY", "class" => "form-control ", "value" => "12", "maxlength" => "2")); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'priodMM', "id" => "priodMM", "class" => "form-control ", "value" => "00", "maxlength" => "2", "max" => "12")); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'priodDD', "id" => "priodDD", "class" => "form-control ", "value" => "00", "maxlength" => "2", "max" => "31")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Give Engagement Period">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Expire Date <span class="text-danger"> * </span></label>

                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'expireDate', "id" => "expireDate", "class" => "form-control ", "readonly" => "readonly", "value" => $result->EXPIRYDATE, "placeholder" => "Expire date")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Auto Calculate Expire Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <hr style=" margin-bottom: 5px; margin-top: 0;">

                            </div>

                        </div>

                        <div class="col-md-12">
<fieldset class="">                                 <legend class="legend" style
="font-size: 12px;  margin-bottom: 10px;">Posting Unit
</legend>                                 <div class="col-sm-6">
<div class="form-group">                                         <label class
="col-sm-4">Zone</label>

                                        <div class="col-sm-6">
                                            <select class="selectpicker" name="zone" data-width="130px"
                                                    id="zone" data-placeholder="Select Zone" data-live-search="true">
                                                <option value="<?php echo $result->ZONEID ?>"><?php echo $result->ZONE_ID ?></option>
                                                <?php
                                                foreach ($zone as $row):
                                                    ?>
                                                    <option value="<?php echo $row->ADMIN_ID; ?>"><?php echo $row->NAME; ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>

                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                           data-placement="right" data-content="Select Zone">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Area</label>

                                        <div class="col-sm-6">
                                            <select id="area" name="area" class="form-control select2"
                                                    data-placeholder="Select One" aria-hidden="true"
                                                    data-allow-clear="true">
                                                <option value="<?php echo $result->AREAID; ?>"><?php echo $result->AREA_ID; ?></option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                           data-placement="right" data-content="Select Area">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Ship/Establishment</label>

                                        <div class="col-sm-6">
                                            <select id="shift" name="shiftEstablishment" class="form-control select2"
                                                    data-placeholder="Select One" aria-hidden="true"
                                                    data-allow-clear="true">
                                                <option value="<?php echo $result->SHIPESTABLISHMENTID; ?>"><?php echo $result->SHIPESTABLISHMENT_ID; ?></option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                           data-placement="right" data-content="Select Ship/Establishment">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-4">Posting Unit</label>

                                        <div class="col-sm-6">
                                            <select id="postingUnit" name="postingUnit" class="form-control select2"
                                                    data-placeholder="Select One" aria-hidden="true"
                                                    data-allow-clear="true">
                                                <option value="<?php echo $result->POSTINGUNITID; ?>"><?php echo $result->POSTINGUNIT_ID; ?></option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                           data-placement="right" data-content="Select Posting Unit">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4">Posting Date</label>

                                        <div class="col-sm-4 date">
                                            <?php echo form_input(array('name' => 'postingDate', "class" => "datePicker form-control", 'value' => $result->PostDate, 'placeholder' => 'Posting Date')); ?>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                           data-placement="right" data-content="Give a Posting Date">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4">Posting Type</label>

                                        <div class="col-md-6">
                                            <select name="postingType" class="form-control select2"
                                                    data-placeholder="Posting Type" aria-hidden="true"
                                                    data-allow-clear="true">
                                                <option value="<?php echo $result->POSTINGTYPE; ?>"><?php echo $result->POSTING_TYPE; ?></option>
                                                <option value="1">Normal</option>
                                                <option value="2">Instructor</option>
                                                <option value="3">Course</option>
                                                <option value="4">School Staff</option>
                                                <option value="5">Release</option>
                                                <option value="6">Deputation</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover"
                                           data-placement="right" data-content="Give a Posting Date">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>

                            </fieldset>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-second">
                        <legend class="legend">Permanent Address:</legend>
                        <div class="col-md-12">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Division <span class="text-danger"> * </span></label>

                                    <div class="col-sm-6">
                                        <select class="selectpicker" name="division" data-width="130px"
                                                id="division_id" data-placeholder="Select Division" data-live-search="true">
                                            <option value="<?php echo $result->D_ID ?>"><?php echo $result->DIVISION ?></option>
                                            <?php
                                            foreach ($division as $row):
                                                ?>
                                                <option value="<?php echo $row->BD_ADMINID; ?>"><?php echo $row->NAME; ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                        <?php /* echo form_dropdown("division", $division, set_value("division"), "class='selectpicker 'style='width:170px' data-live-search='true' placeholder='Select Division' id='division_id'"); */ ?>

                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please sleect Division name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Select District <span class="text-danger"> * </span></label>

                                    <div class="col-sm-6">
                                        <select id="district_id" name="districtName" class="select2 " style="width:180px" data-live-search = "true">
                                            <option value="<?php echo $result->Dis_ID ?>"><?php echo $result->DISTRICT ?></option>
                                            <?php
                                            foreach ($dis as $value) {
                                                ?>
                                                <option value="<?php echo $value->BD_ADMINID; ?>"><?php echo $value->NAME ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select District Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Police Station<span class="text-danger">*</span></label>

                                    <div class="col-sm-6">
                                        <select id="thana_id" name="policeStation" class="select2 "  style="width:180px" data-live-search="true">
                                            <option value="<?php echo $result->TH_ID; ?>"><?php echo $result->POLICESTATION; ?></option>
                                            <?php
                                            foreach ($thanapolice as $key) {
                                                ?>
                                                <option value="<?php echo $key->BD_ADMINID; ?>"><?php echo $key->NAME; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Police Station/Thana Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Permanent Address<span class="text-danger">*</span></label>

                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddress', "class" => "form-control", "value" => $result->PERMANENTADDRESS, 'placeholder' => 'Permanent Address', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Permanent Address">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">স্থায়ী ঠিকানা বাংলায়</label>

                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddressInBan', "class" => "form-control", "value" => $result->PERMANENTADDRESSINBANGLA, 'placeholder' => 'স্থায়ী ঠিকানা বাংলায়', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="স্থায়ী ঠিকানা বাংলায়">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <legend class="legend">Next of kin Information And Address:</legend>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Next of kin<span class="text-danger">*</span></label>

                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'nextOfKinName', "class" => "form-control ", "value" => $result->NEXTOFKIN, '' => '', 'placeholder' => 'Next of kin name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please Training Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">আত্মীয়র নাম বাংলায়</label>

                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'nextOfKinNameInBan', "class" => "form-control", "value" => $result->NEXTOFKININBANGLA, 'placeholder' => 'আত্মীয়র নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select training name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Relation of next kin<span
                                            class="text-danger">*</span></label>

                                    <div class="col-sm-6">
                                        <select class="selectpicker" name="relationOfNextKin" data-width="130px"
                                                id="relation" data-placeholder="Select Relation" data-live-search="true">
                                            <option value="<?php echo $result->RE_ID ?>"><?php echo $result->RELATION_ID ?></option>
                                            <?php
                                            foreach ($relation1 as $row):
                                                ?>
                                                <option value="<?php echo $row->RELATION_ID; ?>"><?php echo $row->NAME; ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                        <?php /* echo form_dropdown('relationOfNextKin', $relation, '', 'id="relation" class="selectpicker " style="width: 200px;" data-live-search="true" '); */ ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select District Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Next of kin address<span
                                            class="text-danger">*</span></label>

                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'nextOfKinAddress', "class" => "form-control", "value" => $result->NEXTOFKINADDRESS, 'placeholder' => 'Next of kin address', 'id' => 'nextOfkinAdd', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Enter Kin Address">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">আত্মীয়র ঠিকানা বাংলায়</label>

                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'nextOfKinAddressInBangla', "class" => "form-control", "value" => $result->NEXTOFKINADDRESSINBANGLA, 'placeholder' => 'আত্মীয়র ঠিকানা বাংলায়', 'id' => 'nextOfKinInBangla', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="আত্মীয়র ঠিকানা বাংলায়">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-third">
                        <div class="form-group col-md-12">
                            <legend class="legend">Academic Information:</legend>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <label class="col-md-4">Exam System<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <select name="examSystem[]" id="examSystem"
                                                class="select2 " data-live-search="true"
                                                data-placeholder="Select Exam System" aria-hidden="true"
                                                data-allow-clear="true" data-width="180px">
                                            <option value="">Select Exam System</option>
                                            <option value="1">Traditional</option>
                                            <option value="2">GPA</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Exam System">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-3">Exam Name<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <?php echo form_dropdown('examNameDesc[]', $exam_name, '', 'id="examNameDesc" class="select2 " data-live-search="true" data-placeholder="Select Exam Name" aria-hidden="true" data-allow-clear="true" data-width="180px"'); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Exam Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <label class="col-md-4">Subject/Group<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <?php echo form_dropdown('subjectGroupDesc[]', $subject_group, '', 'id="subjectGroupDesc" data-live-search="true" class="select2 " data-placeholder="Select Subject Group" aria-hidden="true" data-allow-clear="true" data-width="180px"'); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Subject/Group Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-3">Passing Year<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <select name="passingYear[]" id="passingYearid"
                                                class="select2 " data-live-search="true"
                                                data-placeholder="Select Year" aria-hidden="true"
                                                data-allow-clear="true" data-width="180px">
                                            <option value="">Select Year</option>
                                            <?php
                                            for ($i = 1995; $i <= date("Y"); $i++) {
                                                ?>
                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Passing Year">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <label class="col-md-4">Board/University <span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <?php echo form_dropdown('baordUniversityDesc[]', $board_university, '', 'id="baordUniversityDesc" data-live-search="true" class="select2 " data-placeholder="Select Board/University" aria-hidden="true" data-allow-clear="true" data-width="180px"'); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Exam Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-3">Result<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <?php echo form_dropdown('result[]', $result12, '', 'id="results"  class="select2 " data-placeholder="Select Result" aria-hidden="true" data-allow-clear="true" data-width="180px"'); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Result">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6" id="gpadiv">
                                    <label class="col-md-4">GPA<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-4">
                                        <?php echo form_input(array('name' => 'gpa[]', 'id' => 'gpa', 'type' => 'text', "class" => "form-control numbersOnly ", 'placeholder' => 'Enter GPA')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select GPA">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div id="traditional" class="col-md-6" style="display:none">
                                    <label class="col-md-4">Total Marks</label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'traditional[]', 'id' => 'traditionals', 'type' => 'text', "class" => "form-control numbersOnly ", 'placeholder' => 'Enter Marks')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select GPA">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>


                                <div class="col-md-6 percentage">
                                    <label class="col-md-3">Percentage<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-5">
                                        <?php echo form_input(array('name' => 'percent[]', 'id' => 'percent', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Percentage')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please select Percentage">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6">
                                    <label class="col-sm-4">Result Description<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'resultdesc[]', 'id' => 'resultDesc', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Result Description')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Please Enter Result Description">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <label class="col-md-3">Education Status<span
                                            class="text-danger">*</span></label>

                                    <div class="col-md-6">
                                        <select id="eduStatusDesc" name="eduStatusDesc[]" class="select2 "
                                                data-live-search="true"
                                                data-placeholder="Select Education Status" aria-hidden="true"
                                                data-allow-clear="true" style="width: 100%;">
                                            <option value="">Select Education Status</option>
                                            <option value="1">Entry Education</option>
                                            <option value="2">Entry Period Highest</option>
                                            <option value="3">Last</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover"
                                       data-placement="right" data-content="Select Education Status">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="col-md-6"></div>
                                <div class="col-md-6">
                                    <label class="col-md-1"><span
                                            class="glyphicon glyphicon-arrow-right"></span></label>

                                    <div class="xh col-md-6 " style="text-align: right;">
                                        <span class="btn btn-xs btn-success" id="addTable">
                                            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <table id="selected_pc" class="table table-striped table-bordered" width="100%"
                                   cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Exam System &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Exam Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subject/Group&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th>Passing Year</th>
                                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Board/University&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th>GPA/Result</th>
                                        <th>Total Marks</th>
                                        <th>Percentage</th>
                                        <th>Result Description</th>
                                        <th>Education Status Desc</th>
                                        <th>Remove</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($academicInfo as $result1) {
                                        ?>
                                        <tr>
                                            <td>
                                            <input type="hidden"  name="pracademicTable[]" id="pracademic"  class="form-control" placeholder="pracademic" value="<?php echo $result1->AcademicID ?>">
                                                <select name="examSystemTable[]" id="examSystem"
                                                    class="select2 " data-live-search="true"
                                                    data-placeholder="Select Exam System" aria-hidden="true"
                                                    data-allow-clear="true" data-width="130px">

                                                    <option value="<?php echo $result1->EvaluationSystem ?>"><?php echo $result1->Exam_System ?></option>
                                                    <option value="1">Traditional</option>
                                                    <option value="2">GPA</option>
                                                </select>

                                            </td>
                                            <td>
                                                <select name="examNameDescTable[]" id="examSystem"
                                                        class="select2 " data-live-search="true"
                                                        data-placeholder="Select Exam System" aria-hidden="true"
                                                        data-allow-clear="true" data-width="140px">

                                                        <option value="<?php echo $result1->ExamID ?>"><?php echo $result1->Exam_Name ?></option>
                                                        <?php
                                                            foreach ($exam_name1 as $key) {
                                                                ?>

                                                                <option value="<?php echo $key->GOVT_EXAMID?>"><?php echo $key->NAME?></option>
                                                                <?php
                                                            }
                                                        ?>


                                                </select>

                                            </td>
                                            <td>
                                                <select name="subjectGroupDescTable[]" id="examSystem"
                                                            class="select2 " data-live-search="true"
                                                            data-placeholder="Select Exam System" aria-hidden="true"
                                                            data-allow-clear="true" data-width="140px">

                                                            <option value="<?php echo $result1->SubjectID ?>"><?php echo $result1->Subject ?></option>
                                                            <?php
                                                                foreach ($subject_group1 as $keySub) {
                                                                    ?>

                                                                    <option value="<?php echo $keySub->SUBJECT_GROUPID?>"><?php echo $keySub->NAME?></option>
                                                                    <?php
                                                                }
                                                            ?>


                                                </select>

                                            </td>
                                            <td>
                                                <select name="passingYearTable[]" id="passingYearid"
                                                    class="select2 " data-live-search="true"
                                                    data-placeholder="Select Year" aria-hidden="true"
                                                    data-allow-clear="true" data-width="100px">
                                                <option value="<?php echo $result1->PassingYear ?>"><?php echo $result1->PassingYear ?></option>
                                                <?php
                                                for ($i = 1995; $i <= date("Y"); $i++) {
                                                    ?>
                                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>

                                            </td>
                                            <td>
                                                <select name="baordUniversityDescTable[]" id="examSystem"
                                                                class="select2 " data-live-search="true"
                                                                data-placeholder="Select Exam System" aria-hidden="true"
                                                                data-allow-clear="true" data-width="175px">

                                                                <option value="<?php echo $result1->BoardID ?>"><?php echo $result1->Board_Name ?></option>
                                                                <?php
                                                                    foreach ($board_university1 as $keyBoard) {
                                                                        ?>

                                                                        <option value="<?php echo $keyBoard->BOARD_CENTERID?>"><?php echo $keyBoard->NAME ?></option>
                                                                        <?php
                                                                    }
                                                                ?>


                                                </select>

                                            </td>
                                            <td>
                                                <input type="text" value="<?php echo $result1->Exam_Gred ?>" name="result[]" id="result"  class="form-control " placeholder="Exam Gred" >
                                                <input type="hidden" value="<?php echo $result1->ExamGradeID ?>" name="resultTable[]" id="result"  class="form-control " placeholder="Exam Gred" >
                                            </td>
                                            <td>
                                                <input type="text" value="<?php echo $result1->TotalMarks ?>" name="traditionalTable[]" id="SENIORITY_1"  class="form-control " placeholder="Total Marks" >
                                            </td>
                                            <td>
                                                <input type="text" value="<?php echo $result1->Percentage ?>" name="percentTable[]" id="percent"  class="form-control " placeholder="Percentage" >
                                            </td>
                                            <td>
                                                <input type="text" value="<?php echo $result1->ResultDescription ?>" name="resultdescTable[]" id="resultdesc"  class="form-control " placeholder="Result Description" >
                                            </td>
                                            <td>
                                                <select id="eduStatusDesc" name="eduStatusDescTable[]" class="select2 "
                                                    data-live-search="true"
                                                    data-placeholder="Select Education Status" aria-hidden="true"
                                                    data-allow-clear="true" style="width: 100%;">
                                                    <option value="<?php echo $result1->EDUST ?>"><?php
                                                    if($result1->EDUST == '1')
                                                    {
                                                        echo "Entry Education";
                                                    }
                                                    elseif ($result1->EDUST == '2') {
                                                         echo "Entry Period Highest";
                                                     }
                                                     else
                                                     {
                                                        echo "Last";
                                                     }

                                                    ?></option>
                                                    <option value="1">Entry Education</option>
                                                    <option value="2">Entry Period Highest</option>
                                                    <option value="3">Last</option>
                                            </select>

                                            </td>
                                            <td><input type="button" class="btn btn-danger" name="Remove" value="Remove"></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <fieldset class="">
                                <legend class="legend">Physical Information</legend>
                            </fieldset>
                            <div class="col-md-12">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-3">Height<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-3">
                                            <input name="ftName" id="feetData" type="text"
                                                   class="form-control inputData " placeholder="ft"/>
                                        </div>
                                        <div class="col-sm-3">
                                            <input name="incName" id="incData" type="text"
                                                   class="form-control inputData " placeholder="Inc"/>
                                        </div>
                                        <div class="col-sm-3">
                                            <input name="cmName" id="cmData" type="text" class=" form-control"
                                                   placeholder="cm" value="<?php echo $result->HEIGHT; ?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-3">Weight<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-3">
                                            <input name="kgWeight" id="kgData" type="text" class="form-control "
                                                   placeholder="kg" value="<?php echo $result->WEIGHT; ?>"/>
                                        </div>
                                        <div class="col-sm-3">
                                            <input name="lbWeight" id="lbData" type="text" class="form-control "
                                                   placeholder="lb"/>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-3">Chest<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-3">
                                            <input name="incChest" id="inchChest" type="text"
                                                   class="form-control " placeholder="inch"/>
                                        </div>
                                        <div class="col-sm-3">
                                            <input name="cmChest" id="cmChests" type="text"
                                                   class="form-control " placeholder="cm" value="<?php echo $result->CHEST; ?>"/>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Eye Color <span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-6">
                                            <select name="eyeColor" class="select2 "
                                                    data-live-search="true" data-width="150px"
                                                    data-placeholder="Select Eye Color">
                                                <option value="">Select Eye Color</option>
                                                <option value="1"<?php echo ($result->EYECOLOR == 1) ? 'selected' : '' ?>>Black</option>
                                                <option value="2"<?php echo ($result->EYECOLOR == 2) ? 'selected' : '' ?>>Brown</option>
                                                <option value="3"<?php echo ($result->EYECOLOR == 3) ? 'selected' : '' ?>>Golden</option>
                                                <option value="4"<?php echo ($result->EYECOLOR == 4) ? 'selected' : '' ?>>White</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                $sailor_id = $this->uri->segment(4, 0);
                                ?>

                                <div class=" col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Eye Sight <span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-6">
                                            <select name="eyeSight" class="select2 "
                                                    data-live-search="true" data-placeholder="Select Eye Sight"
                                                    data-width="150px">
                                                <option value="">Select Eye Sight</option>
                                                <option value="1"<?php echo ($result->EYESIGHT == 1) ? 'selected' : '' ?>>6/6</option>
                                                <option value="2"<?php echo ($result->EYESIGHT == 2) ? 'selected' : '' ?>>6/9</option>
                                                <option value="3"<?php echo ($result->EYESIGHT == 3) ? 'selected' : '' ?>>6/12</option>
                                                <option value="4"<?php echo ($result->EYESIGHT == 4) ? 'selected' : '' ?>>6/18</option>
                                                <option value="5"<?php echo ($result->EYESIGHT == 5) ? 'selected' : '' ?>>6/60</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Hair Color<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-6">
                                            <select name="hairColor" class="select2 "
                                                    data-live-search="true" data-width="150px"
                                                    data-placeholder="Select Hair Color">
                                                <option value="">Select Hair Color</option>
                                                <option value="1"<?php echo ($result->HAIRCOLOR == 1) ? 'selected' : '' ?>>Black</option>
                                                <option value="2"<?php echo ($result->HAIRCOLOR == 2) ? 'selected' : '' ?>>Brown</option>
                                                <option value="3"<?php echo ($result->HAIRCOLOR == 3) ? 'selected' : '' ?>>Golden</option>
                                                <option value="4"<?php echo ($result->HAIRCOLOR == 4) ? 'selected' : '' ?>>White</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Blood Group<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-6">
                                            <select name="bloodGroup" class="select2 "
                                                    data-live-search="true" data-placeholder="Select Blood Group"
                                                    data-width="150px">
                                                <option value="">Select Blood Group</option>
                                                <option value="1"<?php echo ($result->BLOODGROUP == 1) ? 'selected' : '' ?>>A+</option>
                                                <option value="2"<?php echo ($result->BLOODGROUP == 2) ? 'selected' : '' ?>>A-</option>
                                                <option value="3"<?php echo ($result->BLOODGROUP == 3) ? 'selected' : '' ?>>B+</option>
                                                <option value="4"<?php echo ($result->BLOODGROUP == 4) ? 'selected' : '' ?>>B-</option>
                                                <option value="5"<?php echo ($result->BLOODGROUP == 5) ? 'selected' : '' ?>>O+</option>
                                                <option value="6"<?php echo ($result->BLOODGROUP == 6) ? 'selected' : '' ?>>O-</option>
                                                <option value="7"<?php echo ($result->BLOODGROUP == 7) ? 'selected' : '' ?>>AB+</option>
                                                <option value="8"<?php echo ($result->BLOODGROUP == 8) ? 'selected' : '' ?>>AB-</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Complexion<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-6">
                                            <select name="complexion" class="select2 "
                                                    data-live-search="true" data-placeholder="Select Complexion"
                                                    data-width="150px">
                                                <option value="">Select Complexion</option>
                                                <option value="1"<?php echo ($result->COMPLEXION == 1) ? 'selected' : '' ?>>Fair</option>
                                                <option value="2"<?php echo ($result->COMPLEXION == 2) ? 'selected' : '' ?>>Dark</option>
                                                <option value="3"<?php echo ($result->COMPLEXION == 3) ? 'selected' : '' ?>>Medium Dark</option>
                                                <option value="4"<?php echo ($result->COMPLEXION == 4) ? 'selected' : '' ?>>Light Dark</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Sex<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-6">
                                            <select name="sex" class="select2 "
                                                    data-live-search="true" data-placeholder="Select Sex"
                                                    data-width="150px">
                                                <option value="">Select Sex</option>
                                                <option value="1"<?php echo ($result->SEX == 1) ? 'selected' : '' ?>>Male</option>
                                                <option value="2"<?php echo ($result->SEX == 2) ? 'selected' : '' ?>>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Id Marks<span
                                                class="text-danger">*</span></label>

                                        <div class="col-sm-8">
                                            <?php echo form_textarea(array('name' => 'idMarks', 'class' => 'form-control ', "value" => $result->IDMARKS, 'placeholder' => 'ID Marks', 'cols' => '8', 'rows' => '1')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-left: 0 !important;">
                                <label class="col-sm-0">&nbsp;</label>

                                <div class="col-sm-8">
                                    <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect"
                                           data-action="SpecialTransection/CapturingNewRecrutiInfo/sailorUpdate/<?php echo $sailor_id; ?>"
                                           data-redirect-action="SpecialTransection/CapturingNewRecrutiInfo/index"
                                           data-type="list" value="submit">
                                </div>

                                <!--                                 data-redirect-action="SpecialTransection/CapturingNewRecrutiInfo/index"-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </form>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 100%;
    }
</style>
<script type="text/javascript">
    //    $(document).on("click", ".step2", function (e) {
    //        var isValid = 0;
    //        var a = 0;
    //        var localNumber = $("#localNumber").val();
    //        var fullName = $(".fullName").val();
    //
    //        if (localNumber == "") {
    //            alert("Select Local Number");
    //            isValid = 1;
    //            return false;
    //        } else if (fullName == "") {
    //            alert("Select Full Name");
    //            isValid = 1;
    //            return false;
    //        }
    //    })


    $(document).ready(function () {
        $('#division_id').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('setup/common/ajax_get_division'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#district_id').html(data);
                }
            });
        });
        $('#district_id').change(function () {
            var selectedValue = $(this).val();
            var url = '<?php echo site_url('setup/common/ajax_get_district') ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#thana_id').html(data);
                }
            });
        });

        $('#division').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('setup/common/ajax_get_division'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#district').html(data);
                }
            });
        });
        $('#district').change(function () {
            var selectedValue = $(this).val();
            var url = '<?php echo site_url('setup/common/ajax_get_district'); ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#thana').html(data);
                }
            });
        });
        $('#zone').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_area'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#area').html(data);
                }
            });
        });
        $('#localNumber').change(function () {
            var selectedValue = $("#localNumber :selected").text().trim();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_local_number'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#localNumberReplace').val(data);
                }
            });
        });
        $('#area').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_shift'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shift').html(data);
                }
            });
        });
        $('#shift').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_postingUnit'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#postingUnit').html(data);
                }
            });
        });
    });
    $("#division").prop("disabled", true);
    $("#district").prop("disabled", true);
    $("#thana").prop("disabled", true);
    $(document).ready(function () {
        handleStatusChanged();
    });
    function handleStatusChanged() {
        $('#checkbox').on('change', function () {
            toggleStatus();
        });
    }
    function toggleStatus() {
        if ($('#checkbox').is(':checked')) {
            $('#check :input').attr('disabled', true);
        } else {
            $('#check :input').removeAttr('disabled');
        }
    }

    $(document).on("click", "#addTable", function (e) {
        var isValid = 0;
        var a = 0;
        var examSystem = $("#examSystem").val();
        var examSystem_name = $("#examSystem :selected").text().trim();
        var exam_id = $("#examNameDesc").val();
        var exam_name = $("#examNameDesc").text().trim();
        var examNameDesc = $("#examNameDesc :selected").val();
        var examNameDesc_name = $("#examNameDesc :selected").text().trim();
        var subjectGroupDesc = $("#subjectGroupDesc :selected").val();
        var subjectGroupDesc_name = $("#subjectGroupDesc :selected").text().trim();
        var subjectValidation = $("#subjectGroupDesc").val();
        var passingYear = $("#passingYearid").val();
        var baordUniversityDesc = $("#baordUniversityDesc :selected").val();
        var baordUniversityDesc_name = $("#baordUniversityDesc :selected").text().trim();
        var boardValidation = $("#baordUniversityDesc").val();
        var result = $("#results :selected").val();
        var result_n = $("#results :selected").text().trim();
        var resultValidation = $("#results").val();
        var resultDesc = $("#resultDesc").val();
        var percent = $("#percent").val();
        var gpa = $("#gpa").val();
        var gpa_name = $("#gpa").text().trim();
        var eduStatusDesc = $("#eduStatusDesc :selected").val();
        var eduStatusDesc_name = $("#eduStatusDesc :selected").text().trim();
        if (examSystem == "") {
            alert("Select Exam System");
            isValid = 1;
            return false;
        } else if (exam_id == "") {
            alert("Select Exam Name");
            isValid = 1;
            return false;
        } else if (subjectValidation == "") {
            alert("Select Subject/Group");
            isValid = 1;
            return false;
        } else if (passingYear == "") {
            alert("Select Passing Year");
            isValid = 1;
            return false;
        } else if (boardValidation == "") {
            alert("Select Board/University");
            isValid = 1;
            return false;
        } else if (resultValidation == "") {
            alert("Select Result");
            isValid = 1;
            return false;
        }
        if (isValid == 0) {
            $.ajax({
                type: "POST",
                url: '<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/getEduDataTable') ?>',
                data: {
                    examSystem: examSystem,
                    examSystem_name : examSystem_name,
                    examNameDesc: examNameDesc,
                    examNameDesc_name : examNameDesc_name,
                    subjectGroupDesc: subjectGroupDesc,
                    subjectGroupDesc_name : subjectGroupDesc_name,
                    passingYear: passingYear,
                    baordUniversityDesc: baordUniversityDesc,
                    baordUniversityDesc_name : baordUniversityDesc_name,
                    result: result,
                    result_n : result_n,
                    resultDesc: resultDesc,
                    eduStatusDesc: eduStatusDesc,
                    eduStatusDesc_name : eduStatusDesc_name,
                    percent: percent,
                    gpa: gpa

                },
                success: function (data) {
                    $(".eximId").each(function () {
                        if ($(this).val() == examNameDesc) {
                            a = 1;
                        }
                    });
                    if (a == 1) {
                        alert("You Already selected this Exam");
                        return false;
                    } else {
                        $("#selected_pc").append(data);
                    }
                }
            });
        }
    })
    $(document).on("click", ".removeEdu", function () {
        $(this).parent().parent("tr").remove();
    });

    function engDate() {
        var year = $("#yearId").val();
        var month = $("#monthId").val();
        var day = $("#dayId").val();
        var expireDate = year + "/" + month + "/" + day;
        console.log(expireDate);
    }

    $('.inputData').keyup(function () {
        var feetValue = $('#feetData').val();
        var inchValue = $('#incData').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/feetconvert'); ?>",
            data: {feetValue: feetValue, inchValue: inchValue},
            dataType: 'html',
            success: function (data) {
                $('#cmData').val(data);
            }
        });
    });

    $('#kgData').keyup(function () {
        var kgValue = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/kgconvert'); ?>",
            data: {kgValue: kgValue},
            dataType: 'html',
            success: function (data) {
                $('#lbData').val(data);
            }
        });
    });

    $('#inchChest').keyup(function () {
        var incChestValue = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/chestconvert'); ?>",
            data: {incChestValue: incChestValue},
            dataType: 'html',
            success: function (data) {
                $('#cmChests').val(data);
            }
        });
    });


    $(document).on("click", "#btnSameAsPermanent", function () {
        if ($(this).is(":checked")) {
            var selectDistVal = $("#district_id :selected").text().trim();
            var selectThanaVal = $("#thana_id :selected").text();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_NextOfKinAddress'); ?>",
                data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
                dataType: 'html',
                success: function (data) {
                    $('#nextOfkinAdd').val(data);
                }
            });
        } else {
            $('#nextOfkinAdd').val('');
        }


    });

    /* $(document).on("click", "#btnSameAsPermanent", function () {
     var selectDistVal = $("#district_id").val();
     var selectThanaVal = $("#thana_id").val();
     $.ajax({
     type: "POST",
     url: "<?php // echo site_url('SpecialTransection/CapturingNewRecrutiInfo/NextOfKinAddressInBangla');                                             ?>",
     data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
     dataType: 'html',
     success: function (data) {
     $('#nextOfKinInBangla').val(data);
     }
     });
     });*/


    $('#thana').change(function () {
        var selectDistVal = $("#district :selected").text().trim();
        var selectThanaVal = $("#thana :selected").text().trim();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_NextOfKinAddressNotSame'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfkinAdd').val(data);
            }
        });
    });

    $('#thana').change(function () {
        var selectDistVal = $("#district").val();
        var selectThanaVal = $("#thana").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_NextOfKinAddressInBangla'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfKinInBangla').val(data);
            }
        });
    });

    function AddDate(oldDate, offset, offsetType) {
        var year = parseInt(oldDate.getFullYear());
        var month = parseInt(oldDate.getMonth());
        var date = parseInt(oldDate.getDate());
        var hour = parseInt(oldDate.getHours());
        var newDate;
        switch (offsetType) {

            case "Y":
            case "y":

                newDate = new Date(year + offset, month, date, hour);
                break;
            case "M":
            case "m":

                newDate = new Date(year, month + offset, date, hour);
                break;

            case "D":
            case "d":

                newDate = new Date(year, month, date + offset, hour);
                break;

            case "H":
            case "h":
                newDate = new Date(year, month, date, hour + offset);
                break;

        }
        return newDate;

    }
    $("#priodDD").on('blur', function () {
        var dateStr = $("#engagementDate").val();
        var hiddenDate = $("#engPeriod").val();

        dArr = dateStr.split("-");
        var EngageDate = dArr[2] + "-" + dArr[1] + "-" + dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val());
        if (month > 12 || date > 31) {
            $("#priodMM").val('00');
            $("#priodDD").val('00');
            $("#expireDate").val('');
            $("#engageFor").val('');
        } else {
            if ($("#priodYY").val().length == 0) {
                year = 0;
            }
            if ($("#priodMM").val().length == 0) {
                month = 0;
            }
            if ($("#priodDD").val().length == 0) {
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if (exp_day.toString().length == 1) {
                exp_day = "0" + exp_day;
            }
            var exp_month = exp_date.getMonth() + 1;
            if (exp_month.toString().length == 1) {
                exp_month = "0" + exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year + "/" + exp_month + "/" + exp_day);

            /*start Engage for Year*/
            var end = new Date(exp_year + "-" + exp_month + "-" + exp_day);
            var cDate = new Date(EngageDate);
            var diff = new Date(end - cDate);
            var totalDays = diff / 1000 / 60 / 60 / 24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if (totalYears.toString().length == 1) {
                totalYears = "0" + totalYears;
            }
            if (totalMonths.toString().length == 1) {
                totalMonths = "0" + totalMonths;
            }
            if (remainingDays.toString().length == 1) {
                remainingDays = "0" + remainingDays;
            }
            $("#engageFor").val(totalYears + "" + totalMonths + "" + remainingDays);

            /*end Engage for Year*/
        }

    });
    $("#priodMM").on('blur', function () {
        var dateStr = $("#engagementDate").val();
        dArr = dateStr.split("-");
        var EngageDate = dArr[2] + "-" + dArr[1] + "-" + dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val());
        if (month > 12 || date > 31) {
            $("#priodMM").val('00');
            $("#priodDD").val('00');
            $("#expireDate").val('');
            $("#engageFor").val('');
        } else {
            if ($("#priodYY").val().length == 0) {
                year = 0;
            }
            if ($("#priodMM").val().length == 0) {
                month = 0;
            }
            if ($("#priodDD").val().length == 0) {
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if (exp_day.toString().length == 1) {
                exp_day = "0" + exp_day;
            }
            var exp_month = exp_date.getMonth() + 1;
            if (exp_month.toString().length == 1) {
                exp_month = "0" + exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year + "/" + exp_month + "/" + exp_day);

            /*start Engage for Year*/
            var end = new Date(exp_year + "-" + exp_month + "-" + exp_day);
            var cDate = new Date(EngageDate);
            var diff = new Date(end - cDate);
            var totalDays = diff / 1000 / 60 / 60 / 24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if (totalYears.toString().length == 1) {
                totalYears = "0" + totalYears;
            }
            if (totalMonths.toString().length == 1) {
                totalMonths = "0" + totalMonths;
            }
            if (remainingDays.toString().length == 1) {
                remainingDays = "0" + remainingDays;
            }
            $("#engageFor").val(totalYears + "" + totalMonths + "" + remainingDays);

            /*end Engage for Year*/
        }
    });
    $("#priodYY").on('blur', function () {
        var dateStr = $("#engagementDate").val();
        dArr = dateStr.split("-");
        var EngageDate = dArr[2] + "-" + dArr[1] + "-" + dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val());
        if (month > 12 || date > 31) {
            $("#priodMM").val('00');
            $("#priodDD").val('00');
            $("#expireDate").val('');
            $("#engageFor").val('');
        } else {
            if ($("#priodYY").val().length == 0) {
                year = 0;
            }
            if ($("#priodMM").val().length == 0) {
                month = 0;
            }
            if ($("#priodDD").val().length == 0) {
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if (exp_day.toString().length == 1) {
                exp_day = "0" + exp_day;
            }
            var exp_month = exp_date.getMonth() + 1;
            if (exp_month.toString().length == 1) {
                exp_month = "0" + exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year + "/" + exp_month + "/" + exp_day);

            /*start Engage for Year*/
            var cDate = new Date(EngageDate);
            var end = new Date(exp_year + "-" + exp_month + "-" + exp_day);
            var diff = new Date(end - cDate);
            var totalDays = diff / 1000 / 60 / 60 / 24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if (totalYears.toString().length == 1) {
                totalYears = "0" + totalYears;
            }
            if (totalMonths.toString().length == 1) {
                totalMonths = "0" + totalMonths;
            }
            if (remainingDays.toString().length == 1) {
                remainingDays = "0" + remainingDays;
            }

            $("#engageFor").val(totalYears + "" + totalMonths + "" + remainingDays);
            /*end Engage for Year*/
        }
    });

    $(document).on('keyup', '.numbersOnly', function () {
        var val = $(this).val();
        if (isNaN(val)) {
            val = val.replace(/[^0-9\.]/g, '');
            if (val.split('.').length > 2) {
                val = val.replace(/\.+$/, "");
            }
        }
        $(this).val(val);
    });

    $(document).ready(function () {
        $("#examSystem").change(function () {
            $(this).find("option:selected").each(function () {
                if ($(this).attr("value") == "1") {
                    $("#gpadiv").hide();
                    $("#traditional").show();
                }
                else if ($(this).attr("value") == "2") {
                    $("#traditional").hide();
                    $("#gpadiv").show();
                }
                else {
                    $("#traditional").hide();
                }
            });
        }).change();
    });
    $('#selected_pc').removeAttr('width').DataTable( {
        "scrollX": true,
        "scrollX": true,
        "bPaginate": false,
        "bFilter": false,
        "bInfo": false,
        "columnDefs": [
            { width: '100px', targets: 0 },
            { width: '100px', targets: 1 },
            { width: '100px', targets: 2 },
            { width: '100px', targets: 3 },
            { width: '100px', targets: 4 },
            { width: '100px', targets: 5 },
            { width: '100px', targets: 6 },
            { width: '100px', targets: 7 },
            { width: '100px', targets: 8 },
            { width: '100px', targets: 9 },
            { width: '100px', targets: 10 },


        ],
        "fixedColumns": true

    } );


</script>
