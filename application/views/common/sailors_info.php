<script>
	/*start search sailorInfo, rank_name by Official Number */
    $("#officialNo").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:1},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailor",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);
                        $(".alertSMS").html('');
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');
                        $(".alertSMS").html('Invalid Officer Number');
                    }
                }
            });
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');
            $(".alertSMS").html('');
        }
    });
    /*end Official Number search part*/

    /*start search sailorInfo, rank_name, ship_establishment, Posting_unit, Posting_date,Promotion Date, Seniority Date, Is Acting, Is Not Qualified by Official Number */
    $("#officialNumber").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:1},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailorInfoByOfficalNo",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".branchId").val(data['BRANCHID']);
                        $(".rank").val(data['RANK_NAME']);
                        $(".rankId").val(data['RANKID']);
                        $(".SHIP_ESTALISHMENT").val(data['SHIP_ESTABLISHMENT']);
                        $(".PostingUnit").val(data['POSTING_UNIT_NAME']);
                        $(".PostingDate").val(data['POSTING_DATE']);
                        $(".promotionDate").val(data['PROMOTION_DATE']);
                        $(".seniorityDate").val(data['SENIORITY_DATE']);
                        $(".entryType").val(data['ENTRY_TYPE_NAME']);
                        $(".alertSMS").html('');
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".branchId").val('');
                        $(".rank").val('');
                        $(".rankId").val('');
                        $(".SHIP_ESTALISHMENT").val('');
                        $(".PostingUnit").val('');
                        $(".PostingDate").val('');
                        $(".promotionDate").val('');
                        $(".seniorityDate").val('');
                        $(".entryType").val('');
                        $(".alertSMS").html('Invalid Officer Number');
                    }
                }
            });
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".branchId").val('');
            $(".rank").val('');
            $(".rankId").val('');
            $(".SHIP_ESTALISHMENT").val('');
            $(".PostingUnit").val('');
            $(".PostingDate").val('');
            $(".promotionDate").val('');
            $(".seniorityDate").val('');
            $(".entryType").val('');
            $(".alertSMS").html('');
        }
    });
    /*end Official Number search part*/

    /*Search Official Number in a sailor & duplicate check official number*/
    $(document).on('blur', '.OFFICIAL_NO', function () {
        var officialNoId = $(this).attr('id');
        var officialNo = $(this).val();
        var len = officialNoId.length;
        var lastD = officialNoId.charAt(len-1); /*find id attribute last string*/
        // already selected
        if(inputsHaveDuplicateValues() !== false){
            $("#OFFICIAL_NO_"+lastD).val('');
            alert("This official number is already ");
        }else{
            var url = '<?php echo site_url('setup/common/checkOfficialNo'); ?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: {officialNo: officialNo, sailorStatus:1},
                success: function (data) {
                    if(data == "Y"){
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo site_url('setup/common/searchSailor'); ?>',
                            data: {officeNumber: officialNo, sailorStatus:1},
                            dataType: 'json',
                            success: function (data1) {
                                $("#FULLNAME_"+lastD).val(data1['FULLNAME']);
                                $("#RANK_"+lastD).val(data1["RANK_NAME"]);
                                $("#POST_UNIT_"+lastD).val(data1["POSTING_UNIT_NAME"]);
                                $("#POST_UNIT_DATE_"+lastD).val(data1["POSTING_DATE"]);
                                $("#SHIP_"+ lastD).val(data1['SHIP_ESTABLISHMENT']);

                                switch (data1['CharacterType'] != null) {
                                    case data1['CharacterType'] == 0 :
                                    $("#LAST_YR_CHAR_"+lastD).val('VG');
                                    break;
                                    case data1['CharacterType'] == 1 :
                                    $("#LAST_YR_CHAR_"+lastD).val('VG_');
                                    break;
                                    case data1['CharacterType'] == 2 :
                                    $("#LAST_YR_CHAR_"+lastD).val('GOOD');
                                    break;
                                    case data1['CharacterType'] == 3 :
                                    $("#LAST_YR_CHAR_"+lastD).val('FAIR');
                                    break;
                                    case data1['CharacterType'] == 4 :
                                    $("#LAST_YR_CHAR_"+lastD).val('INDIF');
                                    break;
                                    case data1['CharacterType'] == 5 :
                                     $("#LAST_YR_CHAR_"+lastD).val('BAD');
                                     break;

                                }
                                switch (data1['EfficiencyType'] != null) {
                                    case data1['EfficiencyType'] == 0 :
                                    $("#LAST_YR_EFF_"+lastD).val('SUPER');
                                    break;
                                    case data1['EfficiencyType'] == 1 :
                                    $("#LAST_YR_EFF_"+lastD).val('SAT');
                                    break;
                                    case data1['EfficiencyType'] == 2 :
                                    $("#LAST_YR_EFF_"+lastD).val('MOD');
                                    break;
                                    case data1['EfficiencyType'] == 3 :
                                    $("#LAST_YR_EFF_"+lastD).val('INFER');
                                    break;
                                    case data1['EfficiencyType'] == 4 :
                                    $("#LAST_YR_EFF_"+lastD).val('UT');
                                    break;
                                }
                                $("#LAST_YR_SHIP_"+lastD).val(data1['SHIPID']);
                            }
                        });
                    }else{
                        alert("Please enter valid official number");
                        $("#OFFICIAL_NO_"+lastD).val('');
                        $("#FULLNAME_"+lastD).val('');
                        $("#RANK_"+lastD).val('');
                        $("#POST_UNIT_"+lastD).val('');
                        $("#POST_UNIT_DATE_"+lastD).val('');
                        $("#SHIP_"+ lastD).val('');
                    }
                }
            });
        }


    });
    $(".endDate").datepicker({
        dateFormat: 'mm/dd/yy',
        onSelect: function(dateText, inst) {
            var endDateId = $(this).attr('id');
            var len = endDateId.length;
            var lastD = endDateId.charAt(len-1); /*find id attribute last string*/

            var startDate = $("#startDate").val();
            var start = new Date(startDate);
            var end = new Date(dateText);
            var diff = new Date(end - start);
            var days = diff/1000/60/60/24;
            if(!isNaN(days)){
                if(0<=days){
                    $("#S_DURATIONS_" + lastD).val(days);
                }else{
                    alert("Start date must be less or equal to end date");
                    $("#S_DURATIONS_" + lastD).val('');
                    $("#END_DATE_" + lastD).val('');
                }
            }
        }
    });
    /*training_course name search by training_course type: TRAINING_TYPE: 2*/
    $(document).on('change', '#TRAINING_COURSE_TYPE', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/trainingCourseName_by_trainingCourseType',
            type: 'POST',
            dataType: 'html',
            data: {TRAINING_TYPE: id},
            beforeSend: function () {
                $("#TRAINING_COURSE_NAME").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#TRAINING_COURSE_NAME').html(data);
            }
        });
    });
    /*exam name search by exam category: exam Type: 1*/
    $(document).on('change', '#EXAM_TYPE_ID', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/examName_by_examType',
            type: 'POST',
            dataType: 'html',
            data: {EXAM_TYPE_ID: id},
            beforeSend: function () {
                $("#EXAM_NAME_ID").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#EXAM_NAME_ID').html(data);
                $('#EXAM_NAME_ID').select2('val', '');
            }
        });

    });
    /*end part exam name search by exam category: exam Type: 1*/

    /*authority area search by authority zone: ADMIN_TYPE: 1*/
    $(document).on('change', '#AUTHORITY_ZONE_ID', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/authorityArea_by_authorityZone',
            type: 'POST',
            dataType: 'html',
            data: {AUTHORITY_ZONE_ID: id},
            beforeSend: function () {
                $("#AUTHORITY_AREA_ID").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#AUTHORITY_AREA_ID').html(data);
                $('#AUTHORITY_AREA_ID').select2('val', '');
            }
        });

    });
    /*end part authority area search by authority zone: ADMIN_TYPE: 1*/

    /*authority area ship search by authority area: ADMIN_TYPE: 2*/
    $(document).on('change', '#SHIP_AREA_ID', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/shipEstablishment_by_area',
            type: 'POST',
            dataType: 'html',
            data: {SHIP_AREA_ID: id},
            beforeSend: function () {
                $("#SHIP_ESTABLISHMENT_ID").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#SHIP_ESTABLISHMENT_ID').html(data);
                $('#SHIP_ESTABLISHMENT_ID').select2('val', '');
            }
        });

    });
    /*Rank search by Branch Id*/
    $(document).on('change', '#BRANCH_ID', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/rankName_by_branch',
            type: 'POST',
            dataType: 'html',
            data: {branchId: id},
            beforeSend: function () {
                $("#RANK_ID").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#RANK_ID').html(data);
                $('#RANK_ID').select2('val', '');
            }
        });

    });

    /*end part authority ship search by authority area: ADMIN_TYPE: 2*/
    /* Posting Unit search by ship_establishment Id*/
    $(document).on('change', '#SHIP_ESTABLISHMENT', function(event) {
        event.preventDefault();
        var id = $(this).val();
        $.ajax({
            url: '<?php echo base_url() ?>setup/common/postingUnit_by_shipEstablishment',
            type: 'POST',
            dataType: 'html',
            data: {shipEst_ID: id},
            beforeSend: function () {
                $("#POSTING_UNIT_ID").html("<img src='<?php echo base_url(); ?>dist/img/loader.gif' />");
            },
            success: function (data) {
                $('#POSTING_UNIT_ID').html(data);
                $('#POSTING_UNIT_ID').select2('val', '');
            }
        });
    });
    /*End Posting Unit search by ship_establishment Id*/

    /*Numbers integer numbers only*/
    $('.integerNumbersOnly').keypress(function(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46
            || event.keyCode === 37 || event.keyCode === 39) {
            return true;
        }
        else if ( key < 48 || key > 57 ) {
            return false;
        }
        else return true;
    });
    /*end part Numbers integer numbers only*/

/*start search sailorInfo, rank_name, ship_establishment, Posting_unit, Posting_date,Promotion Date, Seniority Date, Is Acting, Is Not Qualified by Official Number Only For Acting/NQ task */
    $("#officialNumberact").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:1},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailorInfoByOfficalNo",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);
                        $(".rankId").val(data['RANKID']);
                        $(".DAOID").val(data['DAOID']);
                        $(".DAONUMBER").val(data['DAONUMBER']);
                        $(".SHIPESTABLISHMENTID").val(data['SHIPESTABLISHMENTID']);
                        $(".SHIP_ESTALISHMENT").val(data['SHIP_ESTABLISHMENT']);
                        $(".PostingUnit").val(data['POSTING_UNIT_NAME']);
                        $(".PostingDate").val(data['POSTING_DATE']);
                        $(".promotionDate").val(data['PROMOTION_DATE']);
                        $(".seniorityDate").val(data['SENIORITY_DATE']);
                        $(".entryType").val(data['ENTRY_TYPE_NAME']);
                        $(".ISNOTQUALIFIED").val(data['ISNOTQUALIFIED']);
                        $(".ISACTING").val(data['ISACTING']);
                        var act = $(".ISACTING").val();
                        if(act == 1)
                        {
                           $(".alertSMS").html('');
                        }
                        else
                        {
                            $(".alertSMS").html('Selected Sailor Has No Acting Rules');
                            $(".formSubmitWithRedirect").prop('disabled', true);

                        }
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');
                        $(".rankId").val('');
                        $(".SHIP_ESTALISHMENT").val('');
                        $(".PostingUnit").val('');
                        $(".PostingDate").val('');
                        $(".promotionDate").val('');
                        $(".seniorityDate").val('');
                        $(".entryType").val('');
                        $(".ISNOTQUALIFIED").val('');
                        $(".ISACTING").val('');
                        $(".alertSMS").html('Invalid Officer Number');
                    }
                }
            });
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');
            $(".SHIP_ESTALISHMENT").val('');
            $(".PostingUnit").val('');
            $(".PostingDate").val('');
            $(".promotionDate").val('');
            $(".seniorityDate").val('');
            $(".entryType").val('');
            $(".ISNOTQUALIFIED").val('');
            $(".ISACTING").val('');
            $(".alertSMS").html('');
        }
    });
     /*end Official Number For Acting And Not Qualified*/
      /*start search Reteirment sailorInfo, rank_name, ship_establishment, Posting_unit, Posting_date,Promotion Date, Seniority Date, Is Acting, Is Not Qualified by Official Number */
    $("#retofficialNumber").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:3},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailorInfoByOfficalNo",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);
                        $(".rankId").val(data['RANKID']);
                        $(".SHIP_ESTALISHMENT").val(data['SHIP_ESTABLISHMENT']);
                        $(".PostingUnit").val(data['POSTING_UNIT_NAME']);
                        $(".PostingDate").val(data['POSTING_DATE']);
                        $(".promotionDate").val(data['PROMOTION_DATE']);
                        $(".seniorityDate").val(data['SENIORITY_DATE']);
                        $(".entryType").val(data['ENTRY_TYPE_NAME']);
                        $(".alertSMS").html('');
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');
                        $(".rankId").val('');
                        $(".SHIP_ESTALISHMENT").val('');
                        $(".PostingUnit").val('');
                        $(".PostingDate").val('');
                        $(".promotionDate").val('');
                        $(".seniorityDate").val('');
                        $(".entryType").val('');
                        $(".alertSMS").html('Invalid Officer Number');
                    }
                }
            });
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');
            $(".SHIP_ESTALISHMENT").val('');
            $(".PostingUnit").val('');
            $(".PostingDate").val('');
            $(".promotionDate").val('');
            $(".seniorityDate").val('');
            $(".entryType").val('');
            $(".alertSMS").html('');
        }
    });
    /*end Official Number search part*/
    /*Retirement sailor Search Official Number in a sailor & duplicate check official number*/
    $(document).on('blur', '.RET_OFFICIAL_NO', function () {
        var officialNoId = $(this).attr('id');
        var officialNo = $(this).val();
        var len = officialNoId.length;
        var lastD = officialNoId.charAt(len-1); /*find id attribute last string*/
        // already selected
        if(inputsHaveDuplicateValues() !== false){
            $("#OFFICIAL_NO_"+lastD).val('');
            alert("This official number is already ");
        }else{
            var url = '<?php echo site_url('setup/common/checkOfficialNo'); ?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: {officialNo: officialNo, sailorStatus:3},
                success: function (data) {
                    if(data == "Y"){
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo site_url('setup/common/searchSailor'); ?>',
                            data: {officeNumber: officialNo, sailorStatus:3},
                            dataType: 'json',
                            success: function (data1) {
                                $("#FULLNAME_"+lastD).val(data1['FULLNAME']);
                                $("#RANK_"+lastD).val(data1["RANK_NAME"]);
                                $("#POST_UNIT_"+lastD).val(data1["POSTING_UNIT_NAME"]);
                                $("#POST_UNIT_DATE_"+lastD).val(data1["POSTING_DATE"]);
                                $("#SHIP_"+ lastD).val(data1['SHIP_ESTABLISHMENT']);
                            }
                        });
                    }else{
                        alert("Please enter valid official number");
                        $("#OFFICIAL_NO_"+lastD).val('');
                        $("#FULLNAME_"+lastD).val('');
                        $("#RANK_"+lastD).val('');
                        $("#POST_UNIT_"+lastD).val('');
                        $("#POST_UNIT_DATE_"+lastD).val('');
                        $("#SHIP_"+ lastD).val('');
                    }
                }
            });
        }

    });
    // check dublicate selection value
    function inputsHaveDuplicateValues() {
        var hasDuplicates = false;
        $('.OFFICIAL_NO').each(function () {
            var inputsWithSameValue = $(this).val();
            hasDuplicates = $('.OFFICIAL_NO').not(this).filter(function () {
                return $(this).val() === inputsWithSameValue;
            }).length > 0;
            if (hasDuplicates) return false;
        });
        return hasDuplicates;
    }
</script>
