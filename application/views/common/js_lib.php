<script>
	$(document).on("click", ".formSubmit", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                //alert(label + " Is Empty");
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
                //return false;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm("Are You Sure?")) {
                var frmContent = $(".frmContent").serialize();
                var action_uri = $(this).attr("data-action");
                var type = $(this).attr("data-type");
                var success_action_uri = $(this).attr("data-su-action");
                var ac_type = $(this).attr("");
                var param = "";
                if (type != "list") {
                    param = $(".rowID").val();
                }
                var sn = $("#loader_" + param).siblings("span").text();
                $.ajax({
                    type: "post",
                    data: frmContent,
                    url: "<?php echo site_url(); ?>/" + action_uri,
                    beforeSend: function () {
                        $(".loadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".loadingImg").html("");
                        $(".frmMsg").html(data);
                        $.ajax({
                            type: "post",
                            data: {param: param},
                            url: "<?php echo site_url(); ?>/" + success_action_uri,
                            beforeSend: function () {
                                if (type != "list") {
                                    $("#loader_" + param).removeClass("hidden").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' style='width:10px;' />").siblings("span").addClass("hidden");
                                }
                            },
                            success: function (data1) {
                                //$(".loadingImg").html("");
                                if (type == "list") {
                                    $(".contentArea").html(data1);
                                    /*$(".gridTable").dataTable();*/
                                } else if (type == "msg") {
                                    $('#rinci').html(response).modal();
                                } else {
                                    $("#loader_" + param).addClass("hidden").html("").siblings("span").removeClass("hidden");
                                    $("#row_" + param).html(data1);
                                    $("#loader_" + param).siblings("span").html(sn);
                                }
                            }
                        });
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });
    $(document).on("click", ".deleteItem", function () {
        if (confirm("Are You Sure?")) {
            var item_id = $(this).attr("id");
            var data_field = $(this).attr("data-field");
            var data_tbl = $(this).attr("data-tbl");
            $.ajax({
                type: "post",
                url: "<?php echo site_url('setup/common/deleteItem'); ?>/",
                data: {item_id: item_id, data_field: data_field, data_tbl: data_tbl},
                beforeSend: function () {
                    $("#loader_" + item_id).html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    if (data == "Y") {
                        $("#row_" + item_id).remove();
                    } else {
                        alert("Row Delete Field");
                    }
                }
            });
        } else {
            return false;
        }
    });
    $(document).on("click", ".deleteItemAjaxDataTable", function () {
        if (confirm("Are You Sure?")) {

            var sn = $(this).attr("sn");
            var item_id = $(this).attr("id");
            var data_field = $(this).attr("data-field");
            var data_tbl = $(this).attr("data-tbl");
            $.ajax({
                type: "post",
                url: "<?php echo site_url('setup/common/deleteItem'); ?>/",
                data: {item_id: item_id, data_field: data_field, data_tbl: data_tbl},
                beforeSend: function () {
                    $("#loader_" + sn).html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    if (data == "Y") {
                        $("#row_" + sn).hide();
                        
                    } else {
                        alert("Row Delete Field");
                    }
                }
            });
        } else {
            return false;
        }
    });

    $(document).on('click', '.itemStatus', function () {
        if (confirm("Are You Sure?")) {
            var item_id = $(this).attr("id");
            var data_sn = $(this).attr("data-sn");
            var status = $(this).attr("data-status");
            var data_tbl = $(this).attr("data-tbl");
            var data_field = $(this).attr("data-field");
            var data_fieldId = $(this).attr("data-fieldId");
            var data_su_url = $(this).attr("data-su-url");
            var success_url = "<?php echo site_url() ?>/" + data_su_url;
            if( typeof data_sn === 'undefined'){
                var sn = $("#loader_" + item_id).siblings("span").text();
            }else{
                var sn = $("#loader_" + data_sn).siblings("span").text();                
            }
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url() ?>/setup/common/statusItem',
                data: {
                    item_id: item_id,
                    status: status,
                    data_tbl: data_tbl,
                    data_field: data_field,
                    data_fieldId: data_fieldId
                },
                beforeSend: function () {
                    if( typeof data_sn === 'undefined'){
                        $("#loader_" + item_id).html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    }else{
                        $("#loader_" + data_sn).html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    }
                },
                success: function (data) {
                    if (data == "Y") {
                        $.ajax({
                            type: 'POST',
                            url: success_url,
                            data: {param: item_id},
                            beforeSend: function () {
                                $("#loader_" + item_id).removeClass("hidden").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' style='width:10px;' />").siblings("span").addClass("hidden");
                            },
                            success: function (data1) {
                                if( typeof data_sn === 'undefined'){
                                    $("#loader_" + item_id).addClass("hidden").html("").siblings("span").removeClass("hidden");
                                    $("#row_" + item_id).html(data1);
                                    $("#loader_" + item_id).siblings("span").html(sn);
                                    if(item_id != ''){
                                        $("#row_" + item_id).html(data1);                                    
                                    }
                                }else {
                                    $("#loader_" + data_sn).addClass("hidden").html("").siblings("span").removeClass("hidden");
                                    $("#row_" + data_sn).html(data1);
                                    $("#loader_" + data_sn).siblings("span").html(sn);
                                    if(data_sn != ''){
                                        $("#row_" + data_sn).html(data1);                                    
                                    }
                                }
                            }
                        });
                    } else {
                        return false;
                    }
                }
            });
        } else {
            return false;
        }
    });


    /*submit with redirect root folder*/
    $(document).on("click", ".formSubmitWithRedirect", function () {
        var isValid = 0;
        $('.required').each(function () {
            $(this).keyup(function () {
                $(this).css("border", "1px solid #ccc");
            });
            if ($(this).val() == "") {
                var label = $(this).parent().siblings("label").text();
                //alert(label + " Is Empty");
                $(this).siblings(".validation").html(label + " is required");
                $(this).css("border", "1px solid red");
                isValid = 1;
                //return false;
            } else {
                $(this).siblings(".validation").html("");
                $(this).css("border", "1px solid #ccc");
            }
        });
        if (isValid == 0) {
            if (confirm("Are You Sure?")) {
                var frmContent = $(".frmContent").serialize();
                var action_uri = $(this).attr("data-action");
                var type = $(this).attr("data-type");
                var success_redirect_uri = $(this).attr("data-redirect-action");
                var ac_type = $(this).attr("");
                var param = "";
                if (type != "list") {
                    param = $(".rowID").val();
                }
                var sn = $("#loader_" + param).siblings("span").text();
                $.ajax({
                    type: "post",
                    data: frmContent,
                    url: "<?php echo site_url(); ?>" + action_uri,
                    beforeSend: function () {
                        $(".loadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".loadingImg").html("");
                        $(".frmMsg").html(data);

                        /*window.location.href = "<?php //echo base_url();?>" + success_redirect_uri;*/
                        //window.location.href = "<?php echo base_url();?>"+success_redirect_uri;
                    }
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });

    /*Numbers floating & integer value only*/
    $('.numbersOnly').keypress(function(event) {

        if(event.which == 8 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 9)  /*keyCode is 9 tends to tab*/
            return true;
        else if((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57))
            event.preventDefault();
    });
    /*end part Numbers floating & integer value only*/

    $(document).on('change', '#SHIP_AREA_ID', function () {
        var SHIP_AREA_ID = $(this).val();
        if(SHIP_AREA_ID != ''){
            $("#authorityName").prop("disabled", true);
            $("#authorityName").removeClass('required');

            $("#AUTHORITY_ZONE_ID").prop("disabled", true);
            $("#AUTHORITY_ZONE_ID").removeClass('required');

            $("#AUTHORITY_AREA_ID").prop("disabled", true);
            $("#AUTHORITY_AREA_ID").removeClass('required');

            $("#SHIP_ESTABLISHMENT_ID").addClass('required');           

        }else{
            $("#authorityName").prop("disabled", false);
            $("#authorityName").addClass('required');

            $("#AUTHORITY_ZONE_ID").prop("disabled", false);
            $("#AUTHORITY_ZONE_ID").addClass('required');

            $("#AUTHORITY_AREA_ID").prop("disabled", false);
            $("#AUTHORITY_AREA_ID").addClass('required');
        }
    });
    $(document).on('change', '#AUTHORITY_ZONE_ID', function () {
        var AUTHORITY_ZONE_ID = $(this).val();
        if(AUTHORITY_ZONE_ID != ''){
            $("#authorityName").prop("disabled", true);
            $("#authorityName").removeClass('required');

            $("#SHIP_AREA_ID").prop("disabled", true);
            $("#SHIP_AREA_ID").removeClass('required');

            $("#SHIP_ESTABLISHMENT_ID").prop("disabled", true);
            $("#SHIP_ESTABLISHMENT_ID").removeClass('required');     

            $("#AUTHORITY_AREA_ID").addClass('required');       

        }else{
            $("#authorityName").prop("disabled", false);
            $("#authorityName").addClass('required');

            $("#SHIP_AREA_ID").prop("disabled", false);
            $("#SHIP_AREA_ID").addClass('required');

            $("#SHIP_ESTABLISHMENT_ID").prop("disabled", false);
            $("#SHIP_ESTABLISHMENT_ID").addClass('required'); 
        }
    });
    $(document).on('blur', '#authorityName', function () {
        var authorityName = $(this).val();
        if(authorityName != ''){
            $("#AUTHORITY_ZONE_ID").prop("disabled", true);
            $("#AUTHORITY_ZONE_ID").removeClass('required');

            $("#AUTHORITY_AREA_ID").prop("disabled", true);
            $("#AUTHORITY_AREA_ID").removeClass('required');

            $("#SHIP_AREA_ID").prop("disabled", true);
            $("#SHIP_AREA_ID").removeClass('required');

            $("#SHIP_ESTABLISHMENT_ID").prop("disabled", true);
            $("#SHIP_ESTABLISHMENT_ID").removeClass('required');            

        }else{
            $("#AUTHORITY_ZONE_ID").prop("disabled", false);
            $("#AUTHORITY_ZONE_ID").addClass('required');

            $("#AUTHORITY_AREA_ID").prop("disabled", false);
            $("#AUTHORITY_AREA_ID").addClass('required');

            $("#SHIP_AREA_ID").prop("disabled", false);
            $("#SHIP_AREA_ID").addClass('required');

            $("#SHIP_ESTABLISHMENT_ID").prop("disabled", false);
            $("#SHIP_ESTABLISHMENT_ID").addClass('required'); 
        }
    });
    
</script>
