<td><?php echo $sn; ?></td>
<td><?php echo $row->Code ?></td>
<td><?php echo $row->Name ?></td>
<td><?php echo $row->BN_NAME ?></td>

<td>
    <a class="itemStatus" id="<?php echo $row->Training_Institute_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="Training_Institute_ID" data-field="ACTIVE_STATUS" data-tbl="bn_traininginstitute" data-su-url="setup/TrainingInstitute/examNameById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/TrainingInstitute/view/' . $row->Training_Institute_ID); ?>" title="View Training Institute" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/TrainingInstitute/edit/' . $row->Training_Institute_ID); ?>"  title="Edit Training Institute" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->TRAINING_ORG_ID; ?>" title="Click For Delete" data-type="delete" data-field="TRAINING_ORG_ID" data-tbl="bn_traininginstitute"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>