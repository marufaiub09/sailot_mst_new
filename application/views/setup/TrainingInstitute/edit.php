<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-3 control-label">Code<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'code', 'value' => $result->Code, 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required",'required' => 'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Exam Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'name', 'value' => $result->Name, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Exam name')); ?>
        </div>
    </div>
     <div class="form-group">
        <label class="col-sm-3 control-label">Training Organization<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Training Organization">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="TRAINING_ORG_ID" id="TRAINING_ORG_ID" data-tags="true" data-placeholder="Select Training Organization" data-allow-clear="true">
                <option value="">Select Training Organization</option>
                <?php
                foreach ($organization as $row):
                    ?>
                    <option value="<?php echo $row->TrainingOrganizationID ?>" <?php echo ($result->TRAINING_ORG_ID == $row->TrainingOrganizationID) ? 'selected' : '' ?>><?php echo $row->Name ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', ($result->ACTIVE_STATUS == 1) ? TRUE : FALSE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->Training_Institute_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/TrainingInstitute/updatePart" data-su-action="setup/TrainingInstitute/partList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>

