<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Training Organization</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Training Organization</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php //echo "<PRE>"; print_r($result); exit;?>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->Training_Institute_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->Code ?></td>
                <td><?php echo $row->Name ?></td>
                <td><?php echo $row->BN_NAME ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->Training_Institute_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="Training_Institute_ID" data-field="ACTIVE_STATUS" data-tbl="bn_traininginstitute" data-su-url="setup/TrainingInstitute/examNameById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>

                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" data-modal-size="modal-md" href="<?php echo site_url('setup/TrainingInstitute/view/' . $row->Training_Institute_ID); ?>" title="View Training Institute" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/TrainingInstitute/edit/' . $row->Training_Institute_ID); ?>"  title="Edit Training Institute" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->Training_Institute_ID; ?>" title="Click For Delete" data-type="delete" data-field="Training_Institute_ID" data-tbl="bn_traininginstitute"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>