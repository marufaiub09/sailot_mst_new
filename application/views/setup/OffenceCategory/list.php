<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Entry User</th>
            <th>Entry Date</th>
            <th>Update User</th>
            <th>Update Date</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Entry User</th>
            <th>Entry Date</th>
            <th>Update User</th>
            <th>Update Date</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->OFFENCE_CATAGORYID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->CODE ?></td>
                <td><?php echo $row->NAME ?></td>
                <td><?php echo $row->CRE_BY ?></td>
                <td><?php echo date('d-m-Y', strtotime($row->CRE_DT)) ?></td>
                <td><?php echo $row->UPD_BY ?></td>                
                <td><?php echo date('d-m-Y', strtotime($row->UPD_DT)) ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->OFFENCE_CATAGORYID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="OFFENCE_CATAGORYID" data-field="ACTIVE_STATUS" data-tbl="bn_offencecatagory" data-su-url="setup/OffenceCategory/atById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>
                <td>
        <center>
            <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/OffenceCategory/view/' . $row->OFFENCE_CATAGORYID); ?>" title="View Punishment Causes" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/OffenceCategory/edit/' . $row->OFFENCE_CATAGORYID); ?>"  title="Edit Punishment Causes" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->OFFENCE_CATAGORYID; ?>" title="Click For Delete" data-type="delete" data-field="OFFENCE_CATAGORYID" data-tbl="bn_offencecatagory"><span class="glyphicon glyphicon-trash"></span></a>
        </center>
    </td>
    </tr>
<?php } ?>
</tbody>
</table>