<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">

    <tr>
        <th width="20%">Code</th>
        <td><?php echo $viewdetails->CODE ?></td>
    </tr>
    <tr>
        <th>Name</th>
        <td><?php echo $viewdetails->NAME ?></td>
    </tr>
    <tr>
        <th>Entry User</th>
        <td><?php echo $viewdetails->CRE_BY ?></td>
    </tr>
    <tr>
        <th>Entry Date</th>
        <td><?php echo date('d-m-Y', strtotime($viewdetails->CRE_DT)) ?></td>
    </tr>
    <tr>
        <th>Update Date</th>
        <td><?php echo $viewdetails->UPD_BY ?></td>
    </tr>
    <tr>
        <th>Update User</th>
        <td><?php echo date('d-m-Y', strtotime($viewdetails->UPD_DT)) ?></td>
    </tr>
</table>