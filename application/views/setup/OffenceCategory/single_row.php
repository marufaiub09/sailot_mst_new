<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->CRE_BY ?></td>
<td><?php echo date('d-m-Y', strtotime($row->CRE_DT)) ?></td>
<td><?php echo $row->UPD_BY ?></td>                
<td><?php echo date('d-m-Y', strtotime($row->UPD_DT)) ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->OFFENCE_CATAGORYID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="OFFENCE_CATAGORYID" data-field="ACTIVE_STATUS" data-tbl="bn_offencecatagory" data-su-url="setup/OffenceCategory/atById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>
<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/OffenceCategory/view/' . $row->OFFENCE_CATAGORYID); ?>" title="View Punishment Causes" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/OffenceCategory/edit/' . $row->OFFENCE_CATAGORYID); ?>"  title="Edit Punishment Causes" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->OFFENCE_CATAGORYID; ?>" title="Click For Delete" data-type="delete" data-field="OFFENCE_CATAGORYID" data-tbl="bn_offencecatagory"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>