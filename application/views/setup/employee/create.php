<?php echo validation_errors(); ?>
<!-- form start -->
<?php
echo form_open('', "id='dgdpMainForm' class='form-horizontal'");
?>

<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('salutation') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <?php
                    echo form_dropdown('salutation', $salutation, 'Select Item',
                        'id="office_district" class="form-control" required');
                    ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('first_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" name="first_name" placeholder="<?php echo $this->lang->line('first_name') ?>" required>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('middle_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" name="<?php echo $this->lang->line('middle_name') ?>" placeholder="Middle Name">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('last_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" name="last_name" required placeholder="<?php echo $this->lang->line('last_name') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('father_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" name="father_name" required placeholder="Father Name">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('mother_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" name="mother_name" required placeholder="<?php echo $this->lang->line('mother_name') ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('religion') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <?php
                    echo form_dropdown('religion', $religion, 'Select Item',
                        'id="office_district" class="form-control" required');
                    ?>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('gender') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <?php
                    $option = array(
                        'M' => 'Male',
                        'F' => 'Female',
                        'O' => 'Others');
                    echo form_dropdown('gender', $option, 'M',
                        'id="office_district" class="form-control" required');
                    ?>
                </div>
            </div>
           <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('marital_status') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <?php
                    echo form_dropdown('marital', $marital, 'Select Item',
                        'id="office_district" class="form-control" required');
                    ?>
                </div>
            </div>



        </div>
        <div class="col-md-6">
          

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('phone') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>

                <div class="col-sm-7">
                    <input type="text" class="form-control" required name="<?php echo $this->lang->line('phone') ?>" placeholder="Last Name">
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('nid') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>

                <div class="col-sm-7">
                    <input type="text" class="form-control" name="<?php echo $this->lang->line('nid') ?>" required placeholder="<?php echo $this->lang->line('nid'); ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('joining_date') ?></label>
                <div class="col-sm-7 date">
                    <div class="selectContainer">
                        <div class="input-group input-append date" id="datePicker">
                            <input type="text" class="form-control" required name="date" />
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('email') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>

                <div class="col-sm-7">
                    <input type="email" class="form-control" required name="email" placeholder="Enter Email Address">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('org') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <?php
                    echo form_dropdown('organization', $organization,
                        'Select Item',
                        'id="orgIdDrop" class="form-control" required');
                    ?>
                </div>
            </div>
            <div class="changedBlock" id="changedBlock">
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $this->lang->line('dept') ?></label>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                        <i class="fa fa-question-circle"></i>
                    </a>
                    <div class="col-sm-7">
                        <?php
                        echo form_dropdown('department',
                            array('' => 'Select Levell'), "",
                            'id="departmentDropDown" class="form-control", required');
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $this->lang->line('designation') ?></label>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                        <i class="fa fa-question-circle"></i>
                    </a>
                    <div class="col-sm-7">
                        <?php
                        echo form_dropdown('degisnation',
                            array('' => '--SELECT--'), "",
                            'id="designationDropDown" class="form-control", required');
                        ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label"><?php echo $this->lang->line('rank') ?></label>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                        <i class="fa fa-question-circle"></i>
                    </a>
                    <div class="col-sm-7">
                        <?php
                        echo form_dropdown('rank', array('' => '--SELECT--'),
                            "", 'id="rankDropDown" class="form-control", required');
                        ?>

                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('is_active'); ?></label>
                <div class="col-sm-7">
                    <div class="checkbox checkbox-inline checkbox-primary">
                        <?php
                        echo form_checkbox('ACTIVE_STATUS', '1', TRUE,
                            'class="styled"');
                        ?>
                        <label for="ACTIVE_STATUS"></label>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3">
                    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit') ?></button>
                </div>
            </div>
        </div>

    </div>
</div>

<?php echo form_close(); ?>
<script>
    $(document).on("change","#orgIdDrop",function(){

        var org = $(this).val();
        $desigHtml=$('select#designationDropDown');
        $rankHtml=$('select#rankDropDown');
        $deptHtml=$('select#departmentDropDown');
        $desigHtml.find('option').remove();
        $rankHtml.find('option').remove();
        $deptHtml.find('option').remove();
        $.ajax({
            type: "POST",
            url:"<?php echo site_url('setup/employee/properTyByOrgId'); ?>",
            data: {orgId:org},
            async: true,
            dataType: 'json',
            success: function(result) {
                desig=result.designation;
                rank=result.rank;
                dept=result.department;
                $desigHtml.each(function(){
                    $(this).append("<option value = ' '>--SELECT--</option>");
                    for (var entry1 in desig) {
                        $(this).append("<option  value = '"+entry1+"'>"+desig[entry1]+"</option>");
                    }
                })
                $rankHtml.each(function(){
                    $(this).append("<option value = ' '>--SELECT--</option>");
                    for (var rankID in rank) {
                        $(this).append("<option  value = '"+rankID+"'>"+rank[rankID]+"</option>");
                    }
                })
                $deptHtml.each(function(){
                    $(this).append("<option value = ' '>--SELECT--</option>");
                    for (var entry in dept) {
                        $(this).append("<option value = '"+entry+"'>"+dept[entry]+"</option>");
                    }

                })
            }
        });
    });
</script>