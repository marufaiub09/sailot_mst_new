<form id="dgfpMainForm">
<div class="form-group">
    <label class="col-sm-4 control-label"><?php echo $this->lang->line('dept') ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-7">

        <?php
        echo form_dropdown('department', $department, "",
            'id="level_name" class="form-control" required');
        ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label"><?php echo $this->lang->line('designation') ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-7">
        <?php
        echo form_dropdown('degisnation', $designation, "",
            'id="" class="form-control" required');
        ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label"><?php echo $this->lang->line('rank') ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>

    </a>
    <div class="col-sm-7">
        <?php
        echo form_dropdown('rank', $rank, "",
            'id="" class="form-control" required');
        ?>

    </div>
</div>
</form>