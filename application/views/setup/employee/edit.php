<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open('',
    "id='dgdpMainForm' class='form-horizontal'"); ?>
<div class="row">
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('salutation') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
<?php echo form_dropdown('salutation',
    $salutation, $row->SALUTATION_ID,
    'id="office_district" class="form-control" required'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('first_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" value="<?php echo $row->FIRST_NAME ?>" required name="first_name" placeholder="First Name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('middle_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" value="<?php echo $row->MIDDLE_NAME ?>" name="middle_name" required placeholder="Middle Name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('last_name') ?></label>
                <a class="help-icon" data-container="body"  data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>

                <div class="col-sm-7">
                    <input type="text" class="form-control" value="<?php echo $row->LAST_NAME ?>" name="last_name" required placeholder="Last Name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('father_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" value="<?php echo $row->FATHER_NAME ?>" name="father_name" required placeholder="Father Name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('mother_name') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <input type="text" class="form-control" value="<?php echo $row->MOTHER_NAME ?>" name="mother_name" required placeholder="Mother Name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('religion') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
<?php echo form_dropdown('religion',
    $religion, $row->RELIGION_ID,
    'id="office_district" class="form-control" required'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('gender') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <?php
                    $option = array(
                        'M' => 'Male',
                        'F' => 'Female',
                        'O' => 'Others');
                    echo form_dropdown('gender', $option, $row->GENDER,
                        'id="office_district" class="form-control" required');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('marital_status') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
<?php echo form_dropdown('marital',
    $marital, $row->MARITAL_STATUS,
    'id="office_district" class="form-control" required'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('phone') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>

                <div class="col-sm-7">
                    <input type="text" class="form-control" value="<?php echo $row->MOBILE ?>" required name="mobile" placeholder="Last Name">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('nid') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>

                <div class="col-sm-7">
                    <input type="text" class="form-control"  value="<?php echo $row->NID ?>" name="nid" required placeholder="Last Name">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">J<?php echo $this->lang->line('joining_date') ?></label>
                <div class="col-sm-7 date">
                    <div class="selectContainer">
                        <div class="input-group input-append date" id="datePicker">
                            <input type="text" class="form-control"  value="<?php echo $row->JOINING_DT; ?>" name="date" required />
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('email') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus. Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>

                <div class="col-sm-7">
                    <input type="text" class="form-control"  value="<?php echo $row->EMAIL ?>" required name="email" >
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('org') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
<?php echo form_dropdown('organization',
    $organization, $row->ORG_ID,
    'id="orgIdDrop" class="form-control" required'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('dept') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <?php echo form_dropdown('department',
                        $department, $row->DEPT_ID,
                        'id="departmentDropDown" class="form-control" required'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('rank') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
<?php echo form_dropdown('rank',
    $rank, $row->RANK_ID, 'id="rankDropDown" class="form-control" required'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('designation') ?></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
<?php echo form_dropdown('degisnation',
    $degisnation, $row->DESIG_ID,
    'id="designationDropDown" class="form-control" required'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-4 control-label"><?php echo $this->lang->line('is_active'); ?></label>
                <div class="col-sm-7">
                    <div class="checkbox checkbox-inline checkbox-primary">
<?php echo form_checkbox('ACTIVE_STATUS',
    '1', TRUE, 'class="styled"'); ?>
                        <label for="ACTIVE_STATUS"></label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3">
                    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
<script>
    $(document).on("change","#orgIdDrop",function(){

        var org = $(this).val();
        $desigHtml=$('select#designationDropDown');
        $rankHtml=$('select#rankDropDown');
        $deptHtml=$('select#departmentDropDown');
        $desigHtml.find('option').remove();
        $rankHtml.find('option').remove();
        $deptHtml.find('option').remove();
        $.ajax({
            type: "POST",
            url:"<?php echo site_url('setup/employee/properTyByOrgId'); ?>",
            data: {orgId:org},
            async: true,
            dataType: 'json',
            success: function(result) {
                desig=result.designation;
                rank=result.rank;
                dept=result.department;
                $desigHtml.each(function(){
                    for (var entry1 in desig) {
                        $(this).append("<option  value = '"+entry1+"'>"+desig[entry1]+"</option>");
                    }
                })
                $rankHtml.each(function(){
                    for (var rankID in rank) {
                        $(this).append("<option  value = '"+rankID+"'>"+rank[rankID]+"</option>");
                    }
                })
                $deptHtml.each(function(){
                    for (var entry in dept) {
                        $(this).append("<option value = '"+entry+"'>"+dept[entry]+"</option>");
                    }

                })
            }
        });
    });
</script>