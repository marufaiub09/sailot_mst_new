<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Employee List</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" href="<?php echo site_url('setup/employee/create/'); ?>" class="btn btn-primary btn-xs modalLink"  title="Create Employee">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th># SN</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Designation</th>
                            <th>Rank</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th># SN</th>
                            <th>Name</th>
                            <th>Mobile</th>
                            <th>Designation</th>
                            <th>Rank</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach ($employee as $key => $values) { ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo $values->FULL_NAME; ?></td>
                                <td><?php echo $values->MOBILE; ?></td>
                                <td><?php echo $values->DESIG_NAME; ?></td>
                                <td><?php echo $values->RANK_NAME; ?></td>
                                <td>
                        <center><?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="btn btn-xs btn-success waves-effect waves-button">Active</span>' : '<span class="btn btn-xs btn-danger waves-effect waves-button waves-float">Inactive</span>'; ?></center>
                        </td>
                        <td>
                        <center>
                            <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/employee/view/' . $values->EMP_ID); ?>" title="Employee View"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/employee/edit/' . $values->EMP_ID); ?>" title="Employee Edit" type="button"><span class="edit_division glyphicon glyphicon-edit"></span></a>
    <!--                            <a class="btn btn-danger btn-xs" href="<?php echo site_url('setup/employee/delete/' . $values->EMP_ID); ?>" title="Delete" type="button"><span class="glyphicon glyphicon-trash"></span></i></a>-->
                        </center>
                        </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

