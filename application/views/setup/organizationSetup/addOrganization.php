<style type="text/css">
    .modal-dialog{
        width: 55%!important;
    }
    .col-sm-6 .form-group{
        clear: both !important;
        margin-bottom: 10px !important;
    }
    #imagePreview {
        width: 130px;
        height: 130px;
        background-position: center center;
        background-size: cover;
        -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
        display: inline-block;
        display: none;
        border: 1px solid #002166;
        float: left;
        margin-top: 10px;
    }

</style>
<?php echo validation_errors(); ?> 
<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'addOrgform', 'method' => 'post', 'enctype' => 'multipart/form-data');
echo form_open('', $attributes);
?>
<div class="row">
    <div class="col-sm-8">
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('org_code'); ?></label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter  Organization Code">
                <i class="fa fa-question-circle"></i>
            </a>
            <div class="col-sm-6">
                <?php
                echo form_input(array('type' => 'text', 'name' => 'org_code', 'id' => 'org_code', 'class' => 'form-control',
                    'placeholder' => $this->lang->line('plz_orgCode')));
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('organization_name'); ?></label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter  Organization Name">
                <i class="fa fa-question-circle"></i>
            </a>
            <div class="col-sm-6">
                <?php
                echo form_input(array('type' => 'text', 'name' => 'organization_name', 'id' => 'organization_name', 'class' => 'form-control',
                    'placeholder' => $this->lang->line('plz_organization_name')));
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('organization_bn_name'); ?></label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Bangla Name">
                <i class="fa fa-question-circle"></i>
            </a>
            <div class="col-sm-6">
                <?php
                echo form_input(array('type' => 'text', 'name' => 'organization_bn_name', 'id' => 'organization_bn_name', 'class' => 'form-control',
                    'placeholder' => $this->lang->line('plz_organization_bn_name')));
                ?>
                <input type="hidden" name="parent_organization" value="<?php echo $org_id; ?>">
            </div>
        </div>
        <!-- <div class="form-group">
                    <label class="col-sm-5 control-label">Organization Type</label>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="<?php echo $this->lang->line('dropdown_org_help'); ?> ">
                        <i class="fa fa-question-circle"></i>
                    </a>
                    <div class="col-sm-6">
                        <?php echo form_dropdown('organization_type', $organizations, '', 'class="form-control"'); ?>
                    </div>
                </div> -->        
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('is_active'); ?></label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="<?php echo $this->lang->line('status_help');?>">
                <i class="fa fa-question-circle"></i>
            </a>
            <div class="col-sm-6 checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_FLAG', '', true, 'id="is_active" class="styled"');?>
                <label for="is_active"></label>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
