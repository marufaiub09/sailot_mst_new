<style>
    .modal-dialog{
        width: 40%!important;
    }
</style>

<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'dgdpMainForm', 'method' => 'post', 'enctype' => 'multipart/form-data');
echo form_open('', $attributes);
?>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <div class="col-sm-offset-3">
                <input type="hidden" name="fld_confirm" value="1">
                <p>Are You Sure ?</p>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3">
                <button type="submit" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>