<style type="text/css">
    .modal-dialog{
        width: 55%!important;
    }
    .col-sm-6 .form-group{
        clear: both !important;
        margin-bottom: 10px !important;
    }

</style>
<?php echo validation_errors(); ?> 
<?php
$attributes = array('class' => 'form-horizontal', 'id' => 'dgdpMainForm', 'method' => 'post', 'enctype' => 'multipart/form-data');
echo form_open('', $attributes);
?>
<div class="row">
    <div class="col-sm-8">
        <input type="hidden" name="ORG_ID" id="ORG_ID" value="<?php echo $item->ORGANIZATION_ID; ?>" class="form-control input-sm">
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('org_code'); ?></label>
            <div class="col-sm-6">
                <?php
                echo form_input(array('name' => 'org_code', 'id' => 'LOOKUP_DATA_NAME', 'value' => $item->ORG_CODE,
                    'class' => 'form-control','placeholder'=>$this->lang->line('plz_orgCode'), 'required' => 'required'));
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('organization_name'); ?></label>
            <div class="col-sm-6">
                <?php
                echo form_input(array('name' => 'organization_name', 'id' => 'LOOKUP_DATA_NAME', 'value' => $item->ORG_NAME,
                    'class' => 'form-control','placeholder'=>$this->lang->line('plz_organization_name'), 'required' => 'required'));
                ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('organization_bn_name'); ?></label>
            <div class="col-sm-6">
                <?php
                echo form_input(array('name' => 'organization_bn_name', 'id' => 'LOOKUP_DATA_NAME', 'value' => $item->BANGLA_NAME,
                    'class' => 'form-control','placeholder'=>$this->lang->line('plz_organization_name'), 'required' => 'required'));
                ?>
            </div>
        </div>
        
        <div class="form-group">
            <label class="col-sm-5 control-label"><?php echo $this->lang->line('is_active'); ?></label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="<?php echo $this->lang->line('status_help');?>">
                <i class="fa fa-question-circle"></i>
            </a>
            <div class="col-sm-6 checkbox checkbox-inline checkbox-primary">
                <input id="is_active" type="checkbox" class="styled" name="ACTIVE_FLAG" class="checkBoxStatus" <?php echo $item->ACTIVE_STATUS == 1 ? 'checked' : '' ?> />
                <label for="is_active"></label>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
            </div>
        </div>
    </div>
</div>

<?php echo form_close(); ?>

<script>
    $(document).on('click', '.checkBoxStatus', function () {
        var active_flag = ($(this).is(':checked')) ? 1 : 0;
        $("#active_flag").val(active_flag);
    });
</script>