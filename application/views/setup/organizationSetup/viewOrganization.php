<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tbody>
        <tr>
            <th> <?php echo $this->lang->line('org_code') ?></th>
            <td><?php echo $item->ORG_CODE; ?></td>
        </tr>
        <tr>
            <th> <?php echo $this->lang->line('organization_name') ?></th>
            <td><?php echo $item->ORG_NAME; ?></td>
        </tr>
        <tr>
            <th><?php echo $this->lang->line('BANGLA_NAME') ?> </th>
            <td><?php echo $item->BANGLA_NAME; ?></td>
        </tr>               
    </tbody>
</table>


