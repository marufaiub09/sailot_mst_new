
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th>Charge Head ID</th>
        <td><?php echo $row->UD_CHARGE_ID ?></td>
    </tr>
    <tr>
        <th>Charge Head Name</th>
        <td><?php echo $row->CHARGE_NAME ?></td>
    </tr>
    <tr>
        <th>AMOUNT BDT</th>
        <td><?php echo $row->AMOUNT_BDT ?></td>
    </tr>
    <tr>
        <th>VALID DAY</th>
        <td><?php echo $row->VALID_DAY ?></td>
    </tr>
    
    <tr>
        <th>DESCRIPTION</th>
        <td><?php echo $row->DESCRIPTION ?></td>
    </tr>
    <tr>
        <th>REMARKS</th>
        <td><?php echo $row->REMARKS ?></td>
    </tr>
    <tr>
        <th>WITHDRAWAL</th>
        <td><?php echo ($row->WITHDRAWAL == 1) ? '<span class="btn btn-xs btn-success waves-effect waves-button">Active</span>' : '<span class="btn btn-xs btn-danger waves-effect waves-button waves-float">Inactive</span>'; ?></td>
    </tr>
    
    
</table>