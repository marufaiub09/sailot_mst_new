<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Charge Head List</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink" href="<?php echo site_url('setup/chargehead/create'); ?>" title="Create Charge Head">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Charge Head ID</th>
                            <th>Charge Name</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Charge Head ID</th>
                            <th>Charge Name</th>
                            <th>Amount</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach ($list as $key => $values) { ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo $values->UD_CHARGE_ID ?></td>
                                <td><?php echo $values->CHARGE_NAME ?></td>
                                <td><?php echo $values->AMOUNT_BDT ?></td>
                                <td>
                        <center><?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="btn btn-xs btn-success waves-effect waves-button">Active</span>' : '<span class="btn btn-xs btn-danger waves-effect waves-button waves-float">Inactive</span>'; ?></center>
                        </td>
                        <td>
                        <center>
                            <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/chargehead/view/' . $values->CHARGE_ID); ?>" title="View Charge Head" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/chargehead/edit/' . $values->CHARGE_ID); ?>"  title="Edit Charge Head" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <!--                    <a class="btn btn-danger btn-xs"  href="<?php echo site_url('setup/chargehead/delete/' . $values->CHARGE_ID); ?>" title="Delete" type="button"><span class="glyphicon glyphicon-trash"></span></i></a>-->
                        </center>
                        </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    
    tinymce.init({
        selector: 'textarea',
        height: 200,
        plugins: [
            'searchreplace visualblocks code fullscreen',
        ],
        toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
    
</script>