<style type="text/css">
    #exist{
        display:none;
        color:red;
    }
</style>

<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open('', "id='dgdpMainForm' class='form-horizontal'"); ?>


<div class="form-group">
    <label class="col-sm-3 control-label">Charge Head ID</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Charge Head ID">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-4">
        <?php echo form_input(array('name' => 'UD_CHARGE_ID', 'id' => 'UD_CHARGE_ID', "class" => "form-control caste_name", 'maxlength' => '20', 'required' => 'required', 'placeholder' => 'Chage Head ID')); ?>
        <span id="exist">* This ID already exist. Please try another one.</span>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label" for="NICENAME">Charge Head Name</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter a Charge Head Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-4">
        <?php echo form_input(array('name' => 'CHARGE_NAME', "class" => "form-control caste_name", 'required' => 'required', 'placeholder' => 'Charge Head Name')); ?>
    </div>
</div>

<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label">Amount BDT</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Amount (BDT)">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-2">
        <?php echo form_input(array('name' => 'AMOUNT_BDT', 'type' => 'number', 'required' => 'required', "class" => "form-control caste_name", 'placeholder' => 'Amount (BDT)')); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label">Amount USD</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Amount (USD)">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-2">
        <?php echo form_input(array('name' => 'AMOUNT_USD', 'type' => 'number', "class" => "form-control caste_name", 'placeholder' => 'Amount (USD)')); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label">Valid Day</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Valid Day">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-2">
        <?php echo form_input(array('name' => 'VALID_DAY', 'type' => 'number', "class" => "form-control caste_name", 'placeholder' => 'Valid Day')); ?>
    </div>
</div>

<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label">Description</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Description">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_textarea(array('name' => 'description', "class" => "form-control", 'rows' => '3', 'required' => 'required', 'placeholder' => 'Description')); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label">Remarks</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Remarks">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_textarea(array('name' => 'REMARKS', "class" => "form-control", 'rows' => '3', 'placeholder' => 'Remarks')); ?>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>

    <div class="col-sm-2">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('WITHDRAWAL', '1', TRUE, 'class="styled"'); ?>
            <label for="WITHDRAWAL">WITHDRAWAL</label>
        </div>
    </div>
    <div class="col-sm-2">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox(array('name' => 'ACTIVE_STATUS', 'value' => '1', 'checked' => "checked")); ?>
            <label for="ACTIVE_STATUS">Active</label>
        </div>
    </div>

</div>
<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
    </div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    $("#UD_CHARGE_ID").blur(function(){
        var UD_CHARGE_ID = $("#UD_CHARGE_ID").val();
       
        $.ajax({
            type: 'POST',
            url: '<?php echo site_url('setup/chargehead/checkUserChargeIdExist'); ?>',
            data: {UD_CHARGE_ID: UD_CHARGE_ID },
            success: function (data) {
                if(data>0){
                    $("#exist").show(); 
                    $("#UD_CHARGE_ID").focus();
                }
                else{
                    $("#exist").hide();
                }
               
            }
            //complete: function (data) {
            //$("#showDetaildModal").modal("hide");
            //}
        });
    });
</script>