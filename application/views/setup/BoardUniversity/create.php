<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('board_code'); ?><span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Board Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'boardCode', 'required' => 'required', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'placeholder' => $this->lang->line('board_code'))); ?>
        </div>
    </div>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('board_name'); ?><span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Board Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'boardName', 'required' => 'required', "class" => "form-control caste_name required", 'placeholder' => $this->lang->line('board_name'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/BoardUniversity/saveBoardUniversity" data-su-action="setup/BoardUniversity/boardUniversityList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>


