<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th><?php echo $this->lang->line('sl'); ?></th>
            <th><?php echo $this->lang->line('grade_code'); ?></th> 
            <th><?php echo $this->lang->line('grade_name'); ?></th>
            <th><?php echo $this->lang->line('status'); ?></th>
            <th><?php echo $this->lang->line('action'); ?></th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th><?php echo $this->lang->line('sl'); ?></th>
            <th><?php echo $this->lang->line('grade_code'); ?></th>
            <th><?php echo $this->lang->line('grade_name'); ?></th>
            <th><?php echo $this->lang->line('status'); ?></th>
            <th><?php echo $this->lang->line('action'); ?></th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->EXAM_GRADEID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->CODE ?></td>
                <td><?php echo $row->NAME ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->EXAM_GRADEID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="EXAM_GRADEID" data-field="ACTIVE_STATUS" data-tbl="bn_examgrade" data-su-url="setup/ExamGred/examGradeById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>

                <td>
        <center>
            <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/ExamGred/edit/' . $row->EXAM_GRADEID); ?>" title="Edit Exam Grade" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
        </center>
    </td>
    </tr>
<?php } ?>
</tbody>
</table>