<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->EXAM_GRADEID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="EXAM_GRADEID" data-field="ACTIVE_STATUS" data-tbl="bn_examgrade" data-su-url="setup/ExamGred/examGradeById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/ExamGred/edit/' . $row->EXAM_GRADEID); ?>" title="Edit Exam Grade" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
</center>
</td>