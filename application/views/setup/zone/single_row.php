<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->Org_name ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->ADMIN_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="ADMIN_ID" data-field="ACTIVE_STATUS" data-tbl="bn_navyadminhierarchy" data-su-url="setup/zone/zoneById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/zone/edit/' . $row->ADMIN_ID); ?>"  title="Edit Zone" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->ADMIN_ID; ?>" title="Click For Delete" data-type="delete" data-field="ADMIN_ID" data-tbl="bn_navyadminhierarchy"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>