<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->GOVT_EXAMID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="GOVT_EXAMID" data-field="ACTIVE_STATUS" data-tbl="bn_govtexam_hierarchy" data-su-url="setup/Exam_level/examLevelById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/Exam_level/edit/' . $row->GOVT_EXAMID); ?>" title="Edit Exam Level" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
</center>
</td>