<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Caste Setup</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="modalLink btn btn-primary btn-xs" data-tooltip="tooltip" title="Add New Caste" href="<?php echo site_url("setup/user/create"); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>
                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th><?php echo $this->lang->line('sl_num'); ?></th>
                        <th><?php echo $this->lang->line('name'); ?></th>
                        <th><?php echo $this->lang->line('group'); ?></th>
                        <th><?php echo $this->lang->line('label'); ?></th>
                        <th><?php echo $this->lang->line('department'); ?></th>
                        <th><?php echo $this->lang->line('action'); ?></th>

                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th><?php echo $this->lang->line('sl_num'); ?></th>
                        <th><?php echo $this->lang->line('name'); ?></th>
                        <th><?php echo $this->lang->line('group'); ?></th>
                        <th><?php echo $this->lang->line('label'); ?></th>
                        <th><?php echo $this->lang->line('department'); ?></th>
                        <th><?php echo $this->lang->line('action'); ?></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach ($users as $user) {
                    $userGroup = $this->utilities->findByAttribute("sa_user_group", array("USERGRP_ID" => $user->USERGRP_ID));
                    $userGroupLevel = $this->utilities->findByAttribute("sa_ug_level", array("UG_LEVEL_ID" => $user->USERLVL_ID));
                    //$userDept = $this->utilities->findByAttribute("hr_dept", array("DEPT_NO" => $user->DEPT_ID));
                        ?>
                        <tr>
                             <td><?php echo $i++; ?></td>
                             <td><?php echo $user->FIRST_NAME." ".$user->MIDDLE_NAME." ".$user->LAST_NAME." "; ?></td>
                             <td><?php echo $userGroup->USERGRP_NAME; ?></td>
                             <td><?php echo $userGroupLevel->UGLEVE_NAME; ?></td>
                             <td><?php //echo $userDept->DEPT_NAME;  ?></td>
                             <td><button type="button" href="<?php echo site_url("setup/user/edit/$user->USER_ID"); ?>" title="Edit User" class="btn btn-xs btn-primary btn-sm modalLink"><?php echo $this->lang->line('edit'); ?></button>
                                 <button type="button" href="<?php echo site_url("setup/user/edit/$user->USER_ID"); ?>" title="Edit User" class="btn btn-xs btn-danger btn-sm"><?php echo $this->lang->line('delete'); ?></button>
                             </td>

                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
