<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-3 control-label">Number</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Dao Number">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-5">
            <?php echo form_input(array('name' => 'DaoNumber', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Dao number')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Date</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Date">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'DaoDate', "class" => " datePicker form-control  required", 'required' => 'required', 'placeholder' => 'Dao Date', 'value' => date('d-m-Y'))); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label">Is Published</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('IS_PUBLISHED', '1', FALSE, 'class="styled"'); ?>
                <label for="IS_PUBLISHED"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/DaoNumber/saveDaoNumber" data-su-action="setup/DaoNumber/DaoNumberList" data-type="list" value="submit">
        </div>
    </div>
</form>
