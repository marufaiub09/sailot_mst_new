<td><?php echo $sn; ?></td>
<td><?php echo $row->DAO_NO ?></td> 
<td><?php echo date("d/m/Y", strtotime($row->Date)) ?></td>
<td>
    <a class="itemStatus" data-sn="<?php echo $sn ?>" id="<?php echo $row->DAO_ID; ?>" data-status="<?php echo $row->Is_Published ?>" data-fieldId="DAO_ID" data-field="Is_Published" data-tbl="bn_dao" data-su-url="setup/DaoNumber/DaoNumberById/<?php echo $sn; ?>">
        <?php echo ($row->Is_Published == 1) ? '<span class="label label-success" title="Click For Inactive">True</span>' : '<span class="label label-danger" title="Click For Active">False</span>' ?>
    </a>
</td>
<td>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/DaoNumber/editDaoNumber/' . $row->DAO_ID); ?>"  title="Edit Dao Number" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->DAO_ID; ?>" title="Click For Delete" data-type="delete" data-field="DAO_ID" data-tbl="bn_dao"><span class="glyphicon glyphicon-trash"></span></a>    
</td>


