<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->CLEARANCE_TYPEID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="CLEARANCE_TYPEID" data-field="ACTIVE_STATUS" data-tbl="bn_clearancetype" data-su-url="setup/clearanceType/ctById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/clearanceType/view/' . $row->CLEARANCE_TYPEID); ?>" title="View clearance Type" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/clearanceType/edit/' . $row->CLEARANCE_TYPEID); ?>"  title="Edit clearance Type" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->CLEARANCE_TYPEID; ?>" title="Click For Delete" data-type="delete" data-field="CLEARANCE_TYPEID" data-tbl="bn_clearancetype"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>