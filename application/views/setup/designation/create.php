<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open('', "id='dgdpMainForm' class='form-horizontal'"); ?>

<div class="form-group">
    <label class="col-sm-4 control-label">Organization</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Organization">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_dropdown('organization_id', $organization, '', 'id="organization" class="form-control select2" required'); ?> 
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">Parent Designation</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Parent Designation">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_dropdown('parent_id', '', 'Select Parent Designation', 'id="parent_id" class="form-control select2"'); ?> 
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label"><?php echo $this->lang->line('designation_name'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Designation Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_input(array('name' => 'designation', 'class' => 'form-control', 'required' => 'required', 'placeholder' => $this->lang->line('designation_name'))); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">User Define ID</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter User Define ID">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'udid', 'class' => 'form-control', 'placeholder' => 'User Define ID')); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-4 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <?php echo form_submit(array('class' => 'btn btn-primary', 'value' => $this->lang->line('submit'))); ?> 
    </div>
</div>
<?php echo form_close(); ?>