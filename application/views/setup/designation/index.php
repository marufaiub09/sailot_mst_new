<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><?php echo $this->lang->line('designation_list'); ?></h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink" href="<?php echo site_url('setup/designation/create'); ?>" title="<?php echo $this->lang->line('create_designation'); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('designation_name'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('designation_name'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach ($list as $key => $values) { ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo $values->DESIG_NAME ?></td>
                                <td>
                        <center><?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="btn btn-xs btn-success waves-effect waves-button">Active</span>' : '<span class="btn btn-xs btn-danger waves-effect waves-button waves-float">Inactive</span>'; ?></center>
                        </td>
                        <td>
                        <center>
                            <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/designation/view/' . $values->DESIG_ID); ?>" title="<?php echo $this->lang->line('view_designation') ?>" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/designation/edit/' . $values->DESIG_ID); ?>"  title="<?php echo $this->lang->line('edit_designation') ?>t" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <!--                            <a class="btn btn-danger btn-xs"  href="<?php echo site_url('setup/designation/delete/' . $values->DESIG_ID); ?>" title="Delete" type="button"><span class="glyphicon glyphicon-trash"></span></i></a>-->
                        </center>
                        </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('change','#organization', function(){
        $('#parent_id').html('').change();
        var val = $(this).val();
        if(val != ''){
            $.ajax({
                type: "POST",
                url: "<?php echo site_url("setup/designation/getParentByOrganizationId"); ?>",
                dataType: 'json',
                data: {fld_id: val},
                success: function (data) {    
                    var appenddata;
                    $.each(data, function (key, value) {
                        appenddata += "<option value = '" + value.DESIG_ID + " '>" + value.DESIG_NAME + " </option>";                        
                    });
                    $('#parent_id').html(appenddata);
                }
            });
        }
    })
</script> 