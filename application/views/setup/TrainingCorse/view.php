<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <tr>
        <th><?php echo $this->lang->line('trainig_code') ?> </th>
        <td><?php echo $result->CODE; ?></td>
    </tr>
    <tr>
        <th> <?php echo $this->lang->line('training_name') ?></th>
        <td><?php echo $result->NAME; ?></td>
    </tr>
<tr>
        <th><?php echo $this->lang->line('is_active'); ?></th>
        <td><?php echo $result->ACTIVE_STATUS == 1 ? 'YES' : 'NO'; ?></td>
    </tr>
</table>