<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Name(BN)</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Name(BN)</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($training as $key => $values) { ?>
            <tr id="row_<?php echo $values->TRAINING_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $values->CODE ?></td>
                <td><?php echo $values->NAME ?></td>
                <td><?php echo $values->BANGLA_NAME ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $values->TRAINING_ID; ?>" data-status="<?php echo $values->ACTIVE_STATUS ?>" data-fieldId="TRAINING_ID" data-field="ACTIVE_STATUS" data-tbl="bn_bdadminhierarchy" data-su-url="setup/thana/thanaById/<?php echo $key + 1 ?>">
                        <?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>

                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/TrainingCorse/viewTraining/' . $values->TRAINING_ID); ?>" title="View Training" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/TrainingCorse/editTraining/' . $values->TRAINING_ID); ?>"  title="Edit Training" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $values->TRAINING_ID; ?>" title="Click For Delete" data-type="delete" data-field="TRAINING_ID" data-tbl="bn_bdadminhierarchy"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>