<td><?php echo $sn ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->SHORT_NAME ?></td>
<td><?php echo $row->UNIT_TYPE ?></td>
<td><?php echo $row->SHIP_EST ?></td>
<td><?php echo $row->ORG_NAME ?></td>                           
<td>
    <a class="itemStatus" data-sn="<?php echo $sn ?>" id="<?php echo $row->POSTING_UNITID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="POSTING_UNITID" data-field="ACTIVE_STATUS" data-tbl="bn_posting_unit" data-su-url="setup/postingUnit/puById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/postingUnit/view/' . $row->POSTING_UNITID); ?>" title="View Posting Unit" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/postingUnit/edit/' . $row->POSTING_UNITID); ?>"  title="Edit Posting Unit" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->POSTING_UNITID; ?>" title="Click For Delete" data-type="delete" data-field="POSTING_UNITID" data-tbl="bn_posting_unit"><span class="glyphicon glyphicon-trash"></span></a>    
</td>