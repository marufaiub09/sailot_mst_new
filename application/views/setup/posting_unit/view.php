
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        
        <tr>
            <th width="20%">Code</th>
            <td><?php echo $viewdetails->CODE?></td>
        </tr>
        <tr>
            <th>Name</th>
            <td><?php echo $viewdetails->NAME?></td>
        </tr><tr>
            <th>Name (বাংলা)</th>
            <td><?php echo $viewdetails->BANGLA_NAME?></td>
        </tr>
        <tr>
            <th>Short Name</th>
            <td><?php echo $viewdetails->SHORT_NAME?></td>
        </tr>
        <tr>
            <th>Unit Type</th>
            <td><?php echo $viewdetails->UNIT_TYPE?></td>
        </tr>
        <tr>
            <th>Ship Establishment</th>
            <td><?php echo $viewdetails->SHIP_EST?></td>
        </tr>
        <tr>
            <th>Organization</th>
            <td><?php echo $viewdetails->ORG_NAME?></td>
        </tr>
        <tr>
            <th>SEA SERVICE</th>
            <td><?php echo ($viewdetails->IS_SEA_SERVICE == 1)? "TRUE": "FALSE";?></td>
        </tr>
        <tr>
            <th>ISSUING AUTHORITY</th>
            <td><?php echo ($viewdetails->IS_ISSUING_AUTHORITY == 1)? "TRUE": "FALSE";?></td>
        </tr>


        
</table>