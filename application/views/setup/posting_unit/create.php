<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-2 control-label">Code<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code', 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-4 control-label">Name<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Posting Unit Name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'name', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Posting unit name')); ?>
                </div>
            </div>            
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label class="col-sm-4 control-label">Short Name</label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Posting Unit Short Name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                   <?php echo form_input(array('name' => 'st_name', "class" => "form-control",'required'=>'required', 'placeholder' => 'Short name')); ?>
                </div>
            </div>            
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-4 control-label">Name (বাংলা)</label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter bengali Posting Unit Name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'bn_name', "class" => "form-control",'required'=>'required', 'placeholder' => 'bengali name')); ?>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label class="col-sm-4 control-label">Unit Type<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Unit  type">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <select class="select2 form-control required" name="UNITTYPE_ID" id="UNITTYPE_ID" data-tags="true" data-placeholder="Select Unit type" data-allow-clear="true">
                        <option value="">Select Unit Type</option>
                        <?php
                        foreach ($unitType as $row):
                            ?>
                            <option value="<?php echo $row->UNIT_TYPEID ?>"><?php echo $row->NAME ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-4 control-label" style="padding-left: 0px; padding-right: 0px;">Ship/Establishment<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Ship/Establishment">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                    <select class="select2 form-control required" name="SE_ID" id="SE_ID" data-tags="true" data-placeholder="Select Ship/Establishment" data-allow-clear="true">
                        <option value="">Select Ship/Establishment</option>
                        <?php
                        foreach ($shipEsta as $row):
                            ?>
                            <option value="<?php echo $row->SHIP_ESTABLISHMENTID ?>"><?php echo $row->NAME ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label class="col-sm-4 control-label" style="padding-left: 0;">Organization<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Organization">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <select class="select2 form-control required" name="ORG_ID" id="ORG_ID" data-tags="true" data-placeholder="Select Organization" data-allow-clear="true">
                        <option value="">Select Organization</option>
                        <?php
                        foreach ($org as $row):
                            ?>
                            <option value="<?php echo $row->ORG_ID ?>"><?php echo $row->ORG_NAME ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">        
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-4 control-label"></label>
                <div class="col-sm-6">
                    <div class="col-sm-1 checkbox checkbox-inline checkbox-primary">
                        <?php echo form_checkbox('SEA_SERVICE', '1', FALSE, 'class="styled"'); ?>
                        <label for="SEA_SERVICE"></label>
                    </div>
                    <label class="col-sm-8 control-label">Sea Service</label>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-sm-8">
                    <div class="col-sm-1 checkbox checkbox-inline checkbox-primary">
                        <?php echo form_checkbox('ISSUEING_AUTHORITY', '1', FALSE, 'class="styled"'); ?>
                        <label for="ISSUEING_AUTHORITY"></label>
                    </div>
                    <label class="col-sm-8 control-label">Issueing Authority</label>
                </div>
                <label class="col-sm-4 control-label"></label>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/postingUnit/savePU" data-su-action="setup/postingUnit/puList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>
