<td><?php echo $sn; ?></td>
<td><?php echo $row->GROUP_CODE ?></td>
<td><?php echo $row->GROUP_NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->GROUP_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="GROUP_ID" data-field="ACTIVE_STATUS" data-tbl="bn_daogroup" data-su-url="setup/DaoGroup/DaoGroupById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/DaoGroup/edit/' . $row->GROUP_ID); ?>"  title="Edit DAO Group" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
</center>
</td>