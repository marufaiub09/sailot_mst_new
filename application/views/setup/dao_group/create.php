<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open('', "id='createModuleForm' class='form-horizontal update_caste'"); ?>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('GROUP_CODE'); ?> <span class="text-danger">* </span></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter your Group Code.">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-2">
        <?php echo form_input(array('name' => 'GROUP_CODE', 'required' => 'required', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'placeholder' => $this->lang->line('GROUP_CODE'))); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('GROUP_NAME'); ?> <span class="text-danger"> * </span></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter your Group Name .">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-4">
        <?php echo form_input(array('name' => 'GROUP_NAME', "class" => "form-control group_name required", 'placeholder' => $this->lang->line('GROUP_NAME'))); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-3">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
<?php echo form_close(); ?>
<?php $this->load->view("common/sailors_info"); ?>
