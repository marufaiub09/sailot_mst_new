<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">DAO Group Setup</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="modalLink btn btn-primary btn-xs" title="Add New DAO GROUP" href="<?php echo site_url("setup/DaoGroup/create"); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>
                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">
                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('sl_num'); ?></th>
                            <th><?php echo $this->lang->line('GROUP_CODE'); ?></th>
                            <th><?php echo $this->lang->line('GROUP_NAME'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>

                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('sl_num'); ?></th>
                            <th><?php echo $this->lang->line('GROUP_CODE'); ?></th>
                            <th><?php echo $this->lang->line('GROUP_NAME'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach ($daoGroup_list as $key => $row) { ?>
                            <tr id="row_<?php echo $row->GROUP_ID ?>">
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo $row->GROUP_CODE; ?></td>
                                <td><?php echo $row->GROUP_NAME; ?></td>
                                <td>
                                    <a class="itemStatus" id="<?php echo $row->GROUP_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="GROUP_ID" data-field="ACTIVE_STATUS" data-tbl="bn_daogroup" data-su-url="setup/DaoGroup/DaoGroupById/<?php echo $key + 1 ?>">
                                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                                    </a>
                                </td>
                                <td>
                        <center>
                            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/DaoGroup/edit/' . $row->GROUP_ID); ?>"  title="Edit DAO Group" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                        </center>
                        </td>
                        </tr>

                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
