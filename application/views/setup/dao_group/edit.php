<?php echo validation_errors(); ?>
    <!-- form start -->
<?php echo form_open('',"id='createModuleForm' class='form-horizontal'"); ?>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('GROUP_CODE'); ?> <span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Write Group Code.">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-2">
            <?php echo form_input(array('name' => 'GROUP_CODE','type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required",'value' => $row->GROUP_CODE, 'required placeholder' => $this->lang->line('GROUP_CODE'))); ?>
        </div>
    </div>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('GROUP_NAME'); ?><span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Write Group Name .">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <?php echo form_input(array('name' => 'GROUP_NAME','value' => $row->GROUP_NAME, "class" => "form-control group_name", 'required placeholder' => $this->lang->line('GROUP_NAME'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', ($row->ACTIVE_STATUS == 1) ? TRUE : FALSE, 'class="styled"'); ?>
                <label for="ACTIVE_FLAG"></label>
            </div>
        </div>
    </div>
    <input type="hidden" name="CASTE_ID" value="<?php $row->GROUP_ID; ?>">
    <div class="form-group">
        <div class="col-sm-offset-3">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
    <?php $this->load->view("common/sailors_info"); ?>
<?php echo form_close(); ?>
