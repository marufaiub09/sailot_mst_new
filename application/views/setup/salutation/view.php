<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width ="30%"> <?php echo $this->lang->line('salutation_name') ?></th>
        <td><?php echo $salutation->SALUTATION_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('salutation_gender') ?> </th>
        <td><?php
                        if ($salutation->GENDER == 'M') {
                            echo "Male";
                        } else if ($salutation->GENDER == 'F') {
                            echo "Female";
                        } else {
                            echo "Others";
                        }
                            ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('salutation_religion') ?> </th>
        <td><?php echo $religion_name->RELIGION_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('is_active'); ?></th>
        <td><?php echo $salutation->ACTIVE_STATUS == 1 ? 'YES' : 'NO'; ?></td>
    </tr>


</table>


