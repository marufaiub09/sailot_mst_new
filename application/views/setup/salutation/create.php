
<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open_multipart('', "id='dgdpMainForm' class='form-horizontal'"); ?>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('salutation_name'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter salutation Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
       <?php echo form_input(array('name' => 'salutation_name', 'required' => 'required', "class" => "form-control", 'placeholder' => $this->lang->line('salutation_name'))); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('salutation_gender'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select gender type">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
         <?php 
         $genders = array('' => '-- Select -- ', 'M' => 'Male', 'F' => 'Female', 'O' => 'Others');
         echo form_dropdown('salutation_gender', $genders, '', 'id="salutation_gender" required="required" class="form-control select2"'); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('religion'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select religion Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
         <?php echo form_dropdown('religion', $religion_list, '', 'id="religion" required="required" class="form-control select2"'); ?>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
    </div>
</div>
<?php echo form_close(); ?>






