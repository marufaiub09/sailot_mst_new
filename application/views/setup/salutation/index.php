<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><?php echo $this->lang->line('salutations'); ?></h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink" href="<?php echo site_url('setup/salutation/create'); ?>" title="<?php echo $this->lang->line('add_salutation'); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">
                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('salutation_name'); ?></th>
                            <th><?php echo $this->lang->line('gender'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('salutation_name'); ?></th>
                            <th><?php echo $this->lang->line('gender'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $sl = 1;
                        foreach ($salutation as $values) {
                            ?>
                            <tr>
                                <td><?php echo $sl++; ?></td>
                                <td><?php echo $values->SALUTATION_NAME ?></td>
                                <td><?php
                        if ($values->GENDER == 'M') {
                            echo "Male";
                        } else if ($values->GENDER == 'F') {
                            echo "Female";
                        } else {
                            echo "Others";
                        }
                            ?></td>

                                <td><?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="btn btn-xs btn-success waves-effect waves-button">Active</span>' : '<span class="btn btn-xs btn-danger waves-effect waves-button waves-float">Inactive</span>'; ?>
                                </td>

                                <td>

                                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/salutation/view/' . $values->SALUTATION_ID); ?>"  title="<?php echo $this->lang->line('view_salutation'); ?>" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                                    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/salutation/edit/' . $values->SALUTATION_ID); ?>" title="<?php echo $this->lang->line('edit_salutation'); ?>" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <!--                            <a class="btn btn-danger btn-xs" data-tooltip="tooltip" data-placement="top" href="<?php echo site_url('setup/district/delete/' . $values->DISTRICT_ID); ?>" title="Delete" type="button" data-user-id="<?php echo $values->DISTRICT_ID ?>"><span class="glyphicon glyphicon-trash"></span></i></a>-->

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

