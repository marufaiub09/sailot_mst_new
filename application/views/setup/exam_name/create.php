<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('code'); ?><span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Exam Code Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'ExamCode', 'required' => 'required', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'placeholder' => $this->lang->line('code'))); ?>
        </div>
    </div>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('exam_name'); ?><span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Exam Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'ExamName', 'required' => 'required', "class" => "form-control caste_name required", 'placeholder' => $this->lang->line('exam_name'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Parent ID<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Parent ID ">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="exam_type" id="exam_type" data-tags="true" data-placeholder="Select Parent ID" data-allow-clear="true">
                <option value="">Select Parent ID</option>
                <?php
                foreach ($examType as $row):
                    ?>
                    <option value="<?php echo $row->GOVT_EXAMID ?>"><?php echo $row->NAME ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/ExamName/savePart" data-su-action="setup/ExamName/partList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>



