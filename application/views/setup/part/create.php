<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Code<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code', 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Part Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'name', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Part name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name (বাংলা)</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Part Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'bn_name', "class" => "form-control ", 'placeholder' => ' বাংলা নাম লিখুন')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Trade<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Trade Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="TRADE_ID" id="TRADE_ID" data-tags="true" data-placeholder="Select Trade" data-allow-clear="true">
                <option value="">Select Trade</option>
                <?php
                foreach ($trade as $row):
                    ?>
                    <option value="<?php echo $row->TRADE_ID ?>"><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/part/savePart" data-su-action="setup/part/partList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>

