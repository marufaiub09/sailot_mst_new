<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->SUBJECT_GROUPID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="SUBJECT_GROUPID" data-field="ACTIVE_STATUS" data-tbl="bn_subject_group" data-su-url="setup/SubjectGroup/subjectGroupById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/SubjectGroup/edit/' . $row->SUBJECT_GROUPID); ?>" title="<?php echo $this->lang->line('edit_Subject'); ?>" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
</center>
</td>