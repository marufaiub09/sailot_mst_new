
<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open_multipart('', "id='dgdpMainForm' class='form-horizontal'"); ?>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('DEPT_NAME'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter department Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
       <?php echo form_input(array('name' => 'DEPT_NAME', 'required' => 'required', "class" => "form-control", 'placeholder' => $this->lang->line('DEPT_NAME'))); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('dept_desc'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter department description ">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-5">
        <?php echo form_textarea(array('name' => 'dept_desc', 'rows' => '3', 'required' => 'required', "class" => "form-control ", 'placeholder' => $this->lang->line('dept_desc'))); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('PARENT_DEPT_ID'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter parent Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
         <?php echo form_dropdown('PARENT_DEPT_ID', $departments, '', 'id="PARENT_DEPT_ID" required="required" class="form-control select2"'); ?>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
    </div>
</div>
<?php echo form_close(); ?>






