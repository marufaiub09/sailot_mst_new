<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th> <?php echo $this->lang->line('DEPT_NAME') ?></th>
        <td><?php echo $departments->DEPT_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('dept_desc') ?> </th>
        <td><?php echo $departments->DEPT_DESC; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('PARENT_DEPT_ID') ?> </th>
        <td><?php echo $parent->DEPT_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('is_active'); ?></th>
        <td><?php echo $departments->ACTIVE_STATUS == 1 ? 'YES' : 'NO'; ?></td>
    </tr>


</table>


