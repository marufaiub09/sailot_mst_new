<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Code</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code','value'=>$result->CODE,'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Visit Class</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Visit Class Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="VISIT_CI_ID" id="VISIT_CI_ID" data-tags="true" data-placeholder="Select Visit Class" data-allow-clear="true">
                <option value="">Select Classification</option>
                <?php
                foreach ($visitClass as $row):
                    ?>
                    <option value="<?php echo $row->VISIT_CLASSIFICATIONID ?>" <?php echo ($result->VISIT_CLASSIFICATIONID == $row->VISIT_CLASSIFICATIONID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Visit Information Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'name','value'=>$result->NAME, "class" => "form-control required",'required'=>'required', 'placeholder' => 'Visit Information name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Description</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Description">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_textarea(array('name' => 'desc','value'=>$result->DESCR, "cols" => "3", "rows" => "4", "class" => "form-control ", 'placeholder' => '')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->VISIT_INFO_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/visitinformation/updateVI" data-su-action="setup/visitinformation/viList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>

