
<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open('', "id='dgdpMainForm' class='form-horizontal'"); ?>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('app_group_name'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select organization Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_input(array('name' => 'app_group_name', 'required' => 'required', "class" => "form-control ", 'value' => $result->GROUP_NAME)); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('app_description'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_textarea(array('name' => 'description', "class" => "form-control", 'rows' => '3', 'required' => 'required', 'value' => $result->DESCRIPTION)); ?>
    </div>
</div>

<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('remarks'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_textarea(array('name' => 'remarks', "class" => "form-control", 'rows' => '3', 'required' => 'required', 'value' => $result->REMARKS)); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('restricted'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <input type="checkbox" name ="restricted" id="restricted" class="styled restrictedStatus" <?php echo ($result->RESTRICT_FG == 'Y') ? 'checked' : '' ?>>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <input type="checkbox" name ="ACTIVE_STATUS" id="ACTIVE_STATUS" class="styled checkBoxStatus" <?php echo ($result->ACTIVE_STATUS == 1) ? 'checked' : '' ?>>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
    </div>
</div>
<?php echo form_close(); ?>



<script type="text/javascript">
    $(document).on('click', '.checkBoxStatus', function () {
        var active_flag = ($(this).is(':checked')) ? 1 : 0;
        $("#ACTIVE_STATUS").val(active_flag);
    });
    
    $(document).on('click', '.restrictedStatus', function () {
        var restrictedStatus = ($(this).is(':checked')) ? 1 : 0;
        $("#restricted").val(restrictedStatus);
    });

</script>

