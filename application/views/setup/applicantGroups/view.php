<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th> <?php echo $this->lang->line('app_group_name') ?></th>
        <td><?php echo $result->GROUP_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('app_description') ?> </th>
        <td><?php echo $result->DESCRIPTION; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('remarks') ?> </th>
        <td><?php echo $result->REMARKS; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('restricted'); ?></th>
        <td><?php echo $result->RESTRICT_FG == 'Y' ? 'Restricted' : 'No Restriction'; ?></td> 
    </tr>
	<tr>
        <th><?php echo $this->lang->line('is_active'); ?></th>
        <td><?php echo $result->ACTIVE_STATUS == 1 ? 'YES' : 'NO'; ?></td>
    </tr>


</table>


