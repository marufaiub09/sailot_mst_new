<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><?php echo $this->lang->line('group_list'); ?></h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink"  href="<?php echo site_url('setup/applicantGroups/create'); ?>" title="<?php echo $this->lang->line('add_applicants_groups'); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('app_group_name'); ?></th>
                            <th><?php echo $this->lang->line('app_description'); ?></th> 
                            <th><?php echo $this->lang->line('restricted'); ?></th> 
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('app_group_name'); ?></th>
                            <th><?php echo $this->lang->line('app_description'); ?></th>
                            <th><?php echo $this->lang->line('restricted'); ?></th> 
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $sl = 1;
                        foreach ($result as $values) {
                            ?>
                            <tr>
                                <td><?php echo $sl++; ?></td>
                                <td><?php echo $values->GROUP_NAME ?></td>
                                <td><?php echo $values->DESCRIPTION ?></td>
                                <td><?php echo ($values->RESTRICT_FG == 'Y') ? 'Secured' : '' ?></td>
                                <td><?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="btn btn-xs btn-success waves-effect waves-button">Active</span>' : '<span class="btn btn-xs btn-danger waves-effect waves-button waves-float">Inactive</span>'; ?>
                                </td>
                                <td>
                                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/applicantGroups/view/' . $values->GROUP_ID); ?>"  title="<?php echo $this->lang->line('view_applicantGroups'); ?>" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                                    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/applicantGroups/edit/' . $values->GROUP_ID); ?>" title="<?php echo $this->lang->line('edit_applicantGroups'); ?>" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <!--                            <a class="btn btn-danger btn-xs" data-tooltip="tooltip" data-placement="top" href="<?php echo site_url('setup/district/delete/' . $values->DISTRICT_ID); ?>" title="Delete" type="button" data-user-id="<?php echo $values->DISTRICT_ID ?>"><span class="glyphicon glyphicon-trash"></span></i></a>-->

                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

