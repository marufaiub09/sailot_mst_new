<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->BANGLA_NAME ?></td>
<td><?php echo $row->SHORT_NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->BD_ADMINID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="BD_ADMINID" data-field="ACTIVE_STATUS" data-tbl="bn_bdadminhierarchy" data-su-url="setup/Division/DivisionById/<?php echo $sn ?>">
    <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/division/view/' . $row->BD_ADMINID); ?>" title="view Division" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/division/edit/' . $row->BD_ADMINID); ?>"  title="<?php echo $this->lang->line('edit_division');?>" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->BD_ADMINID; ?>" title="Click For Delete" data-type="delete" data-field="BD_ADMINID" data-tbl="bn_bdadminhierarchy"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>