<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Name (BN)</th>
            <th>Short Name</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Name (BN)</th>
            <th>Short Name</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($divisions as $key => $values) { ?>
            <tr id="row_<?php echo $values->BD_ADMINID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $values->CODE ?></td>
                <td><?php echo $values->NAME ?></td>
                <td><?php echo $values->BANGLA_NAME ?></td>
                <td><?php echo $values->SHORT_NAME ?></td>
                <td>
                <a class="itemStatus" id="<?php echo $values->BD_ADMINID; ?>" data-status="<?php echo $values->ACTIVE_STATUS ?>" data-fieldId="BD_ADMINID" data-field="ACTIVE_STATUS" data-tbl="bn_bdadminhierarchy" data-su-url="setup/Division/DivisionById/<?php echo $key + 1 ?>">
            <?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
        </a>
                </td>

                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/division/view/' . $values->BD_ADMINID); ?>" title="view Division" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/division/edit/' . $values->BD_ADMINID); ?>"  title="<?php echo $this->lang->line('edit_division');?>" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $values->BD_ADMINID; ?>" title="Click For Delete" data-type="delete" data-field="BD_ADMINID" data-tbl="bn_bdadminhierarchy"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>