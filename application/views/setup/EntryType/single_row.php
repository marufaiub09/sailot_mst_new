<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->CRE_BY ?></td>
<td><?php echo date('d-m-Y', strtotime($row->CRE_DT)) ?></td>
<td><?php echo $row->UPD_BY ?></td>                
<td><?php echo date('d-m-Y', strtotime($row->UPD_DT)) ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->ENTRY_TYPEID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="ENTRY_TYPEID" data-field="ACTIVE_STATUS" data-tbl="bn_entrytype" data-su-url="setup/EntryType/atById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/EntryType/view/' . $row->ENTRY_TYPEID); ?>" title="View Entry Type" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/EntryType/edit/' . $row->ENTRY_TYPEID); ?>"  title="Edit Entry Type" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->ENTRY_TYPEID; ?>" title="Click For Delete" data-type="delete" data-field="ENTRY_TYPEID" data-tbl="bn_entrytype"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>