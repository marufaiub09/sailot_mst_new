<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->MISSION_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="MISSION_ID" data-field="ACTIVE_STATUS" data-tbl="bn_mission" data-su-url="setup/mission/missionById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/mission/view/' . $row->MISSION_ID); ?>" title="View Mission" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/mission/edit/' . $row->MISSION_ID); ?>"  title="Edit Mission" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->MISSION_ID; ?>" title="Click For Delete" data-type="delete" data-field="MISSION_ID" data-tbl="bn_mission"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>