<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group">
        <label class="col-sm-3 control-label">Code</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'code', 'value' => $result->Code, 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required' => 'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Appoint Type Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'name', 'value' => $result->Name, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Appoint Type name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Basic Entry Type</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Appoint Type Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <select name="basicTypeEntry" id="basicType" class="form-control required" required="required" data-placeholder="Select Entry Type" aria-hidden="true" data-allow-clear="true">
                <option value="<?php echo $result->Cause; ?>"><?php echo $result->CauseName ?></option>
                <option value="1">Release</option>
                <option value="2">Nominee</option>
                <option value="3">ReIssue Pension</option>
                <option value="4">Suspension Pension</option>
                <option value="5">Benefits</option>
                <option value="6">Pension Book</option>
            </select>
        </div>
    </div>
    <div class="form-group" id="releaseData">
        <label class="col-sm-3 control-label">Release Type</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Appoint Type Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <select name="releaseEntry" class="form-control" data-placeholder="Select Entry Type" aria-hidden="true" data-allow-clear="true">
                <option value="<?php echo $result->SubType; ?>"><?php echo $result->SubTypeName ?></option>
                <option value=""><----></option>
                <option value="1">Release</option>
                <option value="2">Discharge</option>
                <option value="3">Dismissed</option>                            
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', ($result->ACTIVE_STATUS == 1) ? TRUE : FALSE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->CauseTypeID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/RetirementCauses/update" data-su-action="setup/RetirementCauses/atList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $("div#releaseData").hide();
        var basicId = "<?php echo $result->Cause; ?>";
        if(basicId == 1){ 
            $("div#releaseData").show();
        }else {
            $("div#releaseData").hide();
        }
    });
    
        $(document).ready(function () {
        $("#basicType").change(function () {
            $(this).find("option:selected").each(function () {
                if ($(this).attr("value") == "1") {
                    $("div#releaseData").show();
                }
                else {
                    $("div#releaseData").hide();
                }
            });
        }).change();
    });
    
</script>
