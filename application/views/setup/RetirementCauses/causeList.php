 <table id="sailorTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th> Code </th> 
            <th> Name </th>
            <th> Entry User </th>
            <th> Entry Date </th>
            <th> Update User </th>                            
            <th> Update Date </th>
            <th> Action </th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($searchCauseType as $row) { ?>
            <tr id="row_<?php echo $row->CauseTypeID ?>">
                <td><?php echo $row->Code ?></td>
                <td><?php echo $row->Name ?></td>
                <td><?php echo $row->CRE_BY ?></td>
                <td><?php echo date("d/m/Y", strtotime($row->CRE_DT)); ?></td>
                <td><?php echo $row->UPD_BY ?></td>
                <td><?php echo date("d/m/Y", strtotime($row->UPD_DT)); ?></td>
                <td>
        <center>
            <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/RetirementCauses/view/' . $row->CauseTypeID); ?>" title="View Detail Retirement Cause" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/RetirementCauses/edit/' . $row->CauseTypeID); ?>"  title="Edit Detail Retirement Cause" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->CauseTypeID; ?>" title="Click For Delete" data-type="delete" data-field="CauseTypeID" data-tbl="causetype"><span class="glyphicon glyphicon-trash"></span></a>
        </center>
    </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script type="text/javascript" language="javascript">
$('#sailorTable').removeAttr('width').DataTable({
            "scrollX": true,
            "scrollX": true,
            "bPaginate": false,
            "bFilter": false,
            "bInfo": false,
            "columnDefs": [
                {width: '20px', targets: 0},
                {width: '200px', targets: 1},
                {width: '60px', targets: 2},
                {width: '80px', targets: 3},
                {width: '80px', targets: 4},
                {width: '80px', targets: 5},
                {width: '90px', targets: 6},
            ],
            "fixedColumns": true

        });
</script>