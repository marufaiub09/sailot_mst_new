<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th> SN </th>
            <th> Code </th> 
            <th> Name </th>
            <th> Entry User </th>
            <th> Entry Date </th>
            <th> Update User </th>                            
            <th> Update Date </th>
            <th> Action </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>
<?php $this->load->view("common/sailors_info"); ?>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        var dataTable = $('#dataTable').DataTable();
    });
</script>