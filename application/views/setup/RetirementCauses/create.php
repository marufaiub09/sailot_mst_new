<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group">
        <label class="col-sm-3 control-label">Code</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'code', 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required' => 'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Retirement Causes">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'name', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Retirement Causes name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Basic Entry Type</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Basic Entry Type">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <select name="basicEntryType" id="basicEntryCreateId" data-width="170px"  class="form-control required" required="required" data-placeholder="Select Basic Entry Type" aria-hidden="true" data-allow-clear="true">
                <option value="">Select</option>
                <option value="1">Release</option>
                <option value="2">Nominee</option>
                <option value="3">ReIssue Pension</option>
                <option value="4">Suspension Pension</option>
                <option value="5">Benefits</option>
                <option value="6">Pension Book</option>
            </select>
        </div>
    </div> 
    <div class="form-group" id="releaseCreateData">
        <label class="col-sm-3 control-label">Release Type</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Release Tyepe">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <select name="releaseEntry"  class="form-control" data-placeholder="Select Release Type" aria-hidden="true" data-allow-clear="true">
                <option value="">Select</option>
                <option value="1">Release</option>
                <option value="2">Discharge</option>
                <option value="3">Dismissed</option>                            
            </select>
        </div>
    </div> 
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/RetirementCauses/save" data-su-action="setup/RetirementCauses/index" data-type="list" value="submit">
        </div>
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $("#basicEntryCreateId").change(function () {
            $(this).find("option:selected").each(function () {
                if ($(this).attr("value") == "1") {
                    $("#releaseCreateData").show();
                }
                else {
                    $("#releaseCreateData").hide();
                }
            });
        }).change();
    });
</script>