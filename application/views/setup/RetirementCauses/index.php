<div class="row">
    <div class="panel panel-base">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-11 col-sm-10 col-xs-8">
                    <h3 class="panel-title">Punishment Causes Type</h3>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-4">
                    <a class="btn btn-primary btn-xs modalLink" href="<?php echo site_url('setup/RetirementCauses/create'); ?>" title="Add Causes">
                        <i class="glyphicon glyphicon-plus"></i>
                    </a>
                </div>
            </div>
            <span class="pull-right clickable">
                <i class="glyphicon glyphicon-chevron-up"></i>
            </span>
        </div>
        <div>
            <div class="col-md-3" style="background:#e5e7e9;'"> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="cmbBrand">Basic Entry Type</label>
                            <select name="basicEntryType" id="basicEntryId" data-width="170px"  class="form-control basicEntType" data-placeholder="Select Entry Type" aria-hidden="true" data-allow-clear="true">
                                <option value="">Select</option>
                                <option value="1">Release</option>
                                <option value="2">Nominee</option>
                                <option value="3">ReIssue Pension</option>
                                <option value="4">Suspension Pension</option>
                                <option value="5">Benefits</option>
                                <option value="6">Pension Book</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12" id="ifRelease">
                        <label for="cmbModel">Release Type</label >
                        <div class="form-group">
                            <select name="releaseEntry" id="releaseData" class="form-control basicEntType" data-placeholder="Select Entry Type" aria-hidden="true" data-allow-clear="true">
                                <option value="">Select</option>
                                <option value="1">Release</option>
                                <option value="2">Discharge</option>
                                <option value="3">Dismissed</option>                            
                            </select>
                        </div>
                    </div>
                </div>
                <div class="input-group col-md-12" style="padding-top: 7px;">
                    <button type="submit" class="btn btn-primary btn-xs">
                        <img src="<?php echo base_url() . 'dist/img/print-pdf.png'; ?>" width="20px" height="20px" alt="pdf">
                        Print
                    </button>
                    <a class="btn btn-primary btn-xs" download="somedata.xls" href="#" onclick="return ExcellentExport.excel(this, 'example1', 'Sheet Name Here');">
                        <img src="<?php echo base_url() . 'dist/img/excel.png'; ?>" > Excel
                    </a>

                    <a class="btn btn-primary btn-xs" download="somedata.xls" href="#" onclick="return ExcellentExport.excel(this, 'example1', 'Sheet Name Here');">
                        <img src="<?php echo base_url() . 'dist/img/word.png'; ?>" > word
                    </a>
                    <span class="loader"></span>
                </div>
            </div>
            <div class="contentArea causeType col-md-9">
                <table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th> SN </th>
                            <th> Code </th> 
                            <th> Name </th>
                            <th> Entry User </th>
                            <th> Entry Date </th>
                            <th> Update User </th>                            
                            <th> Update Date </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#basicEntryId").change(function () {
            $(this).find("option:selected").each(function () {
                if ($(this).attr("value") == "1") {
                    $("#ifRelease").show();
                }
                else {
                    $("#ifRelease").hide();
                }
            });
        }).change();
    });

    $(document).ready(function () {
        $(".basicEntType").on('change', function () {
            var entryTypeVal = $("#basicEntryId").val();
            var selectReleaseVal = $("#releaseData").val();
            if (entryTypeVal != '') {
                $.ajax({
                    type: "post",
                    url: "<?php echo site_url(); ?>setup/RetirementCauses/searchingData",
                    data: {entryTypeVal: entryTypeVal, selectReleaseVal: selectReleaseVal},
                    beforeSend: function () {
                        $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                    },
                    success: function (data) {
                        $(".causeType").html(data);
                    }
                });

            } else {
                $(".causeType").html('');
            }
        });
    });
</script>