<td <?php echo ($group_data->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><span><?php echo $sr; ?></span><span
        class="hidden" id="loader_<?php echo $group_data->id; ?>"></span></td>
<td <?php echo ($group_data->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $group_data->enum_name ?></td>
<td <?php echo ($group_data->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>>
    <span style="cursor:pointer" id="status<?php echo $group_data->id ?>" class="status"
          look_up_id="<?php echo $group_data->id ?>" data-status="<?php echo $group_data->ACTIVE_STATUS ?>"
          data-su-url="setup/BaseSetup/enumDataById"> <?php echo ($group_data->ACTIVE_STATUS == 1) ? '<span id="toggol_' . $group_data->id . '" class="label label-success" title="Click For Inactive" >Inactive</span>' : '<span id="toggol_' . $group_data->id . '" class="label label-danger" title="Click For Active" >Active</span>'; ?> </span>
    <a class="label label-default openLookUpModal" id="<?php echo $group_data->id; ?>" title="Edit Group Data"
       data-action="setup/BaseSetup/enumDataFormUpdate/<?php echo $group_data->grp_id; ?>/<?php echo $group_data->id; ?>"
       data-type="edit"><i class="fa fa-pencil"></i></a>
    <a class="label label-danger deletelookup" item_id="<?php echo $group_data->id; ?>" title="Click For Delete"
       data-type="delete" data-field="id" data-tbl="sa_enum_data"><i class="fa fa-times"></i></a>
</td>