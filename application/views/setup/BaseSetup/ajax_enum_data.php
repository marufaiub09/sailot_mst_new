<table class="table table-striped table-bordered table-hover gridTable dataTable">
    <thead>
    <tr>
        <th style="width: 5%;color: green">SN</th>
        <th style="color: green">Name</th>
        <th style="color: green" class="text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 0;
    foreach ($group_data as $rr) {
        $i = $i + 1;
        ?>
        <tr>
            <td <?php echo ($rr->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><span><?php echo $i; ?></span><span
                    class="hidden" id="loader_<?php echo $rr->id; ?>"></span></td>
            <td <?php echo ($rr->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>><?php echo $rr->enum_name ?></td>
            <td <?php echo ($rr->ACTIVE_STATUS == 1) ? "" : "class='inactive'"; ?>>
                <span style="cursor:pointer" id="status<?php echo $rr->id ?>" class="status"
                      look_up_id="<?php echo $rr->id ?>" data-status="<?php echo $rr->ACTIVE_STATUS ?>"
                      data-su-url="setup/BaseSetup/enumDataById"> <?php echo ($rr->ACTIVE_STATUS == 1) ? '<span id="toggol_' . $rr->id . '" class="label label-success" title="Click For Inactive" >Inactive</span>' : '<span id="toggol_' . $rr->id . '" class="label label-danger" title="Click For Active" >Active</span>'; ?> </span>
                <a class="label label-default openLookUpModal" id="<?php echo $rr->id; ?>" title="Edit Group Data"
                   data-action="setup/BaseSetup/enumDataFormUpdate/<?php echo $rr->grp_id; ?>/<?php echo $rr->id; ?>"
                   data-type="edit"><i class="fa fa-pencil"></i></a>
                <a class="label label-danger deletelookup" item_id="<?php echo $rr->id; ?>" title="Click For Delete"
                   data-type="delete" data-field="id" data-tbl="sa_enum_data"><i class="fa fa-times"></i></a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>