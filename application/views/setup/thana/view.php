
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <tr>
            <th width="20%">District Name</th>
            <td><?php echo $viewdetails->District?></td>
        </tr>
        <tr>
            <th width="20%">Thana Code</th>
            <td><?php echo $viewdetails->CODE?></td>
        </tr>
        <tr>
            <th>Thana Name</th>
            <td><?php echo $viewdetails->NAME?></td>
        </tr>
        <tr>
            <th>Thana Name (বাংলা)</th>
            <td><?php echo $viewdetails->BANGLA_NAME?></td>
        </tr>
        <tr>
            <th>Short Name</th>
            <td><?php echo $viewdetails->SHORT_NAME?></td>
        </tr>
        
</table>