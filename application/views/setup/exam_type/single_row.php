<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->BN_NAME ?></td>

<td>
    <a class="itemStatus" id="<?php echo $row->EXAM_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="EXAM_ID" data-field="ACTIVE_STATUS" data-tbl="bn_navyexam_hierarchy" data-su-url="setup/ExamTypes/examNameById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/ExamTypes/edit/' . $row->EXAM_ID); ?>"  title="Edit Exam Name" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->EXAM_ID; ?>" title="Click For Delete" data-type="delete" data-field="EXAM_ID" data-tbl="bn_navyexam_hierarchy"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>