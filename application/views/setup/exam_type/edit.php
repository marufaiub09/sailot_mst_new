<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group">
        <label class="col-sm-3 control-label">Code<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'code', 'value' => $result->CODE, 'type' => 'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required' => 'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Exam Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'name', 'value' => $result->NAME, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Exam name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Exam Type<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Exam Type">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="exam_type" id="exam_type" data-tags="true" data-placeholder="Select Exam Type" data-allow-clear="true">
                <option value="">Select Exam Type</option>
                <?php
                foreach ($examType as $row):
                    ?>
                    <option value="<?php echo $row->EXAM_ID; ?>" <?php echo ($result->PARENT_ID == $row->EXAM_ID) ? 'selected' : '' ?>><?php echo $row->NAME; ?></option>
                    <?php
                endforeach;
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', ($result->ACTIVE_STATUS == 1) ? TRUE : FALSE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->EXAM_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/ExamTypes/updatePart" data-su-action="setup/ExamTypes/partList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>

