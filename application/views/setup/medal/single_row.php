<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->SHORT_NAME ?></td>
<td><?php echo $row->DESCR ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->MEDAL_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="MEDAL_ID" data-field="ACTIVE_STATUS" data-tbl="bn_medal" data-su-url="setup/medal/medalById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/medal/view/' . $row->MEDAL_ID); ?>" title="View Medal" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/medal/edit/' . $row->MEDAL_ID); ?>"  title="Edit Medal" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->MEDAL_ID; ?>" title="Click For Delete" data-type="delete" data-field="MEDAL_ID" data-tbl="bn_medal"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>