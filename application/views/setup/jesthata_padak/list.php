<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
            <th>Description</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->JestotaMedal_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->CODE ?></td>
                <td><?php echo $row->NAME ?></td>
                <td><?php echo $row->SHORT_NAME ?></td>
                <td><?php echo $row->DESCR ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->JestotaMedal_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="JestotaMedal_ID" data-field="ACTIVE_STATUS" data-tbl="bn_jesthatapadak" data-su-url="setup/JesthataPadak/jpById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>

                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/JesthataPadak/view/' . $row->JestotaMedal_ID); ?>" title="View jesthata padak" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/JesthataPadak/edit/' . $row->JestotaMedal_ID); ?>"  title="Edit jesthata padak" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->JestotaMedal_ID; ?>" title="Click For Delete" data-type="delete" data-field="JestotaMedal_ID" data-tbl="bn_jesthatapadak"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>