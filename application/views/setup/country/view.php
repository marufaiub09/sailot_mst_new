
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        
        <tr>
            <th width="20%">Code</th>
            <td><?php echo $viewdetails->CODE?></td>
        </tr>
        <tr>
            <th>Name</th>
        <td><?php echo $viewdetails->NAME?></td>
        </tr> 
         <tr>
            <th>Short Name</th>
        <td><?php echo $viewdetails->SHORT_NAME ?></td>
        </tr> 
        <tr>
            <th>Bangla Name</th>
        <td><?php echo $viewdetails->BANGLA_NAME ?></td>
        </tr> 
        <tr>
            <th>Continent</th>
            <td>
                    <?php
                    if ($viewdetails->CONTINENTTYPE == '0') {
                        echo 'None';
                    } elseif ($viewdetails->CONTINENTTYPE == '1') {
                        echo 'Asia';
                    } elseif ($viewdetails->CONTINENTTYPE == '2') {
                        echo 'Europe';
                    } elseif ($viewdetails->CONTINENTTYPE == '3') {
                        echo 'Africa';
                    } elseif ($viewdetails->CONTINENTTYPE == '4') {
                        echo 'Australia';
                    } elseif ($viewdetails->CONTINENTTYPE == '5') {
                        echo 'North America';
                    } elseif ($viewdetails->CONTINENTTYPE == '6') {
                        echo 'South America';
                    } else {
                        echo 'Antarctica';
                    }
                    ?>
                </td>
        </tr>
</table>