<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Short Name</th>
            <th>Bangla Name</th>
            <th>Continent</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th> 
            <th>Short Name</th>
            <th>Bangla Name</th>
            <th>Continent</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->COUNTRY_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->CODE ?></td>
                <td><?php echo $row->NAME ?></td>
                <td><?php echo $row->SHORT_NAME ?></td>
                <td><?php echo $row->BANGLA_NAME ?></td>
                <td>
                    <?php
                    if ($row->CONTINENTTYPE == '0') {
                        echo 'None';
                    } elseif ($row->CONTINENTTYPE == '1') {
                        echo 'Asia';
                    } elseif ($row->CONTINENTTYPE == '2') {
                        echo 'Europe';
                    } elseif ($row->CONTINENTTYPE == '3') {
                        echo 'Africa';
                    } elseif ($row->CONTINENTTYPE == '4') {
                        echo 'Australia';
                    } elseif ($row->CONTINENTTYPE == '5') {
                        echo 'North America';
                    } elseif ($row->CONTINENTTYPE == '6') {
                        echo 'South America';
                    } else {
                        echo 'Antarctica';
                    }
                    ?>
                </td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->COUNTRY_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="COUNTRY_ID" data-field="ACTIVE_STATUS" data-tbl="bn_country" data-su-url="setup/Country/examNameById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>
                <td>
        <center>
            <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/Country/view/' . $row->COUNTRY_ID); ?>" title="View Country" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/Country/edit/' . $row->COUNTRY_ID); ?>"  title="Edit Country" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->COUNTRY_ID; ?>" title="Click For Delete" data-type="delete" data-field="COUNTRY_ID" data-tbl="bn_country"><span class="glyphicon glyphicon-trash"></span></a>
        </center>
    </td>
    </tr>
<?php } ?>
</tbody>
</table>