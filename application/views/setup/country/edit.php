<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Code<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code', 'value'=>$result->CODE,'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Country Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'name', 'value'=>$result->NAME, "class" => "form-control required",'required'=>'required', 'placeholder' => 'Country name')); ?>
        </div>
    </div>
     <div class="form-group">
        <label class="col-sm-3 control-label">Short Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Short Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'short_name', 'value'=>$result->SHORT_NAME, "class" => "form-control",'required'=>'required', 'placeholder' => 'Short name')); ?>
        </div>
    </div>
     <div class="form-group">
        <label class="col-sm-3 control-label">Bangla Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Bangla Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'bangla_name', 'value'=>$result->BANGLA_NAME, "class" => "form-control", 'placeholder' => 'Bangla name')); ?>
        </div>
    </div>
     <div class="form-group">
        <label class="col-sm-3 control-label">Continent<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Continent Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="Continent" id="Continent" data-tags="true" data-placeholder="Select Continent Type" data-allow-clear="true">
              <option value="">Select Continent Type</option>
                <option value="0" <?php echo ($result->CONTINENTTYPE == 0) ? 'selected' : '' ?>>None</option>
                <option value="1" <?php echo ($result->CONTINENTTYPE == 1) ? 'selected' : '' ?>>Asia</option>
                <option value="2" <?php echo ($result->CONTINENTTYPE == 2) ? 'selected' : '' ?>>Europe</option>
                <option value="3" <?php echo ($result->CONTINENTTYPE == 3) ? 'selected' : '' ?>>Africa</option>
                <option value="4" <?php echo ($result->CONTINENTTYPE == 4) ? 'selected' : '' ?>>Australia</option>
                <option value="5" <?php echo ($result->CONTINENTTYPE == 5) ? 'selected' : '' ?>>North America</option>
                <option value="6" <?php echo ($result->CONTINENTTYPE == 6) ? 'selected' : '' ?>>South America</option>
                <option value="7" <?php echo ($result->CONTINENTTYPE == 7) ? 'selected' : '' ?>>Antarctica</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1',  ($result->ACTIVE_STATUS == 1)? TRUE:FALSE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->COUNTRY_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/Country/updatePart" data-su-action="setup/Country/partList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>


