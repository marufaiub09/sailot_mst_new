<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-3 control-label">Code<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'code', 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required' => 'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Country Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'name', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Country name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Short Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Short Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'shortname', "class" => "form-control", 'required' => 'required', 'placeholder' => 'Short name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Bangla Name</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Bangla Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'banglaname', "class" => "form-control", 'placeholder' => 'Bangla name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Continent<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Continent Types ">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="continent" id="continent" data-tags="true" data-placeholder="Select Continent" data-allow-clear="true">
                <option value="">Select Continent Type</option>
                <option value="0">None</option>
                <option value="1">Asia</option>
                <option value="2">Europe</option>
                <option value="3">Africa</option>
                <option value="4">Australia</option>
                <option value="5">North America</option>
                <option value="6">South America</option>
                <option value="7">Antarctica</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/Country/savePart" data-su-action="setup/Country/partList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>

