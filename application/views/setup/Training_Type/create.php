<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group">
        <label class="col-sm-3 control-label">Training Code<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Training Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'training_code', 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required",'required'=>'required', 'placeholder' => 'Training Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Training Name<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Training Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'training_name', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Training name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Training Name (বাংলা)</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Training Bangali Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'training_name_bn', "class" => "form-control", 'placeholder' => 'বাংলা নাম লিখুন')); ?>
        </div>
    </div>
    <div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('parentid'); ?><span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Parent ID ">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="parentid" id="parentid" data-tags="true" data-placeholder="Select Parent ID" data-allow-clear="true">
                <option value="">Select Parent ID</option>
                <?php
                foreach ($parentid as $row):
                    ?>
                    <option value="<?php echo $row->NAVYTrainingID ?>"><?php echo $row->Name ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
<!--    <div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('parentid'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Parent Id">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_dropdown('parentid', $parentid, '', 'id="doidgroup" required="required" class="form-control select2"'); ?>
    </div>
</div>-->
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/TrainingType/saveTrainignName" data-su-action="setup/TrainingType/trainingList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>
