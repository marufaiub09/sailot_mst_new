<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Name (BN)</th>
            <th>Training Type</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Name (BN)</th>
            <th>Training Type</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($traininName as $key => $values) { ?>
            <tr id="row_<?php echo $values->NAVYTrainingID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $values->Code ?></td>
                <td><?php echo $values->Name ?></td>
                <td><?php echo $values->BanglaName ?></td>
                <td><?php echo $values->BN_NAME ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $values->NAVYTrainingID; ?>" data-status="<?php echo $values->ACTIVE_STATUS ?>" data-fieldId="NAVYTrainingID" data-field="ACTIVE_STATUS" data-tbl="bn_navytraininghierarchy" data-su-url="setup/TrainingType/trainingById/<?php echo $key + 1 ?>">
                        <?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>
                <td>
        <center>
            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/TrainingType/edit/' . $values->NAVYTrainingID); ?>"  title="Edit Training" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $values->NAVYTrainingID; ?>" title="Click For Delete" data-type="delete" data-field="NAVYTrainingID" data-tbl="bn_navytraininghierarchy"><span class="glyphicon glyphicon-trash"></span></a>
        </center>
    </td>
    </tr>
<?php } ?>
</tbody>
</table>