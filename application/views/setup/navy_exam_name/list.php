<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Exam Name</th>
            <th>Exam Name (বাংলা)</th>
            <th>Exam Type</th>
            <th>Exam Level</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php //echo "<PRE>"; print_r($result); exit;?>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->EXAM_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->CODE ?></td>
                <td><?php echo $row->NAME ?></td>
                <td><?php echo $row->BANGLA_NAME ?></td>
                <td><?php echo $row->type ?></td>
                <td><?php echo $row->level ?></td>
                <td>
                <center>
                    <a class="itemStatus" id="<?php echo $row->EXAM_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="EXAM_ID" data-field="ACTIVE_STATUS" data-tbl="bn_navyexam_hierarchy" data-su-url="setup/navyExamName/examNameById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/navyExamName/edit/' . $row->EXAM_ID); ?>"  title="Edit Exam Name" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->EXAM_ID; ?>" title="Click For Delete" data-type="delete" data-field="EXAM_ID" data-tbl="bn_navyexam_hierarchy"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>