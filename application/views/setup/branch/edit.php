<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    <div class="form-group">
        <label class="col-sm-3 control-label">Code  <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'branchCode', 'value' => $result->BRANCH_CODE,'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required' => 'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Branch Name  <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Branch Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'branchName', 'value' => $result->BRANCH_NAME, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Branch name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Branch Name Bangla</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Branch Name Bangla">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'banglaName', 'value' => $result->BANGLA_NAME, "class" => "form-control", 'placeholder' => 'Bangla Name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('doidgroup'); ?>  <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select Group Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <?php echo form_dropdown('daogroupid', $daogroupid, $item->GROUP_ID, 'id="doidgroup" required="required" class="form-control select2"'); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Position  <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Position">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'position','value' => $result->POSITION, "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Position')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->BRANCH_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/Branch/updatePart" data-su-action="setup/Branch/branchList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>
