<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        <tr>
        <th><?php echo $this->lang->line('branch_name') ?> </th>
        <td><?php echo $result->BRANCH_NAME; ?></td>
    </tr>
    <tr>
        <th> <?php echo $this->lang->line('branch_name_in_bnagla') ?></th>
        <td><?php echo $result->BANGLA_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('branch_code') ?> </th>
        <td><?php echo $result->BRANCH_CODE; ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang->line('position'); ?></th>
        <td><?php echo $result->POSITION; ?></td>
    </tr>
    
  <tr>
        <th><?php echo $this->lang->line('doidgroup') ?> </th>
        <td><?php echo $bn_daogroup->GROUP_NAME; ?></td>
    </tr>
<tr>
        <th><?php echo $this->lang->line('is_active'); ?></th>
        <td><?php echo $result->ACTIVE_STATUS == 1 ? 'YES' : 'NO'; ?></td>
    </tr>
</table>


