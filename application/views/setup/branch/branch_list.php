<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Bangla Name</th>
            <th>Group Name</th>
            <th>Position</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Bangla Name</th>
            <th>Group Name</th>
            <th>Position</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->BRANCH_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->BRANCH_CODE ?></td>
                <td><?php echo $row->BRANCH_NAME ?></td>
                <td><?php echo $row->BANGLA_NAME ?></td>
                <td><?php echo $row->BN_NAME ?></td>
                <td><?php echo $row->POSITION ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->BRANCH_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="BRANCH_ID" data-field="ACTIVE_STATUS" data-tbl="bn_branch" data-su-url="setup/Branch/BranchById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>

                <td>
        <center>
            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/Branch/edit/' . $row->BRANCH_ID); ?>"  title="Edit Branch" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->BRANCH_ID; ?>" title="Click For Delete" data-type="delete" data-field="BRANCH_ID" data-tbl="bn_branch"><span class="glyphicon glyphicon-trash"></span></a>
        </center>
    </td>
    </tr>
<?php } ?>
</tbody>
</table>