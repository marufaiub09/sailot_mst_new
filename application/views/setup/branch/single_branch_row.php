<td><?php echo $sn; ?></td>
<td><?php echo $row->BRANCH_CODE ?></td>
<td><?php echo $row->BRANCH_NAME ?></td>
<td><?php echo $row->BANGLA_NAME ?></td>
<td><?php echo $row->BN_NAME ?></td>
<td><?php echo $row->POSITION ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->BRANCH_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="BRANCH_ID" data-field="ACTIVE_STATUS" data-tbl="bn_branch" data-su-url="setup/Branch/BranchById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/Branch/edit/' . $row->BRANCH_ID); ?>" title="Edit Branch" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->BRANCH_ID; ?>" title="Click For Delete" data-type="delete" data-field="BRANCH_ID" data-tbl="bn_branch"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>