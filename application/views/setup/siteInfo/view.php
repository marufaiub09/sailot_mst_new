
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th><?php echo $this->lang->line('site_info_type') ?></th>
        <td><?php echo $viewdetails->TYPE ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('site_info_title') ?></th>
        <td><?php echo $viewdetails->S_INFO_TITLE ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('desc') ?></th>
        <td><?php echo $viewdetails->S_INFO_DESC ?></td>
    </tr>
</table>