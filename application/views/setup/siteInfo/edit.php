<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open('', "id='dgdpMainForm' class='form-horizontal'"); ?>

<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('site_info_type') ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_input(array('name' => 'siteInfoType', 'maxlength' => '3', "class" => "form-control", 'value' => $row->TYPE, 'placeholder' => 'Site Information Type','required' => 'required')); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('site_info_title') ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_input(array('name' => 'siteInfoTitle', 'value' => $row->S_INFO_TITLE, 'maxlength' => '200', "class" => "form-control", 'required placeholder' => 'Site Information Title','required' => 'required')); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('desc') ?></label>
    <div class="col-sm-6">
        <textarea class="form-control" name="siteInfoDescription" rows="3"><?php echo $row->S_INFO_DESC; ?></textarea>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('ACTIVE_STATUS', '1', ($row->ACTIVE_STATUS == 1)?true:false, 'class="styled"'); ?>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-3">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit') ?></button>
    </div>
</div>
<?php echo form_close(); ?>


<script type="text/javascript">
    $(document).ready(function() {
        tinymce.init({
            selector: 'textarea',
            height: 200,
            plugins: [
                'searchreplace visualblocks code fullscreen',
            ],
            toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
            content_css: [
                '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
                '//www.tinymce.com/css/codepen.min.css'
            ]
        });
    });
</script>