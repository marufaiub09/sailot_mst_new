<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Site Information</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" href="<?php echo site_url('setup/siteInfo/create/'); ?>" class="btn btn-primary btn-xs modalLink"  title="<?php echo $this->lang->line('create_site_information') ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('type'); ?></th>
                            <th><?php echo $this->lang->line('site_info_title'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th><?php echo $this->lang->line('type'); ?></th>
                            <th><?php echo $this->lang->line('site_info_title'); ?></th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php foreach ($data as $key => $values) { ?>
                            <tr>
                                <td><?php echo $key + 1 ?></td>
                                <td><?php echo $values->TYPE ?></td>
                                <td><?php echo $values->S_INFO_TITLE ?></td>
                                <td class="text-center"><?php echo ($values->ACTIVE_STATUS == 1) ? '<span class="btn btn-xs btn-success waves-effect waves-button">Active</span>' : '<span class="btn btn-xs btn-danger waves-effect waves-button waves-float">Inactive</span>'; ?></td>
                                <td class="text-center">
                                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/siteInfo/view/' . $values->S_INFO_ID); ?>" title="Post Office View"><span class="glyphicon glyphicon-eye-open"></span></a>
                                    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/siteInfo/edit/' . $values->S_INFO_ID); ?>" title="Post Office Edit" type="button"><span class="edit_division glyphicon glyphicon-edit"></span></a>
            <!--                            <a class="btn btn-danger btn-xs" href="<?php echo site_url('setup/siteInfo/delete/' . $values->S_INFO_ID); ?>" title="Delete" type="button"><span class="glyphicon glyphicon-trash"></span></i></a>-->
                                </td>
                            </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

