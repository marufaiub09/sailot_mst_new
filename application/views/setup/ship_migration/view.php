
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        
        <tr>
            <th width="20%">Ship/Establishment</th>
            <td><?php echo $viewdetails->SHIP_ESTABLISHMENT?></td>
        </tr>
        <tr>
            <th>Previous Area</th>
            <td><?php echo $viewdetails->PRE_AREA?></td>
        </tr><tr>
            <th>New Area</th>
            <td><?php echo $viewdetails->NEW_AREA?></td>
        </tr>
        <tr>
            <th>Migration Date</th>
            <td><?php echo date('d-M-Y', strtotime($viewdetails->CHANGE_DATE)); ?></td>
        </tr>
        
</table>