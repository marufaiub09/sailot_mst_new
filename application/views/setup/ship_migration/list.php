<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Ship/Establishment</th>
            <th>Previous Area</th>
            <th>New Area</th>            
            <th>Migration Date</th>   
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->SHIP_MIGRATION_ID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->SHIP_ESTABLISHMENT ?></td>
                <td><?php echo $row->PRE_AREA ?></td>
                <td><?php echo $row->NEW_AREA ?></td>
                <td><?php echo date('d-M-Y', strtotime($row->CHANGE_DATE)); ?></td>
                <td>
                <center>
                    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/shipMigration/view/' . $row->SHIP_MIGRATION_ID); ?>" title="View Ship migration" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/shipMigration/edit/' . $row->SHIP_MIGRATION_ID); ?>"  title="Edit Ship migration" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->SHIP_MIGRATION_ID; ?>" title="Click For Delete" data-type="delete" data-field="SHIP_MIGRATION_ID" data-tbl="bn_ship_migration"><span class="glyphicon glyphicon-trash"></span></a>
                </center>
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>