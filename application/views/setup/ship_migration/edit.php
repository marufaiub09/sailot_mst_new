<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="form-group">
        <label class="col-sm-3 control-label">Ship/Establishment</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Ship/Establishment">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-5">
            <select class="select2 form-control required" name="SE_ID" id="SE_ID" data-tags="true" data-placeholder="Select Ship/Establishment" data-allow-clear="true" disabled>
                <option value="">Select Ship/Establishment</option>
                <?php
                foreach ($shipMigration as $row):
                    ?>
                    <option value="<?php echo $row->SHIP_ESTABLISHMENTID; ?>"  <?php echo ($result->SHIP_ESTABLISHMENTID == $row->SHIP_ESTABLISHMENTID) ? 'selected' : '' ?>><?php echo $row->CODE." ".$row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Previous Area</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Previous Area">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-2">
            <input type="hidden" id="areaId" name="PRE_AREA_ID" value="<?php echo $result->PREVIOUS_AREA_ID ?>">
            <input type="hidden" id="zoneId" name="PRE_ZONE_ID" value="<?php echo $result->PREVIOUS_ZONE_ID ?>">
            <?php echo form_input(array('name' => 'areaCode', 'value'=> $result->PRE_CODE, "class" => "form-control required ", 'id' => 'areaCode', 'required'=>'required', 'placeholder' => 'Code', 'readonly'=>'true')); ?>           
        </div>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'areaName', 'value'=> $result->PRE_AREA, "class" => "form-control required", 'id' => 'areaName', 'required'=>'required', 'placeholder' => 'Name', 'readonly'=>'true')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">New Area<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select new area">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="NEW_AREA_ID" id="NEW_AREA_ID" data-tags="true" data-placeholder="Select new area" data-allow-clear="true">
                <option value="">Select new area</option>
                <?php
                foreach ($newArea as $row):
                    ?>
                    <option value="<?php echo $row->ADMIN_ID ?>"  <?php echo ($result->NEW_AREA_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo $row->CODE." ". $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Migration Date<span class="text-danger"> * </span></label>
        <div class="col-sm-3 date">
            <div class="selectContainer">
                <div class="input-group input-append date" id="datePicker">
                    <?php echo form_input(array('name' => 'migrationDate', 'value'=> date('d-m-Y', strtotime($result->CHANGE_DATE)), "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Date')); ?>
                    <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->SHIP_MIGRATION_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/shipMigration/updateSM" data-su-action="setup/shipMigration/smList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script>
    $(document).ready(function() {
        $("#SE_ID").on('change', function(){
           var shipEstId = this.value;
           $.ajax({
                type: "post",
                dataType: 'json',
                url: "<?php echo site_url(); ?>setup/shipMigration/areaByShipEst",
                data: {shipEstId: shipEstId},
                success: function (data) {
                    $("#areaId").val(data['ADMIN_ID']);
                    $("#zoneId").val(data['zoneId']);
                    $("#areaCode").val(data['CODE']);
                    $("#areaName").val(data['NAME']);
                }
            });
        });
        $('#SE_ID').prop( "disabled", true );


    });
</script>