
<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
        
        <tr>
            <th width="10%">Code</th>
            <td><?php echo $viewdetails->CODE?></td>
            <th colspan="4"></th>
        </tr>
        <tr>
            <th>Name</th>
            <td><?php echo $viewdetails->NAME?></td>
            <th>Short Name</th>
            <td><?php echo $viewdetails->SHORT_NAME?></td>
            <th>More Short Name</th>
            <td><?php echo $viewdetails->MORE_SHORT_NAME?></td>
        </tr>
        <tr>
            <th>Name (বাংলা)</th>
            <td><?php echo $viewdetails->BANGLA_NAME?></td>
            <th>Short Name(বাংলা)</th>
            <td><?php echo $viewdetails->BANGLA_SHORT_NAME?></td>
            <th colspan="2"></th>
        </tr>
        <tr>
            <th>Ship Type</th>
            <td><?php echo $viewdetails->shipType?></td>
            <th colspan="4"></th>
        </tr>
        <tr>
            <th>Area</th>
            <td><?php echo $viewdetails->Area?></td>
            <th colspan="4"></th>
        </tr>
        <tr>
            <th>Zone</th>
            <td><?php echo $viewdetails->Zone?></td>
            <th colspan="4"></th>
        </tr>

        
</table>