<td><?php echo $sn ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td><?php echo $row->BANGLA_NAME ?></td>
<td><?php echo $row->shipType ?></td>
<td><?php echo $row->Area ?></td>
<td><?php echo $row->Zone ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->SHIP_ESTABLISHMENTID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="SHIP_ESTABLISHMENTID" data-field="ACTIVE_STATUS" data-tbl="bn_ship_establishment" data-su-url="setup/ShipEstablishment/seById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/ShipEstablishment/view/' . $row->SHIP_ESTABLISHMENTID); ?>" title="View Ship Establishment" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/ShipEstablishment/edit/' . $row->SHIP_ESTABLISHMENTID); ?>"  title="Edit Ship Establishment" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->SHIP_ESTABLISHMENTID; ?>" title="Click For Delete" data-type="delete" data-field="SHIP_ESTABLISHMENTID" data-tbl="bn_ship_establishment"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>