<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-3 control-label">Code<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'code', 'value'=>$result->CODE,'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
                </div>
            </div>            
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-5 control-label">Short Name</label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Ship Establishment Short Name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'st_name','value'=>$result->SHORT_NAME, "class" => "form-control ", 'placeholder' => 'Short name')); ?>
                </div>
            </div>            
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-3 control-label">Name<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Ship Establishment Name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                   <?php echo form_input(array('name' => 'name','value'=>$result->NAME, "class" => "form-control required",'required'=>'required', 'placeholder' => 'Ship establishment name')); ?>
                </div>
            </div>            
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-5 control-label">More Short Name</label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter ship establishment more short name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'mr_st_name', 'value'=>$result->MORE_SHORT_NAME, "class" => "form-control ", 'placeholder' => 'More short name')); ?>
                </div>
            </div>            
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-3 control-label">Name (বাংলা)</label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter bangali Ship Establishment Name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                   <?php echo form_input(array('name' => 'bn_name', 'value'=>$result->BANGLA_NAME, "class" => "form-control ", 'placeholder' => 'Ship Establishment name')); ?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-5 control-label">Short Name(বাংলা)</label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter bangali Ship Establishment Name">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                   <?php echo form_input(array('name' => 'bn_srt_name', 'value'=>$result->BANGLA_SHORT_NAME, "class" => "form-control ", 'placeholder' => 'Short name')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-3 control-label">Zone<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select zone">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <select class="select2 form-control required" name="ZONE_ID" id="ZONE_ID" data-tags="true" data-placeholder="Select Zone" data-allow-clear="true">
                        <option value="">Select Zone</option>
                        <?php
                        foreach ($zone as $row):
                            ?>
                            <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->ZONE_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-5 control-label">Ship-Type<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select ship  type">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-6">
                    <select class="select2 form-control required" name="SHIPTYPE_ID" id="SHIPTYPE_ID" data-tags="true" data-placeholder="Select Ship type" data-allow-clear="true">
                        <option value="">Select Ship Type</option>
                        <?php
                        foreach ($shipType as $row):
                            ?>
                            <option value="<?php echo $row->SHIP_TYPEID ?>" <?php echo ($result->SHIP_TYPEID == $row->SHIP_TYPEID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                        <?php
                        endforeach; 
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-sm-3 control-label">Area<span class="text-danger"> * </span></label>
                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Area">
                    <i class="fa fa-question-circle"></i>
                </a>
                <div class="col-sm-7">
                    <select class="select2 form-control required" name="AREA_ID" id="AREA_ID" data-tags="true" data-placeholder="Select Area" data-allow-clear="true">
                        <option value="">Select Area</option> 
                        <?php 
                        foreach ($area as $row):
                            if($row->PARENT_ID == $result->ZONE_ID):
                            ?>
                                <option value="<?php echo $row->ADMIN_ID ?>" <?php echo ($result->AREA_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                        <?php
                            endif;
                        endforeach; 
                        ?>                          
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', ($result->ACTIVE_STATUS == 1)? TRUE:FALSE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->SHIP_ESTABLISHMENTID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/shipEstablishment/updateSE" data-su-action="setup/shipEstablishment/seList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script>
    $(document).ready(function() {
        $("#ZONE_ID").on('change', function(){
           var zone = this.value;
           $.ajax({
                type: "post",
                url: "<?php echo site_url(); ?>/setup/ShipEstablishment/areaByZone",
                data: {zoneId: zone},
                success: function (data) {
                    $("#AREA_ID").html(data);
                    $('#AREA_ID').select2('val', '');
                }
            });
        });
    });
</script>
<?php $this->load->view("common/sailors_info"); ?>
