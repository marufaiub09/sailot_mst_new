<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>            
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->OtherSpecializationID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->CODE ?></td>
                <td><?php echo $row->NAME ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->OtherSpecializationID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="OtherSpecializationID" data-field="ACTIVE_STATUS" data-tbl="bn_otherspecialization" data-su-url="setup/specialization/catById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>

                <td>
                    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/specialization/catEdit/' . $row->OtherSpecializationID); ?>"  title="Edit Category" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
                
                </td>
            </tr>
    <?php } ?>
    </tbody>
</table>