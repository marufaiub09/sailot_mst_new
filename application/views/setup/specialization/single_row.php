<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
	<?php 
    $cat = $this->db->query("SELECT * FROM bn_otherspecialization WHERE OtherSpecializationID = $row->PARENT_ID")->row();
    echo $cat->NAME ;
    ?>
</td>
<td>
    <a class="itemStatus" id="<?php echo $row->OtherSpecializationID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="OtherSpecializationID" data-field="ACTIVE_STATUS" data-tbl="bn_otherspecialization" data-su-url="setup/specialization/specById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>    
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/specialization/edit/' . $row->OtherSpecializationID); ?>"  title="Edit Specialization" type="button" ><span class="glyphicon glyphicon-edit"></span></a>

    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->OtherSpecializationID; ?>" title="Click For Delete" data-type="delete" data-field="OtherSpecializationID" data-tbl="bn_otherspecialization"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>