<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->OtherSpecializationID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="OtherSpecializationID" data-field="ACTIVE_STATUS" data-tbl="bn_otherspecialization" data-su-url="setup/specialization/catById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/specialization/catEdit/' . $row->OtherSpecializationID); ?>"  title="Edit Category" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
</td>