<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Code<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code', 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required",'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Specialization Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'name', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Specialization name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Specialization<span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Specialization Type">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <?php 
                $spec = array(
                        "Specialization Category",
                        "Specialization"
                    );
            ?>
            <select class="select2 form-control required" name="SPEC_ID" id="SPEC_ID" data-tags="true" data-placeholder="Select Specification" data-allow-clear="true">
                <?php
                foreach ($spec as $key => $row):
                    ?>
                    <option value="<?php echo $key+1 ?>"><?php echo $row ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <span id="speCat" style="display: none;">
        <div class="form-group" >
            <label class="col-sm-3 control-label">Specialization Category<span class="text-danger"> * </span></label>
            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Specialization Category">
                <i class="fa fa-question-circle"></i>
            </a>
            <div class="col-sm-4">
                <select class="select2 form-control required" name="SPEC_CAT_ID" id="SPEC_CAT_ID" data-tags="true" data-placeholder="Select Category" data-allow-clear="true" style="width:100%">
                    <?php
                    foreach ($Specialization as $row):
                        ?>
                        <option value="<?php echo $row->OtherSpecializationID ?>"><?php echo $row->NAME ?></option>
                    <?php
                    endforeach; 
                    ?>
                </select>
            </div>
        </div>        
    </span>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/specialization/saveSpec" data-su-action="setup/specialization/specList" data-type="list" value="submit">
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('#SPEC_ID').change(function () {
            var val = $(this).val();
            if(val == 2){
                $("#speCat").show();
            }else{
                $("#speCat").hide ();                
            }
        });
    });
</script>
<?php $this->load->view("common/sailors_info"); ?>
