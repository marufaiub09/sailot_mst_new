<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->VISIT_CLASSIFICATIONID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="VISIT_CLASSIFICATIONID" data-field="ACTIVE_STATUS" data-tbl="bn_visitclassification" data-su-url="setup/visitclassification/vcById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/visitclassification/view/' . $row->VISIT_CLASSIFICATIONID); ?>" title="View Visit Classcification" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/visitclassification/edit/' . $row->VISIT_CLASSIFICATIONID); ?>"  title="Edit Visit Classcification" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->VISIT_CLASSIFICATIONID; ?>" title="Click For Delete" data-type="delete" data-field="VISIT_CLASSIFICATIONID" data-tbl="bn_visitclassification"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>