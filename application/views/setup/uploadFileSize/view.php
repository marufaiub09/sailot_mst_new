<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th> <?php echo $this->lang->line('organization_name') ?></th>
        <td><?php echo $org_data->ORG_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('rank_ud_id') ?> </th>
        <td><?php echo $result->UD_RANK_ID; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('rank_name') ?> </th>
        <td><?php echo $result->RANK_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('parent_rank'); ?></th>
        <td><?php echo $rank_data->RANK_NAME; ?></td>
    </tr>
    <tr>
        <th><?php echo $this->lang->line('rank_description') ?> </th>
        <td><?php echo $result->RANK_DESCRIPTION; ?></td>
    </tr>

    <tr>
        <th><?php echo $this->lang->line('rank_image') ?></th>  
        <?php
        $dir = "src/upload/rank_image/"; //image path to a folder
        ?>
        <td>
            <?php
            if ($result->IMAGES_PATH > 0) {
                ?>
                <img id="imagePreview_edit" src="<?php echo base_url($dir) . "/" . $result->IMAGES_PATH; ?>" alt="" width="130" height="130">
                <?php
            }
            ?>
        </td>
    </tr>

    <tr>
        <th><?php echo $this->lang->line('is_active'); ?></th>
        <td><?php echo $result->ACTIVE_STATUS == 1 ? 'YES' : 'NO'; ?></td>
    </tr>


</table>


