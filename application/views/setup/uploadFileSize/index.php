<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"> Setup Upload file size limit<?php //echo $this->lang->line('rank_list');       ?></h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">

                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body panel-group">
                <?php echo form_open('setup/uploadfilesize/save', array('id' => 'this')); ?>
                <input type="hidden" name="file_info_id" value="<?php echo (!empty($file_info))?$file_info->F_INFO_ID:''; ?>"/>
                <div class="form-group">
                    <label class="col-sm-3 control-label">General file size limit (KB)<?php //echo $this->lang->line('rank_list');       ?></label>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter maximum file size in KB. [ 1MB = 1024 KB ]<?php //echo $this->lang->line("msg_full_name"); ?>">
                        <i class="fa fa-question-circle"></i>
                    </a>
                    <div class="col-sm-2">
                        <?php echo form_input(array('name' => 'generalFile', 'class' => "form-control numericOnly", 'value' => (!empty($file_info))?$file_info->GENERAL_FILE:'', 'placeholder' => 'file size in KB', 'autocomplete' => "off")); ?>
                    </div>
                </div>

                <div class="form-group">&nbsp;</div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Others file size limit (KB)<?php //echo $this->lang->line('rank_list');       ?></label>
                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter maximum file size in KB. [ 1MB = 1024 KB ]<?php //echo $this->lang->line("msg_full_name"); ?>">
                        <i class="fa fa-question-circle"></i>
                    </a>
                    <div class="col-sm-2">
                        <?php echo form_input(array('name' => 'othersFile', 'class' => "form-control numericOnly", 'value' => (!empty($file_info))?$file_info->OTHERS_FILE:'', 'placeholder' => 'file size in KB', 'autocomplete' => "off")); ?>
                    </div>
                </div>
                <div class="form-group">&nbsp;</div>
                <div class="form-group">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-2">
                        <input type="submit" class="btn btn-success btn-sm" id="submit" value="Save" />
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('body').on('keyup', '.numericOnly', function () {
            var val = $(this).val();
            $(this).val(val.replace(/[^\d]/g, ''));
        });
    });
</script>