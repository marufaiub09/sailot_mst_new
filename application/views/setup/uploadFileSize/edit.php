<style type="text/css">
    #imagePreview {
        width: 130px;
        height: 130px;
        background-position: center center;
        background-size: cover;
        -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
        display: inline-block;
        display: none;
        border: 1px solid #002166;
        float: left;
        margin-top: 10px;

    }
    
    #imagePreview_edit{
        margin-top: 10px;
    }
</style>
<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open_multipart('', "id='dgdpMainForm' class='form-horizontal'"); ?>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('organization'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select organization Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">

        <?php echo form_dropdown('organizations', $organizations, $item->ORG_ID, 'required="required" class="form-control select2"'); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_ud_id'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'rank_ud_id', 'required' => 'required', "class" => "form-control caste_name", 'value' => $result->UD_RANK_ID)); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_name'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-4">
        <?php echo form_input(array('name' => 'rank_name', 'required' => 'required', "class" => "form-control caste_name", 'value' => $result->RANK_NAME)); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('parent_rank'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select organization Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_dropdown('parent_rank', $rank, $parent_rank->PARENT_RANK_ID, 'id="parent_rank" required="required" class="form-control select2"'); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_description'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-4">
        <?php echo form_textarea(array('rows' => '3', 'name' => 'rank_description', "class" => "form-control caste_name", 'value' => $result->RANK_DESCRIPTION)); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_image'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_input(array('type' => 'file', 'name' => 'rank_image', 'id' => 'rank_image', "class" => "")); ?>
        <?php
        if($result->IMAGES_PATH != ''){
        $dir = "src/upload/rank_image/"; // Image Path to folder
        ?>
        <img id="imagePreview_edit" src="<?php echo base_url($dir)."/".$result->IMAGES_PATH; ?>" alt="" width="130" height="130">
        <div class="col-sm-6 preview_div" id="imagePreview"></div>
        <?php } ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <input type="checkbox" id="ACTIVE_STATUS" name="ACTIVE_STATUS" class="styled checkBoxStatus" <?php echo $result->ACTIVE_STATUS == 1 ? 'checked' : '' ?>/>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
    </div>
</div>
<?php echo form_close(); ?>


<script type="text/javascript">
    // This function is used for preview image before Upload
    $(function() {
        $("#rank_image").on("change", function()
        {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
            if (/^image/.test( files[0].type)){ // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
 
                reader.onloadend = function(){ // set image data as background of div
                    $("#imagePreview").css("background-image", "url("+this.result+")");
                    $("#imagePreview").show();
                    $('#imagePreview_edit').hide();
                }
            }
        });
    });
</script>


<script type="text/javascript">
    $(document).on('click', '.checkBoxStatus', function () {
        var active_flag = ($(this).is(':checked')) ? 1 : 0;
        $("#ACTIVE_STATUS").val(active_flag);
    });

</script>