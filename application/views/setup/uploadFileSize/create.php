<style type="text/css">
 #imagePreview {
        width: 130px;
        height: 130px;
        background-position: center center;
        background-size: cover;
        -webkit-box-shadow: 0 0 1px 1px rgba(0, 0, 0, .3);
        display: inline-block;
        display: none;
        border: 1px solid #002166;
        float: left;
        margin-top: 10px;

    }
</style>
<?php echo validation_errors(); ?>
<!-- form start -->
<?php echo form_open_multipart('', "id='dgdpMainForm' class='form-horizontal'"); ?>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('organization'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select organization Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_dropdown('organizations', $organizations, '', 'id="organizations" required="required" class="form-control select2"'); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_ud_id'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'rank_ud_id', 'required' => 'required', "class" => "form-control caste_name", 'placeholder' => $this->lang->line('rank_ud_id'))); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_name'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'rank_name', 'required' => 'required', "class" => "form-control caste_name", 'placeholder' => $this->lang->line('rank_name'))); ?>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('parent_rank'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Select organization Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_dropdown('parent_rank', $rank_list, '', 'id="parent_rank" required="required" class="form-control select2"'); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_description'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-4">
        <?php echo form_textarea(array('rows' => '3','name' => 'rank_description', "class" => "form-control caste_name", 'placeholder' => $this->lang->line('rank_description'))); ?>
    </div>
</div>
<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('rank_image'); ?></label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter District Name">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-6">
        <?php echo form_input(array('type' => 'file', 'name' => 'rank_image', 'id' => 'rank_image', "class" => "")); ?>
        <div class="col-sm-6 preview_div" id="imagePreview"></div>
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;</label>
    <div class="col-sm-6">
        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('submit'); ?></button>
    </div>
</div>
<?php echo form_close(); ?>


<script type="text/javascript">
    // This function is used for preview image before Upload
    $(function() {
        $("#rank_image").on("change", function()
        {
            var files = !!this.files ? this.files : [];
            if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
 
            if (/^image/.test( files[0].type)){ // only image file
                var reader = new FileReader(); // instance of the FileReader
                reader.readAsDataURL(files[0]); // read the local file
 
                reader.onloadend = function(){ // set image data as background of div
                    $("#imagePreview").css("background-image", "url("+this.result+")");
                    $("#imagePreview").show();
                }
            }
        });
    });
</script>



