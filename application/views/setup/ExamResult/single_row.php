<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->EXAM_RESULT_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="EXAM_RESULT_ID" data-field="ACTIVE_STATUS" data-tbl="bn_exam_result" data-su-url="setup/ExamResult/examResultById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('setup/ExamResult/edit/' . $row->EXAM_RESULT_ID); ?>" title="Edit Exam Result" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
</center>
</td>