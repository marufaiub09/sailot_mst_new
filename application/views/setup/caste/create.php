<?php echo validation_errors(); ?>
    <!-- form start -->
<?php echo form_open('',"id='createModuleForm' class='form-horizontal update_caste'"); ?>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('religion'); ?></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_dropdown('RELIGION_ID', $religion_list, '','id="office_district" class="form-control" required');?>
        </div>
    </div>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('caste'); ?></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus.">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
            <?php echo form_input(array('name' => 'CASTE_NAME', "class" => "form-control caste_name", 'required placeholder' => $this->lang->line('caste_name'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
<?php echo form_close(); ?>