<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->CATEGORY_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="CATEGORY_ID" data-field="ACTIVE_STATUS" data-tbl="bn_fraudulentcategory" data-su-url="setup/fraudulentCategory/fcById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/fraudulentCategory/view/' . $row->CATEGORY_ID); ?>" title="View fraudulent category" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/fraudulentCategory/edit/' . $row->CATEGORY_ID); ?>"  title="Edit fraudulent category" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->CATEGORY_ID; ?>" title="Click For Delete" data-type="delete" data-field="CATEGORY_ID" data-tbl="bn_fraudulentcategory"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>