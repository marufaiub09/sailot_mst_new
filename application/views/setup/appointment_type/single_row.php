<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->APPOINT_TYPEID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="APPOINT_TYPEID" data-field="ACTIVE_STATUS" data-tbl="bn_appointmenttype" data-su-url="setup/appointmentType/atById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/appointmentType/view/' . $row->APPOINT_TYPEID); ?>" title="View Appointment Type" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/appointmentType/edit/' . $row->APPOINT_TYPEID); ?>"  title="Edit Appointment Type" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->APPOINT_TYPEID; ?>" title="Click For Delete" data-type="delete" data-field="APPOINT_TYPEID" data-tbl="bn_appointmenttype"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>