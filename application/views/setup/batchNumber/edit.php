<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Batch Number</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Mission Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-5">
           <?php echo form_input(array('name' => 'batchNumber', 'value'=>$result->BATCH_NUMBER, "class" => "form-control required", 'required'=>'required', 'placeholder' => 'Batch number')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Year of Batch</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Year of Batch">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-2">
           <?php echo form_input(array('name' => 'yearOfBatch', 'value'=>$result->YEAR_OF_BATCH, 'type'=>'number', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Year of Batch')); ?>
        </div>
    </div>
        
    <div class="form-group">
        <label class="col-sm-3 control-label">Is Process</label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('IS_PROCESS', '1', ($result->IS_PROCESS == 1)? TRUE:FALSE, 'class="styled"'); ?>
                <label for="IS_PROCESS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->BATCH_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/batchNumber/updateBatchNumber" data-su-action="setup/batchNumber/batchNumberList" data-type="list" value="submit">
        </div>
    </div>
</form>
