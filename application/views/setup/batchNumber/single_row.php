<td><?php echo $sn; ?></td>
<td><?php echo $row->BATCH_NUMBER ?></td>
<td><?php echo $row->YEAR_OF_BATCH ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->BATCH_ID; ?>" data-status="<?php echo $row->IS_PROCESS ?>" data-fieldId="BATCH_ID" data-field="IS_PROCESS" data-tbl="bn_batchnumber" data-su-url="setup/BatchNumber/batchNumberById/<?php echo $sn ?>">
        <?php echo ($row->IS_PROCESS == 1) ? '<span class="label label-success" title="Click For Inactive">True</span>' : '<span class="label label-danger" title="Click For Active">False</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/batchNumber/edit/' . $row->BATCH_ID); ?>"  title="Edit batch number" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->BATCH_ID; ?>" title="Click For Delete" data-type="delete" data-field="BATCH_ID" data-tbl="bn_batchnumber"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>