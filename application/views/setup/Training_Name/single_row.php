<td><?php echo $sn; ?></td>
<td><?php echo $row->Code ?></td>
<td><?php echo $row->Name ?></td>
<td><?php echo $row->BanglaName ?></td>
<td><?php echo $row->type ?></td>
<td><?php echo $row->level ?></td>
<td>
    <a class="itemStatus" data-sn="<?php echo $sn ?>" id="<?php echo $row->NAVYTrainingID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="NAVYTrainingID" data-field="ACTIVE_STATUS" data-tbl="bn_navytraininghierarchy" data-su-url="setup/TrainingName/trainingNameById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/TrainingName/edit/' . $row->NAVYTrainingID); ?>"  title="Edit Training Type" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->NAVYTrainingID; ?>" title="Click For Delete" data-type="delete" data-field="NAVYTrainingID" data-tbl="bn_navytraininghierarchy"><span class="glyphicon glyphicon-trash"></span></a>
</td>