<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->UNIT_TYPEID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="UNIT_TYPEID" data-field="ACTIVE_STATUS" data-tbl="bn_unittype" data-su-url="setup/unitType/utById/<?php echo $sn ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/unitType/view/' . $row->UNIT_TYPEID); ?>" title="View Unit Type" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/unitType/edit/' . $row->UNIT_TYPEID); ?>"  title="Edit Unit Type" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->UNIT_TYPEID; ?>" title="Click For Delete" data-type="delete" data-field="UNIT_TYPEID" data-tbl="bn_unittype"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>