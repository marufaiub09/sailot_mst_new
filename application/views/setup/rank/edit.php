<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Code <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code', 'value' => $result->RANK_CODE,'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Rank Name <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Rank Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'rank_name', 'value' => $result->RANK_NAME, "class" => "form-control required",'required'=>'required', 'placeholder' => 'Rank name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Rank Name (বাংলা)</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Rank Name(বাংলা)">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'rank_name_bn', 'value' => $result->RANK_BANGLA_NAME, "class" => "form-control",'required'=>'required', 'placeholder' => 'Rank name (বাংলা)')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Branch <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Branch Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="BRANCH_ID" id="BRANCH_ID" data-tags="true" data-placeholder="Select Branch" data-allow-clear="true">
                <option value="">Select Branch</option>
                <?php
                foreach ($branch as $row):
                    ?>
                    <option value="<?php echo $row->BRANCH_ID ?>"<?php echo ($result->BRANCH_ID == $row->BRANCH_ID) ? 'selected' : '' ?>><?php echo $row->BRANCH_NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Equivalent Rank <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Equivalent rank">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="equivalentRank_ID" id="equivalentRank_ID" data-tags="true" data-placeholder="Select Equivalent rank" data-allow-clear="true">
                <option value="">Select Branch</option>
                <?php
                foreach ($equivalent_rank as $row):
                    ?>
                    <option value="<?php echo $row->EQUIVALANT_RANKID ?>"<?php echo ($result->EQUIVALANT_RANKID == $row->EQUIVALANT_RANKID) ? 'selected' : '' ?>><?php echo $row->RANK_NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('SERVICE_LIMIT'); ?> <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Service Limit">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'SERVICE_LIMIT', 'value' => $result->SERVICE_LIMIT, 'required' => 'required', "class" => "form-control caste_name", 'placeholder' => $this->lang->line('SERVICE_LIMIT'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('AGE_LIMIT'); ?> <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Age Limit">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <?php echo form_input(array('name' => 'AGE_LIMIT', 'value' => $result->AGE_LIMIT, 'required' => 'required', "class" => "form-control caste_name", 'placeholder' => $this->lang->line('AGE_LIMIT'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', ($result->ACTIVE_STATUS == 1)? TRUE:FALSE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->RANK_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/rank/updateRank" data-su-action="setup/rank/rankList" data-type="list" value="Update">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>

