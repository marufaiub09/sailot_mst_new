<td><?php echo $sn; ?></td>
<td><?php echo $row->RANK_CODE ?></td>                               
<td><?php echo $row->RANK_NAME ?></td>                               
<td><?php echo $row->EQUIVALANT_RANK ?></td>                               
<td><?php echo $row->BRANCH_NAME ?></td>                               
<td><?php echo $row->AGE_LIMIT ?></td>                               
<td><?php echo $row->SERVICE_LIMIT ?></td>                               
<td>
    <a class="itemStatus" data-sn="<?php echo $sn ?>" id="<?php echo $row->RANK_ID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="RANK_ID" data-field="ACTIVE_STATUS" data-tbl="bn_rank" data-su-url="setup/rank/rankById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/rank/editRank/' . $row->RANK_ID); ?>"  title="Edit rank" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->RANK_ID; ?>" title="Click For Delete" data-type="delete" data-field="RANK_ID" data-tbl="bn_rank"><span class="glyphicon glyphicon-trash"></span></a>    
</td>