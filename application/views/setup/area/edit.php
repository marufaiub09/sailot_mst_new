<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Code <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code','value'=>$result->CODE, 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Area Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'name','value'=>$result->NAME, "class" => "form-control required",'required'=>'required', 'placeholder' => 'Area name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name (বাংলা)</label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Area Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'bn_name','value'=>$result->BANGLA_NAME, "class" => "form-control", 'placeholder' => 'Area bangali name')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Zone <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Pleace Select Zone">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <select class="select2 form-control required" name="ADMIN_ID" id="ADMIN_ID" data-tags="true" data-placeholder="Select Zone" data-allow-clear="true">
                <option value="">Select Zone</option>
                <?php
                foreach ($zone as $row):
                    ?>
                    <option value="<?php echo $row->ADMIN_ID ?>"<?php echo ($result->PARENT_ID == $row->ADMIN_ID) ? 'selected' : '' ?>><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="hidden" name="id" value="<?php echo $result->ADMIN_ID; ?>">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/area/updateArea" data-su-action="setup/area/areaList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>

