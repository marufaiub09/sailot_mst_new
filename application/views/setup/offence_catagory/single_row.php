<td><?php echo $sn; ?></td>
<td><?php echo $row->CODE ?></td>
<td><?php echo $row->NAME ?></td>
<td>
    <a class="itemStatus" id="<?php echo $row->OFFENCE_CATAGORYID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="OFFENCE_CATAGORYID" data-field="ACTIVE_STATUS" data-tbl="bn_offencecatagory" data-su-url="setup/offenceCatagory/ocById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>

<td>
<center>
    <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('setup/offenceCatagory/view/' . $row->OFFENCE_CATAGORYID); ?>" title="View Offence Catagory" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/offenceCatagory/edit/' . $row->OFFENCE_CATAGORYID); ?>"  title="Edit Offence Catagory" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->OFFENCE_CATAGORYID; ?>" title="Click For Delete" data-type="delete" data-field="OFFENCE_CATAGORYID" data-tbl="bn_offencecatagory"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>