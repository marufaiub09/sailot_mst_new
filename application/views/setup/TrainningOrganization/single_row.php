<td><?php echo $sn; ?></td>
<td><?php echo $row->Code ?></td>
<td><?php echo $row->Name ?></td>
<td><?php echo $row->BN_NAME ?></td>

<td>
    <a class="itemStatus" id="<?php echo $row->TrainingOrganizationID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="TrainingOrganizationID" data-field="ACTIVE_STATUS" data-tbl="bn_trainingorganization" data-su-url="setup/TrainningOrganization/examNameById/<?php echo $sn; ?>">
        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
    </a>
</td>
<td>
<center>
    <a class="btn btn-success btn-xs modalLink" data-modal-size="modal-md" href="<?php echo site_url('setup/TrainningOrganization/view/' . $row->TrainingOrganizationID); ?>" title="View Training Organization" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
    <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/TrainningOrganization/edit/' . $row->TrainingOrganizationID); ?>"  title="Edit Training Organization" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
    <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->TrainingOrganizationID; ?>" title="Click For Delete" data-type="delete" data-field="TrainingOrganizationID" data-tbl="bn_trainingorganization"><span class="glyphicon glyphicon-trash"></span></a>
</center>
</td>