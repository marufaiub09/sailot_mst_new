<form class="form-horizontal frmContent" id="dgdpMainForm" method="post">
    <span class="frmMsg"></span>
    
    <div class="form-group">
        <label class="col-sm-3 control-label">Code<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Code">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
           <?php echo form_input(array('name' => 'code', 'type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required'=>'required', 'placeholder' => 'Enter Code')); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">Name<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter Training Organization Name">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-6">
           <?php echo form_input(array('name' => 'name', "class" => "form-control required",'required'=>'required', 'placeholder' => 'Organization name')); ?>
        </div>
    </div>
     <div class="form-group">
        <label class="col-sm-3 control-label">Country<span class="text-danger">* </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Organization Country ">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-3">
            <select class="select2 form-control required" name="country" id="country" data-tags="true" data-placeholder="Select Country" data-allow-clear="true">
                <option value="">Select Country</option>
                <?php
                foreach ($country as $row):
                    ?>
                    <option value="<?php echo $row->COUNTRY_ID ?>"><?php echo $row->NAME ?></option>
                <?php
                endforeach; 
                ?>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label">&nbsp;</label>
        <div class="col-sm-6">
            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="setup/TrainningOrganization/savePart" data-su-action="setup/TrainningOrganization/partList" data-type="list" value="submit">
        </div>
    </div>
</form>
<?php $this->load->view("common/sailors_info"); ?>