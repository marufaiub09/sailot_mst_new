<table id="datatable" class="table table-striped table-bordered " width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Country</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tfoot>
        <tr>
            <th>SN</th>
            <th>Code</th>
            <th>Name</th>
            <th>Country</th>
            <th>Status</th>
            <th>Action</th>
        </tr>
    </tfoot>
    <tbody>
        <?php //echo "<PRE>"; print_r($result); exit;?>
        <?php foreach ($result as $key => $row) { ?>
            <tr id="row_<?php echo $row->TrainingOrganizationID ?>">
                <td><?php echo $key + 1 ?></td>
                <td><?php echo $row->Code ?></td>
                <td><?php echo $row->Name ?></td>
                <td><?php echo $row->BN_NAME ?></td>
                <td>
                    <a class="itemStatus" id="<?php echo $row->TrainingOrganizationID; ?>" data-status="<?php echo $row->ACTIVE_STATUS ?>" data-fieldId="TrainingOrganizationID" data-field="ACTIVE_STATUS" data-tbl="bn_trainingorganization" data-su-url="setup/TrainningOrganization/examNameById/<?php echo $key + 1 ?>">
                        <?php echo ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ?>
                    </a>
                </td>

                <td>
        <center>
            <a class="btn btn-success btn-xs modalLink" data-modal-size="modal-md" href="<?php echo site_url('setup/TrainningOrganization/view/' . $row->TrainingOrganizationID); ?>" title="View Training Organization" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
            <a class="btn btn-warning btn-xs modalLink"  href="<?php echo site_url('setup/TrainningOrganization/edit/' . $row->TrainingOrganizationID); ?>"  title="Edit Training Organization" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
            <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->TrainingOrganizationID; ?>" title="Click For Delete" data-type="delete" data-field="TrainingOrganizationID" data-tbl="bn_trainingorganization"><span class="glyphicon glyphicon-trash"></span></a>
        </center>
    </td>
    </tr>
<?php } ?>
</tbody>
</table>