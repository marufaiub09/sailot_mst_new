<?php echo validation_errors(); ?>
    <!-- form start -->
<?php echo form_open('',"id='createModuleForm' class='form-horizontal update_caste'"); ?>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('RANK_CODE'); ?> <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter your Rank Code.">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-2">
            <?php echo form_input(array('name' => 'RANK_CODE','type'=>'input', 'maxlength' => '8', 'minlength' => '1', "class" => "integerNumbersOnly form-control required", 'required placeholder' => $this->lang->line('RANK_CODE'))); ?>
        </div>
    </div>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('RANK_NAME'); ?> <span class="text-danger"> * </span></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter your Rank Name .">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <?php echo form_input(array('name' => 'RANK_NAME', "class" => "form-control RANK_NAME", 'required placeholder' => $this->lang->line('RANK_NAME'))); ?>
        </div>
    </div>
    <div class="form-group caste_name_input">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('BANGLA_NAME'); ?></label>
        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Enter your Rank Name Bangla .">
            <i class="fa fa-question-circle"></i>
        </a>
        <div class="col-sm-4">
            <?php echo form_input(array('name' => 'BANGLA_NAME', "class" => "form-control BANGLA_NAME", 'placeholder' => $this->lang->line('BANGLA_NAME'))); ?>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
        <div class="col-sm-6">
            <div class="checkbox checkbox-inline checkbox-primary">
                <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
                <label for="ACTIVE_STATUS"></label>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
<?php echo form_close(); ?>
<?php $this->load->view("common/sailors_info"); ?>
