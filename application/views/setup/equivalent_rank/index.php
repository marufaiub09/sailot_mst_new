<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Equivalent Rank Setup</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a type="button" class="modalLink btn btn-primary btn-xs" title="New Equivalent Rank" href="<?php echo site_url("setup/EquivalentRank/create"); ?>">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>
                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th><?php echo $this->lang->line('sl_num'); ?></th>
                        <th><?php echo $this->lang->line('RANK_CODE'); ?></th>
                        <th><?php echo $this->lang->line('RANK_NAME'); ?></th>
                        <th><?php echo $this->lang->line('BANGLA_NAME'); ?></th>
                        <th><?php echo $this->lang->line('action'); ?></th>

                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th><?php echo $this->lang->line('sl_num'); ?></th>
                        <th><?php echo $this->lang->line('RANK_CODE'); ?></th>
                        <th><?php echo $this->lang->line('RANK_NAME'); ?></th>
                        <th><?php echo $this->lang->line('BANGLA_NAME'); ?></th>
                        <th><?php echo $this->lang->line('action'); ?></th>
                    </tr>
                    </tfoot>
                    <tbody>
                    <?php
                    $i = 1;
                    foreach ($EquivalentRank_list as $row) {
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $row->RANK_CODE; ?></td>
                            <td><?php echo $row->RANK_NAME; ?></td>
                            <td><?php echo $row->BANGLA_NAME; ?></td>
                            <td><button type="button" href="<?php echo site_url("setup/EquivalentRank/edit/$row->EQUIVALANT_RANKID"); ?>" title="Edit Equivalent Rank" class="btn btn-xs btn-primary btn-sm modalLink"><?php echo $this->lang->line('edit'); ?></button></td>
                        </tr>
                        <?php
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
