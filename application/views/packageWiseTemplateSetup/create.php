<?php echo validation_errors(); ?>
<?php echo form_open('', 'class="form-horizontal" id="formHorizontal"'); ?>


<div class="form-group">
    <label class="col-sm-3 control-label">Package Name</label>

    <div class="col-sm-3">
        <div class="selectContainer">
            <?php echo form_dropdown('packageName', $package, '', ' id="division office_district" class="form-control" required'); ?>
        </div>
    </div>
</div>

<div class="form-group caste_name_input">
    <label class="col-sm-3 control-label">Template Title</label>
    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Tamplate Title">
        <i class="fa fa-question-circle"></i>
    </a>
    <div class="col-sm-3">
        <?php echo form_input(array('name' => 'templateTitle', "class" => "form-control caste_name", 'required placeholder' => 'Template Name')); ?>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Text Area</label>
    <div class="col-sm-6">
        <textarea class="form-control" name="TextArea" rows="3"></textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label"><?php echo $this->lang->line('is_active'); ?></label>
    <div class="col-sm-6">
        <div class="checkbox checkbox-inline checkbox-primary">
            <?php echo form_checkbox('ACTIVE_STATUS', '1', TRUE, 'class="styled"'); ?>
            <label for="ACTIVE_STATUS"></label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
    <div class="col-sm-offset-2">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
    tinymce.init({
        selector: 'textarea',
        height: 200,
        plugins: [
            'searchreplace visualblocks code fullscreen',
        ],
        toolbar: 'bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        content_css: [
            '//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });
</script>