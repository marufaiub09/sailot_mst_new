<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Package Wise Template Setup</h3>
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-primary btn-xs modalLink"  href="<?php echo site_url('PackageWiseTemplateSetup/create'); ?>" title="Create Package Wise Template">
                            <i class="glyphicon glyphicon-plus"></i>
                        </a>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body">

                <table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th>Package Name</th>
                            <th>Template Title</th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th><?php echo $this->lang->line('sl'); ?></th>
                            <th>Package Name</th>
                            <th>Template Title</th>
                            <th><?php echo $this->lang->line('status'); ?></th>
                            <th><?php echo $this->lang->line('action'); ?></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>Accountant</td>
                            <td>Tokyo</td>
                            <td>2011/07/25</td>
                            <td>
                    <center>
                        <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('packageWiseTemplateSetup/view'); ?>" title="View Package Wise Template" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                        <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('PackageWiseTemplateSetup/edit'); ?>" title="Edit Package Wise Template" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
<!--                            <a class="btn btn-danger btn-xs" data-tooltip="tooltip" data-placement="top" href="<?php echo site_url('setup/district/delete/' . $values->DISTRICT_ID); ?>" title="Delete" type="button" data-user-id="<?php echo $values->DISTRICT_ID ?>"><span class="glyphicon glyphicon-trash"></span></i></a>-->
                    </center>
                    </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>2011/07/25</td>
                        <td>
                    <center>
                        <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('packageWiseTemplateSetup/view'); ?>" title="View Package Wise Template" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                        <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('PackageWiseTemplateSetup/edit'); ?>" title="Edit Package Wise Template" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
<!--                            <a class="btn btn-danger btn-xs" data-tooltip="tooltip" data-placement="top" href="<?php echo site_url('setup/district/delete/' . $values->DISTRICT_ID); ?>" title="Delete" type="button" data-user-id="<?php echo $values->DISTRICT_ID ?>"><span class="glyphicon glyphicon-trash"></span></i></a>-->
                    </center>
                    </td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Accountant</td>
                        <td>Tokyo</td>
                        <td>2011/07/25</td>
                        <td>
                    <center>
                        <a class="btn btn-success btn-xs modalLink" href="<?php echo site_url('packageWiseTemplateSetup/view'); ?>" title="View Package Wise Template" type="button"><span class="glyphicon glyphicon-eye-open"></span></a>
                        <a class="btn btn-warning btn-xs modalLink" href="<?php echo site_url('packageWiseTemplateSetup/edit'); ?>" title="Edit Package Wise Template" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
<!--                            <a class="btn btn-danger btn-xs" data-tooltip="tooltip" data-placement="top" href="<?php echo site_url('setup/district/delete/' . $values->DISTRICT_ID); ?>" title="Delete" type="button" data-user-id="<?php echo $values->DISTRICT_ID ?>"><span class="glyphicon glyphicon-trash"></span></i></a>-->
                    </center>
                    </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

