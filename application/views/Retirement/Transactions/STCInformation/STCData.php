<?php foreach ($STC as $row) { ?>
    <tr id="row_<?php echo $row->STCInformationID ?>">
        <td class="text-center"><?php echo $row->STCNumber ?></td>
        <td class="text-center"><?php echo $row->IssueDate ?></td>
        <td class="text-center"><?php echo $row->AuthorityNumber ?></td>
        <td class="text-center"><?php echo $row->AuthorityDate ?></td> 
        <td>
    <center>
        <a class="btn btn-warning btn-xs modalLink"  data-modal-size="modal-lg" href="<?php echo site_url('Retirement/STCInformation/edit/' . $row->STCInformationID); ?>"  title="Edit STC Information" type="button" ><span class="glyphicon glyphicon-edit"></span></a>
        <a class="btn btn-danger btn-xs deleteItem" id="<?php echo $row->STCInformationID; ?>" title="Click For Delete" data-type="delete" data-field="STCInformationID" data-tbl="stc"><span class="glyphicon glyphicon-trash"></span></a>
    </center>
    </td>
    </tr>
<?php } ?>