<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                   
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title">STC Information</h3>                        
                    </div>  
                </div>
            </div>
            <form class="form-horizontal frmContent unitMainForm" method="post">
                <div class="panel-body">                 
                    <div class="row ">
                        <div class="col-md-12 ">
                            <legend  class="legend">Sailor Information </legend>
                            <fieldset class="">
                                <div class="col-md-5 form-group">                                    
                                        <label class="col-sm-6 control-label">Official Number<span class="text-danger">*</span></label>        
                                        <div class="col-sm-5" >
                                            <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNoSTC', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                            <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Put a Valid Official Number Then Press Tab">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                                                    
                                </div>
                                <div class="col-md-4 form-group">                                   
                                        <label class="col-sm-5 control-label">Full Name</label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                        </div>
                                    
                                </div>
                                <div class="col-md-3 form-group">
                                    
                                        <label class="col-sm-3 control-label">Rank</label>                                            
                                        <div class="col-sm-9">
                                            <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                        </div>                                    
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-md-10 row">
                        <fieldset class="">
                            <legend  class="legend">STC</legend>
                            <table id="paymentTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>STC Number</th>
                                        <th>Issue Date</th>
                                        <th>Authority Number</th>
                                        <th>Authority Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="fiExit" class="contentArea">>
                                    <tr>          
                                        <td>
                                            <input type="text" name="stcNumber[]" id="stcNu"  class="form-control required" placeholder="STC Number" >
                                        </td>
                                        <td>
                                            <?php echo form_input(array('name' => 'issueDate[]', "id" => "issueDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>
                                        </td>
                                        <td>
                                            <input type="text" name="AuthorityNu[]" id="AuthNu"  class="form-control required" placeholder="Authority Number" >
                                        </td>                            
                                        <td>
                                            <?php echo form_input(array('name' => 'authorityDate[]', "id" => "authorityDate", "class" => "datePicker form-control", "value" => date("d-m-Y"))); ?>                                
                                        </td>
                                        <td class="text-center">                       
                                        </td>
                                    </tr>
                                </tbody>
                            </table> 
                        </fieldset>
                    </div>
                </div>
                <div class="box-header col-md-10">
                    <span class="btn btn-xs btn-success pull-right" id="add_record">
                        <i style="cursor:pointer; margin-right: 10px" class="fa fa-plus"> Add More</i>
                    </span>
                    <a href="<?php echo site_url(''); ?>" target="_blank">
                        <button style="float: right; margin-right: 10px" class="btn btn-primary btn-xs" >
                            <img src="<?php echo base_url() . 'dist/img/print-pdf.png'; ?>" width="20px" height="20px" alt="pdf">
                            Print
                        </button>
                    </a>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">&nbsp;</label>
                    <div class="col-sm-6">
                        <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/examTestInfo/save" data-redirect-action="regularTransaction/examTestInfo/index" value="Update">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 80%;
    }
</style>
<?php $this->load->view("common/sailors_info"); ?>
<script>   
    
    $("#officialNoSTC").on('blur', function(){
        var officeNumber = $(this).val();
        if(officeNumber != ''){
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus:3},
                dataType: "json",
                url: "<?php echo site_url(); ?>setup/common/searchSailor",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if(data != null){
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);  
                        $(".alertSMS").html('');
                        
                        var sailor_id = data['SAILORID'];
                         $.ajax({
                            type: "post",
                            url: '<?php echo site_url('Retirement/STCInformation/ifExistData') ?>',
                            data: {sailor_id: sailor_id},
                            beforeSend: function () {
                                $(".contentArea").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                            },
                            success: function (data) {
                                $("#fiExit").html(data);
                            }
                        });                 
                    }else{
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');      
                        $(".alertSMS").html('Invalid Officer Number');      
                    }
                }
            });            
        }else{
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val(''); 
            $(".alertSMS").html(''); 
        }
    });
    
    
    
    // append academic info table
    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#paymentTable tbody").append(' <tr>' +
                '<td>' +
                ' <input type="text" name="stcNumber[]" id="stcNu' + counter + '"  class="form-control required" placeholder="STC Number" >' +
                '</td>' +                
                '<td>' +
                ' <input type="text" name="issueDate[]" value = "<?php echo date('d-m-Y') ?>" id="issueDate' + counter + '"  class="form-control " placeholder="Date" >' +
                '</td>' + 
                '<td>' +
                ' <input type="text" name="AuthorityNumber[]" id="AuthorityNu' + counter + '"  class="form-control " placeholder="Authority Number" >' +
                '</td>' +
                '<td>' +
                
                
                ' <input type="date" name="AuthorityDate[]" value = "<?php echo date('d-m-Y')?>" id="AuthorityDate' + counter + '"  class="datePicker form-control " placeholder="Authority Date" >' +
                '</td>' +
                
                
                
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
        $("#issueDate"+counter).datepicker();
        $("#AuthorityDate"+counter).datepicker();
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    //    $('#paymentTable').removeAttr('width').DataTable({
    //        "scrollX": true,
    //        "scrollX": true,
    //                "bPaginate": false,
    //        "bFilter": false,
    //        "bInfo": false,
    //        "columnDefs": [
    //            {width: '100px', targets: 0},
    //            {width: '100px', targets: 1},
    //            {width: '100px', targets: 2},
    //            {width: '80px', targets: 3},
    //            {width: '80px', targets: 5},
    //            {width: '90px', targets: 6},
    //            {width: '70px', targets: 7},
    //            {width: '90px', targets: 8},
    //            {width: '90px', targets: 9},
    //        ],
    //        "fixedColumns": true
    //
    //    });
</script>