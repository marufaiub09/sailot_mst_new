<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                   
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title">Sailor Death Information</h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">            
                <form class="form-horizontal frmContent unitMainForm" method="post">
                    <div class="row ">
                        <div class="col-md-11">
                            <legend  class="legend">Sailor Information </legend>
                            <fieldset class="">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-sm-5">Official Number<span class="text-danger">*</span></label>        
                                        <div class="col-sm-6" >
                                            <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNoDeathInfo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                            <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Put a Valid Official Number Then Press Tab">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-sm-4">Full Name</label>
                                        <div class="col-sm-8">
                                            <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName ", 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="col-sm-3">Rank</label>                                            
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name' => 'rank', "class" => "form-control rank ", 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <legend  class="legend"></legend>
                    <fieldset class="">
                        <div class="col-md-12 form-group">

                            <label class="col-sm-2">Death Date<span class="text-danger">*</span></label>
                            <div class="col-sm-2" >                            
                                <?php echo form_input(array('name' => 'deathDate', "class" => "datePicker DeathDate form-control required", 'required' => 'required', 'placeholder' => 'Death Date')); ?>
                            </div>
                            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Death date">
                                <i class="fa fa-question-circle"></i>
                            </a>

                        </div>
                        <div class="col-md-12 form-group">

                            <label class="col-sm-2">Death Cause<span class="text-danger">*</span></label>
                            <div class="col-sm-6" >                            
                                <?php echo form_textarea(array('name' => 'deathCause', "class" => "form-control DeathCause required ", 'required' => 'required', 'placeholder' => 'Death Cause', 'rows' => '1')); ?>
                            </div>
                            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Death Cause">
                                <i class="fa fa-question-circle"></i>
                            </a>

                        </div>
                        <div class="col-md-12 form-group">

                            <label class="col-sm-2">Authority Number<span class="text-danger">*</span></label>
                            <div class="col-sm-3" >                            
                                <?php echo form_input(array('name' => 'authorityNu', 'id' => 'authorityNumber', "class" => "form-control DeathAuthorityNumber required", 'required' => 'required', 'placeholder' => 'Authority Number')); ?>           

                            </div>
                            <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Death Cause">
                                <i class="fa fa-question-circle"></i>
                            </a>

                        </div>
                        <div class="col-md-12 form-group">
                            <div>
                                <label class="col-sm-2">Authority Date<span class="text-danger">*</span></label>
                                <div class="col-sm-2" >                            
                                    <?php echo form_input(array('name' => 'authority', "class" => "datePicker form-control DeathAuthorityDate required", 'required' => 'required', 'placeholder' => 'authority Date')); ?>
                                </div>
                                <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Authority date">
                                    <i class="fa fa-question-circle"></i>
                                </a>
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                            <input type="button" class="btn btn-primary btn-sm formSubmit" data-action="Retirement/Transactions/deathInfo/save" data-su-action=" " data-type="list"  value="submit">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 80%;
    }
</style>
<?php $this->load->view("common/sailors_info"); ?>

<script type="text/javascript">
    $("#officialNoDeathInfo").on('blur', function () {
        var officeNumber = $(this).val();
        if (officeNumber != '') {
            $.ajax({
                type: "post",
                data: {officeNumber: officeNumber, sailorStatus: 3},
                dataType: "json",
                url: "<?php echo site_url(); ?>Retirement/Transactions/deathInfo/searchSailor",
                beforeSend: function () {
                    $(".smloadingImg").html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    $(".smloadingImg").html("");
                    if (data != null) {
                        $(".sailorId").val(data['SAILORID']);
                        $(".fullName").val(data['FULLNAME']);
                        $(".rank").val(data['RANK_NAME']);
                        $(".DeathDate").val(data['DeathDate']);
                        $(".DeathCause").val(data['DeathCause']);
                        $(".DeathAuthorityNumber").val(data['DeathAuthorityNumber']);
                        $(".DeathAuthorityDate").val(data['DeathAuthorityDate']);
                        $(".alertSMS").html('');
                    } else {
                        $(".sailorId").val('');
                        $(".fullName").val('');
                        $(".rank").val('');
                        $(".DeathDate").val('');
                        $(".DeathCause").val('');
                        $(".DeathAuthorityNumber").val('');
                        $(".DeathAuthorityDate").val('');
                        $(".alertSMS").html('Invalid Officer Number');
                    }
                }
            });
        } else {
            $(".sailorId").val('');
            $(".fullName").val('');
            $(".rank").val('');
            $(".DeathDate").val('');
            $(".DeathCause").val('');
            $(".DeathAuthorityNumber").val('');
            $(".DeathAuthorityDate").val('');
            $(".alertSMS").html('');
        }
    });
</script>
