<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('regularTransaction/reEngagementInfo/index'); ?>" title="Re-Engagement Information">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Sailor Pension Information</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>    
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">Sailor Information</legend>   
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Official Number</label>        
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'officialNo', 'id' => 'preEngagementNo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                        <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                    </div>
                                    <div class="col-sm-1">
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="col-md-12" >
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3 ">Details</label>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend">General Information </legend>
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 ">Release Cause <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Release Cause">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'realeaseCause', "id"=>"realeaseCause", "class" => "form-control required")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority No <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Authority No">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityNo', "id"=>"authorityNo",  "class" => " form-control required", "placeholder" => "Engagement date")); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityDate', 'value' =>  date("d-m-Y"), "id"=>"authorityDate", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Approve Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Approve date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'approveDAte', 'value' =>  date("d-m-Y"), "id"=>"approveDAte", "class" => "datePicker form-control required"/*,'readonly' => 'readonly'*/)); ?>
                                    </div>
                                </div>                            
                                
                            </div>
                            <div class="col-md-6">                                                           
                                
                                <div class="form-group">
                                    <label class="col-sm-4 ">LPR Taken <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter LPR Taken">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'lprToken', "id"=>"lprToken", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">LPR Availed Days <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter LPR Availed Day">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'availedDays', "id"=>"availedDays", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Accumuted Leave Days <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Accumuted Leave Days">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'leaveDays', "id"=>"leaveDays", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                                
                            </div>                            
                        </fieldset>
                    </div>   
                    <div class="col-md-12">
                        <fieldset class="">
                            <legend  class="legend"></legend>
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px;">Permanent Address<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Permanent Address">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddress', "class" => "form-control", 'placeholder' => 'Permanent Address', 'cols' => '5', 'rows' => '2')); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Present Address <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Present Address">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddress', "class" => "form-control", 'placeholder' => 'Present Address', 'cols' => '5', 'rows' => '2')); ?>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Bank Address <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Bank Address">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddress', "class" => "form-control", 'placeholder' => 'Bank Address', 'cols' => '5', 'rows' => '2')); ?>
                                    </div>
                                </div>                         
                                
                            </div>
                            <div class="col-md-6"> 
                                <div class="col-md-6">
                                    <img style="width:120px; border:1px solid #f9f9f9; margin-top: 24px;margin-bottom: 20px;margin-left: 5%;" id="stdPhoto" class="" src="<?php echo base_url(); ?>src/img/default.png" alt="" />
                                    <br/>
                                    <span>Upload Photo <span class="text-danger">*</span></span>
                                            <input id="STD_PHOTO" type="file" accept="jpg,jpeg,png,gif" name="STD_PHOTO" value="" onchange="StudentImg(this);" class="upload col-sm-12 " />
                                </div>
                                <div class="col-md-6">
                                    <img style="width:120px; border:1px solid #f9f9f9; margin-top: 24px;margin-bottom: 20px;margin-left: 5%;" id="stdPhoto" class="" src="<?php echo base_url(); ?>src/img/default.png" alt="" />
                                    <br/>
                                     <span>Upload Photo <span class="text-danger">*</span></span>
                                            <input id="STD_PHOTO" type="file" accept="jpg,jpeg,png,gif" name="STD_PHOTO" value="" onchange="StudentImg(this);" class="upload col-sm-12 " />
                                </div>

                            </div>                            
                        </fieldset>
                    </div>   
                    <fieldset class="">
                        <legend  class="legend"><input type="checkbox" name="pReceived"> Pension Received</legend>
                        <div class="col-md-12">
                            <fieldset class="">
                                <legend  class="legend" style="font-size: 12px;"> Pension info </legend>
                                <div class="col-md-6">  
                                    <div class="form-group">
                                        <label class="col-sm-4 " style="padding-right: 0px;">Nautre of Pension</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Nautre of Pension">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name' => 'engagementNoNext', "id"=>"engagementNoNext", "class" => "form-control required")); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-4" style="padding-right: 0px;">Cumulation Amount</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Cumulation Amount">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'cumulationAmount', "id"=>"cumulationAmount",  "class" => " form-control required", "placeholder" => "0.00")); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-4 " style="padding-left: 7px;padding-right: 0px;">Cumulation Percentage</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Cumulation Percentage">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'cumulationPercentage', "id"=>"cumulationPercentage", "class" => "form-control required", "placeholder" => "0.00")); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 " style="padding-right: 0px;">Monthly Pension Amount</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Monthly Pension Amount">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'mPendionAmount', "id"=>"mPendionAmount", "class" => "form-control required", "placeholder" => "0.00")); ?>
                                        </div>
                                    </div>                            
                                    <div class="form-group">
                                        <label class="col-sm-4 " style="padding-right: 0px;">Calculated Amount</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Calculated Amount">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'calculateAmount', "id"=>"calculateAmount", "class" => "form-control required", "placeholder" => "0.00")); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">                                                           
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Festival Allowance</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Festival Allowance">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'festivalAllowance', "id"=>"festivalAllowance", "class" => "form-control required", "placeholder" => "0.00")); ?>
                                        </div>
                                    </div>                           
                                    
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Medical Allowance</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Medical Allowance">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'medicalAllowance', "id"=>"medicalAllowance", "class" => "form-control required", "placeholder" => "0.00")); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Ration Allowance</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter Ration Allowance">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'rationAllowance', "id"=>"rationAllowance", "class" => "form-control required", "placeholder" => "0.00")); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Authority Number</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authority number">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'authorityNumber', "id"=>"authorityNumber", "class" => "form-control required")); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 ">Authorized Date</label>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please enter authorized Date">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name' => 'authorityDate', "id"=>"authorityDate", "value" => date("d-m-Y"),  "class" => "datePicker form-control required")); ?>
                                        </div>
                                    </div>
                                </div>                            
                            </fieldset>
                        </div>
                        <legend  class="legend" style="font-size: 12px;"> Dependants</legend>
                        <div class="col-md-12">
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px;">Name</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Write Dependants Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'depName', "id"=>"depName", "class" => "form-control required", 'placeholder' => 'Name')); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px;">Date of Birth</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Date of Birth">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'dateOfBirth', "id"=>"dateOfBirth", "class" => "datePicker form-control", 'placeholder' => 'date of birth')); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">  
                                <div class="form-group">
                                    <label class="col-sm-4 " style="padding-right: 0px;">Relation</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Relation">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'relation', "id"=>"relation", "class" => "form-control required")); ?>
                                    </div>
                                </div>
                            </div>

                        </div> 
                    </fieldset> 
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/reEngagementInfo/save" data-redirect-action="regularTransaction/reEngagementInfo/index" value="submit">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    function StudentImg(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#stdPhoto')
                    .attr('src', e.target.result)
                    .width(120);
            };

            reader.readAsDataURL(input.files[0]);
            /*upload photo specifit directory*/
            var input = document.getElementById("STD_PHOTO");
            file = input.files[0];
            if (file != undefined) {
                formData = new FormData();
                if (!!file.type.match(/image.*/)) {
                    formData.append("image", file);
                    $.ajax({
                        url: "<?php echo site_url(); ?>/portal/studentImagUpload",
                        type: "POST",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {
                            $("#STD_PHOTO").text(data).val();
                        }
                    });
                } else {
                    alert('Not a valid image!');
                }
            } else {
                alert('Input something!');
            }
        }
    }
</script>