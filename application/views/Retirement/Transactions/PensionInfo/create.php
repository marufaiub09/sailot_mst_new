<div class="row">
    <form class="form-horizontal frmContent" id="MainForm" method="post">
        <div class="col-md-12">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/index'); ?>" title="List Sailor informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title"><center>Add Retired Personal</center></h3>
                    </div>  
                </div>
            </div>
            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active"><a href="form-layouts-tabbed.html#tab-first" role="tab" data-toggle="tab">General Info & Engagement Info</a></li>
                    <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Permanent Address and Next of Kin Info</a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="tab-first">
                        <div class="form-group col-md-12">                            
                            <div>
                                <label class="col-md-2 control-label">Official Number</label>
                                <div class="col-md-3">
                                    <?php echo form_input(array('name' => 'officialNumber', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Select Batch Number ')); ?>
                                </div>
                                <label class="col-md-2 control-label">Local Number</label>
                                <div class="col-md-3">
                                    <?php echo form_dropdown('localNumberDis', $localNumber, '', 'id="localNumber" required="required" class="form-control select2"'); ?>
                                </div>
                                <div class="col-md-2">
                                    <?php echo form_input(array('name' => 'localNumber', 'id' => 'localNumberReplace', "class" => "form-control localNumber", 'value' => set_value('localShortName'))); ?>
                                </div>
                            </div>
                            <hr style="height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">
                        </div>
                        <div class="form-group col-md-12">                            
                            <div>
                                <label class="col-md-2 control-label">Batch Number</label>
                                <div class="col-md-3">
                                    <?php echo form_dropdown('batch', $batch, '', 'id="batchid" required="required" class="form-control select2" data-placeholder="Select Branch Number" aria-hidden="true" data-allow-clear="true"'); ?>
                                </div>
                                <label class="col-md-2 control-label">P No</label>
                                <div class="col-md-5">
                                    <?php echo form_input(array('name' => 'poNo', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Select P No ')); ?>
                                </div>
                            </div>
                            <hr style="height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">
                        </div> 




                        <fieldset class="">
                            <legend  class="legend">General Information</legend>                        
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Rank</label>
                                        <div class="col-md-8">
                                            <select class="select2 form-control required" name="RankName" id="RankId" data-placeholder="Select Rank" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Rank</option>
                                                <?php
                                                foreach ($rankData as $row):
                                                    ?>                                           
                                                    <option value="<?php echo $row->RANK_ID . "_" . $row->BRANCH_ID . "_" . $row->EQUIVALANT_RANKID; ?>"><?php echo "[" . $row->RANK_CODE . "] " . $row->RANK_NAME ?></option>
                                                    <?php
                                                endforeach;
                                                ?>
                                            </select>
                                        </div>                                        
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Birth Date</label>
                                        <div class="col-sm-5 date" >
                                            <?php echo form_input(array('name' => 'birthDate', "class" => "datePicker form-control", 'value' => date('d-m-Y'), 'placeholder' => 'Birth Date')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Full Name</label>
                                        <div class="col-md-8">
                                            <?php echo form_input(array('name' => 'fullName', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Full Name')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">Full Name in Bangla</label>
                                        <div class="col-md-6">
                                            <?php echo form_input(array('name' => 'fullNameInBn', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Name In Bangla')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Short Name</label>
                                        <div class="col-md-8">
                                            <?php echo form_input(array('name' => 'shortName', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Short Name')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">Short Name in Bangla</label>
                                        <div class="col-md-6">
                                            <?php echo form_input(array('name' => 'shortNameBangla', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Short Name in Bangla ')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                        <label class="col-md-3 control-label">Nick Name</label>
                                        <div class="col-md-8">
                                            <?php echo form_input(array('name' => 'nickName', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Nick Name')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">                                        
                                        <label class="col-md-5 control-label">Nick Name in Bangla</label>
                                        <div class="col-md-6">
                                            <?php echo form_input(array('name' => 'nickNameInBangla', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Nick Name In Bangla')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" style="padding-left: 0 !important; padding-right: 0 !important  ;">Father's Name</label>
                                        <div class="col-md-8">
                                            <?php echo form_input(array('name' => 'fatherName', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Fathers Name')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">Father's Name in Bangla</label>
                                        <div class="col-md-6">
                                            <?php echo form_input(array('name' => 'fatherNameInBangls', 'type' => 'text', "class" => "form-control ", 'placeholder' => "Father's Name in Bangla")); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" style="padding-left: 0 !important; padding-right: 0 !important  ;">Mother's Name</label>
                                        <div class="col-md-8">
                                            <?php echo form_input(array('name' => 'motherName', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Mothers Name')); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-5 control-label">Mother's Name in Bangla</label>
                                        <div class="col-md-6">
                                            <?php echo form_input(array('name' => 'motherNameInBangla', 'type' => 'text', "class" => "form-control ", 'placeholder' => "Mother's Name in Bangla")); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-3  control-label" style="padding-left: 0 !important; padding-right: 0 !important  ;">Marritial Status</label>
                                        <div class="col-md-6">
                                            <select name="mariatialStatus" class="form-control required select2" data-placeholder="Select Status" aria-hidden="true" data-allow-clear="true" style="width: 100%;">
                                                <option value="0">Unmarried</option>
                                                <option value="1">Married</option>
                                                <option value="2">Separation</option>
                                                <option value="3">Divorce</option>
                                                <option value="4">Widower</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" style="padding-left: 0 !important; padding-right: 0 !important  ;">Religion</label>
                                        <div class="col-md-6">
                                            <select name="religion" class="form-control required select2" data-placeholder="Select Status" aria-hidden="true" data-allow-clear="true" style="width: 100%;">
                                                <option value="1">Islam</option>
                                                <option value="2">Hindu</option>
                                                <option value="3">Buddhist</option>
                                                <option value="4">Christian</option>
                                                <option value="5">Other's</option>                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="">
                                        <legend  class="legend" style="font-size: 12px;  margin-bottom: 10px;">Freedom fighter status</legend>
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label class="check"><input type="radio" class="iradio" name="fredomfighterStatus" checked="checked" value="1" />None</label>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="check"><input type="radio" class="iradio" name="fredomfighterStatus" value="2" /> Freedom Fighter</label>
                                            </div>
                                            <div class="col-md-5">
                                                <label class="check"><input type="radio" class="iradio" name="fredomfighterStatus" name="iradio" value="3" /> Freedom Fighter Child</label>
                                            </div>
                                        </div>                                   
                                    </fieldset>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" style="padding-left: 0 !important; padding-right: 0 !important  ;">Sex</label>
                                        <div class="col-md-6">
                                            <select name="sex" class="form-control required select2" data-placeholder="Select Status" aria-hidden="true" data-allow-clear="true" style="width: 100%;">
                                                <option value="">Select Sex</option>
                                                <option value="1">Male</option>
                                                <option value="2">Female</option>                                          
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="">
                            <legend  class="legend" >Engagement Information</legend>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Entry Date</label>
                                        <div class="col-sm-4 date" >
                                            <?php echo form_input(array('name' => 'entryDate', "class" => "datePicker form-control", 'value' => date('d-m-Y'), 'placeholder' => 'Entry Date')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Entry Type</label>
                                        <div class="col-md-5">
                                            <?php echo form_dropdown('entryType', $entryType, '', 'id="batchid" required="required" class="form-control select2" style="width: 150px;" '); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <fieldset class="">
                                        <legend  class="legend" style="font-size: 12px;  margin-bottom: 1px;">Engagement Period</legend>
                                        <div class="form-group" style="margin-bottom:1px">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-2"> &nbsp; Year </div>
                                            <div class="col-sm-2"> Month </div>
                                            <div class="col-sm-2"> &nbsp; Day </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Period</label>
                                            <div class="col-sm-2">
                                                <?php echo form_input(array('name' => 'priodYY', "id" => "priodYY", "class" => "form-control required", "value" => "00", "maxlength" => "2")); ?>
                                            </div>
                                            <div class="col-sm-2">
                                                <?php echo form_input(array('name' => 'priodMM', "id" => "priodMM", "class" => "form-control required", "value" => "00", "maxlength" => "2")); ?>
                                            </div>
                                            <div class="col-sm-2">
                                                <?php echo form_input(array('name' => 'priodDD', "id" => "priodDD", "class" => "form-control required", "value" => "00", "maxlength" => "2")); ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Expire Date</label>
                                            <div class="col-sm-6" >
                                                <?php echo form_input(array('name' => 'expireDate', "class" => "form-control", 'value' => '00-00-0000', 'placeholder' => 'Expire Date')); ?>
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>

                            </div>    
                        </fieldset>

                        <div class="col-md-12">
                            <fieldset class="">
                                <legend  class="legend" style="font-size: 12px;  margin-bottom: 10px;">Posting Unit</legend>
                                <div class="col-md-6">                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Zone</label>
                                        <div class="col-sm-6" >
                                            <?php echo form_dropdown("zone", $zone, set_value("zone"), "class='form-control select2' style='width: 120px;' id='zone'"); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Area</label>
                                        <div class="col-sm-6" >
                                            <select  id="area" name="area" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                    </div>                                

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Shift/Establishment</label>
                                        <div class="col-sm-6" >
                                            <select  id="shift" name="shiftEstablishment" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                    </div>                                

                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Posting Unit</label>
                                        <div class="col-sm-6" >
                                            <select  id="postingUnit" name="postingUnit" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                    </div>                                
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Posting Date</label>
                                        <div class="col-sm-4 date" >
                                            <?php echo form_input(array('name' => 'postingDate', "class" => "datePicker form-control", 'value' => date('d-m-Y'), 'placeholder' => 'Posting Date')); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Posting Type</label>
                                        <div class="col-md-6">
                                            <select name="postingType" class="form-control select2" data-placeholder="Posting Type" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Posting Type</option>
                                                <option value="1">Normal</option>
                                                <option value="2">Instructor</option>
                                                <option value="3">Course</option>
                                                <option value="4">School Staff</option>
                                                <option value="5">Release</option>
                                                <option value="6">Deputation</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Sailor Status</label>
                                        <div class="col-md-6">
                                            <select name="sailorStatus" class="form-control select2" data-placeholder="Sailor Status" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select Status</option>
                                                <option value="0">Release</option>
                                                <option value="2">Books of Depot</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>                                
                            </fieldset>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-second">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <legend  class="legend">Permanent Address:</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Division</label>
                                        <div class="col-sm-6">
                                            <?php echo form_dropdown("division", $division, set_value("division"), "class='form-control' id='division_id'"); ?>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please sleect Division name">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Select District </label>
                                        <div class="col-sm-6" >
                                            <select  id="district_id" name="districtName" class="form-control">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select District Name">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Select Police Station</label>
                                        <div class="col-sm-6" >
                                            <select  id="thana_id" name="policeStation" class="form-control">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Police Station/Thana Name">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>                                        
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Permanent Address</label>
                                        <div class="col-sm-6">
                                            <?php echo form_textarea(array('name' => 'permanentAddress', "class" => "form-control", 'placeholder' => 'Permanent Address', 'cols' => '12', 'rows' => '2')); ?>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6">                               
                                <legend class="legend">Next of kin Information:</legend>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Next of kin</label>
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'nextOfKinName', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Next of kin name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Training Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Next of Kin Name In Banbla </label>
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'nextOfKinNameInBan', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Next of kin name In Bangla')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select training name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Relation of next kin</label>
                                    <div class="col-sm-6" >
                                        <?php echo form_dropdown('relationOfNextKin', $relation, '', 'id="relation" required="required" class="form-control select2" style="width: 200px;" '); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select District Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Permanent Address In Bangla</label>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddressInBan', "class" => "form-control", 'placeholder' => 'Permanent Address In Bangla', 'cols' => '12', 'rows' => '2')); ?>
                                    </div>
                                </div>
                                </fieldset>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <legend  class="legend">*</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label"></label>
                                        <div class="col-sm-7 checkbox checkbox-danger">
                                            <input  id="checkbox" name= "ifNextOfKinSame"  type="checkbox" class="styled " checked="checked" onchange="toggleStatus()" >
                                            <label for="checkbox6" >
                                                Police Station(if different)
                                            </label>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Unchecked if different">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-md-6" id="check">
                                <fieldset class="form-group">
                                    <legend  class="legend">Next of kin address:</legend>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Division</label>
                                        <div class="col-sm-6" >
                                            <?php echo form_dropdown("divisionOfNextKin", $division, set_value("division"), "class='form-control' id='division'"); ?>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please sleect Division name">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Select District </label>
                                        <div class="col-sm-6" >
                                            <select  id="district" name="districtNameOfKin" class="form-control">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select District Name">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Select Police Station</label>
                                        <div class="col-sm-6" >
                                            <select  id="thana" name="thanaNameOnKin" class="form-control">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Police Station/Thana Name">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-sm-11 form-group"> 
                                <input type="button" class="btn btn-primary btn-sm pull-right" id="btnSameAsPermanent" value="Same as Permanent">
                            </div>
                            <div class="col-md-12">
                                <div class="form-group col-md-6">
                                    <label class="col-sm-4 control-label">Next of kin address</label>
                                    <div class="col-sm-8">
                                        <?php echo form_textarea(array('name' => 'nextOfKinAddress', "class" => "form-control", 'placeholder' => 'Next of kin address', 'id' => 'nextOfkinAdd', 'cols' => '12', 'rows' => '2')); ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="col-sm-4 control-label">Next of kin address in Bangla</label>
                                    <div class="col-sm-8">
                                        <?php echo form_textarea(array('name' => 'nextOfKinAddressInBangla', "class" => "form-control", 'placeholder' => 'Next of kin address in Bangla', 'id' => 'nextOfKinInBangla', 'cols' => '12', 'rows' => '2')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                        <div class="col-md-2">
                            <label class="col-sm-3 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="button" class="btn btn-primary btn-sm pull-right formSubmitWithRedirect" data-action="/SpecialTransection/CapturingNewRecrutiInfo/save" data-su-action="/SpecialTransection/CapturingNewRecrutiInfo/save" data-type="list" value="submit">
                            </div>
                        </div>
                       
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#division_id').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('setup/common/ajax_get_division'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#district_id').html(data);
                }
            });
        });
        $('#district_id').change(function () {
            var selectedValue = $(this).val();
            var url = '<?php echo site_url('setup/common/ajax_get_district') ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#thana_id').html(data);
                }
            });
        });

        $('#division').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('setup/common/ajax_get_division'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#district').html(data);
                }
            });
        });
        $('#district').change(function () {
            var selectedValue = $(this).val();
            var url = '<?php echo site_url('setup/common/ajax_get_district'); ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#thana').html(data);
                }
            });
        });
        $('#zone').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/ajax_get_area'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#area').html(data);
                }
            });
        });

        $('#area').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/ajax_get_shift'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shift').html(data);
                }
            });
        });
        $('#shift').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/ajax_get_postingUnit'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#postingUnit').html(data);
                }
            });
        });
    });
    $("#division").prop("disabled", true);
    $("#district").prop("disabled", true);
    $("#thana").prop("disabled", true);
    $(document).ready(function () {
        handleStatusChanged();
    });
    function handleStatusChanged() {
        $('#checkbox').on('change', function () {
            toggleStatus();
        });
    }
    function toggleStatus() {
        if ($('#checkbox').is(':checked')) {
            $('#check :input').attr('disabled', true);
        } else {
            $('#check :input').removeAttr('disabled');
        }
    }
    
    $(document).on("click", "#btnSameAsPermanent", function () {
        var selectDistVal = $("#district_id :selected").text().trim();
        var selectThanaVal = $("#thana_id :selected").text();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/ajax_get_NextOfKinAddress'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfkinAdd').val(data);
            }
        });
    });

        $(document).on("click", "#btnSameAsPermanent", function () {
        var selectDistVal = $("#district_id").val();
        var selectThanaVal = $("#thana_id").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/NextOfKinAddressInBangla'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfKinInBangla').val(data);
            }
        });
    });


    $('#thana').change(function () {
        var selectDistVal = $("#district :selected").text().trim();
        var selectThanaVal = $("#thana :selected").text().trim();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/ajax_get_NextOfKinAddressNotSame'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfkinAdd').val(data);
            }
        });
    });

    $('#thana').change(function () {
        var selectDistVal = $("#district").val();
        var selectThanaVal = $("#thana").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('RetirementInformations/Transactions/CapturingRetiredProsonnel/ajax_get_NextOfKinAddressInBangla'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfKinInBangla').val(data);
            }
        });
    });
</script>