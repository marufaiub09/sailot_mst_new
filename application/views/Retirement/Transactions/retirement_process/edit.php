<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                    
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title"><center>Retirement Edit</center></h3>                        
                    </div>  
                </div>
            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent" id="MainForm" method="post">
                    <span class="frmMsg"></span>
                    <?php //echo "<pre>"; print_r($retirement); exit(); ?>
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 ">Official Number <span class="text-danger">*</span></label>        
                                <div class="col-sm-5" >
                                    <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNumber', "class" => "form-control required", 'required' => 'required', 'readonly' => 'readonly',  'placeholder' => 'Official Number', 'value' => $retirement->OFFICIALNUMBER)); ?>           
                                    <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                </div>
                                <div class="col-sm-1">
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Official Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="col-md-12" >
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8 danger">
                                        <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-4 ">Details</label>
                                <div class="col-sm-4">
                                    <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => $retirement->FULLNAME)); ?>
                                </div>
                                <div class="col-sm-4">
                                    <?php echo form_input(array('name' => 'rank', "class" => "form-control rank required", 'required' => 'required', 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => $retirement->RANK_NAME)); ?>
                                </div>
                            </div>                                
                        </div>
                    </div>
                    <div class="col-md-12">
                        <fieldset class="">                            
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4 ">Release Type<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Release Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select name="releaseType" id="releaseType"class="form-control required select2" data-placeholder="Select Release Type" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Release Type</option>
                                            <option value="1" <?php echo ($retirement->SubType == 1)? "selected" : "" ?>>Release</option>
                                            <option value="2" <?php echo ($retirement->SubType == 2)? "selected" : "" ?>>Discharge</option>
                                            <option value="3" <?php echo ($retirement->SubType == 3)? "selected" : "" ?>>Dismissed</option>
                                        </select>                                      
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Release Cause<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please select Release Cause">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <select class="select2 form-control required" name="releaseCause" id="releaseCause"  data-placeholder="Select Cause" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Cause</option>   
                                            <?php foreach ($causetype as $key => $row) {?> 
                                                <?php if($row->SubType == $retirement->SubType) : ?>
                                                <option value="<?php echo $row->CauseTypeID; ?>" <?php echo ($row->CauseTypeID == $retirement->CauseTypeID)? "selected":""?>><?php echo $row->Name; ?></option>  
                                                <?php endif; ?> 
                                            <?php }?>
                                        </select>                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority Number<span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please input Authority Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-6">
                                        <?php echo form_input(array('name' => 'authorityNumber','value' => $retirement->AuthorityNumber, "id" => "authorityNumber", "class" => "form-control required")); ?>                              
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Authority Date <span class="text-danger">*</span></label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Authority date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'authorityDate', 'value' => date("d-m-Y", strtotime($retirement->AuthorityDate)), "id" => "authorityDate", "class" => "datePicker form-control required"/* ,'readonly' => 'readonly' */)); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">DAO Number <span class="text-danger">*</span></label>
                                    <div class="col-sm-5" >
                                        <select class="select2 form-control required"  name="DAO_NO" id="DAO_NO" data-placeholder="Select DAO NO" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select DAO</option>
                                            <?php
                                            foreach ($dao as $row):
                                                ?>
                                                <option value="<?php echo $row->DAO_ID ?>" <?php echo ($row->DAO_ID == $retirement->DAOID)? "selected": "" ?>><?php echo $row->DAO_NO ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="left" data-content="Please Select DAO Number">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div> 
                                <div class="form-group">
                                    <label class="col-sm-4 ">Release Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please enter Release date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'releaseDate', 'value' => date("d-m-Y", strtotime($retirement->ApproveDate)), "id" => "releaseDate", "class" => "datePicker form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">LPR Availed Days</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="LPR DAYS">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <input id="lrpAvailedDays" name="lrpAvailed" class="form-control"  type="text" value="<?php $retirement->LPRDays ?>" placeholder="LRP Availed Days"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Actual Release Date</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Actual Release Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'actreleaseDate', 'value' => date("d-m-Y", strtotime($retirement->ActualReleaseDate)), "id" => "releaseDate", "class" => "datePicker form-control required")); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 ">Accuumulated Leave Days</label>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Accummulated Leave Days">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'actleaveDate', 'value' => $retirement->AccumulatedDays, "id" => "lprAvailDay", "class" => "form-control")); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <legend class="legend">Benefits</legend>
                                <div class="form-group" style="overflow: scroll;height: 250px; width: 500px">
                                <table id="sailorTable5" class="table table-striped table-bordered" cellspacing="1" >
                                    <thead>
                                    <tr>
                                        <th>Benefits</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    $retirementBenefit = array();
                                    foreach ($retirementBen as $row): 
                                        $retirementBenefit[] = $row->BenefitsID;
                                    endforeach;
                                    ?>
                                    <?php foreach ($benefits as $row): ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="benefits[]" value="<?php echo $row->CauseTypeID; ?>" <?php echo (in_array($row->CauseTypeID, $retirementBenefit)) ? "checked" : "" ?>><label>&nbsp;&nbsp;<?php echo $row->Name ?></label>
                                            </td>
                                        </tr>
                                        <?php endforeach; ?>   
                                    </tbody>
                                </table>
                                </div>
                            </div>  
                            <div class="form-group">
                                    <div class="col-sm-5">
                                        <input type="checkbox" id="lrp" name="lrp" value="1"><label>&nbsp;&nbsp;LRP Taken</label>&nbsp;&nbsp;&nbsp;&nbsp;<input type="button" id="CalCulateLeave" name="CalCulateLeave" value="Calculate Leave" class="btn btn-danger">
                                    </div>
                            </div>
                            <div class="col-md-6">
                                
                                <div class="form-group" style="overflow: scroll; height: 250px; width: 500px; margin-left: 485px; margin-top: -105px;">
                                <table id="sailorTable5" class="table table-striped table-bordered" cellspacing="1" >
                                    <thead>
                                    <tr>
                                        <th>Leave Year</th>
                                        <th>Days</th>
                                        
                                    </tr>
                                    </thead>
                                    <tbody>
                                    
                                        <!-- <tr>
                                            <td>
                                                <input type="text" name="leaveYear[]" value="">
                                            </td>
                                            <td>
                                                <input type="text" name="leaveDays[]" value="">
                                            </td>
                                        </tr> -->
                                         
                                    </tbody>
                                </table>
                                </div>
                            </div>                          
                        </fieldset>
                    </div>  
                    <div class="col-md-12">
                        <div class="form-group">                        
                            <label class="col-sm-2 ">&nbsp;</label>
                            <div class="col-sm-8">
                                <input type="hidden" value="<?php echo $retirement->RetirementID; ?>" name="id">
                                <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="Retirement/Transactions/RetirementProcess/update" data-redirect-action="Retirement/Transactions/RetirementProcess/index" value="Update">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    $(document).on("change", "#releaseType", function () {
        var id = $(this).val();
        $.ajax({
            type: "post",
            url: "<?php echo site_url('Retirement/Transactions/RetirementProcess/releaseCause'); ?>/",
            data: {causeId: id},
            beforeSend: function () {
                $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
            },
            success: function (data) {
                $('#releaseCause').html(data);
                $('#releaseCause').select2('val', '');
            }
        })
    });
   /* $('#lrp').change(function(){
        $("#lrpAvailedDays").prop("disabled", !$(this).is(':checked'));
    });*/
    $(document).ready(function () {

    $('input[id=lrp]').click(function(){
       if ($('input[id=lrp]').is(':checked')) {
        $('#lrpAvailedDays').val('120');
    }
    else{
        $('#lrpAvailedDays').val('0');
    }
    });
   
});
   
     $(document).on("click", "#CalCulateLeave", function () {
        var isValid = 0;
        var id = $(this).val();
        var officialNumber = $("#officialNumber").val();
        var authorityNumber = $("#authorityNumber").val();
        if(officialNumber == '')
        {
            alert("Please Provide Official Number");
             isValid = 1;
            return false;
        }
        
        else if(authorityNumber == '')
        {
            alert("Please Provide an Authority Number");
             isValid = 1;
            return false;
        }
        var SAILOR_ID = $("#SAILOR_ID").val();
        if( isValid = 0)
        {

            $.ajax({
                type: "post",
                url: "<?php echo site_url('Retirement/Transactions/RetirementProcess/leave'); ?>/",
                data: {SAILOR_ID: SAILOR_ID},
                dataType: "json",
                beforeSend: function () {
                    $("#loader").html("<img src='<?php echo base_url(); ?>assets/img/loader.gif' />");
                },
                success: function (data) {
                    if(data.leave != '')
                    {
                        var data = data.leave;
                        console.log(data);
                        $.each(data, function (obj,value) {
                            var leave = value['LeaveYear']
                            Year =  leave 
                            var availedDay = value['LeaveAvailed'];
                            Day =  availedDay
                            $("#sailorTable5 tbody").append("<tr><td>" + Year + "</td><td>" + Day + "</td></tr>");
                        });
                    }
                }
            })
        }
    });
</script>