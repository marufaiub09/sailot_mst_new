<div class="row">
    <form class="form-horizontal frmContent" id="MainForm" method="post">
        <div class="col-md-12" id="tabs">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-1 col-sm-2 col-xs-4">
                        <a class="btn btn-danger btn-xs "  href="<?php echo site_url('Retirement/Transactions/CapRetiredProso/CapRetiredProso/index'); ?>" title="List Sailor informaion">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Add Retired Personnel</h3>
                    </div>  
                </div>
            </div>
            <div class="panel panel-default tabs">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="active "><a href="form-layouts-tabbed.html#tab-first" role="tab" data-toggle="tab">General Info & Engagement Info</a></li>
                    <li><a href="form-layouts-tabbed.html#tab-second" role="tab" data-toggle="tab">Permanent Address and Next of Kin Info</a></li>
                </ul>
                <div class="panel-body tab-content">
                    <div class="tab-pane active" id="tab-first">


                        <div class="form-group col-md-12">                            
                            <div>
                                <label class="col-md-2 control-label">Official Number</label>
                                <div class="col-md-3">
                                    <?php echo form_input(array('name' => 'officialNumber', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Select Batch Number ')); ?>
                                </div>
                                <label class="col-md-2 control-label">Local Number</label>
                                <div class="col-md-3">
                                    <?php echo form_dropdown('localNumberDis', $localNumber, '', 'id="localNumber" required="required" class="form-control select2"'); ?>
                                </div>
                                <div class="col-md-2">
                                    <?php echo form_input(array('name' => 'localNumber', 'id' => 'localNumberReplace', "class" => "form-control localNumber", 'value' => set_value('localShortName'))); ?>
                                </div>
                            </div>
                            <hr style="height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">
                        </div>
                        <div class="form-group col-md-12">                            
                            <div>
                                <label class="col-md-2 control-label">Batch Number</label>
                                <div class="col-md-3">
                                    <?php echo form_dropdown('batch', $batch, '', 'id="batchid" required="required" class="form-control select2" data-placeholder="Select Branch Number" aria-hidden="true" data-allow-clear="true"'); ?>
                                </div>
                                <label class="col-md-2 control-label">P No</label>
                                <div class="col-md-5">
                                    <?php echo form_input(array('name' => 'poNo', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Select P No ')); ?>
                                </div>
                            </div>
                            <hr style="height: 1px; background: #333; background-image: linear-gradient(to right, #ccc, #333, #ccc);">
                        </div> 
                        <legend  class="legend">General Information</legend>                        
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Rank <span class="text-danger"> * </span></label>
                                    <div class="col-md-6">
                                        <select class="select2 form-control" name="RankName" id="RankId" required="required" data-placeholder="Select Rank" aria-hidden="true" data-allow-clear="true">
                                            <option value="">Select Rank</option>
                                            <?php
                                            foreach ($rankData as $row):
                                                ?>                                           
                                                <option value="<?php echo $row->RANK_ID . "_" . $row->BRANCH_ID . "_" . $row->EQUIVALANT_RANKID; ?>"><?php echo "[" . $row->RANK_CODE . "] " . $row->RANK_NAME ?></option>
                                                <?php
                                            endforeach;
                                            ?>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>                                        
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">

                                    <label class="col-sm-5">Birth Date <span class="text-danger"> *</span></label>
                                    <div class="col-sm-5 date" >
                                        <?php echo form_input(array('name' => 'birthDate', "class" => "datePicker form-control", 'value' => date('d-m-Y'), 'placeholder' => 'Birth Date')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select BirthDate">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Full Name  <span class="text-danger"> * </span></label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fullName', 'type' => 'text', "class" => "form-control fullName required", 'required' => 'required', 'placeholder' => 'Enter Full Name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Full Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">সম্পুর্ণ নাম বাংলায়</label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fullNameInBn', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'সম্পুর্ণ নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Short Name <span class="text-danger"> * </span></label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'shortName', 'type' => 'text', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Enter Short Name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Short Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">সংক্ষিপ্ত নাম বাংলায়</label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'shortNameBangla', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'সংক্ষিপ্ত নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">                                        
                                    <label class="col-md-4">Nick Name</label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'nickName', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'Enter Nick Name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter NIck Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">                                        
                                    <label class="col-md-5">ডাক নাম বাংলায়</label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'nickNameInBangla', 'type' => 'text', "class" => "form-control ", 'placeholder' => 'ডাক নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Father's Name <span class="text-danger"> * </span></label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fatherName', 'type' => 'text', "class" => "form-control required", 'required' => 'required', 'placeholder' => "Enter Father's Name")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Father's Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">বাবার নাম বাংলায়</label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'fatherNameInBangls', 'type' => 'text', "class" => "form-control ", 'placeholder' => "বাবার নাম বাংলায়")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="asa">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Mother's Name <span class="text-danger"> * </span></label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'motherName', 'type' => 'text', "class" => "form-control required", 'required' => 'required', 'placeholder' => "Enter Mother's Name")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Mother's Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-5">মায়ের নাম বাংলায়</label>
                                    <div class="col-md-6">
                                        <?php echo form_input(array('name' => 'motherNameInBangla', 'type' => 'text', "class" => "form-control ", 'placeholder' => "মায়ের নাম বাংলায়")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Rank">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-md-4">Maritial Status <span class="text-danger"> * </span></label>
                                    <div class="col-md-5">
                                        <select name="mariatialStatus" class="form-control required select2" data-placeholder="Select Status" aria-hidden="true" data-allow-clear="true">
                                            <option value="0">Unmarried</option>
                                            <option value="1">Married</option>
                                            <option value="2">Separation</option>
                                            <option value="3">Divorce</option>
                                            <option value="4">Widower</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Maritial Status">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4">Religion <span class="text-danger"> * </span></label>
                                    <div class="col-md-5">
                                        <select name="religion" class="form-control required select2" data-placeholder="Select Status" aria-hidden="true" data-allow-clear="true" style="width: 100%;">
                                            <option value="1">Islam</option>
                                            <option value="2">Hindu</option>
                                            <option value="3">Buddhist</option>
                                            <option value="4">Christian</option>
                                            <option value="5">Other's</option>                                                
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Religion">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <fieldset class="">
                                    <legend  class="legend" style="font-size: 12px;  margin-bottom: 10px;">Freedom fighter status</legend>
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label class="check"><input type="radio" class="iradio" name="fredomfighterStatus" checked="checked" value="0" />None</label>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="check"><input type="radio" class="iradio" name="fredomfighterStatus" value="1" /> Freedom Fighter</label>
                                        </div>
                                        <div class="col-md-5">
                                            <label class="check"><input type="radio" class="iradio" name="fredomfighterStatus" name="iradio" value="2" /> Freedom Fighter Child</label>
                                        </div>
                                    </div> 

                                </fieldset>                 
                            </div>
                        </div>
                        <legend  class="legend" >Engagement Information and Period</legend>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-3">Entry Date</label>
                                    <div class="col-sm-4 date" >
                                        <?php echo form_input(array('name' => 'entryDate', "id" => "engagementDate", "class" => "datePicker form-control", 'value' => date('d-m-Y'), 'placeholder' => 'Entry Date')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Entry Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3">Entry Type</label>
                                    <div class="col-md-4">
                                        <?php echo form_dropdown('entryType', $entryType, '', 'id="batchid" required="required" class="form-control select2" '); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Entry Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">


                                <div class="form-group" style="margin-bottom:1px">
                                    <label class="col-sm-4"></label>
                                    <div class="col-sm-2"> &nbsp; Year </div>
                                    <div class="col-sm-2"> Month </div>
                                    <div class="col-sm-2"> &nbsp; Day </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Period</label>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'priodYY', "id" => "priodYY", "class" => "form-control required", "value" => "12", "maxlength" => "2")); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'priodMM', "id" => "priodMM", "class" => "form-control required", "value" => "00", "maxlength" => "2", "max" => "12")); ?>
                                    </div>
                                    <div class="col-sm-2">
                                        <?php echo form_input(array('name' => 'priodDD', "id" => "priodDD", "class" => "form-control required", "value" => "00", "maxlength" => "2", "max" => "31")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Give Engagement Period">
                                        <i class="fa fa-question-circle"></i>
                                    </a>

                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Expire Date  <span class="text-danger"> * </span></label>
                                    <div class="col-sm-5">
                                        <?php echo form_input(array('name' => 'expireDate', "id" => "expireDate", "class" => "form-control required", "readonly" => "readonly", "placeholder" => "Expire date")); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Auto Calculate Expire Date">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>                                    
                                <hr style=" margin-bottom: 5px; margin-top: 0;">

                            </div>

                        </div>    

                        <div class="col-md-12">
                            <fieldset class="">
                                <legend  class="legend" style="font-size: 12px;  margin-bottom: 10px;">Posting Unit</legend>
                                <div class="col-md-6">                                    
                                    <div class="form-group">
                                        <label class="col-sm-4">Zone</label>
                                        <div class="col-sm-6" >
                                            <?php echo form_dropdown("zone", $zone, set_value("zone"), "class='form-control select2' style='width: 120px;' id='zone'"); ?>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Zone">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Area</label>
                                        <div class="col-sm-6" >
                                            <select  id="area" name="area" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Area">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4">Ship/Establishment</label>
                                        <div class="col-sm-6" >
                                            <select  id="shift" name="shiftEstablishment" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Ship/Establishment">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>                                

                                    <div class="form-group">
                                        <label class="col-sm-4">Posting Unit</label>
                                        <div class="col-sm-6" >
                                            <select  id="postingUnit" name="postingUnit" class="form-control select2" data-placeholder="Select One" aria-hidden="true" data-allow-clear="true">
                                                <option value="">Select One</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Posting Unit">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>                                
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-sm-4">Posting Date</label>
                                        <div class="col-sm-4 date" >
                                            <?php echo form_input(array('name' => 'postingDate', "class" => "datePicker form-control", 'value' => date('d-m-Y'), 'placeholder' => 'Posting Date')); ?>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Give a Posting Date">
                                            <i class="fa fa-question-circle"></i>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4">Posting Type</label>
                                        <div class="col-md-6">
                                            <select name="postingType" class="form-control select2" data-placeholder="Posting Type" aria-hidden="true" data-allow-clear="true">
                                                <option value="1">Normal</option>
                                                <option value="2">Instructor</option>
                                                <option value="3">Course</option>
                                                <option value="4">School Staff</option>
                                                <option value="5">Release</option>
                                                <option value="6">Deputation</option>
                                            </select>
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Select Posting Type">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>
                                </div>                                
                            </fieldset>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-second">
                        <legend  class="legend">Permanent Address:</legend>
                        <div class="col-md-12">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Division <span class="text-danger"> * </span></label>
                                    <div class="col-sm-6">
                                        <?php echo form_dropdown("division", $division, set_value("division"), "class='form-control select2'style='width:180px' placeholder='Select Division' id='division_id'"); ?>

                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please sleect Division name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Select District  <span class="text-danger"> * </span></label>
                                    <div class="col-sm-6" >
                                        <select  id="district_id" name="districtName" class="select2 form-control" style="width:180px">
                                            <option value="">Select One</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select District Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Police Station<span class="text-danger">*</span></label>
                                    <div class="col-sm-6" >
                                        <select  id="thana_id" name="policeStation" class="select2 form-control" style="width:180px">
                                            <option value="">Select One</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Police Station/Thana Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Permanent Address<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddress', "class" => "form-control", 'placeholder' => 'Permanent Address', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Permanent Address">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">স্থায়ী ঠিকানা বাংলায়</label>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'permanentAddressInBan', "class" => "form-control", 'placeholder' => 'স্থায়ী ঠিকানা বাংলায়', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="স্থায়ী ঠিকানা বাংলায়">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <legend class="legend">Next of kin Information And Address: </legend>
                        <div class="col-md-12">
                            <div class="col-md-6">                               
                                <div class="form-group">
                                    <label class="col-sm-4">Next of kin<span class="text-danger">*</span></label>
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'nextOfKinName', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Next of kin name')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Training Type">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4">আত্মীয়র নাম বাংলায়</label>
                                    <div class="col-sm-6" >
                                        <?php echo form_input(array('name' => 'nextOfKinNameInBan', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'আত্মীয়র নাম বাংলায়')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select training name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Relation of next kin<span class="text-danger">*</span></label>
                                    <div class="col-sm-6" >
                                        <?php echo form_dropdown('relationOfNextKin', $relation, '', 'id="relation" required="required" class="form-control select2" style="width: 200px;" '); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select District Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4"></label>
                                    <div class="col-sm-6 checkbox checkbox-danger">
                                        <input  id="checkbox" name= "ifNextOfKinSame"  type="checkbox" class="styled " checked="checked" onchange="toggleStatus()" >
                                        <label for="checkbox6" >
                                            Police Station(if different)
                                        </label>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Unchecked if different">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6"  id="check">
                                <div class="form-group">
                                    <label class="col-sm-4">Division</label>
                                    <div class="col-sm-6" >
                                        <?php echo form_dropdown("divisionOfNextKin", $division, set_value("division"), "class='form-control select2' style='width:180px;' id='division'"); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please sleect Division name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Select District </label>
                                    <div class="col-sm-6" >
                                        <select  id="district" name="districtNameOfKin" class="form-control select2" style="width: 180px">
                                            <option value="">Select One</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select District Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4">Select Police Station</label>
                                    <div class="col-sm-6" >
                                        <select  id="thana" name="thanaNameOnKin" class="form-control select2" style="width: 180px"">
                                            <option value="">Select One</option>
                                        </select>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please select Police Station/Thana Name">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-4"></label>
                                    <div class="col-sm-6 checkbox checkbox-danger">
                                        <input  id="btnSameAsPermanent" name= "ifNextOfKinSame"  type="checkbox" class="styled ">
                                        <label for="checkbox6" >
                                            Address(if Same) 
                                        </label>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Unchecked if different">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-12">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">Next of kin address<span class="text-danger">*</span></label>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'nextOfKinAddress', "class" => "form-control", 'placeholder' => 'Next of kin address', 'id' => 'nextOfkinAdd', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Enter Kin Address">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="col-sm-4">আত্মীয়র ঠিকানা বাংলায়</label>
                                    <div class="col-sm-6">
                                        <?php echo form_textarea(array('name' => 'nextOfKinAddressInBangla', "class" => "form-control", 'placeholder' => 'আত্মীয়র ঠিকানা বাংলায়', 'id' => 'nextOfKinInBangla', 'cols' => '6', 'rows' => '2')); ?>
                                    </div>
                                    <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="আত্মীয়র ঠিকানা বাংলায়">
                                        <i class="fa fa-question-circle"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>
</div>
<script type="text/javascript">
//    $(document).on("click", ".step2", function (e) {
//        var isValid = 0;
//        var a = 0;
//        var localNumber = $("#localNumber").val();
//        var fullName = $(".fullName").val();
//
//        if (localNumber == "") {
//            alert("Select Local Number");
//            isValid = 1;
//            return false;
//        } else if (fullName == "") {
//            alert("Select Full Name");
//            isValid = 1;
//            return false;
//        }
//    })








    $(document).ready(function () {
        $('#division_id').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('setup/common/ajax_get_division'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#district_id').html(data);
                }
            });
        });
        $('#district_id').change(function () {
            var selectedValue = $(this).val();
            var url = '<?php echo site_url('setup/common/ajax_get_district') ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#thana_id').html(data);
                }
            });
        });

        $('#division').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('setup/common/ajax_get_division'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#district').html(data);
                }
            });
        });
        $('#district').change(function () {
            var selectedValue = $(this).val();
            var url = '<?php echo site_url('setup/common/ajax_get_district'); ?>';
            $.ajax({
                type: "POST",
                url: url,
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#thana').html(data);
                }
            });
        });
        $('#zone').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_area'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#area').html(data);
                }
            });
        });
        $('#localNumber').change(function () {
            var selectedValue = $("#localNumber :selected").text().trim();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_local_number'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#localNumberReplace').val(data);
                }
            });
        });
        $('#area').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_shift'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#shift').html(data);
                }
            });
        });
        $('#shift').change(function () {
            var selectedValue = $(this).val();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_postingUnit'); ?>",
                data: {selectedValue: selectedValue},
                dataType: 'html',
                success: function (data) {
                    $('#postingUnit').html(data);
                }
            });
        });
    });
    $("#division").prop("disabled", true);
    $("#district").prop("disabled", true);
    $("#thana").prop("disabled", true);
    $(document).ready(function () {
        handleStatusChanged();
    });
    function handleStatusChanged() {
        $('#checkbox').on('change', function () {
            toggleStatus();
        });
    }
    function toggleStatus() {
        if ($('#checkbox').is(':checked')) {
            $('#check :input').attr('disabled', true);
        } else {
            $('#check :input').removeAttr('disabled');
        }
    }

    $(document).on("click", "#add", function (e) {
        var isValid = 0;
        var a = 0;
        var examSystem = $("#examSystem").val();
        var exam_id = $("#examNameDesc").val();
        var examNameDesc = $("#examNameDesc :selected").text().trim();
        var subjectGroupDesc = $("#subjectGroupDesc :selected").text().trim();
        var subjectValidation = $("#subjectGroupDesc").val();
        var passingYear = $("#passingYearid").val();
        var baordUniversityDesc = $("#baordUniversityDesc :selected").text().trim();
        var boardValidation = $("#baordUniversityDesc").val();
        var result = $("#results :selected").text().trim();
        var resultValidation = $("#results").val();
        var resultDesc = $("#resultDesc").val();
        var percent = $("#percent").val();
        var gpa = $("#gpa").val();
        var eduStatusDesc = $("#eduStatusDesc :selected").text().trim();
        if (examSystem == "") {
            alert("Select Exam System");
            isValid = 1;
            return false;
        } else if (exam_id == "") {
            alert("Select Exam Name");
            isValid = 1;
            return false;
        } else if (subjectValidation == "") {
            alert("Select Subject/Group");
            isValid = 1;
            return false;
        } else if (passingYear == "") {
            alert("Select Passing Year");
            isValid = 1;
            return false;
        } else if (boardValidation == "") {
            alert("Select Board/University");
            isValid = 1;
            return false;
        } else if (resultValidation == "") {
            alert("Select Result");
            isValid = 1;
            return false;
        }
        if (isValid == 0) {
            $.ajax({
                type: "POST",
                url: '<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/getEduData') ?>',
                data: {examSystem: examSystem, examNameDesc: examNameDesc, subjectGroupDesc: subjectGroupDesc, passingYear: passingYear, baordUniversityDesc: baordUniversityDesc, result: result, resultDesc: resultDesc, eduStatusDesc: eduStatusDesc, percent: percent, gpa: gpa},
                success: function (data) {
                    $(".eximId").each(function () {
                        if ($(this).val() == examNameDesc) {
                            a = 1;
                        }
                    });
                    if (a == 1) {
                        alert("You Already selected this Exam");
                        return false;
                    } else {
                        $("#selected_pc").append(data);
                    }
                }
            });
        }
    })
    $(document).on("click", ".removeEdu", function () {
        $(this).parent().parent("tr").remove();
    });

    function engDate() {
        var year = $("#yearId").val();
        var month = $("#monthId").val();
        var day = $("#dayId").val();
        var expireDate = year + "/" + month + "/" + day;
        console.log(expireDate);
    }

    $('.inputData').keyup(function () {
        var feetValue = $('#feetData').val();
        var inchValue = $('#incData').val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/feetconvert'); ?>",
            data: {feetValue: feetValue, inchValue: inchValue},
            dataType: 'html',
            success: function (data) {
                $('#cmData').val(data);
            }
        });
    });

    $('#kgData').keyup(function () {
        var kgValue = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/kgconvert'); ?>",
            data: {kgValue: kgValue},
            dataType: 'html',
            success: function (data) {
                $('#lbData').val(data);
            }
        });
    });

    $('#inchChest').keyup(function () {
        var incChestValue = $(this).val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/chestconvert'); ?>",
            data: {incChestValue: incChestValue},
            dataType: 'html',
            success: function (data) {
                $('#cmChests').val(data);
            }
        });
    });


    $(document).on("click", "#btnSameAsPermanent", function () {
        if ($(this).is(":checked")) {
            var selectDistVal = $("#district_id :selected").text().trim();
            var selectThanaVal = $("#thana_id :selected").text();
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_NextOfKinAddress'); ?>",
                data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
                dataType: 'html',
                success: function (data) {
                    $('#nextOfkinAdd').val(data);
                }
            });
        } else {
            $('#nextOfkinAdd').val('');
        }


    });

    $(document).on("click", "#btnSameAsPermanent", function () {
        var selectDistVal = $("#district_id").val();
        var selectThanaVal = $("#thana_id").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/NextOfKinAddressInBangla'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfKinInBangla').val(data);
            }
        });
    });


    $('#thana').change(function () {
        var selectDistVal = $("#district :selected").text().trim();
        var selectThanaVal = $("#thana :selected").text().trim();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_NextOfKinAddressNotSame'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfkinAdd').val(data);
            }
        });
    });

    $('#thana').change(function () {
        var selectDistVal = $("#district").val();
        var selectThanaVal = $("#thana").val();
        $.ajax({
            type: "POST",
            url: "<?php echo site_url('SpecialTransection/CapturingNewRecrutiInfo/ajax_get_NextOfKinAddressInBangla'); ?>",
            data: {selectDistVal: selectDistVal, selectThanaVal: selectThanaVal},
            dataType: 'html',
            success: function (data) {
                $('#nextOfKinInBangla').val(data);
            }
        });
    });

    function AddDate(oldDate, offset, offsetType) {
        var year = parseInt(oldDate.getFullYear());
        var month = parseInt(oldDate.getMonth());
        var date = parseInt(oldDate.getDate());
        var hour = parseInt(oldDate.getHours());
        var newDate;
        switch (offsetType) {

            case "Y":
            case "y":

                newDate = new Date(year + offset, month, date, hour);
                break;
            case "M":
            case "m":

                newDate = new Date(year, month + offset, date, hour);
                break;

            case "D":
            case "d":

                newDate = new Date(year, month, date + offset, hour);
                break;

            case "H":
            case "h":
                newDate = new Date(year, month, date, hour + offset);
                break;

        }
        return newDate;

    }
    $("#priodDD").on('blur', function () {
        var dateStr = $("#engagementDate").val();
        var hiddenDate = $("#engPeriod").val();

        dArr = dateStr.split("-");
        var EngageDate = dArr[2] + "-" + dArr[1] + "-" + dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val());
        if (month > 12 || date > 31) {
            $("#priodMM").val('00');
            $("#priodDD").val('00');
            $("#expireDate").val('');
            $("#engageFor").val('');
        } else {
            if ($("#priodYY").val().length == 0) {
                year = 0;
            }
            if ($("#priodMM").val().length == 0) {
                month = 0;
            }
            if ($("#priodDD").val().length == 0) {
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if (exp_day.toString().length == 1) {
                exp_day = "0" + exp_day;
            }
            var exp_month = exp_date.getMonth() + 1;
            if (exp_month.toString().length == 1) {
                exp_month = "0" + exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year + "/" + exp_month + "/" + exp_day);

            /*start Engage for Year*/
            var end = new Date(exp_year + "-" + exp_month + "-" + exp_day);
            var cDate = new Date(EngageDate);
            var diff = new Date(end - cDate);
            var totalDays = diff / 1000 / 60 / 60 / 24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if (totalYears.toString().length == 1) {
                totalYears = "0" + totalYears;
            }
            if (totalMonths.toString().length == 1) {
                totalMonths = "0" + totalMonths;
            }
            if (remainingDays.toString().length == 1) {
                remainingDays = "0" + remainingDays;
            }
            $("#engageFor").val(totalYears + "" + totalMonths + "" + remainingDays);

            /*end Engage for Year*/
        }

    });
    $("#priodMM").on('blur', function () {
        var dateStr = $("#engagementDate").val();
        dArr = dateStr.split("-");
        var EngageDate = dArr[2] + "-" + dArr[1] + "-" + dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val());
        if (month > 12 || date > 31) {
            $("#priodMM").val('00');
            $("#priodDD").val('00');
            $("#expireDate").val('');
            $("#engageFor").val('');
        } else {
            if ($("#priodYY").val().length == 0) {
                year = 0;
            }
            if ($("#priodMM").val().length == 0) {
                month = 0;
            }
            if ($("#priodDD").val().length == 0) {
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if (exp_day.toString().length == 1) {
                exp_day = "0" + exp_day;
            }
            var exp_month = exp_date.getMonth() + 1;
            if (exp_month.toString().length == 1) {
                exp_month = "0" + exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year + "/" + exp_month + "/" + exp_day);

            /*start Engage for Year*/
            var end = new Date(exp_year + "-" + exp_month + "-" + exp_day);
            var cDate = new Date(EngageDate);
            var diff = new Date(end - cDate);
            var totalDays = diff / 1000 / 60 / 60 / 24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if (totalYears.toString().length == 1) {
                totalYears = "0" + totalYears;
            }
            if (totalMonths.toString().length == 1) {
                totalMonths = "0" + totalMonths;
            }
            if (remainingDays.toString().length == 1) {
                remainingDays = "0" + remainingDays;
            }
            $("#engageFor").val(totalYears + "" + totalMonths + "" + remainingDays);

            /*end Engage for Year*/
        }
    });
    $("#priodYY").on('blur', function () {
        var dateStr = $("#engagementDate").val();
        dArr = dateStr.split("-");
        var EngageDate = dArr[2] + "-" + dArr[1] + "-" + dArr[0];
        var date = parseInt($("#priodDD").val());
        var month = parseInt($("#priodMM").val());
        var year = parseInt($("#priodYY").val());
        if (month > 12 || date > 31) {
            $("#priodMM").val('00');
            $("#priodDD").val('00');
            $("#expireDate").val('');
            $("#engageFor").val('');
        } else {
            if ($("#priodYY").val().length == 0) {
                year = 0;
            }
            if ($("#priodMM").val().length == 0) {
                month = 0;
            }
            if ($("#priodDD").val().length == 0) {
                date = 0;
            }
            var next_date = AddDate(new Date(EngageDate), date, "D");
            var next_month = AddDate(next_date, month, "M");
            var exp_date = AddDate(next_month, year, "Y");

            var exp_day = exp_date.getDate();
            if (exp_day.toString().length == 1) {
                exp_day = "0" + exp_day;
            }
            var exp_month = exp_date.getMonth() + 1;
            if (exp_month.toString().length == 1) {
                exp_month = "0" + exp_month;
            }
            var exp_year = exp_date.getFullYear();

            $("#expireDate").val(exp_year + "/" + exp_month + "/" + exp_day);

            /*start Engage for Year*/
            var cDate = new Date(EngageDate);
            var end = new Date(exp_year + "-" + exp_month + "-" + exp_day);
            var diff = new Date(end - cDate);
            var totalDays = diff / 1000 / 60 / 60 / 24;
            var totalYears = Math.floor(totalDays / 365);
            var totalMonths = Math.floor((totalDays % 365) / 30);
            var remainingDays = Math.floor((totalDays % 365) % 30);
            if (totalYears.toString().length == 1) {
                totalYears = "0" + totalYears;
            }
            if (totalMonths.toString().length == 1) {
                totalMonths = "0" + totalMonths;
            }
            if (remainingDays.toString().length == 1) {
                remainingDays = "0" + remainingDays;
            }

            $("#engageFor").val(totalYears + "" + totalMonths + "" + remainingDays);
            /*end Engage for Year*/
        }
    });

    $(document).on('keyup', '.numbersOnly', function () {
        var val = $(this).val();
        if (isNaN(val)) {
            val = val.replace(/[^0-9\.]/g, '');
            if (val.split('.').length > 2) {
                val = val.replace(/\.+$/, "");
            }
        }
        $(this).val(val);
    });

    $(document).ready(function () {
        $("#examSystem").change(function () {
            $(this).find("option:selected").each(function () {
                if ($(this).attr("value") == "1") {
                    $("#gpadiv").hide();
                    $("#traditional").show();
                }
                else if ($(this).attr("value") == "2") {
                    $("#traditional").hide();
                    $("#gpadiv").show();
                }
                else {
                    $("#traditional").hide();
                }
            });
        }).change();
    });


</script>