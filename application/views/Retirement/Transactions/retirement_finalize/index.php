<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>dist/scripts/dataTables.responsive.css">

<script type="text/javascript" language="javascript">

    $(document).ready(function () {
        var dataTable = $('#employee-grid').DataTable();        
    });
</script>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-11 col-sm-10 col-xs-8">
                        <h3 class="panel-title">Retirement Finalize</h3>
                    </div>
                </div>

                <span class="pull-right clickable">
                    <i class="glyphicon glyphicon-chevron-up"></i>
                </span>
            </div>
            <div class="panel-body contentArea">
                <table id="employee-grid" class="display table-striped " width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>SN</th>
                            <th>Official NO</th>
                            <th>Name</th>
                            <th>Rank</th>
                            <th>Cause Name</th>
                            <th>Approve Date</th>   
                            <th>Action</th>                            
                        </tr>
                    </thead>
                    <tbody>  
                        <?php $sn = 1;  ?>
                        <?php foreach ($sailor as $key => $row) { ?>
                            <tr id="loader_<?php echo $sn; ?>">
                                <td><?php echo $sn; ?></td>
                                <td><?php echo $row->OFFICIALNUMBER; ?></td>
                                <td><?php echo $row->FULLNAME; ?></td>
                                <td><?php echo $row->RANK_NAME; ?></td>
                                <td><?php echo $row->cause; ?></td>
                                <td><?php echo date("d/m/Y",strtotime($row->ApproveDate)); ?></td>
                                <td>
                                    <?php echo '<a class="btn btn-success btn-xs finalize" sailorID="' . $row->SailorID . '" retirementID="' . $row->RetirementID . '" sn="' . $sn . '" title="Click to Retirement Finalize" type="button">Finalize</a> '.
                                                '<a class="btn btn-success btn-xs modalLink" href="' . site_url('Retirement/Transactions/finalizeRetirement/view/' . $row->RetirementID) . '" title="View Promotion Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                                                '<a class="btn btn-warning btn-xs" href="' . site_url('Retirement/Transactions/finalizeRetirement/edit/' . $row->RetirementID) . '" title="Edit Promotion Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                                                '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->RetirementID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="RetirementID" data-tbl="retirement"><span class="glyphicon glyphicon-trash"></span></a>'; 
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>             
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click", ".finalize", function () {
        if (confirm("Are You Sure?")) {
            var sailorID = $(this).attr("sailorID");
            var retirementID = $(this).attr("retirementID");
            var sn = $(this).attr("sn");
            $.ajax({
                type: "post",
                url: "<?php echo site_url('Retirement/Transactions/finalizeRetirement/finalizeApprove'); ?>/",
                data: {RetirementId: retirementID, sailorID: sailorID },
                beforeSend: function () {
                    $("#loader_" + sn).html("<img src='<?php echo base_url(); ?>dist/img/loader-small.gif' />");
                },
                success: function (data) {
                    if (data == "Y") {
                        $("#loader_" + sn).hide();                        
                        alert("Retirement Finalize Successfully");
                        
                    } else {
                        alert("Retirement Finalize failed ");
                    }
                }
            });
        } else {
            return false;
        }
    });
</script>