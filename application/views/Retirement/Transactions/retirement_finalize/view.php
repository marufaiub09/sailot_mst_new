<table id="datatable" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <tr>
        <th width="20%">Official Number</th>
        <td><?php echo $viewdetails->OFFICIALNUMBER ?></td>
    </tr>
    <tr>
        <th>Full Name</th>
        <td><?php echo $viewdetails->FULLNAME ?></td>
    </tr>
    <tr>
        <th>Rank Name</th>
        <td><?php echo $viewdetails->RANK_NAME ?></td>
    </tr> 
    <tr>
        <th>Approve Date</th>
        <td><?php echo date('Y-m-d', strtotime($viewdetails->ApproveDate)) ?></td>
    </tr> 
    <tr>
        <th>LPR</th>
        <td><?php echo $viewdetails->LPR; ?></td>
    </tr> 
    <tr>
        <th>LPR Days</th>
        <td><?php echo $viewdetails->LPRDays ?></td>
    </tr> 
    <tr>
        <th>Accumulated Days</th>
        <td><?php echo $viewdetails->AccumulatedDays ?></td>
    </tr> 
    <tr>
        <th>LPR Days Text</th>
        <td><?php echo $viewdetails->LPRDaysText ?></td>
    </tr> 
    <tr>
        <th>Accumulated days Text</th>
        <td><?php echo $viewdetails->AccumulatedDaysText ?></td>
    </tr> 
    <tr>
        <th>Actual Release Date</th>
        <td><?php echo date('Y-m-d', strtotime($viewdetails->ActualReleaseDate)) ?></td>
    </tr> 
    <tr>
        <th>Authority Number</th>
        <td><?php echo $viewdetails->AuthorityNumber ?></td>
    </tr> 
    <tr>
        <th>Authority Date</th>
        <td><?php echo date('Y-m-d', strtotime($viewdetails->AuthorityDate)); ?></td>
    </tr> 
    <tr>
        <th>DAO NO</th>
        <td><?php echo $viewdetails->DAONumber ?></td>
    </tr>
</table>