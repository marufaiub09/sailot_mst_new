<div class="row">
    <div class="col-md-12">
        <div class="panel panel-base">
            <div class="panel-heading">
                <div class="row">                   
                    <div class="col-md-11 col-sm-10 col-xs-8 ">
                        <h3 class="panel-title">Re-Approval of Payment Information</h3>                        
                    </div>  
                </div>

            </div>
            <div class="panel-body">
                <form class="form-horizontal frmContent unitMainForm" method="post">
                    <div class="row ">
                        <div class="col-md-10">
                            <legend  class="legend">Sailor Information</legend>
                            <fieldset class="">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Official Number<span class="text-danger">*</span></label>        
                                        <div class="col-sm-6" >
                                            <?php echo form_input(array('name' => 'officialNo', 'id' => 'officialNo', "class" => "form-control required", 'required' => 'required', 'placeholder' => 'Official Number')); ?>           
                                            <input type="hidden" name="SAILOR_ID" id="SAILOR_ID" class="sailorId" value="">           
                                        </div>
                                        <a class="help-icon" data-container="body" data-toggle="popover" data-placement="right" data-content="Please Put a Valid Official Number Then Press Tab">
                                            <i class="fa fa-question-circle"></i>
                                        </a>
                                    </div>                                
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label">Full Name</label>
                                        <div class="col-sm-7">
                                            <?php echo form_input(array('name' => 'fullName', "class" => "form-control fullName ", 'placeholder' => 'Sailor name', 'readonly' => 'readonly', 'value' => set_value('fullName'))); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Rank</label>                                            
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name' => 'rank', "class" => "form-control rank", 'placeholder' => 'Rank', 'readonly' => 'readonly', 'value' => set_value('rank'))); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-8">
                                        <label class="col-sm-2 control-label"></label>        
                                        <div class="col-sm-6" >
                                            <span class="smloadingImg"></span><span class="alertSMS label label-danger" style="font-size: 89%;"></span>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-md-10 row">
                        <legend  class="legend">Re-Approval Information</legend>               
                        <table id="paymentTable"  class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Cause</th>
                                    <th>Monthly Pension Amount</th>
                                    <th>Authority Number</th>
                                    <th>Authority Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        echo form_dropdown('Cause[]', 'default', 'class = "select2 form-control" id="cause"');
                                        ?>
                                    </td>
                                    <td>
                                        <input type="text" value="" name="pamentInfo[]" id="pamentinformation"  class="form-control required" placeholder="Monthly Payment Information" >
                                    </td>
                                    <td>
                                        <input type="text" name="AuthorityNu[]" id="AuthNu"  class="form-control required" placeholder="Authority Number" >
                                    </td>
                                    <td>
                                        <input type="text" name="authorityDate[]" id="authorityDate"  class="endDate col-sm-1 form-control required" placeholder="Authority Date" >
                                    </td>
                                    <td class="text-center">                       
                                    </td>
                                </tr>
                            </tbody>
                        </table>                
                    </div>
                    <div class="xh col-md-10" style="text-align: right;">
                        <span class="btn btn-xs btn-success" id="add_record">
                            <i style="cursor:pointer" class="fa fa-plus"> Add More</i>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">&nbsp;</label>
                        <div class="col-sm-6">
                            <input type="button" class="btn btn-primary btn-sm formSubmitWithRedirect" data-action="regularTransaction/examTestInfo/save" data-redirect-action="regularTransaction/examTestInfo/index" value="Update">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style>
    /* Ensure that the demo table scrolls */
    th, td { white-space: nowrap; }
    div.dataTables_wrapper {
        margin: 0 auto;
    }

    div.container {
        width: 80%;
    }
</style>
<?php $this->load->view("common/sailors_info"); ?>
<script>
    // append academic info table
    var counter = 1;
    $(document).on('click', '#add_record', function () {
        counter++;
        $("#paymentTable tbody").append(' <tr>' +
                '<td>' +
                ' <input type="text" name="causeId[]" id="cause' + counter + '"  class="form-control required" placeholder="Cause" >' +
                '</td>' +
                '<td>' +
                ' <input type="text" value="" name="pamentInfo[]" id="pamentInfo' + counter + '"  class="form-control" placeholder="Monthly Payment Information" >' +
                '</td>' +
                '<td>' +
                ' <input type="text" name="AuthorityNu[]" id="AuthorityNu' + counter + '"  class="form-control " placeholder="Authority Number" >' +
                '</td>' +
                '<td>' +
                ' <input type="text" name="authorityDate[]" id="authorityDate' + counter + '"  class="endDate col-sm-1 form-control required sEndDate" placeholder="Authority date" >' +
                '</td>' +
                '<td class="text-center">' +
                '<span class="btn btn-xs btn-danger" id="remove_tr"><i style="cursor:pointer" class="fa fa-times" > Remove</i></span>' +
                '</td>' +
                '</tr>'
                );
    });
    $(document).on('click', '#remove_tr', function () {
        if (counter > 1) {
            $(this).closest('tr').remove();
            counter--;
        }
        return false;
    });

    $("#authorityDate" + counter).datepicker();

    //    $('#paymentTable').removeAttr('width').DataTable({
    //        "scrollX": true,
    //        "scrollX": true,
    //                "bPaginate": false,
    //        "bFilter": false,
    //        "bInfo": false,
    //        "columnDefs": [
    //            {width: '100px', targets: 0},
    //            {width: '100px', targets: 1},
    //            {width: '100px', targets: 2},
    //            {width: '80px', targets: 3},
    //            {width: '80px', targets: 5},
    //            {width: '90px', targets: 6},
    //            {width: '70px', targets: 7},
    //            {width: '90px', targets: 8},
    //            {width: '90px', targets: 9},
    //        ],
    //        "fixedColumns": true
    //
    //    });
</script>