<?php
    $userSessonData = $this->session->userdata('logged_in');
    $FULLNAME = $userSessonData['FULLNAME'];
    $session_org_id = $userSessonData['SES_ORG_ID'];
    $session_usergroup_id = $userSessonData['USERGRP_ID'];
    $session_user_id = $userSessonData['USER_ID'];
    $session_user_entity = $userSessonData['USER_ENTITY'];


    $org_name = $this->db->query("SELECT ORG_NAME FROM bn_organization_hierarchy WHERE ORG_ID = $session_org_id ")->row();
    $user_group = $this->db->query("SELECT USERGRP_NAME FROM sa_user_group WHERE USERGRP_ID = $session_usergroup_id ")->row();
    $user_profile_image = $this->db->query("SELECT USERIMG FROM sa_users WHERE USER_ID = $session_user_id ")->row();

?>
<style type="text/css">
    .icon_logo{
        height: 90px;
        width: 90px;
        margin-top: 10px;
        padding: 8px;
        border: 1px solid black;
        border-radius: 50%;
    }
    .dropdown-menu li{
        padding-left: 20px;
        padding-right: 20px;
    }
    .top_profile{
        text-align: center;

    }
</style>
<nav class="navbar navbar-side navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle-secondary collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-toggle-primary" id="menu-toggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>dist/img/navy_logo.jpg" width="40"/> Sailor Management System</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">

            <!-- Right nav -->
            <ul class="nav navbar-nav navbar-right">
                <?php if ($session_user_entity == 'A') { ?>
                    <li><a href="#" class="user-menu"><i class="glyphicon glyphicon-user"></i> <?php echo "&nbsp" . $applicant_full_name; ?> </a>
                        <ul class="dropdown-menu">
                            <li>

                                <a class="btn btn-primary btn-flat" href="<?php echo site_url('auth/logout'); ?>"> Sign out</a>

                            </li>
                        </ul>
                    </li> 
                <?php } else { ?>

                    <li><a href = "#" class = "user-menu"><i class = "glyphicon glyphicon-user"></i> <?php echo $FULLNAME;
                    ?> <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <p class="top_profile">
                                    <img class="icon_logo" src="<?php echo base_url("src/upload/profile_picture") . "/" . $user_profile_image->USERIMG; ?>" alt="" width="90" height="90">
                                </p>
                                <p>
                                    <strong> Login :</strong>
                                    <?php
                                    date_default_timezone_set("asia/Dhaka");
                                    echo date("H:i A  m-d-Y");
                                    ?></p>
                                <p><strong>User Group:</strong> <?php echo $user_group->USERGRP_NAME; ?></p>
                                <a class="btn btn-primary btn-flat" href="<?php echo site_url('auth/logout'); ?>"> Sign out</a>

                            </li>
                        </ul>
                    </li>
                <?php } ?>

            </ul>
        </div>
    </div>
</nav>