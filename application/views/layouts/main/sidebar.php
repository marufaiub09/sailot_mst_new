<?php
$orgId       = $_SESSION['logged_in']['SES_ORG_ID'];
$userGroupId = $_SESSION['logged_in']['USERGRP_ID'];
$userLevelId = $_SESSION['logged_in']['USERLVL_ID'];
$modules     = $this->accessControl_model->getModuleByOrganization($orgId);
?>
<div id="sidebar-wrapper">
    <ul id="sidebar-menu" class="sidebar-menu">
        <li data-toggle="collapse" data-target="#dashboard" class="collapsed">
            <a href="<?php echo site_url('dashboard/index'); ?>"><i class="fa fa-tachometer fa-lg"></i> Dashboard</a>
        </li>
        <?php
        if (!empty($modules)) $i           = 1;
        foreach ($modules as $module) {
            $links = $this->accessControl_model->getAccessLink($orgId,
                $userGroupId, $userLevelId, $module->SA_MODULE_ID);
            if (!empty($links)) {
                ?>
                <li data-toggle="collapse" data-target="#service_<?php echo $i; ?>" class="collapsed">
                    <a href="#"><i class="<?php echo $module->MODULE_ICON; ?>"></i><?php echo $module->MODULE_NAME; ?><span class="arrow"></span></a>
                </li>

                <ul class="sub-menu collapse" id="service_<?php echo $i; ?>">

                    <?php
                    $j = 1;
                    foreach ($links as $link) {
                        ?>
                        <li data-toggle="collapse" data-target="#serviceChild_<?php echo $i."_".$j; ?>" class="collapsed" ><a href="<?php echo ($link->URL_URI != "#")? site_url($link->URL_URI) : '#' ?>"><i class="fa fa-angle-right"></i> <?php echo $link->LINK_NAME; ?></a></li>
                        <?php
                        /*$childMenu = $this->db->query("SELECT sml.LINK_NAME,sml.URL_URI URL, som.* FROM sa_org_mlinks som
                                                        INNER JOIN sa_module_links sml on sml.LINK_ID = som.LINK_ID
                                                        WHERE sml.PR_LINK_ID = $link->LINK_ID ")->result();*/
                        $childMenu = $this->db->query("SELECT   sml.LINK_ID, sml.LINK_NAME, sml.LINK_NAME_BN, sml.URL_URI URL, sml.SL_NO, sgm.SA_UGLWM_LINK, sgm.`CREATE`, sgm.`READ`, sgm.`UPDATE`
                                                        FROM     sa_uglw_mlink as sgm
                                                                 LEFT JOIN sa_org_mlinks as som USING (SA_MLINKS_ID)
                                                                 LEFT JOIN sa_module_links as sml ON som.LINK_ID = sml.LINK_ID
                                                        WHERE    sgm.ORG_ID = $orgId and sgm.UG_LEVEL_ID = $userLevelId and sml.ACTIVE_STATUS = 1 and sgm.USERGRP_ID = $userGroupId and sgm.SA_MODULE_ID = $module->SA_MODULE_ID and sgm.STATUS = 1 AND sml.PR_LINK_ID = $link->LINK_ID
                                                        ORDER BY SL_NO ASC")->result();

                        
                        
                        if(!empty($childMenu)){ ?>
                                <ul class="sub-sub-menu collapse" id="serviceChild_<?php echo $i."_".$j++; ?>">
                                <?php foreach ($childMenu as $cm) {?>
                                    <li style="padding-left: 20%;"><a href="<?php echo site_url($cm->URL) ?>"><i class="fa fa-angle-right"></i> <?php echo $cm->LINK_NAME; ?></a></li>
                                <?php } ?>
                                </ul>
                        <?php } ?>
                    <?php } ?>

                </ul>

                <?php
            }

            $i++;
        }
        ?>



    </ul>
</div>
