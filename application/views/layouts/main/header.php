<link rel="stylesheet" href="<?php echo base_url('dist/styles/main.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('dist/styles/custom.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('dist/styles/side_menu.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('dist/styles/jquery.dataTables.min.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('dist/redactor/redactor.css'); ?>">

<link rel="stylesheet" href="<?php echo base_url('bower_components/morris.js/morris.css'); ?>">
<script src="<?php echo base_url('dist/scripts/modernizr.js'); ?>"></script>

<script src="<?php echo base_url('dist/scripts/jquery.min.js'); ?>"></script>
<script src="<?php echo base_url('dist/scripts/jquery-ui.js'); ?>"></script>
<script>window.jQuery || document.write('<script src="dist/scripts/jquery.js"><\/script>')</script>
<script src="<?php echo base_url('dist/scripts/side_menu.js'); ?>"></script>
<script src="<?php echo base_url('dist/scripts/jquery.validate.js'); ?>"></script>
<!--[if lt IE 9]>
<script src="<?php echo base_url('dist/scripts/html5shiv-printshiv.min.js'); ?>"></script>
<script src="<?php echo base_url('dist/scripts/respond.min.js'); ?>"></script>
<![endif]-->