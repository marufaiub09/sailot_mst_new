<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ItemInfoModel extends CI_Model {

    public function insert($tableName, $data) {
        $this->db->insert($tableName, $data);
        return $this->db->insert_id();
    }

    public function allItemInformation($table) {
        return $this->db->query("SELECT a.*, (SELECT b.LOOKUP_DATA_NAME FROM sa_lookup_data b WHERE b.LOOKUP_DATA_ID = a.ITEM_CATEGORY) AS CATEGORY FROM {$table} a ")->result();
    }

    public function getAllItemInformationById($id) {
        return $this->db->query("SELECT a.*, (SELECT b.LOOKUP_DATA_NAME FROM sa_lookup_data b WHERE b.LOOKUP_DATA_ID = a.ITEM_CATEGORY) AS CATEGORY , (SELECT c.ITEM_NAME FROM sa_budget_code c WHERE c.EXP_ITEM_NO = a.EXP_ITEM_NO) AS EXP_ITEM, (SELECT d.ORG_NAME FROM sa_organizations d WHERE d.ORG_ID = a.ORG_ID) AS ORG_NAME, (SELECT e.GROUP_NAME FROM sa_itemgroups e WHERE e.GROUP_ID = a.GROUP_ID) AS GROUP_NAME FROM mm_iteminfo a WHERE a.ITEM_ID = {$id}")->row();
    }

    public function getAllOrganization($table) {
        return $this->db->query("SELECT * FROM {$table}")->result_array();
    }

    public function getAllOrganizationInfoById($id) {
        return $this->db->query("SELECT a.*, (SELECT o.ORG_NAME FROM sa_organizations o WHERE o.ORG_ID = a.PARENT_ORG_ID) AS PARENT_ORG_NAME, (SELECT b.LOOKUP_DATA_NAME FROM sa_lookup_data b WHERE b.LOOKUP_DATA_ID = a.ORG_TYPE_ID) AS ORG_TYPE FROM sa_organizations a WHERE a.ORG_ID = {$id}")->row();
    }

    function dropdownFromTableWithCondition($tableName, $selectText, $key, $value, $condition = '', $orderByField = '', $orderBy = 'ASC') {
        if (!empty($condition)) {
            $this->db->where($condition);
        }
        if ($orderByField == '') {
            $this->db->order_by("$value", "$orderBy");
        } else {
            $this->db->order_by("$orderByField", "$orderBy");
        }
        $query = $this->db->get($tableName);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                if (!empty($row->{$value})) {
                    $lookupInfo[$row->{$key}] = $row->{$value};
                }
            }
        }
        if (!empty($lookupInfo)) {
            return $lookupInfo;
        }
    }

}

