<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Name: DOMPDF
 *
 * Author: Jd Fiscus
 * jdfiscus@gmail.com
 * @iamfiscus
 *
 *
 * Origin API Class: http://code.google.com/p/dompdf/
 *
 * Location: http://github.com/iamfiscus/Codeigniter-DOMPDF/
 *
 * Created: 06.22.2010
 *
 * Description: This is a Codeigniter library which allows you to convert HTML to PDF with the DOMPDF library
 *
 */
class Mpdf_gen {

    public function __construct() {
        $this->CI = & get_instance();
        require_once 'mpdf/mpdf.php';
    }
    public function gen_pdf($html, $paper = 'A4') {
        $mpdf = new mPDF('utf-8', $paper);
        $mpdf->mirrorMargins = 1;
        $mpdf->SetWatermarkImage('dist/img/navy_logo.jpg');
        $mpdf->showWatermarkImage = true;
        $mpdf->watermarkImageAlpha = 0.1;
        $mpdf->WriteHTML($html);
        $fileName = date('Y_m_d_H_i_s');
        $mpdf->Output('DGDP_' . $fileName . '.pdf', 'I');
    }

}
