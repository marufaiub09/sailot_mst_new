<?php
/**
 * Created by PhpStorm.
 * User: streetcoder
 * Date: 2/24/16
 * Time: 4:24 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
class TemplateGenerator extends EXT_Controller{

    public function __construct(){

        parent::__construct();

    }

    public function index(){
        $data['pageTitle'] = 'Template Generator';
        $data['breadcrumbs'] = array(
            'Template Generator' => '#'
        );
        $data['content_view_page'] = 'templateGenerator/index';

        $this->template->display($data);
    }

    public function create(){

        $this->form_validation->set_rules('template_name', 'Template Name', 'trim|required');
        $this->form_validation->set_rules('category', 'Template Category', 'required');
        $this->form_validation->set_rules('type', 'Template Type', 'required');

        if ($this->form_validation->run() == FALSE) {

            $errors = validation_errors();
            if(!empty($errors)){

                $this->util->resultObject(0,validation_errors());

            }else{
                $this->load->view("templateGenerator/create");
            }


        }else{
            $data = array(
                'TEMPL_NAME' => $this->input->post('template_name'),
                'TEMPL_CAT' => $this->input->post('category'),
                'TEMPL_TYPE' => $this->input->post('type'),
                'TEMPL_SUBJECT' => $this->input->post('subject'),
                'TEMPL_BODY' => $this->input->post('body'),
            );
            if ($this->utilities->insertData($data, 'sa_template_letter')) {

                $this->util->resultObject(1,'Record Created Successfully!');
            }
        }

    }

    public function ajaxTplGenIndex(){
        $this->load->library('DataTable');

        $draw               = $_REQUEST['draw'];
        $start              = $_REQUEST['start'];
        $length             = $_REQUEST['length'];
        $colToSort          = $_REQUEST["order"][0]["column"];
        $colToSortAction    = $_REQUEST["order"][0]["dir"];
        $search             = trim($_REQUEST["search"]["value"]);

        $tableName          = 'sa_template_letter';

        $fieldsName         = array('TEMPL_NAME',array('type' => 'alt','field_name' => 'TEMPL_CAT','value' => array('E'=>'Email', 'L'=>'Letter/Forwarding Letter')),array('type' => 'alt','field_name' => 'TEMPL_TYPE','value' => array('I'=>'Individual', 'G'=>'Global')));

        $actions = array('TemplateGenerator/view/','TemplateGenerator/edit/','TemplateGenerator/remove/');
        $modalTitle = array('View','Edit','Remove');
        $icons = array('glyphicon-eye-open','glyphicon-edit','glyphicon-trash');
        $htmlClass = array('modalLink text-primary view','modalLink text-info edit','text-danger ajaxDelete');

        $recordsArr = $this->datatable->getDataTable(
            $draw,
            $start,
            $length,
            $colToSort,
            $colToSortAction,
            $search,
            $tableName,
            $fieldsName,
            $actions,
            $modalTitle,
            $icons,
            $htmlClass
        );

        echo json_encode($recordsArr);
    }

    public function view($id){
        $data['tpl_details'] = $this->utilities->findByAttribute('sa_template_letter',
            array('TEMPL_ID' => $id));
        $this->load->view("templateGenerator/view",$data);
    }

    public function edit($id){

        $this->form_validation->set_rules('template_name', 'Template Name', 'trim|required');
        $this->form_validation->set_rules('category', 'Template Category', 'required');
        $this->form_validation->set_rules('type', 'Template Type', 'required');

        if ($this->form_validation->run() == FALSE) {

            $errors = validation_errors();

            if(!empty($errors)){

                $this->util->resultObject(0,$errors);

            }else{
                $data['tpl_details'] = $this->utilities->findByAttribute('sa_template_letter',
                    array('TEMPL_ID' => $id));
                $this->load->view("templateGenerator/edit",$data);
            }


        }else{

            $data = array(
                'TEMPL_NAME' => $this->input->post('template_name'),
                'TEMPL_CAT' => $this->input->post('category'),
                'TEMPL_TYPE' => $this->input->post('type'),
                'TEMPL_SUBJECT' => $this->input->post('subject'),
                'TEMPL_BODY' => $this->input->post('body'),
            );
            if ($this->utilities->updateData('sa_template_letter', $data,
                array('TEMPL_ID' => $id))) {

                $this->util->resultObject(1,'Record Updated Successfully!');

            }
        }

    }

    public function remove($id){

        if ($this->utilities->deleteRowByAttribute(
            'sa_template_letter', array('TEMPL_ID' => $id))
        ) {

            $this->util->resultObject(1,'Record Deleted Successfully!');

        }

    }

    public function stack(){
        $data['pageTitle'] = 'Stack';
        $data['breadcrumbs'] = array(
            'Stack' => '#'
        );
        $data['content_view_page'] = 'templateGenerator/stack';

        $this->template->display($data);
    }


}