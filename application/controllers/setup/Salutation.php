<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Salutation extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return  // all data from sa_salutation table
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Salutation Setup';
        $data['salutation'] = $this->utilities->findAllFromView('sa_salutation');
        $data['content_view_page'] = 'setup/salutation/index';
        $this->template->display($data);
    }

    /*
     * @methodName Create()
     * @access
     * @param  none
     * @return  // 
     */

    public function create() {
        if (isset($_POST['salutation_name'])) {
            $salutation = array(
                'SALUTATION_NAME' => $this->input->post('salutation_name'),
                'GENDER' => $this->input->post('salutation_gender'),
                'RELIGION_ID' => $this->input->post('religion'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"]
            );


            if ($this->utilities->insertData($salutation, 'sa_salutation')) {
                redirect('setup/salutation/index');
            }
        }
        $data['religion_list'] = $this->utilities-> dropdownFromTableWithCondition('sav_religion','','RELIGION_ID','RELIGION_NAME');
        $this->load->view('setup/salutation/create', $data);
    }

    /*
     * @methodName edit()
     * @access
     * @param  $SALUTATION_ID
     * @return  //
     */

    public function edit($SALUTATION_ID) {

        if (isset($_POST['salutation_name'])) {
             $salutation = array(
                'SALUTATION_NAME' => $this->input->post('salutation_name'),
                'GENDER' => $this->input->post('salutation_gender'),
                'RELIGION_ID' => $this->input->post('religion'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'UPD_DT' => date("Y-m-d H:i:s"),
                'UPD_BY' => $this->user_session["USER_ID"]
            );

            if ($this->utilities->updateData('sa_salutation', $salutation, array('SALUTATION_ID' => $SALUTATION_ID))) {
                redirect('setup/salutation/index');
            }
        }
        $data['salutation'] = $this->utilities->findByAttribute('sa_salutation', array('SALUTATION_ID' => $SALUTATION_ID));
        $data['religion_list'] = $this->utilities-> dropdownFromTableWithCondition('sav_religion','','RELIGION_ID','RELIGION_NAME');
        $this->load->view('setup/salutation/edit', $data);
    }
    
    /*
     * @methodName view()
     * @access
     * @param  $SALUTATION_ID
     * @return  //
     */
    
    public function view($SALUTATION_ID) {
        $data['salutation'] = $this->utilities->findByAttribute('sa_salutation', array('SALUTATION_ID' => $SALUTATION_ID));
        $religion_id = $data['salutation']->RELIGION_ID;
        $data['religion_name'] = $this->utilities->findByAttribute('sav_religion', array('RELIGION_ID' => $religion_id));
        $this->load->view('setup/salutation/view', $data);
    }

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}