<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class publicExRelation extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        $this->load->model('itemInfoModel');
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }
    /**
     * @methodName Tree Builder()
     * @access
     * @param  none
     * @return  Return Tree as unorder list  
     */

    private function buildTree($flat, $pidKey, $idKey = null) {
        $grouped = array();
        foreach ($flat as $sub) {
            $grouped[$sub[$pidKey]][] = $sub;
        }
        $treeBuilder = function($siblings) use (&$treeBuilder, $grouped, $idKey) {
            foreach ($siblings as $k => $sibling) {
                $id = $sibling[$idKey];
                if (isset($grouped[$id])) {
                    $sibling['children'] = $treeBuilder($grouped[$id]);
                }
                $siblings[$k] = $sibling;
            }
            return $siblings;
        };
        $tree = $treeBuilder($grouped['']);
        return $tree;
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
	public function pubExHierarchy()
	{
		$data['metaTitle'] = 'Setup Module';
        $data['breadcrumbs'] = array(
            'Setup' => 'setup/index',
            'Setup' => '#'
        );
        $data['pageTitle'] = 'Public Exam Relationship';

        $query = $this->itemInfoModel->getAllOrganization('bn_govtexam_hierarchy');
        $data['tree'] = $this->buildTree($query, 'PARENT_ID', 'GOVT_EXAMID');

        /*$data['result'] = $this->utilities->findAllFromView('bn_bdadminhierarchy');*/
        $data['content_view_page'] = 'setup/publicExRelation/index';
        $this->template->display($data);
	}

}

/* End of file Tree.php */
/* Location: ./application/controllers/setup/Tree.php */