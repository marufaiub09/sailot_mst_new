<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TrainingName extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Training Name Setup';
        $data['result'] = $this->db->query("SELECT nh.*, type.Name type, level.Name level
											FROM bn_navytraininghierarchy nh 
											INNER JOIN bn_navytraininghierarchy type on type.NAVYTrainingID = nh.ParentID
											INNER JOIN bn_navytraininghierarchy level on level.NAVYTrainingID = type.ParentID
											WHERE nh.NAVYTrainingType = 3")->result();
        $data['content_view_page'] = 'setup/Training_Name/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['training_type'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navytraininghierarchy", array("NAVYTrainingType" => 2),"NAME");
        $this->load->view('setup/Training_Name/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function save() {
        $code = $this->input->post('code', true);
        $name = $this->input->post('name', true);
        $bn_name = $this->input->post('bn_name', true);
        $parentid = $this->input->post('training_type', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navytraininghierarchy", array("Name" => $name, "Code" => $code, 'PartIIID' => $parentid));
        if (empty($check)) {// if exam name name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'BanglaName' => $bn_name,
                'ParentID' => $parentid,
                'NAVYTrainingType' => 3,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navytraininghierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Name Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Training Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul islam <reazul@atilimited.net>
     * @return      single row
     */
    function trainingNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT nh.*, type.Name type, level.Name level
											FROM bn_navytraininghierarchy nh 
											INNER JOIN bn_navytraininghierarchy type on type.NAVYTrainingID = nh.ParentID
											INNER JOIN bn_navytraininghierarchy level on level.NAVYTrainingID = type.ParentID
											WHERE nh.NAVYTrainingID =$id")->row();
        $this->load->view('setup/Training_Name/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul islam <reazul@atilimited.net>
     * @return      
     */
    function TrainingNameList() {
        $data['result'] = $this->db->query("SELECT nh.*, type.Name type, level.Name level
                                        FROM bn_navytraininghierarchy nh 
                                        INNER JOIN bn_navytraininghierarchy type on type.NAVYTrainingID = nh.ParentID
                                        INNER JOIN bn_navytraininghierarchy level on level.NAVYTrainingID = type.ParentID
                                        WHERE nh.NAVYTrainingType = 3")->result();
        $this->load->view("setup/Training_Name/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['trainingType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navytraininghierarchy", array("NAVYTrainingType" => 2),"NAME");
        $data['result'] = $this->utilities->findByAttribute('bn_navytraininghierarchy', array('NAVYTrainingID' => $id));
        $this->load->view('setup/Training_Name/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePart() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $bn_name = $this->input->post('bn_name', true);
        $code = $this->input->post('code', true);
        $parentid = $this->input->post('training_type', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navytraininghierarchy", array("Name" => $name, "Code" => $code, "NAVYTrainingID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'BanglaName' => $bn_name,
                'ParentID' => $parentid,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navytraininghierarchy', $data, array("NAVYTrainingID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Name Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Training Name Already Exist</div>";
        }
    }

    function ajaxTrainingNameSetupList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'nh.NAVYTrainingID', 1 => 'nh.Code', 2 => 'nh.Name', 3 => 'nh.BanglaName', 4 => 'type.Name', 5 => 'level.Name', 6 => 'nh.NAVYTrainingID');

        // getting total number records without any search

        $query = $this->db->query("SELECT nh.*, type.Name type, level.Name level
                                        FROM bn_navytraininghierarchy nh 
                                        INNER JOIN bn_navytraininghierarchy type on type.NAVYTrainingID = nh.ParentID
                                        INNER JOIN bn_navytraininghierarchy level on level.NAVYTrainingID = type.ParentID")->num_rows();


        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT nh.*, type.Name type, level.Name level
                                        FROM bn_navytraininghierarchy nh 
                                        INNER JOIN bn_navytraininghierarchy type on type.NAVYTrainingID = nh.ParentID
                                        INNER JOIN bn_navytraininghierarchy level on level.NAVYTrainingID = type.ParentID
                                      WHERE nh.Code LIKE '" . $requestData['search']['value'] . "%' OR nh.Name LIKE '" . $requestData['search']['value'] . "%' OR type.Name LIKE '" . $requestData['search']['value'] . "%' OR level.Name LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT nh.*, type.Name type, level.Name level
                                        FROM bn_navytraininghierarchy nh 
                                        INNER JOIN bn_navytraininghierarchy type on type.NAVYTrainingID = nh.ParentID
                                        INNER JOIN bn_navytraininghierarchy level on level.NAVYTrainingID = type.ParentID
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $status = ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" id="' . $row->NAVYTrainingID . '" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>';
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->Code;
            $nestedData[] = $row->Name;
            $nestedData[] = $row->BanglaName;
            $nestedData[] = $row->type;
            $nestedData[] = $row->level;
            $nestedData[] =
                    '<a class="itemStatus" data-sn="'.$sn.'" id="'.$row->NAVYTrainingID.'" data-su-url="'. ('setup/TrainingName/trainingNameById/'.$sn).'" data-status="'.$row->ACTIVE_STATUS.'" data-fieldId="NAVYTrainingID" data-field="ACTIVE_STATUS" data-tbl="bn_navytraininghierarchy"  title="Click For status change">'.$status.'</a> '.
                    '<a class="btn btn-warning btn-xs modalLink cm" href="' . site_url('setup/TrainingName/edit/' . $row->NAVYTrainingID) . '" title="Edit Training Name" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->NAVYTrainingID . '" sn="' . $sn. '" title="Click For Delete" data-type="delete" data-field="NAVYTrainingID" data-tbl="bn_navytraininghierarchy"><span class="glyphicon glyphicon-trash"></span></a>' ;           
             $data[] = $nestedData;
             $sn++;
            
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file TrainingName.php */
/* Location: ./application/controllers/setup/TrainingName.php */