<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BoardUniversity extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Board University';
        $data['result'] = $this->utilities->getAll("bn_boardcenter");
        $data['content_view_page'] = 'setup/BoardUniversity/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('setup/BoardUniversity/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function saveBoardUniversity() {
        $CODE = $this->input->post('boardCode', true);
        $NAME = $this->input->post('boardName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_boardcenter", array("NAME" => $NAME));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_boardcenter')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Board/University Name Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Board/University Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Board/University  Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function boardUniversityById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_boardcenter", array("BOARD_CENTERID" => $id));
        $this->load->view('setup/BoardUniversity/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function boardUniversityList() {
        $data['result'] = $this->utilities->getAll("bn_boardcenter");
        $this->load->view("setup/BoardUniversity/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_boardcenter', array('BOARD_CENTERID' => $id));
        $this->load->view('setup/BoardUniversity/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateBoardUniversity() {
        $id = $this->input->post('id', true);
        $CODE = $this->input->post('BoardCode', true);
        $NAME = $this->input->post('BoardName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_boardcenter", array("NAME" => $NAME, "BOARD_CENTERID !=" => $id));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a"));
            if ($this->utilities->updateData('bn_boardcenter', $data, array("BOARD_CENTERID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Board/University Name  Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Board/University Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Board/University Name Already Exist</div>";
        }
    }

//    
//    
//    private function pr($data) {
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit;
//    }
}