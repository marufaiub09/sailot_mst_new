<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class JesthataPadak extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Jesthata Padak Setup';
        $data['result'] = $this->utilities->getAll("bn_jesthatapadak");
        $data['content_view_page'] = 'setup/jesthata_padak/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/jesthata_padak/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveJP()
    {        
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $sh_name= $this->input->post('sh_name', true);     
        $desc = $this->input->post('desc', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_jesthatapadak", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'SHORT_NAME' => $sh_name,
                'DESCR' => $desc,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_jesthatapadak')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Jesthata padakJesthata padak Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Jesthata padakJesthata padak Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Jesthata padakJesthata padak Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function jpById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_jesthatapadak", array("JestotaMedal_ID" => $id));
        $this->load->view('setup/jesthata_padak/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function jpList() {
        $data['result'] = $this->utilities->getAll("bn_jesthatapadak");
        $this->load->view("setup/jesthata_padak/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_jesthatapadak', array('JestotaMedal_ID' => $id));
        $this->load->view('setup/jesthata_padak/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateJP()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $sh_name= $this->input->post('sh_name', true);     
        $desc = $this->input->post('desc', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_jesthatapadak", array("NAME" => $name, "JestotaMedal_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'SHORT_NAME' => $sh_name,
                'DESCR' => $desc,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_jesthatapadak',$data, array("JestotaMedal_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Jesthata padak Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Jesthata padak Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Jesthata padak Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_jesthatapadak', array('JestotaMedal_ID' => $id));
        $this->load->view('setup/jesthata_padak/view',$data);
    }

}
