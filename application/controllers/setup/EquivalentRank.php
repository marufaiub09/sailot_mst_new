<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EquivalentRank extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Template
     */
    function index() {
        $data['pageTitle'] = 'Equivalent Rank Setup';
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['EquivalentRank_list'] = $this->utilities->getAll("bn_equivalent_rank");
        $data['content_view_page'] = 'setup/equivalent_rank/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Template
     */
    function create() {
        if (isset($_POST['RANK_NAME'])) {
            $data = array(
            'RANK_CODE' => $this->input->post('RANK_CODE'),
            'RANK_NAME' => $this->input->post('RANK_NAME'),
            'BANGLA_NAME' => $this->input->post('BANGLA_NAME'),
            'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
            'CRE_BY' => $this->user_session["USER_ID"]

            );
            if ($this->utilities->insertData($data, 'bn_equivalent_rank')) {
                redirect('setup/EquivalentRank/index');
            }
        }
        $this->load->view('setup/equivalent_rank/create');
    }

    /**
     * @access      public
     * @param       Group ID
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Template
     */
    public function edit($id) {
        if (isset($_POST['RANK_NAME'])) {
            $data = array(
                'RANK_CODE' => $this->input->post('RANK_CODE'),
                'RANK_NAME' => $this->input->post('RANK_NAME'),
                'BANGLA_NAME' => $this->input->post('BANGLA_NAME'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DATE' => date("Y-m-d h:i:s a")
            );

            if ($this->utilities->updateData('bn_equivalent_rank', $data, array('EQUIVALANT_RANKID' => $id))) {
                redirect('setup/EquivalentRank/index');
            }
        }
        $data['row'] = $this->utilities->findByAttribute('bn_equivalent_rank', array('EQUIVALANT_RANKID' => $id));
        $this->load->view('setup/equivalent_rank/edit', $data);
    }

}

/* End of file EquivalentRank.php */
/* Location: ./application/controllers/setup/EquivalentRank.php */