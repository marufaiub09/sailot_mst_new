<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DaoNumber extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'DAO Number Setup';
        $data['content_view_page'] = 'setup/DaoNumber/index';
        $this->template->display($data);
    }

    /*
     * @methodName Create()
     * @access
     * @param  none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return  //
     */

    public function create() {
        $this->load->view('setup/DaoNumber/create');
    }

    /*
     * @access
     * @param  rank _id
     * @return  //
     */

    function saveDaoNumber() {
        $DaoNumber = $this->input->post('DaoNumber', true);
        $DaoDate = date('Y-m-d', strtotime($this->input->post('DaoDate', true)));
        $IS_PUBLISHED = (isset($_POST['IS_PUBLISHED'])) ? 1 : 0;
        // checking if trade with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_dao", array("DAO_NO" => $DaoNumber, "Date" => $DaoDate));
        if (empty($check)) {// if trade name available
            $data = array(
                'DAO_NO' => $DaoNumber,
                'Date' => $DaoDate,
                'Is_Published' => $IS_PUBLISHED,
                'ACTIVE_STATUS' => 1,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($IS_PUBLISHED == 1) {
                $this->db->query("UPDATE bn_dao set Is_Published = 0");
            }
            if ($this->utilities->insertData($data, 'bn_dao')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Dao Number Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Dao Number insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Dao Number Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function DaoNumberById($sn) {
        print_r($_POST);
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT * FROM bn_dao WHERE DAO_ID = $id ")->row();
        $this->load->view('setup/DaoNumber/single_DaoNumber_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function DaoNumberList() {
        $data['dao_list'] = $this->db->query("SELECT * FROM bn_dao")->result();
        $this->load->view("setup/DaoNumber/DaoNumber_list", $data);
    }

    /*
     * @access
     * @param  rank _id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return  //
     */

    public function editDaoNumber($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_dao', array('DAO_ID' => $id));
        $this->load->view("setup/DaoNumber/edit", $data);
    }

    /*
     * @methodName viewRank()
     * @access
     * @param  rank _id
     * @return  //
     */

    function updateDaoNumber() {

        $id = $this->input->post('id', true);
        $DaoNumber = $this->input->post('DaoNumber', true);
        $DaoDate = date('Y-m-d', strtotime($this->input->post('DaoDate', true)));
        $IS_PUBLISHED = (isset($_POST['IS_PUBLISHED'])) ? 1 : 0;
        // checking if branch number with this name is already exist

        $check = $this->utilities->hasInformationByThisId("bn_dao", array("DAO_NO" => $DaoNumber, "Date" => $DaoDate ,"DAO_ID !=" => $id));
        if (empty($check)) {// if trade name available
            $data = array(
                'DAO_NO' => $DaoNumber,
                'Date' => $DaoDate,
                'Is_Published' => $IS_PUBLISHED,
                'ACTIVE_STATUS' => 1,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a"));
            if ($IS_PUBLISHED == 1) {
                $this->db->query("UPDATE bn_dao set Is_Published = 0");
            }
            if ($this->utilities->updateData('bn_dao', $data, array("DAO_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Dao Number Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Dao Number Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Dao Number Already Exist</div>";
        }
    }

    function ajaxDaoNumberSetupList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'DAO_ID', 1 => 'DAO_NO', 2 => 'Date', 3 => 'Is_Published', 4 => 'DAO_ID');

        // getting total number records without any search

        $query = $this->db->query("SELECT * FROM bn_dao ORDER BY DAO_ID DESC")->num_rows();


        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT * FROM bn_dao ORDER BY DAO_ID DESC LIKE '" . $requestData['search']['value'] . "%' OR Date LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT * FROM bn_dao 
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $status = ($row->Is_Published == 1) ? '<span class="label label-success" id="' . $row->DAO_ID . '" title="Click For Inactive">True</span>' : '<span class="label label-danger" title="Click For Active">False</span>';
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->DAO_NO;
            $nestedData[] = date("d/m/Y", strtotime($row->Date));
            $nestedData[] = '<a class="itemStatus" data-sn="' . $sn . '" id="' . $row->DAO_ID . '" data-su-url="' . ('setup/DaoNumber/DaoNumberById/' . $sn) . '" data-status="' . $row->Is_Published . '" data-fieldId="DAO_ID" data-field="Is_Published" data-tbl="bn_dao"  title="Click For status change">' . $status . '</a>';
            $nestedData[] =
                    '<a class="btn btn-warning btn-xs modalLink cm" href="' . site_url('setup/DaoNumber/editDaoNumber/' . $row->DAO_ID) . '" title="Edit Dao Number" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->DAO_ID . '" sn="' . $sn . '" title="Click For Delete" data-type="delete" data-field="DAO_ID" data-tbl="bn_dao"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
            $sn++;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

