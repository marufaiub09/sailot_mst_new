<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mission extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Willing Setup';
        $data['result'] = $this->utilities->getAll("bn_mission");
        $data['content_view_page'] = 'setup/mission/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/mission/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveMission()
    {        
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);    
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_mission", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_mission')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Willing Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Willing Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Willing Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function missionById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_mission", array("MISSION_ID" => $id));
        $this->load->view('setup/mission/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function missionList() {
        $data['result'] = $this->utilities->getAll("bn_mission");
        $this->load->view("setup/mission/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_mission', array('MISSION_ID' => $id));
        $this->load->view('setup/mission/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateMission()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);   
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_mission", array("NAME" => $name, "MISSION_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_mission',$data, array("MISSION_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Willing Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Willing Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Willing Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_mission', array('MISSION_ID' => $id));
        $this->load->view('setup/mission/view',$data);
    }

}
