<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TrainingLevel extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Training Setup';
        $data['training'] = $this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_navytraininghierarchy bn WHERE bn.NAVYTrainingID = nh.ParentID) BN_NAME
                                            FROM bn_navytraininghierarchy nh WHERE nh.NAVYTrainingType = 1")->result();
        $data['content_view_page'] = 'setup/Training_Level/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('setup/Training_Level/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function tList() {
        $data['training'] = $this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_navytraininghierarchy bn WHERE bn.NAVYTrainingID = nh.ParentID) BN_NAME
                                            FROM bn_navytraininghierarchy nh WHERE nh.NAVYTrainingType = 1")->result();
        ;
        $this->load->view("setup/Training_Level/Training_list", $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function saveTrainign() {
        $name = $this->input->post('training_name', true);
        $code = $this->input->post('training_code', true);
        $bn_name = $this->input->post('training_name_bn', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navytraininghierarchy", array("Name" => $name, "Code" => $code));
        if (empty($check)) {// if district name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'BanglaName' => $bn_name,
                'PartIIID' => null,
                'NAVYTrainingType' => 1,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navytraininghierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Training Name Already Exist</div>";
        }
    }

    function trcouById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_navytraininghierarchy", array("NAVYTrainingID" => $id));
        $this->load->view('setup/Training_Level/single_row', $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_navytraininghierarchy', array('NAVYTrainingID' => $id));
        $this->load->view('setup/Training_Level/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePart() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('training_name', true);
        $code = $this->input->post('training_code', true);
        $bn_name = $this->input->post('bn_name', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navytraininghierarchy", array("Name" => $name, "Code" => $code, "NAVYTrainingID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'BanglaName' => $bn_name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navytraininghierarchy', $data, array("NAVYTrainingID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Name Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Training Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
   

}
