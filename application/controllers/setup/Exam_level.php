<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Exam_level extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Level';
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_govtexam_hierarchy bn WHERE bn.GOVT_EXAMID = nh.PARENT_ID) BN_NAME
                                            FROM bn_govtexam_hierarchy nh WHERE nh.GOVTEXAM_TYPE = 1")->result();
        $data['content_view_page'] = 'setup/exam_level/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('setup/exam_level/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function saveExamLevel() {
        $CODE = $this->input->post('ExamCode', true);
        $NAME = $this->input->post('ExamName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_govtexam_hierarchy", array("NAME" => $NAME));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'PARENT_ID' => null,
                'GOVTEXAM_TYPE' => 1,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_govtexam_hierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Level Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Level insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Level Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function examLevelById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_govtexam_hierarchy", array("GOVT_EXAMID" => $id));
        $this->load->view('setup/exam_level/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function examLevelList() {
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_govtexam_hierarchy bn WHERE bn.GOVT_EXAMID = nh.PARENT_ID) BN_NAME
                                            FROM bn_govtexam_hierarchy nh WHERE nh.GOVTEXAM_TYPE = 1")->result();
        $this->load->view("setup/exam_level/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_govtexam_hierarchy', array('GOVT_EXAMID' => $id));
        $this->load->view('setup/exam_level/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateExamLevel() {
        $id = $this->input->post('id', true);
        $CODE = $this->input->post('ExamCode', true);
        $NAME = $this->input->post('ExamName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_govtexam_hierarchy", array("NAME" => $NAME, "GOVT_EXAMID !=" => $id));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a"));
            if ($this->utilities->updateData('bn_govtexam_hierarchy', $data, array("GOVT_EXAMID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Level Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Level Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Level Already Exist</div>";
        }
    }

//    
//    
//    private function pr($data) {
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit;
//    }
}