<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ExamGred extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Grade';
        $data['result'] = $this->utilities->getAll("bn_examgrade");
        $data['content_view_page'] = 'setup/ExamGred/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('setup/ExamGred/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function saveExamGrade() {
        $CODE = $this->input->post('gradeCode', true);
        $NAME = $this->input->post('gradeName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_examgrade", array("NAME" => $NAME));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_examgrade')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Grade Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Grade insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Grade Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function examGradeById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_examgrade", array("EXAM_GRADEID" => $id));
        $this->load->view('setup/ExamGred/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function examGradeList() {
        $data['result'] = $this->utilities->getAll("bn_examgrade");
        $this->load->view("setup/ExamGred/list", $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_examgrade', array('EXAM_GRADEID' => $id));
        $this->load->view('setup/ExamGred/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateExamGrade() {
        $id = $this->input->post('id', true);
        $CODE = $this->input->post('GradeCode', true);
        $NAME = $this->input->post('GradeName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;


        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_examgrade", array("NAME" => $NAME, "EXAM_GRADEID !=" => $id));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_examgrade', $data, array("EXAM_GRADEID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Grade Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Grade Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Grade Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
//    
//    
//    private function pr($data) {
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit;
//    }
}