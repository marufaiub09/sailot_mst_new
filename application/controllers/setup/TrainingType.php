<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TrainingType extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Training Name Setup';
        $data['traininName'] = $this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_navytraininghierarchy bn WHERE bn.NAVYTrainingID = nh.ParentID) BN_NAME
                                            FROM bn_navytraininghierarchy nh WHERE nh.NAVYTrainingType = 2")->result();
        $data['content_view_page'] = 'setup/Training_Type/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['parentid'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ParentID" => null));
        $this->load->view('setup/Training_Type/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function saveTrainignName() {
        $name = $this->input->post('training_name', true);
        $code = $this->input->post('training_code', true);
        $bn_name = $this->input->post('bn_name', true);
        $parent = $this->input->post('parentid');
        $bn_name = $this->input->post('training_name_bn', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navytraininghierarchy", array("Name" => $name, "Code" => $code));
        if (empty($check)) {// if district name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'BanglaName' => $bn_name,
                'ParentID' => $parent,
                'NAVYTrainingType' => 2,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navytraininghierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Create successfully</div>";
                //redirect('setup/TrainingCorse/index', 'refresh');
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Training Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function trainingById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_navytraininghierarchy bn WHERE bn.NAVYTrainingID = nh.ParentID) BN_NAME
                                            FROM bn_navytraininghierarchy nh WHERE nh.NAVYTrainingID =$id")->row();
        $this->load->view('setup/Training_Type/single_row', $data);
    }

    public function edit($id) {
        $data['examType'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ParentID" => null));
        $data['result'] = $this->utilities->findByAttribute('bn_navytraininghierarchy', array('NAVYTrainingID' => $id));
        $this->load->view('setup/Training_Type/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePart() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('training_name', true);
        $code = $this->input->post('training_code', true);
        $bn_name = $this->input->post('bn_name', true);
        $parentid = $this->input->post('exam_type', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navytraininghierarchy", array("Name" => $name, "Code" => $code, "NAVYTrainingID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'BanglaName' => $bn_name,
                'ParentID' => $parentid,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navytraininghierarchy', $data, array("NAVYTrainingID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Type Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Type Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Training Type Already Exist</div>";
        }
    }

    function trainingList() {
        $data['traininName'] = $this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_navytraininghierarchy bn WHERE bn.NAVYTrainingID = nh.ParentID) BN_NAME
                                            FROM bn_navytraininghierarchy nh WHERE nh.NAVYTrainingType = 2")->result();
        $this->load->view("setup/Training_Type/TrainingNameList", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
}