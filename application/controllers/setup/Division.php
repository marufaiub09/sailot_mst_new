<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Division extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Division Setup';
        $data['divisions'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE PARENT_ID is null ")->result();
        $data['content_view_page'] = 'setup/division/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/division/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveDivision()
    {
        $name= $this->input->post('division_name', true);
        $code= $this->input->post('division_code', true);
        $bn_name= $this->input->post('division_name_bn', true);
        $short_name= $this->input->post('bd_short_name', true);        
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Division with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_bdadminhierarchy", array("NAME" => $name));
        if (empty($check)) {// if Division name available
            $data = array(
                'PARENT_ID' => null,
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $short_name,
                'BDADMIN_TYPE' => 1,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_bdadminhierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Division Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Division Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Division Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function DivisionById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute('bn_bdadminhierarchy', array('BD_ADMINID' => $id)); // select single data select
        $this->load->view('setup/division/single_division_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function divisionList() {
        $data['divisions'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE PARENT_ID is null ")->result();
        $this->load->view("setup/division/division_list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_bdadminhierarchy', array('BD_ADMINID' => $id));
        $this->load->view('setup/division/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateDivision()
    {
        
        $id= $this->input->post('division_id', true);
        $name= $this->input->post('division_name', true);
        $code= $this->input->post('division_code', true);
        $bn_name= $this->input->post('division_name_bn', true);
        $short_name= $this->input->post('bd_short_name', true);        
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Division with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_bdadminhierarchy", array("NAME" => $name, "BD_ADMINID !=" => $id));
        if (empty($check)) {// if Division name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $short_name,
                'BDADMIN_TYPE' => 1,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_bdadminhierarchy',$data, array("BD_ADMINID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Division Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Division Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Division Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_bdadminhierarchy', array('BD_ADMINID' => $id));
        $this->load->view('setup/division/view',$data);
    }

}
