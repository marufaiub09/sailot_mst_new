<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PostingUnit extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Posting Unit Setup';
        $data['content_view_page'] = 'setup/posting_unit/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['unitType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_unittype", array("ACTIVE_STATUS" => 1),"NAME");
        $data['shipEsta'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['org'] = $this->utilities->findAllByAttributeWithOrderBy("bn_organization_hierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID !=" => "0"),"ORG_NAME");
        $this->load->view('setup/posting_unit/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function savePU() {
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $st_name = $this->input->post('st_name', true);
        $bn_name = $this->input->post('bn_name', true);
        $UNITTYPE_ID = $this->input->post('UNITTYPE_ID', true);
        $SE_ID = $this->input->post('SE_ID', true);
        $ORG_ID = $this->input->post('ORG_ID', true);
        $SEA_SERVICE = (isset($_POST['SEA_SERVICE'])) ? 1 : 0;
        $ISSUEING_AUTHORITY = (isset($_POST['ISSUEING_AUTHORITY'])) ? 1 : 0;
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Posting Unit with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_posting_unit", array('CODE' => $code, 'NAME' => $name, 'BANGLA_NAME' => $bn_name, 'SHORT_NAME' => $st_name, 'UNIT_TYPEID' => $UNITTYPE_ID, 'SHIP_ESTABLISHMENTID' => $SE_ID, 'ORG_ID' => $ORG_ID, 'IS_SEA_SERVICE' => $SEA_SERVICE, 'IS_ISSUING_AUTHORITY' => $ISSUEING_AUTHORITY,));
        if (empty($check)) {// if Posting Unit name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $st_name,
                'UNIT_TYPEID' => $UNITTYPE_ID,
                'SHIP_ESTABLISHMENTID' => $SE_ID,
                'ORG_ID' => $ORG_ID,
                'IS_SEA_SERVICE' => $SEA_SERVICE,
                'IS_ISSUING_AUTHORITY' => $ISSUEING_AUTHORITY,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_posting_unit')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Posting Unit Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Posting Unit insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Posting Unit Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */
    function puById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT pu.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE  oh.ORG_ID = pu.ORG_ID )ORG_NAME, 
                                        (SELECT ut.NAME FROM bn_unittype ut WHERE ut.UNIT_TYPEID = pu.UNIT_TYPEID )UNIT_TYPE,
                                        (SELECT se.NAME FROM bn_ship_establishment se WHERE se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID)SHIP_EST
                                        FROM bn_posting_unit pu
                                        WHERE pu.POSTING_UNITID = $id")->row();
        $this->load->view('setup/posting_unit/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */
    function puList() {
        $data['result'] = $this->db->query("SELECT pu.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE  oh.ORG_ID = pu.ORG_ID )ORG_NAME, 
                                        (SELECT ut.NAME FROM bn_unittype ut WHERE ut.UNIT_TYPEID = pu.UNIT_TYPEID )UNIT_TYPE,
                                        (SELECT se.NAME FROM bn_ship_establishment se WHERE se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID)SHIP_EST
                                        FROM bn_posting_unit pu")->result();
        $this->load->view("setup/posting_unit/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT pu.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE  oh.ORG_ID = pu.ORG_ID )ORG_NAME, 
                                        (SELECT ut.NAME FROM bn_unittype ut WHERE ut.UNIT_TYPEID = pu.UNIT_TYPEID )UNIT_TYPE,
                                        (SELECT se.NAME FROM bn_ship_establishment se WHERE se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID)SHIP_EST
                                        FROM bn_posting_unit pu
                                        WHERE pu.POSTING_UNITID = $id")->row();
        $data['unitType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_unittype", array("ACTIVE_STATUS" => 1),"NAME");
        $data['shipEsta'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['org'] = $this->utilities->findAllByAttributeWithOrderBy("bn_organization_hierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID !=" => "0"),"ORG_NAME");
        $this->load->view('setup/posting_unit/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePU() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $st_name = $this->input->post('st_name', true);
        $bn_name = $this->input->post('bn_name', true);
        $UNITTYPE_ID = $this->input->post('UNITTYPE_ID', true);
        $SE_ID = $this->input->post('SE_ID', true);
        $ORG_ID = $this->input->post('ORG_ID', true);
        $SEA_SERVICE = (isset($_POST['SEA_SERVICE'])) ? 1 : 0;
        $ISSUEING_AUTHORITY = (isset($_POST['ISSUEING_AUTHORITY'])) ? 1 : 0;
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_posting_unit", array("NAME" => $name, "CODE" => $code, "POSTING_UNITID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $st_name,
                'UNIT_TYPEID' => $UNITTYPE_ID,
                'SHIP_ESTABLISHMENTID' => $SE_ID,
                'ORG_ID' => $ORG_ID,
                'IS_SEA_SERVICE' => $SEA_SERVICE,
                'IS_ISSUING_AUTHORITY' => $ISSUEING_AUTHORITY,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_posting_unit', $data, array("POSTING_UNITID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Posting Unit Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Posting Unit Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Posting Unit Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        //echo"<pre>";print_r($id);exit;
        $data['viewdetails'] = $this->db->query("SELECT pu.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE  oh.ORG_ID = pu.ORG_ID )ORG_NAME, 
                                        (SELECT ut.NAME FROM bn_unittype ut WHERE ut.UNIT_TYPEID = pu.UNIT_TYPEID )UNIT_TYPE,
                                        (SELECT se.NAME FROM bn_ship_establishment se WHERE se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID)SHIP_EST
                                        FROM bn_posting_unit pu
                                        WHERE pu.POSTING_UNITID = $id")->row();
        $this->load->view('setup/posting_unit/view', $data);
    }

    function ajaxPostingUnitList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'pu.POSTING_UNITID', 1 => 'pu.CODE', 2 => 'pu.NAME', 3 => 'pu.SHORT_NAME', 4 => 'pu.POSTING_UNITID', 5 => 'se.NAME', 6 => 'oh.ORG_NAME', 7 => 'pu.POSTING_UNITID');

        // getting total number records without any search

        $query = $this->db->query("SELECT pu.*, oh.ORG_NAME ORG_NAME ,ut.NAME UNIT_TYPE, se.NAME SHIP_EST
                                        FROM bn_posting_unit pu 
                                        LEFT JOIN bn_organization_hierarchy oh on oh.ORG_ID = pu.ORG_ID 
                                        LEFT JOIN bn_unittype ut on ut.UNIT_TYPEID = pu.UNIT_TYPEID 
                                        LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT pu.*, oh.ORG_NAME ORG_NAME ,ut.NAME UNIT_TYPE, se.NAME SHIP_EST
                                        FROM bn_posting_unit pu 
                                        LEFT JOIN bn_organization_hierarchy oh on oh.ORG_ID = pu.ORG_ID 
                                        LEFT JOIN bn_unittype ut on ut.UNIT_TYPEID = pu.UNIT_TYPEID 
                                        LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID
                                      WHERE pu.CODE LIKE '" . $requestData['search']['value'] . "%' OR ORG_NAME LIKE '" . $requestData['search']['value'] . "%' OR pu.NAME LIKE '" . $requestData['search']['value'] . "%' OR pu.SHORT_NAME LIKE '" . $requestData['search']['value'] . "%' OR pu.POSTING_UNITID LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT pu.*, oh.ORG_NAME ORG_NAME ,ut.NAME UNIT_TYPE, se.NAME SHIP_EST
                                        FROM bn_posting_unit pu 
                                        LEFT JOIN bn_organization_hierarchy oh on oh.ORG_ID = pu.ORG_ID 
                                        LEFT JOIN bn_unittype ut on ut.UNIT_TYPEID = pu.UNIT_TYPEID 
                                        LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $status = ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" id="' . $row->POSTING_UNITID . '" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>';
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->CODE;
            $nestedData[] = $row->NAME;
            $nestedData[] = $row->SHORT_NAME;
            $nestedData[] = $row->UNIT_TYPE;
            $nestedData[] = $row->SHIP_EST;
            $nestedData[] = $row->ORG_NAME;
            $nestedData[] =
                    '<a class="itemStatus" data-sn="' . $sn . '" id="' . $row->POSTING_UNITID . '" data-su-url="' . ('setup/postingUnit/puById/' . $sn) . '" data-status="' . $row->ACTIVE_STATUS . '" data-fieldId="POSTING_UNITID" data-field="ACTIVE_STATUS" data-tbl="bn_posting_unit"  title="Click For status change">' . $status . '</a> ' .
                    '<a class="btn btn-success btn-xs modalLink" href="' . site_url('setup/postingUnit/view/' . $row->POSTING_UNITID) . '" title="View Posting Unit" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs modalLink cm" href="' . site_url('setup/postingUnit/edit/' . $row->POSTING_UNITID) . '" title="Edit Posting Unit" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->POSTING_UNITID . '" sn="' . $sn. '" title="Click For Delete" data-type="delete" data-field="POSTING_UNITID" data-tbl="bn_posting_unit"><span class="glyphicon glyphicon-trash"></span></a>' ;

            $data[] = $nestedData;
            $sn++;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}
