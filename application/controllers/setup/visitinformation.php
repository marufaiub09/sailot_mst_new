<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Visitinformation extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit information Setup';
        $data['result'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi")->result();
        $data['content_view_page'] = 'setup/visitinformation/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['visitClass'] = $this->utilities->findAllByAttributeWithOrderBy("bn_visitclassification", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('setup/visitinformation/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function saveVI() {
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $descr = $this->input->post('desc', true);
        $vci = $this->input->post('VISIT_CI_ID', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Visit information with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_visitinformation", array("NAME" => $name, "CODE" => $code, "VISIT_CLASSIFICATIONID" => $vci));
        if (empty($check)) {// if Visit information name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'DESCR' => $descr,
                'VISIT_CLASSIFICATIONID' => $vci,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_visitinformation')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Visit information Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Visit information Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Visit information Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */
    function viById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi WHERE vi.VISIT_INFO_ID = $id")->row();
        $this->load->view('setup/visitinformation/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */
    function viList() {
        $data['result'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi")->result();
        $this->load->view("setup/visitinformation/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi WHERE vi.VISIT_INFO_ID = $id")->row();
        $data['visitClass'] = $this->utilities->findAllByAttributeWithOrderBy("bn_visitclassification", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('setup/visitinformation/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateVI() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $descr = $this->input->post('desc', true);
        $vci = $this->input->post('VISIT_CI_ID', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_visitinformation", array("NAME" => $name, "CODE" => $code, "VISIT_INFO_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'DESCR' => $descr,
                'VISIT_CLASSIFICATIONID' => $vci,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_visitinformation', $data, array("VISIT_INFO_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Visit information Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Visit information Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Visit information Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi WHERE vi.VISIT_INFO_ID = $id")->row();
        $this->load->view('setup/visitinformation/view', $data);
    }

    function ajaxVisitInfoSetupList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'vi.VISIT_INFO_ID', 1 => 'vi.CODE', 2 => 'vi.NAME', 3 => 'vc.NAME', 4 => 'vi.DESCR', 5 => 'vi.VISIT_INFO_ID');

        // getting total number records without any search

        $query = $this->db->query("SELECT vi.*, vc.NAME vc_name 
                                        FROM bn_visitinformation vi 
                                        LEFT JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID ")->num_rows();


        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT vi.*, vc.NAME vc_name
                                        FROM bn_visitinformation vi 
                                        LEFT JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID 
                                      WHERE vi.CODE LIKE '" . $requestData['search']['value'] . "%' OR vi.NAME LIKE '" . $requestData['search']['value'] . "%' OR vc.NAME LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT vi.*, vc.NAME vc_name 
                                        FROM bn_visitinformation vi 
                                        LEFT JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = vi.VISIT_CLASSIFICATIONID 
                                        ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $status = ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" id="' . $row->VISIT_INFO_ID . '" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>';
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->CODE;
            $nestedData[] = $row->NAME;
            $nestedData[] = $row->vc_name;
            $nestedData[] = $row->DESCR;
            $nestedData[] =
                    '<a class="itemStatus" data-sn="' . $sn . '" id="' . $row->VISIT_INFO_ID . '" data-su-url="' . ('setup/visitinformation/viById/' . $sn) . '" data-status="' . $row->ACTIVE_STATUS . '" data-fieldId="VISIT_INFO_ID" data-field="ACTIVE_STATUS" data-tbl="bn_visitinformation"  title="Click For status change">' . $status . '</a> ' .
                    '<a class="btn btn-success btn-xs modalLink" href="' . site_url('setup/visitinformation/view/' . $row->VISIT_INFO_ID) . '" title="Visit Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs modalLink cm" href="' . site_url('setup/visitinformation/edit/' . $row->VISIT_INFO_ID) . '" title="Edit Information" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->VISIT_INFO_ID . '" sn="' . $sn . '" title="Click For Delete" data-type="delete" data-field="VISIT_INFO_ID" data-tbl="bn_visitinformation"><span class="glyphicon glyphicon-trash"></span></a>';

            $data[] = $nestedData;
            $sn++;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

