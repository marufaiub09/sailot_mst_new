<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NavyExamName extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Name Setup';
        $data['result'] =$this->db->query("SELECT nh.*, type.NAME type, level.NAME level
											FROM bn_navyexam_hierarchy nh 
											INNER JOIN bn_navyexam_hierarchy type on type.EXAM_ID = nh.PARENT_ID
											INNER JOIN bn_navyexam_hierarchy level on level.EXAM_ID = type.PARENT_ID
											WHERE nh.EXAM_TYPE = 3")->result();
        $data['content_view_page'] = 'setup/navy_exam_name/index';
        $this->template->display($data);
    }

   /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
	   $data['examType'] = $this->utilities->findAllByAttribute("bn_navyexam_hierarchy", array("EXAM_TYPE" => 2));
	   $this->load->view('setup/navy_exam_name/create',$data);
    }

     /**
     * @access      public
     * @param       none
     * @author      Reazul slam <reazul@atilimited.net>
     * @return      View modal
     */

  public function save()
    {
        $code= $this->input->post('code', true);
        $name= $this->input->post('name', true);
        $bn_name= $this->input->post('bn_name', true);
        $parentid = $this->input->post('exam_type', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyexam_hierarchy", array("NAME" => $name, "CODE" => $code, 'PARENT_ID' => $parentid));
        if (empty($check)) {// if exam name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'PARENT_ID' => $parentid,
                'EXAM_TYPE' =>3,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navyexam_hierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Name Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Exam Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <emdadul@atilimited.net>
     * @return      single row
     */
   function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT nh.*, type.NAME type, level.NAME level
											FROM bn_navyexam_hierarchy nh 
											INNER JOIN bn_navyexam_hierarchy type on type.EXAM_ID = nh.PARENT_ID
											INNER JOIN bn_navyexam_hierarchy level on level.EXAM_ID = type.PARENT_ID
											WHERE nh.EXAM_ID =$id")->row();
        $this->load->view('setup/navy_exam_name/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <emdadul@atilimited.net>
     * @return      
     */

    function examNameList() {
        $data['result'] = $this->db->query("SELECT nh.*, type.NAME type, level.NAME level
											FROM bn_navyexam_hierarchy nh 
											INNER JOIN bn_navyexam_hierarchy type on type.EXAM_ID = nh.PARENT_ID
											INNER JOIN bn_navyexam_hierarchy level on level.EXAM_ID = type.PARENT_ID
											WHERE nh.EXAM_TYPE = 3")->result();
        $this->load->view("setup/navy_exam_name/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <emdadul@atilimited.net>
     * @return      templete
     */
  public function edit($id)
    { 
        $data['examType'] = $this->utilities->findAllByAttribute("bn_navyexam_hierarchy", array("EXAM_TYPE" => 2));
        $data['result'] = $this->utilities->findByAttribute('bn_navyexam_hierarchy', array('EXAM_ID' => $id));
        $this->load->view('setup/navy_exam_name/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updatePart()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $bn_name= $this->input->post('bn_name', true);
        $code= $this->input->post('code', true);
        $parentid = $this->input->post('exam_type', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyexam_hierarchy", array("NAME" => $name,  "CODE" => $code, "EXAM_ID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'PARENT_ID' => $parentid,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navyexam_hierarchy',$data, array("EXAM_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Name Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Exam Name Already Exist</div>";
        }
    }
}

/* End of file navyExamName.php */
/* Location: ./application/controllers/setup/navyExamName.php */