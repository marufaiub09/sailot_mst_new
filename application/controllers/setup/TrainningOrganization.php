<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TrainningOrganization extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Types Setup';
        $data['result'] =$this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_country bn WHERE bn.COUNTRY_ID = nh.Country_ID) BN_NAME
        FROM bn_trainingorganization nh")->result();
        $data['content_view_page'] = 'setup/TrainningOrganization/index';
        $this->template->display($data);
    }

   /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
   $data['country'] = $this->db->query("SELECT * FROM `bn_country` WHERE ACTIVE_STATUS = 1")->result();
   $this->load->view('setup/TrainningOrganization/create',$data);
    }

     /**
     * @access      public
     * @param       none
     * @author      Reazul slam <reazul@atilimited.net>
     * @return      View modal
     */

  public function savePart()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $country = $this->input->post('country', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_trainingorganization", array("Name" => $name, "Code" => $code));
        if (empty($check)) {// if part name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'COUNTRY_ID' => $country,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_trainingorganization')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Organization Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Organization insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Training Organization Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
   function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] =$this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_country bn WHERE bn.COUNTRY_ID = nh.Country_ID) BN_NAME
        FROM bn_trainingorganization nh WHERE nh.TrainingOrganizationID =$id")->row();
        $this->load->view('setup/TrainningOrganization/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */

    function partList() {
       $data['result'] =$this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_country bn WHERE bn.COUNTRY_ID = nh.Country_ID) BN_NAME
        FROM bn_trainingorganization nh ")->result();
        $this->load->view("setup/TrainningOrganization/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
  public function edit($id)
    { 
  $data['result'] = $this->utilities->findByAttribute('bn_trainingorganization', array('TrainingOrganizationID' => $id));
  $data['country'] = $this->utilities->findAllByAttribute("bn_country", array("ACTIVE_STATUS" => 1));

     
  $this->load->view('setup/TrainningOrganization/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updatePart()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $country = $this->input->post('TRAINING_ORG_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_trainingorganization", array("Name" => $name,  "Code" => $code, "TrainingOrganizationID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'COUNTRY_ID' => $country,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_trainingorganization',$data, array("TrainingOrganizationID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Organization Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Organization Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Training Organization Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
 $data['viewdetails'] =$this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_country bn WHERE bn.COUNTRY_ID = nh.Country_ID) BN_NAME
 FROM bn_trainingorganization nh WHERE nh.TrainingOrganizationID =$id")->row();   
 $this->load->view('setup/TrainningOrganization/view',$data);
    }

}
