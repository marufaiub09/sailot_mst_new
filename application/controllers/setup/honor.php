<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Honor extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Honor Setup';
        $data['result'] = $this->utilities->getAll("bn_honor");
        $data['content_view_page'] = 'setup/honor/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/honor/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveHonor()
    {        
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $sh_name= $this->input->post('sh_name', true);     
        $desc = $this->input->post('desc', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_honor", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'SHORT_NAME' => $sh_name,
                'DESCR' => $desc,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_honor')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Honor Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Honor Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Honor Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function honorById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_honor", array("HONOR_ID" => $id));
        $this->load->view('setup/honor/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function honorList() {
        $data['result'] = $this->utilities->getAll("bn_honor");
        $this->load->view("setup/honor/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_honor', array('HONOR_ID' => $id));
        $this->load->view('setup/honor/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateHonor()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $sh_name= $this->input->post('sh_name', true);     
        $desc = $this->input->post('desc', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_honor", array("NAME" => $name, "HONOR_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'SHORT_NAME' => $sh_name,
                'DESCR' => $desc,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_honor',$data, array("HONOR_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Honor Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Honor Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Honor Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_honor', array('HONOR_ID' => $id));
        $this->load->view('setup/honor/view',$data);
    }

}
