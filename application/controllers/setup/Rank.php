<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Rank extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return  //Load All Rank Details From Rank Table
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Rank Setup';
        $data['rank_list'] = $this->db->query("SELECT br.*, bb.BRANCH_NAME, er.RANK_NAME EQUIVALANT_RANK
                                                FROM bn_rank br 
                                                INNER JOIN bn_branch bb on bb.BRANCH_ID = br.BRANCH_ID
                                                INNER JOIN bn_equivalent_rank er on er.EQUIVALANT_RANKID = br.EQUIVALANT_RANKID")->result();
        $data['content_view_page'] = 'setup/rank/index';
        $this->template->display($data);
    }

    /*
     * @methodName Create()
     * @access
     * @param  none
     * @return  //
     */

    public function create() {
        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy('bn_branch', array("ACTIVE_STATUS" => 1),"BRANCH_NAME");
        $data['equivalent_rank'] = $this->utilities->findAllByAttributeWithOrderBy('bn_equivalent_rank', array("ACTIVE_STATUS" => 1),"RANK_NAME");
        $this->load->view('setup/rank/create', $data);
    }

    /*
     * @access
     * @param  rank _id
     * @return  //
     */

    function saveRank() {
        $code = $this->input->post('code', true);
        $rank_name = $this->input->post('rank_name', true);
        $rank_name_bn = $this->input->post('rank_name_bn', true);
        $BRANCH_ID = $this->input->post('BRANCH_ID', true);
        $equivalentRank_ID = $this->input->post('equivalentRank_ID', true);
        $SERVICE_LIMIT = $this->input->post('SERVICE_LIMIT', true);
        $AGE_LIMIT = $this->input->post('AGE_LIMIT', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if trade with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_rank", array("RANK_NAME" => $rank_name, "EQUIVALANT_RANKID" => $equivalentRank_ID));
        if (empty($check)) {// if trade name available
            $data = array(
                'RANK_CODE' => $code,
                'RANK_NAME' => $rank_name,
                'RANK_BANGLA_NAME' => $rank_name_bn,
                'BRANCH_ID' => $BRANCH_ID,
                'EQUIVALANT_RANKID' => $equivalentRank_ID,
                'POSITION' => '',
                'SERVICE_LIMIT' => $SERVICE_LIMIT,
                'AGE_LIMIT' => $AGE_LIMIT,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_rank')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Rank Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Rank Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Rank Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */
    function rankById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT br.*, bb.BRANCH_NAME, er.RANK_NAME EQUIVALANT_RANK
                                                FROM bn_rank br 
                                                INNER JOIN bn_branch bb on bb.BRANCH_ID = br.BRANCH_ID
                                                INNER JOIN bn_equivalent_rank er on er.EQUIVALANT_RANKID = br.EQUIVALANT_RANKID
                                                WHERE br.RANK_ID = $id ")->row();
        $this->load->view('setup/rank/single_rank_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */
    function rankList() {
        $data['rank_list'] = $this->db->query("SELECT br.*, bb.BRANCH_NAME, er.RANK_NAME EQUIVALANT_RANK
                                                FROM bn_rank br 
                                                INNER JOIN bn_branch bb on bb.BRANCH_ID = br.BRANCH_ID
                                                INNER JOIN bn_equivalent_rank er on er.EQUIVALANT_RANKID = br.EQUIVALANT_RANKID")->result();
        $this->load->view("setup/rank/rank_list", $data);
    }

    /*
     * @access
     * @param  rank _id
     * @return  //
     */

    public function editRank($id) {

        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy('bn_branch', array("ACTIVE_STATUS" => 1),"BRANCH_NAME");
        $data['equivalent_rank'] = $this->utilities->findAllByAttributeWithOrderBy('bn_equivalent_rank', array("ACTIVE_STATUS" => 1),"RANK_NAME");
        $data['result'] = $this->utilities->findByAttribute('bn_rank', array('RANK_ID' => $id));
        $this->load->view('setup/rank/edit', $data);
    }

    /*
     * @methodName viewRank()
     * @access
     * @param  rank _id
     * @return  //
     */

    function updateRank() {
        $id = $this->input->post('id', true);
        $code = $this->input->post('code', true);
        $rank_name = $this->input->post('rank_name', true);
        $rank_name_bn = $this->input->post('rank_name_bn', true);
        $BRANCH_ID = $this->input->post('BRANCH_ID', true);
        $equivalentRank_ID = $this->input->post('equivalentRank_ID', true);
        $SERVICE_LIMIT = $this->input->post('SERVICE_LIMIT', true);
        $AGE_LIMIT = $this->input->post('AGE_LIMIT', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if trade with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_rank", array("RANK_NAME" => $rank_name, "EQUIVALANT_RANKID" => $equivalentRank_ID, "RANK_ID !=" => $id));
        if (empty($check)) {// if trade name available
            $data = array(
                'RANK_CODE' => $code,
                'RANK_NAME' => $rank_name,
                'RANK_BANGLA_NAME' => $rank_name_bn,
                'BRANCH_ID' => $BRANCH_ID,
                'EQUIVALANT_RANKID' => $equivalentRank_ID,
                'SERVICE_LIMIT' => $SERVICE_LIMIT,
                'AGE_LIMIT' => $AGE_LIMIT,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DATE' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_rank', $data, array("RANK_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Rank Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Rank Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Rank Name Already Exist</div>";
        }
    }

    function ajaxRankSetupList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'br.RANK_ID', 1 => 'br.RANK_CODE', 2 => 'br.RANK_NAME', 3 => 'er.RANK_NAME', 4 => 'bb.BRANCH_NAME', 5 => 'br.AGE_LIMIT', 6 => 'br.SERVICE_LIMIT', 7 => 'br.RANK_ID');

        // getting total number records without any search

        $query = $this->db->query("SELECT br.*, bb.BRANCH_NAME, er.RANK_NAME EQUIVALANT_RANK
                                                FROM bn_rank br 
                                                INNER JOIN bn_branch bb on bb.BRANCH_ID = br.BRANCH_ID
                                                INNER JOIN bn_equivalent_rank er on er.EQUIVALANT_RANKID = br.EQUIVALANT_RANKID")->num_rows();


        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT br.*, bb.BRANCH_NAME, er.RANK_NAME EQUIVALANT_RANK
                                                FROM bn_rank br 
                                                INNER JOIN bn_branch bb on bb.BRANCH_ID = br.BRANCH_ID
                                                INNER JOIN bn_equivalent_rank er on er.EQUIVALANT_RANKID = br.EQUIVALANT_RANKID
                                      WHERE br.RANK_CODE LIKE '" . $requestData['search']['value'] . "%' OR br.RANK_NAME LIKE '" . $requestData['search']['value'] . "%' OR er.RANK_NAME LIKE '" . $requestData['search']['value'] . "%' OR bb.BRANCH_NAME LIKE '" . $requestData['search']['value'] . "%' OR br.AGE_LIMIT LIKE '" . $requestData['search']['value'] ."%' OR br.SERVICE_LIMIT LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT br.*, bb.BRANCH_NAME, er.RANK_NAME EQUIVALANT_RANK
                                                FROM bn_rank br 
                                                INNER JOIN bn_branch bb on bb.BRANCH_ID = br.BRANCH_ID
                                                INNER JOIN bn_equivalent_rank er on er.EQUIVALANT_RANKID = br.EQUIVALANT_RANKID
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $status = ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" id="'.$row->RANK_ID.'" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ;

            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->RANK_CODE;
            $nestedData[] = $row->RANK_NAME;
            $nestedData[] = $row->EQUIVALANT_RANK;
            $nestedData[] = $row->BRANCH_NAME;
            $nestedData[] = $row->AGE_LIMIT;
            $nestedData[] = $row->SERVICE_LIMIT;
            $nestedData[] =
                    '<a class="itemStatus" data-sn="'.$sn.'" id="'.$row->RANK_ID.'" data-su-url="'. ('setup/rank/rankById/'.$sn).'" data-status="'.$row->ACTIVE_STATUS.'" data-fieldId="RANK_ID" data-field="ACTIVE_STATUS" data-tbl="bn_rank"  title="Click For status change">'.$status.'</a>'.
                    '<a class="btn btn-warning btn-xs modalLink cm" href="' . site_url('setup/rank/editRank/' . $row->RANK_ID) . '" title="Edit Rank" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->RANK_ID . '" sn="' . $sn. '" title="Click For Delete" data-type="delete" data-field="RANK_ID" data-tbl="bn_rank"><span class="glyphicon glyphicon-trash"></span></a>' ;
            $data[] = $nestedData;
            $sn++;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}
