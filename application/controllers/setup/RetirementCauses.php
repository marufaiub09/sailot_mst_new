<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RetirementCauses extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Erman Hossen <emran@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Retirement Causes';
        //$data['cause'] = $this->utilities->dropdownFromTableWithCondition('causetype', '-- Select Basic Entry --', 'CauseTypeID', 'Name');
        $data['content_view_page'] = 'setup/RetirementCauses/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      templete
     */
    public function searchingData() {
        $entryTypeId = $this->input->post("entryTypeVal", true);
        $releaseID = $this->input->post("selectReleaseVal", true);
        $entryTypeSql = "";
        $releaseSql = "";
        if ($entryTypeId != "") {
            $entryTypeSql = "WHERE Cause = $entryTypeId";
        }
        if ($releaseID != "") {
            $releaseSql = "AND subType = $releaseID";
        }
        $data['searchCauseType'] = $this->db->query("SELECT * FROM causetype $entryTypeSql $releaseSql ")->result();
        $this->load->view('setup/RetirementCauses/causeList', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      templete
     */
    public function create() {
        $this->load->view('setup/RetirementCauses/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      templete
     */
    public function save() {
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $basicEntryType = $this->input->post('basicEntryType', true);
        $releaseEntry = $this->input->post('releaseEntry', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("causetype", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'Cause' => $basicEntryType,
                'SubType' => $releaseEntry,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'causetype')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Retirement Causes Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Retirement Causes insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Retirement Causes Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      single row
     */
    function atById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("causetype", array("CauseTypeID" => $id));
        $this->load->view('setup/RetirementCauses/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      
     */
    function atList() {
        $data['result'] = $this->utilities->getAll("causetype");
        $this->load->view("setup/RetirementCauses/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Emran Hossen<Emran@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
          $data['result'] = $this->db->query("SELECT c.CauseTypeID,
                                                c.Code,
                                                c.Name,
                                                c.Cause,
                                                (select CASE c.Cause WHEN 1 THEN 'Release' WHEN 2 THEN
                                                'Nominee' WHEN 3 THEN 'ReIsseu Pension' WHEN 4 THEN
                                                'Suspension Pension' WHEN 5 THEN 'Benefits' WHEN 6 THEN 'Pension Book' ELSE NULL END ) AS CauseName,
                                                c.SubType,
                                                (select CASE c.SubType WHEN 1 THEN 'Release' WHEN 2 THEN
                                                'Discharge' WHEN 3 THEN 'Dismissed' ELSE NULL END ) AS SubTypeName,
                                                c.ACTIVE_STATUS,
                                                c.CRE_BY,
                                                c.CRE_DT,
                                                c.UPD_BY,
                                                c.UPD_DT
                                           FROM causetype c
                                           where c.CauseTypeID = $id")->row();
        $this->load->view('setup/RetirementCauses/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function update() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $basicEntryType = $this->input->post('basicTypeEntry', true);
        $releaseEntry = $this->input->post('releaseEntry', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("causetype", array("NAME" => $name, "CauseTypeID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'Cause' => $basicEntryType,
                'SubType' => $releaseEntry,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('causetype', $data, array("CauseTypeID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Retirement Causes Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Retirement Causes Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Retirement Causes  Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT c.CauseTypeID,
                                                c.Code,
                                                c.Name,
                                                (select CASE c.Cause WHEN 1 THEN 'Release' WHEN 2 THEN
                                                'Nominee' WHEN 3 THEN 'ReIsseu Pension' WHEN 4 THEN
                                                'Suspension Pension' WHEN 5 THEN 'Benefits' WHEN 6 THEN 'Pension Book' ELSE NULL END ) AS Cause,
                                                (select CASE c.SubType WHEN 1 THEN 'Release' WHEN 2 THEN
                                                'Discharge' WHEN 3 THEN 'Dismissed' ELSE NULL END ) AS SubType,
                                                c.ACTIVE_STATUS,
                                                c.CRE_BY,
                                                c.CRE_DT,
                                                c.UPD_BY,
                                                c.UPD_DT
                                           FROM causetype c
                                           where c.CauseTypeID = $id")->row();
        $this->load->view('setup/RetirementCauses/view', $data);
    }

}
