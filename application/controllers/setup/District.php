<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class district extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'District Setup';
        $data['district'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE BDADMIN_TYPE = 2 ")->result();
        $data['content_view_page'] = 'setup/district/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['division'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE ACTIVE_STATUS = 1 AND PARENT_ID IS NULL ")->result();
        $this->load->view('setup/district/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveDistrict()
    {
        $PARENT_ID= $this->input->post('DIVISION_ID', true);
        $name= $this->input->post('district_name', true);
        $code= $this->input->post('district_code', true);
        $bn_name= $this->input->post('district_name_bn', true);
        $short_name= $this->input->post('bd_short_name', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_bdadminhierarchy", array("NAME" => $name, "CODE" => $code));
        if (empty($check)) {// if district name available
            $data = array(
                'PARENT_ID' => $PARENT_ID,
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $short_name,
                'BDADMIN_TYPE' => 2,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_bdadminhierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>District Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>District Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>District Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function districtById($sn) { $id = $this->input->post('param'); // id
    $data['sn'] = $sn; $data['row'] =
    $this->utilities->findByAttribute('bn_bdadminhierarchy', array('BD_ADMINID'
    => $id)); // select single data select
    $this->load->view('setup/district/single_district_row', $data); } /**  *
    @access  public  * @param   none  * @author  Emdadul Huq
    <Emdadul@atilimited.net>  * @return    */

    function districtList() {
        $data['district'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE BDADMIN_TYPE = 2")->result();
        $this->load->view("setup/district/district_list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_bdadminhierarchy', array('BD_ADMINID' => $id));
        $data['division'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE ACTIVE_STATUS = 1 AND PARENT_ID IS NULL ")->result();
        $this->load->view('setup/district/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updatedistrict()
    {
        $PARENT_ID= $this->input->post('DIVISION_ID', true);
        $id= $this->input->post('district_id', true);
        $name= $this->input->post('district_name', true);
        $code= $this->input->post('district_code', true);
        $bn_name= $this->input->post('district_name_bn', true);
        $short_name= $this->input->post('bd_short_name', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_bdadminhierarchy", array("NAME" => $name,  "CODE" => $code, "BD_ADMINID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'PARENT_ID' => $PARENT_ID,
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $short_name,
                'BDADMIN_TYPE' => 2,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_bdadminhierarchy',$data, array("BD_ADMINID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>District Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>District Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>District Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT pr.*, (SELECT name FROM bn_bdadminhierarchy WHERE BD_ADMINID = pr.PARENT_ID)Division
                                                    FROM `bn_bdadminhierarchy` pr
                                                    WHERE pr.BD_ADMINID = $id ")->row();
        $this->load->view('setup/district/view',$data);
    }

}
