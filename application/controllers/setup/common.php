<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

    /**
     * @access      public
     * @param       id, table_name, colomn name, column id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Y/N
     */
    public function deleteItem() {
        $item_id = $this->input->post('item_id'); //
        $data_tbl = $this->input->post('data_tbl'); // table name
        $data_field = $this->input->post('data_field'); // column name
        $attribute = array(
            "$data_field" => $item_id
        );
        $result = $this->utilities->deleteRowByAttribute($data_tbl, $attribute);
        if ($result == TRUE) {
            echo "Y";
        } else {
            echo "N";
        }
    }
    /**
     * @access      public
     * @param       id, table_name, colomn name, column id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Y/N
     */

    function statusItem() {
        $item_id = $this->input->post('item_id'); // id
        $status = $this->input->post('status'); // current status
        $data_tbl = $this->input->post('data_tbl'); // table name
        $data_field = $this->input->post('data_field'); // column name
        $data_fieldId = $this->input->post('data_fieldId'); // table column ID
        if ($status == 1) {
            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $update_status = array(
            "$data_field" => $new_status
        );
        if ($this->utilities->updateData($data_tbl, $update_status, array("$data_fieldId" => $item_id))) {
            echo "Y";
        } else {
            echo "N";
        }
    }
    /**
     * @access      public
     * @param       officiar number
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Sailor Info, Rank_name, with last year assessment
     */
    function searchSailor(){
        $officalNumber = $this->input->post("officeNumber");
        $sailorStatus = $this->input->post("sailorStatus");
        $this->db->select('s.*, se.NAME SHIP_ESTABLISHMENT, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME,at.CharacterType,at.EfficiencyType,st.NAME SHIPID, DATE_FORMAT(s.POSTINGDATE,"%d-%m-%Y") POSTING_DATE');
        $this->db->from('sailor as s');
        $this->db->join('bn_posting_unit as pu', 'pu.POSTING_UNITID = s.POSTINGUNITID','INNER');
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID','INNER');
        $this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID', 'INNER');
        $this->db->join('assessment as at', 'at.SailorID = s.SAILORID','at.YEAR(CRE_DT) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))', 'INNER');
        $this->db->join('bn_ship_establishment as st', 'st.SHIP_ESTABLISHMENTID = at.ShipEstablishmentID','at.YEAR(CRE_DT) = YEAR(DATE_SUB(CURDATE(), INTERVAL 1 YEAR))', 'INNER');

        $condition = array('s.OFFICIALNUMBER'=> $officalNumber, 's.SAILORSTATUS'=> $sailorStatus);
        $this->db->where($condition);
        echo json_encode($this->db->get()->row_array());
    }
    /**
     * @access      public
     * @param       officiar number
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Sailor Info, Rank_name, Ship_establishment, Posting unit, date
     */
    function searchSailorInfoByOfficalNo(){
        $officalNumber = $this->input->post("officeNumber");
        $sailorStatus = $this->input->post("sailorStatus");

        $this->db->select('s.*,et.NAME ENTRY_TYPE_NAME, r.RANK_NAME, se.NAME SHIP_ESTABLISHMENT, pu.NAME POSTING_UNIT_NAME,s.ISACTING,s.ISNOTQUALIFIED,DATE_FORMAT(s.POSTINGDATE,"%d-%m-%Y") POSTING_DATE, DATE_FORMAT(s.SENIORITYDATE,"%d-%m-%Y") SENIORITY_DATE, DATE_FORMAT(s.PROMOTIONDATE,"%d-%m-%Y") PROMOTION_DATE');
        $this->db->from('sailor as s');
        $this->db->join('bn_posting_unit as pu', 'pu.POSTING_UNITID = s.POSTINGUNITID','INNER');
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID','INNER');
        $this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID', 'INNER');
        $this->db->join('bn_entrytype as et', 'et.ENTRY_TYPEID = s.ENTRYTYPEID', 'INNER');
        $condition = array('s.OFFICIALNUMBER'=> $officalNumber, 's.SAILORSTATUS'=> $sailorStatus);
        $this->db->where($condition);
        echo json_encode($this->db->get()->row_array());
    }
    /**
     * @param       Officical Number
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Yes/No
     */
    public function checkOfficialNo()
    {
        $no = $this->input->post('officialNo');
        $sailorStatus = $this->input->post("sailorStatus");
        $officialNo = $this->utilities->findByAttribute("sailor", array("OFFICIALNUMBER" => $no, 'SAILORSTATUS'=> $sailorStatus));
        if(!empty($officialNo)){
            echo 'Y';     //Y means Yes
        }else{
            echo 'N';     // N means No
        }
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax Visit information by visitClass ID
     */
    function visitInfo_by_visitClass_id() {
        $VISIT_CLASS_ID = $_POST['VISIT_CLASS_ID'];
        $query = $this->utilities->findAllByAttribute('bn_visitinformation', array("VISIT_CLASSIFICATIONID" => $VISIT_CLASS_ID, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->VISIT_INFO_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax Visit information by visitClass ID
     */
    function visitInfo_by_visitClass_search() {
        $VISIT_CLASS_ID = $_POST['VISIT_CLASS_ID'];
        $query = $this->utilities->findAllByAttribute('bn_visitinformation', array("VISIT_CLASSIFICATIONID" => $VISIT_CLASS_ID, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->VISIT_INFO_ID . '">' . $row->NAME . '</option>';
            }
        }else{
            $query = $this->utilities->findAllByAttribute('bn_visitinformation', array("ACTIVE_STATUS" => 1));
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->VISIT_INFO_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access   public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax Training Name by Training Type
     */
    function trainingName_by_trainingType() {
        $TRAINING_TYPE = $_POST['TRAINING_TYPE'];
        $query = $this->utilities->findAllByAttribute('bn_navyexam_hierarchy', array("PARENT_ID" => $TRAINING_TYPE, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->EXAM_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    public function ajax_get_division() {
        $division_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_bdadminhierarchy', array('PARENT_ID' => $division_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->BD_ADMINID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    public function ajax_get_district() {
    $district_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_bdadminhierarchy', array('PARENT_ID' => $district_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->BD_ADMINID . '">' . $row->NAME . '</option>';
            }
            echo $returnVal;
        }
    }
    public function ajax_get_thana() {
        $thana_id = $_POST['selectedValue'];
        $query = $this->db->get_where('sa_post_offices', array('POST_OFFICE_ID' => $post_id))->result();
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal = '<option value="' . $row->POST_OFFICE_ID . '">' . $row->POST_CODE . '</option>';
            }
            echo $returnVal;
        }
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax ExamName/Test by examType ID
     */
    function examName_by_examType() {
        $EXAM_TYPE_ID = $_POST['EXAM_TYPE_ID'];
        $query = $this->utilities->findAllByAttribute('bn_navyexam_hierarchy', array("ACTIVE_STATUS" => 1, "PARENT_ID" => $EXAM_TYPE_ID));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->EXAM_ID . '">' .  "[".$row->CODE."] ".$row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax Authority area by authoirty zone
     */
    function authorityArea_by_authorityZone() {
        $AUTHORITY_ZONE_ID = $_POST['AUTHORITY_ZONE_ID'];
        $query = $this->utilities->findAllByAttribute('bn_navyadminhierarchy', array("ACTIVE_STATUS" => 1, "PARENT_ID" => $AUTHORITY_ZONE_ID));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->ADMIN_ID . '">' .  "[".$row->CODE."] ".$row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax ship/establishment by area
     */
    function shipEstablishment_by_area() {
        $SHIP_AREA_ID = $_POST['SHIP_AREA_ID'];
        $query = $this->utilities->findAllByAttribute('bn_ship_establishment', array("ACTIVE_STATUS" => 1, "AREA_ID" => $SHIP_AREA_ID));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->SHIP_ESTABLISHMENTID . '">' .  "[".$row->CODE."] ".$row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax Training Course Name by Training Course  Type
     */
    function trainingCourseName_by_trainingCourseType() {
        $PARENT_ID = $_POST['TRAINING_TYPE'];
        $query = $this->utilities->findAllByAttribute('bn_navytraininghierarchy', array("ACTIVE_STATUS" => 1, "ParentID" => $PARENT_ID));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->NAVYTrainingID . '">' .  "[".$row->Code."] ".$row->Name . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   ajax posting Unit by ship/establishment  id
     */
    function postingUnit_by_shipEstablishment() {
        $shipEst_ID = $_POST['shipEst_ID'];
        $query = $this->utilities->findAllByAttribute('bn_posting_unit', array("ACTIVE_STATUS" => 1, "SHIP_ESTABLISHMENTID" => $shipEst_ID));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->POSTING_UNITID . '">' .  "[".$row->CODE."] ".$row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access    public
     * @param
     * @author   Emdadul Huq <Emdadul@atilimited.net>
     * @return   rank Name by branch id
     */
    function rankName_by_branch() {
        $branchId = $_POST['branchId'];
        $query = $this->utilities->findAllByAttributeWithOrderBy('bn_rank', array("ACTIVE_STATUS" => 1, "BRANCH_ID" => $branchId), "POSITION");
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->RANK_ID . '">' . $row->RANK_NAME . '</option>';
            }
        }
        echo $returnVal;
    }

}

/* End of file common.php */
/* Location: ./application/controllers/setup/common.php */
