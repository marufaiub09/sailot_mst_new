<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Thana extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Thana Setup';
        $data['thana'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE BDADMIN_TYPE = 3 ")->result();
        $data['content_view_page'] = 'setup/thana/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['district'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE ACTIVE_STATUS = 1 AND BDADMIN_TYPE = 2")->result();
        $this->load->view('setup/thana/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function saveThana() {
        $PARENT_ID = $this->input->post('DIVISION_ID', true);
        $name = $this->input->post('district_name', true);
        $code = $this->input->post('district_code', true);
        $bn_name = $this->input->post('district_name_bn', true);
        $short_name = $this->input->post('bd_short_name', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_bdadminhierarchy", array("NAME" => $name, "CODE" => $code));
        if (empty($check)) {// if district name available
            $data = array(
                'PARENT_ID' => $PARENT_ID,
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $short_name,
                'BDADMIN_TYPE' => 3,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_bdadminhierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Thana Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Thana Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Thana Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */
    function thanaById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute('bn_bdadminhierarchy', array('BD_ADMINID' => $id)); // select single data select
        $this->load->view('setup/thana/single_thana_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */
    function thanaList() {
        $data['thana'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE BDADMIN_TYPE = 3")->result();
        $this->load->view("setup/thana/thana_list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_bdadminhierarchy', array('BD_ADMINID' => $id));
        $data['district'] = $this->db->query("SELECT * FROM `bn_bdadminhierarchy` WHERE ACTIVE_STATUS = 1 AND BDADMIN_TYPE = 2")->result();
        $this->load->view('setup/thana/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateThana() {
        $PARENT_ID = $this->input->post('DIVISION_ID', true);
        $id = $this->input->post('district_id', true);
        $name = $this->input->post('district_name', true);
        $code = $this->input->post('district_code', true);
        $bn_name = $this->input->post('district_name_bn', true);
        $short_name = $this->input->post('bd_short_name', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_bdadminhierarchy", array("NAME" => $name, "CODE" => $code, "BD_ADMINID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'PARENT_ID' => $PARENT_ID,
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'SHORT_NAME' => $short_name,
                'BDADMIN_TYPE' => 3,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_bdadminhierarchy', $data, array("BD_ADMINID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Thana Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Thana Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Thana Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT pr.*, (SELECT name FROM bn_bdadminhierarchy WHERE BD_ADMINID = pr.PARENT_ID)District
                                                    FROM `bn_bdadminhierarchy` pr
                                                    WHERE pr.BD_ADMINID = $id ")->row();
        $this->load->view('setup/thana/view', $data);
    }

    function ajaxThanaSetupList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'pr.BD_ADMINID', 1 => 'pr.CODE', 2 => 'pr.NAME', 3 => 'pr.BANGLA_NAME', 4 => 'pr.SHORT_NAME', 5 => 'pr.BD_ADMINID');

        // getting total number records without any search

        $query = $this->db->query("SELECT pr.*, ad.NAME District 
                                        FROM bn_bdadminhierarchy pr 
                                        LEFT JOIN bn_bdadminhierarchy ad on ad.BD_ADMINID = pr.PARENT_ID")->num_rows();


        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT pr.*, ad.NAME District 
                                        FROM bn_bdadminhierarchy pr 
                                        LEFT JOIN bn_bdadminhierarchy ad on ad.BD_ADMINID = pr.PARENT_ID
                                      WHERE pr.CODE LIKE '" . $requestData['search']['value'] . "%' OR pr.NAME LIKE '" . $requestData['search']['value'] . "%' OR pr.NAME LIKE '" . $requestData['search']['value'] . $requestData['search']['value'] . "%' OR pr.SHORT_NAME LIKE '" . $requestData['search']['value'] . "%' OR pr.BANGLA_NAME LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT pr.*, ad.NAME District 
                                        FROM bn_bdadminhierarchy pr 
                                        LEFT JOIN bn_bdadminhierarchy ad on ad.BD_ADMINID = pr.PARENT_ID
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $status = ($row->ACTIVE_STATUS == 1) ? '<span class="label label-success" id="'.$row->BD_ADMINID.'" title="Click For Inactive">Inactive</span>' : '<span class="label label-danger" title="Click For Active">Active</span>' ;
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->CODE;
            $nestedData[] = $row->NAME;
            $nestedData[] = $row->BANGLA_NAME;
            $nestedData[] = $row->SHORT_NAME;
            $nestedData[] =
                    '<a class="itemStatus" data-sn="' . $sn . '" id="' . $row->BD_ADMINID . '" data-su-url="' . ('setup/thana/thanaById/' . $sn) . '" data-status="' . $row->ACTIVE_STATUS . '" data-fieldId="BD_ADMINID" data-field="ACTIVE_STATUS" data-tbl="bn_bdadminhierarchy"  title="Click For status change">' . $status . '</a> ' .
                    '<a class="btn btn-success btn-xs modalLink" href="' . site_url('setup/thana/view/' . $row->BD_ADMINID) . '" title="View Thana" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs modalLink cm" href="' . site_url('setup/thana/edit/' . $row->BD_ADMINID) . '" title="Edit Thana" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->BD_ADMINID . '" sn="' . $sn . '" title="Click For Delete" data-type="delete" data-field="BD_ADMINID" data-tbl="bn_bdadminhierarchy"><span class="glyphicon glyphicon-trash"></span></a>';

            $data[] = $nestedData;
            $sn++;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

