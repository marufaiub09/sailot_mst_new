<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trade extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Trade Setup';
        $data['result'] = $this->utilities->findAllByAttributeWithJoin("bn_trade","bn_branch","BRANCH_ID", "BRANCH_ID","BRANCH_NAME", "", "INNER");
        $data['content_view_page'] = 'setup/trade/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy("bn_branch", array("ACTIVE_STATUS" => 1),"BRANCH_NAME");
        $this->load->view('setup/trade/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveTrade()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $bn_name= $this->input->post('BRANCH_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if trade with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_trade", array("NAME" => $name, "CODE" => $code));
        if (empty($check)) {// if trade name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BRANCH_ID' => $bn_name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_trade')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Trade Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Trade Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Trade Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function TradeById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttributeWithJoin("bn_trade","bn_branch","BRANCH_ID", "BRANCH_ID","BRANCH_NAME", array("TRADE_ID" => $id), "INNER"); // select single data select
        $this->load->view('setup/trade/single_trade_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function tradeList() {
        $data['result'] = $this->utilities->findAllByAttributeWithJoin("bn_trade","bn_branch","BRANCH_ID", "BRANCH_ID","BRANCH_NAME", "", "INNER");
        $this->load->view("setup/trade/trade_list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_trade', array('TRADE_ID' => $id));
        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy("bn_branch", array("ACTIVE_STATUS" => 1),"BRANCH_NAME");
        $this->load->view('setup/trade/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateTrade()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $bn_name= $this->input->post('BRANCH_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_trade", array("NAME" => $name,  "CODE" => $code, "TRADE_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BRANCH_ID' => $bn_name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_trade',$data, array("TRADE_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Trade Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Trade Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Trade Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttributeWithJoin("bn_trade","bn_branch","BRANCH_ID", "BRANCH_ID","BRANCH_NAME", array("TRADE_ID" => $id), "INNER");
        $this->load->view('setup/trade/view',$data);
    }

}
