<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BatchNumber extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Batch Number Setup';
        $data['result'] = $this->db->query("SELECT * FROM bn_batchnumber ORDER BY CRE_DT DESC")->result();
        $data['content_view_page'] = 'setup/batchNumber/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/batchNumber/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveBatchNumber()
    {        
        $batchNumber= $this->input->post('batchNumber', true);
        $yearOfBatch= $this->input->post('yearOfBatch', true);    
        $IS_PROCESS = (isset($_POST['IS_PROCESS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_batchnumber", array( 'BATCH_NUMBER' => $batchNumber,));
        if (empty($check)) {// if Batch number available
            $data = array(
                'BATCH_NUMBER' => $batchNumber,
                'YEAR_OF_BATCH' => $yearOfBatch,
                'IS_PROCESS' => $IS_PROCESS,
                'ACTIVE_STATUS' => 1,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if($IS_PROCESS == 1){
                $this->db->query("UPDATE bn_batchnumber set IS_PROCESS = 0");
            }
            if ($this->utilities->insertData($data, 'bn_batchnumber')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Batch Number Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Batch Number  insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Batch Number  Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function batchNumberById($sn) {
        $id = $this->input->post('param'); // id`
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_batchnumber", array("BATCH_ID" => $id));
        $this->load->view('setup/batchNumber/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function batchNumberList() {
        $data['result'] = $this->db->query("SELECT * FROM bn_batchnumber ORDER BY CRE_DT DESC")->result();
        $this->load->view("setup/batchNumber/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_batchnumber', array('BATCH_ID' => $id));
        $this->load->view('setup/batchNumber/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateBatchNumber()
    {
        $id= $this->input->post('id', true);
        $batchNumber= $this->input->post('batchNumber', true);
        $yearOfBatch= $this->input->post('yearOfBatch', true);    
        $IS_PROCESS = (isset($_POST['IS_PROCESS'])) ? 1 : 0;
        // checking if branch number with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_batchnumber", array("BATCH_NUMBER" => $batchNumber, 'YEAR_OF_BATCH' => $yearOfBatch, "BATCH_ID !=" => $id));
        if (empty($check)) {// if branch number name available
            $data = array(
                'BATCH_NUMBER' => $batchNumber,
                'YEAR_OF_BATCH' => $yearOfBatch,
                'IS_PROCESS' => $IS_PROCESS,
                'ACTIVE_STATUS' => 1,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if($IS_PROCESS == 1){
                $this->db->query("UPDATE bn_batchnumber set IS_PROCESS = 0");
            }
            if ($this->utilities->updateData('bn_batchnumber',$data, array("BATCH_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Batch Number Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Batch Number Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Batch Number Already Exist</div>";
        }
    }


}
