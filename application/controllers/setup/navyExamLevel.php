<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class NavyExamLevel extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Setup';
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_navyexam_hierarchy bn WHERE bn.EXAM_ID = nh.PARENT_ID) BN_NAME
                                            FROM bn_navyexam_hierarchy nh WHERE nh.EXAM_TYPE = 1")->result();
        $data['content_view_page'] = 'setup/navyExamLevel/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('setup/navyExamLevel/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function savePart() {
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyexam_hierarchy", array("NAME" => $name));
        if (empty($check)) {// if part name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'PARENT_ID' => 0,
                'EXAM_TYPE' => 1,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navyexam_hierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Level Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Level insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Level Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute('bn_navyexam_hierarchy', array('EXAM_ID' => $id)); // select single data select
        $this->load->view('setup/navyExamLevel/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author     Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function partList() {
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_navyexam_hierarchy bn WHERE bn.EXAM_ID = nh.PARENT_ID) BN_NAME
                                            FROM bn_navyexam_hierarchy nh WHERE nh.EXAM_TYPE = 1")->result();
        $this->load->view("setup/navyExamLevel/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_navyexam_hierarchy', array('EXAM_ID' => $id));
        $this->load->view('setup/navyExamLevel/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePart() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyexam_hierarchy", array("NAME" => $name, "EXAM_ID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navyexam_hierarchy', $data, array("EXAM_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Level Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Level Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Level Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
}
