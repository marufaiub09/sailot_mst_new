<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Zone extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Zone Setup';
        $data['result'] = $this->db->query("SELECT na.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE oh.ORG_ID = na.ORG_TYPEID)Org_name FROM bn_navyadminhierarchy na WHERE na.PARENT_ID is null")->result();
        $data['content_view_page'] = 'setup/zone/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['orgName'] = $this->utilities->findAllByAttribute("bn_organization_hierarchy", array("ACTIVE_STATUS" => 1, "ORG_TYPE" => 1));
        $this->load->view('setup/zone/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveZone()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);    
        $ORG_ID = $this->input->post('ORG_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Zone with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyadminhierarchy", array("NAME" => $name, "CODE" => $code));
        if (empty($check)) {// if Zone name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'PARENT_ID' => null,
                'ADMIN_TYPE' => 1,
                'ORG_TYPEID' => $ORG_ID,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navyadminhierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Zone Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Zone insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Zone Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function zoneById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT na.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE oh.ORG_ID = na.ORG_TYPEID)Org_name FROM bn_navyadminhierarchy na WHERE na.ADMIN_ID = $id")->row();
        $this->load->view('setup/zone/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function zoneList() {
        $data['result'] = $this->db->query("SELECT na.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE oh.ORG_ID = na.ORG_TYPEID)Org_name FROM bn_navyadminhierarchy na WHERE na.PARENT_ID is null")->result();
        $this->load->view("setup/zone/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT na.*, (SELECT oh.ORG_NAME FROM bn_organization_hierarchy oh WHERE oh.ORG_ID = na.ORG_TYPEID)Org_name FROM bn_navyadminhierarchy na WHERE na.ADMIN_ID = $id")->row();
        $data['orgName'] = $this->utilities->findAllByAttribute("bn_organization_hierarchy", array("ACTIVE_STATUS" => 1, "ORG_TYPE" => 1));
        $this->load->view('setup/zone/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateZone()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $ORG_ID = $this->input->post('ORG_ID', true);         
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyadminhierarchy", array("NAME" => $name,  "CODE" => $code, "ADMIN_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'PARENT_ID' => null,
                'ORG_TYPEID' => $ORG_ID,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navyadminhierarchy',$data, array("ADMIN_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Zone Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Zone Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Zone Name Already Exist</div>";
        }
    }


}
