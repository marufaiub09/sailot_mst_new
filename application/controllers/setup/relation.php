<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Relation extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Relation Setup';
        $data['result'] = $this->utilities->getAll("bn_relation");
        $data['content_view_page'] = 'setup/relation/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/relation/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveRelation()
    {        
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $bn_name= $this->input->post('bn_name', true);     
        $desc = $this->input->post('desc', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_relation", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_relation')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Relation Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Relation Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Relation Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function relationById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_relation", array("RELATION_ID" => $id));
        $this->load->view('setup/relation/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function RelationList() {
        $data['result'] = $this->utilities->getAll("bn_relation");
        $this->load->view("setup/relation/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_relation', array('RELATION_ID' => $id));
        $this->load->view('setup/relation/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateRelation()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $bn_name= $this->input->post('bn_name', true);     
        $desc = $this->input->post('desc', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_relation", array("NAME" => $name, "RELATION_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_relation',$data, array("RELATION_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Relation Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Relation Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Relation Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_relation', array('RELATION_ID' => $id));
        $this->load->view('setup/relation/view',$data);
    }

}
