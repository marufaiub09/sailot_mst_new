<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OffenceCatagory extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Language Setup';
        $data['result'] = $this->utilities->getAll("bn_offencecatagory");
        $data['content_view_page'] = 'setup/offence_catagory/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/offence_catagory/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveOC()
    {        
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);    
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_offencecatagory", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_offencecatagory')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Offence catagory Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Offence catagory insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Offence catagory Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function ocById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_offencecatagory", array("OFFENCE_CATAGORYID" => $id));
        $this->load->view('setup/offence_catagory/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function ocList() {
        $data['result'] = $this->utilities->getAll("bn_offencecatagory");
        $this->load->view("setup/offence_catagory/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_offencecatagory', array('OFFENCE_CATAGORYID' => $id));
        $this->load->view('setup/offence_catagory/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateOC()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);   
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_offencecatagory", array("NAME" => $name, "OFFENCE_CATAGORYID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_offencecatagory',$data, array("OFFENCE_CATAGORYID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Offence catagory Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Offence catagory Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Offence catagory  Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_offencecatagory', array('OFFENCE_CATAGORYID' => $id));
        $this->load->view('setup/offence_catagory/view',$data);
    }

}
