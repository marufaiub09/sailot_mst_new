<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ExamResult extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Result';
        $data['result'] = $this->utilities->getAll("bn_exam_result");
        $data['content_view_page'] = 'setup/ExamResult/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('setup/ExamResult/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function saveExamResult() {
        $CODE = $this->input->post('resultCode', true);
        $NAME = $this->input->post('resultName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_exam_result", array("NAME" => $NAME));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_exam_result')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Result Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Result insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Result Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function examResultById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_exam_result", array("EXAM_RESULT_ID" => $id));
        $this->load->view('setup/ExamResult/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function examResultList() {
        $data['result'] = $this->utilities->getAll("bn_exam_result");
        $this->load->view("setup/ExamResult/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_exam_result', array('EXAM_RESULT_ID' => $id));
        $this->load->view('setup/ExamResult/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateExamResult() {
        $id = $this->input->post('id', true);
        $CODE = $this->input->post('ResultCode', true);
        $NAME = $this->input->post('ResultName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_exam_result", array("NAME" => $NAME, "EXAM_RESULT_ID !=" => $id));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a"));
            if ($this->utilities->updateData('bn_exam_result', $data, array("EXAM_RESULT_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Result Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Result Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Result Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
//    
//    
//    private function pr($data) {
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit;
//    }
}