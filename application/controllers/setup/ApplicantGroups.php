<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ApplicantGroups extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return  //Load All Rank Details From Rank Table
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Applicant Group List';
        $data['result'] = $this->utilities->findAllFromView('sa_itemgroups');
        $data['content_view_page'] = 'setup/applicantGroups/index';
        $this->template->display($data);
    }

    /*
     * @methodName Create()
     * @access
     * @param  none
     * @author Gaji Asif <gaji.asif@atilimited.net>
     * @return  
     */

    public function create() {
        if (isset($_POST['app_group_name'])) {
            $applicant_group_data = array(
                'GROUP_NAME' => $this->input->post('app_group_name'),
                'DESCRIPTION' => $this->input->post('description'),
                'REMARKS' => $this->input->post('remarks'),
                'RESTRICT_FG' => (isset($_POST['restricted'])) ? 'Y' : 'N',
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"]
            );

            if ($this->utilities->insertData($applicant_group_data, 'sa_itemgroups')) {
                redirect('setup/applicantGroups/index');
            }
        }

        $this->load->view('setup/applicantGroups/create');
    }

    /*
     * @methodName edit()
     * @access public
     * @param $GROUP_ID
     * @return  all data from sa_itemgroups table
     */

    public function edit($GROUP_ID) {
        if (isset($_POST['app_group_name'])) {

            $applicant_group_data = array(
                'GROUP_NAME' => $this->input->post('app_group_name'),
                'DESCRIPTION' => $this->input->post('description'),
                'REMARKS' => $this->input->post('remarks'),
                'RESTRICT_FG' => (isset($_POST['restricted'])) ? 'Y' : 'N',
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d H:i:s")
            );

            if ($this->utilities->updateData('sa_itemgroups', $applicant_group_data, array('GROUP_ID' => $GROUP_ID))) {
                redirect('setup/applicantGroups/index');
            }
        }
        $data['result'] = $this->utilities->findByAttribute('sa_itemgroups', array('GROUP_ID' => $GROUP_ID));
        $this->load->view('setup/applicantGroups/edit', $data);
    }

    /*
     * @methodName view()
     * @access public
     * @param $GROUP_ID
     * @return  all applicant groups data
     */

    public function view($GROUP_ID) {
        $data['result'] = $this->utilities->findByAttribute('sa_itemgroups', array('GROUP_ID' => $GROUP_ID));
        $this->load->view('setup/applicantGroups/view', $data);
    }

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}