<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Part extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Part Setup';
        $data['result'] = $this->utilities->findAllByAttributeWithJoin("bn_part","bn_trade","TRADE_ID", "TRADE_ID","NAME", "", "INNER");
        $data['content_view_page'] = 'setup/part/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['trade'] = $this->utilities->findAllByAttributeWithOrderBy("bn_trade", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('setup/part/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function savePart()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $bn_name= $this->input->post('bn_name', true);     
        $tradeId = $this->input->post('TRADE_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_part", array("PART_NAME" => $name, "CODE" => $code, "TRADE_ID" => $tradeId));
        if (empty($check)) {// if part name available
            $data = array(
                'CODE' => $code,
                'PART_NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'TRADE_ID' => $tradeId,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_part')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Part Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Part Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Part Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function partById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttributeWithJoin("bn_part","bn_trade","TRADE_ID", "TRADE_ID","NAME", array("PART_ID" => $id), "INNER"); // select single data select
        $this->load->view('setup/part/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function partList() {
        $data['result'] = $this->utilities->findAllByAttributeWithJoin("bn_part","bn_trade","TRADE_ID", "TRADE_ID","NAME", "", "INNER");
        $this->load->view("setup/part/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_part', array('part_ID' => $id));
        $data['trade'] = $this->utilities->findAllByAttributeWithOrderBy("bn_trade", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('setup/part/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updatePart()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $bn_name= $this->input->post('bn_name', true);     
        $tradeId = $this->input->post('TRADE_ID', true);    
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_part", array("PART_NAME" => $name,  "CODE" => $code, "part_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'PART_NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'TRADE_ID' => $tradeId,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_part',$data, array("part_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Part Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Part Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Part Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttributeWithJoin("bn_part","bn_trade","TRADE_ID", "TRADE_ID","NAME", array("part_ID" => $id), "INNER");
        $this->load->view('setup/part/view',$data);
    }

}
