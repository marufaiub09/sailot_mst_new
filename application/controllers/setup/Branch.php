<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Branch extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

   
    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Branch Setup';
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.GROUP_NAME FROM bn_daogroup bn WHERE bn.GROUP_ID = nh.DAO_GROUPID) BN_NAME
        FROM bn_branch nh")->result();
        $data['content_view_page'] = 'setup/branch/index';
        $this->template->display($data);
    }

     /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */

    public function create() {
        $data['daogroupid'] = $this->utilities->dropdownFromTableWithCondition('bn_daogroup', '-- Select Group Name --', 'GROUP_ID', 'GROUP_NAME');
        $this->load->view('setup/branch/create', $data);
    }

    public function saveBranch() {
        $branchName = $this->input->post('branchName', true);
        $branchCode = $this->input->post('branchCode', true);
        $banglaName = $this->input->post('banglaName', true);
        $position = $this->input->post('position', true);
        $daogroupid = $this->input->post('daogroupid', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if branch with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_branch", array("BRANCH_NAME" => $branchName, "BRANCH_CODE" => $branchCode));
        if (empty($check)) {// if branch name available
            $data = array(
                'BRANCH_NAME' => $branchName,
                'BRANCH_CODE' => $branchCode,
                'BANGLA_NAME' => $banglaName,
                'POSITION' => $position,
                'DAO_GROUPID' => $daogroupid,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_branch')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Branch Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Branch Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Branch Name Already Exist</div>";
        }
    }

    function BranchById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT nh.*, (SELECT bn.GROUP_NAME FROM bn_daogroup bn WHERE bn.GROUP_ID = nh.DAO_GROUPID) BN_NAME
        FROM bn_branch nh  WHERE nh.BRANCH_ID =$id")->row();
        $this->load->view('setup/branch/single_branch_row', $data);
    }

   
     /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function branchList() {
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.GROUP_NAME FROM bn_daogroup bn WHERE bn.GROUP_ID = nh.DAO_GROUPID) BN_NAME
        FROM bn_branch nh")->result();
        $this->load->view("setup/branch/branch_list", $data);
    }
     
    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */

    public function edit($id) {
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.GROUP_NAME FROM bn_daogroup bn WHERE bn.GROUP_ID = nh.DAO_GROUPID) BN_NAME
        FROM bn_branch nh  WHERE nh.BRANCH_ID =$id")->row();
        $data['daogroupid'] = $this->utilities->dropdownFromTableWithCondition('bn_daogroup', '-- Select Group Name --', 'GROUP_ID', 'GROUP_NAME');
        $grup_id = $data['result']->DAO_GROUPID;
        $data['item'] = $this->utilities->findByAttribute('bn_daogroup', array('GROUP_ID' => $grup_id));

        $this->load->view('setup/branch/edit', $data);
    }

    public function updatePart() {
        $id = $this->input->post('id', true);
        $branchName = $this->input->post('branchName', true);
        $branchCode = $this->input->post('branchCode', true);
        $banglaName = $this->input->post('banglaName', true);
        $position = $this->input->post('position', true);
        $daogroupid = $this->input->post('daogroupid', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if branch with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_branch", array("BRANCH_NAME" => $branchName, "BRANCH_CODE" => $branchCode));
        if (empty($check)) {// if branch name available
            $data = array(
                'BRANCH_NAME' => $branchName,
                'BRANCH_CODE' => $branchCode,
                'BANGLA_NAME' => $banglaName,
                'POSITION' => $position,
                'DAO_GROUPID' => $daogroupid,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DATE' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_branch', $data, array("BRANCH_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Branch Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Branch  Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Branch Already Exist</div>";
        }
    }
/**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */

//    
//    
//    private function pr($data) {
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit;
//    }
}