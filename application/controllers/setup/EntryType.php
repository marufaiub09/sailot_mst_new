<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EntryType extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Erman Hossen <emran@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Entry Types';
        $data['result'] = $this->utilities->getAll("bn_entrytype");
        $data['content_view_page'] = 'setup/EntryType/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/EntryType/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      templete
     */

    public function save()
    {        
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);    
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_entrytype", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_entrytype')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Entry Type Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Entry Type insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Entry Type Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      single row
     */

    function atById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_entrytype", array("ENTRY_TYPEID" => $id));
        $this->load->view('setup/EntryType/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      
     */

    function atList() {
        $data['result'] = $this->utilities->getAll("bn_entrytype");
        $this->load->view("setup/EntryType/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emran Hossen<Emran@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_entrytype', array('ENTRY_TYPEID' => $id));
        $this->load->view('setup/EntryType/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);   
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_entrytype", array("NAME" => $name, "ENTRY_TYPEID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_entrytype',$data, array("ENTRY_TYPEID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Entry Type Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Entry Type Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Entry Type  Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emran Hossen <emran@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_entrytype', array('ENTRY_TYPEID' => $id));
        $this->load->view('setup/EntryType/view',$data);
    }

}
