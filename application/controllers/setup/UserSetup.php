<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UserSetup extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('utilities');
        // $this->load->model('setupModel');
    }

    /*
     * @methodName index()
     * @access public
     * @param  none
     * @return  // user data 
     */

    public function index() {
        $data['metaTitle'] = 'Setup Module';
        $data['breadcrumbs'] = array(
            'Setup' => 'setup/index',
            'Setup' => '#'
        );

        $data['pageTitle'] = 'All Users';
        //$data['result'] = $this->utilities->findAllFromView('sa_lookup_grp');
        $userSessonData = $this->session->userdata('logged_in');
        $session_org_id = 1; //$userSessonData['SES_ORG_ID'];
        $data['orgList'] = $this->utilities->findAllByAttribute('bn_organization_hierarchy', array("ORG_ID" => $session_org_id));
        $data['content_view_page'] = 'setup/userSetup/userList';
        $this->template->display($data);
    }
    
     /*
     * @methodName addUserSetup()
     * @access public
     * @param  none
     * @return  // none
     */

    public function addUserSetup() {
        if ($_POST) {
           $user_data = array(
                'ORG_ID' => 1,
                'USERGRP_ID' => $this->input->post('group_name'),
                'USERLVL_ID' => $this->input->post('level_name'),
                'EMAIL' => $this->input->post('email'),
                'FULL_NAME' => $this->input->post('fullname'),
                'USERNAME' => $this->input->post('username'),
                'USERPW' => md5($this->input->post('password1')),
                'EFECT_FROM_DT' => date('Y-m-d', strtotime($this->input->post('effective_date'))),
                'EXPR_DT' => date('Y-m-d', strtotime($this->input->post('expired_date'))),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            
            // for Image Upload
            if ($_FILES['user_img']['error'] == 0) {
                $configImage = array(
                    'upload_path' => "src/upload/profile_picture/",
                    'allowed_types' => "gif|jpg|png|jpeg|pdf",
                    'overwrite' => TRUE,
                    'remove_spaces' => TRUE
                        /* 'max_size' => "2048000",
                          'max_height' => "768",
                          'max_width' => "1024" */
                );
                $ext = pathinfo($_FILES['user_img']['name'], PATHINFO_EXTENSION);
                $configImage['file_name'] = date('Y_m_d_') . substr(md5(rand()), 0, 7) . '.' . $ext;
                $this->load->library('upload');
                $this->upload->initialize($configImage);
                if ($this->upload->do_upload('user_img')) {
                    $user_data['USERIMG'] = $this->upload->file_name;
                }
            }

            $this->utilities->insertData($user_data, 'sa_users');

            $this->session->set_flashdata('success',$this->lang->line('user').' '.$this->lang->line('cre_success'));
            redirect('setup/userSetup');
        }

        $userSessonData = $this->session->userdata('logged_in');
        $session_org_id = $userSessonData['SES_ORG_ID'];

        $data['userGroup'] = $this->utilities->dropdownFromTableWithCondition("sa_user_group", "Select One", "USERGRP_ID", "USERGRP_NAME", array("ACTIVE_STATUS =" => 1));
        $data['orgList'] = $this->utilities->findByAttribute('bn_organization_hierarchy', array('ORG_ID' => $session_org_id));
        $data['user_groups_data'] = $this->utilities->dropdownFromTableWithCondition("sa_user_group", "Select Group", "USERGRP_ID", "USERGRP_NAME", array("ACTIVE_STATUS =" => 1, "ORG_ID" => $session_org_id));
        $this->load->view('setup/userSetup/addUserSetup', $data);
    }
    
    /*
     * @methodName getUserGroup()
     * @access public
     * @param  none
     * @return  // 
     */

    public function getUserGroup() {
        $ORG_ID = $this->input->post("ORG_ID");
        $user_groups = $this->utilities->dropdownFromTableWithCondition("sa_user_group", "Select Group", "USERGRP_ID", "USERGRP_NAME", array("ACTIVE_STATUS =" => 1, "ORG_ID" => $ORG_ID));
        echo form_dropdown('group_name', $user_groups);
    }
    
     /*
     * @methodName getGroupLevel()
     * @access public
     * @param  none
     * @return  // 
     */

    public function getGroupLevel() {
        echo "<pre>";

        $group_id = $this->input->post("group_id");
        $user_group_level = $this->utilities->dropdownFromTableWithCondition("sa_ug_level", "Select Level", "UG_LEVEL_ID", "UGLEVE_NAME", array("ACTIVE_STATUS =" => 1, "USERGRP_ID" => $group_id));
        echo form_dropdown('level_name', $user_group_level);
    }
    
     /*
     * @methodName editUserSetup()
     * @access public
     * @param  $USER_ID
     * @return  // 
     */

    public function editUserSetup($USER_ID) {
        $data['result_from_view_table'] = $this->utilities->findByAttribute('sa_users', array('USER_ID' => $USER_ID)); // all data comes form view table
        //$organization_id = $data['result_from_view_table']->ORG_ID;
        $data['groups'] = $this->utilities->dropdownFromTableWithCondition("sa_user_group", '', "USERGRP_ID", "USERGRP_NAME");
        $data['level'] = $this->utilities->findAllByAttribute("sa_ug_level", array("ACTIVE_STATUS" => 1));
        $this->load->view('setup/userSetup/editUserSetup', $data);
    }
    
     /*
     * @methodName updateUserData()
     * @access public
     * @param  
     * @return  // 
     */

    public function updateUserData() {
        if ($_POST) {
            echo"<pre>";
            print_r($_POST);
            exit();
            $pass = $this->input->post('password1');
            unlink('base_url(src/upload/profile_picture/)' . $_FILES['user_img']['name']);
            $USER_ID = $this->input->post('USER_ID');
            $user_data = array(
                'USERGRP_ID' => $this->input->post('group_name'),
                'USERLVL_ID' => $this->input->post('level_name'),
                'EMAIL' => $this->input->post('email'),
                'FULL_NAME' => $this->input->post('fullname'),
                'USERNAME' => $this->input->post('username'),
                'USERPW' => md5($this->input->post('password1')),
                'EFECT_FROM_DT' => date('Y-m-d', strtotime($this->input->post('effective_date'))),
                'EXPR_DT' => date('Y-m-d', strtotime($this->input->post('expired_date'))),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0
            );

            // for Image Upload
            if ($_FILES['user_img']['error'] == 0) {
                $configImage = array(
                    'upload_path' => "src/upload/profile_picture/",
                    'allowed_types' => "gif|jpg|png|jpeg|pdf",
                    'overwrite' => TRUE,
                    'remove_spaces' => TRUE
                        /* 'max_size' => "2048000",
                          'max_height' => "768",
                          'max_width' => "1024" */
                );
                $ext = pathinfo($_FILES['user_img']['name'], PATHINFO_EXTENSION);
                $configImage['file_name'] = date('Y_m_d_') . substr(md5(rand()), 0, 7) . '.' . $ext;
                $this->load->library('upload');
                $this->upload->initialize($configImage);
                if ($this->upload->do_upload('user_img')) {
                    $user_data['USERIMG'] = $this->upload->file_name;
                }
            }
            if ($pass != '') {
                $user_data['USERPW'] = md5($pass);
            }
            $this->utilities->updateData('sa_users', $user_data, array('USER_ID' => $USER_ID));
            $this->session->set_flashdata('success',$this->lang->line('user').' '.$this->lang->line('upd_success'));
            redirect('setup/userSetup');
        }
    }
    
    /*
     * @methodName viewUserSetup()
     * @access public
     * @param  $USER_ID
     * @return  // 
     */

    public function viewUserSetup($USER_ID) {
        $data['result_from_view_table'] = $this->utilities->findByAttribute('sa_users_v', array('USER_ID' => $USER_ID)); // all data comes form view table 
        $this->load->view('setup/userSetup/viewUserSetup', $data);
    }
    
    /*
     * @methodName checkEmailExist()
     * @access public
     * @param  
     * @return  // 
     */


    public function checkEmailExist() {
        $email = $this->input->post('email');
        $data = $this->utilities->findByAttribute('sa_users', array('EMAIL' => $email));
        $number = count($data);
        echo $number;
    }
    
     /*
     * @methodName checkUsernameExist()
     * @access public
     * @param  
     * @return  // 
     */

    public function checkUsernameExist() {
        $username = $this->input->post('username');
        $row = $data['result'] = $this->utilities->findByAttribute('sa_users', array('USERNAME' => $username));
        $number = count($row);
        echo $number;
    }
    
     /*
     * @methodName getEmpByOrg()
     * @access public
     * @param  
     * @return  // 
     */

    public function getEmpByOrg() {
        $ORG_ID = $this->input->post('ORG_ID');
        $empList = $this->utilities->dropdownFromTableWithCondition("sa_emp", "Select Employee", "EMP_ID", "FULL_NAME", array("ACTIVE_STATUS =" => 1, "ORG_ID" => $ORG_ID));
        echo form_dropdown('emp_name', $empList);
    }

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}
