<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Area Setup';
        $data['result'] = $this->db->query("SELECT na.*, hai.NAME  zone_name, oh.ORG_NAME
                                                FROM bn_navyadminhierarchy na 
                                                INNER JOIN bn_navyadminhierarchy hai on hai.ADMIN_ID = na.PARENT_ID
                                                INNER JOIN bn_organization_hierarchy oh on oh.ORG_ID = hai.ORG_TYPEID
                                                WHERE na.PARENT_ID != 0 ")->result();
        $data['content_view_page'] = 'setup/area/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['zone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID" =>null));
        $this->load->view('setup/area/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveArea()
    {
        $code= $this->input->post('code', true);    
        $name= $this->input->post('name', true);
        $bn_name= $this->input->post('bn_name', true);
        $ADMIN_ID = $this->input->post('ADMIN_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Zone with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyadminhierarchy", array("NAME" => $name, "CODE" => $code));
        if (empty($check)) {// if Zone name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'PARENT_ID' => $ADMIN_ID,
                'ADMIN_TYPE' => 2,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navyadminhierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Zone Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Area insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Area Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function areaById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT na.*, hai.NAME  zone_name, oh.ORG_NAME
                                                FROM bn_navyadminhierarchy na 
                                                INNER JOIN bn_navyadminhierarchy hai on hai.ADMIN_ID = na.PARENT_ID
                                                INNER JOIN bn_organization_hierarchy oh on oh.ORG_ID = hai.ORG_TYPEID
                                                WHERE na.PARENT_ID != 0 AND na.ADMIN_ID = $id")->row();
        $this->load->view('setup/area/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function areaList() {
        $data['result'] = $this->db->query("SELECT na.*, hai.NAME  zone_name, oh.ORG_NAME
                                                FROM bn_navyadminhierarchy na 
                                                INNER JOIN bn_navyadminhierarchy hai on hai.ADMIN_ID = na.PARENT_ID
                                                INNER JOIN bn_organization_hierarchy oh on oh.ORG_ID = hai.ORG_TYPEID
                                                WHERE na.PARENT_ID != 0")->result();
        $this->load->view("setup/area/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT na.*, hai.NAME  zone_name, oh.ORG_NAME
                                                FROM bn_navyadminhierarchy na 
                                                INNER JOIN bn_navyadminhierarchy hai on hai.ADMIN_ID = na.PARENT_ID
                                                INNER JOIN bn_organization_hierarchy oh on oh.ORG_ID = hai.ORG_TYPEID
                                                WHERE na.PARENT_ID != 0 AND na.ADMIN_ID = $id")->row();
        $data['zone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID" =>null));
        $this->load->view('setup/area/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateArea()
    {
        $id= $this->input->post('id', true);
        $code= $this->input->post('code', true);    
        $name= $this->input->post('name', true);
        $bn_name= $this->input->post('bn_name', true);
        $ADMIN_ID = $this->input->post('ADMIN_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyadminhierarchy", array("NAME" => $name,  "CODE" => $code, "ADMIN_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'PARENT_ID' => $ADMIN_ID,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navyadminhierarchy',$data, array("ADMIN_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Area Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Area Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Area Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT na.*, hai.NAME  zone_name, oh.ORG_NAME
                                                FROM bn_navyadminhierarchy na 
                                                INNER JOIN bn_navyadminhierarchy hai on hai.ADMIN_ID = na.PARENT_ID
                                                INNER JOIN bn_organization_hierarchy oh on oh.ORG_ID = hai.ORG_TYPEID
                                                WHERE na.PARENT_ID != 0 AND na.ADMIN_ID = $id")->row();
        $this->load->view('setup/area/view',$data);
    }

}
