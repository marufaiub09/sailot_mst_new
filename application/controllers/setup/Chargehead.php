<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Chargehead extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
    }

    /*
     * @methodName index()
     * @access public
     * @param  none
     * @return  //Load data from sa_charge_head table
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Charge Head' => '#'
        );
        $data['pageTitle'] = 'Charge Head Setup';
        $data['list'] = $this->utilities->findAllFromview('sa_charge_head');
//        $this->pr($data);
        $data['content_view_page'] = 'setup/chargehead/index';
        $this->template->display($data);
    }

    /*
     * @methodName Create()
     * @access public
     * @param  none
     * @return  none
     */

    public function create() {
        if (isset($_POST['CHARGE_NAME'])) {
            $data = array(
                'UD_CHARGE_ID' => $this->input->post('UD_CHARGE_ID', true),
                'CHARGE_NAME' => $this->input->post('CHARGE_NAME', true),
                'AMOUNT_BDT' => $this->input->post('AMOUNT_BDT', true),
                'AMOUNT_USD' => $this->input->post('AMOUNT_USD', true),
                'VALID_DAY' => $this->input->post('VALID_DAY', true),
                'DESCRIPTION' => $this->input->post('description', true),
                'REMARKS' => $this->input->post('REMARKS', true),
                'WITHDRAWAL' => (isset($_POST['WITHDRAWAL'])) ? 1 : 0,
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"],
                'CRE_DT' => date('Y-m-d H:i:s'),
                'ORG_ID' => $this->user_session["SES_ORG_ID"]
            );
            if ($this->utilities->insertData($data, 'sa_charge_head')) {
                redirect('setup/chargehead');
            }
        }
        $this->load->view('setup/chargehead/create');
    }

    /*
     * @methodName edit()
     * @access public
     * @param  $id
     * @return  data from sa_charge head against parameter id
     */

    public function edit($id) {
        if (isset($_POST['CHARGE_NAME'])) {
            $data = array(
                'UD_CHARGE_ID' => $this->input->post('UD_CHARGE_ID', true),
                'CHARGE_NAME' => $this->input->post('CHARGE_NAME', true),
                'AMOUNT_BDT' => $this->input->post('AMOUNT_BDT', true),
                'AMOUNT_USD' => $this->input->post('AMOUNT_USD', true),
                'VALID_DAY' => $this->input->post('VALID_DAY', true),
                'DESCRIPTION' => $this->input->post('description', true),
                'REMARKS' => $this->input->post('REMARKS', true),
                'WITHDRAWAL' => (isset($_POST['WITHDRAWAL'])) ? 1 : 0,
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date('Y-m-d H:i:s'),
                'ORG_ID' => $this->user_session["SES_ORG_ID"]
            );
            if ($this->utilities->updateData('sa_charge_head', $data, array('CHARGE_ID' => $id))) {
                redirect('setup/chargehead');
            }
        }
        $data['row'] = $row = $this->utilities->findByAttribute('sa_charge_head', array('CHARGE_ID' => $id));
//        $this->pr($data);
        $this->load->view('setup/chargehead/edit', $data);
    }
    
    
    /*
     * @methodName view()
     * @access public
     * @param  $id
     * @return  all data from sa_charge_head by parameter
     */

    public function view($id) {
        $data['row'] = $row = $this->utilities->findByAttribute('sa_charge_head', array('CHARGE_ID' => $id));
//        $this->pr($data);
        $this->load->view('setup/chargehead/view', $data);
    }
    
    /*
     * @methodName checkUserChargeIdExist()
     * @access public
     * @param  none
     * @return  
     */
    
    public function checkUserChargeIdExist(){
        $UD_CHARGE_ID  = $_POST['UD_CHARGE_ID'];
        $number  = $this->utilities->findByAttribute('sa_charge_head', array('UD_CHARGE_ID' => $UD_CHARGE_ID));        
        $total = count($number);
        echo $total;
    }

    /*
     * @methodName Pr()
     * @access
     * @param  none
     * @return  Debug Function
     */

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}

