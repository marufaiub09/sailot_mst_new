<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BaseSetup extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @methodName index()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      All Group setup list
     */
    function index() {
        $data['contentTitle'] = 'Group List';
        $data["breadcrumbs"] = array(
            "Admin" => "admin/index",
            "Group List" => '#'
        );
        $data['group'] = $this->utilities->getAll('sa_enum_grp');
        $data['content_view_page'] = 'setup/BaseSetup/groupSetup';
        $this->template->display($data);
    }

    /**
     * @methodName lookupGroupForm()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      Open add group Modal
     */
    function enumGroupForm() {
        $this->load->view('setup/BaseSetup/add_group');
    }

    /**
     * @methodName addLookupGroup()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      login template
     */
    function addEnumGroup() {
        $GRP_NAME = $this->input->post('GRP_NAME'); // Group name
        $check = $this->utilities->hasInformationByThisId("sa_enum_grp", array("grp_name" => $GRP_NAME));
        if (empty($check)) {// if Group name available preparing data to insert
            $group = array(
                'grp_name' => $GRP_NAME
            );
            if ($this->utilities->insertData($group, 'sa_enum_grp')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Base Group Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Base Group insert failed</div>";
            }
        } else {// if group name not available
            echo "<div class='alert alert-danger'>Base Group Already Exist</div>";
        }
    }

    /**
     * @methodName lookupDataFormInsert()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function enumDataFormInsert() {

        $data["ac_type"] = 1; // for insert lookUp data
        $id = $this->uri->segment(4);
        $data['id'] = $id;
        $data['name'] = $this->db->query("SELECT grp_name FROM sa_enum_grp WHERE grp_id=$id")->row()->grp_name;
        $this->load->view('setup/BaseSetup/add_enum_data', $data);
    }

    /**
     * @methodName saveLookUpData()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    public function saveEnumData() {
        $grp_id = $this->input->post('grp_id');
//        var_dump($grp_id);
        $LKP_NAME = $this->input->post('LKP_NAME'); // Group lookUp data name
        $status = $this->input->post('status'); // active status
        $check = $this->utilities->hasInformationByThisId("sa_enum_data", array("grp_id" => $grp_id, "enum_name" => $LKP_NAME));
        if (empty($check)) {// if LookUp data name available preparing data to insert
            $insert_info = array(
                'enum_name' => $_POST['LKP_NAME'],
                'grp_id' => $grp_id,
                'ACTIVE_STATUS' => $status
            );
            if ($this->utilities->insertData($insert_info, 'sa_enum_data')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Base data Add successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Base data insert failed</div>";
            }
        } else {// if LookUp data name not available
            echo "<div class='alert alert-danger'>Base data Name Already Exist</div>";
        }
    }

    /**
     * @methodName getLookUpData()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function getEnumData() {
        $GRP_ID = $this->input->post('param'); // Group id / hidden value
        $data['group_data'] = $this->db->query("select * from sa_enum_data where grp_id=$GRP_ID")->result();
        $this->load->view('setup/baseSetup/ajax_enum_data', $data);
    }

    /**
     * @methodName lookupDataFormUpdate()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function enumDataFormUpdate() {
        $data["ac_type"] = 2; //for update course info
        $look_up_group_id = $this->uri->segment(4,0); // for group id
        $look_up_id = $this->uri->segment(5, 0); // for lookUP data id
        $data['look_group_up_id'] = $look_up_group_id;
        $data['look_up_id'] = $look_up_id;
        //var_dump($look_up_group_id);
       // exit();
        
        $data['previousInfo'] = $this->utilities->findByAttribute('sa_enum_data', array('id' => $look_up_id));
        $this->load->view('setup/baseSetup/add_enum_data', $data);
    }

    /**
     * @methodName
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function updateEnumData() {
        $GRP_ID = $this->input->post('grp_id'); // Group ID hidden value
        $look_up_id = $this->input->post('LKP_ID'); // LookUP data ID hidden value
        $LKP_NAME = $this->input->post('LKP_NAME'); // Group lookUp data name
        $status = $this->input->post('status'); // active status
        // checking if m00_lkpdata with this lookUp name is already exist
        $check = $this->utilities->findByAttribute("sa_enum_data", array("id !=" => $look_up_id, "grp_id" => $GRP_ID, "enum_name" => $LKP_NAME));
        if (empty($check)) {// if Group ID, LookUp name available 
            // preparing data to update
            $update = array(
                'enum_name' => $LKP_NAME,
                
                'ACTIVE_STATUS' => $status
            );
            if ($this->utilities->updateData('sa_enum_data', $update, array('id' => $look_up_id))) { // if data update successfully
                echo "<div class='alert alert-success'>Enum data Update successfully</div>";
            } else { // if data update failed
                echo "<div class='alert alert-danger'>Enum data Update failed</div>";
            }
        } else {// if course name not available
            echo "<div class='alert alert-danger'>Enum data Name Already Exist</div>";
        }
    }

    /**
     * @methodName lookUpById()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function enumDataById() {
        $LKP_ID = $this->input->post('param');
        $data['group_data'] = $this->db->query("select * from sa_enum_data where id= $LKP_ID")->row();
        $this->load->view('setup/baseSetup/single_enum_data_row', $data);
    }

    /**
     * @methodName edit_look_up_status()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function edit_enum_data_status() {
        $status = $_POST['status'];
        $look_up_id = $_POST['look_up_id'];
        $pre_status = $status;
        if ($pre_status == 1) {
            $new_status = 0;
        } else {
            $new_status = 1;
        }
        $update_status = array(
            'ACTIVE_STATUS' => $new_status
        );
        if ($this->utilities->updateData('sa_enum_data', $update_status, array('id' => $look_up_id))) {
            echo "Y";
        } else {
            echo "N";
        }
    }

    /**
     * @methodName deletelookUp()
     * @access 
     * @param  
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function deleteEnumData() {
        $item_id = $this->input->post('item_id'); // row
        $data_tbl = $this->input->post('data_tbl'); // table name
        $data_field = $this->input->post('data_field'); // column name
        $attribute = array(
            "$data_field" => $item_id
        );
        $result = $this->utilities->deleteRowByAttribute($data_tbl, $attribute);
        if ($result == TRUE) {
            echo "Y";
        } else {
            echo "N";
        }
    }

}
