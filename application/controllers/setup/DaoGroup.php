<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DaoGroup extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Template
     */
    function index() {
        $data['pageTitle'] = 'DAO Group Setup';
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['daoGroup_list'] = $this->utilities->getAll("bn_daogroup");
        $data['content_view_page'] = 'setup/dao_group/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Template
     */
    function create() {
        if (isset($_POST['GROUP_NAME'])) {
            $data = array(
                'GROUP_CODE' => $this->input->post('GROUP_CODE'),
                'GROUP_NAME' => $this->input->post('GROUP_NAME'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_daogroup')) {
                redirect('setup/DaoGroup/index');
            }
        }
        $this->load->view('setup/dao_group/create');
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function DaoGroupById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute('bn_daogroup', array('GROUP_ID' => $id)); // select single data select
        $this->load->view('setup/dao_group/single_daoGroup_row', $data);
    }

    /**
     * @access      public
     * @param       Group ID
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Template
     */
    public function edit($id) {
        if (isset($_POST['GROUP_NAME'])) {
            $data = array(
                'GROUP_CODE' => $this->input->post('GROUP_CODE'),
                'GROUP_NAME' => $this->input->post('GROUP_NAME'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DATE' => date("Y-m-d h:i:s a")
            );

            if ($this->utilities->updateData('bn_daogroup', $data, array('GROUP_ID' => $id))) {
                redirect('setup/DaoGroup/index');
            }
        }
        $data['row'] = $this->utilities->findByAttribute('bn_daogroup', array('GROUP_ID' => $id));
        $this->load->view('setup/dao_group/edit', $data);
    }

}

/* End of file DaoGroup.php */
/* Location: ./application/controllers/setup/DaoGroup.php */