<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Country Setup';
        $data['result'] = $this->utilities->findAllFromView('bn_country');
        $data['content_view_page'] = 'setup/country/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->form_validation->set_rules('shortname', 'shortname', 'required|alpha');
        $this->load->view('setup/country/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function savePart() {
        $this->form_validation->set_rules('shortname', 'shortname', 'trim|required|alpha_extra');
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $shortname = $this->input->post('shortname', true);
        $banglaname = $this->input->post('banglaname', true);
        $continent = $this->input->post('continent', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_country", array("NAME" => $name, "CODE" => $code));
        if (empty($check)) {// if part name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'SHORT_NAME' => $shortname,
                'BANGLA_NAME' => $banglaname,
                'CONTINENTTYPE' => $continent,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_country')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Country Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Country insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Country Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute('bn_country', array('COUNTRY_ID' => $id)); // select single data select
        $this->load->view('setup/country/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author     Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function partList() {
        $data['result'] = $this->utilities->findAllFromView('bn_country');
        $this->load->view("setup/country/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_country', array('COUNTRY_ID' => $id));
        $this->load->view('setup/country/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePart() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $short_name = $this->input->post('short_name', true);
        $bangla_name = $this->input->post('bangla_name', true);
        $Continent = $this->input->post('Continent', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_country", array("NAME" => $name, "CODE" => $code, "COUNTRY_ID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'SHORT_NAME' => $short_name,
                'BANGLA_NAME' => $bangla_name,
                'CONTINENTTYPE' => $Continent,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_country', $data, array("COUNTRY_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Country  Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Country  Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Country  Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_country', array('COUNTRY_ID' => $id));
        //$data['result'] = $this->utilities->findByAttribute('bn_country', array('COUNTRY_ID' => $id));
        // echo '<pre>';print_r($data['result']);exit;
        $this->load->view('setup/country/view', $data);
    }

}
