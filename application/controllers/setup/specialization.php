<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Specialization extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function category() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Specialization category Setup';
        $data['result'] = $this->utilities->findAllByAttribute("bn_otherspecialization", array("CATEGORY" => 1));
        $data['content_view_page'] = 'setup/specialization/category_index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Specialization Setup';
        $data['result'] = $this->utilities->findAllByAttribute("bn_otherspecialization", array("CATEGORY =" => "2"));
        $data['content_view_page'] = 'setup/specialization/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['Specialization'] = $this->utilities->findAllByAttribute("bn_otherspecialization", array("CATEGORY" => 1, "ACTIVE_STATUS" => 1));
        $this->load->view('setup/specialization/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function catCreate() {
        $this->load->view('setup/specialization/cat_create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function saveCat() {
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_otherspecialization", array("NAME" => $name));
        if (empty($check)) {// if part name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'CATEGORY' => 1,
                'PARENT_ID' => null,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_otherspecialization')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Category Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Category Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Category Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function saveSpec() {
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $SPEC_ID = $this->input->post('SPEC_ID', true);
        $PARENT_ID = $this->input->post('SPEC_CAT_ID', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_otherspecialization", array("NAME" => $name));
        if (empty($check)) {// if part name available
            if ($SPEC_ID == 1) {
                $data = array(
                    'CODE' => $code,
                    'NAME' => $name,
                    'CATEGORY' => 1,
                    'PARENT_ID' => null,
                    'ACTIVE_STATUS' => $ACTIVE_STATUS,
                    'CRE_BY' => $this->user_session["USER_ID"]
                );
            } else {
                $data = array(
                    'CODE' => $code,
                    'NAME' => $name,
                    'CATEGORY' => 2,
                    'PARENT_ID' => $PARENT_ID,
                    'ACTIVE_STATUS' => $ACTIVE_STATUS,
                    'CRE_BY' => $this->user_session["USER_ID"]
                );
            }

            if ($this->utilities->insertData($data, 'bn_otherspecialization')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Specialization Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Specialization Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Specialization Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */
    function catById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_otherspecialization", array("OtherSpecializationID" => $id));
        $this->load->view('setup/specialization/single_cat_row', $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */
    function specById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_otherspecialization", array("OtherSpecializationID" => $id));
        $this->load->view('setup/specialization/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */
    function catList() {
        $data['result'] = $this->utilities->findAllByAttribute("bn_otherspecialization", array("CATEGORY" => '1'));
        $this->load->view("setup/specialization/cat_list", $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */
    function specList() {
        $data['result'] = $this->utilities->findAllByAttribute("bn_otherspecialization", array("CATEGORY =" => '2'));
        $this->load->view("setup/specialization/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_otherspecialization', array('OtherSpecializationID' => $id));
        $data['Specialization'] = $this->utilities->findAllByAttribute("bn_otherspecialization", array("CATEGORY" => 1, "ACTIVE_STATUS" => 1));
        $this->load->view('setup/specialization/edit', $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function catEdit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_otherspecialization', array('OtherSpecializationID' => $id));
        $this->load->view('setup/specialization/cat_edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateCat() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Category with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_otherspecialization", array("NAME" => $name, "OtherSpecializationID !=" => $id));
        if (empty($check)) {// if Category name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_otherspecialization', $data, array("OtherSpecializationID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Category Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Category Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Category Name Already Exist</div>";
        }
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateSpec() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('name', true);
        $code = $this->input->post('code', true);
        $SPEC_ID = $this->input->post('SPEC_ID', true);
        $PARENT_ID = $this->input->post('SPEC_CAT_ID', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_otherspecialization", array("NAME" => $name, "OtherSpecializationID !=" => $id));
        if (empty($check)) {// if part name available
            if ($SPEC_ID == 1) {
                $data = array(
                    'CODE' => $code,
                    'NAME' => $name,
                    'CATEGORY' => 1,
                    'PARENT_ID' => null,
                    'ACTIVE_STATUS' => $ACTIVE_STATUS,
                    'UPD_BY' => $this->user_session["USER_ID"],
                    'UPD_DT' => date("Y-m-d h:i:s a")
                );
            } else {
                $data = array(
                    'CODE' => $code,
                    'NAME' => $name,
                    'CATEGORY' => 2,
                    'PARENT_ID' => $PARENT_ID,
                    'ACTIVE_STATUS' => $ACTIVE_STATUS,
                    'CRE_BY' => $this->user_session["USER_ID"]
                );
            }

            if ($this->utilities->updateData('bn_otherspecialization', $data, array("OtherSpecializationID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Specialization updated successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Specialization updated failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Specialization Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
}
