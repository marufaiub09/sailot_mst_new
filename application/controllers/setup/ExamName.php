<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ExamName extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Level';
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_govtexam_hierarchy bn WHERE bn.GOVT_EXAMID = nh.PARENT_ID) BN_NAME
                                            FROM bn_govtexam_hierarchy nh WHERE nh.GOVTEXAM_TYPE = 2")->result();
        $data['content_view_page'] = 'setup/exam_name/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['examType'] = $this->utilities->findAllByAttribute("bn_govtexam_hierarchy", array("PARENT_ID" => null));
        $this->load->view('setup/exam_name/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul slam <reazul@atilimited.net>
     * @return      View modal
     */
    public function savePart() {
        $name = $this->input->post('ExamName', true);
        $code = $this->input->post('ExamCode', true);
        $parentid = $this->input->post('exam_type', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_govtexam_hierarchy", array("NAME" => $name));
        if (empty($check)) {// if part name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'PARENT_ID' => $parentid,
                'GOVTEXAM_TYPE' => 2,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_govtexam_hierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Name Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_govtexam_hierarchy bn WHERE bn.GOVT_EXAMID = nh.PARENT_ID) BN_NAME
                                            FROM bn_govtexam_hierarchy nh WHERE nh.GOVT_EXAMID =$id")->row();
        $this->load->view('setup/exam_name/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function partList() {
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_govtexam_hierarchy bn WHERE bn.GOVT_EXAMID = nh.PARENT_ID) BN_NAME
                                            FROM bn_govtexam_hierarchy nh WHERE nh.GOVTEXAM_TYPE = 2")->result();
        $this->load->view("setup/exam_name/list", $data);
    }

    public function edit($id) {
        $data['examType'] = $this->utilities->findAllByAttribute("bn_govtexam_hierarchy", array("PARENT_ID" => null));
        $data['result'] = $this->utilities->findByAttribute('bn_govtexam_hierarchy', array('GOVT_EXAMID' => $id));
        $this->load->view('setup/exam_name/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePart() {
        $id = $this->input->post('id', true);
        $name = $this->input->post('ExamName', true);
        $code = $this->input->post('ExamCode', true);
        $parentid = $this->input->post('exam_type', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_govtexam_hierarchy", array("NAME" => $name, "GOVT_EXAMID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'PARENT_ID' => $parentid,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_govtexam_hierarchy', $data, array("GOVT_EXAMID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Name Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Exam Name Already Exist</div>";
        }
    }

}
