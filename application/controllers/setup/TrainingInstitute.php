<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TrainingInstitute extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Types Setup';
        $data['result'] =$this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_trainingorganization bn WHERE bn.TrainingOrganizationID = nh.TRAINING_ORG_ID) BN_NAME
        FROM bn_traininginstitute nh")->result();
        $data['content_view_page'] = 'setup/TrainingInstitute/index';
        $this->template->display($data);
    }

   /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
   $data['Organization'] = $this->db->query("SELECT * FROM `bn_trainingorganization` WHERE ACTIVE_STATUS = 1 ")->result();
   $this->load->view('setup/TrainingInstitute/create',$data);
    }

     /**
     * @access      public
     * @param       none
     * @author      Reazul slam <reazul@atilimited.net>
     * @return      View modal
     */

  public function savePart()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $organization = $this->input->post('organization', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_traininginstitute", array("Name" => $name, "Code" => $code));
        if (empty($check)) {// if part name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'TRAINING_ORG_ID' => $organization,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_traininginstitute')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Institute Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Institute insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Training Institute Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
   function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] =$this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_trainingorganization bn WHERE bn.TrainingOrganizationID = nh.TRAINING_ORG_ID) BN_NAME
        FROM bn_traininginstitute nh WHERE nh.Training_Institute_ID =$id")->row();        
        $this->load->view('setup/TrainingInstitute/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */

    function partList() {
        $data['result'] =$this->db->query("SELECT nh.*, (SELECT bn.Name FROM bn_trainingorganization bn WHERE bn.TrainingOrganizationID = nh.TRAINING_ORG_ID) BN_NAME
        FROM bn_traininginstitute nh")->result();
        $this->load->view("setup/TrainingInstitute/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
  public function edit($id)
    { 
  $data['result'] = $this->utilities->findByAttribute('bn_traininginstitute', array('Training_Institute_ID' => $id));
  $data['organization'] = $this->utilities->findAllByAttribute("bn_trainingorganization", array("ACTIVE_STATUS" => 1));

     
  $this->load->view('setup/TrainingInstitute/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updatePart()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $traninig_org_id = $this->input->post('TRAINING_ORG_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_traininginstitute", array("Name" => $name,  "Code" => $code, "Training_Institute_ID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'Code' => $code,
                'Name' => $name,
                'TRAINING_ORG_ID' => $traninig_org_id,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_traininginstitute',$data, array("Training_Institute_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Training Institute Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Training Institute  Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Training Institute Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
 $data['viewdetails'] =$this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_trainingorganization bn WHERE bn.TrainingOrganizationID = nh.TRAINING_ORG_ID) BN_NAME
 FROM bn_traininginstitute nh WHERE nh.Training_Institute_ID =$id")->row();
 $this->load->view('setup/TrainingInstitute/view',$data);
    }

}
