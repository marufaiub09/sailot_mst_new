<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Shiptype extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Ship Type Setup';
        $data['result'] = $this->utilities->getAll("bn_shiptype");
        $data['content_view_page'] = 'setup/ship_type/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $this->load->view('setup/ship_type/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveST()
    {        
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);    
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_shiptype", array("NAME" => $name));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_shiptype')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Ship Type Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Ship Type insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Ship Type Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function stById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_shiptype", array("SHIP_TYPEID" => $id));
        $this->load->view('setup/ship_type/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function stList() {
        $data['result'] = $this->utilities->getAll("bn_shiptype");
        $this->load->view("setup/ship_type/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findByAttribute('bn_shiptype', array('SHIP_TYPEID' => $id));
        $this->load->view('setup/ship_type/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateST()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);   
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_shiptype", array("NAME" => $name, "SHIP_TYPEID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_shiptype',$data, array("SHIP_TYPEID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Ship Type Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Ship Type Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Ship Type  Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->utilities->findByAttribute('bn_shiptype', array('SHIP_TYPEID' => $id));
        $this->load->view('setup/ship_type/view',$data);
    }

}
