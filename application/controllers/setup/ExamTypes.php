<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ExamTypes extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Exam Types Setup';
        $data['result'] =$this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_navyexam_hierarchy bn WHERE bn.EXAM_ID = nh.PARENT_ID) BN_NAME
                                            FROM bn_navyexam_hierarchy nh WHERE nh.EXAM_TYPE = 2")->result();
        $data['content_view_page'] = 'setup/exam_type/index';
        $this->template->display($data);
    }

   /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
       $data['examType'] = $this->utilities->findAllByAttribute("bn_navyexam_hierarchy", array("PARENT_ID" => 0));
       $this->load->view('setup/exam_type/create',$data);
    }

     /**
     * @access      public
     * @param       none
     * @author      Reazul slam <reazul@atilimited.net>
     * @return      View modal
     */

  public function savePart()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $parentid = $this->input->post('exam_type', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyexam_hierarchy", array("NAME" => $name, "CODE" => $code));
        if (empty($check)) {// if part name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'PARENT_ID' => $parentid,
                'EXAM_TYPE' =>2,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_navyexam_hierarchy')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Type Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Type insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Exam Type Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
   function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_navyexam_hierarchy bn WHERE bn.EXAM_ID = nh.PARENT_ID) BN_NAME
        FROM bn_navyexam_hierarchy nh WHERE nh.EXAM_ID =$id")->row();
        $this->load->view('setup/exam_type/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */

    function partList() {
        $data['result'] = $this->db->query("SELECT nh.*, (SELECT bn.NAME FROM bn_navyexam_hierarchy bn WHERE bn.EXAM_ID = nh.PARENT_ID) BN_NAME
         FROM bn_navyexam_hierarchy nh WHERE nh.EXAM_TYPE = 2")->result();
        $this->load->view("setup/exam_type/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
  public function edit($id)
    { 
        $data['examType'] = $this->utilities->findAllByAttribute("bn_navyexam_hierarchy", array("PARENT_ID" => 0));
        $data['result'] = $this->utilities->findByAttribute('bn_navyexam_hierarchy', array('EXAM_ID' => $id));
        $this->load->view('setup/exam_type/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updatePart()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $parentid = $this->input->post('exam_type', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if exam name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_navyexam_hierarchy", array("NAME" => $name,  "CODE" => $code, "EXAM_ID !=" => $id));
        if (empty($check)) {// if Exam name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'PARENT_ID' => $parentid,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_navyexam_hierarchy',$data, array("EXAM_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Exam Type Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Exam Type Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Exam Type Already Exist</div>";
        }
    }   

}
