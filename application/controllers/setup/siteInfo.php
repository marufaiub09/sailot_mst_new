<?php

defined('BASEPATH') OR exit('NO direct script access allowed !');

class SiteInfo extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('siteInfo_model');
        $this->load->model('utilities');
    }

    /*
     * @methodName Tree Builder()
     * @access
     * @param  none
     * @return  Return Tree as unorder list  
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Site Information' => '#'
        );
        $data['pageTitle'] = 'Site Information';
        $data["data"] = $this->utilities->findAllFromView('site_info');
        $data['content_view_page'] = 'setup/siteInfo/index';
        $this->template->display($data);
    }
    
    
     /*
     * @methodName Create()
     * @access
     * @param  none
     * @return  //Load divition create page 
     */

    public function create() {
        if (isset($_POST['siteInfoType'])) {
            $data = array(
                'TYPE' => $this->input->post('siteInfoType'),
                'S_INFO_TITLE' => $this->input->post('siteInfoTitle'),
                'S_INFO_DESC' => $this->input->post('siteInfoDescription'),
                'ACTIVE_STATUS' => ($this->input->post('ACTIVE_STATUS') == '') ? 0 : 1,
                'CRE_BY' => $this->session->userdata('USER_ID'),
            );
            if ($this->utilities->insertData($data, 'site_info')) {
                redirect('setup/siteInfo');
            }
        }
        $this->load->view('setup/siteInfo/create');
    }
    
    
    /*
     * @methodName edit()
     * @access
     * @param  none
     * @return  //Load divition create page 
     */

    public function edit($id) {
        if (isset($_POST['siteInfoType'])) {
            $data = array(
                'TYPE' => $this->input->post('siteInfoType'),
                'S_INFO_TITLE' => $this->input->post('siteInfoTitle'),
                'S_INFO_DESC' => $this->input->post('siteInfoDescription'),
                'ACTIVE_STATUS' => ($this->input->post('ACTIVE_STATUS') == '') ? 0 : 1,
                'CRE_BY' => $this->session->userdata('USER_ID'),
            );
            if ($this->utilities->updateData('site_info', $data, array('S_INFO_ID' => $id))) {
                redirect('setup/siteInfo');
            }
        }
        $data['row'] = $this->utilities->findByAttribute('site_info', array('S_INFO_ID' => $id));
//        $this->pr($data);
        $this->load->view('setup/siteInfo/edit', $data);
    }
    
    
    /*
     * @methodName View() 
     * @access
     * @param  none
     * @return  //
     */

    public function view($id) {
        $data['viewdetails'] = $this->utilities->findByAttribute('site_info', array('S_INFO_ID' => $id));
        $this->load->view('setup/siteInfo/view', $data);
    }
    

    
    /*
     * @methodName Pr()
     * @access
     * @param  none
     * @return  Debug Function   
     */

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}

