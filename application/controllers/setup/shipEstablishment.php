<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ShipEstablishment extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Ship Establishment Setup';
        $data['result'] = $this->db->query("SELECT se.*, st.NAME shipType, ar.NAME Area, zo.NAME Zone
                                            FROM bn_ship_establishment se
                                            INNER JOIN bn_shiptype st on st.SHIP_TYPEID = se.SHIP_TYPEID
                                            INNER JOIN bn_navyadminhierarchy ar on ar.ADMIN_ID = se.AREA_ID
                                            INNER JOIN bn_navyadminhierarchy zo on zo.ADMIN_ID = se.ZONE_ID")->result();
        $data['content_view_page'] = 'setup/ship_establishment/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['shipType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_shiptype", array("ACTIVE_STATUS" => 1),"NAME");
        $data['zone'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy",  array("ACTIVE_STATUS" => 1, "ADMIN_TYPE =" => "1"),"NAME");
        $this->load->view('setup/ship_establishment/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveSE()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $st_name= $this->input->post('st_name', true);     
        $mr_st_name= $this->input->post('mr_st_name', true);     
        $bn_name = $this->input->post('bn_name', true);     
        $bn_srt_name = $this->input->post('bn_srt_name', true);     
        $SHIPTYPE_ID = $this->input->post('SHIPTYPE_ID', true);     
        $ZONE_ID = $this->input->post('ZONE_ID', true);     
        $AREA_ID = $this->input->post('AREA_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Ship Establishment with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_ship_establishment", array("NAME" => $name, "CODE" => $code,'SHIP_TYPEID' => $SHIPTYPE_ID, 'AREA_ID' => $AREA_ID, 'ZONE_ID' => $ZONE_ID));
        if (empty($check)) {// if Ship Establishment name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'BANGLA_SHORT_NAME' => $bn_srt_name,
                'SHORT_NAME' => $st_name,
                'MORE_SHORT_NAME' => $mr_st_name,
                'SHIP_TYPEID' => $SHIPTYPE_ID,
                'AREA_ID' => $AREA_ID,
                'ZONE_ID' => $ZONE_ID,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_ship_establishment')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Ship Establishment Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Ship Establishment insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Ship Establishment Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function seById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT se.*, st.NAME shipType, ar.NAME Area, zo.NAME Zone
                                        FROM bn_ship_establishment se
                                        INNER JOIN bn_shiptype st on st.SHIP_TYPEID = se.SHIP_TYPEID
                                        INNER JOIN bn_navyadminhierarchy ar on ar.ADMIN_ID = se.AREA_ID
                                        INNER JOIN bn_navyadminhierarchy zo on zo.ADMIN_ID = se.ZONE_ID
                                        WHERE se.SHIP_ESTABLISHMENTID = $id")->row();
        $this->load->view('setup/ship_establishment/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function seList() {
        $data['result'] = $this->db->query("SELECT se.*, st.NAME shipType, ar.NAME Area, zo.NAME Zone
                                            FROM bn_ship_establishment se
                                            INNER JOIN bn_shiptype st on st.SHIP_TYPEID = se.SHIP_TYPEID
                                            INNER JOIN bn_navyadminhierarchy ar on ar.ADMIN_ID = se.AREA_ID
                                            INNER JOIN bn_navyadminhierarchy zo on zo.ADMIN_ID = se.ZONE_ID")->result();
        $this->load->view("setup/ship_establishment/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT se.*, st.NAME shipType, ar.NAME Area, zo.NAME Zone
                                            FROM bn_ship_establishment se
                                            INNER JOIN bn_shiptype st on st.SHIP_TYPEID = se.SHIP_TYPEID
                                            INNER JOIN bn_navyadminhierarchy ar on ar.ADMIN_ID = se.AREA_ID
                                            INNER JOIN bn_navyadminhierarchy zo on zo.ADMIN_ID = se.ZONE_ID
                                            WHERE se.SHIP_ESTABLISHMENTID = $id")->row();
        $data['shipType'] = $this->utilities->findAllByAttribute("bn_shiptype", array("ACTIVE_STATUS" => 1));
        $data['zone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => "1"));
        $data['area'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => "2"));
        $this->load->view('setup/ship_establishment/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateSE()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $st_name= $this->input->post('st_name', true);     
        $mr_st_name= $this->input->post('mr_st_name', true);     
        $bn_name = $this->input->post('bn_name', true);     
        $bn_srt_name = $this->input->post('bn_srt_name', true);     
        $SHIPTYPE_ID = $this->input->post('SHIPTYPE_ID', true);     
        $ZONE_ID = $this->input->post('ZONE_ID', true);     
        $AREA_ID = $this->input->post('AREA_ID', true);      
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_ship_establishment", array("NAME" => $name,  "CODE" => $code, "SHIP_ESTABLISHMENTID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'BANGLA_NAME' => $bn_name,
                'BANGLA_SHORT_NAME' => $bn_srt_name,
                'SHORT_NAME' => $st_name,
                'MORE_SHORT_NAME' => $mr_st_name,
                'SHIP_TYPEID' => $SHIPTYPE_ID,
                'AREA_ID' => $AREA_ID,
                'ZONE_ID' => $ZONE_ID,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_ship_establishment',$data, array("SHIP_ESTABLISHMENTID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Ship Establishment Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Ship Establishment Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Ship Establishment Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT se.*, st.NAME shipType, ar.NAME Area, zo.NAME Zone
                                                FROM bn_ship_establishment se
                                                INNER JOIN bn_shiptype st on st.SHIP_TYPEID = se.SHIP_TYPEID
                                                INNER JOIN bn_navyadminhierarchy ar on ar.ADMIN_ID = se.AREA_ID
                                                INNER JOIN bn_navyadminhierarchy zo on zo.ADMIN_ID = se.ZONE_ID
                                                WHERE se.SHIP_ESTABLISHMENTID = $id")->row();
        $this->load->view('setup/ship_establishment/view',$data);
    }
    /**
     * @access public
     * @param 
     * @author   Emdadul Huq <Emdadul@atilimited.net> 
     * @return      ajax area list by zone ID
     */
    public function areaByZone() {
        $zoneId = $_POST['zoneId'];
        $query = $this->utilities->findAllByAttribute('bn_navyadminhierarchy', array("PARENT_ID" => $zoneId, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->ADMIN_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }

}
