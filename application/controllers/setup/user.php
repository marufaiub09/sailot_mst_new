<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends EXT_Controller
{
    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        $this->load->model('setup_model');
    }

    public function index()
    {
        $data['metaTitle']   = 'All Users';
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data["users"] = $this->utilities->findAllFromView("sa_users");
        $data['content_view_page'] = 'setup/user/index';
        $this->template->display($data);
    }
/*
    public function create()
    {
        if(isset($_POST['RELIGION_ID'])){
            $data=array(
                'CASTE_NAME'=>$this->input->post('CASTE_NAME'),
                'RELIGION_ID'=>$this->input->post('RELIGION_ID'),
                'ACTIVE_STATUS'=> (isset($_POST['ACTIVE_STATUS']))?1:0
            );

            if($this->utilities->insertData($data, 'sa_castes'))
            {
                redirect('setup/caste/index');
            }
        }
        $data['religion_list'] = $this->utilities-> dropdownFromTableWithCondition('sav_religion','','RELIGION_ID','RELIGION_NAME');
        $this->load->view('setup/caste/create',$data);
    }

    public function edit($id)
    {
        if(isset($_POST['RELIGION_ID'])){
            $data=array(
                'CASTE_NAME'=>$this->input->post('CASTE_NAME'),
                'RELIGION_ID'=>$this->input->post('RELIGION_ID'),
                'ACTIVE_STATUS'=> (isset($_POST['ACTIVE_STATUS']))?1:0
            );

            if($this->utilities->updateData('sa_castes', $data, array('CASTE_ID' => $id)))
            {
                redirect('setup/caste/index');
            }
        }
        $data['religion_list'] = $this->utilities-> dropdownFromTableWithCondition('sav_religion','','RELIGION_ID','RELIGION_NAME');
        $data['row'] = $this->utilities->findByAttribute('sa_castes', array('CASTE_ID' => $id));
        $this->load->view('setup/caste/edit',$data);
    }
 * */

}