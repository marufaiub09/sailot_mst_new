<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Designation extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        $this->load->model('designation_model');
    }

    /*
     * @methodName index()
     * @access public
     * @param  none
     * @return  designation details from sa_designation table
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Designation' => '#'
        );
        $data['pageTitle'] = 'Designation Setup';
        $data['list'] = $this->utilities->findAllFromview('sa_designation');
//        $this->pr($data);
        $data['content_view_page'] = 'setup/designation/index';
        $this->template->display($data);
    }

    /*
     * @methodName Create()
     * @access public
     * @param  none
     * @return  
     */

    public function create() {
        if (isset($_POST['designation'])) {
            $data = array(
                'ORG_ID' => $this->input->post('organization_id', true),
                'PARENT_DESIG_ID' => $this->input->post('parent_id', true),
                'DESIG_NAME' => $this->input->post('designation', true),
                'UD_DESIG_ID' => $this->input->post('udid', true),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'sa_designation')) {
                redirect('setup/designation');
            }
        }
        $data['organization'] = $this->utilities->dropdownFromTableWithCondition('sa_organizations', 'Select Organization', 'ORG_ID', 'ORG_NAME');
        $this->load->view('setup/designation/create', $data);
    }

    /*
     * @methodName edit()
     * @access public
     * @param  $id
     * @return  
     */

    public function edit($id) {
        if (isset($_POST['designation'])) {
            $data = array(
                'ORG_ID' => $this->input->post('organization_id', true),
                'PARENT_DESIG_ID' => $this->input->post('parent_id', true),
                'DESIG_NAME' => $this->input->post('designation', true),
                'UD_DESIG_ID' => $this->input->post('udid', true),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->updateData('sa_designation', $data, array('DESIG_ID' => $id))) {
                redirect('setup/designation');
            }
        }
        $data['row'] = $row = $this->utilities->findByAttribute('sa_designation', array('DESIG_ID' => $id));
        $designation = array();
        if ($row->PARENT_DESIG_ID > 0) {
            $designation = $this->utilities->dropdownFromTableWithCondition('sa_designation', '', 'DESIG_ID', 'DESIG_NAME', array('ORG_ID' => $row->ORG_ID));
        }
        $data['designation'] = $designation;
        $data['organization'] = $this->utilities->dropdownFromTableWithCondition('sa_organizations', 'Select Organization', 'ORG_ID', 'ORG_NAME');
        $this->load->view('setup/designation/edit', $data);
    }

    /*
     * @methodName getParentByOrganizationId()
     * @access public
     * @param  none
     * @return  
     */

    public function getParentByOrganizationId() {

        $id = $_POST['fld_id'];
        $result = $this->db->query("SELECT * FROM sa_designation WHERE ORG_ID = {$id}")->result();
        echo json_encode($result);
    }

    /*
     * @methodName view()
     * @access
     * @param  none
     * @return  //
     */

    public function view($id) {
        $data['viewdetails'] = $this->designation_model->gellAllDesignationInfoById($id);
        $this->load->view('setup/designation/view', $data);
    }

    /*
     * @methodName Delete()
     * @access
     * @param  none
     * @return  //
     */

    public function delete($id) {
        $this->utilities->deleteRowByAttribute('sa_designation', array('DESIG_ID' => $id));
        redirect('setup/designation');
    }

    /*
     * @methodName Pr()
     * @access
     * @param  none
     * @return  Debug Function
     */

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}

?>