<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ShipMigration extends EXT_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Ship Migration Setup';
        $data['result'] = $this->db->query("SELECT  sm.*, se.NAME SHIP_ESTABLISHMENT, na.NAME PRE_AREA, bn.NAME NEW_AREA
                                            FROM bn_ship_migration sm
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = sm.SHIP_ESTABLISHMENTID
                                            INNER JOIN bn_navyadminhierarchy na on sm.PREVIOUS_AREA_ID = na.ADMIN_ID
                                            INNER JOIN bn_navyadminhierarchy bn on sm.NEW_AREA_ID = bn.ADMIN_ID")->result();
        $data['content_view_page'] = 'setup/ship_migration/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['shipMigration'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['newArea'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID !=" => '0'),"NAME");
        $this->load->view('setup/ship_migration/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveSM(){
        $SE_ID= $this->input->post('SE_ID', true);
        $PRE_AREA_ID= $this->input->post('PRE_AREA_ID', true);
        $PRE_ZONE_ID= $this->input->post('PRE_ZONE_ID', true);
        $NEW_AREA_ID= $this->input->post('NEW_AREA_ID', true);     
        $nn = $this->db->query("SELECT * FROM bn_navyadminhierarchy WHERE ADMIN_ID =  $NEW_AREA_ID ")->row(); 
        $date1 = explode("-", $this->input->post('migrationDate'));
        $sd = $date1[2] . '-' . $date1[1] . '-' . $date1[0]; 
       // echo"$sd";
         // echo '<pre>';print_r($date1);exit;
        $ACTIVE_STATUS = 1;
        // checking if part with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_ship_migration", array('SHIP_ESTABLISHMENTID' => $SE_ID, 'PREVIOUS_AREA_ID' => $PRE_AREA_ID, 'NEW_AREA_ID' => $NEW_AREA_ID));
        if (empty($check)) {// if part name available
            $data = array(
                'SHIP_ESTABLISHMENTID' => $SE_ID,
                'PREVIOUS_AREA_ID' => $PRE_AREA_ID,
                'PREVIOUS_ZONE_ID' => $PRE_ZONE_ID,
                'NEW_AREA_ID' => $NEW_AREA_ID,
                'NEW_ZONE_ID' => $nn->PARENT_ID,
                'CHANGE_DATE' => $sd,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_ship_migration')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Ship migration create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Ship migration insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Ship migration Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function smList() {
        $data['result'] = $this->db->query("SELECT  sm.*, se.NAME SHIP_ESTABLISHMENT, na.NAME PRE_AREA, bn.NAME NEW_AREA
                                            FROM bn_ship_migration sm
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = sm.SHIP_ESTABLISHMENTID
                                            INNER JOIN bn_navyadminhierarchy na on sm.PREVIOUS_AREA_ID = na.ADMIN_ID
                                            INNER JOIN bn_navyadminhierarchy bn on sm.NEW_AREA_ID = bn.ADMIN_ID")->result();
        $this->load->view("setup/ship_migration/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT  sm.*, se.NAME SHIP_ESTABLISHMENT, na.CODE PRE_CODE, na.NAME PRE_AREA, bn.NAME NEW_AREA
                                            FROM bn_ship_migration sm
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = sm.SHIP_ESTABLISHMENTID
                                            INNER JOIN bn_navyadminhierarchy na on sm.PREVIOUS_AREA_ID = na.ADMIN_ID
                                            INNER JOIN bn_navyadminhierarchy bn on sm.NEW_AREA_ID = bn.ADMIN_ID
                                            WHERE sm.SHIP_MIGRATION_ID = $id")->row();
        $data['shipMigration'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['newArea'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID !=" => '0'),"NAME");
        $this->load->view('setup/ship_migration/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateSM()
    {
        $id= $this->input->post('id', true);
        $PRE_AREA_ID= $this->input->post('PRE_AREA_ID', true);
        $PRE_ZONE_ID= $this->input->post('PRE_ZONE_ID', true);
        $NEW_AREA_ID= $this->input->post('NEW_AREA_ID', true);     
        $nn = $this->db->query("SELECT * FROM bn_navyadminhierarchy WHERE ADMIN_ID =  $NEW_AREA_ID ")->row(); 
        $date1 = explode("-", $this->input->post('migrationDate'));
        $sd = $date1[2] . '-' . $date1[1] . '-' . $date1[0];  
        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_ship_migration", array( 'PREVIOUS_AREA_ID' => $PRE_AREA_ID, 'NEW_AREA_ID' => $NEW_AREA_ID, "SHIP_MIGRATION_ID !=" =>$id));
        if (empty($check)) {// if district name available
            $data = array(
                'NEW_AREA_ID' => $NEW_AREA_ID,
                'NEW_ZONE_ID' => $nn->PARENT_ID,
                'CHANGE_DATE' => $sd,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_ship_migration',$data, array("SHIP_MIGRATION_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Ship migration Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Ship migration Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Ship migration Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT  sm.*, se.NAME SHIP_ESTABLISHMENT, na.NAME PRE_AREA, bn.NAME NEW_AREA
                                            FROM bn_ship_migration sm
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = sm.SHIP_ESTABLISHMENTID
                                            INNER JOIN bn_navyadminhierarchy na on sm.PREVIOUS_AREA_ID = na.ADMIN_ID
                                            INNER JOIN bn_navyadminhierarchy bn on sm.NEW_AREA_ID = bn.ADMIN_ID
                                            WHERE sm.SHIP_MIGRATION_ID = $id")->row();
        $this->load->view('setup/ship_migration/view',$data);
               // echo '<pre>';print_r($data['viewdetails']);exit;

    }
    /**
     * @access public
     * @param 
     * @author   Emdadul Huq <Emdadul@atilimited.net> 
     * @return      ajax area list by zone ID
     */
    public function areaByShipEst() {
        $shipEstId = $_POST['shipEstId'];
        $areaId = $this->db->query("SELECT * FROM bn_ship_establishment WHERE SHIP_ESTABLISHMENTID = $shipEstId")->row_array();
        $query = $this->db->query("SELECT * FROM bn_navyadminhierarchy WHERE ADMIN_ID =  $areaId[AREA_ID] ")->row_array();        
        $query['zoneId'] = $areaId['ZONE_ID'];
        echo json_encode($query);
    }

}
