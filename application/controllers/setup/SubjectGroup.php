<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubjectGroup extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Subject Group';
        $data['result'] = $this->utilities->getAll("bn_subject_group");
        $data['content_view_page'] = 'setup/SubjectGroup/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('setup/SubjectGroup/create');
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function saveSubjectGroup() {
        $CODE = $this->input->post('SubjectCode', true);
        $NAME = $this->input->post('SubjectName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_subject_group", array("NAME" => $NAME));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_subject_group')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Subject Group Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Subject Name insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Subject Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function subjectGroupById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->utilities->findByAttribute("bn_subject_group", array("SUBJECT_GROUPID" => $id));
        $this->load->view('setup/SubjectGroup/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function subjectGroupList() {
        $data['result'] = $this->utilities->getAll("bn_subject_group");
        $this->load->view("setup/SubjectGroup/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->utilities->findByAttribute('bn_subject_group', array('SUBJECT_GROUPID' => $id));
        $this->load->view('setup/SubjectGroup/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updateSubjectGroup() {
        $id = $this->input->post('id', true);
        $CODE = $this->input->post('SubjectCode', true);
        $NAME = $this->input->post('SubjectName', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if name with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_subject_group", array("NAME" => $NAME, "SUBJECT_GROUPID !=" => $id));
        if (empty($check)) {// if name name available
            $data = array(
                'CODE' => $CODE,
                'NAME' => $NAME,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a"));
            if ($this->utilities->updateData('bn_subject_group', $data, array("SUBJECT_GROUPID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Subject Group Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Subject Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Subject Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
//    
//    
//    private function pr($data) {
//        echo "<pre>";
//        print_r($data);
//        echo "</pre>";
//        exit;
//    }
}