<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OrganizationSetup extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        $this->load->model('itemInfoModel');
    }

    /*
     * @methodName Tree Builder()
     * @access
     * @param  none
     * @return  Return Tree as unorder list  
     */

    private function buildTree($flat, $pidKey, $idKey = null) {
        $grouped = array();
        foreach ($flat as $sub) {
            $grouped[$sub[$pidKey]][] = $sub;
        }
        $treeBuilder = function($siblings) use (&$treeBuilder, $grouped, $idKey) {
            foreach ($siblings as $k => $sibling) {
                $id = $sibling[$idKey];
                if (isset($grouped[$id])) {
                    $sibling['children'] = $treeBuilder($grouped[$id]);
                }
                $siblings[$k] = $sibling;
            }
            return $siblings;
        };
        $tree = $treeBuilder($grouped[0]);
        return $tree;
    }

    /*
     * @methodName index()
     * @access public
     * @param  none
     * @return  //Load divition setup page layout  
     */

    public function index() {
        $data['metaTitle'] = 'Setup Module';
        $data['breadcrumbs'] = array(
            'Setup' => 'setup/index',
            'Setup' => '#'
        );
        $data['pageTitle'] = 'Organization Lists';


        $query = $this->db->query("SELECT ad.*, IFNULL(ad.PARENT_ID, 0) P_ID FROM bn_organization_hierarchy ad")->result_array();
        $data['tree'] = $this->buildTree($query, 'P_ID', 'ORG_ID');

        $data['result'] = $this->utilities->findAllFromView('bn_organization_hierarchy');
        $data['content_view_page'] = 'setup/organizationSetup/orgList';
        $this->template->display($data);
    }

    /*
     * @methodName addOrganization()
     * @access public
     * @param  none
     * @return  // none  
     */

    public function addOrganization() {
        $org_id = $this->uri->segment(4);
        if (isset($_POST['organization_name'])) {
            $org_data = array(
                'ORG_CODE' => $this->input->post('org_code'),
                'ORG_NAME' => $this->input->post('organization_name'),
                'BANGLA_NAME' => $this->input->post('organization_bn_name'),
                'PARENT_ID' => $this->input->post('parent_organization'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_FLAG'])) ? 1 : 0,
                'CRE_BY' => '1'
            );

            $this->utilities->insertData($org_data, 'bn_organization_hierarchy');
            $this->session->set_flashdata('success', $this->lang->line('organization') . ' ' . $this->lang->line('cre_success'));
            redirect('setup/organizationSetup');
        }
        $data['org_id'] = $org_id;
        /*$data['organizations'] = $this->db->query("SELECT * FROM bn_organization_hierarchy WHERE PARENT_ID !=0");*/
        $this->load->view('setup/organizationSetup/addOrganization', $data);
    }

    /**
     * @methodName editOrganization()
     * @access public
     * @param  $org_id
     * @return Organizations Details Data
     */
    public function editOrganization($org_id) {
        if (isset($_POST['organization_name'])) {
            $update_org_data = array(
                'ORG_CODE' => $this->input->post('org_code'),
                'ORG_NAME' => $this->input->post('organization_name'),
                'BANGLA_NAME' => $this->input->post('organization_bn_name'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_FLAG'])) ? 1 : 0
            );
            $this->utilities->updateData('bn_organization_hierarchy', $update_org_data, array('ORG_ID' => $org_id));
            $this->session->set_flashdata('success', $this->lang->line('organization') . ' ' . $this->lang->line('upd_success'));
            redirect('setup/organizationSetup');
        }
        /*$data['organizations'] = $this->utilities->dropdownFromTableWithCondition('sav_organization_types', '', 'LOOKUP_DATA_ID', 'LOOKUP_DATA_NAME');*/
        $data['item'] = $this->utilities->findByAttribute('bn_organization_hierarchy', array('ORG_ID' => $org_id));
        $this->load->view('setup/organizationSetup/editOrganization', $data);
    }

    /**
     * @methodName viewOrganization()
     * @access public
     * @param  $org_id
     * @return Organizations Details Data
     */
    public function viewOrganization($org_id) {
        $data['item'] = $this->utilities->findByAttribute("bn_organization_hierarchy", array("ORG_ID" => $org_id));
        $this->load->view('setup/organizationSetup/viewOrganization', $data);
    }

    /**
     * @methodName delOrganization()
     * @access public
     * @param  
     * @return  none
     */
    public function delOrganization($id) {
        if (isset($_POST['fld_confirm'])) {
            $data = $this->utilities->deleteRowByAttribute('bn_organization_hierarchy', array('ORG_ID' => $id));
            redirect('setup/organizationSetup');
        }
        $this->load->view('setup/organizationSetup/deleteOrganization');
    }


    /*
     * @methodName Pr()
     * @access
     * @param  none
     * @return  Debug Function
     */

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }



}

?>