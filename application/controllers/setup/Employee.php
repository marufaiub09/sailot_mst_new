<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Employee extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return  //Load divition setup page layout
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Employee' => '#'
        );
        $data['pageTitle'] = 'Employee Setup';
        $data['employee'] = $this->utilities->findAllFromview('sa_emp_v');
        $data['content_view_page'] = 'setup/employee/index';
        $this->template->display($data);
    }

    /**
     * @methodName Create()
     * @access
     * @param  none
     * @return  //Load divition create page
     */

    public function create() {
    
        if (isset($_POST['religion'])) {
            $data = array(
                'FIRST_NAME' => $this->input->post('first_name'),
                'MIDDLE_NAME' => $this->input->post('middle_name'),
                'LAST_NAME' => $this->input->post('last_name'),
                'FULL_NAME' => $this->input->post('first_name') .' '. $this->input->post('middle_name') .' '. $this->input->post('last_name'),
                'GENDER' => $this->input->post('gender'),
                'MARITAL_STATUS' => $this->input->post('marital'),
                'SALUTATION_ID' => $this->input->post('salutation'),
                'RELIGION_ID' => $this->input->post('religion'),
                'MOBILE' => $this->input->post('mobile'),
                'EMAIL' => $this->input->post('email'),
                'JOINING_DT' => ($this->input->post('date') == '') ? '' : date('Y-m-d', strtotime($this->input->post('date'))),
                'NID' => $this->input->post('nid'),
                'FATHER_NAME' => $this->input->post('father_name'),
                'MOTHER_NAME' => $this->input->post('mother_name'),
                'ORG_ID' => $this->input->post('organization'),
                'DEPT_ID' => $this->input->post('department'),
                'DESIG_ID' => $this->input->post('degisnation'),
                'RANK_ID' => $this->input->post('rank'),
                'ACTIVE_STATUS' => ($this->input->post('ACTIVE_STATUS') == '') ? 0 : 1,
                'CRE_BY' => $this->session->userdata('NAME'),
            );
            if ($this->utilities->insertData($data, 'sa_emp')) {
                redirect('setup/employee');
            }
        }
        $data['organization'] = $this->utilities->dropdownFromTableWithCondition('sa_organizations', '', 'ORG_ID', 'ORG_NAME');
        $data['department'] = $this->utilities->dropdownFromTableWithCondition('sa_dept', '', 'DEPT_ID', 'DEPT_NAME');
        $data['rank'] = $this->utilities->dropdownFromTableWithCondition('sa_rank', '', 'RANK_ID', 'RANK_NAME');
        $data['degisnation'] = $this->utilities->dropdownFromTableWithCondition('sa_designation', '', 'DESIG_ID', 'DESIG_NAME');
        $data['marital'] = $this->utilities->dropdownFromTableWithCondition('sav_marital_status', '', 'MARITAL_ID', 'MARITAL_NAME');
        $data['salutation'] = $this->utilities->dropdownFromTableWithCondition('sa_salutation', '', 'SALUTATION_ID', 'SALUTATION_NAME');
        $data['religion'] = $this->utilities->dropdownFromTableWithCondition('sav_religion', '', 'RELIGION_ID', 'RELIGION_NAME');
         
        $this->load->view('setup/employee/create', $data);
         
       
    }

    /*
     * @methodName edit()
     * @access
     * @param  none
     * @return  //Load divition create page
     */

    public function edit($id) {
        if (isset($_POST['religion'])) {
            $data = array(
                'FIRST_NAME' => $this->input->post('first_name'),
                'MIDDLE_NAME' => $this->input->post('middle_name'),
                'LAST_NAME' => $this->input->post('last_name'),
                'FULL_NAME' => $this->input->post('first_name') .' '. $this->input->post('middle_name') .' '. $this->input->post('last_name'),
                'GENDER' => $this->input->post('gender'),
                'MARITAL_STATUS' => $this->input->post('marital'),
                'SALUTATION_ID' => $this->input->post('salutation'),
                'RELIGION_ID' => $this->input->post('religion'),
                'MOBILE' => $this->input->post('mobile'),
                'EMAIL' => $this->input->post('email'),
                'JOINING_DT' => ($this->input->post('date') == '') ? '' : date('Y-m-d', strtotime($this->input->post('date'))),
                'NID' => $this->input->post('nid'),
                'FATHER_NAME' => $this->input->post('father_name'),
                'MOTHER_NAME' => $this->input->post('mother_name'),
                'ORG_ID' => $this->input->post('organization'),
                'DEPT_ID' => $this->input->post('department'),
                'DESIG_ID' => $this->input->post('degisnation'),
                'RANK_ID' => $this->input->post('rank'),
                'ACTIVE_STATUS' => ($this->input->post('ACTIVE_STATUS') == '') ? 0 : 1,
                'CRE_BY' => $this->session->userdata('NAME'),
            );
            if ($this->utilities->updateData('sa_emp', $data, array('EMP_ID' => $id))) {
               redirect('setup/employee');
            }
        }
        $data['row'] = $this->utilities->findByAttribute('sa_emp_v', array('EMP_ID' => $id));
        $data['organization'] = $this->utilities->dropdownFromTableWithCondition('sa_organizations', '', 'ORG_ID', 'ORG_NAME');
        $data['department'] = $this->utilities->dropdownFromTableWithCondition('sa_dept', '', 'DEPT_ID', 'DEPT_NAME');
        $data['rank'] = $this->utilities->dropdownFromTableWithCondition('sa_rank', '', 'RANK_ID', 'RANK_NAME');
        $data['degisnation'] = $this->utilities->dropdownFromTableWithCondition('sa_designation', '', 'DESIG_ID', 'DESIG_NAME');
        $data['marital'] = $this->utilities->dropdownFromTableWithCondition('sav_marital_status', '', 'MARITAL_ID', 'MARITAL_NAME');
        $data['salutation'] = $this->utilities->dropdownFromTableWithCondition('sa_salutation', '', 'SALUTATION_ID', 'SALUTATION_NAME');
        $data['religion'] = $this->utilities->dropdownFromTableWithCondition('sav_religion', '', 'RELIGION_ID', 'RELIGION_NAME');
        $this->load->view('setup/employee/edit', $data);
    }

    /*
     * @methodName view()
     * @access
     * @param  none
     * @return  //
     */

    public function view($id) {
        $m=$data['viewdetails'] = $this->utilities->findByAttribute('sa_emp_v', array('EMP_ID' => $id));
        $this->load->view('setup/employee/view', $data);
    }



     /*
     * @methodName view()
     * @access
     * @param  none
     * @return  //
     */
    public function delete($id)
    {
        $this->utilities->deleteRowByAttribute('sa_emp',array('EMP_ID' => $id));
        redirect('setup/employee');
    }
    public function properTyByOrgId()
    {
        $orgId=$this->input->post('orgId');
        $data['department'] = $this->utilities->dropdownFromTableWithCondition('sa_dept', 'none', 'DEPT_ID', 'DEPT_NAME', array("ORG_ID" => $orgId));
        $data['rank'] = $this->utilities->dropdownFromTableWithCondition('sa_rank', 'none', 'RANK_ID', 'RANK_NAME', array("ORG_ID" => $orgId));
        $data['designation'] =$this->utilities->dropdownFromTableWithCondition("sa_designation", "none", "DESIG_ID", "DESIG_NAME", array("ORG_ID" => $orgId));
        echo json_encode($data);
       // echo $this->load->view('setup/employee/ajaxDepDesRank',$data,TRUE);
    }

    /*
     * @methodName Pr()
     * @access
     * @param  none
     * @return  Debug Function
     */

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}

?>