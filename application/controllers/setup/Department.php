<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access public
     * @param  none
     * @return  // department details
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Department Setup';
        $data['departments'] = $this->utilities->findAllFromView('sa_dept');
        $data['content_view_page'] = 'setup/department/index';
        $this->template->display($data);
    }

    /*
     * @methodName Create()
     * @access public
     * @param  none
     * @return  // none
     */

    public function create() {
        if (isset($_POST['DEPT_NAME'])) {
            $dept_data = array(
                'DEPT_NAME' => $this->input->post('DEPT_NAME'),
                'DEPT_DESC' => $this->input->post('dept_desc'),
                'PARENT_DEPT_ID' => $this->input->post('PARENT_DEPT_ID'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'CRE_BY' => $this->user_session["USER_ID"]
            );


            if ($this->utilities->insertData($dept_data, 'sa_dept')) {
                redirect('setup/department/index');
            }
        }
        $data['departments'] = $this->utilities->dropdownFromTableWithCondition('sa_dept', '-- Select one --', 'DEPT_ID', 'DEPT_NAME');
        $this->load->view('setup/department/create', $data);
    }

    /*
     * @methodName edit()
     * @access
     * @param   $DEPT_ID
     * @return  // department details
     */

    public function edit($DEPT_ID) {

        if (isset($_POST['DEPT_NAME'])) {
            $dept_data = array(
                'DEPT_NAME' => $this->input->post('DEPT_NAME'),
                'DEPT_DESC' => $this->input->post('dept_desc'),
                'PARENT_DEPT_ID' => $this->input->post('PARENT_DEPT_ID'),
                'ACTIVE_STATUS' => (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0,
                'UPD_DT' => date("Y-m-d H:i:s"),
                'UPD_BY' => $this->user_session["USER_ID"]
            );

            if ($this->utilities->updateData('sa_dept', $dept_data, array('DEPT_ID' => $DEPT_ID))) {
                redirect('setup/department/index');
            }
        }
        $data['departments'] = $this->utilities->findByAttribute('sa_dept', array('DEPT_ID' => $DEPT_ID));
        $data['all_dept'] = $this->utilities->dropdownFromTableWithCondition('sa_dept', '', 'DEPT_ID', 'DEPT_NAME');
        $this->load->view('setup/department/edit', $data);
    }

    /*
     * @methodName view()
     * @access
     * @param   $DEPT_ID
     * @return  // department details
     */

    public function view($DEPT_ID) {
        $data['departments'] = $this->utilities->findByAttribute('sa_dept', array('DEPT_ID' => $DEPT_ID));
        $parent_dept = $data['departments']->PARENT_DEPT_ID;
        $data['parent'] = $this->utilities->findByAttribute('sa_dept', array('PARENT_DEPT_ID' => $parent_dept));
        $this->load->view('setup/department/view', $data);
    }

    private function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}