<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   PromotionRoaster
 * @package    Promotion Roster
 * @author     Maruf <ahasan@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class PromotionRoster extends CI_Controller {

		private $now;

		public function __construct() {
				parent::__construct();
				$this->user_session = $this->session->userdata('logged_in');
				if (!$this->user_session) {
						redirect('auth/index');
				}
				date_default_timezone_set("Asia/Dhaka");
				$this->now = date('Y-m-d H:i:s', time());
		}

		/**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      templete
		 */
		public function index() {
				$data['breadcrumbs'] = array(
						'Modules' => '#'
				);
				$data['branch'] = $this->utilities->findAllByAttribute("bn_branch", array("ACTIVE_STATUS" => 1));
				$data['code'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 3));
				$data['exam'] = $this->utilities->findAllByAttribute("bn_navyexam_hierarchy", array("ACTIVE_STATUS" => 1, "EXAM_TYPE" => 3));
				$data['medal'] = $this->utilities->findAllByAttribute("bn_medal", array("ACTIVE_STATUS" => 1));
				$data['honor'] = $this->utilities->findAllByAttribute("bn_honor", array("ACTIVE_STATUS" => 1));
				$data['pageTitle'] = 'Sailor Pension Information';
				$data['content_view_page'] = 'promotion/promotion_roster/index';
				$this->template->display($data);
		}

		/**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Dao Group Wise Branch Name
		 */
		function branchName() {
				$rad = $_POST['rad'];
				$query = $this->utilities->findAllByAttribute('bn_branch', array("ACTIVE_STATUS" => 1, "DAO_GROUPID!=" => 2));
				$returnVal = '<option value = "">Select Branch</option>';
				if (!empty($query)) {
						foreach ($query as $row) {
								$returnVal .= '<option value = "' . $row->BRANCH_ID . '">' . $row->BRANCH_NAME . '</option>';
						}
				}
				echo $returnVal;
		}

		/**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Select Radio Wise Branch Name
		 */
		function branchNameByIn() {
				$rad = $_POST['rad'];
				if ($rad == 1) {
						$query = $this->utilities->findAllByAttribute('bn_branch', array("ACTIVE_STATUS" => 1, "DAO_GROUPID!=" => 2));
						$returnVal = '<option value = "">Select Branch</option>';
						if (!empty($query)) {
								foreach ($query as $row) {
										$returnVal .= '<option value = "' . $row->BRANCH_ID . '">' . $row->BRANCH_NAME . '</option>';
								}
						}
						echo $returnVal;
				} else {
						$query = $this->utilities->findAllByAttribute('bn_branch', array("ACTIVE_STATUS" => 1, "DAO_GROUPID" => 2));
						$returnVal = '<option value = "">Select Branch</option>';
						if (!empty($query)) {
								foreach ($query as $row) {
										$returnVal .= '<option value = "' . $row->BRANCH_ID . '">' . $row->BRANCH_NAME . '</option>';
								}
						}
						echo $returnVal;
				}
		}

		/**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Promotion Criteria
		 */
		public function promotion() {
				$rank = $this->input->post("rank");
				$rad = $this->input->post("rad");
				$cr = $this->input->post("cr");
//        Query For Order criteria in Promotion
				$data['order'] = $this->db->query("SELECT obr.ValueID,obr.Description,obr.Position
				FROM OrderByRoaster obr
						 LEFT JOIN promotionroaster pr ON pr.RoasterID = obr.RoasterID
			 WHERE pr.RankID != $rank AND  pr.RoasterCriteria = $cr")->result();

//        Query For Service criteria in Promotion
				$service = $this->db->query("SELECT pm.RoasterID,pm.SeaService,
				 pm.RankServiceID,
				 pm.CSSRankID,
				 pm.IsReqCSS AS IsRCSService,
				 (SELECT r.RANK_NAME
						FROM bn_rank r
					 WHERE r.rank_id = pm.CSSRankID)
						AS RCSServive,
				 (SELECT r.RANK_NAME
						FROM bn_rank r
					 WHERE r.rank_id = pm.RankServiceID)
						AS StrtingRank,
				 pm.SeaServicePoint AS See_Service,
				 pm.RankServicePoint AS Rank_Service,
				 pm.InstructorServicePoint AS Instructor_Service,
				 pm.ServiceLengthPoint AS Service_Length,
				 pm.EffiGainValue AS Given_Value,
				 pm.EffiMaxValue AS maximumValue,
				 pm.IsRecomMandatory AS MRforPromotion,
				 pm.RecomCriteria,
				 (SELECT CASE pm.RecomCriteria
										WHEN 1 THEN 'D(Seniority Date)'
										WHEN 2 THEN 'E(Exam Date)'
										WHEN 3 THEN 'T (Training Date)'
										WHEN 4 THEN 'A (Rank)'
										ELSE NULL
								 END)
						AS Recom_From,
						pm.RecomGainValue as Give_Value,
						pm.RecomMaxValue as Maximum_Value,
						pm.IsCalSeniority as Calculate_Senority,
						pm.DueByTimeGainValue as RSPPMADTime,
						pm.ContiniousVG as Continuas_VG,
						pm.DueByTime as Due_by_Time,
						pm.ReportHeader1 as ReportCriteriaHeading1,
						pm.ReportHeader2 as ReportCriteriaHeading2
						FROM promotionroaster pm
					 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = $cr")->row();

				//Query For Training criteria in Promotion

				$training = $this->db->query("SELECT bn.NAVYTrainingID,bn.code,
			 bn.name,
			 q.GainValue AS Gain,
			 q.IsMandatory AS Is_Mandatory,
			 q.CourseDateColumn,
			 q.ValueID,
			 (SELECT CASE q.CourseDateColumn
									WHEN 1 THEN 'Course-1(Date)'
									WHEN 2 THEN 'Course-2(Date)'
									WHEN 3 THEN 'Course-3(Date)'
									ELSE NULL
							 END)
					AS Output_Column
						FROM bn_navytraininghierarchy bn
			 LEFT JOIN qualificationroaster q ON q.TrainingID = bn.NAVYTrainingID
			 LEFT JOIN promotionroaster pm ON pm.RoasterID = q.RoasterID
			 LEFT JOIN bn_rank r ON r.RANK_ID = pm.RankID
 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = $cr")->result();

				//Query For Exam criteria in Promotion

				$exam = $this->db->query(" SELECT bn.EXAM_ID,bn.code,bn.name,q.CourseDateColumn,q.GainValue,q.IsMandatory,q.ValueID,(SELECT CASE q.CourseDateColumn
									WHEN 1 THEN 'Course-1(Date)'
									WHEN 2 THEN 'Course-2(Date)'
									WHEN 3 THEN 'Course-3(Date)'
									ELSE NULL
							 END)
					AS Output_Column
	FROM bn_navyexam_hierarchy bn
			 LEFT JOIN qualificationroaster q ON q.ExamID = bn.EXAM_ID
			 LEFT JOIN promotionroaster pm ON pm.RoasterID = q.RoasterID
 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = $cr")->result();

				//Query For Recommendation criteria in Promotion

				$recomnSelect = $this->db->query("SELECT re.ValueID,re.RecomNo as RecoNo,
			 re.GainValue as Gain
	FROM recommroaster re
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = re.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = $cr")->result();

				//Query For Recommendation Rank Branch criteria in Promotion

				$recomnBranch = $this->db->query("SELECT p.RoasterID,p.IsRecomMandatory AS mendatory_recommendation_for_promotion,p.RecomCriteria,
			 (SELECT CASE p.RecomCriteria
									WHEN 1 THEN 'D(Seniority Date)'
									WHEN 2 THEN 'E(Exam Date)'
									WHEN 3 THEN 'T (Training Date)'
									WHEN 4 THEN 'A (Rank)'
									ELSE NULL
							 END)
					AS Recom_From,
			 (SELECT r.RANK_NAME
					FROM bn_rank r
				 WHERE r.rank_id = p.FromRankID)
					AS FromRankID,
			 p.ServiceLengthPoint AS Gain_Value,
			 p.RecomMaxValue AS Maximum_limit
	FROM PromotionRoaster p
 WHERE p.RankID = $rank AND  p.RoasterCriteria = $cr")->row();

				//Query For Medal criteria in Promotion

				$medal = $this->db->query("SELECT bm.MEDAL_ID,bm.CODE, bm.NAME, mr.GainValue,mr.ValueID
	FROM bn_medal bm
			 LEFT JOIN medalhonorroaster mr ON mr.MedalID = bm.MEDAL_ID
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = mr.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = $cr")->result();

				//Query For Honor criteria in Promotion

				$honor = $this->db->query("SELECT bn.HONOR_ID,bn.CODE, bn.NAME, mh.GainValue,mh.ValueID
	FROM bn_honor bn
			 LEFT JOIN medalhonorroaster mh ON mh.HonorID = bn.HONOR_ID
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = mh.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = $cr")->result();

				//Query For OrderBy criteria in Promotion

				$OrderBy = $this->db->query("SELECT obr.ValueID,obr.Description,obr.Position,obr.ValueID
	FROM OrderByRoaster obr
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = obr.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = $cr")->result();
				 //Query For Training Dropdown criteria in Promotion
				$trainCode = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 3));
				$multipleRows = array(
						'service' => $service,
						'training' => $training,
						'exam' => $exam,
						'honor' => $honor,
						'medal' => $medal,
						'recomnSelect' => $recomnSelect,
						'recomnBranch' => $recomnBranch,
						'OrderBy' => $OrderBy,
						'trainCode' => $trainCode
				);
				echo json_encode($multipleRows);
//    echo '<pre>';
//    print_r($multipleRows);
//    exit();
		}

		public function promotion1() {
				$rank = $this->input->post("rank");
				$rad = $this->input->post("rad");
				$cr = $this->input->post("cr");
//        Query For Order criteria in Promotion
				$data['order'] = $this->db->query("SELECT obr.ValueID,obr.Description,obr.Position
				FROM OrderByRoaster obr
						 LEFT JOIN promotionroaster pr ON pr.RoasterID = obr.RoasterID
			 WHERE pr.RankID != $rank AND  pr.RoasterCriteria = 1")->result();

//        Query For Service criteria in Promotion
				$service = $this->db->query("SELECT pm.RoasterID,pm.SeaService,
				 pm.RankServiceID,
				 pm.CSSRankID,
				 pm.IsReqCSS AS IsRCSService,
				 (SELECT r.RANK_NAME
						FROM bn_rank r
					 WHERE r.rank_id = pm.CSSRankID)
						AS RCSServive,
				 (SELECT r.RANK_NAME
						FROM bn_rank r
					 WHERE r.rank_id = pm.RankServiceID)
						AS StrtingRank,
				 pm.SeaServicePoint AS See_Service,
				 pm.RankServicePoint AS Rank_Service,
				 pm.InstructorServicePoint AS Instructor_Service,
				 pm.ServiceLengthPoint AS Service_Length,
				 pm.EffiGainValue AS Given_Value,
				 pm.EffiMaxValue AS maximumValue,
				 pm.IsRecomMandatory AS MRforPromotion,
				 pm.RecomCriteria,
				 (SELECT CASE pm.RecomCriteria
										WHEN 1 THEN 'D(Seniority Date)'
										WHEN 2 THEN 'E(Exam Date)'
										WHEN 3 THEN 'T (Training Date)'
										WHEN 4 THEN 'A (Rank)'
										ELSE NULL
								 END)
						AS Recom_From,
						pm.RecomGainValue as Give_Value,
						pm.RecomMaxValue as Maximum_Value,
						pm.IsCalSeniority as Calculate_Senority,
						pm.DueByTimeGainValue as RSPPMADTime,
						pm.ContiniousVG as Continuas_VG,
						pm.DueByTime as Due_by_Time,
						pm.ReportHeader1 as ReportCriteriaHeading1,
						pm.ReportHeader2 as ReportCriteriaHeading2
						FROM promotionroaster pm
					 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = 1")->row();

				//Query For Training criteria in Promotion

				$training = $this->db->query("SELECT bn.NAVYTrainingID,bn.code,
			 bn.name,
			 q.GainValue AS Gain,
			 q.IsMandatory AS Is_Mandatory,
			 q.CourseDateColumn,
			 q.ValueID,
			 (SELECT CASE q.CourseDateColumn
									WHEN 1 THEN 'Course-1(Date)'
									WHEN 2 THEN 'Course-2(Date)'
									WHEN 3 THEN 'Course-3(Date)'
									ELSE NULL
							 END)
					AS Output_Column
						FROM bn_navytraininghierarchy bn
			 LEFT JOIN qualificationroaster q ON q.TrainingID = bn.NAVYTrainingID
			 LEFT JOIN promotionroaster pm ON pm.RoasterID = q.RoasterID
			 LEFT JOIN bn_rank r ON r.RANK_ID = pm.RankID
 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = 1")->result();

				//Query For Exam criteria in Promotion

				$exam = $this->db->query(" SELECT bn.EXAM_ID,bn.code,bn.name,q.CourseDateColumn,q.GainValue,q.IsMandatory,q.ValueID,(SELECT CASE q.CourseDateColumn
									WHEN 1 THEN 'Course-1(Date)'
									WHEN 2 THEN 'Course-2(Date)'
									WHEN 3 THEN 'Course-3(Date)'
									ELSE NULL
							 END)
					AS Output_Column
	FROM bn_navyexam_hierarchy bn
			 LEFT JOIN qualificationroaster q ON q.ExamID = bn.EXAM_ID
			 LEFT JOIN promotionroaster pm ON pm.RoasterID = q.RoasterID
 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = 1")->result();

				//Query For Recommendation criteria in Promotion

				$recomnSelect = $this->db->query("SELECT re.ValueID,re.RecomNo as RecoNo,
			 re.GainValue as Gain
	FROM recommroaster re
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = re.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 1")->result();

				//Query For Recommendation Rank Branch criteria in Promotion

				$recomnBranch = $this->db->query("SELECT p.RoasterID,p.IsRecomMandatory AS mendatory_recommendation_for_promotion,p.RecomCriteria,
			 (SELECT CASE p.RecomCriteria
									WHEN 1 THEN 'D(Seniority Date)'
									WHEN 2 THEN 'E(Exam Date)'
									WHEN 3 THEN 'T (Training Date)'
									WHEN 4 THEN 'A (Rank)'
									ELSE NULL
							 END)
					AS Recom_From,
			 (SELECT r.RANK_NAME
					FROM bn_rank r
				 WHERE r.rank_id = p.FromRankID)
					AS FromRankID,
			 p.ServiceLengthPoint AS Gain_Value,
			 p.RecomMaxValue AS Maximum_limit
	FROM PromotionRoaster p
 WHERE p.RankID = $rank AND  p.RoasterCriteria = 1")->row();

				//Query For Medal criteria in Promotion

				$medal = $this->db->query("SELECT bm.MEDAL_ID,bm.CODE, bm.NAME, mr.GainValue,mr.ValueID
	FROM bn_medal bm
			 LEFT JOIN medalhonorroaster mr ON mr.MedalID = bm.MEDAL_ID
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = mr.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 1")->result();

				//Query For Honor criteria in Promotion

				$honor = $this->db->query("SELECT bn.HONOR_ID,bn.CODE, bn.NAME, mh.GainValue,mh.ValueID
	FROM bn_honor bn
			 LEFT JOIN medalhonorroaster mh ON mh.HonorID = bn.HONOR_ID
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = mh.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 1")->result();

				//Query For OrderBy criteria in Promotion

				$OrderBy = $this->db->query("SELECT obr.ValueID,obr.Description,obr.Position,obr.ValueID
	FROM OrderByRoaster obr
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = obr.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 1")->result();
				 //Query For Training Dropdown criteria in Promotion
				$trainCode = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 3));
				$multipleRows = array(
						'service' => $service,
						'training' => $training,
						'exam' => $exam,
						'honor' => $honor,
						'medal' => $medal,
						'recomnSelect' => $recomnSelect,
						'recomnBranch' => $recomnBranch,
						'OrderBy' => $OrderBy,
						'trainCode' => $trainCode
				);
				echo json_encode($multipleRows);
//    echo '<pre>';
//    print_r($multipleRows);
//    exit();
		}

		public function promotion2() {
			$rank = $this->input->post("rank");
				$rad = $this->input->post("rad");
				$cr = $this->input->post("cr");
//        Query For Order criteria in Promotion
				$data['order'] = $this->db->query("SELECT obr.ValueID,obr.Description,obr.Position
				FROM OrderByRoaster obr
						 LEFT JOIN promotionroaster pr ON pr.RoasterID = obr.RoasterID
			 WHERE pr.RankID != $rank AND  pr.RoasterCriteria = 2")->result();

//        Query For Service criteria in Promotion
				$service = $this->db->query("SELECT pm.RoasterID,pm.SeaService,
				 pm.RankServiceID,
				 pm.CSSRankID,
				 pm.IsReqCSS AS IsRCSService,
				 (SELECT r.RANK_NAME
						FROM bn_rank r
					 WHERE r.rank_id = pm.CSSRankID)
						AS RCSServive,
				 (SELECT r.RANK_NAME
						FROM bn_rank r
					 WHERE r.rank_id = pm.RankServiceID)
						AS StrtingRank,
				 pm.SeaServicePoint AS See_Service,
				 pm.RankServicePoint AS Rank_Service,
				 pm.InstructorServicePoint AS Instructor_Service,
				 pm.ServiceLengthPoint AS Service_Length,
				 pm.EffiGainValue AS Given_Value,
				 pm.EffiMaxValue AS maximumValue,
				 pm.IsRecomMandatory AS MRforPromotion,
				 pm.RecomCriteria,
				 (SELECT CASE pm.RecomCriteria
										WHEN 1 THEN 'D(Seniority Date)'
										WHEN 2 THEN 'E(Exam Date)'
										WHEN 3 THEN 'T (Training Date)'
										WHEN 4 THEN 'A (Rank)'
										ELSE NULL
								 END)
						AS Recom_From,
						pm.RecomGainValue as Give_Value,
						pm.RecomMaxValue as Maximum_Value,
						pm.IsCalSeniority as Calculate_Senority,
						pm.DueByTimeGainValue as RSPPMADTime,
						pm.ContiniousVG as Continuas_VG,
						pm.DueByTime as Due_by_Time,
						pm.ReportHeader1 as ReportCriteriaHeading1,
						pm.ReportHeader2 as ReportCriteriaHeading2
						FROM promotionroaster pm
					 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = 2")->row();

				//Query For Training criteria in Promotion

				$training = $this->db->query("SELECT bn.NAVYTrainingID,bn.code,
			 bn.name,
			 q.GainValue AS Gain,
			 q.IsMandatory AS Is_Mandatory,
			 q.CourseDateColumn,
			 q.ValueID,
			 (SELECT CASE q.CourseDateColumn
									WHEN 1 THEN 'Course-1(Date)'
									WHEN 2 THEN 'Course-2(Date)'
									WHEN 3 THEN 'Course-3(Date)'
									ELSE NULL
							 END)
					AS Output_Column
						FROM bn_navytraininghierarchy bn
			 LEFT JOIN qualificationroaster q ON q.TrainingID = bn.NAVYTrainingID
			 LEFT JOIN promotionroaster pm ON pm.RoasterID = q.RoasterID
			 LEFT JOIN bn_rank r ON r.RANK_ID = pm.RankID
 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = 2")->result();

				//Query For Exam criteria in Promotion

				$exam = $this->db->query(" SELECT bn.EXAM_ID,bn.code,bn.name,q.CourseDateColumn,q.GainValue,q.IsMandatory,q.ValueID,(SELECT CASE q.CourseDateColumn
									WHEN 1 THEN 'Course-1(Date)'
									WHEN 2 THEN 'Course-2(Date)'
									WHEN 3 THEN 'Course-3(Date)'
									ELSE NULL
							 END)
					AS Output_Column
	FROM bn_navyexam_hierarchy bn
			 LEFT JOIN qualificationroaster q ON q.ExamID = bn.EXAM_ID
			 LEFT JOIN promotionroaster pm ON pm.RoasterID = q.RoasterID
 WHERE pm.RankID = $rank AND  pm.RoasterCriteria = 2")->result();

				//Query For Recommendation criteria in Promotion

				$recomnSelect = $this->db->query("SELECT re.ValueID,re.RecomNo as RecoNo,
			 re.GainValue as Gain
	FROM recommroaster re
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = re.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 2")->result();

				//Query For Recommendation Rank Branch criteria in Promotion

				$recomnBranch = $this->db->query("SELECT p.RoasterID,p.IsRecomMandatory AS mendatory_recommendation_for_promotion,p.RecomCriteria,
			 (SELECT CASE p.RecomCriteria
									WHEN 1 THEN 'D(Seniority Date)'
									WHEN 2 THEN 'E(Exam Date)'
									WHEN 3 THEN 'T (Training Date)'
									WHEN 4 THEN 'A (Rank)'
									ELSE NULL
							 END)
					AS Recom_From,
			 (SELECT r.RANK_NAME
					FROM bn_rank r
				 WHERE r.rank_id = p.FromRankID)
					AS FromRankID,
			 p.ServiceLengthPoint AS Gain_Value,
			 p.RecomMaxValue AS Maximum_limit
	FROM PromotionRoaster p
 WHERE p.RankID = $rank AND  p.RoasterCriteria = 2")->row();

				//Query For Medal criteria in Promotion

				$medal = $this->db->query("SELECT bm.MEDAL_ID,bm.CODE, bm.NAME, mr.GainValue,mr.ValueID
	FROM bn_medal bm
			 LEFT JOIN medalhonorroaster mr ON mr.MedalID = bm.MEDAL_ID
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = mr.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 2")->result();

				//Query For Honor criteria in Promotion

				$honor = $this->db->query("SELECT bn.HONOR_ID,bn.CODE, bn.NAME, mh.GainValue,mh.ValueID
	FROM bn_honor bn
			 LEFT JOIN medalhonorroaster mh ON mh.HonorID = bn.HONOR_ID
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = mh.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 2")->result();

				//Query For OrderBy criteria in Promotion

				$OrderBy = $this->db->query("SELECT obr.ValueID,obr.Description,obr.Position,obr.ValueID
	FROM OrderByRoaster obr
			 LEFT JOIN promotionroaster pr ON pr.RoasterID = obr.RoasterID
 WHERE pr.RankID = $rank AND  pr.RoasterCriteria = 2")->result();
				 //Query For Training Dropdown criteria in Promotion
				$trainCode = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 3));
				$multipleRows = array(
						'service' => $service,
						'training' => $training,
						'exam' => $exam,
						'honor' => $honor,
						'medal' => $medal,
						'recomnSelect' => $recomnSelect,
						'recomnBranch' => $recomnBranch,
						'OrderBy' => $OrderBy,
						'trainCode' => $trainCode
				);
				echo json_encode($multipleRows);
//    echo '<pre>';
//    print_r($multipleRows);
//    exit();
		}
		 function trainingSelect() {
				$id = $_POST['id'];
				$query = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 3));
				$returnVal = '<option value = "">Select One</option>';
				if (!empty($query)) {
						foreach ($query as $row) {
								$returnVal .= '<option value = "' . $row->NAVYTrainingID . '">"'.$row->Name . '"</option>';
						}
				}
				echo $returnVal;
		}

		/**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return
		 */
 public function save() {
				$calculationBase = $this->input->post('calculationBase', true);
				$rosterCriteria = $this->input->post('rosterCriteria', true);
				$branch_id = $this->input->post('branch', true);
				$currRank = $this->input->post('currRank', true);
				/* Start Service Part (promotionroaster) */
				$prSeaService = $this->input->post('prSeaservice', true);
				$seaService = $this->input->post('seaService', true);
				$RCSS = $this->input->post('RCSS', true);
				$CombainSeeService = $this->input->post('CombainSeeService', true);
				$StartingService = $this->input->post('StartingService', true);
				$gainValue = $this->input->post('gainValue', true);
				$maximumLimit = $this->input->post('maximumLimit', true);
				$RankServicePoint = $this->input->post('RankServicePoint', true);
				$RankService = $this->input->post('RankService', true);
				$InstructorService = $this->input->post('InstructorService', true);
				$ServiceLength = $this->input->post('ServiceLength', true);
				/* End Service Part (promotionroaster) */
				/* Training/Course List (qualificationroaster) */
				$prtrain = $this->input->post('prtrain', true);
				$code = $this->input->post('code', true);
				$codetrain = $this->input->post('codetrain', true);
				$valuetrain = $this->input->post('valuetrain', true);
				$gain = $this->input->post('gain', true);
				$isMandatory = $this->input->post('isMandatory', true);
				$outputColumn = $this->input->post('outputColumn', true);

				/* Exam Test List (qualificationroaster) */
				$prexam = $this->input->post('prexam', true);
				$codeExam = $this->input->post('examcode', true);
				$codeexam = $this->input->post('codeexam',true);
				$valueexam = $this->input->post('valueexam', true);
				$examgain = $this->input->post('examgain', true);
				$examisMandatory = $this->input->post('examisMandatory', true);
				$examoutputColumn = $this->input->post('examoutputColumn', true);
				/* End All Round Qualifications (promotionroaster) */
				/* Start Recommendation (promotionroaster) */
				$MRfPromotion = $this->input->post('MRfPromotion', true);
				$branchFrom = $this->input->post('branchFrom', true);
				$rank_from = $this->input->post('rank_from', true);
				$RecomFrom = $this->input->post('RecomFrom', true);
				$prrecomn = $this->input->post('prrecomn', true);
				$recomnRec = $this->input->post('recomnRec', true);
				$recomnGain = $this->input->post('recomnGain', true);
				$recomnGaintext = $this->input->post('recomnGaintext', true);
				$recomnMax = $this->input->post('recomnMax', true);
				/* End Recommendation (promotionroaster) */

				/* Medal List (medalhonorroaster) differ with medal_id,Honor_id */
				$prmedal = $this->input->post('prmedal', true);
				$medalName = $this->input->post('medalName', true);
				$medalGain = $this->input->post('medalGain', true);
				$MedalID = $this->input->post('MedalID', true);
				$medalValue = $this->input->post('medalValue', true);

				/* Honer List (medalhonorroaster)differ with medal_id,Honor_id */
				$prhonor = $this->input->post('prhonor', true);
				$HonorID = $this->input->post('HonorID', true);
				$honorValue = $this->input->post('honorValue', true);
				$honorName = $this->input->post('honorName', true);
				$honorGain = $this->input->post('honorGain', true);
				/* End Medal/Honor (promotionroaster) */

				/* Start Others(promotionroaster) */
				$CSPRank = $this->input->post('CSPRank', true);
				$continuousVg = $this->input->post('continuousVg', true);
				$RSPPMADTime = $this->input->post('RSPPMADTime', true);
				$dueByTime = $this->input->post('dueByTime');
				$RCHeading1 = $this->input->post('RCHeading1', true);
				$RCHeading2 = $this->input->post('RCHeading2', true);
				/* End Others(promotionroaster) */

				/* Start OrderBY(promotionroaster) */
				$orderby = $this->input->post('orderby', true);
				$prorder = $this->input->post('prorder', true);
				$position = $this->input->post('position', true);
				/*Start : For CheckBox in Training*/
				for ($i = 0; $i < count($codetrain); $i++) {
					$j = $i+1;
					$isMandatory = $this->input->post("isMandatory_$j", true);
					if(empty($isMandatory)){
							$isMandatory = 0;
					}

				}
			/*End : For CheckBox in Training*/
			/*Start : For CheckBox in Exam*/
				for ($i = 0; $i < count($codeexam); $i++) {
					$j = $i+1;
					$examisMandatory = $this->input->post("examisMandatory_$j", true);
					if(empty($examisMandatory)){
							$examisMandatory = 0;
					}

				}
			 /*End : For CheckBox in Exam*/

				/*var_dump($orderby);
				var_dump($prorder);
				var_dump($position);

			 exit();*/
				$success = 0;
				if ($prSeaService <= 0) {
						$promotionRosterData = array(
								'BranchID' => $branch_id,
								'RankID' => $currRank,
								'RoasterCriteria' => $rosterCriteria,
								'CalculationBase' => $calculationBase,
								'SeaService' => $seaService,
								'IsReqCSS' => $RCSS,
								'CSSRankID' => $CombainSeeService,
								'SeaServicePoint' => $RankServicePoint,
								'RankServiceID' => $StartingService,
								'EffiGainValue' => $gainValue,
								'EffiMaxValue' => $maximumLimit,
								'SeaServicePoint' => $RankServicePoint,
								'RankServicePoint' => $RankService,
								'InstructorServicePoint' => $InstructorService,
								'ServiceLengthPoint' => $ServiceLength,
								'IsRecomMandatory' => $MRfPromotion,
								'RecomCriteria' => $RecomFrom,
								'FromRankID' => $rank_from,
								'RecomGainValue' => $recomnGaintext,
								'RecomMaxValue' => $recomnMax,
								'IsCalSeniority' => $CSPRank,
								'ContiniousVG' => $continuousVg,
								'DueByTimeGainValue' => $RSPPMADTime,
								'DueByTime' => $dueByTime,
								'ReportHeader1' => $RCHeading1,
								'ReportHeader2' => $RCHeading2,
								'CRE_BY' => $this->user_session["USER_ID"],
								'CRE_DT' => date("Y-m-d h:i:s a")
						);
						if ($promotionId = $this->utilities->insert('promotionroaster', $promotionRosterData))
								$success = 1 ;
				} else {
						$promotionRosterData = array(
								'BranchID' => $branch_id,
								'RankID' => $currRank,
								'RoasterCriteria' => $rosterCriteria,
								'CalculationBase' => $calculationBase,
								'SeaService' => $seaService,
								'IsReqCSS' => $RCSS,
								'CSSRankID' => $CombainSeeService,
								'SeaServicePoint' => $RankServicePoint,
								'RankServiceID' => $StartingService,
								'EffiGainValue' => $gainValue,
								'EffiMaxValue' => $maximumLimit,
								'SeaServicePoint' => $RankServicePoint,
								'RankServicePoint' => $RankService,
								'InstructorServicePoint' => $InstructorService,
								'ServiceLengthPoint' => $ServiceLength,
								'IsRecomMandatory' => $MRfPromotion,
								'RecomCriteria' => $RecomFrom,
								'FromRankID' => $rank_from,
								'RecomGainValue' => $recomnGaintext,
								'RecomMaxValue' => $recomnMax,
								'IsCalSeniority' => $CSPRank,
								'ContiniousVG' => $continuousVg,
								'DueByTimeGainValue' => $RSPPMADTime,
								'DueByTime' => $dueByTime,
								'ReportHeader1' => $RCHeading1,
								'ReportHeader2' => $RCHeading2,
								'UPD_BY' => $this->user_session["USER_ID"],
								'UPD_DT' => date("Y-m-d:H-i-s")
						);
						if ($this->utilities->updateData('promotionroaster', $promotionRosterData, array(
												"RoasterID" => $prSeaService
										)))
								$success = 1 ;
				}
				/*Start: INSERT AND UPDATE FOR TRAINING*/
				for ($i = 0; $i < count($codetrain); $i++) {
					$j = $i+1;
					$isMandatory = $this->input->post("isMandatory_$j", true);
					if(empty($isMandatory)){
							$isMandatory = 0;
					}
						if ($i >= count($prtrain)) {
								$qualifiRosTrain = array(
										'RoasterID' => $prSeaService,
										'TrainingID' => $codetrain[$i],
										'GainValue' => $gain[$i],
										'IsMandatory' => $isMandatory,
										'CourseDateColumn' => $outputColumn[$i],
										'CRE_BY' => $this->user_session["USER_ID"],
										'CRE_DT' => date("Y-m-d h:i:s a")
								);
								if ($this->utilities->insertData($qualifiRosTrain, 'qualificationroaster'))
										$success = 1 ;
						} else {
								$qualifiRosTrain = array(
										'RoasterID' => $prSeaService,
										'TrainingID' => $codetrain[$i],
										'GainValue' => $gain[$i],
										'IsMandatory' => $isMandatory,
										'CourseDateColumn' => $outputColumn[$i],
										'UPD_BY' => $this->user_session["USER_ID"],
										'UPD_DT' => date("Y-m-d:H-i-s")
								);
								if ($this->utilities->updateData('qualificationroaster', $qualifiRosTrain, array(
														"ValueID" => $valuetrain[$i]
												)))
										$success = 1;
						}
				}
				/*END: INSERT AND UPDATE FOR TRAINING*/

				/*Start: INSERT AND UPDATE FOR EXAM*/
				 for ($i = 0; $i < count($codeexam); $i++) {
					$j = $i+1;
					$examisMandatory = $this->input->post("examisMandatory_$j", true);
					if(empty($examisMandatory)){
							$examisMandatory = 0;
					}
						if ($i >= count($prexam)) {

								$qualifiRosExam = array(
										'RoasterID' => $prSeaService,
										'ExamID' => $codeexam[$i],
										'GainValue' => $examgain[$i],
										'IsMandatory' => $examisMandatory,
										'CourseDateColumn' => $examoutputColumn[$i],
										'CRE_BY' => $this->user_session["USER_ID"],
										'CRE_DT' => date("Y-m-d h:i:s a")
								);
								if ($this->utilities->insertData($qualifiRosExam, 'qualificationroaster'))
										$success = 1 ;
						} else {
								$qualifiRosExam = array(
										'RoasterID' => $prSeaService,
										'ExamID' => $codeexam[$i],
										'GainValue' => $examgain[$i],
										'IsMandatory' => $examisMandatory,
										'CourseDateColumn' => $examoutputColumn[$i],
										'UPD_BY' => $this->user_session["USER_ID"],
										'UPD_DT' => date("Y-m-d:H-i-s")
								);
								if ($this->utilities->updateData('qualificationroaster', $qualifiRosExam, array(
														"ValueID" => $valueexam[$i]
												)))
										$success = 1 ;
						}
				}
				/*END: INSERT AND UPDATE FOR EXAM*/

				/*Start: INSERT AND UPDATE FOR Recommndation*/
				for ($i = 0; $i < count($recomnRec); $i++) {
						if ($i >= count($prrecomn)) {
								$recomroster = array(
										'RoasterID' => $prSeaService,
										'RecomNo' => $recomnRec[$i],
										'GainValue' => $recomnGain[$i],
										'CRE_BY' => $this->user_session["USER_ID"],
										'CRE_DT' => date("Y-m-d h:i:s a")
								);
								if ($this->utilities->insertData($recomroster, 'recommroaster'))
										$success = 1;
						} else {
								$recomroster = array(
										'RoasterID' => $prSeaService,
										'RecomNo' => $recomnRec[$i],
										'GainValue' => $recomnGain[$i],
										'UPD_BY' => $this->user_session["USER_ID"],
										'UPD_DT' => date("Y-m-d:H-i-s")
								);
								if ($this->utilities->updateData('recommroaster', $recomroster, array(
														"ValueID" => $prrecomn[$i]
												)))
										$success = 1;
						}
				}
				/*END: INSERT AND UPDATE FOR Recommndation*/

				/*Start: INSERT AND UPDATE FOR Medal*/
				for ($i = 0; $i < count($MedalID); $i++) {
						if ($i >= count($prmedal)) {

								$medulInfo = array(
										'RoasterID' => $prSeaService,
										'MedalID' => $MedalID[$i],
										'GainValue' => $medalGain[$i],
										'CRE_BY' => $this->user_session["USER_ID"],
										'CRE_DT' => date("Y-m-d h:i:s a")
								);

								if ($this->utilities->insertData($medulInfo, 'medalhonorroaster'))
										$success = 1;
						} else {

								$medulInfo = array(
										'RoasterID' => $prSeaService,
										'MedalID' => $MedalID[$i],
										'GainValue' => $medalGain[$i],
										'UPD_BY' => $this->user_session["USER_ID"],
										'UPD_DT' => date("Y-m-d:H-i-s")
								);

								if ($this->utilities->updateData('medalhonorroaster', $medulInfo, array(
														"ValueID" => $medalValue[$i]
												)))
										$success = 1;
						}
				}
				/*END: INSERT AND UPDATE FOR Medal*/

				/*Start: INSERT AND UPDATE FOR Honor*/
				for ($i = 0; $i < count($HonorID); $i++) {
						if ($i >= count($prhonor)) {
								$honorInfo = array(
										'RoasterID' => $prSeaService,
										'HonorID' => $HonorID[$i],
										'GainValue' => $honorGain[$i],
										'CRE_BY' => $this->user_session["USER_ID"],
										'CRE_DT' => date("Y-m-d h:i:s a")
								);
								if ($this->utilities->insertData($honorInfo, 'medalhonorroaster'))
										$success = 1;
						} else {
								$honorInfo = array(
										'RoasterID' => $prSeaService,
										'HonorID' => $HonorID[$i],
										'GainValue' => $honorGain[$i],
										'UPD_BY' => $this->user_session["USER_ID"],
										'UPD_DT' => date("Y-m-d:H-i-s")
								);
								if ($this->utilities->updateData('medalhonorroaster', $honorInfo, array(
														"ValueID" => $honorValue[$i]
												)))
										$success = 1;
						}
				}
				/*END: INSERT AND UPDATE FOR Honor*/

				/*Start: INSERT AND UPDATE FOR OrderBy*/
				for ($i = 0; $i < count($orderby); $i++) {
						if ($i >= count($prorder)) {
								$orderInfo = array(
										'RoasterID' => $prSeaService,
										'Description' => $orderby[$i],
										'Position' => $position[$i],
										'CRE_BY' => $this->user_session["USER_ID"],
										'CRE_DT' => date("Y-m-d h:i:s a")
								);
								if ($this->utilities->insertData($orderInfo, 'orderbyroaster'))
									 $success = 1 ;
						}/*else
						{
							$success = 0;
								$orderInfo = array(
										'RoasterID' => $prSeaService,
										'Description' => $orderby[$i],
										'UPD_BY' => $this->user_session["USER_ID"],
										'UPD_DT' => date("Y-m-d:H-i-s")
								);
								if ($this->utilities->updateData('orderbyroaster', $orderInfo, array(
														"ValueID" => $prorder[$i],
												)))
										;
						}*/
				}
				/*End: INSERT AND UPDATE FOR OrderBy*/

			if ($success == 1) {
						echo "<div class='alert alert-success'>Set Roaster Created successfully</div>";
				} else {
						echo "<div class='alert alert-danger'>Problem On Created Set Roaster Criteria</div>";
				}
		}
		 /**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Delete Training
		 */
		public function trainDel()
		{
			$id = $this->input->post('id');
			//var_dump($id);
			$data=array(
				'ValueID' => $id
				);
			$this->db->query("DELETE FROM qualificationroaster WHERE ValueID = $id");
		}
		 /**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Delete Exam
		 */
		public function examDel()
		{
			$id = $this->input->post('id');
		 // var_dump($id);
			$data=array(
				'ValueID' => $id
				);
			$this->db->query("DELETE FROM qualificationroaster WHERE ValueID = $id");
		}
		 /**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Delete Medal
		 */
		public function medalDel()
		{
			$id = $this->input->post('id');
			//var_dump($id);
			$data=array(
				'ValueID' => $id
				);
			$this->db->query("DELETE FROM medalhonorroaster WHERE ValueID = $id");
		}
		 /**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Delete Honor
		 */
		public function honorDel()
		{
			$id = $this->input->post('id');
			//var_dump($id);
			$data=array(
				'ValueID' => $id
				);
			$this->db->query("DELETE FROM medalhonorroaster WHERE ValueID = $id");
		}
			/**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Delete Order
		 */
		public function orderDel()
		{
			$id = $this->input->post('id');
			//var_dump($id);
			$data=array(
				'ValueID' => $id
				);
			$this->db->query("DELETE FROM orderbyroaster WHERE ValueID = $id");
		}
				 /**
		 * @access      public
		 * @param       none
		 * @author      Maruf <ahasan@atilimited.net>
		 * @return      Delete Recomn
		 */
		public function recomnDel()
		{
			$id = $this->input->post('id');
			//var_dump($id);
			$data=array(
				'ValueID' => $id
				);
			$this->db->query("DELETE FROM recommroaster WHERE ValueID = $id");
		}
}
