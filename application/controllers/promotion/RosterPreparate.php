<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Promotion
 * @package    Roster Preparation
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class RosterPreparate extends CI_Controller {

	private $now;
    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Pension Information';
        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy("bn_branch", array("ACTIVE_STATUS" => 1), "POSITION");
        $data['reporttype'] = $this->utilities->findAllByAttributeWithOrderBy("reportsubtype", array("ACTIVE_STATUS" => 1,"ReportTypeID" => 3), "ReportSubTypeName");
        $data['reportsetup'] = $this->utilities->findAllByAttributeWithOrderBy("reportsetup", array("ACTIVE_STATUS" => 1,"ReportTypeID" => 3), "Name");
        $data['content_view_page'] = 'promotion/roster_preparation/index';
        $this->template->display($data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    function reportByReportType(){
        $typeId = $this->input->post("reportType");
        $report = $this->utilities->findAllByAttributeWithOrderBy("reportsetup", array("ACTIVE_STATUS" => 1,"ReportSubTypeID" => $typeId), "Name");
        $returnVal = '';
        foreach ($report as $row) {
             $returnVal .= '<tr><td class ="report" id = "' . $row->ReportSetupID . '">' . $row->Name . '</td></tr>';
        }
        echo $returnVal;
    }
    /**
     * @access      public
     * @param       none
     * @author      Maruf  <ahasan@atilimited.net>
     * @return      Query
     */
    public function prepareQuery()
    {
        $branch =$this->input->post("branch");
        $rank = $this->input->post("rank");
        $process = $this->db->query("SELECT s.SAILORID,
           s.OFFICIALNUMBER AS O_NO,
           s.FULLNAME,
           s.PROMOTIONDATE,
           (SELECT br.RANK_NAME
              FROM bn_rank br
             WHERE br.RANK_ID = s.RANKID)
              AS PRESENT_RANK
            --  (SELECT COUNT(c.Seniority)
            --     FROM coursetran c
            --     WHERE c.SailorID = s.SAILORID)
            --      AS seniority_points
          FROM sailor s
                 INNER JOIN promotionroaster p ON p.RankID = s.RANKID
                 INNER JOIN qualificationroaster q ON q.RoasterID = p.RoasterID
                 INNER JOIN coursetran c ON (c.CourseID = q.TrainingID and s.SAILORID = c.SailorID)
              --   INNER JOIN examtran e ON (e.ExamID = q.ExamID and s.SAILORID = e.SailorID)

             --   INNER JOIN medalhonorroaster mr ON mr.RoasterID = p.RoasterID
            --    INNER JOIN medaltran m ON (m.MedalID = mr.MedalID and s.SAILORID = m.SailorID)
             --  INNER JOIN honortran h ON (h.HonorID = mr.HonorID and s.SAILORID = h.SailorID)
         WHERE s.RANKID > $rank
         and s.BRANCHID = $branch Group By s.SAILORID ")->result();
        $multipleRows = array(
            'process' => $process
            );
        echo json_encode($multipleRows);

    }



}
