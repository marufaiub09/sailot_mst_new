<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Re-advancement
 * @package    Re-advancement
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class ReAdvancement extends CI_Controller {

    private $now;
    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor ReAdvancement Information';
        $data['flag'] = 0; /* flag status = 0 is Active sailor; status = 1 is retirement sailor; */
        $data['content_view_page'] = 'promotion/re_advancement/index';
        $this->template->display($data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     View modal
     */
    public function create() {
        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy("bn_branch", array("ACTIVE_STATUS" => 1), "POSITION");
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", 'DESC');
        $data['flag'] = 0; /* flag status = 0 is Active sailor; status = 1 is retirement sailor; */
        $data['content_view_page'] = 'promotion/re_advancement/create';
        $this->template->display($data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     no
     */
    public function save() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $branchCurr = $this->input->post('branchCurr', true);
        $rankCurr = $this->input->post('rankCurr', true);
        $branchRecomm = $this->input->post('branchRecomm', true);
        $rankRecomm = $this->input->post('rankRecomm', true);
        $reAdvancedDate = date('Y-m-d', strtotime($this->input->post('reAdvancedDate', true)));
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_NO = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_NO));
        $ship_establishment = $this->input->post('ship_establishment', true);
        $promotion = array(
            'SailorID' => $SAILOR_ID,
            'PromotionType' => 3,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAOID' => $DAO_NO,
            'DAONumber' => $DAO->DAO_NO,
            'CurrentRankID' => $rankRecomm,
            'LastRankID' => $rankCurr,
            'PromoDate' => $reAdvancedDate,
            'ShipID' => $ship_establishment,
            'IsNQ' => 0,
            'IsActing' => 0,
            'PromoEffectDate' => $reAdvancedDate,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($promotion, 'promotion')) {
            /* end */
            $sailorUpdate = array(
                'BRANCHID' => $branchRecomm,
                'RANKID' => $rankRecomm,
                    /* 'UPD_BY' => $this->user_session["USER_ID"],
                      'UPD_DT' => date("Y-m-d h:i:s a") */
            );
            if ($this->utilities->updateData('sailor', $sailorUpdate, array('SAILORID' => $SAILOR_ID))) {
                echo "<div class='alert alert-success'>Re-advancement Insert successfully</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Re-advancement Insert Failed</div>";
        }
    }

    /**
     * @access     public
     * @param      none
     * @author     Reazul Islam <reazul@atilimited.net>
     * @return     no
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT p.*, s.OFFICIALNUMBER,s.FULLNAME, cr.RANK_NAME CR_RANK,cr.BRANCH_ID CR_BRANCH, pr.RANK_NAME PRE_RANK, pr.BRANCH_ID PRE_BRANCH,pu.NAME POSTING_UNIT_NAME,se.NAME SHIP_EST,s.SENIORITYDATE
                                    FROM promotion p
                                    JOIN sailor s on s.SAILORID = p.SailorID
                                    JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                    JOIN bn_rank pr on pr.RANK_ID = p.LastRankID 
                                    JOIN bn_posting_unit pu ON pu.POSTING_UNITID = s.POSTINGUNITID
                                    JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.ShipEstablishmentID
                                    WHERE p.PromotionID =$id ")->row();
        //echo '<pre>';print_r($data['result']);exit;
        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy("bn_branch", array("ACTIVE_STATUS" => 1), "POSITION");
        $data['rank'] = $this->utilities->findAllByAttribute("bn_rank", array("ACTIVE_STATUS" => 1));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", 'DESC');
        $data['flag'] = 0; /* flag status = 0 is Active sailor; status = 1 is retirement sailor; */
        $data['content_view_page'] = 'promotion/re_advancement/edit';
        $this->template->display($data);
    }

    /*
     * @methodName viewRank()
     * @access
     * @param  rank _id
     * @return  //
     */

    function readvancement_Update() {
        $id = $this->input->post('id', true);
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $branchCurr = $this->input->post('branchCurr', true);
        $rankCurr = $this->input->post('rankCurr', true);
        $branchRecomm = $this->input->post('branchRecomm', true);
        $rankRecomm = $this->input->post('rankRecomm', true);
        $reAdvancedDate = date('Y-m-d', strtotime($this->input->post('reAdvancedDate', true)));
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_NO = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_NO));
        $ship_establishment = $this->input->post('ship_establishment', true);
        $promotion = array(
            'PromotionType' => 3,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAOID' => $DAO_NO,
            'DAONumber' => $DAO->DAO_NO,
            'CurrentRankID' => $rankRecomm,
            'LastRankID' => $rankCurr,
            'PromoDate' => $reAdvancedDate,
            'ShipID' => $ship_establishment,
            'IsNQ' => 0,
            'IsActing' => 0,
            'PromoEffectDate' => $reAdvancedDate,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('promotion', $promotion, array("PromotionID" => $id))) {
            /* end */
            $sailorUpdate = array(
                'BRANCHID' => $branchRecomm,
                'RANKID' => $rankRecomm,
                    /* 'UPD_BY' => $this->user_session["USER_ID"],
                      'UPD_DT' => date("Y-m-d h:i:s a") */
            );
            if ($this->utilities->updateData('sailor', $sailorUpdate, array('SAILORID' => $SAILOR_ID))) {
                echo "<div class='alert alert-success'>Re-advancement Update successfully</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Re-advancement Update Failed</div>";
        }
    }

    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT p.*, s.OFFICIALNUMBER,s.FULLNAME, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK,pu.NAME POSTING_UNIT_NAME,se.NAME SHIP_EST,s.SENIORITYDATE
                                    FROM promotion p
                                    JOIN sailor s on s.SAILORID = p.SailorID
                                    JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                    JOIN bn_rank pr on pr.RANK_ID = p.LastRankID 
                                    JOIN bn_posting_unit pu ON pu.POSTING_UNITID = s.POSTINGUNITID
                                    JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.ShipEstablishmentID
                                    WHERE p.PromotionID = $id")->row();
        // echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('promotion/re_advancement/view', $data);
    }

    function ajaxAdvanceInfoList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            // t.TransferID, t.TONumber , s.OFFICIALNUMBER, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, at.NAME AP_TYPE_NAME
            0 => 'p.PromotionID', 1 => 's.OFFICIALNUMBER', 2 => 'pr.RANK_NAME', 3 => 'cr.RANK_NAME ', 4 => 'p.PromoDate', 5 => 'p.PromoEffectDate', 6 => 'p.PromotionID');

        // getting total number records without any search

        $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK
                                    FROM promotion p
                                    JOIN sailor s on s.SAILORID = p.SailorID
                                    JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                    JOIN bn_rank pr on pr.RANK_ID = p.LastRankID 
                                    WHERE p.PromotionType = 3 AND s.SAILORSTATUS = 1")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK
                                        FROM promotion p
                                        JOIN sailor s on s.SAILORID = p.SailorID
                                        JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                        JOIN bn_rank pr on pr.RANK_ID = p.LastRankID
                                        WHERE p.PromotionType = 3 AND s.SAILORSTATUS = 1 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK
                                        FROM promotion p
                                        JOIN sailor s on s.SAILORID = p.SailorID
                                        JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                        JOIN bn_rank pr on pr.RANK_ID = p.LastRankID
                                        WHERE p.PromotionType = 3 AND s.SAILORSTATUS = 1
                                        ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->PRE_RANK;
            $nestedData[] = $row->CR_RANK;
            $nestedData[] = date('Y-m-d', strtotime($row->PromoDate));
            $nestedData[] = date('Y-m-d', strtotime($row->PromoEffectDate));
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('promotion/ReAdvancement/view/' . $row->PromotionID) . '" title="View Readvancement Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs modalLink" data-modal-size="modal-lg" href="' . site_url('promotion/ReAdvancement/edit/' . $row->PromotionID) . '" title="Edit Readvancement Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->PromotionID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="PromotionID" data-tbl="promotion"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }
    function searchReAdvancementInfo(){
        $sailorId = $this->input->post('sailorId', true);
        $sailorStatus = $this->input->post('sailorStatus', true);
        $query = $this->db->query("SELECT p.*, DATE_FORMAT( p.PromoDate ,'%m-%d-%Y') PromotionDate, s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME LAST_RANK
                                    FROM promotion p
                                    JOIN sailor s on s.SAILORID = p.SailorID
                                    JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                    JOIN bn_rank pr on pr.RANK_ID = p.LastRankID 
                                    WHERE p.PromotionType = 3 AND s.SAILORSTATUS = $sailorStatus AND s.SAILORID = $sailorId
                                    ORDER BY p.PromotionID DESC")->result();
        echo json_encode($query);
    }

}