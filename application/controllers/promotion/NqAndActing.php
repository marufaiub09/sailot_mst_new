<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Qa And Acting
 * @package    Qa And Acting
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class NqAndActing extends CI_Controller
{

    private $now;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index()
    {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['bn_ship_establishment'] = $this->utilities->findAllByAttribute('bn_ship_establishment', array("ACTIVE_STATUS" => 1));
        $data['bn_dao'] = $this->utilities->findAllByAttribute('bn_dao', array("ACTIVE_STATUS" => 1));
        $data['pageTitle'] = 'Sailor Pension Information';
        $data['content_view_page'] = 'promotion/qa_and_acting/index';
        $this->template->display($data);
    }

    public function save()
    {
        error_reporting('0');
        //date_format($date,"Y/m/d H:i:s");
        $RemoveType = $this->input->post('RemoveType', true);
        $AuthorityNumber = $this->input->post('AuthorityNumber', true);
        $AuthorityDate = $this->input->post('AuthorityDate', true);
        $SailorID = $this->input->post('SailorID', true);
        $DAOID = $this->input->post('DAOID', true);
        $DAONumber = $this->input->post('DAOnumber', true);
        $ShipEstablishmentID = $this->input->post('ShipEstablishmentID', true);
        $RankID = $this->input->post('RankID', true);
        $ISACTING = $this->input->post('ISACTING', true);
        $ISNOTQUALIFIED = $this->input->post('ISNOTQUALIFIED', true);
        $NQRemoveDate = $this->input->post('NQRemoveDate', true);
        $str_explode = explode(".", $DAOID);
        $string1 = $str_explode[0]; // hello
        $string2 = $str_explode[1]; // test

        if ($RemoveType == 2) {
            $Date = date('Y-m-d', strtotime($NQRemoveDate));
        } else {
            $Date = "NULL";
        }
        $success = 0;
        $data = array(
            'RemoveType' => $RemoveType,
            'AuthorityNumber' => $AuthorityNumber,
            'AuthorityDate' => date('Y-m-d', strtotime($AuthorityDate)),
            'SailorID' => $SailorID,
            'DAOID' => $DAOID,
            'DAONumber' => $string2,
            'ShipEstablishmentID' => $ShipEstablishmentID,
            'RankID' => $RankID,
            'NQRemoveDate' => $Date,
            'EntryUserID' => $this->user_session["USER_ID"]
        );

        /* echo '<pre>';
          print_r($data);
          exit(); */
        if ($this->utilities->insertData($data, 'nqactingremove')) {
            if ($RemoveType == 1) {
                $UpdateDate = array(
                    'ISACTING' => 0
                );
            } else {
                $UpdateDate = array(
                    'ISNOTQUALIFIED' => 0
                );
            }
            if ($this->utilities->updateData('sailor', $UpdateDate, array('SAILORID' => $SailorID)))
                echo "<div class='alert alert-success'>NQ and Acting Successfully Removed</div>";
        } else {
            echo "<div class='alert alert-success'>NQ and Acting Remove Failed</div>";
        }
    }

}