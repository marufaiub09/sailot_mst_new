<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Re Commendation
 * @package    Re Commendation
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class ReCommendation extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Pension Information';
        $data['content_view_page'] = 'promotion/re_commendation/index';
        $this->template->display($data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     View modal
     */
    public function create() {
        $data['branch'] = $this->utilities->findAllByAttribute("bn_branch", array("ACTIVE_STATUS" => 1));
        $data['rank'] = $this->utilities->findAllByAttribute("bn_rank", array("ACTIVE_STATUS" => 1));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", 'DESC');
        $data['content_view_page'] = 'promotion/re_commendation/create';
        $this->template->display($data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     no
     */
    public function recomm_save() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $RecommYear = $this->input->post('RecommendationYear', true);
        $RecommDate = $this->input->post('RecommendationDate', true);
        if ($RecommDate == 0) {
            $RecomDate = $RecommYear . "-06-15";
        }if ($RecommDate == 1) {
            $RecomDate = $RecommYear . "-12-15";
        }if ($RecommDate == 2) {
            $RecomDate = $RecommYear . "-07-15";
        }
        $rankCurr = $this->input->post('rankCurr', true);
        $rankRecomm = $this->input->post('rankRecomm', true);
        $type = $this->input->post('type', true);
        $ship_establishment = $this->input->post('ship_establishment', true);
        if (isset($_POST['isNQ'])) {
            $isNQ = $this->input->post('isNQ', true);
        } else {
            $isNQ = 0;
        }
        $recommend = array(
            'SailorID' => $SAILOR_ID,
            'RecomDate' => $RecomDate,
            'RecomType' => $type,
            'IsNQ' => $isNQ,
            'RankID' => $rankCurr,
            'RecommRankID' => $rankRecomm,
            'ShipEstablishmentID' => $ship_establishment,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($recommend, 'recommendation')) {
            /* end */
            /* $sailorUpdate = array(
              'BRANCHID' => $branchTo,
              'RANKID' => $rankTo,
              'UPD_BY' => $this->user_session["USER_ID"],
              'UPD_DT' => date("Y-m-d h:i:s a")
              ); */
            //if ($this->utilities->updateData('sailor', $sailorUpdate, array('SAILORID' => $SAILOR_ID))) { 
            echo "<div class='alert alert-success'>Recommendation Insert successfully</div>";
            //}
        } else {
            echo "<div class='alert alert-danger'>Recommendation Information Insert Failed</div>";
        }
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     View modal
     */
    public function reCom_edit($id) {
        $data['result'] = $this->db->query("SELECT r.*,
                                            s.OFFICIALNUMBER,
                                            s.FULLNAME,
                                            cr.RANK_NAME CR_RANK,
                                            DATE_FORMAT(r.RecomDate, '%m') as RecommandationDate,
                                            cr.BRANCH_ID CR_BRAMCH,
                                            pr.RANK_NAME PRE_RANK,
                                            pr.BRANCH_ID PRE_BRAMCH,
                                            pu.NAME POSTING_UNIT_NAME,
                                            s.POSTINGDATE,
                                            s.PROMOTIONDATE,
                                            s.SENIORITYDATE
                                       FROM recommendation r
                                            JOIN sailor s ON s.SAILORID = r.SailorID
                                            JOIN bn_posting_unit pu ON pu.POSTING_UNITID = s.POSTINGUNITID
                                            JOIN bn_rank cr ON cr.RANK_ID = r.RecommRankID
                                            JOIN bn_rank pr ON pr.RANK_ID = r.RankID
                                      WHERE r.RecommendationID =$id ")->row();
        $data['branch'] = $this->utilities->findAllByAttribute("bn_branch", array("ACTIVE_STATUS" => 1));
        $data['rank'] = $this->utilities->findAllByAttribute("bn_rank", array("ACTIVE_STATUS" => 1));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", 'DESC');
        $data['content_view_page'] = 'promotion/re_commendation/recom_edit';
        $this->template->display($data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emran <Emran@atilimited.net>
     * @return     View data 
     */
    function reCom_Update() {
        $id = $this->input->post('id', true);
        $RecommYear = $this->input->post('RecommendationYear', true);
        $RecommDate = $this->input->post('RecommendationDate', true);
        if ($RecommDate == 0) {
            $RecomDate = $RecommYear . "-06-15";
        }if ($RecommDate == 1) {
            $RecomDate = $RecommYear . "-12-15";
        }if ($RecommDate == 2) {
            $RecomDate = $RecommYear . "-07-15";
        }
        $sailorId = $this->input->post('SAILOR_ID', true);
        $PostingUnit = $this->input->post('PostingUnit', true);
        $rankCurr = $this->input->post('rankCurr', true);
        $rankRecomm = $this->input->post('rankRecomm', true);
        $type = $this->input->post('type', true);
        $ship_establishment = $this->input->post('ship_establishment', true);
        if (isset($_POST['isNQ'])) {
            $isNQ = $this->input->post('isNQ', true);
        } else {
            $isNQ = 0;
        }

        $recommendationData = array(
            'SailorID' => $sailorId,
            'RecomDate' => $RecomDate,
            'RecomType' => $type,
            'IsNQ' => $isNQ,
            'RankID' => $rankCurr,
            'RecommRankID' => $rankRecomm,
            'PostingUnitID' => $PostingUnit,
            'ShipEstablishmentID' => $ship_establishment,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('recommendation', $recommendationData, array('RecommendationID' => $id))) {
            echo "<div class='alert alert-success'>Recommendation update successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Recommendation update Failed</div>";
        }
    }

    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT r.*,
                                            s.OFFICIALNUMBER,
                                            s.FULLNAME,
                                            cr.RANK_NAME CR_RANK,
                                            DATE_FORMAT(r.RecomDate, '%m') as RecommandationDate,
                                            cr.BRANCH_ID CR_BRAMCH,
                                            pr.RANK_NAME PRE_RANK,
                                            pr.BRANCH_ID PRE_BRAMCH,
                                            pu.NAME POSTING_UNIT_NAME,
                                            s.POSTINGDATE,
                                            s.PROMOTIONDATE,
                                            s.SENIORITYDATE,
                                            se.NAME SHIP_EST
                                       FROM recommendation r
                                            JOIN sailor s ON s.SAILORID = r.SailorID
                                            JOIN bn_posting_unit pu ON pu.POSTING_UNITID = s.POSTINGUNITID
                                            JOIN bn_rank cr ON cr.RANK_ID = r.RecommRankID
                                            JOIN bn_rank pr ON pr.RANK_ID = r.RankID
                                            JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.ShipEstablishmentID
                                      WHERE r.RecommendationID = $id")->row();
        //echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('promotion/re_commendation/view', $data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     View data 
     */
    function ajaxRecomInfoList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            // t.TransferID, t.TONumber , s.OFFICIALNUMBER, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, at.NAME AP_TYPE_NAME
            0 => 'p.PromotionID', 1 => 's.OFFICIALNUMBER', 2 => 'pr.RANK_NAME', 3 => 'cr.RANK_NAME ', 4 => 'p.PromoDate', 5 => 'p.PromoEffectDate', 6 => 'p.PromotionID');

        // getting total number records without any search

        $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK
                                    FROM promotion p
                                    JOIN sailor s on s.SAILORID = p.SailorID
                                    JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                    JOIN bn_rank pr on pr.RANK_ID = p.LastRankID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK
                                        FROM promotion p
                                        JOIN sailor s on s.SAILORID = p.SailorID
                                        JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                        JOIN bn_rank pr on pr.RANK_ID = p.LastRankID
                                        WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK
                                        FROM promotion p
                                        JOIN sailor s on s.SAILORID = p.SailorID
                                        JOIN bn_rank cr on cr.RANK_ID = p.CurrentRankID
                                        JOIN bn_rank pr on pr.RANK_ID = p.LastRankID
                                        ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->RecomType; 
            $nestedData[] = date('Y-m-d', strtotime($row->PromoDate));
            $nestedData[] = $row->CR_RANK;
            $nestedData[] = $row->PRE_RANK;
            $nestedData[] = date('Y-m-d', strtotime($row->PromoEffectDate));
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('promotion/ReCommendation/view/' . $row->PromotionID) . '" title="View  Recommendation Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('promotion/ReCommendation/reCom_edit/' . $row->PromotionID) . '" title="Edit Promotion Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->PromotionID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="PromotionID" data-tbl="promotion"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }
    function searchRecommendationInfo(){
        $sailorId = $this->input->post('sailorId', true);
        $sailorStatus = $this->input->post('sailorStatus', true);
        $query = $this->db->query("SELECT   p.RecommendationID, DATE_FORMAT( p.RecomDate ,'%m-%d-%Y')RecomDate, p.RecomType, p.IsNQ,  s.OFFICIALNUMBER, cr.RANK_NAME CR_RANK, pr.RANK_NAME PRE_RANK
                                    FROM     recommendation p
                                    JOIN sailor s on s.SAILORID = p.SailorID
                                    JOIN bn_rank cr on cr.RANK_ID = p.RankID
                                    JOIN bn_rank pr on pr.RANK_ID = p.RecommRankID
                                    WHERE s.SAILORSTATUS = $sailorStatus AND s.SAILORID = $sailorId
                                    ORDER BY p.RecommendationID DESC")->result();
        echo json_encode($query);
    }

}
