<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PackageWiseTemplateSetup extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('utilities');
    }

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Thana Setup';
        $data['content_view_page'] = 'packageWiseTemplateSetup/index';
        $this->template->display($data);
    }

    /*
     * @methodName create()
     * @access
     * @param  none
     * @return  //Load divition create page
     */

    public function create() {
        if (isset($_POST['DISTRICT_ID'])) {
            $data1 = array(
                'THANA_ENAME' => $this->input->post('THANA_ENAME', true),
                'DISTRICT_ID' => $this->input->post('DISTRICT_ID', true),
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data1, 'sa_thanas')) {
                $this->session->set_flashdata('success', $this->lang->line('thana') . ' ' . $this->lang->line('cre_success'));
                redirect('packageWiseTemplateSetup/index');
            }
        }
        $data['package'] = $this->utilities->dropdownFromTableWithCondition("sa_lookup_data", 'Select package', "LOOKUP_DATA_ID", "LOOKUP_DATA_NAME", array('LOOKUP_GRP_ID' => '39'));
        $this->load->view('packageWiseTemplateSetup/create', $data);
    }

    /*
     * @methodName edit()
     * @access
     * @param  none
     * @return  //Load divition edit page
     */

    public function edit($id = '') {
        if (isset($_POST['DISTRICT_ID'])) {
            $data = array(
                'THANA_ENAME' => $this->input->post('THANA_ENAME', true),
                'DISTRICT_ID' => $this->input->post('DISTRICT_ID', true),
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d:H-i-s")
            );
            if ($this->utilities->updateData('sa_thanas', $data, array('THANA_ID' => $id))) {
                $this->session->set_flashdata('success', $this->lang->line('thana') . ' ' . $this->lang->line('upd_success'));
                redirect('setup/thana');
            }
        }
        //$data['row'] = $this->setup_model->getAllThanaInfoById($id);
        $data['package'] = $this->utilities->dropdownFromTableWithCondition("sa_lookup_data", 'Select package', "LOOKUP_DATA_ID", "LOOKUP_DATA_NAME", array('LOOKUP_GRP_ID' => '39'));
        //$this->pr($data);
        $this->load->view('packageWiseTemplateSetup/edit', $data);
    }

    /*
     * @methodName view()
     * @access
     * @param  none
     * @return  //
     */
    public function view($id = '') {
        //$data['viewdetails'] = $this->setup_model->getAllthanaById($id);
        $this->load->view('packageWiseTemplateSetup/view');
    }

}

