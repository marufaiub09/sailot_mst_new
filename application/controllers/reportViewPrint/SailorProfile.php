<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SailorProfile extends CI_Controller
{

    private $now;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      templete
     */
    public function index()
    {

        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit Reprot Information';
        $data['content_view_page'] = 'reportViewPrint/index';
        $this->template->display($data);
    }

    function searchSailorInfoByOfficalNo()
    {
        $officalNumber = $this->input->post("officeNumber");
        $this->db->select('s.*,et.NAME ENTRY_TYPE_NAME,s.REMARKS, r.RANK_NAME, se.NAME SHIP_ESTABLISHMENT, pu.NAME POSTING_UNIT_NAME, DATE_FORMAT(s.POSTINGDATE,"%d-%m-%Y") POSTING_DATE');
        $this->db->from('sailor as s');
        $this->db->join('bn_posting_unit as pu', 'pu.POSTING_UNITID = s.POSTINGUNITID', 'INNER');
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID', 'INNER');
        $this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID', 'INNER');
        $this->db->join('bn_entrytype as et', 'et.ENTRY_TYPEID = s.ENTRYTYPEID', 'INNER');
        $this->db->where('s.OFFICIALNUMBER', $officalNumber);
        echo json_encode($this->db->get()->row_array());
    }

    function createView()
    {
        //  error_reporting('0');
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        if (isset($_POST['Print'])) {

            if (!empty($_POST['SAILOR_ID'])) {

                $data['SAILOR_ID'] = $_POST['SAILOR_ID'];
                $data['fullName'] = $_POST['fullName'];
                $data['officialNo'] = $_POST['officialNo'];
                $data['rank'] = $_POST['rank'];
                $data['checkbox'] = $_POST['checkbox'];
                $data['pageSize'] = $_POST['pageSize'];
                $data['orientation'] = $_POST['orientation'];
                $data['topMargin'] = $_POST['topMargin'];
                $data['bottomMargin'] = $_POST['bottomMargin'];
                $data['rightMargin'] = $_POST['rightMargin'];
                $data['leftMargin'] = $_POST['leftMargin'];
                $data['fontType'] = $_POST['fontType'];
                $data['fontSize'] = $_POST['fontSize'];
                $data['borderType'] = $_POST['borderType'];
                $data['borderColour'] = $_POST['borderColour'];
            }
        }
        foreach ($_POST['checkbox'] as $Info) {
            $Info;
        }
        $sailorId = $_POST['SAILOR_ID'];


        switch (!empty($_POST['SAILOR_ID'])) {
            case($Info == 'GI'):
                $data['abs'] = $this->db->query("SELECT
                            sailor.OFFICIALNUMBER,
                            DATE_FORMAT(sailor.BIRTHDATE,'%d-%m-%Y') BIRTHDATE,
                            DATE_FORMAT(es.EngagementDate, '%d-%m-%Y') EngagementDate,
                            DATE_FORMAT(sailor.PROMOTIONDATE,'%d-%m-%Y') PROMOTIONDATE,
                            gh.Name,
                            sailor.MARITALSTATUS,
                            sailor.WEIGHT,
                            sailor.COMPLEXION,
                            sailor.FATHERNAME,
                            ms.SpouseName as SpouseName,
                            sailor.NEXTOFKIN as NextOfKin,
                            sailor.IDMARKS,
                            sailor.PERMANENTADDRESS,
                            sailor.REMARKS,
                            sailor.MOTHERNAME,
                            sailor.FULLNAME,
                            r.RANK_NAME as RANKID,
                            e.NAME as ENTRYTYPEID,
                            sailor.ENGAGEMENTPERIOD,
                            sailor.ENTRYDATE,
                            DATE_FORMAT(sailor.ENTRYDATE, '%d-%m-%Y') ENTRYDATE,
                            DATE_FORMAT(es.ExpiryDate , '%d-%m-%Y') EngagementExpiry,
                            DATE_FORMAT(sailor.SENIORITYDATE , '%d-%m-%Y') SENIORITYDATE,
                            DATE_FORMAT(sailor.POSTINGDATE , '%d-%m-%Y') POSTINGDATE,
                            sailor.HEIGHT,
                            sailor.BLOODGROUP,
                            sailor.MOTHERNAME,
                            sailor.CHEST,
                            sailor.EYESIGHT,
                            sailor.RELATIONID,
                            DATE_FORMAT(sailor.MARRIAGEDATE,'%d-%m-%Y') MARRIAGEDATE,
                            bd.NAME as DISTRICTID,
                            a.TotalMarks,
                            su.NAME SubjectID,
                            sm.Description as MEDICALCATEGORY,
                            a.ResultDescription,
                            sailor.ACTIVE_STATUS
                            FROM sailor
                            LEFT OUTER JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
                            LEFT OUTER JOIN bn_entrytype e ON sailor.ENTRYTYPEID = e.ENTRY_TYPEID
                            LEFT OUTER JOIN bn_bdadminhierarchy bd ON sailor.DISTRICTID = bd.BD_ADMINID
                            LEFT OUTER JOIN academic a ON sailor.SAILORID = a.SailorID
                            LEFT OUTER JOIN bn_govtexam_hierarchy gh ON a.ExamID = gh.GOVT_EXAMID
                            LEFT OUTER JOIN bn_subject_group su ON a.SubjectID = su.SUBJECT_GROUPID
                            LEFT OUTER JOIN medical sm ON sailor.MEDICALCATEGORYID = sm.MedicalID
                            LEFT OUTER JOIN marriage ms ON sailor.SAILORID = ms.MarriageID
                            LEFT OUTER JOIN engagement es ON sailor.SAILORID = es.SailorID WHERE es.EngagementNo in (SELECT MAX(EngagementNo) FROM engagement)
                             AND sailor.SAILORID = $sailorId")->result();

                $output = $this->load->view('reportViewPrint/genInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'FVI'):
                $data['abs'] = $this->db->query("SELECT DATE_FORMAT(fv.StartDate,'%d-%m-%Y') StartDate,DATE_FORMAT(fv.EndDate,'%d-%m-%Y') EndDate,fv.Duration,fv.Remarks, vi.NAME VI_NAME, country.NAME COUNTRY_NAME
                                                FROM foreignvisit fv
                                                INNER JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = fv.VisitClassID
                                                INNER JOIN bn_visitinformation vi on vi.VISIT_INFO_ID = fv.VisitInfoID
                                                INNER JOIN visitcountry vcountry on vcountry.ForeignVisitID = fv.ForeignVisitID
                                                INNER JOIN bn_country country on country.COUNTRY_ID = vcountry.CountryID
                                                INNER JOIN visitsailor cr on cr.ForeignVisitID = fv.ForeignVisitID
                                                WHERE cr.SailorID =  $sailorId")->result();

                $output = $this->load->view('reportViewPrint/fvInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);

                break;
            case($Info == 'MI'):
                $data['abs'] = $this->db->query("SELECT m.SailorID,m.MovementID,m.PostingUnitID,DATE_FORMAT(m.DraftInDate,'%d-%m-%Y') PostingDate,DATE_FORMAT(m.DraftOutDate,'%d-%m-%Y') EndDate, DATEDIFF(m.DraftOutDate, m.DraftInDate) Duration, m.AppointmentTypeID, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST, bse.AREA_ID AUTHO_AREA_ID, bse.NAME AUTHO_SHIP_EST
											FROM movement m
											INNER JOIN sailor s on s.SAILORID = m.SailorID
                      						INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
											INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
											INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
											INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
											INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
											WHERE m.SailorID = $sailorId")->result();

                $output = $this->load->view('reportViewPrint/miInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);

                break;
            case($Info == 'TMI'):
                $data['abs'] = $this->db->query("SELECT m.ShipEstablishmentID, m.PostingUnitID, DATE_FORMAT(m.DraftInDate,'%d-%m-%Y') StartDate,DATE_FORMAT(m.DraftOutDate,'%d-%m-%Y') EndDate, DATEDIFF(m.DraftOutDate,m.DraftInDate) Duration, s.OFFICIALNUMBER, ap.NAME Appoint_Type, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, bse.NAME AUTHO_SHIP_EST
                                    FROM movementtemp m
                                    INNER JOIN sailor s on s.SAILORID = m.SailorID
                                    INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                    INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID

                                    WHERE m.SailorID =  $sailorId")->result();

                $output = $this->load->view('reportViewPrint/tmiInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);

                break;
            case($Info == 'EI'):
                $data['abs'] = $this->db->query("SELECT DATE_FORMAT(e.EngagementDate,'%d-%m-%Y') EngageDate, DATE_FORMAT(e.ExpiryDate,'%d-%m-%Y') ExpiryDate,e.EngageFor,
                                e.DAONo,e.ShipEstablishmentID,bn.NAME, s.OFFICIALNUMBER, e.EngagementType,e.EngagementYear,
                                e.EngagementMonth,e.EngagementDay,
                                DATEDIFF(e.ExpiryDate,e.EngagementDate) Duration
                                 FROM engagement  e
                                JOIN sailor s on s.SAILORID = e.SailorID
                                JOIN bn_entrytype et on et.ENTRY_TYPEID = e.EntryTypeID
                                INNER JOIN bn_ship_establishment bn on e.ShipEstablishmentID = bn.SHIP_ESTABLISHMENTID
                                WHERE e.SailorID =  $sailorId")->result();
                $output = $this->load->view('reportViewPrint/eiInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'TCI'):
                $data['abs'] = $this->db->query("SELECT e.*, na.PARENT_ID AUTHO_ZONE, se.AREA_ID SHIP_AREA,tro.Name ORG_NAME,
                                             ti.Name INSTITUTE_NAME, th.Name TRAINING_COURSE, s.OFFICIALNUMBER,s.FULLNAME,
                                             r.RANK_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE, se.SHIP_ESTABLISHMENTID,
                                             se.NAME AUTHO_SHIP_EST, na.NAME AUTHO_AREA,DATE_FORMAT(e.StartDate,'%d-%m-%Y') Dateforstart,
                                             DATE_FORMAT(e.EndDate,'%d-%m-%Y') Dateforend
                                            FROM coursetran e
                                            INNER JOIN sailor s on s.SAILORID = e.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
                                            INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
                                            LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
                                            INNER JOIN bn_navytraininghierarchy th on th.NAVYTrainingID = e.CourseID
                                            INNER JOIN bn_traininginstitute ti on ti.Training_Institute_ID = e.InstituteID
                                            INNER JOIN bn_trainingorganization tro on tro.TrainingOrganizationID = e.OrganizationID
                                            WHERE e.IsForeign = 0 AND s.SAILORID = $sailorId")->result();
                $output = $this->load->view('reportViewPrint/tciInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'SI'):
                $data['abs'] = $this->db->query("SELECT a.SpecTranID, a.SpecTranDate, a.DAONumber,
                                                  os.NAME SPECIALIZATION
                                                  FROM spectran a
                                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                                  INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                                  Where s.SAILORID =   $sailorId")->result();
                $output = $this->load->view('reportViewPrint/siInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'EXI'):
                $data['abs'] = $this->db->query("SELECT e.*, ex.NAME EXAM_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE,DATE_FORMAT(e.ExamDate,'%d-%m-%Y') Date
									FROM examtran e
									INNER JOIN sailor s on s.SAILORID = e.SailorID
                                    INNER JOIN bn_navyexam_hierarchy ex on ex.EXAM_ID = e.ExamID
									INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
									INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
									LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
									WHERE  s.SAILORID =  $sailorId")->result();
                $output = $this->load->view('reportViewPrint/exInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'AI'):
                $data['abs'] = $this->db->query("SELECT a.AssessID, a.AssessYear, a.CharacterType, a.EfficiencyType, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                  FROM assessment a
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  WHERE s.SAILORID = $sailorId")->result();
                $output = $this->load->view('reportViewPrint/asInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'PLI'):
                $data['abs'] = $this->db->query("SELECT l.*, s.OFFICIALNUMBER,DATE_FORMAT(l.StartDate,'%d-%m-%y') StartDate, DATE_FORMAT(l.EndDate,'%d-%m-%y') EndDate,
                                    ld.LeaveYear,bn.NAME,ld.LeaveDays,ld.LeaveAvailed
                                    FROM `leave` l
                                    INNER JOIN sailor s on s.SAILORID = l.SailorID
                                    INNER JOIN leavedetail ld on l.LeaveID = ld.LeaveID
                                    INNER JOIN bn_ship_establishment bn on l.AuthorityShipID = bn.SHIP_ESTABLISHMENTID
                                    WHERE l.LeaveType = 0 AND s.SAILORID =  $sailorId")->result();
                $output = $this->load->view('reportViewPrint/plInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'GCI'):
                $data['abs'] = $this->db->query("SELECT g.GCBNumber,DATE_FORMAT(g.EffectDate,'%d-%m-%Y') Date,g.DAONumber,g.EffectDate,g.AuthorityNumber,g.SailorID,g.ShipID,g.GCBType, s.OFFICIALNUMBER, se.NAME AUTHO_SHIP_EST
									FROM gcb g
									INNER JOIN sailor s on s.SAILORID = g.SailorID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = g.ShipID
									WHERE s.SAILORID = $sailorId")->result();
                $output = $this->load->view('reportViewPrint/gciInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            case($Info == 'JPI'):
                $data['abs'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME JESTHATA_PADAK_NAME, se.NAME SHIP_EST,DATE_FORMAT(h.AwardDate,'%d-%m-%Y') Date
                                    FROM jesthatapadaktran h
                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                    INNER JOIN bn_jesthatapadak bh on bh.JestotaMedal_ID = h.JesthataPadakID
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                    WHERE s.SAILORID = $sailorId")->result();
                $output = $this->load->view('reportViewPrint/jpiInfoPdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            
            
            
            case($Info == 'PI'):
                $data['promotion_Data'] = $this->db->query("SELECT p.PromotionID,p.SailorID,p.DAOID,p.AuthorityNumber,
                                                DATE_FORMAT (p.PromoDate,'%d-%m-%Y') as PromoDate,
                                                DATE_FORMAT (p.PromoEffectDate,'%d-%m-%Y') as PromoEffectDate,(select case p.IsNQ WHEN 1 THEN 'Yes' ELSE NULL END ) AS IsNQ,
                                                (select case p.IsActing when 0 then 'No' when 1 then 'Yes' else null end ) as IsActing,
                                                s.OFFICIALNUMBER,cr.RANK_NAME CR_RANK,pr.RANK_NAME PRE_RANK
                                           FROM promotion p
                                           JOIN sailor s ON s.SAILORID = p.SailorID
                                                JOIN bn_rank cr ON cr.RANK_ID = p.CurrentRankID
                                                JOIN bn_rank pr ON pr.RANK_ID = p.LastRankID
                                          WHERE p.PromotionType = 1 AND s.SAILORSTATUS = 1 AND s.SAILORID = $sailorId")->result();
                $output = $this->load->view('reportViewPrint/PIInfopdf', $data, TRUE);
                $this->load->library("mpdf_gen");
                $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
                break;
            
            
            
        }

    }

    function createHtmlView()
    {
        error_reporting('0');
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        if (isset($_POST['HTMLVIEW'])) {

            if (!empty($_POST['SAILOR_ID'])) {

                $data['SAILOR_ID'] = $_POST['SAILOR_ID'];
                $data['checkbox'] = $_POST['checkbox'];
                $data['fullName'] = $_POST['fullName'];
                $data['officialNo'] = $_POST['officialNo'];
                $data['rank'] = $_POST['rank'];

            }
        }
        foreach ($_POST['checkbox'] as $Info) {
            $Info;
        }
        $sailorId = $_POST['SAILOR_ID'];
        switch (!empty($_POST['SAILOR_ID'])) {
            case($Info == 'GI'):
                $data['abs'] = $this->db->query("SELECT
                            sailor.OFFICIALNUMBER,
                            DATE_FORMAT(sailor.BIRTHDATE,'%d-%m-%Y') BIRTHDATE,
                            DATE_FORMAT(es.EngagementDate, '%d-%m-%Y') EngagementDate,
                            DATE_FORMAT(sailor.PROMOTIONDATE,'%d-%m-%Y') PROMOTIONDATE,
                            gh.Name,
                            sailor.MARITALSTATUS,
                            sailor.WEIGHT,
                            sailor.COMPLEXION,
                            sailor.FATHERNAME,
                            ms.SpouseName as SpouseName,
                            sailor.NEXTOFKIN as NextOfKin,
                            sailor.IDMARKS,
                            sailor.PERMANENTADDRESS,
                            sailor.REMARKS,
                            sailor.MOTHERNAME,
                            sailor.FULLNAME,
                            r.RANK_NAME as RANKID,
                            e.NAME as ENTRYTYPEID,
                            sailor.ENGAGEMENTPERIOD,
                            sailor.ENTRYDATE,
                            DATE_FORMAT(sailor.ENTRYDATE, '%d-%m-%Y') ENTRYDATE,
                            DATE_FORMAT(es.ExpiryDate , '%d-%m-%Y') EngagementExpiry,
                            DATE_FORMAT(sailor.SENIORITYDATE , '%d-%m-%Y') SENIORITYDATE,
                            DATE_FORMAT(sailor.POSTINGDATE , '%d-%m-%Y') POSTINGDATE,
                            sailor.HEIGHT,
                            sailor.BLOODGROUP,
                            sailor.MOTHERNAME,
                            sailor.CHEST,
                            sailor.EYESIGHT,
                            sailor.RELATIONID,
                            DATE_FORMAT(sailor.MARRIAGEDATE,'%d-%m-%Y') MARRIAGEDATE,
                            bd.NAME as DISTRICTID,
                            a.TotalMarks,
                            su.NAME SubjectID,
                            sm.Description as MEDICALCATEGORY,
                            a.ResultDescription,
                            sailor.ACTIVE_STATUS
                            FROM sailor
                            LEFT OUTER JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
                            LEFT OUTER JOIN bn_entrytype e ON sailor.ENTRYTYPEID = e.ENTRY_TYPEID
                            LEFT OUTER JOIN bn_bdadminhierarchy bd ON sailor.DISTRICTID = bd.BD_ADMINID
                            LEFT OUTER JOIN academic a ON sailor.SAILORID = a.SailorID
                            LEFT OUTER JOIN bn_govtexam_hierarchy gh ON a.ExamID = gh.GOVT_EXAMID
                            LEFT OUTER JOIN bn_subject_group su ON a.SubjectID = su.SUBJECT_GROUPID
                            LEFT OUTER JOIN medical sm ON sailor.MEDICALCATEGORYID = sm.MedicalID
                            LEFT OUTER JOIN marriage ms ON sailor.SAILORID = ms.MarriageID
                            LEFT OUTER JOIN engagement es ON sailor.SAILORID = es.SailorID WHERE es.EngagementNo in (SELECT MAX(EngagementNo) FROM engagement)
                             AND sailor.SAILORID = $sailorId")->result();

                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/genInfoHtml';
                $this->template->display($data);

                break;
            case($Info == 'FVI'):
                $data['abs'] = $this->db->query("SELECT DATE_FORMAT(fv.StartDate,'%d-%m-%Y') StartDate,DATE_FORMAT(fv.EndDate,'%d-%m-%Y') EndDate,fv.Duration,fv.Remarks, vi.NAME VI_NAME, country.NAME COUNTRY_NAME
                                                FROM foreignvisit fv
                                                INNER JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = fv.VisitClassID
                                                INNER JOIN bn_visitinformation vi on vi.VISIT_INFO_ID = fv.VisitInfoID
                                                INNER JOIN visitcountry vcountry on vcountry.ForeignVisitID = fv.ForeignVisitID
                                                INNER JOIN bn_country country on country.COUNTRY_ID = vcountry.CountryID
                                                INNER JOIN visitsailor cr on cr.ForeignVisitID = fv.ForeignVisitID
                                                WHERE cr.SailorID =  $sailorId")->result();

                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/fvInfoHtml';
                $this->template->display($data);

                break;
            case($Info == 'MI'):
                $data['abs'] = $this->db->query("SELECT m.SailorID,m.MovementID,m.PostingUnitID,DATE_FORMAT(m.DraftInDate,'%d-%m-%Y') PostingDate,DATE_FORMAT(m.DraftOutDate,'%d-%m-%Y') EndDate, DATEDIFF(m.DraftOutDate, m.DraftInDate) Duration, m.AppointmentTypeID, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST, bse.AREA_ID AUTHO_AREA_ID, bse.NAME AUTHO_SHIP_EST
											FROM movement m
											INNER JOIN sailor s on s.SAILORID = m.SailorID
                      						INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
											INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
											INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
											INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
											INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
											WHERE m.SailorID = $sailorId")->result();

                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/miInfoHtml';
                $this->template->display($data);

                break;
            case($Info == 'TMI'):
                $data['abs'] = $this->db->query("SELECT m.ShipEstablishmentID, m.PostingUnitID, DATE_FORMAT(m.DraftInDate,'%d-%m-%Y') StartDate,DATE_FORMAT(m.DraftOutDate,'%d-%m-%Y') EndDate, DATEDIFF(m.DraftOutDate,m.DraftInDate) Duration, s.OFFICIALNUMBER, ap.NAME Appoint_Type, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, bse.NAME AUTHO_SHIP_EST
                                    FROM movementtemp m
                                    INNER JOIN sailor s on s.SAILORID = m.SailorID
                                    INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                    INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
                                    WHERE m.SailorID =  $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/tmiInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'EI'):
                $data['abs'] = $this->db->query("SELECT DATE_FORMAT(e.EngagementDate,'%d-%m-%Y') EngageDate, DATE_FORMAT(e.ExpiryDate,'%d-%m-%Y') ExpiryDate,e.EngageFor,
                                e.DAONo,e.ShipEstablishmentID,bn.NAME, s.OFFICIALNUMBER, e.EngagementType,e.EngagementYear,
                                e.EngagementMonth,e.EngagementDay,
                                DATEDIFF(e.ExpiryDate,e.EngagementDate) Duration
                                FROM engagement  e
                                JOIN sailor s on s.SAILORID = e.SailorID
                                JOIN bn_entrytype et on et.ENTRY_TYPEID = e.EntryTypeID
                                INNER JOIN bn_ship_establishment bn on e.ShipEstablishmentID = bn.SHIP_ESTABLISHMENTID
                                WHERE e.SailorID =  $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/eiInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'TCI'):
                $data['abs'] = $this->db->query("SELECT e.*, na.PARENT_ID AUTHO_ZONE, se.AREA_ID SHIP_AREA,tro.Name ORG_NAME,
                                             ti.Name INSTITUTE_NAME, th.Name TRAINING_COURSE, s.OFFICIALNUMBER,s.FULLNAME,
                                             r.RANK_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE, se.SHIP_ESTABLISHMENTID,
                                             se.NAME AUTHO_SHIP_EST, na.NAME AUTHO_AREA,DATE_FORMAT(e.StartDate,'%d-%m-%Y') Dateforstart,
                                             DATE_FORMAT(e.EndDate,'%d-%m-%Y') Dateforend
                                            FROM coursetran e
                                            INNER JOIN sailor s on s.SAILORID = e.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
                                            INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
                                            LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
                                            INNER JOIN bn_navytraininghierarchy th on th.NAVYTrainingID = e.CourseID
                                            INNER JOIN bn_traininginstitute ti on ti.Training_Institute_ID = e.InstituteID
                                            INNER JOIN bn_trainingorganization tro on tro.TrainingOrganizationID = e.OrganizationID
                                            WHERE e.IsForeign = 0 AND s.SAILORID = $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/tciInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'SI'):
                $data['abs'] = $this->db->query("SELECT a.SpecTranID, a.SpecTranDate, a.DAONumber,
                                                  os.NAME SPECIALIZATION
                                                  FROM spectran a
                                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                                  INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                                  Where s.SAILORID =   $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/siInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'EXI'):
                $data['abs'] = $this->db->query("SELECT e.*, ex.NAME EXAM_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE,DATE_FORMAT(e.ExamDate,'%d-%m-%Y') Date
									FROM examtran e
									INNER JOIN sailor s on s.SAILORID = e.SailorID
                                    INNER JOIN bn_navyexam_hierarchy ex on ex.EXAM_ID = e.ExamID
									INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
									INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
									LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
									WHERE  s.SAILORID =  $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/exInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'AI'):
                $data['abs'] = $this->db->query("SELECT a.AssessID, a.AssessYear, a.CharacterType, a.EfficiencyType, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                  FROM assessment a
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  WHERE s.SAILORID = $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/asInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'PLI'):
                $data['abs'] = $this->db->query("SELECT l.*, s.OFFICIALNUMBER,DATE_FORMAT(l.StartDate,'%d-%m-%y') StartDate, DATE_FORMAT(l.EndDate,'%d-%m-%y') EndDate,
                                    ld.LeaveYear,bn.NAME,ld.LeaveDays,ld.LeaveAvailed
                                    FROM `leave` l
                                    INNER JOIN sailor s on s.SAILORID = l.SailorID
                                    INNER JOIN leavedetail ld on l.LeaveID = ld.LeaveID
                                    INNER JOIN bn_ship_establishment bn on l.AuthorityShipID = bn.SHIP_ESTABLISHMENTID
                                    WHERE l.LeaveType = 0 AND s.SAILORID =  $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/plInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'GCI'):
                $data['abs'] = $this->db->query("SELECT g.GCBNumber,DATE_FORMAT(g.EffectDate,'%d-%m-%Y') Date,g.DAONumber,g.EffectDate,g.AuthorityNumber,g.SailorID,g.ShipID,g.GCBType, s.OFFICIALNUMBER, se.NAME AUTHO_SHIP_EST
									FROM gcb g
									INNER JOIN sailor s on s.SAILORID = g.SailorID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = g.ShipID
									WHERE s.SAILORID = $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/gciInfoHtml';
                $this->template->display($data);
                break;
            case($Info == 'JPI'):
                $data['abs'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME JESTHATA_PADAK_NAME, se.NAME SHIP_EST,DATE_FORMAT(h.AwardDate,'%d-%m-%Y') Date
                                    FROM jesthatapadaktran h
                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                    INNER JOIN bn_jesthatapadak bh on bh.JestotaMedal_ID = h.JesthataPadakID
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                    WHERE s.SAILORID = $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/jpiInfoHtml';
                $this->template->display($data);
                break;
            
            case($Info == 'PI'):
                $data['promotion_Data'] = $this->db->query("SELECT p.PromotionID,p.SailorID,p.DAOID,p.AuthorityNumber,
                                                DATE_FORMAT (p.PromoDate,'%d-%m-%Y') as PromoDate,
                                                DATE_FORMAT (p.PromoEffectDate,'%d-%m-%Y') as PromoEffectDate,(select case p.IsNQ WHEN 1 THEN 'Yes' ELSE NULL END ) AS IsNQ,
                                                (select case p.IsActing when 0 then 'No' when 1 then 'Yes' else null end ) as IsActing,
                                                s.OFFICIALNUMBER,cr.RANK_NAME CR_RANK,pr.RANK_NAME PRE_RANK
                                           FROM promotion p
                                           JOIN sailor s ON s.SAILORID = p.SailorID
                                                JOIN bn_rank cr ON cr.RANK_ID = p.CurrentRankID
                                                JOIN bn_rank pr ON pr.RANK_ID = p.LastRankID
                                          WHERE p.PromotionType = 1 AND s.SAILORSTATUS = 1 AND s.SAILORID = $sailorId")->result();
                $data['pageTitle'] = 'Sailor Reprot Information';
                $data['content_view_page'] = 'reportViewPrint/PIInfoHtml';
                $this->template->display($data);
                break;
            


        }
    }


}