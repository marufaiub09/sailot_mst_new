<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Report View Print
 * @package    Report Preparation
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class PrepareCourseRoster extends CI_Controller {

    private $now;
    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }
    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <Emran@atilimited.net>
     * @return      View modal
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Prepare course roster';
        $data['appointType'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));
        $data['mission'] = $this->utilities->findAllByAttribute("bn_mission", array("ACTIVE_STATUS" => 1));
        $data['trainingType'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 2));
        $data['rosterReport'] = $this->utilities->findAllByAttribute("reportsetup", array("ReportTypeID" => 4));
        $data['content_view_page'] = 'reportViewPrint/courseRoster/index';
        $this->template->display($data);
    }
}

/* End of file courseRoster.php */
/* Location: ./application/controllers/reportViewPrint/courseRoster.php */