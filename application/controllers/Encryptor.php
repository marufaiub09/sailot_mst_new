<?php
/**
 * Created by PhpStorm.
 * User: streetcoder
 * Date: 2/9/16
 * Time: 12:23 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');
class Encryptor extends EXT_Controller{


    /**
     * Class constructor
     *
     */
    function __construct()
    {
        parent::__construct();
    }


    public function index(){
        $data['pageTitle'] = 'Encryption form Index';
        $data['breadcrumbs'] = array(
            'Encryptor Index' => '#'
        );
        $data['content_view_page'] = 'encryptor/index';

        $this->template->display($data);
    }

}