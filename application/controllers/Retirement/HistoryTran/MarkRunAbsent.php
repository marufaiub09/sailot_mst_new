<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   MarkRunAbsent Activity
 * @package    MarkRunAbsent Activity Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class MarkRunAbsent extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Mark Run Absent Information';
        $data['content_view_page'] = 'regularTransaction/mark_run_absent/index';
        $this->template->display($data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function mark_absent($flag, $sailorId) {
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = $flag;
        $data['sailorId'] = $sailorId;
        if($flag == 1 || $flag == 2 ){
        	$this->load->view('regularTransaction/mark_run_absent/create_mark_absent',$data);        	
        }else if($flag == 3){
        	$this->load->view('regularTransaction/mark_run_absent/create_surrender',$data);
        }else if($flag == 4){
        	$this->load->view('regularTransaction/mark_run_absent/create_run_remove',$data);
        }else if($flag == 5){
        	$this->load->view('regularTransaction/mark_run_absent/create_books_deport',$data);
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      mark Run all data
     */
    function markAbsentSave(){

    	$sailorId = $this->input->post('sailorId', true);
        $status = $this->input->post('Status', true);
        $SHIP_ESTABLISHMENT = $this->input->post('SHIP_ESTABLISHMENT', true);
        if(isset($_POST['absentDate'])){
        	$absentDate = date('Y-m-d', strtotime($this->input->post('absentDate',TRUE)));
        	$abAuthorityNo = $this->input->post('abAuthorityNo',TRUE);
        	$abAuthorityDate = date('Y-m-d', strtotime($this->input->post('abAuthorityDate',TRUE)));
        	$ab_DAO_ID = $this->input->post('ab_DAO_ID',TRUE);
        	$DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $ab_DAO_ID));
        	$data = array(
        		'SailorID' => $sailorId,
        		'Status' => $status,
        		'RunShipID'	 => $SHIP_ESTABLISHMENT,
        		'AbsentDate' => $absentDate,
        		'AbsentAuthorityNo' => $abAuthorityNo,
        		'AbsentAuthorityDate' => $abAuthorityDate,
        		'AbsentDAOID' => $ab_DAO_ID,
        		'AbsentDAONumber' => $DAO->DAO_NO,
        		'CRE_BY' => $this->user_session["USER_ID"]
        		);
        	if ($this->utilities->insertData($data, 'markrun')) {
                echo "<div class='alert alert-success'>Data saved successfully</div>";
	        }else{
	            echo "<div class='alert alert-success'>Data saved Failed</div>";
		    }  
        }else{
        	$runDate = date('Y-m-d', strtotime($this->input->post('runDate',TRUE)));
        	$runAuthoNo = $this->input->post('runAuthoNo',TRUE);
        	$runAuthoDate = date('Y-m-d', strtotime($this->input->post('runAuthoDate',TRUE)));
        	$run_DAO_ID = $this->input->post('run_DAO_ID',TRUE);
        	$DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $run_DAO_ID));

        	$data = array(
        		'SailorID' => $sailorId,
        		'Status'	 => $status,
        		'RunShipID'	 => $SHIP_ESTABLISHMENT,
        		'RunDate'	 => $runDate,
        		'RunAuthorityNo'	 => $runAuthoNo,
        		'RunAuthorityDate'	 => $runAuthoDate,
        		'RunDAOID'	 => $run_DAO_ID,
        		'RunDAONumber'	 => $DAO->DAO_NO,
        		'CRE_BY' => $this->user_session["USER_ID"]
        		);
        	if ($this->utilities->insertData($data, 'markrun')) {
                echo "<div class='alert alert-success'>Data saved successfully</div>";
	        }else{
	            echo "<div class='alert alert-success'>Data saved Failed</div>";
		    }  
        }

    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      none
     */
    function surrenderSave(){

    	$sailorId = $this->input->post('sailorId', true);
        $status = $this->input->post('Status', true);
        if(isset($_POST['surrenderDate'])){
        	$surrenderDate = date('Y-m-d', strtotime($this->input->post('surrenderDate',TRUE)));
        	$suAuthorityNo = $this->input->post('suAuthorityNo',TRUE);
        	$suAuthorityDate = date('Y-m-d', strtotime($this->input->post('suAuthorityDate',TRUE)));
        	$su_DAO_ID = $this->input->post('su_DAO_ID',TRUE);
        	$DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $su_DAO_ID));
        	$data = array(
        		'Status' => $status,
        		'SurrenderAuthorityNo' => $suAuthorityNo,
        		'SurrenderAuthorityDate' => $suAuthorityDate,
        		'SurrenderDAOID' => $su_DAO_ID,
        		'SurrenderDAONumber' => $DAO->DAO_NO,
        		'UPD_BY' => $this->user_session["USER_ID"],
        		'UPD_DT' => date("Y-m-d h:i:s a")
        		);
        	if ($this->utilities->insertData($data, 'markrun')) {
                echo "<div class='alert alert-success'>Data saved successfully</div>";
	        }else{
	            echo "<div class='alert alert-success'>Data saved Failed</div>";
		    }  
        }
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      none
     */
    function runRemoveSave(){
    	$markRunId = 1; /*value from input field*/
    	$sailorId = $this->input->post('sailorId', true);
        $status = $this->input->post('Status', true);
        $remove_ship = $this->input->post('remove_ship', true);
        if(isset($_POST['runRemoveDate'])){
        	$runRemoveDate = date('Y-m-d', strtotime($this->input->post('runRemoveDate',TRUE)));
        	$runRemoveAuthoNo = $this->input->post('runRemoveAuthoNo',TRUE);
        	$runRemoveAuthoDate = date('Y-m-d', strtotime($this->input->post('runRemoveAuthoDate',TRUE)));
        	$run_remove_DAO_ID = $this->input->post('run_remove_DAO_ID',TRUE);
        	$DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $run_remove_DAO_ID));
        	$run_remove_by = $this->input->post('run_remove_by',TRUE);
        	$data = array(
        		'Status' => $status,
        		'RemoveDate' => $runRemoveDate,
        		'RemoveBy' => $run_remove_by,
        		'RemoveShipID' => $remove_ship,
        		'RemoveAuthorityNo' => $runRemoveAuthoNo,
        		'RemoveAuthorityDate' => $runRemoveAuthoDate,
        		'RemoveDAOID' => $run_remove_DAO_ID,
        		'RemoveDAONumber' => $DAO->DAO_NO,
        		'RemoveUserID' => $this->user_session["USER_ID"],
        		'UPD_BY' => $this->user_session["USER_ID"],
        		'UPD_DT' => date("Y-m-d h:i:s a")

        		);
        	if ($this->utilities->updateData('markrun', $data, array('MarkRunID', $markRunId))) {
                echo "<div class='alert alert-success'>Data saved successfully</div>";
	        }else{
	            echo "<div class='alert alert-success'>Data saved Failed</div>";
		    }  
        }
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      none
     */
    function booksDeportSave(){

    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      mark Run all data
     */
    function searchMarkRunAbsentInfo(){
    	$sailorId = $this->input->post("sailorId");
    	$this->db->select('m.*, se.NAME RUN_SHIP, ship.NAME REMOVE_SHIP, DATE_FORMAT(m.AbsentDate ,"%d-%m-%Y") AbsentDate, DATE_FORMAT(m.RunDate,"%d-%m-%Y") RunDate, DATE_FORMAT(m.RemoveDate ,"%d-%m-%Y") RemoveDate,  DATE_FORMAT(m.SurrenderDate ,"%d-%m-%Y")SurrenderDate ');
    	$this->db->from('markrun m');
    	$this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = m.RunShipID','INNER');
    	$this->db->join('bn_ship_establishment as ship', ' ship.SHIP_ESTABLISHMENTID = m.RemoveShipID','left');
    	$this->db->where('m.SailorID', $sailorId);
    	echo json_encode($this->db->get()->result_array());
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Full Name, status ,rank, ship establishment, posting unit, posting date.
     */
    function searchSailorInfoByOfficalNo(){
    	$officalNumber = $this->input->post("officeNumber");
        $this->db->select('s.SAILORID, s.FULLNAME, s.SAILORSTATUS, r.RANK_NAME, se.NAME SHIP_ESTABLISHMENT, pu.NAME POSTING_UNIT_NAME, DATE_FORMAT(s.POSTINGDATE,"%d-%m-%Y") POSTING_DATE');
        $this->db->from('sailor as s');
        $this->db->join('bn_posting_unit as pu', 'pu.POSTING_UNITID = s.POSTINGUNITID','INNER'); 
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID','INNER');
        $this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID', 'INNER');
        $this->db->where('s.OFFICIALNUMBER', $officalNumber);
        echo json_encode($this->db->get()->row_array());
    }
}

/* End of file markRunAbsent.php */
/* Location: ./application/controllers/regularTransaction/markRunAbsent.php */