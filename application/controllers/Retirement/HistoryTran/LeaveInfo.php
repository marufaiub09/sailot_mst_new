<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Leave
 * @package    Leave Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class LeaveInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Leave Information';
        $data['content_view_page'] = 'regularTransaction/leave_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['authorityArea'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2),"NAME");        
        $data['content_view_page'] = 'regularTransaction/leave_info/create';
        $this->template->display($data);
    }
    
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {
        /*echo "<pre>";
        print_r($_POST);
        exit();*/
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID', true);       
        $lvStartDate = date('Y-m-d', strtotime($this->input->post('lvStartDate', true)));
        $lvEndDate = date('Y-m-d', strtotime($this->input->post('lvEndDate', true)));
        $leaveType = $this->input->post('leaveType', true);
        $LeaveOption = 0;
        if($leaveType == 0 || $leaveType == 3){
            if(isset($_POST['leaveCat'])){
                $leaveIntervel[0] = $this->input->post('firstYear', true);
                $leaveIntervel[1] = $this->input->post('secondYear', true);
                $leaveCount[0] = $this->input->post('leaveOne', true);
                $leaveCount[1] = $this->input->post('leaveTwo', true);
                $totalLeave = $leaveCount[0] + $leaveCount[1] ;
    	        $leaveTrans_1 = array(
                    'LeaveCategory' => $this->input->post('leaveCat', true),
                    'TotalDays' => $totalLeave,
                    'LeaveOption' => $LeaveOption,
                    'EndDate' => $lvEndDate,
                    'CycleYear' => date('Y', strtotime($this->input->post('lvStartDate', true)))
    	        );

            }
        }else if($leaveType == 1){
            $LeaveOption = $this->input->post('leaveOption', true);
            if (isset($_POST['leaveOption']) && isset($_POST['lvCycleYear']) && isset($_POST['totalLeave'])) {
                $leaveTrans_1 = array(
                    'LeaveOption' => $LeaveOption,
                    'CycleYear' => $this->input->post('lvCycleYear', true),
                    'EndDate' => $lvEndDate,
                    'TotalDays' => $this->input->post('totalLeave', true)
                );
            }else{
                $leaveTrans_1 = array(
                    'LeaveOption' => $LeaveOption,
                    'CycleYear' => $this->input->post('lvCycleYear', true),
                    'TotalDays' => 0,
                );
            }
        }else if($leaveType == 2 || $leaveType == 4 ){
            if (isset($_POST['totalLeave'])) {
                $leaveTrans_1 = array(
                    'LeaveOption' => $LeaveOption,
                    'TotalDays' => $this->input->post('totalLeave', true),
                    'EndDate' => $lvEndDate,
                    'CycleYear' => date('Y', strtotime($this->input->post('lvStartDate', true)))
                );
            }
        }
        $recallLeave = (isset($_POST['recallLeave'])) ? 1 : 0 ;        
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        
        /*end sailorInfo part*/
        //CourseInformation insert part
        
        $leaveTrans_3 = array(
            'LeaveType' => $leaveType,
            'SailorID' => $SAILOR_ID,
            'StartDate' => $lvStartDate,            
            'AuthorityShipID' => $SHIP_ESTABLISHMENT_ID,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,            
            'IsRecalled' => $recallLeave,            
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($recallLeave == 1) {
            $leaveTrans_2 = array(
                'RecallDate' => date('Y-m-d', strtotime($this->input->post('recallDate', true))),
                'RecallAuthorityNumber' => $this->input->post('reAuthoNo', true),
                'RecallAuthorityDate' => date('Y-m-d', strtotime($this->input->post('reAuthoDate', true))),
            );
            $insert = array_merge($leaveTrans_1, $leaveTrans_2, $leaveTrans_3);
        }else{
            $insert = array_merge($leaveTrans_1, $leaveTrans_3);            
        }
        if ($LeaveID = $this->utilities->insert('leave', $insert)) { // if courseInfo inserted successfully            
	    	if($leaveType == 0 || $leaveType == 3){
                for ($i=0; $i < 2 ; $i++) { 
                    $leaveDetails = array(
                        'LeaveID' => $LeaveID,
                        'LeaveYear' => $leaveIntervel[$i],
                        'LeaveDays' => $leaveCount[$i],
                        'LeaveAvailed' =>$leaveCount[$i],
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $this->utilities->insert('leavedetail', $leaveDetails);
                }
                echo "<div class='alert alert-success'>Leave added successfully</div>";           
            }else{                
                $leaveDetails = array(
                    'LeaveID' => $LeaveID,
                    'LeaveYear' => date('Y', strtotime($this->input->post('lvStartDate', true))),
                    'LeaveDays' => $this->input->post('totalLeave', true),
                    'LeaveAvailed' =>$this->input->post('totalLeave', true),
                    'CRE_BY' => $this->user_session["USER_ID"]
                );
                if($LeaveOption != 2){ /* If leave Option is not equal to Only allowance*/
                    if($this->utilities->insert('leavedetail', $leaveDetails)){
                        echo "<div class='alert alert-success'>Leave added successfully</div>";           
                    } 
                }
                else{
                        echo "<div class='alert alert-success'>Leave added successfully</div>"; 
                }               
            }

        }else{
            echo "<div class='alert alert-success'>Leave Inserted Failed</div>";            
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT l.*, s.OFFICIALNUMBER, s.FULLNAME,  se.NAME, r.RANK_NAME, se.AREA_ID
                                            FROM `leave` l 
                                            INNER JOIN sailor s on s.SAILORID = l.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = l.AuthorityShipID
                                            WHERE l.LeaveID = $id")->row();
        $data['leaveDetails'] = $this->db->query("SELECT *
                                                FROM leavedetail 
                                                WHERE LeaveID = $id ")->result();
        $data['authorityArea'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2),"NAME");
        $data['shipEstablish'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['content_view_page'] = 'regularTransaction/leave_info/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update() {
        echo "<pre>";
        print_r($_POST);
        exit();
        $id= $this->input->post('id', true);
        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID', true);       
        $lvStartDate = date('Y-m-d', strtotime($this->input->post('lvStartDate', true)));
        $lvEndDate = date('Y-m-d', strtotime($this->input->post('lvEndDate', true)));
        $leaveType = $this->input->post('leaveType', true);
        $LeaveOption = 0;
        if($leaveType == 0 || $leaveType == 3){
            if(isset($_POST['leaveCat'])){
                $leaveIntervel[0] = $this->input->post('firstYear', true);
                $leaveIntervel[1] = $this->input->post('secondYear', true);
                $leaveCount[0] = $this->input->post('leaveOne', true);
                $leaveCount[1] = $this->input->post('leaveTwo', true);
                $totalLeave = $leaveCount[0] + $leaveCount[1] ;
                $leaveTrans_1 = array(
                    'LeaveCategory' => $this->input->post('leaveCat', true),
                    'TotalDays' => $totalLeave,
                    'LeaveOption' => $LeaveOption,
                    'EndDate' => $lvEndDate,
                    'CycleYear' => date('Y', strtotime($this->input->post('lvStartDate', true)))
                );

            }            
        }else if($leaveType == 1){
            $LeaveOption = $this->input->post('leaveOption', true);
            if (isset($_POST['leaveOption']) && isset($_POST['lvCycleYear']) && isset($_POST['totalLeave'])) {
                $leaveTrans_1 = array(
                    'LeaveOption' => $LeaveOption,
                    'CycleYear' => $this->input->post('lvCycleYear', true),
                    'EndDate' => $lvEndDate,
                    'TotalDays' => $this->input->post('totalLeave', true)
                );
            }else{
                $leaveTrans_1 = array(
                    'LeaveOption' => $LeaveOption,
                    'CycleYear' => $this->input->post('lvCycleYear', true),
                    'TotalDays' => 0,
                );
            }
        }else if($leaveType == 2 || $leaveType == 4 ){
            if (isset($_POST['totalLeave'])) {
                $leaveTrans_1 = array(
                    'LeaveOption' => $LeaveOption,
                    'TotalDays' => $this->input->post('totalLeave', true),
                    'EndDate' => $lvEndDate,
                    'CycleYear' => date('Y', strtotime($this->input->post('lvStartDate', true)))
                );
            }
        }
        $recallLeave = (isset($_POST['recallLeave'])) ? 1 : 0 ;        
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        
        /*end sailorInfo part*/
        //CourseInformation insert part
        
        $leaveTrans_3 = array(
            'LeaveType' => $leaveType,
            'SailorID' => $SAILOR_ID,
            'StartDate' => $lvStartDate,            
            'AuthorityShipID' => $SHIP_ESTABLISHMENT_ID,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,            
            'IsRecalled' => $recallLeave,            
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($recallLeave == 1) {
            $leaveTrans_2 = array(
                'RecallDate' => date('Y-m-d', strtotime($this->input->post('recallDate', true))),
                'RecallAuthorityNumber' => $this->input->post('reAuthoNo', true),
                'RecallAuthorityDate' => date('Y-m-d', strtotime($this->input->post('reAuthoDate', true))),
            );
            $insert = array_merge($leaveTrans_1, $leaveTrans_2, $leaveTrans_3);
        }else{
            $insert = array_merge($leaveTrans_1, $leaveTrans_3);            
        }
    }
    
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View leave info
     */
    public function view($id) {
        $data['leave'] = $this->db->query("SELECT l.*, s.OFFICIALNUMBER, se.NAME SHIP_NAME
                                            FROM `leave` l 
                                            INNER JOIN sailor s on s.SAILORID = l.SailorID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = l.AuthorityShipID
                                            WHERE l.LeaveID=$id ")->row();
        $data['leaveDetails'] = $this->db->query("SELECT *
                                                FROM leavedetail 
                                                WHERE LeaveID = $id ")->row();
        $this->load->view('regularTransaction/leave_info/view', $data);
    }
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxLeaveInfoList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'l.LeaveID', 1 => 's.OFFICIALNUMBER', 2 => 'l.CycleYear', 3 => 'l.StartDate', 4 => 'l.EndDate', 5 => 'l.TotalDays', 6 => 'l.LeaveType',  7 => 'l.LeaveID');

        // getting total number records without any search

        $query = $this->db->query("SELECT l.*, s.OFFICIALNUMBER
                                    FROM `leave` l 
                                    INNER JOIN sailor s on s.SAILORID = l.SailorID")->num_rows();
       
        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT l.*, s.OFFICIALNUMBER
                                    FROM `leave` l 
                                    INNER JOIN sailor s on s.SAILORID = l.SailorID
									WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value']. 
                                    "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

        } else {

            $query = $this->db->query("SELECT l.*, s.OFFICIALNUMBER
                                    FROM `leave` l 
                                    INNER JOIN sailor s on s.SAILORID = l.SailorID
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $leaveType = array('0' => 'PL','1' => 'RL','2' => 'ML','3' => 'APL','4' => 'AL');
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->CycleYear;
            $nestedData[] = date('d-m-Y',strtotime($row->StartDate));
            $nestedData[] = ($row->EndDate != null)? date('d-m-Y',strtotime($row->EndDate)) : '';
            $nestedData[] = $row->TotalDays;
            $nestedData[] = array_key_exists($row->LeaveType, $leaveType)? $leaveType[$row->LeaveType] : '';
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('regularTransaction/leaveInfo/view/' . $row->LeaveID) . '" title="View leave info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                            '<a class="btn btn-warning btn-xs" href="'.site_url('regularTransaction/leaveInfo/edit/' . $row->LeaveID) .'" title="Edit Leave Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->LeaveID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="LeaveID" data-tbl="leave"><span class="glyphicon glyphicon-trash"></span></a>';
            
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }
}

/* End of file leaveInfo.php */
/* Location: ./application/controllers/regularTransaction/leaveInfo.php */