<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Honor Activity
 * @package    Honor Activity Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class HonorActivity extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function honor_index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Honor Information';
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/honor_index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function honor_create() {
        $data['authorityArea'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2),"NAME");
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['honorName'] = $this->utilities->findAllByAttributeWithOrderBy("bn_honor", array("ACTIVE_STATUS" => 1),"NAME");
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1),"DAO_NO");
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/honor_create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function honor_save() {
        $HONOR_NAME = $this->input->post('HONOR_NAME', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        if (isset($_POST['authorityName'])) {
            $authorityName = $this->input->post('authorityName', true);
            $honortran_1 = array('AuthorityName' => $authorityName);
        } else {
            $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
            $honortran_1 = array('ShipID' => $AUTHO_SHIP_ESTABLISHMENT);
        }
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $OFFICIAL_NO = $this->input->post('OFFICIAL_NO', true);
        for ($i = 0; $i < count($OFFICIAL_NO); $i++) {
            $row = $this->utilities->findByAttribute("sailor", array('OFFICIALNUMBER' => $OFFICIAL_NO[$i]));
            $sailorId[] = $row->SAILORID;
        }
        $success = 0;
        for ($i = 0; $i < count($OFFICIAL_NO); $i++) {
            $honortran_2 = array(
                'SailorID' => $sailorId[$i],
                'HonorID' => $HONOR_NAME,
                'AwardDate' => $awardDate,
                'DAOID' => $DAO_ID,
                'DAONumber  ' => $DAO->DAO_NO,
                'AuthorityNumber' => $authorityNumber,
                'AuthorityDate' => $authorityDate,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            $honortran = array_merge($honortran_1, $honortran_2);
            if ($this->utilities->insertData($honortran, 'honortran')) {
                $success = 1;
            }
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Honor Information added successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Honor Information Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function honor_edit($id) {
        $data['result'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, bh.NAME HONOR_NAME,se.AREA_ID, se.NAME SHIP_EST
                                            FROM honortran h                  
                                            INNER JOIN sailor s on s.SAILORID = h.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_honor bh on bh.HONOR_ID = h.HonorID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                            WHERE h.HonorTranID = $id")->row();
        $data['authorityArea'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2),"NAME");
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['honorName'] = $this->utilities->findAllByAttributeWithOrderBy("bn_honor", array("ACTIVE_STATUS" => 1),"NAME");
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1),"NAME");
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/honor_edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function honor_update() {

        $id = $this->input->post('id', true);
        $HONOR_NAME = $this->input->post('HONOR_NAME', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        if (isset($_POST['authorityName'])) {
            $authorityName = $this->input->post('authorityName', true);
            $honortran_1 = array('AuthorityName' => $authorityName, 'ShipID' => null);
        } else {
            $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
            $honortran_1 = array('AuthorityName' => '', 'ShipID' => $AUTHO_SHIP_ESTABLISHMENT);
        }
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $honortran_2 = array(
            'HonorID' => $HONOR_NAME,
            'AwardDate' => $awardDate,
            'DAOID' => $DAO_ID,
            'DAONumber  ' => $DAO->DAO_NO,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        $honortran = array_merge($honortran_1, $honortran_2);

        if ($this->utilities->updateData('honortran', $honortran, array('HonorTranID' => $id))) {
            echo "<div class='alert alert-success'>Honor Information Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Honor Information Update Failed</div>";
        }
    }

    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function honor_view($id) {
        $data['viewdetails'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, bh.NAME HONOR_NAME,se.AREA_ID,se.NAME SHIP_EST
                                            FROM honortran h                  
                                            INNER JOIN sailor s on s.SAILORID = h.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_honor bh on bh.HONOR_ID = h.HonorID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                            WHERE h.HonorTranID= $id")->row();
        //echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/honour_activity/honor_view', $data);
    }

    function ajaxHonorActivityList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'h.HonorTranID', 1 => 's.OFFICIALNUMBER', 2 => 'bh.NAME', 3 => 'h.AwardDate', 4 => 'se.NAME', 5 => 'h.AuthorityName', 6 => 'h.DAONumber', 7 => 'h.HonorTranID');

        // getting total number records without any search

        $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME HONOR_NAME, se.NAME SHIP_EST
                                                    FROM honortran h
                                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                                    INNER JOIN bn_honor bh on bh.HONOR_ID = h.HonorID
                                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                                    WHERE s.SAILORSTATUS = 3")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME HONOR_NAME, se.NAME SHIP_EST
                                                    FROM honortran h
                                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                                    INNER JOIN bn_honor bh on bh.HONOR_ID = h.HonorID
                                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                                   WHERE s.SAILORSTATUS = 3 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] . "%' OR bh.NAME LIKE '" . $requestData['search']['value'] .
                            "%' OR h.AwardDate LIKE '" . $requestData['search']['value'] . "%' OR h.DAONumber LIKE '" . $requestData['search']['value'] ."%' OR h.AuthorityName LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME HONOR_NAME, se.NAME SHIP_EST
                                                    FROM honortran h
                                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                                    INNER JOIN bn_honor bh on bh.HONOR_ID = h.HonorID
                                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                                    WHERE s.SAILORSTATUS = 3
                                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->HONOR_NAME;
            $nestedData[] = date("Y-m-d", strtotime($row->AwardDate));
            $nestedData[] = $row->SHIP_EST;
            $nestedData[] = $row->AuthorityName;
            /*$nestedData[] = $row->DAONumber;*/
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('Retirement/HistoryTran/honorActivity/honor_view/' . $row->HonorTranID) . '" title="View Honor Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('Retirement/HistoryTran/honorActivity/honor_edit/' . $row->HonorTranID) . '" title="Edit Honor Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->HonorTranID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="HonorTranID" data-tbl="honortran"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function medal_index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Medal Information';
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/medal_index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function medal_create() {
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['medalName'] = $this->utilities->findAllByAttributeWithOrderBy("bn_medal", array("ACTIVE_STATUS" => 1),"NAME");
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/medal_create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function medal_save() {
        $MEDAL_NAME = $this->input->post('MEDAL_NAME', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        if (isset($_POST['authorityName'])) {
            $authorityName = $this->input->post('authorityName', true);
            $medaltran_1 = array('AuthorityName' => $authorityName);
        } else {
            $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
            $medaltran_1 = array('ShipID' => $AUTHO_SHIP_ESTABLISHMENT);
        }
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $OFFICIAL_NO = $this->input->post('OFFICIAL_NO', true);
        for ($i = 0; $i < count($OFFICIAL_NO); $i++) {
            $row = $this->utilities->findByAttribute("sailor", array('OFFICIALNUMBER' => $OFFICIAL_NO[$i]));
            $sailorId[] = $row->SAILORID;
        }
        $success = 0;
        for ($i = 0; $i < count($OFFICIAL_NO); $i++) {
            $medaltran_2 = array(
                'SailorID' => $sailorId[$i],
                'MedalID' => $MEDAL_NAME,
                'AwardDate' => $awardDate,
                'DAOID' => $DAO_ID,
                'DAONumber  ' => $DAO->DAO_NO,
                'AuthorityNumber' => $authorityNumber,
                'AuthorityDate' => $authorityDate,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            $medaltran = array_merge($medaltran_1, $medaltran_2);
            if ($this->utilities->insertData($medaltran, 'medaltran')) {
                $success = 1;
            }
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Medal Information added successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Medal Information Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function medal_edit($id) {
        $data['result'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, bh.NAME MEDAL_NAME,se.AREA_ID, se.NAME SHIP_EST
                                            FROM medaltran h                  
                                            INNER JOIN sailor s on s.SAILORID = h.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_medal bh on bh.MEDAL_ID = h.MedalID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                            WHERE h.MedalTranID = $id")->row();
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['medalName'] = $this->utilities->findAllByAttributeWithOrderBy("bn_medal", array("ACTIVE_STATUS" => 1),"NAME");
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/medal_edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function medal_update() {

        $id = $this->input->post('id', true);
        $MEDAL_NAME = $this->input->post('MEDAL_NAME', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        if (isset($_POST['authorityName'])) {
            $authorityName = $this->input->post('authorityName', true);
            $medaltran_1 = array('AuthorityName' => $authorityName, 'ShipID' => null);
        } else {
            $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
            $medaltran_1 = array('AuthorityName' => '', 'ShipID' => $AUTHO_SHIP_ESTABLISHMENT);
        }
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $medaltran_2 = array(
            'MedalID' => $MEDAL_NAME,
            'AwardDate' => $awardDate,
            'DAOID' => $DAO_ID,
            'DAONumber  ' => $DAO->DAO_NO,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        $medaltran = array_merge($medaltran_1, $medaltran_2);

        if ($this->utilities->updateData('medaltran', $medaltran, array('MedalTranID' => $id))) {
            echo "<div class='alert alert-success'>Medal Information Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Medal Information Update Failed</div>";
        }
    }

    /**
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function medal_view($id) {
        $data['viewdetails'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, bh.NAME MEDAL_NAME,se.AREA_ID,se.NAME SHIP_EST
                                            FROM medaltran h                  
                                            INNER JOIN sailor s on s.SAILORID = h.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_medal bh on bh.MEDAL_ID = h.MedalID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                            WHERE h.MedalTranID = $id")->row();
        //echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/honour_activity/medal_view', $data);
    }

    function ajaxMedalActivityList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'h.MedalTranID', 1 => 's.OFFICIALNUMBER', 2 => 'bh.NAME', 3 => 'h.AwardDate', 4 => 'se.NAME', 5 => 'h.AuthorityName', 6 => 'h.DAONumber', 7 => 'h.MedalTranID');

        // getting total number records without any search

        $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME MEDAL_NAME, se.NAME SHIP_EST
                                    FROM medaltran h
                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                    INNER JOIN bn_medal bh on bh.MEDAL_ID = h.MedalID
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                    WHERE s.SAILORSTATUS = 3")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME MEDAL_NAME, se.NAME SHIP_EST
                                        FROM medaltran h
                                        INNER JOIN sailor s on s.SAILORID = h.SailorID
                                        INNER JOIN bn_medal bh on bh.MEDAL_ID = h.MedalID
                                        LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                        WHERE s.SAILORSTATUS = 3 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] . "%' OR bh.NAME LIKE '" . $requestData['search']['value'] .
                            "%' OR h.AwardDate LIKE '" . $requestData['search']['value'] . "%' OR h.DAONumber LIKE '" . $requestData['search']['value'] ."%' OR h.AuthorityName LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME MEDAL_NAME, se.NAME SHIP_EST
                                        FROM medaltran h
                                        INNER JOIN sailor s on s.SAILORID = h.SailorID
                                        INNER JOIN bn_medal bh on bh.MEDAL_ID = h.MedalID
                                        LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                        WHERE s.SAILORSTATUS = 3
                                        ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->MEDAL_NAME;
            $nestedData[] = date("Y-m-d", strtotime($row->AwardDate));
            $nestedData[] = $row->SHIP_EST;
            $nestedData[] = $row->AuthorityName;
            /*$nestedData[] = $row->DAONumber;*/
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('Retirement/HistoryTran/honorActivity/medal_view/' . $row->MedalTranID) . '" title="View Medal Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('Retirement/HistoryTran/honorActivity/medal_edit/' . $row->MedalTranID) . '" title="Edit Medal Information" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->MedalTranID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="MedalTranID" data-tbl="medaltran"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

    public function jesthataPadak_index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Jesthata Padak Information';
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/jesthataPadak_index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function jesthataPadak_create() {
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['jesthatapadakName'] = $this->utilities->findAllByAttribute("bn_jesthatapadak", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/jesthataPadak_create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function jesthataPadak_save() {
        // echo'<pre>';print_r($_POST);exit;
        $JESHTATA_PADAK_NAME = $this->input->post('JESHTATA_PADAK_NAME', true);
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        if (isset($_POST['authorityName'])) {
            $authorityName = $this->input->post('authorityName', true);
            $jesthatran_1 = array('AuthorityName' => $authorityName);
        } else {
            $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
            $jesthatran_1 = array('ShipID' => $AUTHO_SHIP_ESTABLISHMENT);
        }
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $success = 0;
        $jesthatran_2 = array(
            'SailorID' => $SAILOR_ID,
            'JesthataPadakID' => $JESHTATA_PADAK_NAME,
            'AwardDate' => $awardDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $DAO->DAO_NO,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        $jesthatran = array_merge($jesthatran_1, $jesthatran_2);

        if ($this->utilities->insertData($jesthatran, 'jesthatapadaktran')) {
            $success = 1;
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Jesthata Padak Information added successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Jesthata Padak Information Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function jesthataPadak_edit($id) {
        $data['result'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, bh.NAME JESTHATA_PADAK_NAME,se.AREA_ID, se.NAME SHIP_EST
                                            FROM jesthatapadaktran h                  
                                            INNER JOIN sailor s on s.SAILORID = h.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_jesthatapadak bh on bh.JestotaMedal_ID = h.JesthataPadakID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.SailorID
                                            WHERE h.JesthataPadakTranID = $id")->row();
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['jesthatapadakName'] = $this->utilities->findAllByAttribute("bn_jesthatapadak", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/honour_activity/jesthataPadak_edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function jesthataPadak_update() {

        $id = $this->input->post('id', true);
        $JESHTATA_PADAK_NAME = $this->input->post('JESHTATA_PADAK_NAME', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        if (isset($_POST['authorityName'])) {
            $authorityName = $this->input->post('authorityName', true);
            $jesthatran_1 = array('AuthorityName' => $authorityName, 'ShipID' => null);
        } else {
            $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
            $jesthatran_1 = array('AuthorityName' => '', 'ShipID' => $AUTHO_SHIP_ESTABLISHMENT);
        }
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $jesthatran_2 = array(
            'JesthataPadakID' => $JESHTATA_PADAK_NAME,
            'AwardDate' => $awardDate,
            'DAOID' => $DAO_ID,
            'DAONumber  ' => $DAO->DAO_NO,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        $jesthatran = array_merge($jesthatran_1, $jesthatran_2);

        if ($this->utilities->updateData('jesthatapadaktran', $jesthatran, array('JesthataPadakTranID' => $id))) {
            echo "<div class='alert alert-success'>Jesthata Padak Information Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Jesthata Padak Information Update Failed</div>";
        }
    }

    /**
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function jesthataPadak_view($id) {
        $data['viewdetails'] = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, bh.NAME JESTHATA_PADAK_NAME,se.AREA_ID, se.NAME SHIP_EST
                                            FROM jesthatapadaktran h                  
                                            INNER JOIN sailor s on s.SAILORID = h.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_jesthatapadak bh on bh.JestotaMedal_ID = h.JesthataPadakID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.SailorID
                                            WHERE h.JesthataPadakTranID = $id")->row();
        //echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/honour_activity/jesthataPadak_view', $data);
    }

    function ajaxJesthataPadakActivityList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'h.JesthataPadakTranID', 1 => 's.OFFICIALNUMBER', 2 => 'bh.NAME', 3 => 'h.AwardDate', 4 => 'se.NAME', 5 => 'h.AuthorityName', 6 => 'h.DAONumber', 7 => 'h.JesthataPadakTranID');

        // getting total number records without any search

        $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME JESTHATA_PADAK_NAME, se.NAME SHIP_EST
                                    FROM jesthatapadaktran h
                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                    INNER JOIN bn_jesthatapadak bh on bh.JestotaMedal_ID = h.JesthataPadakID
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                    WHERE s.SAILORSTATUS = 3")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME JESTHATA_PADAK_NAME, se.NAME SHIP_EST
                                    FROM jesthatapadaktran h
                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                    INNER JOIN bn_jesthatapadak bh on bh.JestotaMedal_ID = h.JesthataPadakID
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                    WHERE s.SAILORSTATUS = 3 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] . "%' OR bh.NAME LIKE '" . $requestData['search']['value'] .
                            "%' OR h.AwardDate LIKE '" . $requestData['search']['value'] . "%' OR h.DAONumber LIKE '" . $requestData['search']['value'] ."%' OR h.AuthorityName LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT h.*, s.OFFICIALNUMBER, bh.NAME JESTHATA_PADAK_NAME, se.NAME SHIP_EST
                                    FROM jesthatapadaktran h
                                    INNER JOIN sailor s on s.SAILORID = h.SailorID
                                    INNER JOIN bn_jesthatapadak bh on bh.JestotaMedal_ID = h.JesthataPadakID
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = h.ShipID
                                    WHERE s.SAILORSTATUS = 3
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->JESTHATA_PADAK_NAME;
            $nestedData[] = date("Y-m-d", strtotime($row->AwardDate));
            $nestedData[] = $row->SHIP_EST;
            $nestedData[] = $row->AuthorityName;
            /*$nestedData[] = $row->DAONumber;*/
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('Retirement/HistoryTran/honorActivity/jesthataPadak_view/' . $row->JesthataPadakTranID) . '" title="View Jesthata padak Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('Retirement/HistoryTran/honorActivity/jesthataPadak_edit/' . $row->JesthataPadakTranID) . '" title="Edit jesthata padak  Information" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->JesthataPadakTranID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="JesthataPadakTranID" data-tbl="jesthatapadaktran"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file honorActivity.php */
/* Location: ./application/controllers/regularTransaction/honorActivity.php */