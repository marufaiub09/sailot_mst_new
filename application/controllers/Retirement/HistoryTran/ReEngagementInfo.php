<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Re-Engagement
 * @package    Re-Engagement Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class ReEngagementInfo extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Re-Engagement Information';
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/engagement_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['appointment'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['entryType'] = $this->utilities->findAllByAttribute("bn_entrytype", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/engagement_info/create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function save() {

        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $engagementNoNext = $this->input->post('engagementNoNext', true);
        $engagementDate = date('Y-m-d', strtotime($this->input->post('engagementDate', true)));
        $engagePeriod = $this->input->post('engageFor', true);
        $SHIP_AREA_ID = $this->input->post('SHIP_AREA_ID', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $DAO_NO = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_NO));
        $expireDate = $this->input->post('expireDate', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $engagement_type = $this->input->post('engagement_type', true);
        $ENTRY_TYPE = $this->input->post('ENTRY_TYPE', true);
        $priodYY = $this->input->post('priodYY', true);
        $priodMM = $this->input->post('priodMM', true);
        $priodDD = $this->input->post('priodDD', true);

        $totalEngagePeriod = $engagePeriod ; 

        $updateEngagement = array(
                "ENGAGEMENTPERIOD" =>$priodYY+$priodMM+$priodDD,
                "ENGAGEMENTTYPE" =>$engagement_type,
                "TOTALENGAGEPERIOD" =>$totalEngagePeriod
            );
        $engagement = array(
            'SailorID' => $SAILOR_ID,
            'EngagementDate' => $engagementDate,
            'EntryTypeID' => $ENTRY_TYPE,
            'EngagementYear' => $priodYY,
            'EngagementMonth' => $priodMM,
            'EngagementDay' => $priodDD,
            'ExpiryDate' => $expireDate,
            'EngagementNo' => $engagementNoNext,
            'DAOID' => $DAO_NO,
            'DAONo' => $DAO->DAO_NO,
            'ShipEstablishmentID' => $AUTHO_SHIP_ESTABLISHMENT,
            'EngagementType' => $engagement_type,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if(isset($_POST['ncsToCS'])){
            $ncsToCS = $this->input->post('ncsToCS', true);
            $ncs = array(
                    'IsNCSToCS' => $ncsToCS
                );
            $insert = array_merge($engagement, $ncs);
            if ($this->utilities->insertData($insert, 'engagement')) {
                if ($this->utilities->updateData('sailor', $updateEngagement, array('SAILORID' => $SAILOR_ID))){
                    echo "<div class='alert alert-success'>Engagement Info added successfully</div>";                    
                }

            } else {
                echo "<div class='alert alert-success'>Engagement Info Inserted Failed</div>";
            } 
        }else{
            if ($this->utilities->insertData($engagement, 'engagement')) {
                if ($this->utilities->updateData('sailor', $updateEngagement, array('SAILORID' => $SAILOR_ID))){
                    echo "<div class='alert alert-success'>Engagement Info added successfully</div>";                    
                }
            } else {
                echo "<div class='alert alert-success'>Engagement Info Inserted Failed</div>";
            }            
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST, bse.AREA_ID AUTHO_AREA_ID, bse.NAME AUTHO_SHIP_EST
                                            FROM movement m
                                            INNER JOIN sailor s on s.SAILORID = m.SailorID
                                            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
                                            INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                            INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                            INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
                                            WHERE m.MovementID = $id")->row();
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['PostingUnit'] = $this->utilities->findAllByAttribute("bn_posting_unit", array("ACTIVE_STATUS" => 1));
        $data['appointment'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/engagement_info/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function update() {
        $id = $this->input->post('id', true);
        $POSTING_UNIT_ID = $this->input->post('POSTING_UNIT_ID', true);
        $APPOINTMENT_TYPE = $this->input->post('APPOINTMENT_TYPE', true);
        $SHIP_ESTABLISHMENT = $this->input->post('SHIP_ESTABLISHMENT', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $orderNumber = $this->input->post('orderNumber', true);
        $remarks = $this->input->post('remarks', true);
        $orderDate = date('Y-m-d', strtotime($this->input->post('orderDate', true)));

        $draftInDate = date('Y-m-d', strtotime($this->input->post('draftInDate', true)));
        $draftOutDate = date('Y-m-d', strtotime($this->input->post('draftOutDate', true)));


        $update = array(
            'AppointmentTypeID' => $APPOINTMENT_TYPE,
            'ShipEstablishmentID' => $SHIP_ESTABLISHMENT,
            'PostingUnitID' => $POSTING_UNIT_ID,
            'DraftInDate' => $draftInDate,
            'DraftOutDate' => $draftOutDate,
            'TransferOrderNo' => $orderNumber,
            'TransferOrderDate' => $orderDate,
            'AuthorityShipID' => $AUTHO_SHIP_ESTABLISHMENT,
            'GenFormNo' => $authorityNumber,
            'GenFormDate' => $authorityDate,
            'Remarks' => $remarks,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('movement', $update, array('MovementID' => $id))) {
            echo "<div class='alert alert-success'>Movement History Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Movement History Update Failed</div>";
        }
    }

    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST, bse.AREA_ID AUTHO_AREA_ID, bse.NAME AUTHO_SHIP_EST
                                            FROM movement m
                                            INNER JOIN sailor s on s.SAILORID = m.SailorID
                                            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
                                            INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                            INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                            INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
                                            WHERE m.MovementID= $id")->row();
     // echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/engagement_info/view', $data);
    }

}

/* End of file reEngagementInfo.php */
/* Location: ./application/controllers/regularTransaction/reEngagementInfo.php */