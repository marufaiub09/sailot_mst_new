<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Children
 * @package    Children Info
 * @author     reazul Islam <reazul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class ChildrenInfo extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author     reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['pageTitle'] = 'Children Information';
        $data['content_view_page'] = 'regularTransaction/ChildrenInfo/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author    reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['zone'] = $this->utilities->dropdownFromTableWithCondition('bn_navyadminhierarchy', ' Select Zone ', 'ADMIN_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1, "ADMIN_TYPE =" => '1'));
        $this->load->view('regularTransaction/ChildrenInfo/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function save() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $CHILD_NAME = $this->input->post('ChildName', true);
        $birthDate = date('Y-m-d', strtotime($this->input->post('birthDate', true)));
        $gender = $this->input->post('Gender', true);
        $CHILD_NAME_BANGLA = $this->input->post('ChildNameBangla', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $AuthorityDate = date('Y-m-d', strtotime($this->input->post('AuthorityDate', true)));
        $DAO_NUMBER = $this->input->post('DAO_NUMBER', true);
        $dao_no = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_NUMBER));
        $shipEstId = $this->input->post('shipEstId', true);
        $data = array(
            'SailorID' => $SAILOR_ID,
            'Name' => $CHILD_NAME,
            'BirthDate' => $birthDate,
            'Gender' => $gender,
            'ChildNameBangla' => $CHILD_NAME_BANGLA,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $AuthorityDate,
            'DAOID' => $DAO_NUMBER,
            'DAONo' => $dao_no->DAO_NO,
            'ShipEstablishmentID' => $shipEstId,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        //echo '<pre>';print_r($data);exit;

        if ($this->utilities->insertData($data, "children")) { // if data inserted successfully
            echo "<div class='alert alert-success'>Children Transaction add successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Children Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam  <reazul@atilimited.net>
     * @return      
     */
    function childList() {
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view("regularTransaction/ChildrenInfo/list");
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("
                                  SELECT a.*, s.SailorID, s.OFFICIALNUMBER, s.FULLNAME,DATE_FORMAT(s.POSTINGDATE,'%Y-%m-%d') POSTING_DATE,se.ZONE_ID AUTHO_ZONE,se.AREA_ID SHIP_AREA,se.SHIP_TYPEID,pu.NAME,se.CODE, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME 
                                  FROM children a
                                  LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_posting_unit pu  on s.POSTINGUNITID = pu.POSTING_UNITID  WHERE a.ChildernID= $id")->row();

        $data['authorityZone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 1));
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEstablishment'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['zone'] = $this->utilities->dropdownFromTableWithCondition('bn_navyadminhierarchy', ' Select Zone ', 'ADMIN_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1, "ADMIN_TYPE =" => '1'));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view('regularTransaction/ChildrenInfo/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function update() {
        $id = $this->input->post('id', true);
        $CHILD_NAME = $this->input->post('ChildName', true);
        $birthDate = date('Y-m-d', strtotime($this->input->post('birthDate', true)));
        $gender = $this->input->post('Gender', true);
        $CHILD_NAME_BANGLA = $this->input->post('ChildNameBangla', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $AuthorityDate = date('Y-m-d', strtotime($this->input->post('AuthorityDate', true)));
        $DAO_NUMBER = $this->input->post('DAO_NUMBER', true);
        $dao_no = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_NUMBER));
        $shipEstId = $this->input->post('shipEstId', true);
        if (isset($_POST['AUTHORITY_ZONE_ID'])) {
            $AUTHORITY_ZONE_ID = $this->input->post('AUTHORITY_ZONE_ID', true);
            $examTrans_1 = array(
                'AuthorityAreaID' => $this->input->post('AUTHORITY_AREA_ID', true),
                'AuthorityShipID' => null,
                'AuthorityName' => ''
            );
        } elseif (isset($_POST['SHIP_AREA_ID'])) {
            $SHIP_AREA_ID = $this->input->post('SHIP_AREA_ID', true);
            $examTrans_1 = array(
                'AuthorityShipID' => $this->input->post('SHIP_ESTABLISHMENT_ID', true),
                'AuthorityAreaID' => null,
                'AuthorityName' => ''
            );
        } elseif (isset($_POST['authorityName'])) {
            $examTrans_1 = array(
                'AuthorityName' => $this->input->post('authorityName', true),
                'AuthorityAreaID' => null,
                'AuthorityShipID' => null
            );
        }
        $data = array(
            'Name' => $CHILD_NAME,
            'BirthDate' => $birthDate,
            'Gender' => $gender,
            'ChildNameBangla' => $CHILD_NAME_BANGLA,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $AuthorityDate,
            'DAOID' => $DAO_NUMBER,
            'DAONo' => $dao_no->DAO_NO,
            'ShipEstablishmentID' => $shipEstId,
//           'ShipEstablishmentID' => $shipEstId,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );


        if ($this->utilities->updateData('children', $data, array("ChildernID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>Children Transaction Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Children Update Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT a.*, s.SailorID, s.OFFICIALNUMBER, s.FULLNAME,s.POSTINGDATE POSTING_DATE,pu.NAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME 
        FROM children a
        LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_posting_unit pu  on s.POSTINGUNITID = pu.POSTING_UNITID  WHERE a.ChildernID = $id")->row();
//        echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/ChildrenInfo/view', $data);
    }

    /**
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    function ajaxChildTranList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'a.Name', 1 => 'a.Gender', 2 => 'a.BirthDate', 3 => 'se.NAME', 4 => 'a.AuthorityNumber', 5 => 'a.AuthorityDate', 6 => 's.OFFICIALNUMBER', 7 => 's.FULLNAME', 8 => 'a.DAONo');

        // getting total number records without any search

        $query = $this->db->query("SELECT a.*, s.SailorID, s.OFFICIALNUMBER, s.FULLNAME,s.POSTINGDATE POSTING_DATE,pu.NAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME 
                                  FROM children a
                                  LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_posting_unit pu  on s.POSTINGUNITID = pu.POSTING_UNITID
                                  WHERE s.SAILORSTATUS = 3")->num_rows();


        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT a.*, s.SailorID, s.OFFICIALNUMBER, s.FULLNAME,s.POSTINGDATE POSTING_DATE,pu.NAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME 
            FROM children a
            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_posting_unit pu  on s.POSTINGUNITID = pu.POSTING_UNITID
                                  WHERE s.SAILORSTATUS = 3 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR a.Gender LIKE '" . $requestData['search']['value'] . "%' OR a.BirthDate LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT a.*, s.SailorID, s.OFFICIALNUMBER, s.FULLNAME,s.POSTINGDATE POSTING_DATE,pu.NAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME 
            FROM children a
            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_posting_unit pu  on s.POSTINGUNITID = pu.POSTING_UNITID
                                  WHERE s.SAILORSTATUS = 3
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        $gender = array('0' => 'Male', '1' => 'Female', '2' => 'Others');
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->Name;
            $nestedData[] = array_key_exists($row->Gender, $gender) ? $gender[$row->Gender] : '';
            $nestedData[] = date("d/m/Y", strtotime($row->BirthDate));
            $nestedData[] = $row->SHIP_ESTABLISHMENT;
            $nestedData[] = "<b><i>Number " . $row->AuthorityNumber . "</i></b><br><b><i>Date " . date('d/m/Y', strtotime($row->AuthorityDate)) . "</i></b>";
            $nestedData[] = "<b><i>Official No. " . $row->OFFICIALNUMBER . "</i></b><br><b><i>Name " . $row->FULLNAME . "</i></b>";
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('Retirement/HistoryTran/childrenInfo/view/' . $row->ChildernID) . '" title="View Children Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs modalLink cm" href="' . site_url('Retirement/HistoryTran/childrenInfo/edit/' . $row->ChildernID) . '" title="Edit Children Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->ChildernID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="ChildernID" data-tbl="children"><span class="glyphicon glyphicon-trash"></span></a>';

            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

    public function ajax_get_area() {
        $zone_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_navyadminhierarchy', array('PARENT_ID' => $zone_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->ADMIN_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }

    public function ajax_get_shift() {
        $area_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_ship_establishment', array('AREA_ID' => $area_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->SHIP_ESTABLISHMENTID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }

    public function ajax_get_shipEstablishmentNumber() {
        $area_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_ship_establishment', array('SHIP_ESTABLISHMENTID' => $area_id))->result();
        $returnVal = '';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<label class="col-sm-3 control-label">Authority Ship</label> <div class="col-sm-2" ><input type="text" class="form-control" value="' . $row->CODE . '"></div>';

//                $returnVal .= '<label class="col-sm-3 control-label">Authority Ship</label><input type="text" class="form-control" value="' . $row->CODE . '">';
            }
        }
        echo $returnVal;
    }

    public function ajax_get_shipEstablishmentName() {
        $area_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_ship_establishment', array('SHIP_ESTABLISHMENTID' => $area_id))->result();
        $returnVal = '';
        if (!empty($query)) {
            foreach ($query as $row) {
                // $returnVal .= '<div class="col-sm-6" ><input type="text" class="form-control" value="' . $row->NAME . '"></div></div>';
                $returnVal .= '<div class="col-sm-4" ><input type="text" class="form-control" value="' . $row->NAME . '"></div>';
            }
        }
        echo $returnVal;
    }

}

/* End of file childrenInfo.php */
/* Location: ./application/controllers/regularTransaction/childrenInfo.php */