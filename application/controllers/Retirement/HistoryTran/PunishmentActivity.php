<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Punishment Activity
 * @package    Punishment Activity Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class PunishmentActivity extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function punishment_index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Punishment Information';
        $data['flag'] = 1;/*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/punishment_activity/punishment_index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function punishment_create() {
        $data['authorityArea'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2),"NAME");
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['punishmentType'] = $this->utilities->findAllByAttributeWithOrderBy("punishmenttype", array("ACTIVE_STATUS" => 1),"NAME");
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['offenceCat'] = $this->utilities->findAllByAttributeWithOrderBy("bn_offencecatagory", array("ACTIVE_STATUS" => 1),"CODE");
        $data['flag'] = 1;/*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/punishment_activity/punishment_create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function punishment_save() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $punishmentCauses = $this->input->post('punishmentCauses', true);
        $offenceCat = $this->input->post('offenceCat', true);
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));

        $PUNISHMENT_NO = $this->input->post('PUNISHMENT_NO', true);
        $punishmentDate = date('Y-m-d', strtotime($this->input->post('punishmentDate', true)));
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $punishment = array(
            'SailorID' => $SAILOR_ID,
            'PunishmentTypeID' => $PUNISHMENT_NO,
            'EffectDate' => $punishmentDate,
            'NoOfDays' => 0,
            'NoOfGCBDeprive' => 0,
            'DAOID' => $DAO_ID,
            'DAONumber' => $DAO->DAO_NO,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'ShipID' => $AUTHO_SHIP_ESTABLISHMENT,
            'Cause' => $punishmentCauses,
            'CRE_BY' => $this->user_session["USER_ID"]
        );

        if ($PunishmentID = $this->utilities->insert('punishment', $punishment)) {
            if (!empty($offenceCat)) {
                for ($i = 0; $i < count($offenceCat); $i++) {
                    $punishOffence = array(
                        "OffenceCatagoryID" => $offenceCat[$i],
                        "PunishmentID" => $PunishmentID,
                        "CRE_BY" => $this->user_session["USER_ID"]
                    );
                    if ($this->utilities->insertData($punishOffence, 'punishmentoffence')) {
                        $success = 1;
                    }
                }
            }
            echo "<div class='alert alert-success'>Punishment added successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>punishment Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function punishment_edit($id) {
        $data['result'] = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, pt.Name PUNISHMENT_TYPE, se.AREA_ID
                                    FROM punishment p
                                    INNER JOIN sailor s on s.SAILORID = p.SailorID
                                    INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                    INNER JOIN punishmenttype pt on pt.PunishmentTypeID = p.PunishmentTypeID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = p.ShipID
                                    WHERE p.PunishmentID = $id")->row();
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['punishmentType'] = $this->utilities->findAllByAttribute("punishmenttype", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['offenceCat'] = $this->utilities->findAllByAttribute("bn_offencecatagory", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1;/*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/punishment_activity/punishment_edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */
    function searchNextRank(){
        $rankId = $this->input->post('rankId', true);
        print_r($rankId);
    }
    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function punishment_update() {

        $id = $this->input->post('id', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $punishmentCauses = $this->input->post('punishmentCauses', true);
        $offenceCat = $this->input->post('offenceCat', true);
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));

        $PUNISHMENT_NO = $this->input->post('PUNISHMENT_NO', true);
        $punishmentDate = date('Y-m-d', strtotime($this->input->post('punishmentDate', true)));
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));

        $punishment = array(
            'PunishmentTypeID' => $PUNISHMENT_NO,
            'EffectDate' => $punishmentDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $DAO->DAO_NO,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'ShipID' => $AUTHO_SHIP_ESTABLISHMENT,
            'Cause' => $punishmentCauses,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );

        if ($this->utilities->updateData('punishment', $punishment, array('PunishmentID' => $id))) {
            $this->utilities->deleteRowByAttribute('punishmentoffence', array('PunishmentID' => $id));
            if (!empty($offenceCat)) {
                for ($i = 0; $i < count($offenceCat); $i++) {
                    $punishOffence = array(
                        "OffenceCatagoryID" => $offenceCat[$i],
                        "PunishmentID" => $id,
                        "UPD_BY" => $this->user_session["USER_ID"]
                    );
                    if ($this->utilities->insertData($punishOffence, 'punishmentoffence')) {
                        $success = 1;
                    }
                }
            }
            echo "<div class='alert alert-success'>Punishment updated successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>punishment updated Failed</div>";
        }
    }

    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function punishment_view( $id) {
        
        $data['viewdetails'] = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, pt.Name PUNISHMENT_TYPE, se.AREA_ID,se.NAME SHIP_EST
                                                        FROM punishment p
                                                        INNER JOIN sailor s on s.SAILORID = p.SailorID
                                    INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                    INNER JOIN punishmenttype pt on pt.PunishmentTypeID = p.PunishmentTypeID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = p.ShipID
                                    WHERE p.PunishmentID= $id")->row();

       $data['offenceCat'] = $this->db->query("SELECT oc.*FROM bn_offencecatagory oc 
                                                INNER JOIN punishmentoffence p on oc.OFFENCE_CATAGORYID = p.OffenceCatagoryID
                                                WHERE p.PunishmentID= $id")->result();

        // echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/punishment_activity/punishment_view', $data);
    }

    function ajaxPunishmentActivityList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'p.PunishmentID', 1 => 's.OFFICIALNUMBER', 2 => 'pt.Name', 3 => 'p.EffectDate', 4 => 'p.Cause', 5 => 'p.PunishmentID');

        // getting total number records without any search

        $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, pt.Name PUNISHMENT_TYPE
                                    FROM punishment p
                                    INNER JOIN sailor s on s.SAILORID = p.SailorID
                                    INNER JOIN punishmenttype pt on pt.PunishmentTypeID = p.PunishmentTypeID
                                    WHERE s.SAILORSTATUS = 3")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, pt.Name PUNISHMENT_TYPE
                                    FROM punishment p
                                    INNER JOIN sailor s on s.SAILORID = p.SailorID
                                    INNER JOIN punishmenttype pt on pt.PunishmentTypeID = p.PunishmentTypeID
                                    WHERE s.SAILORSTATUS = 3 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR pt.Name LIKE '" . $requestData['search']['value'] . "%' OR p.EffectDate LIKE '" . $requestData['search']['value'] . "%' OR p.Cause LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, pt.Name PUNISHMENT_TYPE
                                    FROM punishment p
                                    INNER JOIN sailor s on s.SAILORID = p.SailorID
                                    INNER JOIN punishmenttype pt on pt.PunishmentTypeID = p.PunishmentTypeID
                                    WHERE s.SAILORSTATUS = 3
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->PUNISHMENT_TYPE;
            $nestedData[] = date("Y-m-d", strtotime($row->EffectDate));
            $nestedData[] = $row->Cause;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('regularTransaction/punishmentActivity/punishment_view/' . $row->PunishmentID) . '" title="View punishment Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('regularTransaction/punishmentActivity/punishment_edit/' . $row->PunishmentID) . '" title="Edit punishment Information" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->PunishmentID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="PunishmentID" data-tbl="punishment"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file punishmentActivity.php */
/* Location: ./application/controllers/regularTransaction/punishmentActivity.php */