<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Medical Category
 * @package    Medical Category Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class MedicalCatInfo extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Medical category Information';
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/medical_cat_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", "DESC");
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['appointment'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/medical_cat_info/create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function save() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $medicalCat = $this->input->post('medicalCat', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $EffectiveDate = date('Y-m-d', strtotime($this->input->post('EffectiveDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $remarks = $this->input->post('remarks', true);

        $medical_1 = array(
            'SailorID' => $SAILOR_ID,
            'Medicalcategory' => $medicalCat,
            'Effectivedate' => $EffectiveDate,
            'Description' => $remarks,
            'AuthorityNumber' => $authorityNumber,
            'Authoritydate' => $authorityDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $DAO->DAO_NO,
            'IsActive' => 1,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if (isset($_POST['categoryType'])) {
            $categoryType = $this->input->post('categoryType', true);
            $medical_2 = array('CategoryType' => $categoryType);
            if ($categoryType == 2) {
                $Duration = $this->input->post('Duration', true);
                $medical_3 = array('Duration' => $Duration);
                $medical = array_merge($medical_1, $medical_2, $medical_3);
                $this->medicalInsert($medical);
            } else {
                $medical = array_merge($medical_1, $medical_2);
                $this->medicalInsert($medical);
            }
        } else {
            $this->medicalInsert($medical_1);
        }
    }

    function medicalInsert($medical) {
        if ($medicalID = $this->utilities->insert('medical', $medical)) {
            echo "<div class='alert alert-success'>Medical category info added successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Medical category info Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, se.NAME SHIP_EST
                                            FROM medical m
                                            INNER JOIN sailor s on s.SAILORID = m.SailorID
                                            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                            WHERE m.MedicalID = $id")->row();
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", "DESC");
        $data['flag'] = 1; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/medical_cat_info/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function update() {
        $id = $this->input->post('id', true);
        $medicalCat = $this->input->post('medicalCat', true);
        $Duration = $this->input->post('Duration', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $EffectiveDate = date('Y-m-d', strtotime($this->input->post('EffectiveDate', true)));
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $remarks = $this->input->post('remarks', true);

        $update = array(
            'Medicalcategory' => $medicalCat,
            'Duration' => $Duration,
            'Effectivedate' => $EffectiveDate,
            'Description' => $remarks,
            'AuthorityNumber' => $authorityNumber,
            'Authoritydate' => $authorityDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $DAO->DAO_NO,
            'IsActive' => 1,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );

        if ($this->utilities->updateData('medical', $update, array('MedicalID' => $id))) {
            echo "<div class='alert alert-success'>Medical Category Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Medical Category Update Failed</div>";
        }
    }

    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, se.NAME SHIP_EST
                                            FROM medical m
                                            INNER JOIN sailor s on s.SAILORID = m.SailorID
                                            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                            WHERE m.MedicalID = $id")->row();
//        echo '<pre>';
//        print_r($data['viewdetails']);
//        exit;
        $this->load->view('regularTransaction/medical_cat_info/view', $data);
    }

    /**
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    function ajaxMedicalInfo() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'm.MedicalID', 1 => 's.OFFICIALNUMBER', 2 => 'm.CategoryType', 3 => 'm.MedicalCategory', 4 => 'm.Effectivedate', 5 => 'm.Duration', 6 => 'm.MedicalID');

        // getting total number records without any search

        $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER,  IF(m.CategoryType = 1,'Permanent','Temporary') as m_cat
                                    FROM medical m
                                    INNER JOIN sailor s on s.SAILORID = m.SailorID
                                    WHERE s.SAILORSTATUS = 3")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER,  IF(m.CategoryType = 1,'Permanent','Temporary') as m_cat
                                        FROM medical m
                                        INNER JOIN sailor s on s.SAILORID = m.SailorID
                                        WHERE s.SAILORSTATUS = 3 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR m.CategoryType LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER,  IF(m.CategoryType = 1,'Permanent','Temporary') as m_cat
                                        FROM medical m
                                        INNER JOIN sailor s on s.SAILORID = m.SailorID
                                        WHERE s.SAILORSTATUS = 3
                                        ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        $medicalCat = array("1" => "A (AYE)", "2" => "B (BEE)", "3" => "C (CEE)", "4" => "D (DEE)", "5" => "E (EEE)");
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->m_cat;
            $nestedData[] = array_key_exists($row->MedicalCategory, $medicalCat) ? $medicalCat[$row->MedicalCategory] : '';
            $nestedData[] = date("Y-m-d", strtotime($row->Effectivedate));
            $nestedData[] = $row->Duration;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('Retirement/HistoryTran/medicalCatInfo/view/' . $row->MedicalID) . '" title="View Medical category info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('Retirement/HistoryTran/medicalCatInfo/edit/' . $row->MedicalID) . '" title="Edit Medical category info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->MedicalID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="MedicalID" data-tbl="medical"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file medicalCatInfo.php */
/* Location: ./application/controllers/regularTransaction/medicalCatInfo.php */