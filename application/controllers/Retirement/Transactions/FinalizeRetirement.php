<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Retirement
 * @package    Retirement Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class FinalizeRetirement extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Retirement Finalize';
        $data['sailor'] = $this->db->query("SELECT s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.Name cause, r.*
                                            FROM retirement r
                                            JOIN sailor s on s.SAILORID = r.SailorID
                                            JOIN bn_rank br on br.RANK_ID = s.RANKID
                                            JOIN causetype ct on ct.CauseTypeID = r.CauseTypeID
                                            WHERE s.SAILORSTATUS = 2 ")->result();
        $data['content_view_page'] = 'retirement/Transactions/retirement_finalize/index';
        $this->template->display($data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul <Emdadul@atilimited.net>
     * @return      Edit retirement Process
     */
    function edit($id){
        $data['causetype'] = $this->utilities->findAllByAttribute('causetype', array("ACTIVE_STATUS" => 1, "SubType !=" => 0));
        $data['benefits'] = $this->db->query("SELECT * FROM causetype WHERE Cause = 5")->result();
        $data['dao'] = $this->utilities->findAllByAttribute('bn_dao', array("ACTIVE_STATUS" => 1));        
        $data['retirement'] = $this->db->query("SELECT s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.SubType, ct.Name cause, r.*
                                            FROM retirement r
                                            JOIN sailor s on s.SAILORID = r.SailorID
                                            JOIN bn_rank br on br.RANK_ID = s.RANKID
                                            JOIN causetype ct on ct.CauseTypeID = r.CauseTypeID
                                            WHERE r.RetirementID = $id ")->row();
        $data["retirementBen"] = $this->utilities->findAllByAttribute("retirementbenefits", array("RetirementID" => $id));
        $data['content_view_page'] = 'retirement/Transactions/retirement_process/edit';
        $this->template->display($data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul <Emdadul@atilimited.net>
     * @return      Retirement Process Info
     */
    
    function view($id){
        $data['viewdetails'] = $this->db->query("SELECT s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.Name cause, r.*
                                            FROM retirement r
                                            JOIN sailor s on s.SAILORID = r.SailorID
                                            JOIN bn_rank br on br.RANK_ID = s.RANKID
                                            JOIN causetype ct on ct.CauseTypeID = r.CauseTypeID
                                            WHERE r.RetirementID = $id ")->row();
        $this->load->view('retirement/Transactions/retirement_finalize/view', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul <Emdadul@atilimited.net>
     * @return      Sailor Status update to 2 to 3 (Process = 2, Release=3)
     */
    public function finalizeApprove() 
    {
        $sailorID = $this->input->post("sailorID");
        $retirementId = $this->input->post("RetirementId");

        /*update sailor sailorStatus 2 to 3*/        
        $sailorUpdate = array(
                    "SAILORSTATUS" => 3
                );
        if($this->utilities->updateData("sailor", $sailorUpdate, array("SAILORID" => $sailorID)))
        {
            
            /*Retirement finalize update 0 to 1*/
            $retirementUpdate = array(
                                    "IsFinalized" => 1
                                );
            if($this->utilities->updateData("retirement", $retirementUpdate, array("RetirementID" => $RetirementId)))
            {
                echo "Y";
            }
        }
    }

}