 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class PaymentInfo extends CI_Controller
{
    private $now;
    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }
    /**
     * @access      public
     * @param       none
     * @author      Emran  <emran@atilimited.net>
     * @return      templete
     */
    public function index()
    {
        $data['breadcrumbs']       = array(
            'Modules' => '#'
        );
        $data['pageTitle']         = 'Re-Approbal of Payment Information';
        $data['content_view_page'] = 'Retirement/Transactions/paymentInformation/index';
        $this->template->display($data);
    }
    //emran
    
    
    
    public function searchPostingUnitID()
    {
        $PostingUnitID    = $this->input->post("posting", true);
        $data['rankData'] = $this->utilities->findAllByAttribute("bn_rank", array(
            "ACTIVE_STATUS" => 1
        ));
        $data['partII']   = $this->utilities->findAllByAttribute("partii", array(
            "ACTIVE_STATUS" => 1
        ));
        // echo "<pre>";
        // print_r($PostingUnitID);
        // exit;
        $data['unitData'] = $this->db->query("
                                        SELECT UnitWiseSanctionID,
                                               ShipEstablishmentID,
                                               PostingUnitID,
                                               RankID,
                                               PartIIID,
                                               SanctionNo,
                                               Remarks,
                                               CRE_BY,
                                               CRE_DT,
                                               UPD_BY,
                                               UPD_DT,
                                               IsOrganizationWise,
                                               OrganizationID
                                          FROM unitwisesanction
                                          where PostingUnitID = $PostingUnitID
                                        ")->result();
        $this->load->view('othersInfo/unitWiseSanction/unitWiseData', $data);
    }
    
    public function saveData()
    {
        $shiftEstablishment   = $this->input->post('shiftEstablishment', true);
        $postingUnitName      = $this->input->post('postingUnitName', true);
        $RankName             = $this->input->post('RankName', true);
        $shiftEstablishmentId = $this->input->post('UnitWiseSanctionID');
        $PartName             = $this->input->post('PartName', true);
        $SanctionNo           = $this->input->post('SanctionNo', true);
        $Remarks              = $this->input->post('Remarks', true);
        $success              = 0;
        for ($i = 0; $i < count($RankName); $i++) {
            if ($i >= count($shiftEstablishmentId)) {
                $data = array(
                    'ShipEstablishmentID' => $shiftEstablishment,
                    'PostingUnitID' => $postingUnitName,
                    'RankID' => $RankName[$i],
                    'PartIIID' => $PartName[$i],
                    'SanctionNo' => $SanctionNo[$i],
                    'Remarks' => $Remarks[$i],
                    'CRE_BY' => $this->user_session["USER_ID"]
                );
                if ($this->utilities->insertData($data, 'unitwisesanction')) { // if data inserted successfully
                    $success = 1;
                }
            } else {
                $data = array(
                    'ShipEstablishmentID' => $shiftEstablishment,
                    'PostingUnitID' => $postingUnitName,
                    'RankID' => $RankName[$i],
                    'PartIIID' => $PartName[$i],
                    'SanctionNo' => $SanctionNo[$i],
                    'Remarks' => $Remarks[$i],
                    'UPD_BY' => $this->user_session["USER_ID"],
                    'UPD_DT' => date("Y-m-d:H-i-s")
                );
                
                if ($this->utilities->updateData('unitwisesanction', $data, array(
                    "UnitWiseSanctionID" => $shiftEstablishmentId[$i]
                ))) {
                    $success = 1;
                }
            }
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Unit Wise Sanction Inserted successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Unit Wise Sanction Inserted Failed</div>";
        }
    }
    
    //emran
    
    public function deleteUnitData()
    {
        $success             = 0;
        $ShipEstablishmentID = $this->input->post("UnitWiseSanctionID");
        for ($i = 0; $i < count($ShipEstablishmentID); $i++) {
            $this->utilities->deleteRowByAttribute('unitwisesanction', array(
                "UnitWiseSanctionID" => $ShipEstablishmentID[$i]
            ));
            $success = 1;
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Data Delete successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Data Delete Failed</div>";
        }
    }
    
}
/* End of file unitWiseSanction.php */
/* Location: ./application/controllers/othersInfo/UnitWaiseSanction.php */ 