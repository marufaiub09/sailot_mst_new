 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class STCInformation extends CI_Controller
{
    private $now;
    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }
    /**
     * @access      public
     * @param       none
     * @author      Emran  <emran@atilimited.net>
     * @return      templete
     */
    public function index()
    {
        $data['breadcrumbs']       = array(
            'Modules' => '#'
        );
        $data['pageTitle']         = 'STC Information';
        $data['content_view_page'] = 'Retirement/Transactions/STCInformation/index';
        $this->template->display($data);
    }
    //emran

      public function ifExistData() {
          var_dump($_POST);
          exit;
        $sailor_id = $this->input->post("sailor_id");
        $data['STC'] = $this->db->query("SELECT s.FULLNAME,
                                                r.RANK_NAME,
                                                s.SAILORID,
                                                stc.STCInformationID,
                                                stc.RetirementID,
                                                stc.STCNumber,      
                                                DATE_FORMAT(stc.IssueDate, '%d-%m-%Y') IssueDate,
                                                stc.AuthorityNumber,
                                                DATE_FORMAT(stc.AuthorityDate, '%d-%m-%Y') AuthorityDate
                                                from sailor s
                                                INNER JOIN bn_rank r ON r.RANK_ID = s.RANKID
                                                INNER JOIN retirement re ON re.SailorID = s.SAILORID
                                                INNER JOIN stc stc ON stc.RetirementID = re.RetirementID
                                                where s.SAILORID = $sailor_id")->result();
        $this->load->view('Retirement/Transactions/STCInformation/STCData', $data);
    }
    
    
    //emran
    
    public function deleteUnitData()
    {
        $success             = 0;
        $ShipEstablishmentID = $this->input->post("UnitWiseSanctionID");
        for ($i = 0; $i < count($ShipEstablishmentID); $i++) {
            $this->utilities->deleteRowByAttribute('unitwisesanction', array(
                "UnitWiseSanctionID" => $ShipEstablishmentID[$i]
            ));
            $success = 1;
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Data Delete successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Data Delete Failed</div>";
        }
    }
    
}
/* End of file unitWiseSanction.php */
/* Location: ./application/controllers/othersInfo/UnitWaiseSanction.php */ 