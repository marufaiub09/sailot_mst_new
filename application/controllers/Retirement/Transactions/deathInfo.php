 <?php
defined('BASEPATH') OR exit('No direct script access allowed');
class deathInfo extends CI_Controller
{
    private $now;
    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }
    /**
     * @access      public
     * @param       none
     * @author      Emran  <emran@atilimited.net>
     * @return      templete
     */
    public function index()
    {
        $data['breadcrumbs']       = array(
            'Modules' => '#'
        );
        $data['pageTitle']         = 'STC Information';
        $data['content_view_page'] = 'Retirement/Transactions/SailorDeathInformation/index';
        $this->template->display($data);
    }
    //emran
    
    function searchSailor(){
        $officalNumber = $this->input->post("officeNumber");
        $sailorStatus = $this->input->post("sailorStatus");
        $this->db->select('s.FULLNAME,r.RANK_NAME,s.SAILORID, re.RetirementID,re.DeathCause, DATE_FORMAT(re.DeathDate,"%d-%m-%Y")DeathDate,re.DeathAuthorityNumber, DATE_FORMAT(re.DeathAuthorityDate,"%d-%m-%Y")DeathAuthorityDate');
        $this->db->from('sailor as s');
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID','INNER');
        $this->db->join('retirement as re', 're.SailorID = s.SAILORID','INNER'); 
        $condition = array('s.OFFICIALNUMBER'=> $officalNumber, 's.SAILORSTATUS'=> $sailorStatus);
        $this->db->where($condition);
        echo json_encode($this->db->get()->row_array());
    }
    
    //emran
    
    
    public function save() {
        /* Academic Information */
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $deathDate = date('Y-m-d', strtotime($this->input->post('deathDate', true)));
        $deathCause = $this->input->post('deathCause', true);
        $authorityNu = $this->input->post('authorityNu', true);
        $authority = date('Y-m-d', strtotime($this->input->post('authority', true)));
        var_dump($deathCause);
        /* end Education insert part */
    
            $deathdata = array(
                'DeathDate' => $deathDate,
                'DeathCause' => $deathCause,
                'DeathAuthorityNumber' => $authorityNu,
                'DeathAuthorityDate' => $authority
            );
            if ($this->utilities->updateData('retirement', $deathdata, array("SailorID" => $SAILOR_ID) )) {
                /* end Academic insert part */
                $success = 1;
            }
        
        if ($success == 1) {
            echo "<div class='alert alert-success'>Death Information Update successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Death Information Not successfully Updated</div>";
        }
    }
    
  
    
}
/* End of file unitWiseSanction.php */
/* Location: ./application/controllers/othersInfo/UnitWaiseSanction.php */ 