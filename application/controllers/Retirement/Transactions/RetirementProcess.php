<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Retirement
 * @package    Retirement Process
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class RetirementProcess extends CI_Controller {

    public function __construct() 
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Retirement Process';
        $data['benefits'] = $this->db->query("SELECT * FROM causetype WHERE Cause = 5")->result();
        $data['dao'] = $this->utilities->findAllByAttribute('bn_dao', array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'retirement/Transactions/retirement_process/index';
        $this->template->display($data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      cause according to release Type
     */
    public function releaseCause() {
        $causeId = $_POST['causeId'];
        if($causeId == 1)
        {
            $query = $this->utilities->findAllByAttributeWithOrderBy('causetype', array("ACTIVE_STATUS" => 1, "Cause" => $causeId, "SubType !=" => 0),"Name");
            $returnVal = '<option value = "">Select One</option>';
            if (!empty($query)) {
                foreach ($query as $row) {
                    $returnVal .= '<option value = "' . $row->CauseTypeID . '">' . $row->Name . '</option>';
                }
            }
            echo $returnVal;
            
        }
        elseif($causeId == 2)
        {
            $query = $this->utilities->findAllByAttributeWithOrderBy('causetype', array("ACTIVE_STATUS" => 1, "Cause" => 1, "SubType" => 2),"Name");
            $returnVal = '<option value = "">Select One</option>';
            if (!empty($query)) {
                foreach ($query as $row) {
                    $returnVal .= '<option value = "' . $row->CauseTypeID . '">' . $row->Name . '</option>';
                }
            }
            echo $returnVal;    
        }
        else
        {
            $query = $this->utilities->findAllByAttributeWithOrderBy('causetype', array("ACTIVE_STATUS" => 1, "Cause" => 1, "SubType" => 3),"Name");
            $returnVal = '<option value = "">Select One</option>';
            if (!empty($query)) {
                foreach ($query as $row) {
                    $returnVal .= '<option value = "' . $row->CauseTypeID . '">' . $row->Name . '</option>';
                }
            }
            echo $returnVal;    
        }
    }
    public function leave()
    {
        $SAILOR_ID = $this->input->post("SAILOR_ID");

        $leaveQuery = $this->db->query("SELECT lv.LeaveID,lvd.LeaveYear,lvd.LeaveAvailed 
                                        FROM navy_db_final.`leave` AS lv
                                        JOIN leavedetail lvd ON lvd.LeaveID = lv.LeaveID 
                                        WHERE lv.SailorID = $SAILOR_ID")->result();
        $leave = array('leave' => $leaveQuery);
        echo json_encode($leave);
        /*echo '<pre>';
        print_r($leaveQuery);
        exit();*/
    }
    public function save()
    {
        error_reporting('0');
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $releaseType = $this->input->post('releaseType', true);
        $releaseCause = $this->input->post('releaseCause', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_NO', true);
        $releaseDate = date('Y-m-d', strtotime($this->input->post('releaseDate', true)));
        $lrpAvailed = $this->input->post('lrpAvailed', true);
        $actreleaseDate = date('Y-m-d', strtotime($this->input->post('actreleaseDate', true)));
        $actleaveDate = $this->input->post('actleaveDate', true);
        $benefits = $this->input->post('benefits', true);
        $lrp = $this->input->post('lrp', true);
        if ($lrp != 1)
        {
            $lrp = 0;
        }
        else
        {
            $lrp = $this->input->post('lrp', true);
        }

        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $data = array(
            'SailorID' => $SAILOR_ID,
            'CauseTypeID' => $releaseCause,
            'LPR' => $lrp,
            'LPRDays' => $lrpAvailed,
            'ApproveDate' =>$releaseDate,
            'AccumulatedDays' => $actleaveDate,
            'ActualReleaseDate' => $actreleaseDate,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAONumber' => $DAO->DAO_NO,
            'DAOID' => $DAO_ID
        );
       // var_dump($data);
        if ($RetirementID = $this->utilities->insert('retirement', $data))
        {
                for ($i = 0; $i < count($benefits); $i++) {
                $databenefit = array(
                    'RetirementID' => $RetirementID,
                    'BenefitsID' => $benefits[$i]
                    );
              
                $as = $this->utilities->insertData($databenefit, 'retirementbenefits');
                    
               }
               if($as == true)
                {
                    $UpdateDate = array(
                            'SAILORSTATUS' => 2
                    );
                    if ($this->utilities->updateData('sailor', $UpdateDate, array('SAILORID' => $SAILOR_ID)))
                    {
                        echo "<div class='alert alert-success'>Retirements Data Inserted And Sailor data Update Successfully</div>";
                    }
                    
                }
                else
                {
                    echo "<div class='alert alert-success'>No Retirement Benefits Select!!!</div>";
                    $UpdateDate = array(
                            'SAILORSTATUS' => 2
                    );
                    if ($this->utilities->updateData('sailor', $UpdateDate, array('SAILORID' => $SAILOR_ID)))
                    {
                        echo "<div class='alert alert-success'>Retirements Data Inserted And Sailor data Update Successfully With Out Retirement Benefits</div>";
                    }
                }
        }
        else 
        {
            echo "<div class='alert alert-success'> Failed To Insert</div>";
        }
    }
     /**
     * @access      public
     * @param       none
     * @author      Emdadul <Emdadul@atilimited.net>
     * @return      Retirement Process Update
     */
    function update(){
        error_reporting('0');
        $RetirementID = $this->input->post('id', true);
        $releaseType = $this->input->post('releaseType', true);
        $releaseCause = $this->input->post('releaseCause', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_NO', true);
        $releaseDate = date('Y-m-d', strtotime($this->input->post('releaseDate', true)));
        $lrpAvailed = $this->input->post('lrpAvailed', true);
        $actreleaseDate = date('Y-m-d', strtotime($this->input->post('actreleaseDate', true)));
        $actleaveDate = date('Y-m-d', strtotime($this->input->post('actleaveDate', true)));
        $benefits = $this->input->post('benefits', true);
        $lrp = $this->input->post('lrp', true);
        if ($lrp != 1)
        {
            $lrp = 0;
        }
        else
        {
            $lrp = $this->input->post('lrp', true);
        }

        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $data = array(
            'CauseTypeID' => $releaseCause,
            'LPR' => $lrp,
            'LPRDays' => $lrpAvailed,
            'ApproveDate' =>$releaseDate,
            'AccumulatedDays' => $actleaveDate,
            'ActualReleaseDate' => $actreleaseDate,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAONumber' => $DAO->DAO_NO,
            'DAOID' => $DAO_ID
        );
        if ($this->utilities->updateData('retirement', $data, array("RetirementID" => $RetirementID)))
        {
            $this->utilities->deleteRowByAttribute("retirementbenefits", array("RetirementID" => $RetirementID));
            for ($i = 0; $i < count($benefits); $i++) {
                $databenefit = array(
                    'RetirementID' => $RetirementID,
                    'BenefitsID' => $benefits[$i]
                    );
                $as = $this->utilities->insertData($databenefit, 'retirementbenefits');
            }
            if($as == true)
            {
                $UpdateDate = array(
                        'SAILORSTATUS' => 2
                );
                if ($this->utilities->updateData('sailor', $UpdateDate, array('SAILORID' => $SAILOR_ID)))
                {
                    echo "<div class='alert alert-success'>Retirements Data Update Successfully</div>";
                }                
            }            
        }
        else 
        {
            echo "<div class='alert alert-success'> Failed To Insert</div>";
        }

    }
    

}