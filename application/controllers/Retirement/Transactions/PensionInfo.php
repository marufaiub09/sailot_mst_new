<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Pension
 * @package    Pension Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class PensionInfo extends CI_Controller {

	   private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Pension Information';
        $data['content_view_page'] = 'Retirement/Transactions/PensionInfo/index';
        $this->template->display($data);
    }

    

}