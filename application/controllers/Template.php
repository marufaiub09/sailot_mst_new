<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Template extends CI_Controller
{
    public function __construct() {
        parent::__construct();
    }

    /**
     * @description
     */
    public function index(){

        $data['metaTitle'] = 'Welcome to the templating';
        $data['breadcrumb'] = array(
            'Dashboard' => 'academic/index',
            'Dashboard List' => '#'
        );
        $data['pageTitle'] = 'Welcome to the test page';
        $data['content_view_page'] = 'layouts/admin/another_test';
        $this->admin_template->display($data);
    }
}