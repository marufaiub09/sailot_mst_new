<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BranchAndRankWiseSection extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran  <emran@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Branch And Rank Wise Section';
        $data['branch'] = $this->utilities->findAllByAttribute("bn_branch", array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'othersInfo/branchAndRankWiseSection/index';
        $this->template->display($data);
    }

    public function branchAndRankWiseSection() {
        $branch_id = $this->input->post("branch_id");
        $data["branch_id"] = $this->utilities->branchAndRankWiserData($branch_id);
        echo $this->load->view('othersInfo/branchAndRankWiseSection/index', $data, true);
    }

    public function searchBranchAndRankData() {
        $branch_id = $this->input->post("branch_id", true);
        $data['branchRank'] = $this->db->query("SELECT b.BRANCH_NAME, r.RANK_ID,
                                        r.RANK_NAME,
                                        r.BRANCH_ID,
                                        r.SANCTION_NUMBER
                                        FROM bn_rank r
                                        INNER JOIN bn_branch b on b.BRANCH_ID = r.BRANCH_ID
                                        WHERE b.BRANCH_ID = $branch_id
                                        ")->result();
        $this->load->view('othersInfo/branchAndRankWiseSection/branchRankList', $data);
    }

//emran
    public function saveBranchAndRank() {
        $rank_id = $this->input->post("rank_id");
        $sectionName = $this->input->post("sectionNumber");
        for ($i = 0; $i < count($rank_id); $i++) {
            $id = $rank_id[$i];
            $ins = array(
                "SANCTION_NUMBER" => $sectionName[$i]
            );
            $this->utilities->updateData('bn_rank', $ins, array("RANK_ID" => $id));
        }
        redirect('othersInfo/BranchAndRankWiseSection/index', 'refresh');
    }

//emran

    public function deleteBranchAndRank() {
        // echo "<pre>";
        // print_r($_POST);
        // exit();
        $rank_id = $this->input->post("rank_id");
        $id = $rank_id[$i];
        // print_r($id);
        // exit;
        for ($i = 0; $i < count($rank_id); $i++) {
            $id = $rank_id[$i];

            $this->utilities->deleteRowByAttribute('bn_rank', array("RANK_ID" => $id));
        }
        //redirect('othersInfo/BranchAndRankWiseSection/index', 'refresh');
    }

//emran
    function ajaxBranchAndRank() {
// storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;
        $columns = array(
// datatable column index  => database column name
            0 => 'r.RANK_NAME', 1 => 'r.SANCTION_NUMBER');
// getting total number records without any search
        $query = $this->db->query("SELECT b.BRANCH_NAME, r.RANK_ID,
                                    r.RANK_NAME,
                                    r.BRANCH_ID,
                                    r.SANCTION_NUMBER
                                    FROM bn_rank r
                                    INNER JOIN bn_branch b on b.BRANCH_ID = r.BRANCH_ID")->num_rows();
        $totalData = $query;
        $totalFiltered = $totalData;
// when there is no search parameter then total number rows = total number filtered rows.
        if (!empty($requestData['search']['value'])) {
// if there is a search parameter
            $query = $this->db->query("SELECT b.BRANCH_NAME, r.RANK_ID,
                                        r.RANK_NAME,
                                        r.BRANCH_ID,
                                        r.SANCTION_NUMBER
                                        FROM bn_rank r
                                        INNER JOIN bn_branch b on b.BRANCH_ID = r.BRANCH_ID
                                        WHERE b.BRANCH_NAME LIKE '" . $requestData['search']['value'] . "%' OR r.RANK_NAME LIKE '" . $requestData['search']['value'] . "%' OR r.SANCTION_NUMBER LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */
            $totalFiltered = $query;
// when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {
            $query = $this->db->query("SELECT b.*, r.RANK_ID,
                                        r.RANK_NAME,
                                        r.BRANCH_ID,
                                        r.SANCTION_NUMBER
                                        FROM bn_rank r
                                        INNER JOIN bn_branch b on b.BRANCH_ID = r.BRANCH_ID
                                        ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
// preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->RANK_NAME;
            $nestedData[] = $row->SANCTION_NUMBER;
            $nestedData[] = '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->RANK_ID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="ForeignVisitID" data-tbl="spectran"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
// for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
// total number of records
            "recordsFiltered" => intval($totalFiltered),
// total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data
// total data array
        );
        echo json_encode($json_data);
    }

}

/* End of file foreignVisitInfo.php */
/* Location: ./application/controllers/othersInfo/branchAndRankWiseSection.php */