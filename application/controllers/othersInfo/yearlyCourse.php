<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class YearlyCourse extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Yearly Course Plan';
        $data['result'] = $this->db->query("SELECT a.*, b.Name COURSE_NAME,c.Name Training_Institute, d.BATCH_NUMBER Batch_name FROM bn_yearlycourseplan a
                                            INNER JOIN bn_batchnumber d on d.BATCH_ID = a.BatchNumber
                                            INNER JOIN bn_navytraininghierarchy b on b.NAVYTrainingID = a.CourseID
                                            INNER JOIN bn_traininginstitute c on c.Training_Institute_ID = a.InstituteID")->result();
        //echo '<pre>';print_r($data['result']);exit;
        $data['content_view_page'] = 'othersInfo/yearlyCourse/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul slam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['course'] = $this->db->query("select * FROM bn_navytraininghierarchy where ParentID is not null and NAVYTrainingType != '2'")->result();
        $data['batch_number'] = $this->db->query("SELECT * FROM `bn_batchnumber` WHERE ACTIVE_STATUS = 1")->result();
        $data['institute_name'] = $this->db->query("SELECT * FROM `bn_traininginstitute` WHERE ACTIVE_STATUS = 1")->result();
        $this->load->view('othersInfo/yearlyCourse/create', $data);
    }

    public function savePart() {
        $Course = $this->input->post('Course', true);
        $batch_number = $this->input->post('batch_number', true);
        $date1 = explode("-", $this->input->post('starting_date'));
        $start_date = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
        $duration = $this->input->post('duration', true);
        $date2 = explode("-", $this->input->post('completion_date'));
        $completion_date = $date2[2] . '-' . $date2[1] . '-' . $date2[0];
        $trainees_no = $this->input->post('trainees_no', true);
        $institute_name = $this->input->post('institute_name', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
            $data = array(
                'CourseID' => $Course,
                'BatchNumber' => $batch_number,
                'StartDate' => $start_date,
                'Duration' => $duration,
                'EndDate' => $completion_date,
                'BatchSize' => $trainees_no,
                'InstituteID' => $institute_name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_yearlycourseplan')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Yearly Course Plan Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Yearly Course Plan insert failed</div>";
            }
        } 
    

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      single row
     */
    function examNameById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT a.*, b.Name COURSE_NAME,c.Name Training_Institute, d.BATCH_NUMBER Batch_name FROM bn_yearlycourseplan a
                                            INNER JOIN bn_batchnumber d on d.BATCH_ID = a.BatchNumber
                                            INNER JOIN bn_navytraininghierarchy b on b.NAVYTrainingID = a.CourseID
                                            INNER JOIN bn_traininginstitute c on c.Training_Institute_ID = a.InstituteID  WHERE a.YearlyCoursePlanID  = $id")->row();
        //echo '<pre>';print_r($data['row']);exit;
        $this->load->view('othersInfo/yearlyCourse/single_row', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function partList() {
        $data['result'] = $this->db->query("SELECT a.*, b.Name COURSE_NAME,c.Name Training_Institute, d.BATCH_NUMBER Batch_name FROM bn_yearlycourseplan a
                                            INNER JOIN bn_batchnumber d on d.BATCH_ID = a.BatchNumber
                                            INNER JOIN bn_navytraininghierarchy b on b.NAVYTrainingID = a.CourseID
                                            INNER JOIN bn_traininginstitute c on c.Training_Institute_ID = a.InstituteID")->result();
        $this->load->view("othersInfo/yearlyCourse/list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT a.*, b.Name COURSE_NAME,c.Name Training_Institute, d.BATCH_NUMBER Batch_name FROM bn_yearlycourseplan a
                                            INNER JOIN bn_batchnumber d on d.BATCH_ID = a.BatchNumber
                                            INNER JOIN bn_navytraininghierarchy b on b.NAVYTrainingID = a.CourseID
                                            INNER JOIN bn_traininginstitute c on c.Training_Institute_ID = a.InstituteID  WHERE a.YearlyCoursePlanID = $id")->row();
        //echo '<pre>';print_r($data['result']);exit;
        // $data['result'] = $this->utilities->findByAttribute('bn_yearlycourseplan', array('YearlyCoursePlanID' => $id));
        $data['course'] = $this->db->query("select * FROM bn_navytraininghierarchy where ParentID is not null and NAVYTrainingType != '2'")->result();
        $data['batch_number'] = $this->db->query("SELECT * FROM `bn_batchnumber` WHERE ACTIVE_STATUS = 1")->result();
        // echo '<pre>';print_r($data['result']);exit;
        $data['institute_name'] = $this->utilities->findAllByAttributeWithOrderBy("bn_traininginstitute", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('othersInfo/yearlyCourse/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    public function updatePart() {
        $id = $this->input->post('id', true);
        //echo '<pre>';print_r($id);exit;
        $Course = $this->input->post('Course', true);
        $batch_number = $this->input->post('batch_number', true);
        $date1 = explode("-", $this->input->post('starting_date'));
        $start_date = $date1[2] . '-' . $date1[1] . '-' . $date1[0];
        $duration = $this->input->post('duration', true);
        $date2 = explode("-", $this->input->post('completion_date'));
        $completion_date = $date2[2] . '-' . $date2[1] . '-' . $date2[0];
        $trainees_no = $this->input->post('trainees_no', true);
        $institute_name = $this->input->post('institute_name', true);
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;
        // checking if part with this name is already exist
            $data = array(
                'CourseID' => $Course,
                'BatchNumber' => $batch_number,
                'StartDate' => $start_date,
                'Duration' => $duration,
                'EndDate' => $completion_date,
                'BatchSize' => $trainees_no,
                'InstituteID' => $institute_name,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );              
          // echo '<pre>';print_r($data);exit;
            if ($this->utilities->updateData('bn_yearlycourseplan', $data, array("YearlyCoursePlanID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Yearly Course Plan Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Yearly Course Plan Update failed</div>";
            }
        
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT a.*, b.Name COURSE_NAME,c.Name Training_Institute, d.BATCH_NUMBER Batch_name FROM bn_yearlycourseplan a
                                            INNER JOIN bn_batchnumber d on d.BATCH_ID = a.BatchNumber
                                            INNER JOIN bn_navytraininghierarchy b on b.NAVYTrainingID = a.CourseID
                                            INNER JOIN bn_traininginstitute c on c.Training_Institute_ID = a.InstituteID  WHERE a.YearlyCoursePlanID = $id")->row();
        // echo '<pre>';print_r($data['viewdetails']);exit;
        // echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('othersInfo/yearlyCourse/view', $data);
    }

}

/* End of file yearlyCourse.php */
/* Location: ./application/controllers/regularTransaction/yearlyCourse.php */