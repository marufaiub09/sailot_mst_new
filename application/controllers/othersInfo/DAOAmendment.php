<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DAOAmendment extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access reazul Islam <reazul@atilimited.net>
     * @param  none
     * @return  //Load All Rank Details From Rank Table
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'DAO Amendments';
        $data['daoAmendment'] = $this->db->query("SELECT d.DAOAmendID, s.OFFICIALNUMBER, pdao.DAO_NO PRE_DAO, adao.DAO_NO AMEND_DAO  , date_format(d.AuthorityDate, '%d-%m-%Y') AuthorityDate, d.AuthorityNumber
                                                  FROM   daoamend d
                                                  JOIN sailor s on s.SAILORID = d.SailorID
                                                  JOIN bn_dao pdao on pdao.DAO_ID = d.PreviousDAOID
                                                  JOIN bn_dao adao on adao.DAO_ID = d.DAOAmendID")->result();
        $data['content_view_page'] = 'othersInfo/DAOAmendment/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <Emran@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", "DESC");
        $data['zone'] = $this->utilities->dropdownFromTableWithCondition('bn_navyadminhierarchy', ' Select Zone ', 'ADMIN_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1, "ADMIN_TYPE =" => '1'));
        $data['area'] = $this->utilities->dropdownFromTableWithCondition('bn_navyadminhierarchy', ' Select area ', 'ADMIN_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1, "ADMIN_TYPE =" => '2'));
        $data['establishment'] = $this->utilities->dropdownFromTableWithCondition('bn_ship_establishment', ' Select Shift ', 'SHIP_ESTABLISHMENTID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'othersInfo/DAOAmendment/create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran <emran@atilimited.net>
     */
    public function save() {
        $SailorID = $this->input->post('SAILOR_ID', true);
        $currentDao = $this->input->post('currentDao', true);
        $str = explode("_", $currentDao);
        $currentDaoId = $str[0];
        $currentDaoNumber = $str[1];
        $previousDAO = $this->input->post('previousDAO', true);
        $str = explode("_", $previousDAO);
        $previousDaoId = $str[0];
        $previousDaoNumber = $str[1];
        $zone = $this->input->post('zone', true);
        $area = $this->input->post('area', true);
        $shiftEstablishment = $this->input->post('shiftEstablishment', true);
        $postingUnit = $this->input->post('postingUnit', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $amendTest = $this->input->post('amendTest', true);

        $daoAmendment = array(
            'SailorID' => $SailorID,
            'PreviousDAOID' => $previousDaoId,
            'PreviousDAONumber' => $previousDaoNumber,
            'CurrentDAOID' => $currentDaoId,
            'CurrentDAONumber' => $currentDaoNumber,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'AmendText' => $amendTest,
            'CRE_BY' => $this->user_session["USER_ID"],
            'ShipID' => $postingUnit
        );
        if ($this->utilities->insertData($daoAmendment, 'daoamend')) { // if examtran inserted successfully            
            echo "<div class='alert alert-success'>DAO Amendment Inserted successfully</div>";
        } else {
            echo "<div class='alert alert-success'>DAO Amendment Inserted Failed</div>";
        }
    }

    /* @access      public
     * @param       id
     * @author      Emran Hossne <Emran@atilimited.net>
     * @return      templete
     */

    public function edit($id) {
//        echo "<pre>";
//    print_r($id);
//    exit();
        $data['result'] = $this->db->query("SELECT d.DAOAmendID,
                                            s.OFFICIALNUMBER,
                                            s.FULLNAME,
                                            br.RANK_NAME,
                                            pu.NAME as POSTING_UNIT_NAME,
                                            se.NAME SHIP_EST,
                                            jo.NAME zone,
                                            jo.ADMIN_ID,
                                            se.NAME area,
                                            se.AREA_ID,
                                            se.SHIP_ESTABLISHMENTID,
                                            pu.POSTING_UNITID,
                                            d.SailorID,
                                            d.PreviousDAOID,
                                            d.PreviousDAONumber,
                                            d.CurrentDAOID,
                                            d.CurrentDAONumber,
                                            d.AuthorityNumber,
                                            date_format(d.AuthorityDate,'%d-%m-%Y') AuthorityDate,
                                            d.AmendText,
                                            d.ACTIVE_STATUS,
                                            d.CRE_BY,
                                            d.CRE_DT,
                                           d.UPD_BY,
                                           d.UPD_DT,
                                            d.ShipID
                                       FROM daoamend d
                                      INNER JOIN sailor s on s.SAILORID = d.SailorID
                                      INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                      LEFT JOIN bn_posting_unit pu on pu.POSTING_UNITID = d.ShipID
                                      LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID
                                      LEFT JOIN bn_navyadminhierarchy jo on se.ZONE_ID = jo.ADMIN_ID
                                      where d.DAOAmendID = $id")->row();
        $data['ac_type'] = "edit";
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['zone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 1));
        $data['area'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['establishment'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['PostingUnit'] = $this->utilities->findAllByAttribute("bn_posting_unit", array("ACTIVE_STATUS" => 1));
        //$data['establishment'] = $this->utilities->dropdownFromTableWithCondition('bn_ship_establishment', ' Select Shift ', 'SHIP_ESTABLISHMENTID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'othersInfo/DAOAmendment/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access  reazul Islam <reazul@atilimited.net>
     * @param  all movement info
     * @return  custom message
     */

    public function update() {
      //  echo "<pre>";
     // print_r($_POST); exit();
        $id = $this->input->post('id', true);
//        $SailorID = $this->input->post('officialNo', true);
        $currentDao = $this->input->post('currentDao', true);
        $str = explode("_", $currentDao);     		
        $currentDaoId = $str[0];
        $currentDaoNumber = $str[1];
        $previousDAO = $this->input->post('previousDAO', true);
        $str = explode("_", $previousDAO);
        $previousDaoId = $str[0];
        $previousDaoNumber = $str[1];
        $shiftEstablishment = $this->input->post('shiftEstablishment', true);
        $postingUnit = $this->input->post('postingUnit', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $amendTest = $this->input->post('amendTest', true);
        $daoAmend = array(
//            'SailorID' => $SailorID,
            'PreviousDAOID' => $previousDaoId,
            'PreviousDAONumber' => $previousDaoNumber,
            'CurrentDAOID' => $currentDaoId,
            'CurrentDAONumber' => $currentDaoNumber,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'AmendText' => $amendTest,
            'ShipID' => $postingUnit,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
// echo "<pre>";
//       print_r($daoAmend); exit();
        if ($this->utilities->updateData('daoamend', $daoAmend, array("DAOAmendID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>DAO Amendment Update successfully</div>";
        } else { // if data inserted failed
            echo "<div class='alert alert-danger'>DAO Amendment Update failed</div>";
        }
    }

    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT d.DAOAmendID,
                                            s.OFFICIALNUMBER,
                                            s.FULLNAME,
                                            br.RANK_NAME,
                                            pu.NAME as POSTING_UNIT_NAME,
                                            se.NAME SHIP_EST,
                                            jo.NAME zone,
                                            jo.ADMIN_ID,
                                            se.NAME area,
                                            se.AREA_ID,
                                            se.SHIP_ESTABLISHMENTID,
                                            pu.POSTING_UNITID,
                                            d.SailorID,
                                            d.PreviousDAOID,
                                            d.PreviousDAONumber,
                                            d.CurrentDAOID,
                                            d.CurrentDAONumber,
                                            d.AuthorityNumber,
                                            date_format(d.AuthorityDate,'%d-%m-%Y') AuthorityDate,
                                            d.AmendText,
                                            d.ACTIVE_STATUS,
                                            d.CRE_BY,
                                            d.CRE_DT,
                                           d.UPD_BY,
                                           d.UPD_DT,
                                            d.ShipID
                                       FROM daoamend d
                                      INNER JOIN sailor s on s.SAILORID = d.SailorID
                                      INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                      LEFT JOIN bn_posting_unit pu on pu.POSTING_UNITID = d.ShipID
                                      LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = pu.SHIP_ESTABLISHMENTID
                                      LEFT JOIN bn_navyadminhierarchy jo on se.ZONE_ID = jo.ADMIN_ID
                                      where d.DAOAmendID = $id")->row();
        // echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('othersInfo/DAOAmendment/view', $data);
    }

}