<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CapturingNewRecrutiInfo extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return 
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Recruitments';
        $data['batch'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', '-- Select Batch --', 'BATCH_ID', 'BATCH_NUMBER');
        $data['content_view_page'] = 'SpecialTransection/CapturingNewRecrutiInfo';
        $this->template->display($data);
    }

    public function sailorUpdate() {
        $sailor_id = $this->uri->segment(4);
        $batchNumber = $this->input->post('batch', true);
        $localNumber = $this->input->post('localNumber', true);
        $rank = $this->input->post('RankName', true);
        $str = explode("_", $rank);
        $rankId = $str[0];
//        $branchId = $str[1];
//        $equivalantRankId = $str[2];
        $birthDate = date('Y-m-d', strtotime($this->input->post('birthDate', true)));
        $fullName = $this->input->post('fullName', true);
        $fullNameInBn = $this->input->post('fullNameInBn', true);
        $shortName = $this->input->post('shortName', true);
        $shortNameBangla = $this->input->post('shortNameBangla', true);
        $nickName = $this->input->post('nickName', true);
        $nickNameInBangla = $this->input->post('nickNameInBangla', true);
        $fatherName = $this->input->post('fatherName', true);
        $fatherNameInBangls = $this->input->post('fatherNameInBangls', true);
        $motherName = $this->input->post('motherName', true);
        $motherNameInBangla = $this->input->post('motherNameInBangla', true);
        $mariatialStatus = $this->input->post('mariatialStatus', true);
        $religion = $this->input->post('religion', true);
        $fredomfighterStatus = $this->input->post('fredomfighterStatus', true);
        $entryDate = date('Y-m-d', strtotime($this->input->post('entryDate', true)));
        $entrytype = $this->input->post('entryType', true);
        $expireDate = $this->input->post('expireDate', true);
        $divisionOfSailor = $this->input->post('division', true);
        $districtOfSailor = $this->input->post('districtName', true);
        $thanaOfSailor = $this->input->post('policeStation', true);
        $zone = $this->input->post('zone', true);
        $area = $this->input->post('area', true);
        $shiftEstablishment = $this->input->post('shiftEstablishment', true);
        $postingUnit = $this->input->post('postingUnit', true);
        $postingType = $this->input->post('postingType', true);
        $postingDate = $this->input->post('postingDate', true);
        $permanentAddress = $this->input->post('permanentAddress', true);
        $permanentAddressInBan = $this->input->post('permanentAddressInBan', true);
        $nextOfKinInAddress = $this->input->post('nextOfKinAddress');
        $nextOfKinAddressInBangla = $this->input->post('nextOfKinAddressInBangla');
        $height = $this->input->post('cmName', true);
        $weight = $this->input->post('kgWeight', true);
        $chest = $this->input->post('cmChest', true);
        $eyeColor = $this->input->post('eyeColor', true);
        $eyeSight = $this->input->post('eyeSight', true);
        $hairColor = $this->input->post('hairColor', true);
        $bloodGroup = $this->input->post('bloodGroup', true);
        $complexion = $this->input->post('complexion', true);
        $sex = $this->input->post('sex', true);
        $idMarks = $this->input->post('idMarks', true);
        $priodYY = $this->input->post('priodYY', true);
        $priodMM = $this->input->post('priodMM', true);
        $priodDD = $this->input->post('priodDD', true);

        /* Address Information */
        $nextOfKinName = $this->input->post('nextOfKinName', true);
        $nextOfKinNameInBan = $this->input->post('nextOfKinNameInBan', true);
        $relationOfNextKin = $this->input->post('relationOfNextKin', true);

        /* Next of kin Address */
        $ifNaxtOfKinSame = $this->input->post('ifNextOfKinSame', true);
        $divisionOfNextKin = $this->input->post('divisionOfNextKin', true);
        $districtNameOfKing = $this->input->post('districtNameOfKin', true);
        $thanaNameOnKin = $this->input->post('thanaNameOnKin', true);

        /* Academic Information */
       // $academicid = $this->input->post('pracademic', true);
        $examSystem = $this->input->post('examSystem', true);
        $pracademic = $this->input->post('pracademic', true);
        $examName = $this->input->post('examNameDesc', true);
        $subject = $this->input->post('subjectGroupDesc', true);
        $passingYear = $this->input->post('passingYear', true);
        $boardUniversity = $this->input->post('baordUniversityDesc', true);
        $result = $this->input->post('result', true);
        $gpaName = $this->input->post('gpa', true);
        $percentage = $this->input->post('percent');
        $resultDescription = $this->input->post('resultdesc', true);
        $subjectId = $this->input->post('subjectGroupDesc', true);
        $educationStatus = $this->input->post('eduStatusDesc', true);
        $traditional = $this->input->post('traditional', true);
        //var_dump(count($examSystem));
        /* Physical Information */

        $ftName = $this->input->post('ftName', true);
        $incName = $this->input->post('incName', true);
        $cmName = $this->input->post('cmName', true);
        $kgWeight = $this->input->post('kgWeight', true);
        $lbWeight = $this->input->post('lbWeight', true);

        $acaSailor = $this->db->query("SELECT AcademicID FROM academic")->result();
        $newRequireInfo = array(
//            'b' => (($ifNaxtOfKinSame == 1) ? $batchNumber : $localNumber),
            'BATCHNUMBER' => $batchNumber,
            'LOCALNUMBER' => $localNumber,
            'RANKID' => $rankId,
//            'BRANCHID' => $branchId,
//            'EQUIVALANTRANKID' => $equivalantRankId,
            'BIRTHDATE' => $birthDate,
            'ENTRYTYPEID' => $entrytype,
            'FULLNAME' => $fullName,
            'FULLNAMEINBANGLA' => $fullNameInBn,
            'SHORTNAME' => $shortName,
            'SHORTNAMEINBANGLA' => $shortNameBangla,
            'NICKNAME' => $nickName,
            'NICKNAMEINBANGLA' => $nickNameInBangla,
            'NEXTOFKIN' => $nextOfKinName,
            'NEXTOFKININBANGLA' => $nextOfKinNameInBan,
            'RELATIONID' => $relationOfNextKin,
            'FATHERNAME' => $fatherName,
            'FATHERNAMEINBANGLA' => $fatherNameInBangls,
            'motherName' => $motherName,
            'MOTHERNAMEINBANGLA' => $motherNameInBangla,
            'MARITALSTATUS' => $mariatialStatus,
            'RELIGION' => $religion,
            'FREEDOMFIGHTERSTATUS' => $fredomfighterStatus,
            'ENTRYDATE' => $entryDate,
            'EXPIRYDATE' => $expireDate,
            'ZONEID' => $zone,
            'AREAID' => $area,
            'DIVISIONID' => $divisionOfSailor,
            'DISTRICTID' => $districtOfSailor,
            'THANAID' => $thanaOfSailor,
            'PERMANENTADDRESS' => $permanentAddress,
            'PERMANENTADDRESSINBANGLA' => $permanentAddressInBan,
            'SHIPESTABLISHMENTID' => $shiftEstablishment,
            'POSTINGUNITID' => $postingUnit,
            'POSTINGTYPE' => $postingType,
            'POSTINGDATE' => $postingDate,
            'NEXTOFKINADDRESS' => $nextOfKinInAddress,
            'NEXTOFKINADDRESSINBANGLA' => $nextOfKinAddressInBangla,
            'HEIGHT' => $height,
            'WEIGHT' => $weight,
            'CHEST' => $chest,
            'HAIRCOLOR' => $hairColor,
            'EYECOLOR' => $eyeColor,
            'EYESIGHT' => $eyeSight,
            'COMPLEXION' => $complexion,
            'BLOODGROUP' => $bloodGroup,
            'APPOINTMENTTYPEID' => 'entrytype',
            'SEX' => $sex,
            'IDMARKS' => $idMarks
        );
//      var_dump($newRequireInfo);
//     exit();
        if ($this->utilities->updateData('sailor', $newRequireInfo, array("SAILORID" => $sailor_id))) {

            $engagement = array(
//                'SailorID' => $sailorId,
                'EntryTypeID' => $entrytype,
                'EngagementDate' => $entryDate,
                'ExpiryDate' => $expireDate,
                'ShipEstablishmentID' => $shiftEstablishment,
                'EngagementYear' => $priodYY,
                'EngagementMonth' => $priodMM,
                'EngagementDay' => $priodDD
            );

            $success = 0;
            if ($this->utilities->updateData('engagement', $engagement, array("EagagementID" => $sailor_id))) {

<input type="text" value="<?php echo $result1->Exam_System ?>" name="examSystem[]" id="examSystem"  class="form-control" placeholder="Exam System">
                                                <input type="hidden" value="<?php echo $result1->EvaluationSystem ?>" name="examSystemTable[]" id="examSystem"  class="form-control" placeholder="Exam System">
                                                <input type="text" value="<?php echo $result1->Subject ?>" name="subjectGroupDesc[]" id="subjectGroupDesc"  class="form-control " placeholder="Subject">
                                                <input type="hidden" value="<?php echo $result1->SubjectID ?>" name="subjectGroupDescTable[]" id="subjectGroupDesc"  class="form-control " placeholder="Subject">


                /* end Education insert part */
                for ($i = 0; $i < count($pracademic); $i++) {
                    foreach ($pracademic as $key) {
                        var_dump($key);
                        $acaSailor = $this->db->query("SELECT AcademicID FROM academic")->result();
                        // var_dump($acaSailor);
                        if (in_array($key, $acaSailor)) {
                            echo 'paa';
                            $academicInf = array(
                                'SailorID' => $sailor_id,
                                'ExamID' => $examName[$i],
                                'EvaluationSystem' => $examSystem[$i],
                                'BoardID' => $boardUniversity[$i],
                                'SubjectID' => $subjectId[$i],
                                'PassingYear' => $passingYear[$i],
                                'ExamGradeID' => $result[$i],
                                'TotalMarks' => $traditional[$i],
                                'Percentage' => $percentage[$i],
                                'ResultDescription' => $resultDescription[$i],
                                'EducationStatus' => $educationStatus[$i],
                                'UPD_BY' => $this->user_session["USER_ID"],
                                'UPD_DT' => date("Y-m-d h:i:s a")
                            );
                            if ($this->utilities->updateData('academic', $academicInf, array("AcademicID" => $sailor_id, "AcademicID" => $key))) {

                                ;
                            } else {
                                $academicInf = array(
                                    'SailorID' => $sailor_id,
                                    'ExamID' => $examName[$i],
                                    'EvaluationSystem' => $examSystem[$i],
                                    'BoardID' => $boardUniversity[$i],
                                    'SubjectID' => $subjectId[$i],
                                    'PassingYear' => $passingYear[$i],
                                    'ExamGradeID' => $result[$i],
                                    'TotalMarks' => $traditional[$i],
                                    'Percentage' => $percentage[$i],
                                    'ResultDescription' => $resultDescription[$i],
                                    'EducationStatus' => $educationStatus[$i],
                                    'UPD_BY' => $this->user_session["USER_ID"],
                                    'UPD_DT' => date("Y-m-d h:i:s a")
                                );
                                if ($this->utilities->insertData($academicInf, 'academic'))
                                /* end Academic insert part */
                                    $success = 1;
                            }
                            if ($success == 1) {
                                echo "<div class='alert alert-success'>New Requirement Update successfully</div>";
                            } else {
                                echo "<div class='alert alert-danger'>New Requirement  Update Failed</div>";
                            }
                        }
                    }
                }
            }
        }
    }

}

