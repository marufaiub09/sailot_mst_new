<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CapturingNewRecrutiInfo extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return 
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Recruitments';
        $data['batch'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', '-- Select Batch --', 'BATCH_ID', 'BATCH_NUMBER');
        $data['content_view_page'] = 'SpecialTransection/CapturingNewRecrutiInfo';
        $this->template->display($data);
    }

//emran

    public function create() {
        $data['localNumber'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', '-- Select Local Number --', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 2));
        $data['batch'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', '-- Select Batch --', 'BATCH_ID', 'BATCH_NUMBER');
        $data['entryType'] = $this->utilities->dropdownFromTableWithCondition('bn_entrytype', '-- Select Type --', 'ENTRY_TYPEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data ['relation'] = $this->utilities->dropdownFromTableWithCondition('bn_relation', '--Select Relation--', 'RELATION_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['postingUnit'] = $this->utilities->dropdownFromTableWithCondition('bn_posting_unit', '-- Select Type --', 'POSTING_UNITID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['division'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Division ', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 1));
        $data['exam_name'] = $this->utilities->dropdownFromTableWithCondition('bn_govtexam_hierarchy', 'Select Exam Name', 'CODE', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['subject_group'] = $this->utilities->dropdownFromTableWithCondition('bn_subject_group', 'Select Group Name', 'SUBJECT_GROUPID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['board_university'] = $this->utilities->dropdownFromTableWithCondition('bn_boardcenter', 'Select Bard/University', 'BOARD_CENTERID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['result'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', 'Select Result', 'EXAM_GRADEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['district'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Division ', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 2));
        $data['thana'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Thana ', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 3));
        $data['zone'] = $this->utilities->dropdownFromTableWithCondition('bn_navyadminhierarchy', ' Select Zone ', 'ADMIN_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1, "ADMIN_TYPE =" => '1'));
        $data['area'] = $this->utilities->dropdownFromTableWithCondition('bn_navyadminhierarchy', ' Select area ', 'ADMIN_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1, "ADMIN_TYPE =" => '2'));
        $data['establishment'] = $this->utilities->dropdownFromTableWithCondition('bn_ship_establishment', ' Select Shift ', 'SHIP_ESTABLISHMENTID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['rankData'] = $this->utilities->findAllByAttribute("bn_rank", array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'SpecialTransection/create';
        $this->template->display($data);
    }

//emran

    public function ajax_get_area() {
        $zone_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_navyadminhierarchy', array('PARENT_ID' => $zone_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->ADMIN_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }

//emran

    public function feetconvert() {
        $feetvalue = $_POST['feetValue'];
        $inchvalue = $_POST['inchValue'];
        $feetVal = ($feetvalue * 30.48);
        $inchVal = ($inchvalue * 2.54);
        $totalInch = ($feetVal + $inchVal);
        echo $totalInch;
    }

//emran

    public function kgconvert() {
        $kgval = $_POST['kgValue'];
        $convlb = ($kgval / 0.45359237 );
        echo number_format((float) $convlb, 2, '.', '');
    }

//emran

    public function chestconvert() {
        $incChestVal = $_POST['incChestValue'];
        $convCm = ($incChestVal * 2.54);
        echo $convCm;
    }

//emran

    public function ajax_get_local_number() {
        $localNumber = $_POST['selectedValue'];

        $returnVal = $_POST['selectedValue'];

        echo $returnVal;
    }

//emran

    public function ajax_get_NextOfKinAddress() {
        $nextOfKinDiv = $_POST['selectDistVal'];
        $nextOfKinthana = $_POST['selectThanaVal'];
        $concateData = "District:" . " " . $nextOfKinDiv . " " . "Thana:" . " " . $nextOfKinthana;
        echo $concateData;
    }

//emran
    public function NextOfKinAddressInBangla() {
        $nextOfKinDiv = $_POST['selectDistVal'];
        $nextOfKinthana = $_POST['selectThanaVal'];
        ;
        $districtInBangla = $this->db->query("select BANGLA_NAME FROM bn_bdadminhierarchy WHERE BD_ADMINID = $nextOfKinDiv")->row();
        $thanaInBangla = $this->db->query("select BANGLA_NAME FROM bn_bdadminhierarchy WHERE BD_ADMINID = $nextOfKinthana")->row();
        $concateData = "District:" . " " . $districtInBangla->BANGLA_NAME . " " . "Thana:" . " " . $thanaInBangla->BANGLA_NAME;
        echo $concateData;
    }

//emran

    public function ajax_get_NextOfKinAddressNotSame() {
        $nextOfKinDiv = $_POST['selectDistVal'];
        $nextOfKinthana = $_POST['selectThanaVal'];
        $concateData = "District:" . " " . $nextOfKinDiv . " " . "Thana:" . " " . $nextOfKinthana;
        echo $concateData;
    }

//emran

    public function ajax_get_NextOfKinAddressInBangla() {
        $nextOfKinDiv = $_POST['selectDistVal'];
        $nextOfKinthana = $_POST['selectThanaVal'];
        $districtInBangla = $this->db->query("select BANGLA_NAME FROM bn_bdadminhierarchy WHERE BD_ADMINID = $nextOfKinDiv")->row();
        $thanaInBangla = $this->db->query("select BANGLA_NAME FROM bn_bdadminhierarchy WHERE BD_ADMINID = $nextOfKinthana")->row();
        $concateData = "District:" . " " . $districtInBangla->BANGLA_NAME . " " . "Thana:" . " " . $thanaInBangla->BANGLA_NAME;
        echo $concateData;
    }

//emran

    public function ajax_get_shift() {
        $area_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_ship_establishment', array('AREA_ID' => $area_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->SHIP_ESTABLISHMENTID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }

//emran

    public function ajax_get_postingUnit() {
        $posting_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_posting_unit', array('SHIP_ESTABLISHMENTID' => $posting_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->POSTING_UNITID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }

//emran

    public function ajax_get_org() {
        $org_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_organization_hierarchy', array('PARENT_ID' => $org_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->ORG_ID . '">' . $row->ORG_NAME . '</option>';
            }
        }
        echo $returnVal;
    }

//emran


    public function getSelectededucation() {
        $examName = $this->input->post("examName");
        $subject = $this->input->post("subject");
        echo '<tr>
        <td class="text-center">' . $examName . '<input type="hidden" name="examName[]" class="examName" value="' . $examName . '" /></td>
        <td class="text-center">' . $subject . '</td>
        
        <td class="text-center"><span class="removeProduct btn btn-danger  btn-xs">X</span></td>
    </tr>';
    }

    public function getEduData() {
        $examSystem = $this->input->post("examSystem");
        $examNameDesc = $this->input->post("examNameDesc");
        $subjectGroupDesc = $this->input->post("subjectGroupDesc");
        $passingYear = $this->input->post("passingYear");
        $baordUniversityDesc = $this->input->post("baordUniversityDesc");
        $result = $this->input->post("result");
        $resultdesc = $this->input->post("resultDesc");
        $eduStatusDesc = $this->input->post("eduStatusDesc");
        $percent = $this->input->post("percent");
        $traditional = $this->input->post("traditional");
        $gpa = $this->input->post("gpa");
       

        echo '<tr>
        <td class="text-center">' . $examSystem . '<input type="hidden" name="examSystem[]" class="examSystem" value="' . $examSystem . '" /></td>
        <td class="text-center">' . $examNameDesc . '<input type="hidden" name="examNameDesc[]" class="eximId" value="' . $examNameDesc . '" /></td>
        <td class="text-center">' . $subjectGroupDesc . '<input type="hidden" name="subjectGroupDesc[]" class="subjectGroupDesc" value="' . $subjectGroupDesc . '" /></td>
        <td class="text-center">' . $passingYear . '<input type="hidden" name="passingYear[]" class="passingYear" value="' . $passingYear . '" /></td>
        <td class="text-center">' . $baordUniversityDesc . '<input type="hidden" name="baordUniversityDesc[]" class="baordUniversityDesc" value="' . $baordUniversityDesc . '" /></td>
        <td class="text-center">' . $result . '<input type="hidden" name="result[]" class="result" value="' . $result . '" /></td>
        <td class="text-center">' . $gpa . '<input type="hidden" name="gpa[]" class="gpa" value="' . $gpa . '" /></td>
        <td class="text-center">' . $traditional . '<input type="hidden" name="traditional[]" class="traditional" value="' . $traditional . '" /></td>
        <td class="text-center">' . $percent . '<input type="hidden" name="percent[]" class="percent" value="' . $percent . '" /></td>
        <td class="text-center">' . $resultdesc . '<input type="hidden" name="resultdesc[]" class="resultdesc" value="' . $resultdesc . '" /></td>
        <td class="text-center">' . $eduStatusDesc . '<input type="hidden" name="eduStatusDesc[]" class="eduStatusDesc" value="' . $eduStatusDesc . '" /></td>
        <td class="text-center"><span class="removeEdu btn btn-danger  btn-xs">X</span></td>
    </tr>';
    }

//emran


    public function save() {
       /*echo "<pre>";
       print_r($_POST);
       exit();*/
        /* start New Rquirement Info part */

        $batchNumber = $this->input->post('batch', true);
        $localNumber = $this->input->post('localNumber', true);
        $rank = $this->input->post('RankName', true);
        $str = explode("_", $rank);
        $rankId = $str[0];
        $branchId = $str[1];
        $equivalantRankId = $str[2];
        $birthDate = date('Y-m-d', strtotime($this->input->post('birthDate', true)));
        $fullName = $this->input->post('fullName', true);
        $fullNameInBn = $this->input->post('fullNameInBn', true);
        $shortName = $this->input->post('shortName', true);
        $shortNameBangla = $this->input->post('shortNameBangla', true);
        $nickName = $this->input->post('nickName', true);
        $nickNameInBangla = $this->input->post('nickNameInBangla', true);
        $fatherName = $this->input->post('fatherName', true);
        $fatherNameInBangls = $this->input->post('fatherNameInBangls', true);
        $motherName = $this->input->post('motherName', true);
        $motherNameInBangla = $this->input->post('motherNameInBangla', true);
        $mariatialStatus = $this->input->post('mariatialStatus', true);
        $religion = $this->input->post('religion', true);
        $fredomfighterStatus = $this->input->post('fredomfighterStatus', true);
        $entryDate = date('Y-m-d', strtotime($this->input->post('entryDate', true)));
        $entrytype = $this->input->post('entryType', true);
        $expireDate = $this->input->post('expireDate', true);
        $divisionOfSailor = $this->input->post('division', true);
        $districtOfSailor = $this->input->post('districtName', true);
        $thanaOfSailor = $this->input->post('policeStation', true);
        $zone = $this->input->post('zone', true);
        $area = $this->input->post('area', true);
        $shiftEstablishment = $this->input->post('shiftEstablishment', true);
        $postingUnit = $this->input->post('postingUnit', true);
        $postingType = $this->input->post('postingType', true);
        $postingDate = $this->input->post('postingDate', true);
        $permanentAddress = $this->input->post('permanentAddress', true);
        $permanentAddressInBan = $this->input->post('permanentAddressInBan', true);
        $nextOfKinInAddress = $this->input->post('nextOfKinAddress');
        $nextOfKinAddressInBangla = $this->input->post('nextOfKinAddressInBangla');
        $height = $this->input->post('cmName', true);
        $weight = $this->input->post('kgWeight', true);
        $chest = $this->input->post('cmChest', true);
        $eyeColor = $this->input->post('eyeColor', true);
        $eyeSight = $this->input->post('eyeSight', true);
        $hairColor = $this->input->post('hairColor', true);
        $bloodGroup = $this->input->post('bloodGroup', true);
        $complexion = $this->input->post('complexion', true);
        $sex = $this->input->post('sex', true);
        $idMarks = $this->input->post('idMarks', true);
        $priodYY = $this->input->post('priodYY', true);
        $priodMM = $this->input->post('priodMM', true);
        $priodDD = $this->input->post('priodDD', true);

        /* Address Information */
        $nextOfKinName = $this->input->post('nextOfKinName', true);
        $nextOfKinNameInBan = $this->input->post('nextOfKinNameInBan', true);
        $relationOfNextKin = $this->input->post('relationOfNextKin', true);

        /* Next of kin Address */
        $ifNaxtOfKinSame = $this->input->post('ifNextOfKinSame', true);
        $divisionOfNextKin = $this->input->post('divisionOfNextKin', true);
        $districtNameOfKing = $this->input->post('districtNameOfKin', true);
        $thanaNameOnKin = $this->input->post('thanaNameOnKin', true);

        /* Academic Information */

        
        $examSystem = $this->input->post('examSystem', true);
        $examName = $this->input->post('examNameDesc', true);
        $subject = $this->input->post('subjectGroupDesc', true);
        $passingYear = $this->input->post('passingYear', true);
        $boardUniversity = $this->input->post('baordUniversityDesc', true);
        $result = $this->input->post('result', true);
        $gpaName = $this->input->post('gpa', true);
        $percentage = $this->input->post('percent');
        $resultDescription = $this->input->post('resultdesc', true);
        $subjectId = $this->input->post('subjectGroupDesc', true);
        $educationStatus = $this->input->post('eduStatusDesc', true);
        $traditional = $this->input->post('traditionalName', true);

        /* Physical Information */

        $ftName = $this->input->post('ftName', true);
        $incName = $this->input->post('incName', true);
        $cmName = $this->input->post('cmName', true);
        $kgWeight = $this->input->post('kgWeight', true);
        $lbWeight = $this->input->post('lbWeight', true);


        $newRequireInfo = array(
//          'b'=> (($ifNaxtOfKinSame == 1) ? $batchNumber:$localNumber),
            'BATCHNUMBER' => $batchNumber,
            'LOCALNUMBER' => $localNumber,
            'RANKID' => $rankId,
            'BRANCHID' => $branchId,
            'EQUIVALANTRANKID' => $equivalantRankId,
            'BIRTHDATE' => $birthDate,
            'ENTRYTYPEID' => $entrytype,
            'FULLNAME' => $fullName,
            'FULLNAMEINBANGLA' => $fullNameInBn,
            'SHORTNAME' => $shortName,
            'SHORTNAMEINBANGLA' => $shortNameBangla,
            'NICKNAME' => $nickName,
            'NICKNAMEINBANGLA' => $nickNameInBangla,
            'NEXTOFKIN' => $nextOfKinName,
            'NEXTOFKININBANGLA' => $nextOfKinNameInBan,
            'RELATIONID' => $relationOfNextKin,
            'FATHERNAME' => $fatherName,
            'FATHERNAMEINBANGLA' => $fatherNameInBangls,
            'motherName' => $motherName,
            'MOTHERNAMEINBANGLA' => $motherNameInBangla,
            'MARITALSTATUS' => $mariatialStatus,
            'RELIGION' => $religion,
            'FREEDOMFIGHTERSTATUS' => $fredomfighterStatus,
            'ENTRYDATE' => $entryDate,
            'EXPIRYDATE' => $expireDate,
            'ZONEID' => $zone,
            'AREAID' => $area,
            'DIVISIONID' => $divisionOfSailor,
            'DISTRICTID' => $districtOfSailor,
            'THANAID' => $thanaOfSailor,
            'PERMANENTADDRESS' => $permanentAddress,
            'PERMANENTADDRESSINBANGLA' => $permanentAddressInBan,
            'SHIPESTABLISHMENTID' => $shiftEstablishment,
            'POSTINGUNITID' => $postingUnit,
            'POSTINGTYPE' => $postingType,
            'POSTINGDATE' => $postingDate,
            'NEXTOFKINADDRESS' => $nextOfKinInAddress,
            'NEXTOFKINADDRESSINBANGLA' => $nextOfKinAddressInBangla,
            'HEIGHT' => $height,
            'WEIGHT' => $weight,
            'CHEST' => $chest,
            'HAIRCOLOR' => $hairColor,
            'EYECOLOR' => $eyeColor,
            'EYESIGHT' => $eyeSight,
            'COMPLEXION' => $complexion,
            'BLOODGROUP' => $bloodGroup,
            'APPOINTMENTTYPEID' => 'entrytype',
            'SEX' => $sex,
            'IDMARKS' => $idMarks
        );
        if ($sailorId = $this->utilities->insert('sailor', $newRequireInfo)) {
            $engagement = array(
                'SailorID' => $sailorId,
                'EntryTypeID' => $entrytype,
                'EngagementDate' => $entryDate,
                'ExpiryDate' => $expireDate,
                'ShipEstablishmentID' => $shiftEstablishment,
                'EngagementYear' => $priodYY,
                'EngagementMonth' => $priodMM,
                'EngagementDay' => $priodDD
//'POSTINGTYPE' => $postingType,
            );
            $success = 0;
            if ($academic = $this->utilities->insert('engagement', $engagement)) { // if New Require inserted successfully
                /* end Education insert part */
                if($traditional == '')
                {
                    $traditional = 0;
                }

                for ($i = 0; $i < count($batchNumber); $i++) {
                    $academicInf = array(
                        'SailorID' => $sailorId,
                        'ExamID' => $examName[$i],
                        'EvaluationSystem' => $examSystem[$i],
                        'BoardID' => $boardUniversity[$i],
                        'SubjectID' => $subjectId[$i],
                        'PassingYear' => $passingYear[$i],
                        'ExamGradeID' => $result[$i],
                        'TotalMarks' => $traditional[$i],
                        'Percentage' => $percentage[$i],
                        'ResultDescription' => $resultDescription[$i],
                        'EducationStatus' => $educationStatus[$i],
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    if ($courseTranId = $this->utilities->insertData($academicInf, 'academic')) {
                        /* end Academic insert part */
                        $success = 1;
                    }
                }
                if ($success == 1) {
                    echo "<div class='alert alert-success'>New Requirement Inserted successfully</div>";
                } else {
                    echo "<div class='alert alert-danger'>New Requirement Inserted successfully</div>";
                }
            }
        }
    }

//emran

    function ajaxSailorList() {
// storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
// datatable column index  => database column name
            0 => 'sailor.OFFICIALNUMBER', 1 => 'sailor.LOCALNUMBER', 2 => 'sailor.FULLNAME', 3 => 'sailor.SHORTNAME', 4 => 'r.RANK_NAME', 5 => 'e.NAME', 6 => 'sailor.BIRTHDATE');

// getting total number records without any search

        $query = $this->db->query("SELECT
                                  sailor.SAILORID
                                 ,sailor.OFFICIALNUMBER
                                 ,sailor.LOCALNUMBER
                                 ,sailor.FULLNAME
                                 ,sailor.SHORTNAME
                                 ,r.RANK_NAME AS RANKID
                                 ,e.NAME AS ENTRYTYPEID
                                 ,sailor.ENGAGEMENTPERIOD
                                 ,DATE_FORMAT(
                                    sailor.BIRTHDATE
                                   ,'%d-%m-%Y')
                                    BIRTHDATE
                                 ,sailor.ENTRYDATE
                                 ,sailor.FATHERNAME
                                 ,sailor.MOTHERNAME
                                 ,bd.NAME AS DISTRICTID
                                FROM
                                  sailor
                                  LEFT  JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
                                  LEFT  JOIN bn_entrytype e ON sailor.ENTRYTYPEID = e.ENTRY_TYPEID
                                  LEFT  JOIN bn_bdadminhierarchy bd ON sailor.DISTRICTID = bd.BD_ADMINID
                                                ")->num_rows();


        $totalData = $query;

        $totalFiltered = $totalData;
// when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

// if there is a search parameter

            $query = $this->db->query("SELECT
                                      sailor.SAILORID
                                     ,sailor.OFFICIALNUMBER
                                     ,sailor.LOCALNUMBER
                                     ,sailor.FULLNAME
                                     ,sailor.SHORTNAME
                                     ,r.RANK_NAME AS RANKID
                                     ,e.NAME AS ENTRYTYPEID
                                     ,sailor.ENGAGEMENTPERIOD
                                     ,DATE_FORMAT(
                                        sailor.BIRTHDATE
                                       ,'%d-%m-%Y')
                                        BIRTHDATE
                                     ,sailor.ENTRYDATE
                                     ,sailor.FATHERNAME
                                     ,sailor.MOTHERNAME
                                     ,bd.NAME AS DISTRICTID
                                    FROM
                                      sailor
                                      LEFT  JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
                                      LEFT  JOIN bn_entrytype e ON sailor.ENTRYTYPEID = e.ENTRY_TYPEID
                                      LEFT  JOIN bn_bdadminhierarchy bd ON sailor.DISTRICTID = bd.BD_ADMINID
                                    WHERE sailor.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR sailor.LOCALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR sailor.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR sailor.SHORTNAME LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
// when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {
            $query = $this->db->query("SELECT  sailor.SAILORID
                                     ,sailor.OFFICIALNUMBER
                                     ,sailor.LOCALNUMBER
                                     ,sailor.FULLNAME
                                     ,sailor.SHORTNAME
                                     ,r.RANK_NAME AS RANKID
                                     ,e.NAME AS ENTRYTYPEID
                                     ,sailor.ENGAGEMENTPERIOD
                                     ,DATE_FORMAT(
                                        sailor.BIRTHDATE
                                       ,'%d-%m-%Y')
                                        BIRTHDATE
                                     ,sailor.ENTRYDATE
                                     ,sailor.FATHERNAME
                                     ,sailor.MOTHERNAME
                                     ,bd.NAME AS DISTRICTID
                                    FROM
                                      sailor
                                      LEFT  JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
                                      LEFT  JOIN bn_entrytype e ON sailor.ENTRYTYPEID = e.ENTRY_TYPEID
                                      LEFT  JOIN bn_bdadminhierarchy bd ON sailor.DISTRICTID = bd.BD_ADMINID
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
//$gender = array('0' => 'Male', '1' => 'Female', '2' => 'Others');
        foreach ($query as $row) {
// preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->LOCALNUMBER;
            $nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->SHORTNAME;
            $nestedData[] = $row->RANKID;
            $nestedData[] = date('Y-m-d', strtotime($row->BIRTHDATE));
            $nestedData[] = '<a class="btn btn-warning btn-xs" href="' . site_url('SpecialTransection/CapturingNewRecrutiInfo/edit/' . $row->SAILORID) . '" title="Edit Sailor Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ';

            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

// total data array
        );

        echo json_encode($json_data);
    }

//emran

    public function searchbramch() {
        $branchID = $this->input->post("batch", true);
        $data['searchSailor'] = $this->db->query("SELECT  sailor.SAILORID,
                                                sailor.OFFICIALNUMBER,
                                                sailor.LOCALNUMBER,
                                                sailor.FULLNAME,
                                                sailor.SHORTNAME,
                                                r.RANK_NAME as RANKID,
                                                e.NAME as ENTRYTYPEID,
                                                sailor.ENGAGEMENTPERIOD,
                                                DATE_FORMAT(sailor.BIRTHDATE, '%d-%m-%Y') BIRTHDATE,
                                                sailor.ENTRYDATE,
                                                sailor.FATHERNAME,
                                                sailor.MOTHERNAME,
                                                bd.NAME as DISTRICTID,
                                                gh.name,
                                                a.TotalMarks,
                                                su.NAME SubjectID,
                                                a.ResultDescription                                                
                                                FROM sailor
                                                LEFT OUTER JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
                                                LEFT OUTER JOIN bn_entrytype e ON sailor.ENTRYTYPEID = e.ENTRY_TYPEID
                                                LEFT OUTER JOIN bn_bdadminhierarchy bd ON sailor.DISTRICTID = bd.BD_ADMINID
                                                LEFT OUTER JOIN academic a ON sailor.SAILORID = a.SailorID
                                                LEFT OUTER JOIN bn_govtexam_hierarchy gh ON a.ExamID = gh.GOVT_EXAMID
                                                LEFT OUTER JOIN bn_subject_group su ON a.SubjectID = su.SUBJECT_GROUPID
                                                where sailor.BATCHNUMBER = $branchID")->result();
        $this->load->view('SpecialTransection/branchWiseList', $data);
    }

//emran

    public function edit($SAILORID) {

        $data['result'] = $this->db->query("SELECT s.SAILORID,s.OFFICIALNUMBER,s.LOCALNUMBER,r.RANK_NAME AS RANK_ID,s.RANKID AS R,DATE_FORMAT(s.BIRTHDATE, '%d-%m-%Y') BIRTHDATE,
                               s.FULLNAME,s.FULLNAMEINBANGLA,s.SHORTNAME,s.SHORTNAMEINBANGLA,s.NICKNAME,s.NICKNAMEINBANGLA,s.FATHERNAME,s.ENTRYTYPEID,
                               s.FATHERNAMEINBANGLA,s.MOTHERNAME,s.MOTHERNAMEINBANGLA,s.PERMANENTADDRESS,s.PERMANENTADDRESSINBANGLA,s.NEXTOFKINADDRESS,
                               s.NEXTOFKINADDRESSINBANGLA,s.NEXTOFKIN,s.NEXTOFKININBANGLA,s.POSTINGDATE,s.MARITALSTATUS,(SELECT CASE s.MARITALSTATUS
                                          WHEN 0 THEN 'Unmarried'
                                          WHEN 1 THEN 'Married'
                                          WHEN 2 THEN 'Separation'
                                          WHEN 3 THEN 'Divorce'
                                          WHEN 4 THEN 'Widower'
                                          ELSE NULL
                                       END) AS MARITAL_STATUS,
                                       DATE_FORMAT(s.POSTINGDATE,'%d-%m-%Y') PostDate,
                                       s.POSTINGTYPE,
                                       s.BATCHNUMBER as Batch,
                                       s.FREEDOMFIGHTERSTATUS,
                                       (SELECT CASE s.FREEDOMFIGHTERSTATUS
                                          WHEN 0 THEN 'None'
                                          WHEN 1 THEN 'Freedom Fighter'
                                          WHEN 2 THEN 'Freedom Fighter Child'
                                          ELSE NULL
                                       END) AS FredomFighter,s.RELIGION,
                               (SELECT CASE s.RELIGION
                                          WHEN 1 THEN 'Islam'
                                          WHEN 2 THEN 'Hindu'
                                          WHEN 3 THEN 'Buddhist'
                                          WHEN 4 THEN 'Christian'
                                          WHEN 5 THEN 'Others'
                                          ELSE NULL
                                       END) AS RELI_GION,
                               DATE_FORMAT(s.ENTRYDATE, '%d-%m-%Y') AS ENTRYDATE1,
                               DATE_FORMAT(s.EXPIRYDATE, '%d-%m-%Y') AS EXPIRYDATE,
                               e.NAME AS ENTRYTYPE_ID,
                               (SELECT nv.NAME
                                  FROM bn_navyadminhierarchy nv
                                 WHERE s.ZONEID = nv.ADMIN_ID) AS ZONE_ID,
                               (SELECT nv.NAME
                                  FROM bn_navyadminhierarchy nv
                                 WHERE s.AREAID = nv.ADMIN_ID)
                                  AS AREA_ID,s.POSTINGDATE,
                                  s.POSTINGTYPE,
                               (SELECT CASE s.POSTINGTYPE
                                          WHEN 1 THEN 'Normal'
                                          WHEN 2 THEN 'Instructor'
                                          WHEN 3 THEN 'COURSE'
                                          WHEN 4 THEN 'School Staff'
                                          WHEN 5 THEN 'Release'
                                          WHEN 6 THEN 'Deputation'
                                          ELSE NULL
                                       END) AS POSTING_TYPE,
                               sh.NAME AS SHIPESTABLISHMENT_ID,pu.NAME AS POSTINGUNIT_ID,s.POSTINGUNITID,bd.NAME AS DIVISION,
                               (SELECT bd.NAME
                                  FROM bn_bdadminhierarchy bd
                                 WHERE bd.BD_ADMINID = s.DIVISIONID)
                                  AS DIVISION,
                                  s.DIVISIONID AS D_ID,
                               (SELECT bd.NAME
                                  FROM bn_bdadminhierarchy bd
                                 WHERE bd.BD_ADMINID = s.DISTRICTID)
                                  AS DISTRICT,
                                  s.DISTRICTID AS Dis_ID,
                               (SELECT bd.NAME
                                  FROM bn_bdadminhierarchy bd
                                 WHERE bd.BD_ADMINID = s.THANAID)
                                  AS POLICESTATION,
                                  s.THANAID AS TH_ID,
                                rn.BATCH_NUMBER AS Batch,
                               (SELECT re.NAME
                                  FROM bn_relation re
                                 WHERE re.RELATION_ID = s.RELATIONID)
                                  AS RELATION_ID,
                                  s.RELATIONID AS RE_ID,
                               (SELECT CASE a.EvaluationSystem
                                          WHEN 1 THEN 'Traditional'
                                          WHEN 2 THEN 'GPA'
                                          ELSE NULL
                                       END) AS ExamSystem,gh.NAME AS ExamName,gh.GOVT_EXAMID AS G_EX_ID,su.NAME SubjectName,su.SUBJECT_GROUPID AS SubGrp_ID,a.PassingYear,
                               bo.NAME AS BoardName,bo.BOARD_CENTERID AS BoardID, a.ResultDescription,a.Percentage,s.AREAID,bne.EXAM_GRADEID AS EXGRADEID, bne.NAME AS EXAMGRADENAME,
                               (SELECT CASE a.EducationStatus
                                          WHEN 1 THEN 'Entry Education'
                                          WHEN 2 THEN 'Entry Period Highest'
                                          ELSE NULL
                                       END) AS EducationStatus,
                               s.HEIGHT,s.WEIGHT,s.CHEST,s.EYECOLOR,s.EYESIGHT,s.HAIRCOLOR,s.BLOODGROUP,s.COMPLEXION,s.SEX,s.ZONEID,s.SHIPESTABLISHMENTID,
                               s.IDMARKS,s.SHORTNAME,r.RANK_NAME AS RANKID,s.ENGAGEMENTPERIOD,
                               DATE_FORMAT(s.BIRTHDATE, '%d-%m-%Y') BIRTHDATE,s.ENTRYDATE,s.FATHERNAME,s.MOTHERNAME,
                               bd.NAME AS DISTRICTID,a.TotalMarks,su.NAME SubjectID,a.ResultDescription,s.ACTIVE_STATUS
                          FROM sailor s
                               LEFT OUTER JOIN bn_batchnumber rn ON s.BATCHNUMBER = rn.BATCH_ID
                               LEFT OUTER JOIN bn_rank r ON s.RANKID = r.RANK_ID
                               
                               LEFT OUTER JOIN bn_entrytype e ON s.ENTRYTYPEID = e.ENTRY_TYPEID
                               LEFT OUTER JOIN bn_bdadminhierarchy bd ON s.DISTRICTID = bd.BD_ADMINID
                               LEFT OUTER JOIN academic a ON s.SAILORID = a.SailorID
                               LEFT OUTER JOIN bn_govtexam_hierarchy gh ON a.ExamID = gh.GOVT_EXAMID
                               LEFT OUTER JOIN bn_examgrade bne ON a.ExamGradeID = bne.EXAM_GRADEID
                               LEFT OUTER JOIN bn_subject_group su ON a.SubjectID = su.SUBJECT_GROUPID
                               LEFT OUTER JOIN bn_navyadminhierarchy nv ON s.ZONEID = nv.ADMIN_ID
                               LEFT OUTER JOIN bn_ship_establishment sh ON s.SHIPESTABLISHMENTID = sh.SHIP_ESTABLISHMENTID
                               LEFT OUTER JOIN bn_posting_unit pu ON s.POSTINGUNITID = pu.POSTING_UNITID
                               LEFT OUTER JOIN bn_relation re ON re.RELATION_ID = s.RELATIONID
                               LEFT OUTER JOIN bn_boardcenter bo ON a.BoardID = bo.BOARD_CENTERID
                               where a.SAILORID = $SAILORID")->row();
        //echo "<pre>";print_r( $data['result']);exit;

        $data['academicInfo'] = $this->db->query("SELECT 
                                 academic.ExamID,
                                (SELECT e.name
                                    FROM bn_govtexam_hierarchy e
                                    WHERE e.GOVT_EXAMID = academic.ExamID)
                                    AS Exam_Name,

                            (SELECT b.NAME
                                    FROM bn_boardcenter b
                                    WHERE b.BOARD_CENTERID = academic.BoardID)
                                    AS Board_Name,
                                    academic.BoardID,
                            (SELECT sg.NAME
                                    FROM bn_subject_group sg
                                    WHERE sg.SUBJECT_GROUPID = academic.SubjectID)
                                    Subject,
                                    academic.SubjectID,
                            PassingYear,
                            EvaluationSystem,
                            (SELECT CASE EvaluationSystem
                                                    WHEN 1 THEN 'Traditional'
                                                    WHEN 2 THEN 'GPA'
                                                    ELSE NULL
                                            END)
                                    AS Exam_System,
                            (SELECT ex.NAME
                                    FROM bn_examgrade ex
                                    WHERE ex.EXAM_GRADEID = academic.ExamGradeID)
                                    AS Exam_Gred,
                                    academic.ExamGradeID,
                                    academic.EducationStatus AS EDUST,
                            ResultDescription,
                            TotalMarks,
                            Percentage,
                                            (SELECT CASE EducationStatus
                                                    WHEN 1 THEN 'Entry Education'
                                                    WHEN 2 THEN 'Entry Period Highest'
                                                    when 3 then 'Last'
                                                    ELSE NULL
                                            END)
                                    AS EducationStatus, 
    academic.AcademicID     
                    FROM academic 
                    WHERE SailorID = $SAILORID")->result();
//        $data['result3'] = $this->db->query("SELECT nh.* FROM academic nh")->row();
//       // echo"<pre>";print_r( $data['result3']->AcademicID);exit;
//        if( $data['result3']->AcademicID == ''){
//            echo 'insert';
//        }else{
//            echo 'update';
//        }

        $data['passingyear'] = $this->utilities->dropdownFromTableWithCondition('academic', ' Select Passing Year', 'AcademicID', 'PassingYear', $condition = array("ACTIVE_STATUS" => 1));


        $data['result12'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', 'Select Result', 'EXAM_GRADEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['localNumber'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Local Number', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 2));

        $data['batch'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', 'Select Batch', 'BATCH_ID', 'BATCH_NUMBER');

        /* $data['entryType'] = $this->utilities->dropdownFromTableWithCondition('bn_entrytype', 'Select Type', '
          ENTRY_TYPEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1)); */

        $data ['relation'] = $this->utilities->dropdownFromTableWithCondition('bn_relation', 'Select Relation', 'RELATION_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));

        $data['postingUnit'] = $this->utilities->dropdownFromTableWithCondition('bn_posting_unit', 'Select Type', 'POSTING_UNITID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));


        $data['exam_name'] = $this->utilities->dropdownFromTableWithCondition('bn_govtexam_hierarchy', 'Select Exam Name', 'GOVT_EXAMID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));

        $data['subject_group'] = $this->utilities->dropdownFromTableWithCondition('bn_subject_group', 'Select Group Name', 'SUBJECT_GROUPID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));

        $data['board_university'] = $this->utilities->dropdownFromTableWithCondition('bn_boardcenter', 'Select Bard/University', 'BOARD_CENTERID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));

        $data['ExamGrade'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', 'Select Result', 'EXAM_GRADEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));

        $data['district'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Division ', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 2));
        $data['division1'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', 'Select Division', 'BD_ADMINID', 'NAME', $condition = array("PARENT_ID" => 0));

        $data['thana'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Thana ', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 3));

        $data['area'] = $this->utilities->findAllByAttribute('bn_navyadminhierarchy', array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => '2'));
        $data['dis'] = $this->utilities->findAllByAttribute('bn_bdadminhierarchy', array("ACTIVE_STATUS" => 1, "BDADMIN_TYPE" => '2'));


        $data['establishment'] = $this->utilities->dropdownFromTableWithCondition('bn_ship_establishment', ' Select Shift ', 'SHIP_ESTABLISHMENTID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));

        $data['rankData'] = $this->utilities->findAllByAttribute("bn_rank", array("ACTIVE_STATUS" => 1));
//    echo"<pre>";print_r($data['rankData']);exit;

        $data['entryType1'] = $this->utilities->findAllByAttribute('bn_entrytype', array("ACTIVE_STATUS" => 1));

        $data['zone'] = $this->utilities->findAllByAttribute('bn_navyadminhierarchy', array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => '1'));

        $data['division'] = $this->utilities->findAllByAttribute('bn_bdadminhierarchy', array("ACTIVE_STATUS" => 1));

        $data ['relation1'] = $this->utilities->findAllByAttribute('bn_relation', array("ACTIVE_STATUS" => 1));
        $data['exam_name1'] = $this->utilities->findAllByAttribute('bn_govtexam_hierarchy', array("ACTIVE_STATUS" => 1));
        $data['subject_group1'] = $this->utilities->findAllByAttribute('bn_subject_group', array("ACTIVE_STATUS" => 1));
        $data['board_university1'] = $this->utilities->findAllByAttribute('bn_boardcenter', array("ACTIVE_STATUS" => 1));
        $data['results1'] = $this->utilities->findAllByAttribute('bn_examgrade', array("ACTIVE_STATUS" => 1));
        $data['thanapolice'] = $this->utilities->findAllByAttribute('bn_bdadminhierarchy', array("BDADMIN_TYPE" => 3, "ACTIVE_STATUS" => 1));
        //echo "<pre>";print_r( $data['thanapolice']);exit;
        $data['content_view_page'] = 'SpecialTransection/edit';
        $this->template->display($data);
    }


   public function sailorPreview() {
       $data['breadcrumbs'] = array(
           'Modules' => '#'
       );
       $data['pageTitle'] = 'Sailor Recruitments';

       $data['content_view_page'] = 'SpecialTransection/sailorPreview';
       $this->template->display($data);
   }
     public function getEduDataTable() {
        $examSystem = $this->input->post("examSystem");
        $examSystem_name =$this->input->post("examSystem_name");
        $examNameDesc = $this->input->post("examNameDesc");
        $examNameDesc_name =$this->input->post("examNameDesc_name");
        $subjectGroupDesc = $this->input->post("subjectGroupDesc");
        $subjectGroupDesc_name = $this->input->post("subjectGroupDesc_name");
        $passingYear = $this->input->post("passingYear");
        $baordUniversityDesc = $this->input->post("baordUniversityDesc");
        $baordUniversityDesc_name = $this->input->post("baordUniversityDesc_name");
        $result = $this->input->post("result");
        $result_n = $this->input->post("result_n");
        $resultdesc = $this->input->post("resultDesc");
        $eduStatusDesc = $this->input->post("eduStatusDesc");
        $eduStatusDesc_name = $this->input->post("eduStatusDesc_name");
        $percent = $this->input->post("percent");
        $traditional = $this->input->post("traditional");
        $gpa = $this->input->post("gpa");
        $gpa_name = $this->input->post("gpa_name");

/*" $gpa . '<input type="hidden" name="gpa[]" class="gpa" value="' . $gpa . '" */
        echo '<tr>
        <td class="text-center">' . $examSystem_name . '<input type="hidden" name="examSystemTable[]" class="examSystem" value="' . $examSystem . '" /></td>
        <td class="text-center">' . $examNameDesc_name . '<input type="hidden" name="examNameDescTable[]" class="eximId" value="' . $examNameDesc . '" /></td>
        <td class="text-center">' . $subjectGroupDesc_name . '<input type="hidden" name="subjectGroupDescTable[]" class="subjectGroupDesc" value="' . $subjectGroupDesc . '" /></td>
        <td class="text-center">' . $passingYear . '<input type="hidden" name="passingYearTable[]" class="passingYear" value="' . $passingYear . '" /></td>
        <td class="text-center">' . $baordUniversityDesc_name . '<input type="hidden" name="baordUniversityDescTable[]" class="baordUniversityDesc" value="' . $baordUniversityDesc . '" /></td>
        <td class="text-center">' . $result_n . '<input type="hidden" name="resultTable[]" class="result" value="' . $result .'"/></td>
        <td class="text-center">' . $traditional . '<input type="hidden" name="traditionalTable[]" class="traditional" value="' . $traditional . '" /></td>
        <td class="text-center">' . $percent . '<input type="hidden" name="percentTable[]" class="percent" value="' . $percent . '" /></td>
        <td class="text-center">' . $resultdesc . '<input type="hidden" name="resultdescTable[]" class="resultdesc" value="' . $resultdesc . '" /></td>
        <td class="text-center">' . $eduStatusDesc_name . '<input type="hidden" name="eduStatusDescTable[]" class="eduStatusDesc" value="' . $eduStatusDesc . '" /></td>
        <td class="text-center"><span class="removeEdu btn btn-danger  btn-xs">X</span></td>
    </tr>';
    }
    public function sailorUpdate() {
        $sailor_id = $this->uri->segment(4);
        $batchNumber = $this->input->post('batch', true);
        $localNumber = $this->input->post('localNumber', true);
        $rank = $this->input->post('RankName', true);
        $str = explode("_", $rank);
        $rankId = $str[0];
//        $branchId = $str[1];
//        $equivalantRankId = $str[2];
        $birthDate = date('Y-m-d', strtotime($this->input->post('birthDate', true)));
        $fullName = $this->input->post('fullName', true);
        $fullNameInBn = $this->input->post('fullNameInBn', true);
        $shortName = $this->input->post('shortName', true);
        $shortNameBangla = $this->input->post('shortNameBangla', true);
        $nickName = $this->input->post('nickName', true);
        $nickNameInBangla = $this->input->post('nickNameInBangla', true);
        $fatherName = $this->input->post('fatherName', true);
        $fatherNameInBangls = $this->input->post('fatherNameInBangls', true);
        $motherName = $this->input->post('motherName', true);
        $motherNameInBangla = $this->input->post('motherNameInBangla', true);
        $mariatialStatus = $this->input->post('mariatialStatus', true);
        $religion = $this->input->post('religion', true);
        $fredomfighterStatus = $this->input->post('fredomfighterStatus', true);
        $entryDate = date('Y-m-d', strtotime($this->input->post('entryDate', true)));
        $entrytype = $this->input->post('entryType', true);
        $expireDate = date('Y-m-d', strtotime($this->input->post('expireDate', true)));
        $divisionOfSailor = $this->input->post('division', true);
        $districtOfSailor = $this->input->post('districtName', true);
        $thanaOfSailor = $this->input->post('policeStation', true);
        $zone = $this->input->post('zone', true);
        $area = $this->input->post('area', true);
        $shiftEstablishment = $this->input->post('shiftEstablishment', true);
        $postingUnit = $this->input->post('postingUnit', true);
        $postingType = $this->input->post('postingType', true);
        $postingDate =  date('Y-m-d', strtotime($this->input->post('postingDate', true)));
        $permanentAddress = $this->input->post('permanentAddress', true);
        $permanentAddressInBan = $this->input->post('permanentAddressInBan', true);
        $nextOfKinInAddress = $this->input->post('nextOfKinAddress');
        $nextOfKinAddressInBangla = $this->input->post('nextOfKinAddressInBangla');
        $height = $this->input->post('cmName', true);
        $weight = $this->input->post('kgWeight', true);
        $chest = $this->input->post('cmChest', true);
        $eyeColor = $this->input->post('eyeColor', true);
        $eyeSight = $this->input->post('eyeSight', true);
        $hairColor = $this->input->post('hairColor', true);
        $bloodGroup = $this->input->post('bloodGroup', true);
        $complexion = $this->input->post('complexion', true);
        $sex = $this->input->post('sex', true);
        $idMarks = $this->input->post('idMarks', true);
        $priodYY = $this->input->post('priodYY', true);
        $priodMM = $this->input->post('priodMM', true);
        $priodDD = $this->input->post('priodDD', true);

        /* Address Information */
        $nextOfKinName = $this->input->post('nextOfKinName', true);
        $nextOfKinNameInBan = $this->input->post('nextOfKinNameInBan', true);
        $relationOfNextKin = $this->input->post('relationOfNextKin', true);

        /* Next of kin Address */
        $ifNaxtOfKinSame = $this->input->post('ifNextOfKinSame', true);
        $divisionOfNextKin = $this->input->post('divisionOfNextKin', true);
        $districtNameOfKing = $this->input->post('districtNameOfKin', true);
        $thanaNameOnKin = $this->input->post('thanaNameOnKin', true);

        /* Academic Information */
       // $academicid = $this->input->post('pracademic', true);
        $examSystem = $this->input->post('examSystemTable', true);
        $pracademic = $this->input->post('pracademicTable', true);
        //var_dump($pracademic);
        $examName = $this->input->post('examNameDescTable', true);
        $subject = $this->input->post('subjectGroupDescTable', true);
        $passingYear = $this->input->post('passingYearTable', true);
        $boardUniversity = $this->input->post('baordUniversityDescTable', true);
        $result = $this->input->post('resultTable', true);
        $gpaName = $this->input->post('gpaTable', true);
        $percentage = $this->input->post('percentTable');
        $resultDescription = $this->input->post('resultdescTable', true);
        $subjectId = $this->input->post('subjectGroupDescTable', true);
        $educationStatus = $this->input->post('eduStatusDescTable', true);
        $traditional = $this->input->post('traditionalTable', true);
        //var_dump(count($examSystem));
        /* Physical Information */

        $ftName = $this->input->post('ftName', true);
        $incName = $this->input->post('incName', true);
        $cmName = $this->input->post('cmName', true);
        $kgWeight = $this->input->post('kgWeight', true);
        $lbWeight = $this->input->post('lbWeight', true);
        
        
        $acaSailor = $this->db->query("SELECT AcademicID FROM academic")->result();
        $newRequireInfo = array(
//            'b' => (($ifNaxtOfKinSame == 1) ? $batchNumber : $localNumber),
            'BATCHNUMBER' => $batchNumber,
            'LOCALNUMBER' => $localNumber,
            'RANKID' => $rankId,
//            'BRANCHID' => $branchId,
//            'EQUIVALANTRANKID' => $equivalantRankId,
            'BIRTHDATE' => $birthDate,
            'ENTRYTYPEID' => $entrytype,
            'FULLNAME' => $fullName,
            'FULLNAMEINBANGLA' => $fullNameInBn,
            'SHORTNAME' => $shortName,
            'SHORTNAMEINBANGLA' => $shortNameBangla,
            'NICKNAME' => $nickName,
            'NICKNAMEINBANGLA' => $nickNameInBangla,
            'NEXTOFKIN' => $nextOfKinName,
            'NEXTOFKININBANGLA' => $nextOfKinNameInBan,
            'RELATIONID' => $relationOfNextKin,
            'FATHERNAME' => $fatherName,
            'FATHERNAMEINBANGLA' => $fatherNameInBangls,
            'motherName' => $motherName,
            'MOTHERNAMEINBANGLA' => $motherNameInBangla,
            'MARITALSTATUS' => $mariatialStatus,
            'RELIGION' => $religion,
            'FREEDOMFIGHTERSTATUS' => $fredomfighterStatus,
            'ENTRYDATE' => $entryDate,
            'EXPIRYDATE' => $expireDate,
            'ZONEID' => $zone,
            'AREAID' => $area,
            'DIVISIONID' => $divisionOfSailor,
            'DISTRICTID' => $districtOfSailor,
            'THANAID' => $thanaOfSailor,
            'PERMANENTADDRESS' => $permanentAddress,
            'PERMANENTADDRESSINBANGLA' => $permanentAddressInBan,
            'SHIPESTABLISHMENTID' => $shiftEstablishment,
            'POSTINGUNITID' => $postingUnit,
            'POSTINGTYPE' => $postingType,
            'POSTINGDATE' => $postingDate,
            'NEXTOFKINADDRESS' => $nextOfKinInAddress,
            'NEXTOFKINADDRESSINBANGLA' => $nextOfKinAddressInBangla,
            'HEIGHT' => $height,
            'WEIGHT' => $weight,
            'CHEST' => $chest,
            'HAIRCOLOR' => $hairColor,
            'EYECOLOR' => $eyeColor,
            'EYESIGHT' => $eyeSight,
            'COMPLEXION' => $complexion,
            'BLOODGROUP' => $bloodGroup,
            'APPOINTMENTTYPEID' => 'entrytype',
            'SEX' => $sex,
            'IDMARKS' => $idMarks
        );
//      var_dump($newRequireInfo);
//     exit();
        if ($this->utilities->updateData('sailor', $newRequireInfo, array("SAILORID" => $sailor_id))) {

            $engagement = array(
//                'SailorID' => $sailorId,
                'EntryTypeID' => $entrytype,
                'EngagementDate' => $entryDate,
                'ExpiryDate' => $expireDate,
                'ShipEstablishmentID' => $shiftEstablishment,
                'EngagementYear' => $priodYY,
                'EngagementMonth' => $priodMM,
                'EngagementDay' => $priodDD
            );

            $success = 0;
            if ($this->utilities->updateData('engagement', $engagement, array("EagagementID" => $sailor_id))) {
        /*var_dump($pracademic);
        var_dump($examName);
        var_dump($examSystem);
        var_dump($boardUniversity);
        var_dump($subjectId);
        var_dump($passingYear);
        var_dump($result);
        var_dump($traditional);
        var_dump($percentage);
        var_dump($resultDescription);
        var_dump($educationStatus);
        var_dump($gpaName);*/
                /* end Education insert part */
                for ($i = 0; $i < count($examName); $i++) {
                    // var_dump($acaSailor);
                        if ($i >= count($pracademic)) 
                        {
                          //  var_dump("expression");
        
                            $academicInf = array(
                                'SailorID' => $sailor_id,
                                'ExamID' => $examName[$i],
                                'EvaluationSystem' => $examSystem[$i],
                                'BoardID' => $boardUniversity[$i],
                                'SubjectID' => $subjectId[$i],
                                'PassingYear' => $passingYear[$i],
                                'ExamGradeID' => $result[$i],
                                'TotalMarks' => $traditional[$i],
                                'Percentage' => $percentage[$i],
                                'ResultDescription' => $resultDescription[$i],
                                'EducationStatus' => $educationStatus[$i],
                                'UPD_BY' => $this->user_session["USER_ID"],
                                'UPD_DT' => date("Y-m-d h:i:s a")
                            );
                            //var_dump($academicInf);
                            $this->utilities->insertData($academicInf, 'academic'); 
                        }
                        else 
                        {
                            //var_dump("asas");
                            $academicInf = array(
                                'SailorID' => $sailor_id,
                                'ExamID' => $examName[$i],
                                'EvaluationSystem' => $examSystem[$i],
                                'BoardID' => $boardUniversity[$i],
                                'SubjectID' => $subjectId[$i],
                                'PassingYear' => $passingYear[$i],
                                'ExamGradeID' => $result[$i],
                                'TotalMarks' => $traditional[$i],
                                'Percentage' => $percentage[$i],
                                'ResultDescription' => $resultDescription[$i],
                                'EducationStatus' => $educationStatus[$i],
                                'UPD_BY' => $this->user_session["USER_ID"],
                                'UPD_DT' => date("Y-m-d h:i:s a")
                                );
                                 if ($abc = $this->utilities->updateData('academic', $academicInf, array("AcademicID" => $pracademic[$i]))) 
                                //var_dump($abc);
                                /* end Academic insert part */
                                    $success = 1;
                        }
                            if ($success == 1) {
                                echo "<div class='alert alert-success'>New Requirement Update successfully</div>";
                            } else {
                                echo "<div class='alert alert-danger'>New Requirement  Update Failed</div>";
                            }
                        
                    
                }
            }
        }
    }
}
