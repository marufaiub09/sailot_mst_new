<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class sailorPreview extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Recruitments';
        $data['content_view_page'] = 'SpecialTransection/sailorPreview';
        $this->template->display($data);
    }

    public function sailorDetails() {
        $officialNumber = $_POST['officialNumber'];

        $sql = $this->db->query("SELECT * FROM sailor WHERE OFFICIALNUMBER = $officialNumber")->row();

        $sailor_id = $sql->SAILORID;
        //echo $sailor_id;
        //exit();

        $result = $this->db->query("SELECT s.SAILORID,s.OFFICIALNUMBER,s.LOCALNUMBER,r.RANK_NAME AS RANK_ID,s.RANKID AS R,DATE_FORMAT(s.BIRTHDATE, '%d-%m-%Y') BIRTHDATE,
                               s.FULLNAME,s.FULLNAMEINBANGLA,s.SHORTNAME,s.SHORTNAMEINBANGLA,s.NICKNAME,s.NICKNAMEINBANGLA,s.FATHERNAME,s.ENTRYTYPEID,
                               s.FATHERNAMEINBANGLA,s.MOTHERNAME,s.MOTHERNAMEINBANGLA,s.PERMANENTADDRESS,s.PERMANENTADDRESSINBANGLA,s.NEXTOFKINADDRESS,
                               s.NEXTOFKINADDRESSINBANGLA,s.NEXTOFKIN,s.NEXTOFKININBANGLA,s.POSTINGDATE,s.MARITALSTATUS,(SELECT CASE s.MARITALSTATUS
                                          WHEN 0 THEN 'Unmarried'
                                          WHEN 1 THEN 'Married'
                                          WHEN 2 THEN 'Separation'
                                          WHEN 3 THEN 'Divorce'
                                          WHEN 4 THEN 'Widower'
                                          ELSE NULL
                                       END) AS MARITAL_STATUS,
                                       DATE_FORMAT(s.POSTINGDATE,'%d-%m-%Y') PostDate,
                                       s.POSTINGTYPE,
                                       s.BATCHNUMBER as Batch,
                                       s.FREEDOMFIGHTERSTATUS,
                                       (SELECT CASE s.FREEDOMFIGHTERSTATUS
                                          WHEN 0 THEN 'None'
                                          WHEN 1 THEN 'Freedom Fighter'
                                          WHEN 2 THEN 'Freedom Fighter Child'
                                          ELSE NULL
                                       END) AS FredomFighter,s.RELIGION,
                               (SELECT CASE s.RELIGION
                                          WHEN 1 THEN 'Islam'
                                          WHEN 2 THEN 'Hindu'
                                          WHEN 3 THEN 'Buddhist'
                                          WHEN 4 THEN 'Christian'
                                          WHEN 5 THEN 'Others'
                                          ELSE NULL
                                       END) AS RELI_GION,
                               DATE_FORMAT(s.ENTRYDATE, '%d-%m-%Y') AS ENTRYDATE1,
                               DATE_FORMAT(s.EXPIRYDATE, '%d-%m-%Y') AS EXPIRYDATE,
                               e.NAME AS ENTRYTYPE_ID,
                               (SELECT nv.NAME
                                  FROM bn_navyadminhierarchy nv
                                 WHERE s.ZONEID = nv.ADMIN_ID) AS ZONE_ID,
                               (SELECT nv.NAME
                                  FROM bn_navyadminhierarchy nv
                                 WHERE s.AREAID = nv.ADMIN_ID)
                                  AS AREA_ID,DATE_FORMAT(s.POSTINGDATE, '%d-%m-%Y') AS POSTING_DATE,
                                  s.POSTINGTYPE,
                               (SELECT CASE s.POSTINGTYPE
                                          WHEN 1 THEN 'Normal'
                                          WHEN 2 THEN 'Instructor'
                                          WHEN 3 THEN 'COURSE'
                                          WHEN 4 THEN 'School Staff'
                                          WHEN 5 THEN 'Release'
                                          WHEN 6 THEN 'Deputation'
                                          ELSE NULL
                                       END) AS POSTING_TYPE,
                               sh.NAME AS SHIPESTABLISHMENT_ID,pu.NAME AS POSTINGUNIT_ID,s.POSTINGUNITID,bd.NAME AS DIVISION_NAME,
                               (SELECT bd.NAME
                                  FROM bn_bdadminhierarchy bd
                                 WHERE bd.BD_ADMINID = s.DIVISIONID)
                                  AS DIVISION,
                                  s.DIVISIONID AS D_ID,
                               (SELECT bd.NAME
                                  FROM bn_bdadminhierarchy bd
                                 WHERE bd.BD_ADMINID = s.DISTRICTID)
                                  AS DISTRICT,
                                  s.DISTRICTID AS Dis_ID,
                               (SELECT bd.NAME
                                  FROM bn_bdadminhierarchy bd
                                 WHERE bd.BD_ADMINID = s.THANAID)
                                  AS POLICESTATION,
                                  s.THANAID AS TH_ID,
                                rn.BATCH_NUMBER AS Batch,
                               (SELECT re.NAME
                                  FROM bn_relation re
                                 WHERE re.RELATION_ID = s.RELATIONID)
                                  AS RELATION_ID,
                                  s.RELATIONID AS RE_ID,
                               (SELECT CASE a.EvaluationSystem
                                          WHEN 1 THEN 'Traditional'
                                          WHEN 2 THEN 'GPA'
                                          ELSE NULL
                                       END) AS ExamSystem,gh.NAME AS ExamName,gh.GOVT_EXAMID AS G_EX_ID,su.NAME SubjectName,su.SUBJECT_GROUPID AS SubGrp_ID,a.PassingYear,
                               bo.NAME AS BoardName,bo.BOARD_CENTERID AS BoardID, a.ResultDescription,a.Percentage,s.AREAID,bne.EXAM_GRADEID AS EXGRADEID, bne.NAME AS EXAMGRADENAME,
                               (SELECT CASE a.EducationStatus
                                          WHEN 1 THEN 'Entry Education'
                                          WHEN 2 THEN 'Entry Period Highest'
                                          ELSE NULL
                                       END) AS EducationStatus,
                               s.HEIGHT,s.WEIGHT,s.CHEST,s.EYECOLOR,s.EYESIGHT,s.HAIRCOLOR,s.BLOODGROUP,s.COMPLEXION,s.SEX,s.ZONEID,s.SHIPESTABLISHMENTID,
                               s.IDMARKS,s.SHORTNAME,r.RANK_NAME AS RANKID,s.ENGAGEMENTPERIOD,
                               DATE_FORMAT(s.BIRTHDATE, '%d-%m-%Y') BIRTHDATE,s.ENTRYDATE,s.FATHERNAME,s.MOTHERNAME,
                               bd.NAME AS DISTRICTID,a.TotalMarks,su.NAME SubjectID,a.ResultDescription,s.ACTIVE_STATUS
                          FROM sailor s
                               LEFT OUTER JOIN bn_batchnumber rn ON s.BATCHNUMBER = rn.BATCH_ID
                               LEFT OUTER JOIN bn_rank r ON s.RANKID = r.RANK_ID
                               
                               LEFT OUTER JOIN bn_entrytype e ON s.ENTRYTYPEID = e.ENTRY_TYPEID
                               LEFT OUTER JOIN bn_bdadminhierarchy bd ON s.DISTRICTID = bd.BD_ADMINID
                               LEFT OUTER JOIN academic a ON s.SAILORID = a.SailorID
                               LEFT OUTER JOIN bn_govtexam_hierarchy gh ON a.ExamID = gh.GOVT_EXAMID
                               LEFT OUTER JOIN bn_examgrade bne ON a.ExamGradeID = bne.EXAM_GRADEID
                               LEFT OUTER JOIN bn_subject_group su ON a.SubjectID = su.SUBJECT_GROUPID
                               LEFT OUTER JOIN bn_navyadminhierarchy nv ON s.ZONEID = nv.ADMIN_ID
                               LEFT OUTER JOIN bn_ship_establishment sh ON s.SHIPESTABLISHMENTID = sh.SHIP_ESTABLISHMENTID
                               LEFT OUTER JOIN bn_posting_unit pu ON s.POSTINGUNITID = pu.POSTING_UNITID
                               LEFT OUTER JOIN bn_relation re ON re.RELATION_ID = s.RELATIONID
                               LEFT OUTER JOIN bn_boardcenter bo ON a.BoardID = bo.BOARD_CENTERID
                               where s.OFFICIALNUMBER = $officialNumber")->row();
        $academicInfo = $this->db->query("SELECT 
                                 academic.ExamID,
                                (SELECT e.name
                                    FROM bn_govtexam_hierarchy e
                                    WHERE e.GOVT_EXAMID = academic.ExamID)
                                    AS Exam_Name,

                            (SELECT b.NAME
                                    FROM bn_boardcenter b
                                    WHERE b.BOARD_CENTERID = academic.BoardID)
                                    AS Board_Name,
                                    academic.BoardID,
                            (SELECT sg.NAME
                                    FROM bn_subject_group sg
                                    WHERE sg.SUBJECT_GROUPID = academic.SubjectID)
                                    Subject,
                                    academic.SubjectID,
                            PassingYear,
                            EvaluationSystem,
                            (SELECT CASE EvaluationSystem
                                                    WHEN 1 THEN 'Traditional'
                                                    WHEN 2 THEN 'GPA'
                                                    ELSE NULL
                                            END)
                                    AS Exam_System,
                            (SELECT ex.NAME
                                    FROM bn_examgrade ex
                                    WHERE ex.EXAM_GRADEID = academic.ExamGradeID)
                                    AS Exam_Gred,
                                    academic.ExamGradeID,
                                    academic.EducationStatus AS EDUST,
                            ResultDescription,
                            TotalMarks,
                            Percentage,
                            (SELECT CASE EducationStatus
                                                    WHEN 1 THEN 'Entry Education'
                                                    WHEN 2 THEN 'Entry Period Highest'
                                                    when 3 then 'Last'
                                                    ELSE NULL                
                                       END)
                    AS EducationStatus, 
                    academic.AcademicID     
                    FROM academic 
                    WHERE SailorID = $sailor_id")->result();

        $multiple = array(
            'academic' => $academicInfo,
            'general' => $result
        );
        echo json_encode($multiple);
    }

}
