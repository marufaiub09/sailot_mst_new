<?php

/**
 * This file is part of the DGDP\e-DP System package
 *
 * (c) ATI Limited. <info@atilimited.net>
 *
 * PHP version 5 (5.5.9)
 *
 * @package     DGDP\e-DP
 * @author      ATI Limited Dev Team
 * @copyright   2016 atilimited.net
 */
/**
 * Class Site
 *
 * Site is the extended class of CI_Controller.
 *
 * This class implements all methods related to Role Based
 * Access Control for User of e-DP System.
 *
 * @package     DGDP\Controllers
 * @author      saif <saif017@gmail.com>
 * @copyright   2016 atilimited.net
 * @version     GIT: $Id$ In development. 1.0.0
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('string');
        $this->load->library('email');
    }

    public function registration_first_step() {
        $this->load->view('application_form_first_step');
    }

    public function registration_second_step() {

        $this->load->view('registration_second_step');
    }

    public function form() {
        $this->load->view('new_form');
    }

    public function application_form() {

        if ($_POST) {
            $transKey = 'ATI' . uniqid();

            $fname = strtoupper($this->input->post('app_f_name'));
            $mname = strtoupper($this->input->post('app_m_name'));
            $lname = strtoupper($this->input->post('app_l_name'));
            $email_address = strtolower($this->input->post('email'));

            $autoKey = $this->utilities->generateRandomString('25');
            if ($this->utilities->hasInformationByThisId('rg_temp_applicant', array('SECURITY_CODE' => $autoKey))) {
                $autoKey = $this->utilities->generateRandomString('25');
            }

            $code = mt_rand(10000, 99999);
            if ($this->utilities->hasInformationByThisId('rg_temp_applicant', array('CODE' => $code))) {
                $code = mt_rand(10000, 99999);
            }

            $applicantType = $this->input->post('applicantType');
            $insertData = array(
                'APPLICANT_TYPE' => $applicantType,
                'CURRENCY_TYPE' => $this->input->post('currency_type'),
                'SALUTATION_ID' => $this->input->post('salutationL'),
                'APPLICANT_FNAME' => $fname,
                'APPLICANT_MNAME' => $mname,
                'APPLICANT_LNAME' => $lname,
                'APPLICANT_LFNAME' => strtoupper($this->input->post('app_name_bn')),
                //'COUNTRY_ID' => $this->input->post('countryCode'),
                'BUSINESS_TYPE' => $this->input->post('business_type'),
                'CODE' => $code,
                'SECURITY_CODE' => $autoKey,
                'COMPANY_NAME' => strtoupper($this->input->post('company_name')),
                'HOUSE_NO_NAME' => $this->input->post('office_house'),
                'ROAD_AVENO_NAME' => $this->input->post('office_road'),
                'VILLAGE_WARD' => $this->input->post('office_vill'),
                'DISTRICT_ID' => $this->input->post('office_district'),
                'THANA_ID' => $this->input->post('office_upzilla'),
                'POLICE_STATION_ID' => $this->input->post('office_ps'),
                'UNION_ID' => $this->input->post('office_union'),
                'POST_OFFICE_ID' => $this->input->post('office_post_office'),
                'COMPANY_TYPE' => $this->input->post('company_type'),
                'EMAIL_ADRESS' => $email_address,
                'MOBILE_NO' => $this->input->post('phone'),
                'TELEPHONE_NO' => $this->input->post('telephone_no'),
                'FAX_NO' => $this->input->post('fax_no'),
                'WEBSITE' => $this->input->post('website'),
                'NATIONAL_ID' => $this->input->post('national_id'),
                'OFFICE_ADRESS' => $this->input->post('company_address'),
                'HAS_BRANCH_ADDRESS' => $this->input->post('HAS_BRANCH_ADDRESS'),
                'BRANCH_ADDRESS' => $this->input->post('BRANCH_ADDRESS'),
                'TRANSACTION_KEY' => $transKey
            );
            if ($this->db->insert('rg_temp_applicant', $insertData)) {
                $temp_app_id = $this->db->insert_id();

                // Address Info
                if (isset($_POST['same_as_present'])) {
                    $same_as_present = 'PR';
                } else {
                    $same_as_present = "PS";
                }

                $insert_applicant_present_address = array(
                    'TEMP_APPLICANT_ID' => $temp_app_id,
                    'HOUSE_NO_NAME' => $this->input->post('pre_house_no'),
                    'ROAD_AVENO_NAME' => $this->input->post('pre_road_no'),
                    'VILLAGE_WARD' => $this->input->post('pre_vill'),
                    'DISTRICT_ID' => $this->input->post('pre_district'),
                    'THANA_ID' => $this->input->post('pre_upzilla'),
                    'POLICE_STATION_ID' => $this->input->post('pre_ps'),
                    'UNION_ID' => $this->input->post('pre_union'),
                    'POST_OFFICE_ID' => $this->input->post('pre_po'),
                    'ADRESS_TYPE' => 'PS',
                    'SAS_PSORPR' => $same_as_present
                );

                $this->db->insert('rg_temp_applicant_adress', $insert_applicant_present_address);

                if ($same_as_present == 'PS') {
                    $insert_applicant_permanent_address = array(
                        'TEMP_APPLICANT_ID' => $temp_app_id,
                        'HOUSE_NO_NAME' => $this->input->post('per_house_no'),
                        'ROAD_AVENO_NAME' => $this->input->post('per_road_no'),
                        'VILLAGE_WARD' => $this->input->post('per_vill'),
                        'DISTRICT_ID' => $this->input->post('per_district'),
                        'THANA_ID' => $this->input->post('per_upzilla'),
                        'POLICE_STATION_ID' => $this->input->post('per_ps'),
                        'UNION_ID' => $this->input->post('per_union'),
                        'POST_OFFICE_ID' => $this->input->post('per_post_office'),
                        'ADRESS_TYPE' => 'PR',
                        'SAS_PSORPR' => 'PR'
                    );
                    $this->db->insert('rg_temp_applicant_adress', $insert_applicant_permanent_address);
                }

                //Service Info
                if ($applicantType == 'M') {
                    $serviceDataArray = array(
                        'TEMP_APPLICANT_ID' => $temp_app_id,
                        'EX_PERSONAL_NUMBER' => $this->input->post('ex_personal_number'),
                        'POST_RET_NO' => $this->input->post('post_retirement_number'),
                        'CORPS' => $this->input->post('corps'),
                        'COMMISSION_ENROLMENT_DT' => ($this->input->post('commission_enrolment') == '') ? '' : date('Y-m-d', strtotime($this->input->post('commission_enrolment'))),
                        'LAST_UNIT_SERVED' => $this->input->post('last_unit_served'),
                        'HOW_RETD' => $this->input->post('ret_type'),
                        'RETIREMENT_DT' => ($this->input->post('date_of_retirement') == '') ? '' : date('Y-m-d', strtotime($this->input->post('date_of_retirement'))),
                        'REASONS_OF_RETIREMENT' => $this->input->post('reasons_of_retirement')
                    );
                    $this->db->insert('rg_temp_service_information', $serviceDataArray);

                    $unit_served_designation = $this->input->post('unit_served_designation');
                    if (!empty($unit_served_designation)) {
                        for ($i = 0; $i < count($unit_served_designation); $i++) {
                            $usDataArray = array(
                                'TEMP_APPLICANT_ID' => $temp_app_id,
                                'RANK' => $_POST['served_rank'][$i],
                                'DESIG_ID' => $_POST['unit_served_designation'][$i],
                                'UNIT_ID' => $_POST['unit_served_unit'][$i],
                                'DIRECTORATE' => $_POST['served_directorate'][$i],
                                'BRANCH' => $_POST['served_branch'][$i],
                                'FROM_DT' => ($_POST['unit_served_from'][$i] == '') ? '' : date('Y-m-d', strtotime($_POST['unit_served_from'][$i])),
                                'TO_DT' => ($_POST['unit_served_to'][$i] == '') ? '' : date('Y-m-d', strtotime($_POST['unit_served_to'][$i]))
                            );
                            $this->db->insert('rg_temp_si_unit_served', $usDataArray);
                        }
                    }
                }
                $actionUrl = site_url('site/check_user_authentication') . '/' . $autoKey;
                $data["msgBody"] = "$fname $mname $lname, <br /><br /> We are pleased to inform you that, your application was submitted successfully. Please verify your information by using the code or clicking the link following link.<br />
                                                To varify use this varification code: <b>$code</b> or click the following link below: <br />
                                                <a href=" . $actionUrl . ">" . $actionUrl . "</a><br /><br />
                                                
                                                After verify your information, you have to pay Application processing fee. If you faield to pay the fee your application will be rejected. 
                                                You will be guided through the site to see full set of instruction and requirement, please provide all information required by DGDP.
                                                <br /><br />
                                                On behalf of 
                                                DGDP ( Registration Office )";

                require 'gmail_app/class.phpmailer.php';
                $mail = new PHPMailer;
                $mail->IsSMTP();
                $mail->Host = "cloud2.eicra.com";
                $mail->Port = "465";
                $mail->SMTPAuth = true;
                $mail->Username = "dgdp@atilimited.net";
                $mail->Password = "dgdp@1234";
                $mail->SMTPSecure = 'ssl';
                $mail->From = "dgdp@atilimited.net";
                $mail->FromName = "DGDP Registration Office";
                $mail->AddAddress($email_address);
                $mail->AddReplyTo('dgdp@atilimited.net');
                $mail->WordWrap = 1000;
                $mail->IsHTML(TRUE);
                $mail->Subject = "DGDP Online Procurement";
                $mail->Body = $this->load->view('email_template', $data, TRUE); // email template
                $send = $mail->Send(); // email send
                if ($send) {
                    redirect("site/applicant_code_check", 'refresh');
                }
            }
            /* $regCharge = ($this->input->post('applicantType') == 'L') ? $this->input->post('regChargeBDT') : $this->input->post('regChargeUSD');
              redirect('http://atilimited.net/sms_test/online_pay/shurjoPay.php?amount='.$regCharge.''); */
        }
        $data['payment'] = '';
        $data['group_type'] = $this->utilities->findAllFromView('dgdpv_group_type');
        $data['nTitle'] = $this->utilities->dropdownFromTableWithCondition('sa_salutation', '', 'SALUTATION_ID', 'SALUTATION_NAME', array('ACTIVE_STATUS' => 1));
        $data['countries'] = $this->utilities->findAllFromView('sa_country');
        $data['bType'] = $this->utilities->dropdownFromTableWithCondition('sav_business_type', '', 'BUSINESS_TYPE', 'BUSS_TYPE_NAME', array('ACTIVE_FLAG' => 1));
        $data['comType'] = $this->utilities->dropdownFromTableWithCondition('sav_company_type', '', 'COMPANY_TYPE', 'COM_TYPE_NAME', '', 'ORDER_SL_NO');
        $data['marital_status'] = $this->utilities->dropdownFromTableWithCondition('sav_marital_status', '', 'CHAR_LOOKUP', 'MARITAL_NAME');
        $data['education_degree'] = $this->utilities->dropdownFromTableWithCondition('education_degree', '', 'DEGREE_ID', 'DEGREE_NAME');



        $present_thanas = $present_police_stations = $present_unions = $present_post_offices = $permanent_thanas = $permanent_police_stations = $permanent_unions = $permanent_post_offices = array('' => '-- Select --');

        $data['present_thanas'] = $present_thanas;
        $data['present_police_stations'] = $present_police_stations;
        $data['present_unions'] = $present_unions;
        $data['present_post_offices'] = $present_post_offices;
        $data['permanent_thanas'] = $permanent_thanas;
        $data['permanent_police_stations'] = $permanent_police_stations;
        $data['permanent_unions'] = $permanent_unions;
        $data['permanent_post_offices'] = $permanent_post_offices;



        $data['districts'] = $this->utilities->dropdownFromTableWithCondition('sa_districts', '', 'DISTRICT_ID', 'DISTRICT_ENAME');
        $firm_thanas = $firm_police_stations = $firm_unions = $firm_post_offices = array('' => '-- Select --');
        if (!empty($firm_adress_info)) {
            $firm_thanas = $this->utilities->dropdownFromTableWithCondition('sa_thanas', '', 'THANA_ID', 'THANA_ENAME', array('DISTRICT_ID' => ($firm_adress_info->DISTRICT_ID > 0) ? $firm_adress_info->DISTRICT_ID : 0));
            $firm_police_stations = $this->utilities->dropdownFromTableWithCondition('sa_police_station', '', 'POLICE_STATION_ID', 'PS_ENAME', array('THANA_ID' => ($firm_adress_info->THANA_ID > 0) ? $firm_adress_info->THANA_ID : 0));
            $firm_unions = $this->utilities->dropdownFromTableWithCondition('sa_unions', '', 'UNION_ID', 'UNION_NAME', array('THANA_ID' => ($firm_adress_info->THANA_ID > 0) ? $firm_adress_info->THANA_ID : 0));
            $firm_post_offices = $this->utilities->dropdownFromTableWithCondition('sa_post_offices', '', 'POST_OFFICE_ID', 'POST_OFFICE_ENAME', array('THANA_ID' => ($firm_adress_info->THANA_ID > 0) ? $firm_adress_info->THANA_ID : 0));
        }
        $data['firm_thanas'] = $firm_thanas;
        $data['firm_police_stations'] = $firm_police_stations;
        $data['firm_unions'] = $firm_unions;
        $data['firm_post_offices'] = $firm_post_offices;
        $data['ranks'] = $this->utilities->dropdownFromTableWithCondition('sa_rank', '', 'RANK_ID', 'RANK_NAME', array('ACTIVE_STATUS' => 1));
        $data['designations'] = $this->utilities->dropdownFromTableWithCondition('sa_designation', '', 'DESIG_ID', 'DESIG_NAME', array('ACTIVE_STATUS' => 1));
        $data['units'] = $this->utilities->dropdownFromTableWithCondition('sav_units', '', 'UNIT_ID', 'UNIT_NAME', array('ACTIVE_FLAG' => 1));

        $data['content_view_page'] = 'application_form/application_form';
        $this->template_auth->display($data);
    }

    public function aaa() {
        $code = mt_rand(10000, 99999);
        if ($this->utilities->hasInformationByThisId('rg_temp_applicant', array('CODE' => $code))) {
            $code = 'M' . mt_rand(10000, 99999);
        }
        echo $code;
    }

    public function check_user_authentication($security) {
        //$this->session->set_userdata('temp_app_id', '');
        $msg = '';
        if ($this->session->userdata('temp_app_id') > 0) {
            //echo 1; exit;

            $auth_details = $this->utilities->findByAttribute('rg_temp_applicant', array('TEMP_APPLICANT_ID' => $this->session->userdata('temp_app_id')));
            if ($auth_details->STATUS_FG == 0) {
                redirect("site/application_payment?check_code=true", 'refresh');
            } else {
                $this->session->unset_userdata('temp_app_id');
                $this->session->unset_userdata('logged_in');
                $this->session->set_flashdata('Success', 'Please login to check your Application status.');
                redirect("auth/index", 'refresh'); // go to login
            }
        } else {

            $auth_details = $this->utilities->findByAttribute('rg_temp_applicant', array('SECURITY_CODE' => $security));
            if ($auth_details):
                if ($auth_details->USED_FG == 'N'):
                    $this->session->set_userdata('temp_app_id', $auth_details->TEMP_APPLICANT_ID);
                    //echo 3; exit;

                    $this->utilities->updateData('rg_temp_applicant', array('USED_FG' => 'Y'), array('TEMP_APPLICANT_ID' => $this->session->userdata('temp_app_id')));
                    redirect("site/application_payment?check_code=true", 'refresh');
                else:
                    //echo 4; exit;
                    $this->session->set_userdata('temp_app_id', $auth_details->TEMP_APPLICANT_ID);
                    if ($auth_details->STATUS_FG == 0) {
                        redirect("site/application_payment?check_code=true", 'refresh');
                    } else {
                        $this->session->unset_userdata('temp_app_id');
                        $this->session->unset_userdata('logged_in');
                        $this->session->set_flashdata('Success', 'Please login to check your Application status.');
                        redirect("auth/index", 'refresh'); // go to login
                    }

                    $msg = '<span style="color:red;">Sorry! the URL has been expired.</span>';
                endif;
            else:
                $msg = '<span style="color:red;">Sorry the URL you are trying is invalid, Please try again with a valid URL.</span>';
            endif;
        }
        $data['msg'] = $msg;
        $data['content_view_page'] = 'application_form/applicant_auth_faild';
        $this->template_auth->display($data);
    }

    public function applicant_code_check() {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('code', 'Code', 'required|callback_check_code_database');
        if ($this->form_validation->run() == TRUE) {
            if ($this->session->userdata('temp_app_id') != '') {
                $this->utilities->updateData('rg_temp_applicant', array('USED_FG' => 'Y'), array('TEMP_APPLICANT_ID' => $this->session->userdata('temp_app_id')));
                redirect("site/application_payment?check_code=true", 'refresh');
            }
        }

        $data['content_view_page'] = 'application_form/applicant_code_check';
        $this->template_auth->display($data);
    }

    function check_code_database($code) {
        // print_r($_POST); exit;
        $result = $this->utilities->findByAttribute('rg_temp_applicant', array('CODE' => $code, 'USED_FG' => 'N'));
        if ($result) {
            $this->session->set_userdata('temp_app_id', $result->TEMP_APPLICANT_ID);
            return TRUE;
        } else {
            $this->session->set_userdata('temp_app_id', '');
            $this->form_validation->set_message('check_code_database', '<div class="alert alert-danger">Sorry! The Code you entered is not valid. Please try again with valid code.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            return false;
        }
    }

    public function checkEmailExist() {
        $email = $this->input->post('email');
        $data = $this->utilities->findByAttribute('rg_temp_applicant', array('EMAIL_ADRESS' => $email));
        $number = count($data);
        echo $number;
    }

    public function application_payment() {
        $temp_app_id = $this->session->userdata('temp_app_id');
        $applicant_details = $this->utilities->findByAttributeWithJoin('rg_temp_applicant', 'sa_salutation', 'SALUTATION_ID', 'SALUTATION_ID', 'SALUTATION_NAME', array('rg_temp_applicant.TEMP_APPLICANT_ID' => $temp_app_id));

        if ($applicant_details) {
            if ($_POST) {
                $appDaataArray = array(
                    'SALUTATION_ID' => $applicant_details->SALUTATION_ID,
                    'CURRENCY_TYPE' => $applicant_details->CURRENCY_TYPE,
                    'APPLICANT_TYPE'=> $applicant_details->APPLICANT_TYPE,
                    'APPLICANT_FNAME' => $applicant_details->APPLICANT_FNAME,
                    'APPLICANT_MNAME' => $applicant_details->APPLICANT_MNAME,
                    'APPLICANT_LNAME' => $applicant_details->APPLICANT_LNAME,
                    'MOBILE_NO' => $applicant_details->MOBILE_NO,
                    'TELEPHONE_NO' => $applicant_details->TELEPHONE_NO,
                    'FAX_NO' => $applicant_details->FAX_NO,
                    'WEBSITE' => $applicant_details->WEBSITE,
                    'NATIONAL_ID' => $applicant_details->NATIONAL_ID,
                    'BUSINESS_TYPE' => $applicant_details->BUSINESS_TYPE,
                    'COMPANY_NAME' => $applicant_details->COMPANY_NAME,
                    'COMPANY_TYPE' => $applicant_details->COMPANY_TYPE,
                    'OFFICE_ADRESS' => $applicant_details->OFFICE_ADRESS,
                    'HAS_BRANCH_ADDRESS' => $applicant_details->HAS_BRANCH_ADDRESS,
                    'BRANCH_ADDRESS' => $applicant_details->BRANCH_ADDRESS,
                    'BANK_NAME' => $applicant_details->BANK_NAME,
                    'BANK_ADDRESS' => $applicant_details->BANK_ADDRESS,
                    'BANK_ACC_NO' => $applicant_details->BANK_ACC_NO,
                    'ACC_REL_WITH_SUPPLIER' => $applicant_details->ACC_REL_WITH_SUPPLIER,
                    'COUNTRY_ID' => $applicant_details->COUNTRY_ID,
                    'EMAIL_ADRESS' => $applicant_details->EMAIL_ADRESS
                );

                if ($this->db->insert('rg_applicant_info', $appDaataArray)) {
                    $max_app_id = $this->db->insert_id();

                    $firmAdressArray = array(
                        'APPLICANT_ID' => $max_app_id,
                        'HOUSE_NO_NAME' => $applicant_details->HOUSE_NO_NAME,
                        'ROAD_AVENO_NAME' => $applicant_details->ROAD_AVENO_NAME,
                        'VILLAGE_WARD' => $applicant_details->VILLAGE_WARD,
                        'DISTRICT_ID' => $applicant_details->DISTRICT_ID,
                        'THANA_ID' => $applicant_details->THANA_ID,
                        'POLICE_STATION_ID' => $applicant_details->POLICE_STATION_ID,
                        'UNION_ID' => $applicant_details->UNION_ID,
                        'POST_OFFICE_ID' => $applicant_details->POST_OFFICE_ID
                    );
                    $this->db->insert('rg_firm_adress', $firmAdressArray);

                    //FROM TEMP------

                    $address_details = $this->utilities->findAllByAttribute('rg_temp_applicant_adress', array('TEMP_APPLICANT_ID' => $temp_app_id));
//                    echo '<pre>';
//                    print_r($address_details);
//                    echo '</pre>'; exit;
                    if (!empty($address_details)) {
                        for ($i = 0; $i < count($address_details); $i++) {
                            $insert_applicant_address = array(
                                'APPLICANT_ID' => $max_app_id,
                                'HOUSE_NO_NAME' => $address_details[$i]->HOUSE_NO_NAME,
                                'ROAD_AVENO_NAME' => $address_details[$i]->ROAD_AVENO_NAME,
                                'VILLAGE_WARD' => $address_details[$i]->VILLAGE_WARD,
                                'DISTRICT_ID' => $address_details[$i]->DISTRICT_ID,
                                'THANA_ID' => $address_details[$i]->THANA_ID,
                                'POLICE_STATION_ID' => $address_details[$i]->POLICE_STATION_ID,
                                'UNION_ID' => $address_details[$i]->UNION_ID,
                                'POST_OFFICE_ID' => $address_details[$i]->POST_OFFICE_ID,
                                'ADRESS_TYPE' => $address_details[$i]->ADRESS_TYPE,
                                'SAS_PSORPR' => $address_details[$i]->SAS_PSORPR
                            );
                            $this->db->insert('rg_applicant_adress', $insert_applicant_address);
                        }
                    }

                    //Service
                    if ($applicant_details->APPLICANT_TYPE == 'M') {
                        $service_details = $this->utilities->findByAttribute('rg_temp_service_information', array('TEMP_APPLICANT_ID' => $temp_app_id));
                        $serviceDataArray = array(
                            'APPLICANT_ID' => $max_app_id,
                            'EX_PERSONAL_NUMBER' => $service_details->EX_PERSONAL_NUMBER,
                            'POST_RET_NO' => $service_details->POST_RET_NO,
                            'CORPS' => $service_details->CORPS,
                            'COMMISSION_ENROLMENT_DT' => ($service_details->COMMISSION_ENROLMENT_DT == '') ? '' : date('Y-m-d', strtotime($service_details->COMMISSION_ENROLMENT_DT)),
                            'LAST_UNIT_SERVED' => $service_details->LAST_UNIT_SERVED,
                            'HOW_RETD' => $service_details->HOW_RETD,
                            'RETIREMENT_DT' => ($service_details->RETIREMENT_DT == '') ? '' : date('Y-m-d', strtotime($service_details->RETIREMENT_DT)),
                            'REASONS_OF_RETIREMENT' => $service_details->REASONS_OF_RETIREMENT
                        );
                        $this->db->insert('rg_service_information', $serviceDataArray);

                        $unit_served_details = $this->utilities->findAllByAttribute('rg_temp_si_unit_served', array('TEMP_APPLICANT_ID' => $temp_app_id));
                        if (!empty($unit_served_details)) {
                            for ($i = 0; $i < count($unit_served_details); $i++) {
                                $usDataArray = array(
                                    'APPLICANT_ID' => $max_app_id,
                                    'RANK' => $unit_served_details[$i]->RANK,
                                    'DESIG_ID' => $unit_served_details[$i]->DESIG_ID,
                                    'UNIT_ID' => $unit_served_details[$i]->UNIT_ID,
                                    'DIRECTORATE' => $unit_served_details[$i]->DIRECTORATE,
                                    'BRANCH' => $unit_served_details[$i]->BRANCH,
                                    'FROM_DT' => ($unit_served_details[$i]->FROM_DT == '') ? '' : date('Y-m-d', strtotime($unit_served_details[$i]->FROM_DT)),
                                    'TO_DT' => ($unit_served_details[$i]->TO_DT == '') ? '' : date('Y-m-d', strtotime($unit_served_details[$i]->TO_DT))
                                );
                                $this->db->insert('rg_si_unit_served', $usDataArray);
                            }
                        }
                    }

//----------Temp------->
                    //$max_app_id = $this->utilities->get_max_value("rg_applicant_info", "APPLICANT_ID");

                    $this->db->query("UPDATE rg_applicant_info SET STEP_TLSL = 2, USERLVL_ID = 0 WHERE APPLICANT_ID = $max_app_id");
                    $this->db->query("UPDATE rg_temp_applicant SET STATUS_FG = 1 WHERE TEMP_APPLICANT_ID = $temp_app_id");
                    
                    $transactionDataArray = array(
                        'APPLICANT_ID'=>$max_app_id,
                        'CHARGE_ID'=>1,
                        'TRANS_AMMOUNT'=> $this->input->post('amount')
                    );
                    $this->db->insert('rg_transaction_history', $transactionDataArray);
                    //echo $code;
                    $actionUrl = site_url() . 'auth/index';

                    $data["msgBody"] = "Dear $applicant_details->SALUTATION_NAME $applicant_details->APPLICANT_FNAME $applicant_details->APPLICANT_MNAME $applicant_details->APPLICANT_LNAME, <br /><br /> We are pleased to inform you that, your payment process successfull.
                                                You will get an email with User Id and Password after varify your application.<br />
                                                You will be guided through the site to see full set of instruction and requirement, please provide all information required by DGDP.
                                                <br /><br />
                                                On behalf of 
                                                DGDP (Registration Office)";

                    require 'gmail_app/class.phpmailer.php';
                    $mail = new PHPMailer;
                    $mail->IsSMTP();
                    $mail->Host = "cloud2.eicra.com";
                    $mail->Port = "465";
                    $mail->SMTPAuth = true;
                    $mail->Username = "dgdp@atilimited.net";
                    $mail->Password = "dgdp@1234";
                    $mail->SMTPSecure = 'ssl';
                    $mail->From = "dgdp@atilimited.net";
                    $mail->FromName = "DGDP Registration Office";
                    $mail->AddAddress($applicant_details->EMAIL_ADRESS);
                    $mail->AddReplyTo('dgdp@atilimited.net');
                    $mail->WordWrap = 1000;
                    $mail->IsHTML(TRUE);
                    $mail->Subject = "DGDP Online Procurement";
                    $mail->Body = $this->load->view('email_template', $data, TRUE);
                    $send = $mail->Send();
                }
                $this->session->unset_userdata('logged_in');
                $this->session->set_flashdata('Success', 'Your payment was successfull. Please check your email for login details.');
                redirect("auth/index?payment_complete=true", 'refresh');
            }
            $data['content_view_page'] = 'application_form/application_payment';
            $this->template_auth->display($data);
        } else {
            $this->session->unset_userdata('temp_app_id');
            $this->session->unset_userdata('logged_in');
            $this->session->set_flashdata('Success', 'Please login to check your Application status.');
            //redirect("auth/index", 'refresh'); // go to login
            $data['content_view_page'] = 'application_form/application_payment';
            $this->template_auth->display($data);
        }

        //$this->load->view('application_form/application_payment');
    }

    function delete_row_by_id() {
        $id = $_POST['id'];
        /** @var VARCHAR $table_name */
        $table_name = $_POST['table_name'];
        /** @var VARCHAR $field_name */
        $field_name = $_POST['field_name'];
        $this->utilities->deleteRowByAttribute($table_name, array($field_name => $id));
        echo 'Y';
    }

    function get_caste_by_religion() {
        $castes = $this->utilities->dropdownFromTableWithCondition('sa_castes', 'none', 'CASTE_ID', 'CASTE_NAME', array('ACTIVE_STATUS' => 1, 'RELIGION_ID' => $_POST['RELIGION_ID']));
        echo json_encode($castes);
    }

    function district_wise_ps() {
        $DISTRICT_ID = $_POST['disteict_id'];
        $data['thana'] = $this->utilities->findAllByAttribute('sa_thanas', array('DISTRICT_ID' => $DISTRICT_ID));
        $this->load->view('application_form/ajax_view/ajax_thana_district_wise', $data);
    }

    function upzilla_wise_ps() {
        $THANA_ID = $_POST['upzilla_id'];
        $data['sa_police_station'] = $this->utilities->findAllByAttribute('sa_police_station', array('THANA_ID' => $THANA_ID));
        $this->load->view('application_form/ajax_view/ajax_ps_upzilla_wise', $data);
    }

    function district_wise_union() {
        $THANA_ID = $_POST['upzilla_id'];
        $data['sa_unions'] = $this->utilities->findAllByAttribute('sa_unions', array('THANA_ID' => $THANA_ID));
        $this->load->view('application_form/ajax_view/ajax_union_upzilla_wise', $data);
    }

    function district_wise_post_office() {
        $THANA_ID = $_POST['upzilla_id'];
        $data['sa_post_offices'] = $this->utilities->findAllByAttribute('sa_post_offices', array('THANA_ID' => $THANA_ID));
        $this->load->view('application_form/ajax_view/ajax_post_office_upzilla_wise', $data);
    }

    //Admin
//    public function applied_applicant_list() {
//        $data['pageTitle'] = 'Applicant list';
//        //$data['all_applicant'] = $this->db->query("SELECT * FROM rg_temp_applicant WHERE STATUS_FG = 0")->result();
//        $data['all_applicant'] = $this->utilities->findAllByAttribute('rg_applicant_info_v', array('STEP_TLSL' => 1, 'USERLVL_ID' => 0));
//        $data['content_view_page'] = 'application_form/admin/applicant_list';
//        $this->template->display($data);
//    }

    public function view_applicant_details($id) {
        $data['applicant_details'] = $this->utilities->findByAttribute('rg_applicant_info_v', array('APPLICANT_ID' => $id));
        $data['firm_address'] = $this->utilities->findByAttribute('rg_firm_adress_v', array('APPLICANT_ID' => $id));
        $data['applicant_trans_info'] = $this->utilities->findByAttribute('rg_transaction_history', array('APPLICANT_ID' => $id, 'CHARGE_ID' => 1));
        //$data['applicant_details'] = $this->db->query("SELECT t.*, sg.GROUP_TYPE_NAME FROM rg_applicant t LEFT JOIN dgdpv_group_type sg ON t.GROUP = sg.GROUP_TYPE WHERE t.TEMP_APPLICANT_ID = $id")->row();
        ////$this->db->query("SELECT * FROM rg_temp_applicant WHERE TEMP_APPLICANT_ID = $id")->row();
        $this->load->view('application_form/admin/applicant_view', $data);
    }

    public function print_applicant_info($id) {
        //$data['applicant_details'] = $this->db->query("SELECT t.*, sg.GROUP_TYPE_NAME FROM rg_temp_applicant t LEFT JOIN dgdpv_group_type sg ON t.GROUP = sg.GROUP_TYPE WHERE t.TEMP_APPLICANT_ID = $id")->row();
        $data['applicant_details'] = $this->utilities->findByAttribute('rg_applicant_info_v', array('APPLICANT_ID' => $id));
        $data['firm_address'] = $this->utilities->findByAttribute('rg_firm_adress_v', array('APPLICANT_ID' => $id));
        $data['applicant_trans_info'] = $this->utilities->findByAttribute('rg_transaction_history', array('APPLICANT_ID' => $id, 'CHARGE_ID' => 1));
        $output = $this->load->view('application_form/admin/applicant_view', $data, true);
        $this->load->library('mpdf_gen');
        $this->mpdf_gen->gen_pdf($output);
    }

    public function pro_selected_applicant_details($APPLICANT_ID) {

        $data['applicant_info'] = $applicant_info = $this->utilities->findByAttribute('rg_applicant_info_v', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['firm_adress_info'] = $firm_adress_info = $this->utilities->findByAttribute('rg_firm_adress_v', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['service_info'] = $service_info = $this->utilities->findByAttribute('rg_service_information', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['unit_served_infos'] = $this->utilities->findAllByAttribute('rg_si_unit_served_v', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['defence_served_infos'] = $this->utilities->findAllByAttribute('rg_relatives_def_serv_v', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['prominent_non_relatives'] = $this->utilities->findAllByAttribute('rg_prominent_non_relatives', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['relatives_assets_info'] = $this->utilities->findAllByAttribute('rg_relatives_assets_info_v', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['bro_sisters_info'] = $this->db->query("SELECT * FROM rg_applicant_relative WHERE APPLICANT_ID = $APPLICANT_ID AND OR_PARTNER_ID = 0 AND REL_TYPE IN('B', 'SI')")->result();
        $data['spous_info'] = $this->db->query("SELECT spi.*, (SELECT r.RELIGION_NAME
              FROM sav_religion r WHERE (convert(r.RELIGION_ID USING utf8) = spi.RELIGION_ID))
              AS RELIGION_NAME FROM rg_applicant_relative spi WHERE APPLICANT_ID = $APPLICANT_ID AND OR_PARTNER_ID = 0 AND REL_TYPE = 'SP'")->row();
        $data['present_address_info'] = $present_address_info = $this->utilities->findByAttribute('rg_applicant_adress_v', array('APPLICANT_ID' => $APPLICANT_ID, 'ADRESS_TYPE' => 'PS', 'OR_PARTNER_ID' => 0));
        $permanent_address_info = $present_address_info;
        if (!empty($present_address_info)) {
            if ($present_address_info->SAS_PSORPR == 'PS') {
                $permanent_address_info = $this->utilities->findByAttribute('rg_applicant_adress_v', array('APPLICANT_ID' => $APPLICANT_ID, 'ADRESS_TYPE' => 'PR', 'OR_PARTNER_ID' => 0));
            }
        }
        $data['permanent_address_info'] = $permanent_address_info;
        //$data['license_info'] = $this->utilities->findAllByAttribute('rg_license_certificate', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['license_info'] = $this->db->query("SELECT lc.*, (SELECT r.LOOKUP_DATA_NAME
              FROM sav_license_types r WHERE r.LOOKUP_DATA_ID = lc.LICENSE_TYPE)
              AS LICENSE_NAME, (SELECT fy.FY_NAME
              FROM sa_fin_year fy WHERE fy.FY_NO = lc.LAST_RENEWAL_FY)
              AS LAST_RENEWED
                FROM rg_license_certificate lc WHERE APPLICANT_ID = $APPLICANT_ID")->result();
        $data['regCharge'] = $this->utilities->findByAttribute('sa_charge_head', array('UD_CHARGE_ID' => 'A1'));
        $data['attachment_info'] = $this->utilities->findAllByAttribute('rg_attachment_info_v', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['edu_details'] = $this->utilities->findAllByAttribute('rg_academic_info_v', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['bank_details'] = $this->utilities->findAllByAttribute('rg_bank_accounts_v', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['membership_details'] = $this->utilities->findAllByAttribute('rg_membership_info', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        $data['partner_details'] = $this->db->query("SELECT pa.*,
       (SELECT r.RELIGION_NAME
          FROM sav_religion r
         WHERE (convert(r.RELIGION_ID USING utf8) = pa.RELIGION_ID))
          AS RELIGION_NAME,
       (SELECT c.CASTE_NAME
          FROM sa_castes c
         WHERE (convert(c.CASTE_ID USING utf8) = pa.CASTE_ID))
          AS CASTE_NAME,
       (SELECT fy.SHORT_NAME
          FROM sa_fin_year fy
         WHERE fy.FY_NO = pa.VALID_PERIOD)
          AS FY_NAME,
                (SELECT lkp.LOOKUP_DATA_NAME
              FROM sa_lookup_data lkp
             WHERE (lkp.LOOKUP_DATA_ID = pa.COLOR_OR_RACE))
              AS COLOR_OR_RACE_NAME,
           (SELECT lkp.LOOKUP_DATA_NAME
              FROM sa_lookup_data lkp
             WHERE (lkp.LOOKUP_DATA_ID = pa.COLOR_OF_EYES))
              AS COLOR_OF_EYES_NAME,
           (SELECT lkp.LOOKUP_DATA_NAME
              FROM sa_lookup_data lkp
             WHERE (lkp.LOOKUP_DATA_ID = pa.BODY_STRUCTURE))
              AS BODY_STRUCTURE_NAME
  FROM rg_partner_info pa
 WHERE pa.APPLICANT_ID = $APPLICANT_ID")->result();

        $data['relatives_org_info'] = $this->utilities->findAllByAttribute('rg_relatives_org_info', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['shared_org_info'] = $this->utilities->findAllByAttribute('rg_shared_org_info', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['others_org_info'] = $this->utilities->findAllByAttribute('rg_others_org_info', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['others_occupation_info'] = $this->utilities->findAllByAttribute('rg_others_occupation_info', array('APPLICANT_ID' => $APPLICANT_ID, 'OR_PARTNER_ID' => 0));
        //$data['pay_order_info'] = $this->utilities->findAllByAttribute('rg_pay_order_info_v', array('APPLICANT_ID' => $APPLICANT_ID));
        $data['witness_info'] = $this->utilities->findAllByAttribute('rg_witness_info', array('APPLICANT_ID' => $APPLICANT_ID));



        $this->load->view('application_form/admin/applicant_details_view', $data);
    }

    public function applicant_primary_approve() {
        $app_id = $this->input->post('appId');
        $data['applicant_details'] = $applicant_details = $this->db->query("SELECT * FROM rg_temp_applicant WHERE TEMP_APPLICANT_ID = $app_id")->row();

        $app_password = random_string('alnum', 8);
        $appDaataArray = array(
            'SALUTATION_ID' => $applicant_details->SALUTATION_ID,
            'APPLICANT_FNAME' => $applicant_details->APPLICANT_FNAME,
            'APPLICANT_MNAME' => $applicant_details->APPLICANT_MNAME,
            'APPLICANT_LNAME' => $applicant_details->APPLICANT_LNAME,
            'APPLICANT_LFNAME' => $applicant_details->APPLICANT_LFNAME,
            'MOBILE_NO' => $applicant_details->MOBILE_NO,
            'NATIONAL_ID' => $applicant_details->NATIONAL_ID,
            'BUSINESS_TYPE' => $applicant_details->BUSINESS_TYPE,
            'COMPANY_NAME' => $applicant_details->COMPANY_NAME,
            'COMPANY_TYPE' => $applicant_details->COMPANY_TYPE,
            'OFFICE_ADRESS' => $applicant_details->OFFICE_ADRESS,
            'HAS_BRANCH_ADDRESS' => $applicant_details->HAS_BRANCH_ADDRESS,
            'BRANCH_ADDRESS' => $applicant_details->BRANCH_ADDRESS,
            'BANK_NAME' => $applicant_details->BANK_NAME,
            'BANK_ADDRESS' => $applicant_details->BANK_ADDRESS,
            'BANK_ACC_NO' => $applicant_details->BANK_ACC_NO,
            'ACC_REL_WITH_SUPPLIER' => $applicant_details->ACC_REL_WITH_SUPPLIER,
            'COUNTRY_ID' => $applicant_details->COUNTRY_ID,
            'EMAIL_ADRESS' => $applicant_details->EMAIL_ADRESS,
            'APPLICANT_PSWD' => md5($app_password),
            'GROUP_TYPE' => $applicant_details->GROUP
        );

        if ($this->db->insert('rg_applicant_info', $appDaataArray)) {
            //$max_app_id = $this->utilities->get_max_value("rg_applicant_info", "APPLICANT_ID");
            $max_app_id = $this->db->insert_id();
            $code_ss = str_pad($max_app_id, 3, "0", STR_PAD_LEFT);
            $code = date('y') . '' . $code_ss;
            $this->db->query("UPDATE rg_applicant_info SET CODE = $code, STEP_TLSL = 1, USERLVL_ID = 0 WHERE APPLICANT_ID = $max_app_id");
            $this->db->query("UPDATE rg_temp_applicant SET STATUS_FG = 1 WHERE TEMP_APPLICANT_ID = $app_id");
            //echo $code;
            $actionUrl = site_url() . 'auth/index';

            $data["msgBody"] = "Dear $applicant_details->APPLICANT_FNAME $applicant_details->APPLICANT_MNAME $applicant_details->APPLICANT_LNAME  , <br /><br /> We are pleased to inform you that, you have received provisional approval to proceed with enlistment process with DGDP.
                                                To continue with enlistment process, kindly click the following link below:
                                                <a href=" . $actionUrl . ">" . $actionUrl . "</a><br /><br />
                                                Use the following Credentials to complete your application process. <br />
                                                User Id:  $code <br />
                                                Password: $app_password <br />
                                                You will be guided through the site to see full set of instruction and requirement, please provide all information required by DGDP.
                                                <br /><br />
                                                On behalf of 
                                                DGDP ( Registration Office )";

            require 'gmail_app/class.phpmailer.php';
            $mail = new PHPMailer;
            $mail->IsSMTP();
            $mail->Host = "cloud2.eicra.com";
            $mail->Port = "465";
            $mail->SMTPAuth = true;
            $mail->Username = "dgdp@atilimited.net";
            $mail->Password = "dgdp@1234";
            $mail->SMTPSecure = 'ssl';
            $mail->From = "dgdp@atilimited.net";
            $mail->FromName = "DGDP Registration Office";
            $mail->AddAddress($applicant_details->EMAIL_ADRESS);
            $mail->AddReplyTo('dgdp@atilimited.net');
            $mail->WordWrap = 1000;
            $mail->IsHTML(TRUE);
            $mail->Subject = "DGDP Online Procurement";
            $mail->Body = $this->load->view('email_template', $data, TRUE);
            $send = $mail->Send();
        }
        //$this->load->view('application_form/admin/applicant_wiew', $data);
    }

    public function primary_approved_applicant_list() {
        $data['pageTitle'] = 'Approved Applicant list';
        $data['breadcrumbs'] = array(
            'Approved Applicant list' => '#'
        );
        $data['all_applicant'] = $this->db->query("SELECT * FROM rg_applicant_info WHERE STEP_TLSL = 1 AND USERLVL_ID = 0")->result();
        $data['content_view_page'] = 'application_form/admin/primaryApprovedList';
        //$data['content_view_page'] = 'application_form/admin/primary_approved_list';
        $this->template->display($data);
    }

    public function view_primary_approved_applicant_details($id) {
        $data['applicant_details'] = $this->db->query("SELECT * FROM rg_applicant_info WHERE APPLICANT_ID = $id")->row();
        $this->load->view('application_form/admin/applicant_view', $data);
    }

    public function step_seven() {
        $data['pageTitle'] = 'Payment Information';
        //$data['applicant_details'] = $this->db->query("SELECT * FROM rg_applicant_info WHERE APPLICANT_ID = $id")->row();
        $data['content_view_page'] = 'application_form/application_payment_step_seven';
        $this->template_applicant->display($data);
    }

    public function actionToApplicant() {
        $this->load->view('application_form/admin/actionToApplicant');
    }

    public function stepEight() {
        $data['pageTitle'] = 'Registration Completed';
        $data['breadcrumbs'] = array(
            'Registration Completed' => '#'
        );
        $data['content_view_page'] = 'application_form/admin/registrationCompleted';
        $this->template->display($data);
    }

    public function stepEightModal() {
        $this->load->view('application_form/admin/registrationCompletedModal');
    }

}
