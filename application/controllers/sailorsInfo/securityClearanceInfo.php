<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SecurityClearanceInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit Security clearance Information';
        $data['content_view_page'] = 'sailorsInfo/securityclearance_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
		$data['clearanceType'] = $this->utilities->findAllByAttribute("bn_clearancetype", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/securityclearance_info/create',$data);
    }
	
    public function save()
    {
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $clearance_ID= $this->input->post('clearance_ID', true);
        $CLEARED = (isset($_POST['CLEARED'])) ? 1 : 0;
        $clearanceDat= date('Y-m-d',strtotime($this->input->post('clearanceDate')));
        $validUpTo= date('Y-m-d',strtotime($this->input->post('validUpTo')));     
        $authorityName = $this->input->post('authorityName', true);     
        $authorityNumber = $this->input->post('authorityNumber', true);     
        $authorityDate = date('Y-m-d',strtotime($this->input->post('authorityDate')));     
        $remarks = $this->input->post('remarks', true);     

        $data = array(
            'SailorID' => $SAILOR_ID,
            'ClearanceTypeID' => $clearance_ID,
            'TranDate' => $clearanceDat,
            'ValidDate' => $validUpTo,
            'IsClearance' => $CLEARED,
            'AuthorityName' => $authorityName,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'Remarks' => $remarks,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($data, 'clearanceinfo')) { // if data inserted successfully
            echo "<div class='alert alert-success'>Security clearance Inserted successfully</div>";
        } else { // if data inserted failed
            echo "<div class='alert alert-danger'>Security Clearance insert failed</div>";
        }
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function clearanceList() {
        $this->load->view("sailorsInfo/securityclearance_info/list");
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT c.ClearanceTranID,c.ClearanceTypeID, c.TranDate, c.ValidDate, c.IsClearance, c.AuthorityName, c.AuthorityNumber, c.AuthorityDate, c.Remarks,s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.NAME CLEAR_TYLE, se.NAME SHIP_ESTABLIHSMENT
                                            FROM clearanceinfo c
                                            INNER JOIN sailor s on s.SAILORID = c.SailorID
                                            INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                            INNER JOIN bn_ship_establishment se on s.SHIPESTABLISHMENTID = se.SHIP_ESTABLISHMENTID
                                            INNER JOIN bn_clearancetype ct on ct.CLEARANCE_TYPEID = c.ClearanceTypeID 
                                            WHERE c.ClearanceTranID = $id")->row();
        $data['clearanceType'] = $this->utilities->findAllByAttribute("bn_clearancetype", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/securityclearance_info/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update()
    {
        $id= $this->input->post('id', true);
        $clearance_ID= $this->input->post('clearance_ID', true);
        $CLEARED = (isset($_POST['CLEARED'])) ? 1 : 0;
        $clearanceDat= date('Y-m-d',strtotime($this->input->post('clearanceDate')));
        $validUpTo= date('Y-m-d',strtotime($this->input->post('validUpTo')));     
        $authorityName = $this->input->post('authorityName', true);     
        $authorityNumber = $this->input->post('authorityNumber', true);     
        $authorityDate = date('Y-m-d',strtotime($this->input->post('authorityDate')));     
        $remarks = $this->input->post('remarks', true);     

        $data = array(
            'ClearanceTypeID' => $clearance_ID,
            'TranDate' => $clearanceDat,
            'ValidDate' => $validUpTo,
            'IsClearance' => $CLEARED,
            'AuthorityName' => $authorityName,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'Remarks' => $remarks,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('clearanceinfo',$data, array("ClearanceTranID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>Security clearance info Update successfully</div>";
        } else { // if data inserted failed
            echo "<div class='alert alert-danger'>Security clearance info Update failed</div>";
        }
       
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT c.ClearanceTranID, c.TranDate, c.ValidDate, c.IsClearance, c.AuthorityName, c.AuthorityNumber, c.AuthorityDate, c.Remarks,s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.NAME CLEAR_TYPE
                                    FROM clearanceinfo c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_clearancetype ct on ct.CLEARANCE_TYPEID = c.ClearanceTypeID 
                                    WHERE c.ClearanceTranID = $id")->row();
        $this->load->view('sailorsInfo/securityclearance_info/view',$data);
    }
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxSecurityClearanceInfoList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'OFFICIALNUMBER', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'CLEAR_TYPE', 4 => 'IsClearance', 5 => 'TranDate', 6 => 'ValidDate', 7 => 'Remarks');

        // getting total number records without any search

        $query = $this->db->query("SELECT c.ClearanceTranID, c.TranDate, c.ValidDate, c.IsClearance, c.AuthorityName, c.AuthorityNumber, c.AuthorityDate, c.Remarks,s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.NAME CLEAR_TYPE
                                    FROM clearanceinfo c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_clearancetype ct on ct.CLEARANCE_TYPEID = c.ClearanceTypeID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT c.ClearanceTranID, c.TranDate, c.ValidDate, c.IsClearance, c.AuthorityName, c.AuthorityNumber, c.AuthorityDate, c.Remarks, s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.NAME CLEAR_TYPE
                                    FROM clearanceinfo c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_clearancetype ct on ct.CLEARANCE_TYPEID = c.ClearanceTypeID
                                WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR c.TranDate LIKE '" . $requestData['search']['value']. "%' OR c.ValidDate LIKE '" . $requestData['search']['value']. "%' OR ct.NAME LIKE '" . $requestData['search']['value'] . 
                                "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT c.ClearanceTranID, c.TranDate, c.ValidDate, c.IsClearance, c.AuthorityName, c.AuthorityNumber, c.AuthorityDate, c.Remarks, s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, ct.NAME CLEAR_TYPE
                                    FROM clearanceinfo c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_clearancetype ct on ct.CLEARANCE_TYPEID = c.ClearanceTypeID  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();

            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->RANK_NAME;
            $nestedData[] = $row->CLEAR_TYPE;
            $nestedData[] = ($row->IsClearance == 1)? "Yes" : "NO";
            $nestedData[] = date("d/m/Y", strtotime($row->TranDate));
            $nestedData[] = date("d/m/Y", strtotime($row->ValidDate));
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('sailorsInfo/securityClearanceInfo/view/' . $row->ClearanceTranID) .'" title="View Security Clearance Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink" href="'.site_url('sailorsInfo/securityClearanceInfo/edit/' . $row->ClearanceTranID) .'" title="Edit Security Clearance Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->ClearanceTranID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="ClearanceTranID" data-tbl="clearanceinfo"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file NomineeInfo.php */
/* Location: ./application/controllers/sailorsInfo/NomineeInfo.php */