<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BranchChange extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Branch Change';
        $data['content_view_page'] = 'sailorsInfo/branch_change/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", "DESC");
        $data['branch'] = $this->utilities->findAllByAttribute("bn_branch", array("ACTIVE_STATUS" => 1));
        $data['area'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $this->load->view('sailorsInfo/branch_change/create', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function saveBranch() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $CURRENT_BRANCH_ID = $this->input->post('CURR_BRANCH_ID', true);
        $CURRENT_RANK_ID = $this->input->post('CURR_RANK_ID', true);
        $BRANCH_ID = $this->input->post('BRANCH_ID', true);
        $RANK_ID = $this->input->post('RANK_ID', true);
        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID', true);
        $DAO_ID = $this->input->post('DAO_ID', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $authorityNo = $this->input->post('authorityNo', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $changeDate = date('Y-m-d', strtotime($this->input->post('changeDate', true)));

        $ship = $this->utilities->findByAttribute("bn_ship_establishment", array("SHIP_ESTABLISHMENTID" => $SHIP_ESTABLISHMENT_ID));
        // checking if trade with this name is already exist
        $check = $this->utilities->hasInformationByThisId("branchchange", array('CURRENT_RANK_ID' => $CURRENT_RANK_ID, 'LAST_RANK_ID' => $RANK_ID,));
        if (empty($check)) {// if trade name available
            $data = array(
                'CURRENT_BRANCH_ID' => $CURRENT_BRANCH_ID,
                'LAST_BRANCH_ID' => $BRANCH_ID,
                'CURRENT_RANK_ID' => $CURRENT_RANK_ID,
                'LAST_RANK_ID' => $RANK_ID,
                'SHIP_ID' => $SHIP_ESTABLISHMENT_ID,
                'SAILOR_ID' => $SAILOR_ID,
                'CHANGE_DATE' => $changeDate,
                'DAO_ID' => $DAO_ID,
                'DAO_NUMBER' => $DAO->DAO_NO,
                'AUTHORITY_NO' => $authorityNo,
                'AUTHORITY_DATE' => $authorityDate,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'branchchange')) { // if data inserted successfully
                
                $sailorInfo = array(
                    'BRANCHID' => $CURRENT_RANK_ID,
                    'RANKID' => $RANK_ID,
                    'SHIPESTABLISHMENTID' =>$SHIP_ESTABLISHMENT_ID,
                    'ZONEID' =>$ship->ZONE_ID,
                    'AREAID' =>$ship->AREA_ID
                );
                if($this->utilities->updateData('sailor', $sailorInfo, array("SAILORID" => $SAILOR_ID))){
                    echo "<div class='alert alert-success'>Branch Change successfully</div>";
                }                
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Branch Change insert failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Branch Change Already Exist</div>";
        }
    }

    /*     * 6
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */
    function branchList() {
        $data['result'] = $this->utilities->findAllByAttributeWithJoin("bn_trade", "bn_branch", "BRANCH_ID", "BRANCH_ID", "BRANCH_NAME", "", "INNER");
//               echo "<pre>";print_r($data['result']);exit;
        $this->load->view("sailorsInfo/branch_change/branch_change_list", $data);
    }

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT b.*, b.BRANCH_CHANGE_ID, b.CHANGE_DATE ,s.SAILORID ,s.OFFICIALNUMBER, s.FULLNAME, cr.RANK_NAME CURR_RANK, lr.RANK_NAME LAST_RANK, se.AREA_ID, se.NAME SHIP_EST,b.DAO_NUMBER,b.AUTHORITY_NO,b.AUTHORITY_DATE,DATE_FORMAT(s.POSTINGDATE,'%Y-%m-%d') POSTING_DATE,pu.NAME POSTING_UNIT_NAME
                                    FROM branchchange b
                                    JOIN sailor s on s.SAILORID = b.SAILOR_ID
                                    JOIN bn_rank cr on cr.RANK_ID = b.CURRENT_RANK_ID
                                    JOIN bn_rank lr on lr.RANK_ID = b.LAST_RANK_ID
                                    JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = b.SHIP_ID
                                    JOIN bn_posting_unit pu  on s.POSTINGUNITID = pu.POSTING_UNITID
                                    WHERE b.BRANCH_CHANGE_ID = $id")->row();
        //echo "<pre>";print_r($data['result']);exit;

        $data['dao'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", "DESC");
        $data['branch'] = $this->utilities->findAllByAttribute("bn_branch", array("ACTIVE_STATUS" => 1));
        $data['area'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEstablishment'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['rank'] = $this->utilities->findAllByAttribute("bn_rank", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/branch_change/edit', $data);
    }

    /*
     * @methodName Update()LAST_BRANCH_ID
     * @access
     * @author      Reazul Islam <reazul@atilimited.net>
     * @param  none
     * @return  //
     */

    public function updateBranchChange() {
      // echo "<pre>";print_r($_POST);exit;
        $id = $this->input->post('id', true);
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $BRANCH_ID = $this->input->post('BRANCH_ID', true);
        $RANK_ID = $this->input->post('RANK_ID', true);
        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID', true);
        $DAO_ID = $this->input->post('DAO_ID', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        $authorityNo = $this->input->post('authorityNo', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $changeDate = date('Y-m-d', strtotime($this->input->post('changeDate', true)));
        $ship = $this->utilities->findByAttribute("bn_ship_establishment", array("SHIP_ESTABLISHMENTID" => $SHIP_ESTABLISHMENT_ID));
        // checking if trade with this name is already exist
        $data = array(
            'LAST_BRANCH_ID' => $BRANCH_ID,
            'LAST_RANK_ID' => $RANK_ID,
            'SHIP_ID' => $SHIP_ESTABLISHMENT_ID,
            'CHANGE_DATE' => $changeDate,
            'DAO_ID' => $DAO_ID,
            'DAO_NUMBER' => $DAO->DAO_NO,
            'AUTHORITY_NO' => $authorityNo,
            'AUTHORITY_DATE' => $authorityDate,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a"));
        if ($this->utilities->updateData('branchchange', $data, array("BRANCH_CHANGE_ID" => $id))) { // if data inserted successfully
            $sailorInfo = array(
                'BRANCHID' => $BRANCH_ID,
                'RANKID' => $RANK_ID,
                'SHIPESTABLISHMENTID' =>$SHIP_ESTABLISHMENT_ID,
                'ZONEID' =>$ship->ZONE_ID,
                'AREAID' =>$ship->AREA_ID
            );
            if($this->utilities->updateData('sailor', $sailorInfo, array("SAILORID" => $SAILOR_ID))){
                echo "<div class='alert alert-success'>Branch Change successfully</div>";
            } 
        } else { // if data inserted failed
            echo "<div class='alert alert-danger'>Branch Change Update failed</div>";
        }
    }
    

    /**
     * @access      public
     * @param       id
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT b.BRANCH_CHANGE_ID, b.CHANGE_DATE ,s.OFFICIALNUMBER, s.FULLNAME, cr.RANK_NAME CURR_RANK, lr.RANK_NAME LAST_RANK, se.NAME SHIP_EST,b.DAO_NUMBER,b.AUTHORITY_NO,b.AUTHORITY_DATE,DATE_FORMAT(s.POSTINGDATE,'%Y-%m-%d') POSTING_DATE,pu.NAME POSTING_UNIT_NAME
                                    FROM branchchange b
                                    JOIN sailor s on s.SAILORID = b.SAILOR_ID
                                    JOIN bn_rank cr on cr.RANK_ID = b.CURRENT_RANK_ID
                                    JOIN bn_rank lr on lr.RANK_ID = b.LAST_RANK_ID
                                    JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = b.SHIP_ID
                                    JOIN bn_posting_unit pu  on s.POSTINGUNITID = pu.POSTING_UNITID
                                    WHERE b.BRANCH_CHANGE_ID = $id")->row();
        //echo "<pre>";print_r($data['viewdetails']);exit;
        $this->load->view('sailorsInfo/branch_change/view', $data);
    }

    /**
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View Data list
     */
    function ajaxBranchChange() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'b.BRANCH_CHANGE_ID', 1 => 's.OFFICIALNUMBER', 2 => 's.FULLNAME', 3 => 'cr.RANK_NAME', 4 => 'lr.RANK_NAME', 5 => 'b.CHANGE_DATE', 6 => 'se.NAME', 7 => 'b.BRANCH_CHANGE_ID');

        // getting total number records without any search

        $query = $this->db->query("SELECT b.BRANCH_CHANGE_ID, b.CHANGE_DATE ,s.OFFICIALNUMBER, s.FULLNAME, cr.RANK_NAME CURR_RANK, lr.RANK_NAME LAST_RANK, se.NAME SHIP_EST
                                    FROM branchchange b
                                    JOIN sailor s on s.SAILORID = b.SAILOR_ID
                                    JOIN bn_rank cr on cr.RANK_ID = b.CURRENT_RANK_ID
                                    JOIN bn_rank lr on lr.RANK_ID = b.LAST_RANK_ID
                                    JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = b.SHIP_ID
                                    WHERE s.SAILORSTATUS = 1")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT b.BRANCH_CHANGE_ID, b.CHANGE_DATE ,s.OFFICIALNUMBER, s.FULLNAME, cr.RANK_NAME CURR_RANK, lr.RANK_NAME LAST_RANK, se.NAME SHIP_EST
                                    FROM branchchange b
                                    JOIN sailor s on s.SAILORID = b.SAILOR_ID
                                    JOIN bn_rank cr on cr.RANK_ID = b.CURRENT_RANK_ID
                                    JOIN bn_rank lr on lr.RANK_ID = b.LAST_RANK_ID
                                    JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = b.SHIP_ID
                                    WHERE s.SAILORSTATUS = 1 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR cr.RANK_NAME LIKE '" . $requestData['search']['value'] .
                            "%' OR lr.RANK_NAME LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT b.BRANCH_CHANGE_ID, b.CHANGE_DATE ,s.OFFICIALNUMBER, s.FULLNAME, cr.RANK_NAME CURR_RANK, lr.RANK_NAME LAST_RANK, se.NAME SHIP_EST
                                    FROM branchchange b
                                    JOIN sailor s on s.SAILORID = b.SAILOR_ID
                                    JOIN bn_rank cr on cr.RANK_ID = b.CURRENT_RANK_ID
                                    JOIN bn_rank lr on lr.RANK_ID = b.LAST_RANK_ID
                                    JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = b.SHIP_ID
                                    WHERE s.SAILORSTATUS = 1
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->CURR_RANK;
            $nestedData[] = $row->LAST_RANK;
            $nestedData[] = date("Y-m-d", strtotime($row->CHANGE_DATE));
            $nestedData[] = $row->SHIP_EST;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('sailorsInfo/branchChange/view/' . $row->BRANCH_CHANGE_ID) . '" title="View Branch Change" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs modalLink" data-modal-size="modal-lg" href="' . site_url('sailorsInfo/branchChange/edit/' . $row->BRANCH_CHANGE_ID) . '" title="Edit Branch Change" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->BRANCH_CHANGE_ID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="BRANCH_CHANGE_ID" data-tbl="branchchange"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file branchChange.php */
/* Location: ./application/controllers/sailorsInfo/branchChange.php */