<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OverweightInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Over weight Setup';
        $data['content_view_page'] = 'sailorsInfo/over_weight_info/overweight_index';
        $this->template->display($data);
    }


    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxOverWeightList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'OFFICIALNUMBER', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'WEIGHT', 4 => 'SHIP_ESTABLISHMENT', 5 => 'DATE_OF_RETURN');

        // getting total number records without any search

        $query = $this->db->query("SELECT ow.OVER_WEIGHT_ID, ow.SAILOR_ID, ow.DATE_OF_RETURN, ow.WEIGHT, ow.AUTHORITY_NUMBER, ow.AUTHORITY_DATE, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                FROM overweight ow
                                INNER JOIN sailor s on s.SAILORID = ow.SAILOR_ID
                                INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = ow.SHIP_ID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT ow.OVER_WEIGHT_ID, ow.SAILOR_ID, ow.DATE_OF_RETURN, ow.WEIGHT, ow.AUTHORITY_NUMBER, ow.AUTHORITY_DATE, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                FROM overweight ow
                                INNER JOIN sailor s on s.SAILORID = ow.SAILOR_ID
                                INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = ow.SHIP_ID
                                WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR ow.WEIGHT LIKE '" . $requestData['search']['value']. "%' OR br.RANK_NAME LIKE '" . $requestData['search']['value']. "%' OR se.NAME LIKE '" . $requestData['search']['value'] .
                                "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT ow.OVER_WEIGHT_ID, ow.SAILOR_ID, ow.DATE_OF_RETURN, ow.WEIGHT, ow.AUTHORITY_NUMBER, ow.AUTHORITY_DATE, s.OFFICIALNUMBER,s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                FROM overweight ow
                                INNER JOIN sailor s on s.SAILORID = ow.SAILOR_ID
                                INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = ow.SHIP_ID  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();

            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->RANK_NAME;
            $nestedData[] = $row->WEIGHT;
            $nestedData[] = $row->SHIP_ESTABLISHMENT;
            $nestedData[] = date('d/m/Y', strtotime($row->DATE_OF_RETURN));
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('sailorsInfo/overweightInfo/view/' . $row->OVER_WEIGHT_ID) .'" title="View over weight" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink" href="'.site_url('sailorsInfo/overweightInfo/edit/' . $row->OVER_WEIGHT_ID) .'" title="Edit over weight" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->OVER_WEIGHT_ID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="OVER_WEIGHT_ID" data-tbl="overweight"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['shipEstablishment'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/over_weight_info/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {
        $dateOfReturn= date('Y-m-d', strtotime($this->input->post('dateOfReturn')));
        $authorityNo= $this->input->post('authorityNo', true);
        $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate')));
        $SHIP_ID= $this->input->post('SHIP_ID', true);
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $overweight= $this->input->post('OverWeight', true);

        // checking if same information with this name is already exist
        $check = $this->utilities->hasInformationByThisId("overweight", array('SAILOR_ID' => $SAILOR_ID, 'DATE_OF_RETURN' => $dateOfReturn, 'WEIGHT' => $overweight, 'SHIP_ID' => $SHIP_ID, 'AUTHORITY_NUMBER' => $authorityNo, 'AUTHORITY_DATE' => $authorityDate));
        if (empty($check)) {// if same information available
            $data = array(
                'SAILOR_ID' => $SAILOR_ID,
                'DATE_OF_RETURN' => $dateOfReturn,
                'WEIGHT' => $overweight,
                'SHIP_ID' => $SHIP_ID,
                'AUTHORITY_NUMBER' => $authorityNo,
                'AUTHORITY_DATE' => $authorityDate,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'overweight')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Over weight inserted successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Over weight  insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Over weight  Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return
     */

    function overWeightList() {
        $this->load->view("sailorsInfo/over_weight_info/overweight_list");
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findBySailorNameRank("overweight","OVER_WEIGHT_ID", $id);
        $data['shipEstablishment'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/over_weight_info/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update()
    {
        $id= $this->input->post('id', true);
        $dateOfReturn= date('Y-m-d', strtotime($this->input->post('dateOfReturn')));
        $authorityNo= $this->input->post('authorityNo', true);
        $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate')));
        $SHIP_ID= $this->input->post('SHIP_ID', true);
        $overweight= $this->input->post('OverWeight', true);

        $data = array(
            'DATE_OF_RETURN' => $dateOfReturn,
            'WEIGHT' => $overweight,
            'SHIP_ID' => $SHIP_ID,
            'AUTHORITY_NUMBER' => $authorityNo,
            'AUTHORITY_DATE' => $authorityDate,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('overweight',$data, array("OVER_WEIGHT_ID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>Over weight Update successfully</div>";
        } else { // if data inserted failed
            echo "<div class='alert alert-danger'>Over weight Update failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT ow.OVER_WEIGHT_ID, ow.SAILOR_ID, ow.DATE_OF_RETURN, ow.WEIGHT, ow.AUTHORITY_NUMBER, ow.AUTHORITY_DATE, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                FROM overweight ow
                                INNER JOIN sailor s on s.SAILORID = ow.SAILOR_ID
                                INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = ow.SHIP_ID
                                WHERE ow.OVER_WEIGHT_ID = $id")->row();
        $this->load->view('sailorsInfo/over_weight_info/view',$data);
    }

}


/* End of file overweightInfo.php */
/* Location: ./application/controllers/sailorsInfo/overweightInfo.php */
