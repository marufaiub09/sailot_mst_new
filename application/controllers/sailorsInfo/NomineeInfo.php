<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NomineeInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit Nominee Information';
        $data['content_view_page'] = 'sailorsInfo/nominee_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
		$data['relation'] = $this->utilities->findAllByAttributeWithOrderBy("bn_relation", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('sailorsInfo/nominee_info/create',$data);
    }
	
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {       
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $nomineeName= $this->input->post('nomineeName', true);
        $RELATION_ID= $this->input->post('RELATION_ID', true);     
        $percentage = $this->input->post('percentage', true);     

        // checking if Visit information with this name is already exist
        $check = $this->utilities->hasInformationByThisId("nominee", array('SailorID' => $SAILOR_ID, 'Name' => $nomineeName, 'RelationID' => $RELATION_ID,'Percentage' => $percentage));
        if (empty($check)) {// if Visit information name available
            $data = array(
                'SailorID' => $SAILOR_ID,
                'Name' => $nomineeName,
                'RelationID' => $RELATION_ID,
                'Percentage' => $percentage,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'nominee')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Nominee information Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Nominee information Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Nominee information Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function nomineeList() {
        $this->load->view("sailorsInfo/nominee_info/list");
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->utilities->findBySailorInfoParticulars("nominee","NomineeID", $id);
        $data['relation'] = $this->utilities->findAllByAttributeWithOrderBy("bn_relation", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('sailorsInfo/nominee_info/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update()
    {
        $id= $this->input->post('id', true);
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $nomineeName= $this->input->post('nomineeName', true);
        $RELATION_ID= $this->input->post('RELATION_ID', true);     
        $percentage = $this->input->post('percentage', true);     

        // checking if nominee information with this name is already exist
        $check = $this->utilities->hasInformationByThisId("nominee", array('SailorID' => $SAILOR_ID, 'Name' => $nomineeName, 'RelationID' => $RELATION_ID,'Percentage' => $percentage));
        if (empty($check)) {// if nominee information name available
            $data = array(
                'Name' => $nomineeName,
                'RelationID' => $RELATION_ID,
                'Percentage' => $percentage,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('nominee',$data, array("NomineeID" => $id))) { // if data update successfully
                echo "<div class='alert alert-success'>Nominee information Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Nominee information update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Nominee information Already Exist</div>";
        }
    }

    /**
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT n.NomineeID, n.SailorID, n.Name, n.RelationID, n.Percentage, s.OFFICIALNUMBER, s.FULLNAME, r.NAME RELATION_NAME, br.RANK_NAME
                                FROM nominee n 
                                INNER JOIN sailor s on s.SAILORID = n.SailorID
                                INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                INNER JOIN bn_relation r on r.RELATION_ID = n.RelationID
                                WHERE n.NomineeID = $id")->row();
        $this->load->view('sailorsInfo/nominee_info/view',$data);
    }
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxNomieeList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'OFFICIALNUMBER', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'Name', 4 => 'RELATION_NAME', 5 => 'Percentage');

        // getting total number records without any search

        $query = $this->db->query("SELECT n.NomineeID, n.SailorID, n.Name, n.RelationID, n.Percentage, s.OFFICIALNUMBER, s.FULLNAME, r.NAME RELATION_NAME, br.RANK_NAME
                                FROM nominee n 
                                INNER JOIN sailor s on s.SAILORID = n.SailorID
                                INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                INNER JOIN bn_relation r on r.RELATION_ID = n.RelationID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT n.NomineeID, n.Name, n.Percentage, s.OFFICIALNUMBER, s.FULLNAME, r.NAME RELATION_NAME, br.RANK_NAME
                                FROM nominee n 
                                INNER JOIN sailor s on s.SAILORID = n.SailorID
                                INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                INNER JOIN bn_relation r on r.RELATION_ID = n.RelationID
                                WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR n.Name LIKE '" . $requestData['search']['value']. "%' OR br.RANK_NAME LIKE '" . $requestData['search']['value']. "%' OR r.NAME LIKE '" . $requestData['search']['value'] . 
                                "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT n.NomineeID, n.SailorID, n.Name, n.RelationID, n.Percentage, s.OFFICIALNUMBER, s.FULLNAME, r.NAME RELATION_NAME, br.RANK_NAME
                                    FROM nominee n 
                                    INNER JOIN sailor s on s.SAILORID = n.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_relation r on r.RELATION_ID = n.RelationID  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();

            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->RANK_NAME;
            $nestedData[] = $row->Name;
            $nestedData[] = $row->RELATION_NAME;
            $nestedData[] = $row->Percentage;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('sailorsInfo/NomineeInfo/view/' . $row->NomineeID) .'" title="View Nominee Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink" href="'.site_url('sailorsInfo/NomineeInfo/edit/' . $row->NomineeID) .'" title="Edit Nominee Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->NomineeID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="NomineeID" data-tbl="nominee"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file NomineeInfo.php */
/* Location: ./application/controllers/sailorsInfo/NomineeInfo.php */