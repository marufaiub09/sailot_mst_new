<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ForeignVisitInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Foreign Visit Information';
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['visitClass'] = $this->utilities->findAllByAttributeWithOrderBy("bn_visitclassification", array("ACTIVE_STATUS" => 1),"VISIT_CLASSIFICATIONID");
        $data['visitInfo'] = $this->utilities->findAllByAttribute("bn_visitinformation", array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'sailorsInfo/foreign_visit_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['ac_type'] = "create";
        $data['visitClass'] = $this->utilities->findAllByAttributeWithOrderBy("bn_visitclassification", array("ACTIVE_STATUS" => 1),"VISIT_CLASSIFICATIONID");
        $data['country'] = $this->utilities->findAllByAttributeWithOrderBy("bn_country", array("ACTIVE_STATUS" => 1),"NAME");
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['trainingType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyexam_hierarchy", array("ACTIVE_STATUS" => 1, "EXAM_TYPE" => 2),"NAME");
        $data['examGrade'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', '---Select---', 'EXAM_GRADEID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['examResult'] = $this->utilities->dropdownFromTableWithCondition('bn_exam_result', '---Select---', 'EXAM_RESULT_ID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['examGrade_one'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
        $data['examResul_one'] = $this->utilities->findAllByAttribute("bn_exam_result", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/        
        $data['content_view_page'] = 'sailorsInfo/foreign_visit_info/create';
        $this->template->display($data);
    }
    
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {
        $SAILOR_NO = $this->input->post('OFFICIAL_NO', true);
        for ($i=0; $i < count($SAILOR_NO) ; $i++) { 
            $row = $this->utilities->findByAttribute("sailor", array('OFFICIALNUMBER' => $SAILOR_NO[$i]));
            $sailorId[] = $row->SAILORID;
        }
        /*start foreignvisit part*/

        $VISIT_CI_ID= $this->input->post('VISIT_CI_ID', true);
        $VISIT_ID= $this->input->post('VISIT_ID', true);
        $remarks= $this->input->post('remarks', true);
        $COURSE_ID= $this->input->post('TRAINING_NAME', true);
        /*end foreignvisit part*/

        /*start visitcountry part*/

        $COUNTRY_ID= $this->input->post('COUNTRY_ID', true);
        $startDate= date('Y-m-d', strtotime($this->input->post('startDate', true)));
        $endDate= date('Y-m-d', strtotime($this->input->post('endDate', true)));
        $durations= $this->input->post('durations', true);
        /*end visitcountry part*/

        /*end coursetran part*/
        $s_endDate = $this->input->post('s_endDate', true);
        $s_durations = $this->input->post('s_durations', true);
        $GRADE = $this->input->post('GRADE', true);
        $MARKS = $this->input->post('MARKS', true);
        $PERCENTAGE = $this->input->post('PERCENTAGE', true);
        $RESULT = $this->input->post('RESULT', true);
        $SENIORITY = $this->input->post('SENIORITY', true);

        $authorityNumber= $this->input->post('authorityNumber', true);
        if($authorityNumber != ''){
            $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
            $DAO_ID= $this->input->post('DAO_ID', true);
            $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        }else{
            $authorityDate= NULL;
            $DAO_ID= NULL;
        }
        //foreignvisit insert part
        $foreignvisit = array(
            'VisitClassID' => $VISIT_CI_ID,
            'VisitInfoID' => $VISIT_ID,
            'CourseID' => $COURSE_ID,
            'StartDate' => $startDate,
            'EndDate' => $endDate,
            'Duration' => $durations,
            'Remarks' => $remarks,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($foreignVisitID = $this->utilities->insert('foreignvisit', $foreignvisit)) { // if foreignvisit inserted successfully
            /*visitcountry insert part*/
            $visitcountry = array(
                'ForeignVisitID' => $foreignVisitID,
                'CountryID' => $COUNTRY_ID,
                'StartDate' => $startDate,
                'EndDate' => $endDate,
                'Duration' => $durations,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($visitCountryID = $this->utilities->insertData($visitcountry, 'visitcountry')) { // if foreignvisit inserted successfully

            }
            /*end visitcountry insert part*/
            for ($i=0; $i < count($SAILOR_NO) ; $i++) {
                /*coursetran insert part*/
                $coursetran = array(
                    'CourseID' => $COURSE_ID,
                    'SailorID' => $sailorId[$i],
                    'ExamGradeID' => $GRADE[$i],
                    'ExamResultID' => $RESULT[$i],
                    'StartDate' => $startDate,
                    'EndDate' => $s_endDate[$i],
                    'Duration' => $s_durations[$i],
                    'Mark' => $MARKS[$i],
                    'Percentage' => $PERCENTAGE[$i],
                    'Seniority' => $SENIORITY[$i],
                    'AuthorityNumber' => $authorityNumber,
                    'AuthorityDate' => $authorityDate,
                    'DAOID' => $DAO_ID,
                    'DAONumber' => (empty($DAO->DAO_NO))? '': $DAO->DAO_NO,
                    'IsForeign' => 1,
                    'RefForeignVisitID' => $foreignVisitID,
                    'Remarks' => "Foreign Course",
                    'CRE_BY' => $this->user_session["USER_ID"]
                );
                if($courseTranId = $this->utilities->insertData($coursetran, 'coursetran')){ /*end coursetran insert part*/
                    /*visitsailor insert part*/
                    $visitsailor = array(
                        'ForeignVisitID' => $foreignVisitID,
                        'SailorID' => $sailorId[$i],
                        'StartDate' => $startDate,
                        'EndDate' => $endDate,
                        'Duration' => $durations,
                        'RefCourseTranID' => $courseTranId,
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $visitSailorID = $this->utilities->insertData($visitsailor, 'visitsailor');
                    /*end visitsailor insert part*/       
                }         
            }
            echo "<div class='alert alert-success'>Foreign Visit Inserted successfully</div>";

        }else{
            echo "<div class='alert alert-success'>spectran Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function spacialList() {
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view("sailorsInfo/foreign_visit_info/list");
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT a.*, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, os.NAME SPECIALIZATION
                                  FROM spectran a 
                                  LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                  WHERE a.SpecTranID = $id")->row();
        $data['ac_type'] = "edit";
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['shipEstablishment'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['otherSpecialization'] = $this->utilities->findAllByAttribute("bn_otherspecialization", array("ACTIVE_STATUS" => 1));
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/foreign_visit_info/edit',$data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update() {
        $id= $this->input->post('id', true);
        $otherSpecialization= $this->input->post('otherSpecialization', true);
        $EffectingDate= date('Y-m-d', strtotime($this->input->post('EffectingDate', true)));
        $SHIP_ID= $this->input->post('SHIP_ID', true);
        $DAO_ID= $this->input->post('DAO_ID', true);
        $dao_no = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        if($this->input->post('authorityName', true)){
            $authorityName= $this->input->post('authorityName', true);
        }else{
            $SHIP_ESTABLISHMENT= $this->input->post('AUTHORITY_SHIP', true);  
            $authorityName = '';
        }
        $authorityNumber= $this->input->post('authorityNumber', true);
        $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate', true))) ;
      
        $data = array(
            'SpecTranDate' => $EffectingDate,
            'AuthorityName' => $authorityName,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $dao_no->DAO_NO,
            'OtherSpecializationID' => $otherSpecialization,
            'ShipEstablishmentID' => ($authorityName == '')? $SHIP_ESTABLISHMENT: NULL,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('spectran',$data, array("SpecTranID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>specialization Transaction Update successfully</div>";
        }else{
            echo "<div class='alert alert-success'>spectran Update Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT a.SpecTranID, a.SpecTranDate, a.AuthorityName, a.AuthorityNumber, a.AuthorityDate, a.DAONumber, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, os.NAME SPECIALIZATION
                                  FROM spectran a 
                                  LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                  WHERE a.SpecTranID = $id")->row();
        $this->load->view('sailorsInfo/foreign_visit_info/view',$data);
    }

    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function searchForeignVisitInfo(){
        $flag = 0;
        $class_ID = '';
        $info_ID = '';
        $fromDate = '';
        $toDate = '';
        $class_info_ID = '';
        $VISIT_CI_ID = $this->input->post("VISIT_CI_ID", true);
        $VISIT_ID = $this->input->post("VISIT_ID", true);
        if (isset($_POST['startDate'])) {
            $start_dt = date('Y-m-d',strtotime($this->input->post("fromDate")));
            $end_dt = date('Y-m-d',strtotime($this->input->post("toDate")));
            if($VISIT_CI_ID != '' && $VISIT_ID == ''){
                $class_ID = "fv.VisitClassID = $VISIT_CI_ID AND (fv.StartDate >= '$start_dt' AND fv.EndDate <= '$end_dt')";
            }
            else if ($VISIT_ID != '' && $VISIT_CI_ID == '') {
                $info_ID = "fv.VisitInfoID = $VISIT_ID AND (fv.StartDate >= '$start_dt' AND fv.EndDate <= '$end_dt')";
            }
            else if ($VISIT_ID != '' && $VISIT_CI_ID != '') {
                $class_info_ID = "fv.VisitClassID = $VISIT_CI_ID AND fv.VisitInfoID = $VISIT_ID AND (fv.StartDate >= '$start_dt' AND fv.EndDate <= '$end_dt')";
            } 
        }else{
            if($VISIT_CI_ID != '' && $VISIT_ID == ''){
                $class_ID = "fv.VisitClassID = $VISIT_CI_ID";
            }
            else if ($VISIT_ID != '' && $VISIT_CI_ID == '') {
                $info_ID = "fv.VisitInfoID = $VISIT_ID";
            }
            else if ($VISIT_ID != '' && $VISIT_CI_ID != '') {
                $class_info_ID = "fv.VisitClassID = $VISIT_CI_ID AND fv.VisitInfoID = $VISIT_ID";
            }else{
                $flag = 1;
                $data['foreignVisit'] = $this->db->query("SELECT fv.*, vc.NAME VC_NAME, vi.NAME VI_NAME, vi.DESCR VI_DESCR, country.NAME COUNTRY_NAME
                                                FROM foreignvisit fv
                                                INNER JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = fv.VisitClassID
                                                INNER JOIN bn_visitinformation vi on vi.VISIT_INFO_ID = fv.VisitInfoID
                                                INNER JOIN visitcountry vcountry on vcountry.ForeignVisitID = fv.ForeignVisitID
                                                INNER JOIN bn_country country on country.COUNTRY_ID = vcountry.CountryID
                                                ")->result();
            }        
        }
        if ($flag == 0) {
            $data['foreignVisit'] = $this->db->query("SELECT fv.*, vc.NAME VC_NAME, vi.NAME VI_NAME, vi.DESCR VI_DESCR, country.NAME COUNTRY_NAME
                                                    FROM foreignvisit fv
                                                    INNER JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = fv.VisitClassID
                                                    INNER JOIN bn_visitinformation vi on vi.VISIT_INFO_ID = fv.VisitInfoID
                                                    INNER JOIN visitcountry vcountry on vcountry.ForeignVisitID = fv.ForeignVisitID
                                                    INNER JOIN bn_country country on country.COUNTRY_ID = vcountry.CountryID
                                                    WHERE $class_ID $info_ID $class_info_ID
                                                    ")->result();            
        }
        $this->load->view('sailorsInfo/foreign_visit_info/search_visit_list', $data);
    }
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxForeignVisitList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'vi.NAME', 1 => 'vi.DESCR', 3 => 'country.NAME', 4 => 'StartDate', 5 => 'EndDate', 6 => 'Duration');

        // getting total number records without any search

        $query = $this->db->query("SELECT fv.*, vc.NAME VC_NAME, vi.NAME VI_NAME, vi.DESCR VI_DESCR, country.NAME COUNTRY_NAME
                                    FROM foreignvisit fv
                                    INNER JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = fv.VisitClassID
                                    INNER JOIN bn_visitinformation vi on vi.VISIT_INFO_ID = fv.VisitInfoID
                                    INNER JOIN visitcountry vcountry on vcountry.ForeignVisitID = fv.ForeignVisitID
                                    INNER JOIN bn_country country on country.COUNTRY_ID = vcountry.CountryID")->num_rows();
       
        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT fv.*, vc.NAME VC_NAME, vi.NAME VI_NAME, vi.DESCR VI_DESCR, country.NAME COUNTRY_NAME
                                    FROM foreignvisit fv
                                    INNER JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = fv.VisitClassID
                                    INNER JOIN bn_visitinformation vi on vi.VISIT_INFO_ID = fv.VisitInfoID
                                    INNER JOIN visitcountry vcountry on vcountry.ForeignVisitID = fv.ForeignVisitID
                                    INNER JOIN bn_country country on country.COUNTRY_ID = vcountry.CountryID
                                    WHERE vc.NAME LIKE '" . $requestData['search']['value'] ."%' OR vi.NAME LIKE '" . $requestData['search']['value'] . "%' OR vi.DESCR LIKE '" . $requestData['search']['value']. 
                                    "%' OR country.NAME LIKE '" . $requestData['search']['value']. "%' OR fv.StartDate LIKE '" . $requestData['search']['value']. "%' OR fv.EndDate LIKE '" . $requestData['search']['value'].
                                    "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

        } else {

            $query = $this->db->query("SELECT fv.*, vc.NAME VC_NAME, vi.NAME VI_NAME, vi.DESCR VI_DESCR, country.NAME COUNTRY_NAME
                                    FROM foreignvisit fv
                                    INNER JOIN bn_visitclassification vc on vc.VISIT_CLASSIFICATIONID = fv.VisitClassID
                                    INNER JOIN bn_visitinformation vi on vi.VISIT_INFO_ID = fv.VisitInfoID
                                    INNER JOIN visitcountry vcountry on vcountry.ForeignVisitID = fv.ForeignVisitID
                                    INNER JOIN bn_country country on country.COUNTRY_ID = vcountry.CountryID
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->VI_NAME;
            $nestedData[] = $row->VI_DESCR;
            $nestedData[] = $row->COUNTRY_NAME;
            $nestedData[] = date("d/m/Y", strtotime($row->StartDate));
            $nestedData[] = date("d/m/Y", strtotime($row->EndDate));
            $nestedData[] = $row->Duration;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('sailorsInfo/foreignVisitInfo/view/' . $row->ForeignVisitID) .'" title="View Specialization Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink cm" href="'.site_url('sailorsInfo/foreignVisitInfo/edit/' . $row->ForeignVisitID) .'" title="Edit Specialization Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->ForeignVisitID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="ForeignVisitID" data-tbl="spectran"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file foreignVisitInfo.php */
/* Location: ./application/controllers/sailorsInfo/foreignVisitInfo.php */