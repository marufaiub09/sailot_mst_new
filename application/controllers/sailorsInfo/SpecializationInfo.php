<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SpecializationInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Specialization Information';
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'sailorsInfo/specialization_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['ac_type'] = "create";
        $data['shipEstablishment'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['otherSpecialization'] = $this->utilities->findAllByAttributeWithOrderBy("bn_otherspecialization", array("ACTIVE_STATUS" => 1),"NAME");
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view('sailorsInfo/specialization_info/create',$data);
    }
    
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $otherSpecialization= $this->input->post('otherSpecialization', true);
        $EffectingDate= date('Y-m-d', strtotime($this->input->post('EffectingDate', true)));
        $SHIP_ID= $this->input->post('SHIP_ID', true);
        $DAO_ID= $this->input->post('DAO_ID', true);
        $dao_no = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        if($this->input->post('authorityName', true)){
            $authorityName= $this->input->post('authorityName', true);
        }else{
            $SHIP_ESTABLISHMENT= $this->input->post('AUTHORITY_SHIP', true);  
            $authorityName = '';
        }
        $authorityNumber= $this->input->post('authorityNumber', true);
        $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate', true))) ;
      
        $data = array(
            'SailorID' => $SAILOR_ID,
            'SpecTranDate' => $EffectingDate,
            'AuthorityName' => $authorityName,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $dao_no->DAO_NO,
            'OtherSpecializationID' => $otherSpecialization,
            'ShipEstablishmentID' => ($authorityName == '')? $SHIP_ESTABLISHMENT: NULL,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($data, 'spectran')) { // if data inserted successfully
            echo "<div class='alert alert-success'>specialization Transaction add successfully</div>";
        }else{
            echo "<div class='alert alert-success'>spectran Inserted Failed</div>";
        }
        
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function spacialList() {
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view("sailorsInfo/specialization_info/list");
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT a.*, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, os.NAME SPECIALIZATION
                                  FROM spectran a 
                                  LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                  WHERE a.SpecTranID = $id")->row();
        $data['ac_type'] = "edit";
        $data['shipEstablishment'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['otherSpecialization'] = $this->utilities->findAllByAttributeWithOrderBy("bn_otherspecialization", array("ACTIVE_STATUS" => 1),"NAME");
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view('sailorsInfo/specialization_info/edit',$data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update() {
        $id= $this->input->post('id', true);
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $otherSpecialization= $this->input->post('otherSpecialization', true);
        $EffectingDate= date('Y-m-d', strtotime($this->input->post('EffectingDate', true)));
        $SHIP_ID= $this->input->post('SHIP_ID', true);
        $DAO_ID= $this->input->post('DAO_ID', true);
        $dao_no = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        if($this->input->post('authorityName', true)){
            $authorityName= $this->input->post('authorityName', true);
        }else{
            $SHIP_ESTABLISHMENT= $this->input->post('AUTHORITY_SHIP', true);  
            $authorityName = '';
        }
        $authorityNumber= $this->input->post('authorityNumber', true);
        $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate', true))) ;
      
        $data = array(
            'SpecTranDate' => $EffectingDate,
            'AuthorityName' => $authorityName,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $dao_no->DAO_NO,
            'OtherSpecializationID' => $otherSpecialization,
            'ShipEstablishmentID' => ($authorityName == '')? $SHIP_ESTABLISHMENT: NULL,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('spectran',$data, array("SpecTranID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>specialization Transaction Update successfully</div>";
        }else{
            echo "<div class='alert alert-success'>spectran Update Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT a.SpecTranID, a.SpecTranDate, a.AuthorityName, a.AuthorityNumber, a.AuthorityDate, a.DAONumber, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, os.NAME SPECIALIZATION
                                  FROM spectran a 
                                  LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                  WHERE a.SpecTranID = $id")->row();
        $this->load->view('sailorsInfo/specialization_info/view',$data);
    }

    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxSpecTranList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'OFFICIALNUMBER', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'SPECIALIZATION', 4 => 'SpecTranDate', 5 => 'SHIP_ESTABLISHMENT', 6 => 'AuthorityName', 7 => 'AuthorityNumber', 8 => 'DAONumber');

        // getting total number records without any search

        $query = $this->db->query("SELECT a.SpecTranID, a.SpecTranDate, a.AuthorityName, a.AuthorityNumber, a.AuthorityDate, a.DAONumber, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, os.NAME SPECIALIZATION
                                  FROM spectran a 
                                  LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                  WHERE s.SAILORSTATUS = 1")->num_rows();

       
        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT a.SpecTranID, a.SpecTranDate, a.AuthorityName, a.AuthorityNumber, a.AuthorityDate, a.DAONumber, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, os.NAME SPECIALIZATION
                                    FROM spectran a 
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                    INNER JOIN sailor s on s.SAILORID = a.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                    WHERE s.SAILORSTATUS = 1 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] ."%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR a.SpecTranDate LIKE '" . $requestData['search']['value']. 
                                    "%' OR a.DAONumber LIKE '" . $requestData['search']['value']. "%' OR a.AuthorityName LIKE '" . $requestData['search']['value']. "%' OR a.AuthorityDate LIKE '" . $requestData['search']['value']."%' OR br.RANK_NAME LIKE '" . $requestData['search']['value']."%' OR os.NAME LIKE '" . $requestData['search']['value']. "%' OR se.NAME LIKE '" . $requestData['search']['value'] . 
                                    "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT a.SpecTranID, a.SpecTranDate, a.AuthorityName, a.AuthorityNumber, a.AuthorityDate, a.DAONumber, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, os.NAME SPECIALIZATION
                                    FROM spectran a 
                                    LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                    INNER JOIN sailor s on s.SAILORID = a.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_otherspecialization os on os.OtherSpecializationID = a.OtherSpecializationID
                                    WHERE s.SAILORSTATUS = 1
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->SPECIALIZATION;
            $nestedData[] = $row->SpecTranDate;
            $nestedData[] = $row->AuthorityName."<br><b><i>Number ".$row->AuthorityNumber."</i></b><br><b><i>Date ". $row->AuthorityDate."</i></b>";
            /*$nestedData[] = $row->AuthorityName;
            $nestedData[] = $row->authorityDate;*/
            $nestedData[] = $row->SHIP_ESTABLISHMENT;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('sailorsInfo/specializationInfo/view/' . $row->SpecTranID) .'" title="View Specialization Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink cm" href="'.site_url('sailorsInfo/specializationInfo/edit/' . $row->SpecTranID) .'" title="Edit Specialization Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->SpecTranID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="SpecTranID" data-tbl="spectran"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file SpecializationInfo.php */
/* Location: ./application/controllers/sailorsInfo/SpecializationInfo.php */