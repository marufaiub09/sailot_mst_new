<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SailorRemark extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Remarks Info Transction';
        $data['content_view_page'] = 'sailorsInfo/sailorRemarkInfo/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $this->load->view('sailorsInfo/sailorRemarkInfo/create');
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */

    function remarkUpdate() {
        $S_id = $this->input->post('SAILOR_ID', true);
        $remarks = $this->input->post('remarks', true);

        $data = array(
            'REMARKS' => $remarks
        );
//
//        $this->db->select('REMARKS');
//        $this->db->from('sailor');
//        $this->db->where('SAILORID', $S_id); //you can use another field
//        $num_results = $this->db->count_all_results();
////      var_dump($num_results);

        $this->utilities->updateData('sailor', $data, array("SAILORID" => $S_id));
        echo "<div class='alert alert-success'>Sailor Remark update successfully</div>";
    }

    /**
     * @access      public
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      
     */
    function SailorRemarkList() {
        $this->load->view("sailorsInfo/sailorRemarkInfo/list");
    }

    /**
     * @access      public
     * @param       id
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      templete
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, p.REMARKS 
        FROM sailor p
        INNER JOIN sailor s on s.SAILORID = p.SAILORID
        INNER JOIN sailor pt on pt.SAILORID = p.SAILORID
                                    WHERE p.SAILORID = $id")->row();
        $this->load->view('sailorsInfo/sailorRemarkInfo/view', $data);
    }

    function searchSailorInfoByOfficalNo() {
        $officalNumber = $this->input->post("officeNumber");
        $this->db->select('s.*,et.NAME ENTRY_TYPE_NAME,s.REMARKS, r.RANK_NAME, se.NAME SHIP_ESTABLISHMENT, pu.NAME POSTING_UNIT_NAME, DATE_FORMAT(s.POSTINGDATE,"%d-%m-%Y") POSTING_DATE');
        $this->db->from('sailor as s');
        $this->db->join('bn_posting_unit as pu', 'pu.POSTING_UNITID = s.POSTINGUNITID', 'INNER');
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID', 'INNER');
        $this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID', 'INNER');
        $this->db->join('bn_entrytype as et', 'et.ENTRY_TYPEID = s.ENTRYTYPEID', 'INNER');
        $this->db->where('s.OFFICIALNUMBER', $officalNumber);
        echo json_encode($this->db->get()->row_array());
    }

    /**
     * @param       none
     * @author      reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    function ajaxSailorRemarkList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'SAILORID', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'REMARKS', 4 => 'OFFICIALNUMBER',);

        $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, p.REMARKS 
        FROM sailor p
        INNER JOIN sailor s on s.SAILORID = p.SAILORID
        INNER JOIN sailor pt on pt.SAILORID = p.SAILORID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, p.REMARKS 
             FROM sailor p
             INNER JOIN sailor s on s.SAILORID = p.SAILORID
             INNER JOIN sailor pt on pt.SAILORID = p.SAILORID
                                WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR p.REMARKS LIKE '" .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT p.*, s.OFFICIALNUMBER, p.REMARKS 
        FROM sailor p
        INNER JOIN sailor s on s.SAILORID = p.SAILORID
        INNER JOIN sailor pt on pt.SAILORID = p.SAILORID ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->REMARKS;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('sailorsInfo/SailorRemark/view/' . $row->SAILORID) . '" title="View Sailor Remark Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file languageInfo.php */
/* Location: ./application/controllers/retirementInfo/SailorRemark.php */