<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AssessmentInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Assessment Information';
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'sailorsInfo/assessment_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
      $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
      $data['shipEstablishment'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
		  $this->load->view('sailorsInfo/assessment_info/create',$data);
    }
	
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveVI()
    {
        $assesment_year= $this->input->post('assesment_year', true);
        $SHIP_ESTABLISHMENT= $this->input->post('SHIP_ESTABLISHMENT', true);
        $OFFICIAL_NO= $this->input->post('OFFICIAL_NO', true);
        $CHAR= $this->input->post('CHAR', true);
        $EFFICIANCY= $this->input->post('EFFICIANCY', true);
        $success = 0;
        for ($i=0; $i < count($OFFICIAL_NO) ; $i++) {       
            $sailorId = $this->utilities->findByAttribute("sailor", array("OFFICIALNUMBER" => $OFFICIAL_NO[$i]));  
            $data = array(
                'AssessYear' => $assesment_year,
                'SailorID' => $sailorId->SAILORID,
                'ShipEstablishmentID' => $SHIP_ESTABLISHMENT,
                'CharacterType' => $CHAR[$i],
                'EfficiencyType' => $EFFICIANCY[$i],
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'assessment')) { // if data inserted successfully
                $success = 1;
            } 
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Assessment Inserted successfully</div>";
        }else{
            echo "<div class='alert alert-success'>Assessment Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function assessList() {
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view("sailorsInfo/assessment_info/list",$data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT a.*, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                  FROM assessment a 
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  WHERE a.AssessID = $id")->row();
        $data['shipEstablishment'] = $this->utilities->findAllByAttributeWithOrderBy("bn_ship_establishment", array("ACTIVE_STATUS" => 1),"NAME");
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $this->load->view('sailorsInfo/assessment_info/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateAssess()
    {
        $id= $this->input->post('id', true);
        $assesment_year= $this->input->post('assesment_year', true);
        $SHIP_ESTABLISHMENT= $this->input->post('SHIP_ESTABLISHMENT', true);
        $CHAR= $this->input->post('CHAR', true);
        $EFFICIANCY= $this->input->post('EFFICIANCY', true);

        $data = array(
            'AssessYear' => $assesment_year,
            'ShipEstablishmentID' => $SHIP_ESTABLISHMENT,
            'CharacterType' => $CHAR,
            'EfficiencyType' => $EFFICIANCY,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('assessment',$data, array("AssessID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>Assessment Information Update successfully</div>";
        } else { // if data inserted failed
            echo "<div class='alert alert-danger'>Assessment Information Update failed</div>";
        }
        
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT a.*, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                  FROM assessment a 
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  WHERE a.AssessID = $id")->row();
        $this->load->view('sailorsInfo/assessment_info/view',$data);
    }

    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxAssessmentList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'OFFICIALNUMBER', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'AssessYear', 4 => 'SHIP_ESTABLISHMENT', 5 => 'CharacterType', 6 => 'EfficiencyType');

        // getting total number records without any search

        $query = $this->db->query("SELECT a.AssessID, a.AssessYear, a.CharacterType, a.EfficiencyType, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                  FROM assessment a 
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                  WHERE s.SAILORSTATUS = 1")->num_rows();

       
        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT a.AssessID, a.AssessYear, a.CharacterType, a.EfficiencyType, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                    FROM assessment a 
                                    INNER JOIN sailor s on s.SAILORID = a.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID
                                    WHERE s.SAILORSTATUS = 1 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] ."%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR a.AssessYear LIKE '" . $requestData['search']['value']. "%' OR br.RANK_NAME LIKE '" . $requestData['search']['value']. "%' OR se.NAME LIKE '" . $requestData['search']['value'] . 
                                    "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT a.AssessID, a.AssessYear, a.CharacterType, a.EfficiencyType, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME
                                  FROM assessment a 
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = a.ShipEstablishmentID  
                                  WHERE s.SAILORSTATUS = 1
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        $char = array("VG","VG_", "GOOD", "FAIR", "INDIF", "BAD");
        $efficiency = array("SUPER","SAT","MOD","INFER","UT");
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->RANK_NAME;
            $nestedData[] = $row->AssessYear;
            $nestedData[] = $row->SHIP_ESTABLISHMENT;
            $nestedData[] = array_key_exists($row->CharacterType, $char) ? $char[$row->CharacterType] : '';
            $nestedData[] = array_key_exists($row->EfficiencyType, $efficiency) ? $efficiency[$row->EfficiencyType] : '';
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" data-modal-size="modal-md"  href="'.site_url('sailorsInfo/assessmentInfo/view/' . $row->AssessID) .'" title="View Assessment Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink" data-modal-size="modal-md"  href="'.site_url('sailorsInfo/assessmentInfo/edit/' . $row->AssessID) .'" title="Edit Assessment Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->AssessID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="AssessID" data-tbl="assessment"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}