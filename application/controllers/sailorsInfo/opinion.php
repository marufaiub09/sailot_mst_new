<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Opinion extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Sailor Opinion Setup';
        $data['content_view_page'] = 'sailorsInfo/opinion/opinion_index';
        $this->template->display($data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxOpinionList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'WillingNotWillingID', 1 => 'OFFICIALNUMBER', 2 => 'FULLNAME', 3 => 'RANK_NAME', 4 => 'se.NAME', 5 => 'm.NAME', 6 => 'WillingNotWillingID');

        // getting total number records without any search

        $query = $this->db->query("SELECT  w.WillingNotWillingID, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, m.NAME MISSION
									FROM willingnotwilling w 
									INNER JOIN bn_mission m on m.MISSION_ID = w.MissionID
									INNER JOIN sailor s on s.SAILORID = w.SailorID
									INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT  w.WillingNotWillingID, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, m.NAME MISSION
									FROM willingnotwilling w 
									INNER JOIN bn_mission m on m.MISSION_ID = w.MissionID
									INNER JOIN sailor s on s.SAILORID = w.SailorID
									INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                	WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR m.NAME LIKE '" . $requestData['search']['value']. "%' OR br.RANK_NAME LIKE '" . $requestData['search']['value']. "%' OR se.NAME LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . 
                                "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT  w.WillingNotWillingID, s.OFFICIALNUMBER, s.FULLNAME, se.NAME SHIP_ESTABLISHMENT, br.RANK_NAME, m.NAME MISSION
									FROM willingnotwilling w 
									INNER JOIN bn_mission m on m.MISSION_ID = w.MissionID
									INNER JOIN sailor s on s.SAILORID = w.SailorID
									INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();

            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->RANK_NAME;
            $nestedData[] = $row->SHIP_ESTABLISHMENT;
            $nestedData[] = $row->MISSION;
            $nestedData[] = '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->WillingNotWillingID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="WillingNotWillingID" data-tbl="willingnotwilling"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['mission'] = $this->utilities->findAllByAttribute("bn_mission", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/opinion/create',$data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {       
        $MISSION_ID= $this->input->post('MISSION_ID', true);    
        $OFFICIAL_NO= $this->input->post('OFFICIAL_NO', true); 
        $mission = explode("-", $MISSION_ID);
        $success = 0;
        for ($i=0; $i < count($OFFICIAL_NO); $i++) { 
        	$sailors = $this->utilities->findByAttribute('sailor', array("OFFICIALNUMBER" => $OFFICIAL_NO[$i]));
        	
        	// checking if same information with this name is already exist
	        $check = $this->utilities->hasInformationByThisId("willingnotwilling", array('SailorID' => $sailors->SAILORID, 'MissionID' => $mission[1]));
	        if (empty($check)) {// if same information available
	            $data = array(
	                'Code' => $mission[0],
	                'Opinion' => 1,
	                'MissionID' => $mission[1],
	                'SailorID' => $sailors->SAILORID,
	                'CRE_BY' => $this->user_session["USER_ID"]
	            );
	            if ($this->utilities->insertData($data, 'willingnotwilling')) { // if data inserted successfully
	                $success = 1 ;
	            }
	        }else{
	        	$willing = $this->utilities->findByAttribute('willingnotwilling', array("MissionID" => $mission[1], 'SailorID' => $sailors->SAILORID));
	        	$data = array(
	                'Code' => $mission[0],
	                'Opinion' => 1,
	                'MissionID' => $mission[1],
	                'SailorID' => $sailors->SAILORID,
	                'UPD_BY' => $this->user_session["USER_ID"],
	                'UPD_DT' => date("Y-m-d h:i:s a")
	            );
	            if ($this->utilities->updateData('willingnotwilling', $data, array("WillingNotWillingID" => $willing->WillingNotWillingID))) { // if data inserted successfully
	               	$success = 1 ;
	            } 
	        }
        }
        if($success == 1){
        	echo "<div class='alert alert-success'>Opinion entry inserted successfully</div>";            
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function opinionList() {
        $this->load->view("sailorsInfo/opinion/opinion_list");
    }
    

}

/* End of file opinion.php */
/* Location: ./application/controllers/sailorsInfo/opinion.php */