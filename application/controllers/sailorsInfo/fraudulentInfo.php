<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FraudulentInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Fraudulent Info Transction';
        $data['content_view_page'] = 'sailorsInfo/fraudulent_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['fraudulentCat'] = $this->utilities->findAllByAttribute("bn_fraudulentcategory", array("ACTIVE_STATUS" => 1));
        $data['shipEstablish'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/fraudulent_info/create',$data);
    }

    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $FRADULENT_ID= $this->input->post('FRADULENT_ID', true);

        $TranStatus= $this->input->post('TranStatus', true);
        $EffectingDate= date('Y-m-d', strtotime($this->input->post('effectiveDate', true)));

        if($this->input->post('authoName', true)){
            $authorityName= $this->input->post('authoName', true);
        }else{
            $SHIP_ESTABLISHMENTID= $this->input->post('SHIP_ESTABLISHMENTID', true);  
            $authorityName = '';
        }

        $authorityNumber= $this->input->post('authoNumber', true);
        $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate', true))) ;
        $remarks= $this->input->post('remarks', true);
      
        $data = array(
            'SailorID' => $SAILOR_ID,
            'CategoryID' => $FRADULENT_ID,
            'TranStatus' => $TranStatus,
            'TranDate' => $EffectingDate,
            'ShipID' => ($authorityName == '')? $SHIP_ESTABLISHMENTID: NULL,

            'AuthorityName' => $authorityName,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'Remarks' => $remarks,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($data, 'fraudulentinfo')) { // if data inserted successfully
            echo "<div class='alert alert-success'>Fraudulent Transaction add successfully</div>";
        }else{
            echo "<div class='alert alert-success'>Fraudulent Inserted Failed</div>";
        }
        
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function fraudulentList() {
        $this->load->view("sailorsInfo/fraudulent_info/fraudulent_list");
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT a.DetailID, a.SailorID, a.CategoryID, a.TranStatus, a.TranDate, a.ShipID, a.AuthorityNumber, a.AuthorityDate, a.Remarks, a.AuthorityName, s.OFFICIALNUMBER, s.FULLNAME, f.NAME CATEGORY, br.RANK_NAME, os.NAME SHIP_ESTABLISHMENT, se.NAME SE_ESTABLISHMENT
                                  FROM fraudulentinfo a 
                                  LEFT JOIN bn_fraudulentcategory f on f.CATEGORY_ID = a.CategoryID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                  LEFT JOIN bn_ship_establishment os on os.SHIP_ESTABLISHMENTID = a.ShipID
                                  WHERE a.DetailID = $id")->row();
        $data['fraudulentCat'] = $this->utilities->findAllByAttribute("bn_fraudulentcategory", array("ACTIVE_STATUS" => 1));
        $data['shipEstablish'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/fraudulent_info/edit',$data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update() {
        $id= $this->input->post('id', true);
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $FRADULENT_ID= $this->input->post('FRADULENT_ID', true);

        $TranStatus= $this->input->post('TranStatus', true);
        $EffectingDate= date('Y-m-d', strtotime($this->input->post('effectiveDate', true)));

        if($this->input->post('authoName', true)){
            $authorityName= $this->input->post('authoName', true);
        }else{
            $SHIP_ESTABLISHMENTID= $this->input->post('SHIP_ESTABLISHMENTID', true);  
            $authorityName = '';
        }

        $authorityNumber= $this->input->post('authoNumber', true);
        $authorityDate= date('Y-m-d', strtotime($this->input->post('authorityDate', true))) ;
        $remarks= $this->input->post('remarks', true);
        $data = array(
            'CategoryID' => $FRADULENT_ID,
            'TranStatus' => $TranStatus,
            'TranDate' => $EffectingDate,
            'ShipID' => ($authorityName == '')? $SHIP_ESTABLISHMENTID: NULL,

            'AuthorityName' => $authorityName,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'Remarks' => $remarks,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('fraudulentinfo',$data, array("DetailID" => $id))) { // if data inserted successfully
            echo "<div class='alert alert-success'>Fraudulent Transaction Update successfully</div>";
        }else{
            echo "<div class='alert alert-success'>Fraudulent Update Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT a.DetailID, a.SailorID, a.CategoryID, a.TranStatus, a.TranDate, a.ShipID, a.AuthorityNumber, a.AuthorityDate, a.Remarks, a.AuthorityName, s.OFFICIALNUMBER, s.FULLNAME, f.NAME CATEGORY, br.RANK_NAME, os.NAME SHIP_ESTABLISHMENT, se.NAME SE_ESTABLISHMENT
                                  FROM fraudulentinfo a 
                                  LEFT JOIN bn_fraudulentcategory f on f.CATEGORY_ID = a.CategoryID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                  LEFT JOIN bn_ship_establishment os on os.SHIP_ESTABLISHMENTID = a.ShipID
                                  WHERE a.DetailID = $id")->row();
        $this->load->view('sailorsInfo/fraudulent_info/view',$data);
    }

    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxFraudulentList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'OFFICIALNUMBER', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'CategoryID', 4 => 'TranStatus', 5 => 'TranDate', 6 => 'AuthorityName', 7 => 'AuthorityNumber', 8 => 'ShipID');

        // getting total number records without any search

        $query = $this->db->query("SELECT a.DetailID, a.SailorID, a.CategoryID, a.TranStatus, a.TranDate, a.ShipID, a.AuthorityNumber, a.AuthorityDate, a.Remarks, a.AuthorityName, s.OFFICIALNUMBER, s.FULLNAME, f.NAME CATEGORY, br.RANK_NAME, os.NAME SHIP_ESTABLISHMENT
                                  FROM fraudulentinfo a 
                                  LEFT JOIN bn_fraudulentcategory f on f.CATEGORY_ID = a.CategoryID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  LEFT JOIN bn_ship_establishment os on os.SHIP_ESTABLISHMENTID = a.ShipID")->num_rows();

       
        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT a.DetailID, a.SailorID, a.CategoryID, a.TranStatus, a.TranDate, a.ShipID, a.AuthorityNumber, a.AuthorityDate, a.Remarks, a.AuthorityName, s.OFFICIALNUMBER, s.FULLNAME, f.NAME CATEGORY, br.RANK_NAME, os.NAME SHIP_ESTABLISHMENT
                                  FROM fraudulentinfo a 
                                  LEFT JOIN bn_fraudulentcategory f on f.CATEGORY_ID = a.CategoryID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  LEFT JOIN bn_ship_establishment os on os.SHIP_ESTABLISHMENTID = a.ShipID
                                  WHERE s.OFFICIALNUMBER  LIKE '" . $requestData['search']['value'] ."%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR f.NAME LIKE '" . $requestData['search']['value']. 
                                    "%' OR a.TranStatus LIKE '" . $requestData['search']['value']. "%' OR a.AuthorityName LIKE '" . $requestData['search']['value']. "%' OR a.AuthorityDate LIKE '" . $requestData['search']['value']."%' OR br.RANK_NAME LIKE '" . $requestData['search']['value']."%' OR os.NAME LIKE '" . $requestData['search']['value']. 
                                    "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT a.DetailID, a.SailorID, a.CategoryID, a.TranStatus, a.TranDate, a.ShipID, a.AuthorityNumber, a.AuthorityDate, a.Remarks, a.AuthorityName, s.OFFICIALNUMBER, s.FULLNAME, f.NAME CATEGORY, br.RANK_NAME, os.NAME SHIP_ESTABLISHMENT
                                  FROM fraudulentinfo a 
                                  LEFT JOIN bn_fraudulentcategory f on f.CATEGORY_ID = a.CategoryID
                                  INNER JOIN sailor s on s.SAILORID = a.SailorID
                                  INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                  LEFT JOIN bn_ship_establishment os on os.SHIP_ESTABLISHMENTID = a.ShipID
                                  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        $status = array('1' => 'Record','2' => 'Disposed','3' => 'Under Disposal');
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            /*$nestedData[] = $row->FULLNAME;
            $nestedData[] = $row->RANK_NAME;*/
            $nestedData[] = $row->CATEGORY;
            $nestedData[] = array_key_exists($row->TranStatus, $status) ? $status[$row->TranStatus] : '';
            $nestedData[] =  date("Y-m-d", strtotime($row->TranDate));
            $nestedData[] = $row->SHIP_ESTABLISHMENT;
            $nestedData[] = $row->AuthorityName."<br><b><i>Number ".$row->AuthorityNumber."</i></b><br><b><i>Date ". date("Y-m-d", strtotime($row->AuthorityDate))."</i></b>";
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('sailorsInfo/fraudulentInfo/view/' . $row->DetailID) .'" title="View  Fraudulent Informaion" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink cm" href="'.site_url('sailorsInfo/fraudulentInfo/edit/' . $row->DetailID) .'" title="Edit  Fraudulent Informaion" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->DetailID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="DetailID" data-tbl="fraudulentinfo"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}


/* End of file fraudulentInfo.php */
/* Location: ./application/controllers/sailorsInfo/fraudulentInfo.php */