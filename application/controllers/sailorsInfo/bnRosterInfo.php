<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BnRosterInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Bn Roster Information';        
        $data['content_view_page'] = 'sailorsInfo/bnRoster_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
		$data['result'] = $this->db->query("SELECT se.*, st.NAME shipType, ar.NAME Area, zo.NAME Zone
                                            FROM bn_ship_establishment se
                                            INNER JOIN bn_shiptype st on st.SHIP_TYPEID = se.SHIP_TYPEID
                                            INNER JOIN bn_navyadminhierarchy ar on ar.ADMIN_ID = se.AREA_ID
                                            INNER JOIN bn_navyadminhierarchy zo on zo.ADMIN_ID = se.ZONE_ID")->result();
		
        $data['zone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID =" => "0"));
		$data['area'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "PARENT_ID =" => "0"));
		$data['visitClass'] = $this->utilities->findAllByAttribute("bn_visitclassification", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/bnRoster_info/create',$data);
    }
	public function areaByZone() {
        $zoneId = $_POST['zoneId'];
        $query = $this->utilities->findAllByAttribute('bn_navyadminhierarchy', array("PARENT_ID" => $zoneId, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->ADMIN_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
	/*Error */
	public function shipbyarea() {
        $areaId = $_POST['areaId'];
		echo $areaId;
		exit();
		$query = $this->utilities->findAllByAttribute('bn_shiptype', array("PARENT_ID" => $areaId, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->SHIP_TYPEID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
	
	
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function saveVI()
    {
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $descr= $this->input->post('desc', true);     
        $vci = $this->input->post('VISIT_CI_ID', true);     
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if Visit information with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_visitinformation", array("NAME" => $name, "CODE" => $code, "VISIT_CLASSIFICATIONID" => $vci));
        if (empty($check)) {// if Visit information name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'DESCR' => $descr,
                'VISIT_CLASSIFICATIONID' => $vci,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'bn_visitinformation')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Visit information Create successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Visit information Name insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Visit information Name Already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      single row
     */

    function viById($sn) {
        $id = $this->input->post('param'); // id
        $data['sn'] = $sn;
        $data['row'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vc.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi WHERE vi.VISIT_INFO_ID = $id")->row();
        $this->load->view('sailorsInfo/assessment_info/single_row', $data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function viList() {
        $data['result'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vc.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi")->result();
        $this->load->view("sailorsInfo/assessment_info/list", $data);
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vc.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi WHERE vi.VISIT_INFO_ID = $id")->row();
        $data['visitClass'] = $this->utilities->findAllByAttribute("bn_visitclassification", array("ACTIVE_STATUS" => 1));
        $this->load->view('sailorsInfo/bnRoster_info/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function updateVI()
    {
        $id= $this->input->post('id', true);
        $name= $this->input->post('name', true);
        $code= $this->input->post('code', true);
        $descr= $this->input->post('desc', true);     
        $vci = $this->input->post('VISIT_CI_ID', true);    
        $ACTIVE_STATUS = (isset($_POST['ACTIVE_STATUS'])) ? 1 : 0;

        // checking if district with this name is already exist
        $check = $this->utilities->hasInformationByThisId("bn_visitinformation", array("NAME" => $name,  "CODE" => $code, "VISIT_INFO_ID !=" => $id));
        if (empty($check)) {// if district name available
            $data = array(
                'CODE' => $code,
                'NAME' => $name,
                'DESCR' => $descr,
                'VISIT_CLASSIFICATIONID' => $vci,
                'ACTIVE_STATUS' => $ACTIVE_STATUS,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('bn_visitinformation',$data, array("VISIT_INFO_ID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Visit information Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Visit information Name Update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Visit information Name Already Exist</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT vi.*, (SELECT vc.NAME FROM bn_visitclassification vc WHERE vc.VISIT_CLASSIFICATIONID = vc.VISIT_CLASSIFICATIONID)vc_name FROM bn_visitinformation vi WHERE vi.VISIT_INFO_ID = $id")->row();
        $this->load->view('sailorsInfo/bnRoster_info/view',$data);
    }

}

/* End of file bnRosterInfo.php */
/* Location: ./application/controllers/sailorsInfo/bnRosterInfo.php */