<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LanguageInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Language Info Transction';
        $data['content_view_page'] = 'sailorsInfo/language_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['language'] = $this->utilities->findAllByAttributeWithOrderBy("bn_language", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('sailorsInfo/language_info/create',$data);
    }

    public function save()
    {
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $LANGUAGE_ID= $this->input->post('LANGUAGE_ID', true);  
        if(isset($_POST['read'])){
            $read = $this->input->post("read");
            $read_status = $this->input->post("read_status");
        }else{
            $read = 0 ;
            $read_status = 0 ;
        }
        if(isset($_POST['speck'])){
            $speck = $this->input->post("speck");
            $speck_status = $this->input->post("specking_status");
        }else{
            $speck = 0 ;
            $speck_status = 0 ;
        }
        if(isset($_POST['write'])){
            $write = $this->input->post("write");
            $write_status = $this->input->post("writing_status");
        }else{
            $write = 0 ;
            $write_status = 0 ;
        }
        if(isset($_POST['listen'])){
            $listen = $this->input->post("listen");
            $listen_status = $this->input->post("listen_status");
        }else{
            $listen = 0 ;
            $listen_status = 0 ;
        }
        $check = $this->utilities->hasInformationByThisId("languagetran", array('SailorID' => $SAILOR_ID, 'LanguageID' => $LANGUAGE_ID));
        if (empty($check)) {// if languageId & sailor not empty  name available
            $data = array(
                'SailorID' => $SAILOR_ID,
                'LanguageID' => $LANGUAGE_ID,
                'CanRead' => $read,
                'ReadEfficiency' => $read_status,
                'CanWrite' => $write,
                'WriteEfficiency' => $write_status,
                'CanSpeak' => $speck,
                'SpeakEfficiency' => $speck_status,
                'CanLeassening' => $listen,
                'LeasseningEfficiency' => $listen_status,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($data, 'languagetran')) { // if data inserted successfully
                echo "<div class='alert alert-success'>Language transaction Inserted successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Language transaction insert failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Language already Exist</div>";
        }
    }
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      
     */

    function languageList() {
        $this->load->view("sailorsInfo/language_info/list");
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT c.DetailID, c.SailorID, c.LanguageID, c.CanRead, c.ReadEfficiency, c.CanWrite, c.WriteEfficiency, c.CanSpeak, c.SpeakEfficiency, c.CanLeassening, c.LeasseningEfficiency, s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, l.NAME LANGUAGE_NAME, se.NAME SHIP_ESTABLISHMENT
                                    FROM languagetran c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_language l on l.LANGUAGE_ID = c.LanguageID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                    WHERE c.DetailID = $id ")->row();
        $data['language'] = $this->utilities->findAllByAttributeWithOrderBy("bn_language", array("ACTIVE_STATUS" => 1),"NAME");
        $this->load->view('sailorsInfo/language_info/edit', $data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update()
    {
        $id= $this->input->post('id', true);
        $SAILOR_ID= $this->input->post('SAILOR_ID', true);
        $LANGUAGE_ID= $this->input->post('LANGUAGE_ID', true);  
        if(isset($_POST['read'])){
            $read = $this->input->post("read");
            $read_status = $this->input->post("read_status");
        }else{
            $read = 0 ;
            $read_status = 0 ;
        }
        if(isset($_POST['speck'])){
            $speck = $this->input->post("speck");
            $speck_status = $this->input->post("specking_status");
        }else{
            $speck = 0 ;
            $speck_status = 0 ;
        }
        if(isset($_POST['write'])){
            $write = $this->input->post("write");
            $write_status = $this->input->post("writing_status");
        }else{
            $write = 0 ;
            $write_status = 0 ;
        }
        if(isset($_POST['listen'])){
            $listen = $this->input->post("listen");
            $listen_status = $this->input->post("listen_status");
        }else{
            $listen = 0 ;
            $listen_status = 0 ;
        }
        $check = $this->utilities->hasInformationByThisId("languagetran", array('SailorID' => $SAILOR_ID, 'LanguageID' => $LANGUAGE_ID, 'DetailID !=' => $id));
        if (empty($check)) {// if languageId & sailor not empty  name available
            $data = array(
                'LanguageID' => $LANGUAGE_ID,
                'CanRead' => $read,
                'ReadEfficiency' => $read_status,
                'CanWrite' => $write,
                'WriteEfficiency' => $write_status,
                'CanSpeak' => $speck,
                'SpeakEfficiency' => $speck_status,
                'CanLeassening' => $listen,
                'LeasseningEfficiency' => $listen_status,                
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('languagetran', $data, array("DetailID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Language transaction update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Language transaction update failed</div>";
            }
        }else{
            echo "<div class='alert alert-danger'>Language already Exist</div>";
        }
       
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT c.DetailID, c.SailorID, c.LanguageID, c.CanRead, c.ReadEfficiency, c.CanWrite, c.WriteEfficiency, c.CanSpeak, c.SpeakEfficiency, c.CanLeassening, c.LeasseningEfficiency, s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, l.NAME LANGUAGE_NAME, se.NAME SHIP_ESTABLISHMENT
                                    FROM languagetran c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_language l on l.LANGUAGE_ID = c.LanguageID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                    WHERE c.DetailID = $id")->row();
        $this->load->view('sailorsInfo/language_info/view',$data);
    }
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxlangTranList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'OFFICIALNUMBER', 1 => 'FULLNAME', 2 => 'RANK_NAME', 3 => 'LANGUAGE_NAME', 4 => 'ReadEfficiency', 5 => 'WriteEfficiency', 6 => 'SpeakEfficiency', 7 => 'LeasseningEfficiency');

        // getting total number records without any search

        $query = $this->db->query("SELECT c.DetailID, c.SailorID, c.LanguageID, c.CanRead, c.ReadEfficiency, c.CanWrite, c.WriteEfficiency, c.CanSpeak, c.SpeakEfficiency, c.CanLeassening, c.LeasseningEfficiency, s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, l.NAME LANGUAGE_NAME
                                    FROM languagetran c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_language l on l.LANGUAGE_ID = c.LanguageID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT c.DetailID, c.SailorID, c.LanguageID, c.CanRead, c.ReadEfficiency, c.CanWrite, c.WriteEfficiency, c.CanSpeak, c.SpeakEfficiency, c.CanLeassening, c.LeasseningEfficiency, s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, l.NAME LANGUAGE_NAME
                                    FROM languagetran c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_language l on l.LANGUAGE_ID = c.LanguageID
                                WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR s.FULLNAME LIKE '" . $requestData['search']['value'] . "%' OR c.ReadEfficiency LIKE '" . $requestData['search']['value']. "%' OR c.WriteEfficiency LIKE '" . $requestData['search']['value']."%' OR c.SpeakEfficiency LIKE '" . $requestData['search']['value']."%' OR c.LeasseningEfficiency LIKE '" . $requestData['search']['value']. "%' OR l.NAME LIKE '" . $requestData['search']['value'] . 
                                "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query


        } else {

            $query = $this->db->query("SELECT c.DetailID, c.SailorID, c.LanguageID, c.CanRead, c.ReadEfficiency, c.CanWrite, c.WriteEfficiency, c.CanSpeak, c.SpeakEfficiency, c.CanLeassening, c.LeasseningEfficiency, s.OFFICIALNUMBER, s.FULLNAME, br.RANK_NAME, l.NAME LANGUAGE_NAME
                                    FROM languagetran c
                                    INNER JOIN sailor s on s.SAILORID = c.SailorID
                                    INNER JOIN bn_rank br on s.RANKID = br.RANK_ID
                                    INNER JOIN bn_language l on l.LANGUAGE_ID = c.LanguageID  ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        $status = array('1' => 'Excellent', '2' => 'Good', '3' => 'Moderate', '4' => 'Poor' );
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            /*$nestedData[] = $row->FULLNAME;*/
            /*$nestedData[] = $row->RANK_NAME;*/
            $nestedData[] = $row->LANGUAGE_NAME;
            $nestedData[] = array_key_exists($row->ReadEfficiency, $status) ? $status[$row->ReadEfficiency] : '';
            $nestedData[] = array_key_exists($row->WriteEfficiency, $status) ? $status[$row->WriteEfficiency] : '';
            $nestedData[] = array_key_exists($row->SpeakEfficiency, $status) ? $status[$row->SpeakEfficiency] : '';
            $nestedData[] = array_key_exists($row->LeasseningEfficiency, $status) ? $status[$row->LeasseningEfficiency] : '';
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('sailorsInfo/LanguageInfo/view/' . $row->DetailID) .'" title="View Language Transction Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
                            '<a class="btn btn-warning btn-xs modalLink" href="'.site_url('sailorsInfo/LanguageInfo/edit/' . $row->DetailID) .'" title="Edit Language Transction Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->DetailID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="DetailID" data-tbl="languagetran"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file languageInfo.php */
/* Location: ./application/controllers/sailorsInfo/languageInfo.php */