<?php
/**
 * Created by PhpStorm.
 * User: streetcoder
 * Date: 1/25/16
 * Time: 12:53 PM
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class UI extends CI_Controller
{

    public function __construct() {
        parent::__construct();
    }

    public function dynamicField(){

        if(!empty($_POST)){

            echo '<pre>'.print_r($_POST,1).'</pre>';
        }

        $data['pageTitle'] = 'Dynamic Field';
        $data['breadcrumbs'] = array(
            'Dynamic Field' => '#'
        );
        $data['content_view_page'] = 'ui/dynamicField';

        $this->template->display($data);
    }

    public function dynamicFormCreate(){
        $this->load->view("ui/dynamicFormCreate");
    }

    public function index(){

        $data['pageTitle'] = 'Basic Form';
        $data['breadcrumbs'] = array(
            'Basic Form' => '#'
        );
        $data['content_view_page'] = 'ui/select2';
        //$data['content_view_page'] = 'ui/basicForm';

        $this->template->display($data);
    }

    public function formValidation(){

        $data['pageTitle'] = 'Form Validation';
        $data['breadcrumbs'] = array(
            'Form Validation' => '#'
        );
        $data['content_view_page'] = 'ui/formValidation';

        $this->template->display($data);
    }

    public function dualListbox(){
        $data['pageTitle'] = 'Dual Select';

        $data['breadcrumbs'] = array(
            'Dual Select' => '#'
        );
        $data['content_view_page'] = 'ui/dualSelect';
        $this->template->display($data);
    }

    public function multiSelect(){
        $data['pageTitle'] = 'Dual Select';
        $data['breadcrumbs'] = array(
            'Multi Select' => '#'
        );

        $data['content_view_page'] = 'ui/multiSelect';
        $this->template->display($data);
    }

    public function dataTable(){
        $data['pageTitle'] = 'Data Table';
        $data['breadcrumbs'] = array(
            'Data Table' => '#'
        );

        $data['content_view_page'] = 'ui/dataTable';
        $this->template->display($data);
    }

    /*
     * this is a barebones example of datatable at server side processing
     * dtss is the short form of data table server processing
     * we can use this example ofr our other case
     *
     * */
    public function dtsp(){

        $data['pageTitle'] = 'Data Table Server Processing';
        $data['breadcrumbs'] = array(
            'Data Table Server Processing' => '#'
        );

        $data['content_view_page'] = 'ui/dtsp';
        $this->template->display($data);

    }

    public function ajaxDtsp(){
        $this->load->library('DataTable');

        $draw               = $_REQUEST['draw'];
        $start              = $_REQUEST['start'];
        $length             = $_REQUEST['length'];
        $colToSort          = $_REQUEST["order"][0]["column"];
        $colToSortAction    = $_REQUEST["order"][0]["dir"];
        $search             = trim($_REQUEST["search"]["value"]);

        $tableName          = 'actor';
        $fieldsName         = array('first_name','last_name','last_update');
        $actions = array('UI/view','UI/edit','UI/remove');

        $recordsArr = $this->datatable->getDataTable(
            $draw,
            $start,
            $length,
            $colToSort,
            $colToSortAction,
            $search,
            $tableName,
            $fieldsName,
            $actions
        );

        echo json_encode($recordsArr);
    }

    public function dataTableJoin(){
        $data['pageTitle'] = 'DataTable Multi';
        $data['breadcrumbs'] = array(
            'DataTable Multi' => '#'
        );

        $data['content_view_page'] = 'ui/dataTableJoin';
        $this->template->display($data);
    }

    public function ajaxDataTableJoin(){
        $this->load->library('DataTable');

        $draw               = $_REQUEST['draw'];
        $start              = $_REQUEST['start'];
        $length             = $_REQUEST['length'];
        $colToSort          = $_REQUEST["order"][0]["column"];
        $sortType    = $_REQUEST["order"][0]["dir"];
        $search             = trim($_REQUEST["search"]["value"]);

        $fieldsName         = array('actor.actor_id','actor.first_name','film.film_id', 'film.title');
        $actions = array('UI/view','UI/edit','UI/remove');

        $sqlPartial = 'SELECT actor.actor_id, film.film_id, actor.first_name, film.title  FROM actor
          JOIN film_actor ON actor.actor_id = film_actor.actor_id
          JOIN film ON film.film_id = film_actor.film_id';

        $recordsArr = $this->datatable->getJoinDataTable(
            $sqlPartial,
            $fieldsName,
            $colToSort,
            $sortType,
            $start,
            $length,
            $actions,
            $search,
            $draw);

        echo json_encode($recordsArr);
    }

    public function modalValidation(){
        $data['pageTitle'] = 'Partial Modal Validation';
        $data['breadcrumbs'] = array(
            'Partial Modal Validation' => '#'
        );

        $data['content_view_page'] = 'ui/partialModalValidation';
        $this->template->display($data);
    }


}