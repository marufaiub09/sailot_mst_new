<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Academic information
 * @author     Emran Hossen <emran@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class AcademicInfo extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Academic Information';
        $data['flag'] = 0; /* flag status = 0 is Active sailor; status = 1 is retirement sailor; */
        $data['exam_name'] = $this->utilities->dropdownFromTableWithCondition('bn_govtexam_hierarchy', 'Select Exam Name', 'CODE', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['subject_group'] = $this->utilities->dropdownFromTableWithCondition('bn_subject_group', 'Select Group Name', 'SUBJECT_GROUPID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['board_university'] = $this->utilities->dropdownFromTableWithCondition('bn_boardcenter', 'Select Bard/University', 'BOARD_CENTERID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['result'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', 'Select Result', 'EXAM_GRADEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'regularTransaction/AcademicInformation/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function ifExistData() {
        $sailor_id = $this->input->post("sailor_id");
        $data['academicInfo'] = $this->db->query("SELECT 
                                 academic.ExamID,
                                (SELECT e.name
                                    FROM bn_govtexam_hierarchy e
                                    WHERE e.GOVT_EXAMID = academic.ExamID)
                                    AS Exam_Name,
                            (SELECT b.NAME
                                    FROM bn_boardcenter b
                                    WHERE b.BOARD_CENTERID = academic.BoardID)
                                    AS Board_Name,
                                    academic.BoardID,
                            (SELECT sg.NAME
                                    FROM bn_subject_group sg
                                    WHERE sg.SUBJECT_GROUPID = academic.SubjectID)
                                    Subject,
                                    academic.SubjectID,
                            PassingYear,
                            EvaluationSystem,
                            (SELECT CASE EvaluationSystem
                                                    WHEN 1 THEN 'Traditional'
                                                    WHEN 2 THEN 'GPA'
                                                    ELSE NULL
                                            END)
                                    AS Exam_System,
                            (SELECT ex.NAME
                                    FROM bn_examgrade ex
                                    WHERE ex.EXAM_GRADEID = academic.ExamGradeID)
                                    AS Exam_Gred,
                                    academic.ExamGradeID,
                                    (SELECT CASE academic.EducationStatus
                  WHEN 1 THEN 'Entry Education'
                  WHEN 2 THEN 'Entry Period Highest'
                  WHEN 3 THEN 'Last'
                  ELSE NULL
               END)
          AS EDUST,
                                    academic.EducationStatus,
                            ResultDescription,
                            TotalMarks,
                            Percentage,
                                            (SELECT CASE EducationStatus
                                                    WHEN 1 THEN 'Entry Education'
                                                    WHEN 2 THEN 'Entry Period Highest'
                                                    when 3 then 'Last'
                                                    ELSE NULL
                                            END)
                                    AS EducationStatus, 
                                    academic.AcademicID     
                    FROM academic 
                    WHERE SailorID = $sailor_id")->result();
        $this->load->view('regularTransaction/AcademicInformation/academicData', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function updateSailorList($sailor_id) {
        $data['academicInfo'] = $this->db->query("SELECT 
                                 academic.ExamID,
                                (SELECT e.name
                                    FROM bn_govtexam_hierarchy e
                                    WHERE e.GOVT_EXAMID = academic.ExamID)
                                    AS Exam_Name,
                            (SELECT b.NAME
                                    FROM bn_boardcenter b
                                    WHERE b.BOARD_CENTERID = academic.BoardID)
                                    AS Board_Name,
                                    academic.BoardID,
                            (SELECT sg.NAME
                                    FROM bn_subject_group sg
                                    WHERE sg.SUBJECT_GROUPID = academic.SubjectID)
                                    Subject,
                                    academic.SubjectID,
                            PassingYear,
                            EvaluationSystem,
                            (SELECT CASE EvaluationSystem
                                                    WHEN 1 THEN 'Traditional'
                                                    WHEN 2 THEN 'GPA'
                                                    ELSE NULL
                                            END)
                                    AS Exam_System,
                            (SELECT ex.NAME
                                    FROM bn_examgrade ex
                                    WHERE ex.EXAM_GRADEID = academic.ExamGradeID)
                                    AS Exam_Gred,
                                    academic.ExamGradeID,
                                    (SELECT CASE academic.EducationStatus
                  WHEN 1 THEN 'Entry Education'
                  WHEN 2 THEN 'Entry Period Highest'
                  WHEN 3 THEN 'Last'
                  ELSE NULL
               END)
          AS EDUST,
                                    academic.EducationStatus,
                            ResultDescription,
                            TotalMarks,
                            Percentage,
                                            (SELECT CASE EducationStatus
                                                    WHEN 1 THEN 'Entry Education'
                                                    WHEN 2 THEN 'Entry Period Highest'
                                                    when 3 then 'Last'
                                                    ELSE NULL
                                            END)
                                    AS EducationStatus, 
                                    academic.AcademicID     
                    FROM academic 
                    WHERE SailorID = $sailor_id")->result();
        $this->load->view('regularTransaction/AcademicInformation/academicList', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function getEduData() {
        $examSystem = $this->input->post("examSystem");
        $examSystem_name = $this->input->post("examSystem_name");
        $examNameDesc = $this->input->post("examNameDesc");
        $examNameDesc_name = $this->input->post("examNameDesc_name");
        $subjectGroupDesc = $this->input->post("subjectGroupDesc");
        $subjectGroupDesc_name = $this->input->post("subjectGroupDesc_name");
        $passingYear = $this->input->post("passingYear");
        $baordUniversityDesc = $this->input->post("baordUniversityDesc");
        $baordUniversityDesc_name = $this->input->post("baordUniversityDesc_name");
        $result = $this->input->post("result");
        $result_n = $this->input->post("result_n");
        $resultdesc = $this->input->post("resultDesc");
        $eduStatusDesc = $this->input->post("eduStatusDesc");
        $eduStatusDesc_name = $this->input->post("eduStatusDesc_name");
        $percent = $this->input->post("percent");
        $traditional = $this->input->post("traditional");
        $gpa = $this->input->post("gpa");
        echo '<tr>            
        <td class="text-center">' . $examSystem_name . '<input type="hidden" name="examSystemTable[]" class="examSystem" value="' . $examSystem . '" /></td>
        <td class="text-center">' . $examNameDesc_name . '<input type="hidden" name="examNameDescTable[]" class="eximId" value="' . $examNameDesc . '" /></td>
        <td class="text-center">' . $subjectGroupDesc_name . '<input type="hidden" name="subjectGroupDescTable[]" class="subjectGroupDesc" value="' . $subjectGroupDesc . '" /></td>
        <td class="text-center">' . $passingYear . '<input type="hidden" name="passingYearTable[]" class="passingYear" value="' . $passingYear . '" /></td>
        <td class="text-center">' . $baordUniversityDesc_name . '<input type="hidden" name="baordUniversityDescTable[]" class="baordUniversityDesc" value="' . $baordUniversityDesc . '" /></td>
        <td class="text-center">' . $result_n . '<input type="hidden" name="resultTable[]" class="result" value="' . $result . '" /></td>
        <td class="text-center">' . $gpa . '<input type="hidden" name="gpaTable[]" class="gpa" value="' . $gpa . '" /></td>
        <td class="text-center">' . $traditional . '<input type="hidden" name="traditionTable[]" class="eximId" value="' . $traditional . '" /></td>  
        <td class="text-center">' . $percent . '<input type="hidden" name="percentTable[]" class="percent" value="' . $percent . '" /></td>
        <td class="text-center">' . $resultdesc . '<input type="hidden" name="resultdescTable[]" class="resultdesc" value="' . $resultdesc . '" /></td>
        <td class="text-center">' . $eduStatusDesc_name . '<input type="hidden" name="eduStatusDescTable[]" class="eduStatusDesc" value="' . $eduStatusDesc . '" /></td>
        <td class="text-center"><span class="removeEdu btn btn-danger  btn-xs">Remove</span></td>
    </tr>';
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function save() {
        /* Academic Information */
        $sailorId = $this->input->post('sailorId', true);
        $examSystem = $this->input->post('examSystemTable', true);
        $examName = $this->input->post('examNameDescTable', true);
        $subject = $this->input->post('subjectGroupDescTable', true);
        $passingYear = $this->input->post('passingYearTable', true);
        $boardUniversity = $this->input->post('baordUniversityDescTable', true);
        $result = $this->input->post('resultTable', true);
        $gpaName = $this->input->post('gpaTable', true);
        $percentage = $this->input->post('percentTable');
        $resultDescription = $this->input->post('resultdescTable', true);
        $educationStatus = $this->input->post('eduStatusDescTable', true);
        $traditional = $this->input->post('traditionTable', true);
        
        $success = 1;
        for ($i = 0; $i < count($examName); $i++) {
            $academicInf = array(
                'SailorID' => $sailorId,
                'ExamID' => $examName[$i],
                'EvaluationSystem' => $examSystem[$i],
                'BoardID' => $boardUniversity[$i],
                'SubjectID' => $subject[$i],
                'PassingYear' => $passingYear[$i],
                'ExamGradeID' => $result[$i],
                'TotalMarks' => $traditional[$i],
                'Percentage' => $percentage[$i],
                'ResultDescription' => $resultDescription[$i],
                'EducationStatus' => $educationStatus[$i],
                'CRE_BY' => $this->user_session["USER_ID"],
                'CRE_DT' => date("Y-m-d h:i:s a")
            );
            if ($Academic = $this->utilities->insertData($academicInf, 'academic')) {
                /* end Academic insert part */
                $success = 1;
            }
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Academic Information successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Academic Information successfully</div>";
        }
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['results'] = $this->db->query("SELECT a.AcademicID,
                                            a.SailorID AS SailorID,
                                            s.OFFICIALNUMBER,
                                            s.FULLNAME,
                                            r.RANK_NAME,
                                            po.NAME AS postingUnit,       
                                            DATE_FORMAT(s.POSTINGDATE,'%d/%m/%Y') AS POSTINGDATE,
                                            a.ExamID,
                                            a.BoardID,
                                            a.SubjectID,
                                            a.ExamGradeID,
                                            a.PassingYear,
                                             a.EvaluationSystem,       
                                            a.ResultDescription,
                                            a.TotalMarks,
                                            a.Percentage,
                                            a.EducationStatus
                                       FROM academic a
                                            LEFT JOIN Sailor s ON a.SailorID = s.SAILORID
                                            LEFT JOIN bn_rank r ON s.RANKID = r.RANK_ID
                                            LEFT JOIN bn_posting_unit po ON s.POSTINGUNITID = po.POSTING_UNITID
                                            WHERE a.AcademicID = $id")->row();
        $data['exam_nameData'] = $this->utilities->findAllByAttribute("bn_govtexam_hierarchy", array("ACTIVE_STATUS" => 1));
        $data['subject_group'] = $this->utilities->findAllByAttribute("bn_subject_group", array("ACTIVE_STATUS" => 1));
        $data['board_university'] = $this->utilities->findAllByAttribute("bn_boardcenter", array("ACTIVE_STATUS" => 1));
        $data['result'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
        $this->load->view('regularTransaction/AcademicInformation/edit', $data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function update() {
        $id = $this->input->post('idEdit', true);
        $sailor_id = $this->input->post('sailorId', true);
        $examSystem = $this->input->post('examSystem', true);
        $examName = $this->input->post('examName', true);
        $subjectGroup = $this->input->post('subjectGroup', true);
        $passingYear = $this->input->post('passingYear', true);
        $boardUniversity = $this->input->post('boardUniversity', true);
        $result = $this->input->post('result', true);
        $gpa = $this->input->post('gpa', true);
        $tradition = $this->input->post('tradition', true);
        $percent = $this->input->post('percent', true);
        $resultdesc = $this->input->post('resultdesc', true);
        $eduStatusDesc = $this->input->post('eduStatusDesc', true);
        if ($examSystem == 1) {
            $tradition_1 = $this->input->post('tradition', true);
        } else {
            $tradition_1 = $this->input->post('gpa', true);
        }
        $check = $this->utilities->hasInformationByThisId("academic", array("ExamID" => $examName, "SailorID" => $sailor_id, "AcademicID !=" => $id));
        //$ss = $this->db->last_query();
        if (empty($check)) {// if Academic Information available
            $data = array(
                'ExamID' => $examName,
                'BoardID' => $boardUniversity,
                'SubjectID' => $subjectGroup,
                'PassingYear' => $passingYear,
                'EvaluationSystem' => $examSystem,
                'ExamGradeID' => $result,
                'ResultDescription' => $resultdesc,
                'TotalMarks' => $tradition_1,
                'EducationStatus' => $eduStatusDesc,
                'Percentage' => $percent,
                'ACTIVE_STATUS' => 1,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('academic', $data, array("AcademicID" => $id))) { // if data inserted successfully
                echo "<div class='alert alert-success'>Academic Update successfully</div>";
            } else { // if data inserted failed
                echo "<div class='alert alert-danger'>Division Name Update failed</div>";
            }
        } else {
            echo "<div class='alert alert-danger'>Academic Name Already Exist</div>";
        }
    }

//
//    //emran
//
//    public function edit($id) {
//        $data['pageTitle'] = 'Academic Information';
//        $data['results'] = $this->db->query("SELECT a.AcademicID,
//                                            a.SailorID AS SailorID,
//                                            s.OFFICIALNUMBER,
//                                            s.FULLNAME,
//                                            r.RANK_NAME,
//                                            po.NAME AS postingUnit,       
//                                            DATE_FORMAT(s.POSTINGDATE,'%d/%m/%Y') AS POSTINGDATE,
//                                            a.ExamID,
//                                            a.BoardID,
//                                            a.SubjectID,
//                                            a.ExamGradeID,
//                                            a.PassingYear,
//                                             a.EvaluationSystem,       
//                                            a.ResultDescription,
//                                            a.TotalMarks,
//                                            a.Percentage,
//                                            a.EducationStatus
//                                       FROM academic a
//                                            LEFT JOIN Sailor s ON a.SailorID = s.SAILORID
//                                            LEFT JOIN bn_rank r ON s.RANKID = r.RANK_ID
//                                            LEFT JOIN bn_posting_unit po ON s.POSTINGUNITID = po.POSTING_UNITID
//                                             WHERE a.AcademicID = $id")->row();
//        $data['exam_nameData'] = $this->utilities->findAllByAttribute("bn_govtexam_hierarchy", array("ACTIVE_STATUS" => 1));
//        $data['subject_group'] = $this->utilities->findAllByAttribute("bn_subject_group", array("ACTIVE_STATUS" => 1));
//        $data['board_university'] = $this->utilities->findAllByAttribute("bn_boardcenter", array("ACTIVE_STATUS" => 1));
//        $data['result'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
//        $data['content_view_page'] = 'regularTransaction/AcademicInfo/edit';
//        $this->template->display($data);
//    }
//    
//    
//    
//     public function update() {
//        $id = $this->input->post('id', true);
//        $examSystem = $this->input->post('examSystem', true);
//        $examName = $this->input->post('examName', true);
//        $subjectGroup = $this->input->post('subjectGroup', true);        
//        $passingYear = $this->input->post('passingYear', true);
//        $boardUniversity = $this->input->post('boardUniversity', true);
//        $result = $this->input->post('result', true);        
//        $percent = $this->input->post('percent', true);        
//        $resultdesc = $this->input->post('resultdesc', true);
//        $eduStatusDesc = $this->input->post('eduStatusDesc', true);
//        if ($examSystem == 1){
//            $tradition_1 = $this->input->post('tradition', true);
//        } else {
//            $tradition_1 = $this->input->post('gpa', true);
//        }
//        $update = array(
//            'ExamID' => $examName,            
//            'BoardID' => $boardUniversity,            
//            'SubjectID' => $subjectGroup,            
//            'PassingYear' => $passingYear,            
//            'EvaluationSystem' => $examSystem,            
//            'ExamGradeID' => $result,            
//            'ResultDescription' => $resultdesc,            
//            'TotalMarks' => $tradition_1,            
//            'EducationStatus' => $eduStatusDesc,
//            'Percentage' => $percent,
//            'ACTIVE_STATUS'=> 1,
//            'UPD_BY' => $this->user_session["USER_ID"],
//            'UPD_DT' => date("Y-m-d h:i:s a")
//        );
//        echo '<pre>';
//        print_r($update);
//        //exit;
//        if ($this->utilities->updateData('academic', $update, array('AcademicID' => $id))) {
//            echo "<div class='alert alert-success'>Academic Information Update successfully</div>";
//        } else {
//            echo "<div class='alert alert-success'>Academic Information Update Failed</div>";
//        }
//    }
}
