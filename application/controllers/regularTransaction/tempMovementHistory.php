<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Movement History
 * @package    Movement History Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class TempMovementHistory extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Movement Information';
        $data['content_view_page'] = 'regularTransaction/temp_movement_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['appointment'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'regularTransaction/temp_movement_info/create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function save() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $POSTING_UNIT_ID = $this->input->post('POSTING_UNIT_ID', true);
        $APPOINTMENT_TYPE = $this->input->post('APPOINTMENT_TYPE', true);
        $SHIP_ESTABLISHMENT = $this->input->post('SHIP_ESTABLISHMENT', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $orderNumber = $this->input->post('orderNumber', true);
        $remarks = $this->input->post('remarks', true);
        $orderDate = date('Y-m-d', strtotime($this->input->post('orderDate', true)));

        $draftInDate = date('Y-m-d', strtotime($this->input->post('draftInDate', true)));

        $movement_temp2 = array(
            'SailorID' => $SAILOR_ID,
            'AppointmentTypeID' => $APPOINTMENT_TYPE,
            'ShipEstablishmentID' => $SHIP_ESTABLISHMENT,
            'PostingUnitID' => $POSTING_UNIT_ID,
            'DraftInDate' => $draftInDate,
            'TransferOrderNo' => $orderNumber,
            'TransferOrderDate' => $orderDate,
            'AuthorityShipID' => $AUTHO_SHIP_ESTABLISHMENT,
            'GenFormNo' => $authorityNumber,
            'GenFormDate' => $authorityDate,
            'Remarks' => $remarks,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if (isset($_POST['draftOutDate'])) {
            $draftOutDate = date('Y-m-d', strtotime($this->input->post('draftOutDate', true)));
            $movement_temp1 = array('DraftOutDate' => $draftOutDate);
            $movementtemp = array_merge($movement_temp1, $movement_temp2);

            if ($this->utilities->insertData($movementtemp, 'movementtemp')) {
                echo "<div class='alert alert-success'>Tempory Movement Info added successfully</div>";
            } else {
                echo "<div class='alert alert-success'>Tempory Movement Info Inserted Failed</div>";
            }
        } else {

            if ($this->utilities->insertData($movement_temp2, 'movementtemp')) {
                echo "<div class='alert alert-success'>Tempory Movement History added successfully</div>";
            } else {
                echo "<div class='alert alert-success'>Tempory Movement History Inserted Failed</div>";
            }
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST, bse.AREA_ID AUTHO_AREA_ID, bse.NAME AUTHO_SHIP_EST
                                            FROM movementtemp m
                                            INNER JOIN sailor s on s.SAILORID = m.SailorID
                                            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
                                            INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                            INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                            INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
                                            WHERE m.MovementID = $id")->row();
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['PostingUnit'] = $this->utilities->findAllByAttribute("bn_posting_unit", array("ACTIVE_STATUS" => 1));
        $data['appointment'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));

        $data['content_view_page'] = 'regularTransaction/temp_movement_info/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function update() {
        $id = $this->input->post('id', true);
        $POSTING_UNIT_ID = $this->input->post('POSTING_UNIT_ID', true);
        $APPOINTMENT_TYPE = $this->input->post('APPOINTMENT_TYPE', true);
        $SHIP_ESTABLISHMENT = $this->input->post('SHIP_ESTABLISHMENT', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $orderNumber = $this->input->post('orderNumber', true);
        $remarks = $this->input->post('remarks', true);
        $orderDate = date('Y-m-d', strtotime($this->input->post('orderDate', true)));

        $draftInDate = date('Y-m-d', strtotime($this->input->post('draftInDate', true)));

        $movement_temp2 = array(
            'AppointmentTypeID' => $APPOINTMENT_TYPE,
            'ShipEstablishmentID' => $SHIP_ESTABLISHMENT,
            'PostingUnitID' => $POSTING_UNIT_ID,
            'DraftInDate' => $draftInDate,
            'TransferOrderNo' => $orderNumber,
            'TransferOrderDate' => $orderDate,
            'AuthorityShipID' => $AUTHO_SHIP_ESTABLISHMENT,
            'GenFormNo' => $authorityNumber,
            'GenFormDate' => $authorityDate,
            'Remarks' => $remarks,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if (isset($_POST['draftOutDate'])) {
            $draftOutDate = date('Y-m-d', strtotime($this->input->post('draftOutDate', true)));
            $movement_temp1 = array('DraftOutDate' => $draftOutDate);
            $movementtemp = array_merge($movement_temp1, $movement_temp2);

            if ($this->utilities->updateData('movementtemp', $movementtemp, array('MovementID' => $id))) {
                echo "<div class='alert alert-success'>Tempory Movement information updated successfully</div>";
            } else {
                echo "<div class='alert alert-success'>Tempory Movement information updated Failed</div>";
            }
        } else {

            if ($this->utilities->updateData('movementtemp', $movement_temp2, array('MovementID' => $id))) {
                echo "<div class='alert alert-success'>Tempory Movement information updated successfully</div>";
            } else {
                echo "<div class='alert alert-success'>Tempory Movement information updated Failed</div>";
            }
        }
    }

    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST, bse.AREA_ID AUTHO_AREA_ID, bse.NAME AUTHO_SHIP_EST
                                            FROM movementtemp m
                                            INNER JOIN sailor s on s.SAILORID = m.SailorID
                                            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
                                            INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                            INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                            INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
                                            WHERE m.MovementID = $id")->row();
       // echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/temp_movement_info/view', $data);
    }

    /**
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      View modal
     */
    function ajaxMovementHistoryList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'm.MovementID', 1 => 's.OFFICIALNUMBER', 2 => 'se.NAME', 3 => 'pu.NAME', 4 => 'm.DraftInDate', 5 => 'm.DraftOutDate', 6 => 'ap.NAME', 7 => 'm.MovementID');

        // getting total number records without any search

        $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, ap.NAME Appoint_Type, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, bse.NAME AUTHO_SHIP_EST
                                    FROM movementtemp m
                                    INNER JOIN sailor s on s.SAILORID = m.SailorID
                                    INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                    INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, ap.NAME Appoint_Type, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, bse.NAME AUTHO_SHIP_EST
                                    FROM movementtemp m
                                    INNER JOIN sailor s on s.SAILORID = m.SailorID
                                    INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                    INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
                                    WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] . "%' OR pu.NAME LIKE '" . $requestData['search']['value'] .
                            "%' OR ap.NAME LIKE '" . $requestData['search']['value'] . "%' OR m.DraftInDate LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, ap.NAME Appoint_Type, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, bse.NAME AUTHO_SHIP_EST
                                    FROM movementtemp m
                                    INNER JOIN sailor s on s.SAILORID = m.SailorID
                                    INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = m.AppointmentTypeID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = m.PostingUnitID
                                    INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = m.AuthorityShipID
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->SHIP_EST;
            $nestedData[] = $row->POSTING_UNIT;
            $nestedData[] = date("Y-m-d", strtotime($row->DraftInDate));
            $nestedData[] = (!is_null($row->DraftOutDate) ) ? date("Y-m-d", strtotime($row->DraftOutDate)) : '';
            $nestedData[] = $row->Appoint_Type;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('regularTransaction/tempMovementHistory/view/' . $row->MovementID) . '" title="View Temporary Movement Information" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('regularTransaction/tempMovementHistory/edit/' . $row->MovementID) . '" title="Edit Temporary Movement Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->MovementID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="MovementID" data-tbl="movement"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file movementHistory.php */
    /* Location: ./application/controllers/regularTransaction/movementHistory.php */