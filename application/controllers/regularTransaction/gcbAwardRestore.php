<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   GCB Ward/Restore
 * @package    GCB Ward/Restore Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class GcbAwardRestore extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'GCB Award/Restore Information';
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/gcb_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/gcb_info/create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function save() {
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $GCBType = $this->input->post('GCBType', true);
        $GCB_NO = $this->input->post('GCB_NO', true);
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));

        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));


        $gcb = array(
            'SailorID' => $SAILOR_ID,
            'GCBNumber' => $GCB_NO,
            'EffectDate' => $awardDate,
            'DepriveType' => 0,
            'GCBType' => $GCBType,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $DAO->DAO_NO,
            'ShipID' => $AUTHO_SHIP_ESTABLISHMENT,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($gcb, 'gcb')) {
            echo "<div class='alert alert-success'>GCB Award/Restore added successfully</div>";
        } else {
            echo "<div class='alert alert-success'>GCB Award/Restore Inserted Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT g.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME , bse.NAME SHIP_EST ,se.NAME AUTHO_SHIP_EST, se.AREA_ID
									FROM gcb g
									INNER JOIN sailor s on s.SAILORID = g.SailorID
				                  	INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
				                  	INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = g.ShipID
                  					WHERE g.GCBID = $id")->row();
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/gcb_info/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function update() {

        $id = $this->input->post('id', true);
        $GCBType = $this->input->post('GCBType', true);
        $GCB_NO = $this->input->post('GCB_NO', true);
        $awardDate = date('Y-m-d', strtotime($this->input->post('awardDate', true)));

        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));

        $update = array(
            'EffectDate' => $awardDate,
            'GCBType' => $GCBType,
            'AuthorityNumber' => $authorityNumber,
            'AuthorityDate' => $authorityDate,
            'DAOID' => $DAO_ID,
            'DAONumber' => $DAO->DAO_NO,
            'ShipID' => $AUTHO_SHIP_ESTABLISHMENT,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        if ($this->utilities->updateData('gcb', $update, array('GCBID' => $id))) {
            echo "<div class='alert alert-success'>GCB Award/Restore Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>GCB Award/Restore Update Failed</div>";
        }
    }

    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT g.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME , bse.NAME SHIP_EST ,se.NAME AUTHO_SHIP_EST, se.AREA_ID
									FROM gcb g
									INNER JOIN sailor s on s.SAILORID = g.SailorID
				                  	INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
				                  	INNER JOIN bn_ship_establishment bse on bse.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = g.ShipID
                  					WHERE g.GCBID= $id")->row();
//        echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/gcb_info/view', $data);
    }

    function ajaxAwardRestoreList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 'g.GCBID', 1 => 's.OFFICIALNUMBER', 2 => 'g.GCBNumber', 3 => 'g.GCBType', 4 => 'g.EffectDate', 5 => 'g.DAONumber', 6 => 'se.NAME', 7 => 'g.AuthorityNumber');


        // getting total number records without any search

        $query = $this->db->query("SELECT g.*, s.OFFICIALNUMBER, se.NAME AUTHO_SHIP_EST
									FROM gcb g
									INNER JOIN sailor s on s.SAILORID = g.SailorID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = g.ShipID
                                    WHERE s.SAILORSTATUS = 1")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT g.*, s.OFFICIALNUMBER, se.NAME AUTHO_SHIP_EST
									FROM gcb g
									INNER JOIN sailor s on s.SAILORID = g.SailorID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = g.ShipID
                                    WHERE s.SAILORSTATUS = 1 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR se.NAME LIKE '" . $requestData['search']['value'] . "%' OR g.GCBNumber LIKE '" . $requestData['search']['value'] .
                            "%' OR g.EffectDate LIKE '" . $requestData['search']['value'] . "%' OR g.DAONumber LIKE '" . $requestData['search']['value'] . "%' OR g.AuthorityNumber LIKE '" . $requestData['search']['value'] . "%' OR g.GCBType LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT g.*, s.OFFICIALNUMBER, se.NAME AUTHO_SHIP_EST
									FROM gcb g
									INNER JOIN sailor s on s.SAILORID = g.SailorID
									INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = g.ShipID
                                    WHERE s.SAILORSTATUS = 1
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->GCBNumber;
            $nestedData[] = ($row->GCBType == 1) ? "Award" : "Restore";
            $nestedData[] = $row->DAONumber;
            $nestedData[] = $row->AUTHO_SHIP_EST;
            $nestedData[] = $row->AuthorityNumber;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="' . site_url('regularTransaction/gcbAwardRestore/view/' . $row->GCBID) . '" title="View Award/Restore" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('regularTransaction/gcbAwardRestore/edit/' . $row->GCBID) . '" title="Edit Award/Restore Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->GCBID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="GCBID" data-tbl="gcb"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

    /**
     * @access      public
     * @param       officiar number
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Sailor Info, Rank_name, Ship_establishment
     */
    function searchSailorInfoByOfficalNo() {
        $officalNumber = $this->input->post("officeNumber");
        $sailorStatus = $this->input->post("sailorStatus");
        $this->db->select('s.*, r.RANK_NAME, se.NAME SHIP_ESTABLISHMENT');
        $this->db->from('sailor as s');
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID', 'INNER');
        $this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID', 'INNER');
        $condition = array('s.OFFICIALNUMBER' => $officalNumber, 's.SAILORSTATUS' => $sailorStatus);
        $this->db->where($condition);
        echo json_encode($this->db->get()->row_array());

    }

    /**
     * @access      public
     * @param       officiar number
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      gcbWard count
     */
    function gcbCountByOfficalNo() {
        $officalNumber = $this->input->post("officeNumber");
        $this->db->select('count(g.GCBNumber) gcb');
        $this->db->from('sailor as s');
        $this->db->join('gcb as g', 'g.SailorID = s.SailorID', 'left');
        $this->db->where('s.OFFICIALNUMBER', $officalNumber);
        echo json_encode($this->db->get()->row_array());
    }

}

/* End of file gcbWardRestore.php */
/* Location: ./application/controllers/regularTransaction/gcbWardRestore.php */