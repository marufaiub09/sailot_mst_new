<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Re-Engagement
 * @package    Re-Engagement Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class ReEngagementInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Re-Engagement Information';
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/engagement_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create() {
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['appointment'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['entryType'] = $this->utilities->findAllByAttribute("bn_entrytype", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/engagement_info/create';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function save() {

        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $engagementNoNext = $this->input->post('engagementNoNext', true);
        $engagementDate = date('Y-m-d', strtotime($this->input->post('engagementDate', true)));
        $engagePeriod = $this->input->post('engageFor', true);
        $SHIP_AREA_ID = $this->input->post('SHIP_AREA_ID', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $DAO_NO = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_NO));
        $expireDate = $this->input->post('expireDate', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $engagement_type = $this->input->post('engagement_type', true);
        $ENTRY_TYPE = $this->input->post('ENTRY_TYPE', true);
        $priodYY = $this->input->post('priodYY', true);
        $priodMM = $this->input->post('priodMM', true);
        $priodDD = $this->input->post('priodDD', true);
        $allEngagement = $priodYY.'-'.$priodMM.'-'.$priodDD;
        $totalEngagePeriod = $engagePeriod ;

        /*$updateEngagement = array(
                "ENGAGEMENTPERIOD" =>$priodYY+$priodMM+$priodDD,
                "ENGAGEMENTTYPE" =>$engagement_type,
                "TOTALENGAGEPERIOD" =>$dateForRe
            );*/
        $engagement = array(
            'SailorID' => $SAILOR_ID,
            'EngagementDate' => $engagementDate,
            'EntryTypeID' => $ENTRY_TYPE,
            'EngagementYear' => $priodYY,
            'EngagementMonth' => $priodMM,
            'EngagementDay' => $priodDD,
            'ExpiryDate' => $expireDate,
            'EngagementNo' => $engagementNoNext,
            'DAOID' => $DAO_NO,
            'DAONo' => $DAO->DAO_NO,
            'ShipEstablishmentID' => $AUTHO_SHIP_ESTABLISHMENT,
            'EngagementType' => $engagement_type,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if(isset($_POST['ncsToCS'])){
            $ncsToCS = $this->input->post('ncsToCS', true);
            $ncs = array(
                    'IsNCSToCS' => $ncsToCS
                );
            $insert = array_merge($engagement, $ncs);
            if ($this->utilities->insertData($insert, 'engagement')) {
               $dateForRe = $this->db->query("SELECT SUM(engagement.EngagementYear) AY FROM engagement WHERE engagement.SailorID = $SAILOR_ID")->row();
                $data2 = $dateForRe->AY;

                 if(strlen($data2) == 1)
                {
                    $data2 = str_pad($data2,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $data2 = $data2;
                }
               $newDate = str_pad($data2,6,"0");
               if(strlen($priodYY) == 1)
                {
                    $priodYY = str_pad($priodYY,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodYY = $priodYY;
                }
                 if(strlen($priodMM) == 1)
                {
                    $priodMM = str_pad($priodMM,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodMM = $priodMM;
                }
                 if(strlen($priodDD) == 1)
                {
                    $priodDD = str_pad($priodDD,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodDD = $priodDD;
                }

                $priod = $priodYY.''.$priodMM.''.$priodDD;

                 $updateEngagement1 = array(
                "ENGAGEMENTPERIOD" =>$priod,
                "ENGAGEMENTTYPE" =>$engagement_type,
                "TOTALENGAGEPERIOD" =>$newDate
            );
                if ($this->utilities->updateData('sailor', $updateEngagement1, array('SAILORID' => $SAILOR_ID))){
                    echo "<div class='alert alert-success'>Engagement Info added successfully</div>";
                }

            }

             else {
                echo "<div class='alert alert-success'>Engagement Info Inserted Failed</div>";
            }
        }else{
            if ($this->utilities->insertData($engagement, 'engagement')) {
                $dateForRe = $this->db->query("SELECT SUM(engagement.EngagementYear) AY FROM engagement WHERE engagement.SailorID = $SAILOR_ID")->row();
                $data2 = $dateForRe->AY;

                 if(strlen($data2) == 1)
                {
                    $data2 = str_pad($data2,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $data2 = $data2;
                }
                $newDate = str_pad($data2,6,"0");
                if(strlen($priodYY) == 1)
                {
                    $priodYY = str_pad($priodYY,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodYY = $priodYY;
                }
                 if(strlen($priodMM) == 1)
                {
                    $priodMM = str_pad($priodMM,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodMM = $priodMM;
                }
                 if(strlen($priodDD) == 1)
                {
                    $priodDD = str_pad($priodDD,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodDD = $priodDD;
                }

                $priod = $priodYY.''.$priodMM.''.$priodDD;
                 $updateEngagement2 = array(
                "ENGAGEMENTPERIOD" =>$priod,
                "ENGAGEMENTTYPE" =>$engagement_type,
                "TOTALENGAGEPERIOD" =>$newDate
            );
                if ($this->utilities->updateData('sailor', $updateEngagement2, array('SAILORID' => $SAILOR_ID))){
                    echo "<div class='alert alert-success'>Engagement Info added successfully</div>";
                }
            } else {
                echo "<div class='alert alert-success'>Engagement Info Inserted Failed</div>";
            }
        }
    }

    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id) {
        $data['result'] = $this->db->query("SELECT e.*, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST,se.AREA_ID AUTHO_AREA_ID, se.NAME AUTHO_SHIP_EST,s.ENGAGEMENTPERIOD, s.ENGAGEMENTTYPE, s.ENGAGEFOR, s.TOTALENGAGEPERIOD, et.NAME entryName
            FROM engagement e
            INNER JOIN sailor s on s.SAILORID = e.SailorID
            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
            INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = e.EngagementType
            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.ShipEstablishmentID
            INNER JOIN bn_entrytype et on et.ENTRY_TYPEID = e.EntryTypeID
            WHERE e.EagagementID = $id")->row();

        //echo"<pre>";print_r($data['result'] );exit;
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));
        $data['shipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['appointment'] = $this->utilities->findAllByAttribute("bn_appointmenttype", array("ACTIVE_STATUS" => 1));
        $data['dao'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['entryType'] = $this->utilities->findAllByAttribute("bn_entrytype", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /* flag status = 0 is Active sailor; status = 1 is retirement sailor; */
        $data['content_view_page'] = 'regularTransaction/engagement_info/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

     public function update() {

        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        $id = $this->input->post('id', true);
        $engagementNoNext = $this->input->post('engagementNoNext', true);
        $engagementDate = date('Y-m-d', strtotime($this->input->post('engagementDate', true)));
        $engagePeriod = $this->input->post('engageFor', true);
        $SHIP_AREA_ID = $this->input->post('SHIP_AREA_ID', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $DAO_NO = $this->input->post('DAO_NO', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_NO));
        $expireDate = $this->input->post('expireDate', true);
        $AUTHO_SHIP_ESTABLISHMENT = $this->input->post('AUTHO_SHIP_ESTABLISHMENT', true);
        $engagement_type = $this->input->post('engagement_type', true);
        $ENTRY_TYPE = $this->input->post('ENTRY_TYPE', true);
        $priodYY = $this->input->post('priodYY', true);
        $priodMM = $this->input->post('priodMM', true);
        $priodDD = $this->input->post('priodDD', true);

        $totalEngagePeriod = $engagePeriod;

        /*$updateEngagement = array(
            "ENGAGEMENTPERIOD" => $priodYY + $priodMM + $priodDD,
            "ENGAGEMENTTYPE" => $engagement_type,
            "TOTALENGAGEPERIOD" => $totalEngagePeriod
        );*/
        $engagement = array(
            'EngagementDate' => $engagementDate,
            'EntryTypeID' => $ENTRY_TYPE,
            'EngagementYear' => $priodYY,
            'EngagementMonth' => $priodMM,
            'EngagementDay' => $priodDD,
            'ExpiryDate' => $expireDate,
            'EngagementNo' => $engagementNoNext,
            'DAOID' => $DAO_NO,
            'DAONo' => $DAO->DAO_NO,
            'ShipEstablishmentID' => $AUTHO_SHIP_ESTABLISHMENT,
            'EngagementType' => $engagement_type,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if (isset($_POST['ncsToCS'])) {
            $ncsToCS = $this->input->post('ncsToCS', true);
            $ncs = array(
                'IsNCSToCS' => $ncsToCS
            );
            $update = array_merge($engagement, $ncs);
            if ($this->utilities->updateData('engagement', $update,array('EagagementID' => $id))){
                 $dateForRe = $this->db->query("SELECT SUM(engagement.EngagementYear) AY FROM engagement WHERE engagement.SailorID = $SAILOR_ID")->row();
                $data2 = $dateForRe->AY;

                 if(strlen($data2) == 1)
                {
                    $data2 = str_pad($data2,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $data2 = $data2;
                }
                $newDate = str_pad($data2,6,"0");
                if(strlen($priodYY) == 1)
                {
                    $priodYY = str_pad($priodYY,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodYY = $priodYY;
                }
                 if(strlen($priodMM) == 1)
                {
                    $priodMM = str_pad($priodMM,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodMM = $priodMM;
                }
                 if(strlen($priodDD) == 1)
                {
                    $priodDD = str_pad($priodDD,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodDD = $priodDD;
                }

                $priod = $priodYY.''.$priodMM.''.$priodDD;
                 $updateEngagement2 = array(
                "ENGAGEMENTPERIOD" =>$priod,
                "ENGAGEMENTTYPE" =>$engagement_type,
                "TOTALENGAGEPERIOD" =>$newDate
            );

                if ($this->utilities->updateData('sailor', $updateEngagement2, array('SAILORID' => $SAILOR_ID))) {
                    echo "<div class='alert alert-success'>Engagement Info Update successfully</div>";
                }
            } else {
                echo "<div class='alert alert-success'>Engagement Info Update Failed</div>";
            }
        } else {
            if ($this->utilities->updateData('engagement', $engagement,array('EagagementID' => $id))) {
                 $dateForRe = $this->db->query("SELECT SUM(engagement.EngagementYear) AY FROM engagement WHERE engagement.SailorID = $SAILOR_ID")->row();
                $data2 = $dateForRe->AY;

                 if(strlen($data2) == 1)
                {
                    $data2 = str_pad($data2,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $data2 = $data2;
                }
                $newDate = str_pad($data2,6,"0");
                if(strlen($priodYY) == 1)
                {
                    $priodYY = str_pad($priodYY,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodYY = $priodYY;
                }
                 if(strlen($priodMM) == 1)
                {
                    $priodMM = str_pad($priodMM,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodMM = $priodMM;
                }
                 if(strlen($priodDD) == 1)
                {
                    $priodDD = str_pad($priodDD,2,"0",STR_PAD_LEFT);
                }
                else
                {
                    $priodDD = $priodDD;
                }

                $priod = $priodYY.''.$priodMM.''.$priodDD;
                 $updateEngagement1 = array(
                "ENGAGEMENTPERIOD" =>$priod,
                "ENGAGEMENTTYPE" =>$engagement_type,
                "TOTALENGAGEPERIOD" =>$newDate
            );
                if ($this->utilities->updateData('sailor', $updateEngagement1, array('SAILORID' => $SAILOR_ID))) {
                    echo "<div class='alert alert-success'>Engagement Info Update successfully</div>";
                }
            } else {
                echo "<div class='alert alert-success'>Engagement Info Update Failed</div>";
            }
        }
    }

    public function view($id) {
        $data['viewdetails'] = $this->db->query("SELECT e.*, s.OFFICIALNUMBER,  s.FULLNAME, r.RANK_NAME , s.POSTINGDATE, ap.NAME Appoint_Type, se.NAME SHIP_EST,se.AREA_ID AUTHO_AREA_ID, se.NAME AUTHO_SHIP_EST,s.ENGAGEMENTPERIOD, s.ENGAGEMENTTYPE, s.ENGAGEFOR, s.TOTALENGAGEPERIOD, et.NAME entryName
            FROM engagement e
            INNER JOIN sailor s on s.SAILORID = e.SailorID
            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
            INNER JOIN bn_appointmenttype ap on ap.APPOINT_TYPEID = e.EngagementType
            INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.ShipEstablishmentID
            INNER JOIN bn_entrytype et on et.ENTRY_TYPEID = e.EntryTypeID
            WHERE e.EagagementID = $id")->row();
     // echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/engagement_info/view', $data);
    }

    /**
     * @param       none
     * @author      Reazul Islam <reazul@atilimited.net>
     * @return      Search Engagement information
     */
    function searchEngagementInfo(){
        $id = $this->input->post('sailorId', true);
        $data['engagement'] = $this->db->query("SELECT e.*, s.OFFICIALNUMBER, s.ENGAGEMENTPERIOD, s.ENGAGEMENTTYPE, s.ENGAGEFOR, s.TOTALENGAGEPERIOD, et.NAME entryName
                                                FROM engagement  e
                                                JOIN sailor s on s.SAILORID = e.SailorID
                                                JOIN bn_entrytype et on et.ENTRY_TYPEID = e.EntryTypeID
                                                WHERE e.SailorID = $id")->result();
        $this->load->view('regularTransaction/engagement_info/search_engagement_info', $data);
    }

    /**
     * @access      public
     * @param       officiar number
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      Sailor Info, Rank_name, Ship_establishment, Posting unit, date
     */
    function searchSailorInfoByOfficalNo(){
        $officalNumber = $this->input->post("officeNumber");
        $sailorStatus = $this->input->post("sailorStatus");
        $this->db->select('s.*,et.NAME ENTRY_TYPE_NAME, r.RANK_NAME, se.NAME SHIP_ESTABLISHMENT, pu.NAME POSTING_UNIT_NAME, DATE_FORMAT(s.POSTINGDATE,"%d-%m-%Y") POSTING_DATE');
        $this->db->from('sailor as s');
        $this->db->join('bn_posting_unit as pu', 'pu.POSTING_UNITID = s.POSTINGUNITID','INNER');
        $this->db->join('bn_rank as r', 'r.RANK_ID = s.RANKID','INNER');
        $this->db->join('bn_ship_establishment as se', 'se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID', 'INNER');
        $this->db->join('bn_entrytype as et', 'et.ENTRY_TYPEID = s.ENGAGEMENTTYPE', 'LEFT');
        $condition = array('s.OFFICIALNUMBER'=> $officalNumber, 's.SAILORSTATUS'=> $sailorStatus);
        $this->db->where($condition);
        echo json_encode($this->db->get()->row_array());
    }
    function countEngagementInfo(){
        $officalNumber = $this->input->post("officeNumber");
        $this->db->select('e.*');
        $this->db->from('engagement as e');
        $this->db->join('sailor as s', 's.SAILORID = e.SailorID','INNER');
        $this->db->where('s.OFFICIALNUMBER', $officalNumber);
        $count = $this->db->count_all_results();
        echo $count;

    }
    function engagementCurrentInfo(){

        $officalNumber = $this->input->post("officeNumber");
        /*Find engagement already exit or not*/
        $query = $this->db->query("SELECT sailor.SAILORID FROM sailor WHERE sailor.OFFICIALNUMBER = $officalNumber")->row();

        $engagement = $this->db->query("SELECT max(e.EagagementID) ID, e.EntryTypeID, e.EngagementYear, DATE_FORMAT(e.EngagementDate,'%d-%m-%Y') EngagementDate, DATE_FORMAT(e.ExpiryDate,'%d-%m-%Y') ExpiryDate
                                        FROM engagement e
                                        WHERE e.SailorID = $query->SAILORID AND e.EagagementID in (SELECT MAX(EagagementID) FROM engagement WHERE engagement.SailorID = $query->SAILORID)")->row_array();
        if(!empty($engagement)){
            echo json_encode($engagement);
        }
    }

}

/* End of file reEngagementInfo.php */
/* Location: ./application/controllers/regularTransaction/reEngagementInfo.php */
