<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Transfer
 * @package    Transfer Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class TransferInfo extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access    public
     * @param     none
     * @author    Emdadul Huq<Emdadul@atilimited.net>
     * @return    View index template
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Transfer Information';
        $data['content_view_page'] = 'regularTransaction/transfer_info/index';
        $this->template->display($data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     View modal
     */
    public function create() {
        $data['appointmentType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_appointmenttype", array("ACTIVE_STATUS" => 1),"NAME");
        $data['shipEstablishment'] = $this->utilities->dropdownFromTableWithCondition('bn_ship_establishment', ' Select Ship Establishment ', 'SHIP_ESTABLISHMENTID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['traiiningType'] = $this->utilities->dropdownFromTableWithCondition('bn_navytraininghierarchy', ' Select Training Type ', 'NAVYTrainingID','Name', $condition = array("ACTIVE_STATUS" => 1, "NAVYTrainingType =" => '2'));
        /*test*/

        $data['postingUnit'] = $this->utilities->dropdownFromTableWithCondition('bn_posting_unit', '---Select---', 'POSTING_UNITID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['Appointment'] = $this->utilities->dropdownFromTableWithCondition('bn_appointmenttype', '---Select---', 'APPOINT_TYPEID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['batchNumber'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', '---Select---', 'BATCH_ID', 'BATCH_NUMBER', $condition = array('ACTIVE_STATUS' => 1));
        $data['courseTraining'] = $this->utilities->dropdownFromTableWithCondition('bn_navytraininghierarchy', '---Select---', 'NAVYTrainingID', 'Name', $condition = array('NAVYTrainingType' => 3));

        $data['postingUnit_one'] = $this->utilities->findAllByAttributeWithOrderBy('bn_posting_unit', array('ACTIVE_STATUS' => 1),"NAME");
        $data['Appointment_one'] = $this->utilities->findAllByAttributeWithOrderBy('bn_appointmenttype', array('ACTIVE_STATUS' => 1),"NAME");
        $data['batchNumber_one'] = $this->utilities->findAllByAttributeWithOrderBy('bn_batchnumber', array('ACTIVE_STATUS' => 1),"BATCH_NUMBER");
        $data['courseTraining_one'] = $this->utilities->findAllByAttributeWithOrderBy('bn_navytraininghierarchy', array('NAVYTrainingType' => 3),"NAME");

        $data['examGrade_one'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
        $data['examResul_one'] = $this->utilities->findAllByAttribute("bn_exam_result", array("ACTIVE_STATUS" => 1));

        $data['content_view_page'] = 'regularTransaction/transfer_info/create';
        $this->template->display($data);
    }

    /**
     * @access     public
     * @param      none
     * @author     Emdadul Huq<Emdadul@atilimited.net>
     * @return     no
     */
    public function transfer_save() {

        $OFFICIAL_NO = $this->input->post('OFFICIAL_NO', true);
        $d_PostingUnit = $this->input->post('d_PostingUnit', true);
        $Appointment = $this->input->post('Appointment', true);
        $ORDER_NO = $this->input->post('ORDER_NO', true);
        $ORDER_DATE = $this->input->post('ORDER_DATE', true);
        $Effect = $this->input->post('Effect', true);
        $course = $this->input->post('course', true);
        $batchNumber = $this->input->post('batchNumber', true);
        $REMARKS = $this->input->post('REMARKS', true);

        $success = 0;
        for ($i = 0; $i < count($OFFICIAL_NO); $i++) {
            $batchNo = $this->utilities->findByAttribute("bn_batchnumber", array("BATCH_ID" => $batchNumber[$i]));
            $BatchNumber[] = $batchNo->BATCH_NUMBER;
            $row = $this->utilities->findByAttribute("sailor", array('OFFICIALNUMBER' => $OFFICIAL_NO[$i]));
            $sailorId[] = $row->SAILORID;
            $j = $i + 1;
            if($this->input->post("temporary_$j", true)){
                $temp = $this->input->post("temporary_$j", true);
            }else{
                $temp = 0;
            }
            $transfer = array(
                'SailorID' => $sailorId[$i],
                'TODate' => date('Y-m-d', strtotime( $ORDER_DATE[$i])),
                'TONumber' => $ORDER_NO[$i],
                'PostingUnitID  ' => $d_PostingUnit[$i],
                'EWDate' => date('Y-m-d', strtotime( $Effect[$i])),
                'IsTemp' => $temp,
                'TransferType' => 0,
                'IsHeldOver' => 0,
                'BatchNumber' => $BatchNumber[$i],
                'CourseID' => $course[$i],
                'AppointmentTypeID' => $Appointment[$i],
                'Remarks' => $REMARKS[$i],
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            if ($this->utilities->insertData($transfer, 'transfer')) {
                $success = 1;
            }
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Transfer Information Insert successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Transfer Information Insert Failed</div>";
        }
    }

    /**
     * @access      public
     * @param       Transfer Id
     * @author      Emdadul Huq <emdadul@atilimited.net>
     * @return      Edit page
     */
    public function transfer_edit($id) {
        $data['result'] = $this->db->query("SELECT t.*,  s.OFFICIALNUMBER, s.FULLNAME,  s.POSTINGDATE, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME, tpu.SHIP_ESTABLISHMENTID tShipId, nt.ParentID TrainningType
                                            FROM transfer t
                                            INNER JOIN sailor s on s.SAILORID = t.SailorID
                                            INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                            INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = s.POSTINGUNITID
                                            INNER JOIN bn_posting_unit tpu on tpu.POSTING_UNITID = t.PostingUnitID
                                            LEFT JOIN bn_navytraininghierarchy nt on nt.NAVYTrainingID = t.CourseID
                                            WHERE t.TransferID = $id")->row();
        $batch = $data['result']->BatchNumber;
        $data['batchId'] = $this->db->query("SELECT BATCH_ID FROM bn_batchnumber WHERE BATCH_NUMBER = '$batch' ")->row();

        $data['appointmentType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_appointmenttype", array("ACTIVE_STATUS" => 1),"NAME");
        $data['shipEstablishment'] = $this->utilities->dropdownFromTableWithCondition('bn_ship_establishment', ' Select Ship Establishment ', 'SHIP_ESTABLISHMENTID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['traiiningType'] = $this->utilities->dropdownFromTableWithCondition('bn_navytraininghierarchy', ' Select Training Type ', 'NAVYTrainingID','Name', $condition = array("ACTIVE_STATUS" => 1, "NAVYTrainingType =" => '2'));
        /*test*/

        $data['postingUnit'] = $this->utilities->dropdownFromTableWithCondition('bn_posting_unit', '---Select---', 'POSTING_UNITID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['Appointment'] = $this->utilities->dropdownFromTableWithCondition('bn_appointmenttype', '---Select---', 'APPOINT_TYPEID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['batchNumber'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', '---Select---', 'BATCH_ID', 'BATCH_NUMBER', $condition = array('ACTIVE_STATUS' => 1));
        $data['courseTraining'] = $this->utilities->dropdownFromTableWithCondition('bn_navytraininghierarchy', '---Select---', 'NAVYTrainingID', 'Name', $condition = array('NAVYTrainingType' => 3));

        $data['postingUnit_one'] = $this->utilities->findAllByAttributeWithOrderBy('bn_posting_unit', array('ACTIVE_STATUS' => 1),"NAME");
        $data['Appointment_one'] = $this->utilities->findAllByAttribute('bn_appointmenttype', array('ACTIVE_STATUS' => 1));
        $data['batchNumber_one'] = $this->utilities->findAllByAttribute('bn_batchnumber', array('ACTIVE_STATUS' => 1));
        $data['courseTraining_one'] = $this->utilities->findAllByAttribute('bn_navytraininghierarchy', array('NAVYTrainingType' => 3));

        $data['examGrade_one'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
        $data['examResul_one'] = $this->utilities->findAllByAttribute("bn_exam_result", array("ACTIVE_STATUS" => 1));


        $data['content_view_page'] = 'regularTransaction/transfer_info/transfer_edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  all movement info
     * @return  //
     */

    public function transfer_update() {
        $id = $this->input->post('id', true);
        $OFFICIAL_NO = $this->input->post('OFFICIAL_NO', true);
        $d_PostingUnit = $this->input->post('d_PostingUnit', true);
        $Appointment = $this->input->post('Appointment', true);
        $ORDER_NO = $this->input->post('ORDER_NO', true);
        $ORDER_DATE = $this->input->post('ORDER_DATE', true);
        $Effect = $this->input->post('Effect', true);
        $course = $this->input->post('course', true);
        $batchNumber = $this->input->post('batchNumber', true);
        $REMARKS = $this->input->post('REMARKS', true);

        $success = 0;
        for ($i = 0; $i < count($OFFICIAL_NO); $i++) {
            $batchNo = $this->utilities->findByAttribute("bn_batchnumber", array("BATCH_ID" => $batchNumber[$i]));
            $BatchNumber[] = $batchNo->BATCH_NUMBER;
            $j = $i + 1;
            if($this->input->post("temporary_$j", true)){
                $temp = $this->input->post("temporary_$j", true);
            }else{
                $temp = 0;
            }
            $transfer = array(
                'TODate' => date('Y-m-d', strtotime( $ORDER_DATE[$i])),
                'TONumber' => $ORDER_NO[$i],
                'PostingUnitID  ' => $d_PostingUnit[$i],
                'EWDate' => date('Y-m-d', strtotime( $Effect[$i])),
                'IsTemp' => $temp,
                'TransferType' => 0,
                'IsHeldOver' => 0,
                'BatchNumber' => $BatchNumber[$i],
                'CourseID' => $course[$i],
                'AppointmentTypeID' => $Appointment[$i],
                'Remarks' => $REMARKS[$i],
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
            );
            if ($this->utilities->updateData('transfer', $transfer, array('TransferID' => $id))) {
                $success = 1;
            }
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Transfer Information Updatee successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Transfer Information Updated Failed</div>";
        }
    }

    /**
     * @param      Transfer Id
     * @author     Emdadul Huq <emdadul@atilimited.net>
     * @return     transfer view data
     */
    function TransferInfoExecute(){
        $id = $this->input->post('transferId', true);
        $row = $this->utilities->findByAttribute("transfer", array("TransferID" => $id));
        $ship = $this->utilities->findByAttribute("bn_posting_unit", array("POSTING_UNITID" => $row->PostingUnitID));

        /*tramsfer info add into movement array*/
        $movement = array(
            'SailorID' => $row->SailorID,
            'AppointmentTypeID' => $row->AppointmentTypeID,
            'ShipEstablishmentID' => $ship->SHIP_ESTABLISHMENTID,
            'PostingUnitID' => $row->PostingUnitID,
            'DraftInDate' => $row->TODate,
            'TransferOrderNo' => $row->TONumber,
            'TransferOrderDate' => $row->EWDate,
            'Remarks' => $row->Remarks,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($movement, 'movement')) {
            /*Posting type Permanent=1, Temporary = 2 */
            if($row->IsTemp == 0){
                $postingType = 2;
            }else{
                $postingType = 1;
            }
            /*end */
            $sailorUpdate = array(
                    'POSTINGUNITID' => $row->PostingUnitID,
                    'POSTINGDATE' => $row->TODate,
                    'POSTINGTYPE' => $postingType,
                    'APPOINTMENTTYPEID' => $row->AppointmentTypeID
                );
            if ($this->utilities->updateData('sailor', $sailorUpdate, array('SAILORID' => $row->SailorID))) {
                $result = $this->utilities->deleteRowByAttribute('transfer', array("TransferID" => $id ));
                if(!empty($result)){
                    echo "Y";
                }
            }
        }else{
            echo "N";
        }
    }
    /**
     * @param      Transfer Id
     * @author     Emdadul Huq <emdadul@atilimited.net>
     * @return     transfer view data
     */

    public function transfer_view($id) {
        $data['viewdetails'] = $this->db->query("SELECT t.*,  s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, at.NAME AP_TYPE_NAME, training.Name Training_Name
                                                FROM transfer t
                                                INNER JOIN sailor s on s.SAILORID = t.SailorID
                                                INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
                                                INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                                INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = t.PostingUnitID
                                                INNER JOIN bn_appointmenttype at on at.APPOINT_TYPEID = t.AppointmentTypeID
                                                LEFT JOIN bn_navytraininghierarchy training on training.NAVYTrainingID = t.CourseID
                                                WHERE t.TransferID = $id")->row();
        //echo '<pre>';print_r($data['viewdetails']);exit;
        $this->load->view('regularTransaction/transfer_info/transfer_view', $data);
    }

    function ajaxTransferInfoList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            // t.TransferID, t.TONumber , s.OFFICIALNUMBER, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, at.NAME AP_TYPE_NAME
            0 => 't.TransferID', 1 => 's.OFFICIALNUMBER', 2 => 'SHIP_EST', 3 => 'POSTING_UNIT', 4 => 't.TONumber',5 => 'AP_TYPE_NAME', 6 => 't.TransferID');

        // getting total number records without any search

        $query = $this->db->query("SELECT t.TransferID, t.TONumber , s.OFFICIALNUMBER, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, at.NAME AP_TYPE_NAME
                                    FROM transfer t
                                    INNER JOIN sailor s on s.SAILORID = t.SailorID
                                    INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                    INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = t.PostingUnitID
                                    INNER JOIN bn_appointmenttype at on at.APPOINT_TYPEID = t.AppointmentTypeID")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT t.TransferID, t.TONumber , s.OFFICIALNUMBER, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, at.NAME AP_TYPE_NAME
                                        FROM transfer t
                                        INNER JOIN sailor s on s.SAILORID = t.SailorID
                                        INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                        INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = t.PostingUnitID
                                        INNER JOIN bn_appointmenttype at on at.APPOINT_TYPEID = t.AppointmentTypeID
                                        WHERE s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT t.TransferID, t.TONumber , s.OFFICIALNUMBER, se.NAME SHIP_EST, pu.NAME POSTING_UNIT, at.NAME AP_TYPE_NAME
                                        FROM transfer t
                                        INNER JOIN sailor s on s.SAILORID = t.SailorID
                                        INNER JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = s.SHIPESTABLISHMENTID
                                        INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = t.PostingUnitID
                                        INNER JOIN bn_appointmenttype at on at.APPOINT_TYPEID = t.AppointmentTypeID
                                        ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->SHIP_EST;
            $nestedData[] = $row->POSTING_UNIT;
            $nestedData[] = $row->TONumber;
            $nestedData[] = $row->AP_TYPE_NAME;
            $nestedData[] = '<a class="btn btn-info btn-xs transfer" id="' . $row->TransferID . '"  sn="' . $sn . '" title="Click to Transfer Execute" type="button"><span class="glyphicon glyphicon glyphicon-transfer">Execute</span></a> ' .
                    '<a class="btn btn-success btn-xs modalLink" href="' . site_url('regularTransaction/TransferInfo/transfer_view/' . $row->TransferID) . '" title="View Transfer Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> ' .
                    '<a class="btn btn-warning btn-xs" href="' . site_url('regularTransaction/TransferInfo/transfer_edit/' . $row->TransferID) . '" title="Edit Transfer Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->TransferID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="TransferID" data-tbl="transfer"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

    public function ajax_get_postingUnit() {
        $ship_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_posting_unit', array('SHIP_ESTABLISHMENTID' => $ship_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->POSTING_UNITID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    public function ajax_get_trainingName() {
        $trainingType_id = $_POST['selectedValue'];
        $query = $this->db->get_where('bn_navytraininghierarchy', array('ParentID' => $trainingType_id))->result();
        $returnVal = '<option value="">Select one</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value="' . $row->NAVYTrainingID . '">' . $row->Name . '</option>';
            }
        }
        echo $returnVal;
    }


}
/* End of file transferInfo.php */
/* Location: ./application/controllers/regularTransaction/transferInfo.php */
