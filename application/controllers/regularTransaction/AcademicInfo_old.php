<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Academic information
 * @author     Emran Hossen <emran@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class AcademicInfo extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen<emran@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Academic Information';
        $data['flag'] = 0; /* flag status = 0 is Active sailor; status = 1 is retirement sailor; */
        $data['content_view_page'] = 'regularTransaction/AcademicInfo/index';
        $this->template->display($data);
    }

    function ajaxAcamedicList() {
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(
            // datatable column index  => database column name
            0 => 's.OFFICIALNUMBER', 1 => 'ex.NAME', 2 => 'bo.NAME', 3 => 'sg.NAME', 4 => 'eg.NAME', 5 => 'a.PassingYear');

        // getting total number records without any search

        $query = $this->db->query("SELECT a.AcademicID,a.SailorID,s.OFFICIALNUMBER,       
                                    ex.NAME AS Exam_Name,
                                    bo.NAME AS Board,
                                    sg.NAME AS Subject_Group,
                                    a.PassingYear,
                                    (select case a.EvaluationSystem when 1 then 'Traditional' when 2 then 'GPA' else null end) as Exam_System,
                                    eg.NAME AS Result,
                                    a.ResultDescription,
                                    a.TotalMarks,
                                    a.Percentage
                               FROM academic a
                                    LEFT JOIN Sailor s ON a.SailorID = s.SAILORID
                                    LEFT JOIN bn_govtexam_hierarchy ex ON a.ExamID = ex.GOVT_EXAMID
                                    LEFT JOIN bn_boardcenter bo ON a.BoardID = bo.BOARD_CENTERID
                                    LEFT JOIN bn_subject_group sg ON a.SubjectID = sg.SUBJECT_GROUPID
                                    LEFT JOIN bn_examgrade eg ON a.ExamGradeID = eg.EXAM_GRADEID
                                    WHERE a.ACTIVE_STATUS = 1")->num_rows();

        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT a.AcademicID,a.SailorID,s.OFFICIALNUMBER,       
                                    ex.NAME AS Exam_Name,
                                    bo.NAME AS Board,
                                    sg.NAME AS Subject_Group,
                                    a.PassingYear,
                                    (select case a.EvaluationSystem when 1 then 'Traditional' when 2 then 'GPA' else null end) as Exam_System,
                                    eg.NAME AS Result,
                                    a.ResultDescription,
                                    a.TotalMarks,
                                    a.Percentage
                               FROM academic a
                                    LEFT JOIN Sailor s ON a.SailorID = s.SAILORID
                                    LEFT JOIN bn_govtexam_hierarchy ex ON a.ExamID = ex.GOVT_EXAMID
                                    LEFT JOIN bn_boardcenter bo ON a.BoardID = bo.BOARD_CENTERID
                                    LEFT JOIN bn_subject_group sg ON a.SubjectID = sg.SUBJECT_GROUPID
                                    LEFT JOIN bn_examgrade eg ON a.ExamGradeID = eg.EXAM_GRADEID
                                    WHERE a.ACTIVE_STATUS = 1
                                    AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] . "%' OR ex.NAME LIKE '" . $requestData['search']['value'] . "%' OR bo.NAME LIKE '" . $requestData['search']['value'] .
                            "%' OR sg.NAME LIKE '" . $requestData['search']['value'] . "%'OR eg.NAME LIKE '" . $requestData['search']['value'] . "%'OR a.PassingYear LIKE '" . $requestData['search']['value'] .
                            "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
            /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query
        } else {

            $query = $this->db->query("SELECT a.AcademicID, a.SailorID,s.OFFICIALNUMBER,       
                                    ex.NAME AS Exam_Name,
                                    bo.NAME AS Board,
                                    sg.NAME AS Subject_Group,
                                    a.PassingYear,
                                    (select case a.EvaluationSystem when 1 then 'Traditional' when 2 then 'GPA' else null end) as Exam_System,
                                    eg.NAME AS Result,
                                    a.ResultDescription,
                                    a.TotalMarks,
                                    a.Percentage
                               FROM academic a
                                    LEFT JOIN Sailor s ON a.SailorID = s.SAILORID
                                    LEFT JOIN bn_govtexam_hierarchy ex ON a.ExamID = ex.GOVT_EXAMID
                                    LEFT JOIN bn_boardcenter bo ON a.BoardID = bo.BOARD_CENTERID
                                    LEFT JOIN bn_subject_group sg ON a.SubjectID = sg.SUBJECT_GROUPID
                                    LEFT JOIN bn_examgrade eg ON a.ExamGradeID = eg.EXAM_GRADEID
                                    WHERE a.ACTIVE_STATUS = 1
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn = 1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->Exam_Name;
            $nestedData[] = $row->Board;
            $nestedData[] = $row->Subject_Group;
            $nestedData[] = $row->PassingYear;
            $nestedData[] = $row->Result;
            $nestedData[] = $row->TotalMarks;
            $nestedData[] = $row->Percentage;
            $nestedData[] = '<a class="btn btn-warning btn-xs" href="' . site_url('regularTransaction/AcademicInfo/edit/' . $row->AcademicID) . '" title="Edit Academic Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> ' .
                    '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="' . $row->AcademicID . '" sn="' . $sn++ . '" title="Click For Delete" data-type="delete" data-field="AcademicID" data-tbl="academic"><span class="glyphicon glyphicon-trash"></span></a>';

            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

                // total data array
        );

        echo json_encode($json_data);
    }

    public function create() {
        $data['pageTitle'] = 'Academic Information';
        $data['academicInfo'] = $this->db->query("SELECT 
                                 academic.ExamID,
                                (SELECT e.name
                                    FROM bn_govtexam_hierarchy e
                                    WHERE e.GOVT_EXAMID = academic.ExamID)
                                    AS Exam_Name,

                            (SELECT b.NAME
                                    FROM bn_boardcenter b
                                    WHERE b.BOARD_CENTERID = academic.BoardID)
                                    AS Board_Name,
                                    academic.BoardID,
                            (SELECT sg.NAME
                                    FROM bn_subject_group sg
                                    WHERE sg.SUBJECT_GROUPID = academic.SubjectID)
                                    Subject,
                                    academic.SubjectID,
                            PassingYear,
                            EvaluationSystem,
                            (SELECT CASE EvaluationSystem
                                                    WHEN 1 THEN 'Traditional'
                                                    WHEN 2 THEN 'GPA'
                                                    ELSE NULL
                                            END)
                                    AS Exam_System,
                            (SELECT ex.NAME
                                    FROM bn_examgrade ex
                                    WHERE ex.EXAM_GRADEID = academic.ExamGradeID)
                                    AS Exam_Gred,
                                    academic.ExamGradeID,
                                    academic.EducationStatus AS EDUST,
                            ResultDescription,
                            TotalMarks,
                            Percentage,
                                            (SELECT CASE EducationStatus
                                                    WHEN 1 THEN 'Entry Education'
                                                    WHEN 2 THEN 'Entry Period Highest'
                                                    when 3 then 'Last'
                                                    ELSE NULL
                                            END)
                                    AS EducationStatus, 
    academic.AcademicID     
                    FROM academic 
                    WHERE SailorID = $SAILORID")->result();
////        $data['result3'] = $this->db->query("SELECT nh.* FROM academic nh")->row();
////       // echo"<pre>";print_r( $data['result3']->AcademicID);exit;
////        if( $data['result3']->AcademicID == ''){
////            echo 'insert';
////        }else{
////            echo 'update';
////        }
//
//        $data['passingyear'] = $this->utilities->dropdownFromTableWithCondition('academic', ' Select Passing Year', 'AcademicID', 'PassingYear', $condition = array("ACTIVE_STATUS" => 1));
//
//
//        $data['result12'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', 'Select Result', 'EXAM_GRADEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//        $data['localNumber'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Local Number', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 2));
//
//        $data['batch'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', 'Select Batch', 'BATCH_ID', 'BATCH_NUMBER');
//
//        /* $data['entryType'] = $this->utilities->dropdownFromTableWithCondition('bn_entrytype', 'Select Type', '
//          ENTRY_TYPEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1)); */
//
//        $data ['relation'] = $this->utilities->dropdownFromTableWithCondition('bn_relation', 'Select Relation', 'RELATION_ID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//
//        $data['postingUnit'] = $this->utilities->dropdownFromTableWithCondition('bn_posting_unit', 'Select Type', 'POSTING_UNITID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//
//
//        $data['exam_name'] = $this->utilities->dropdownFromTableWithCondition('bn_govtexam_hierarchy', 'Select Exam Name', 'GOVT_EXAMID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//
//        $data['subject_group'] = $this->utilities->dropdownFromTableWithCondition('bn_subject_group', 'Select Group Name', 'SUBJECT_GROUPID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//
//        $data['board_university'] = $this->utilities->dropdownFromTableWithCondition('bn_boardcenter', 'Select Bard/University', 'BOARD_CENTERID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//
//        $data['ExamGrade'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', 'Select Result', 'EXAM_GRADEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//
//        $data['district'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Division ', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 2));
//        $data['division1'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', 'Select Division', 'BD_ADMINID', 'NAME', $condition = array("PARENT_ID" => 0));
//
//        $data['thana'] = $this->utilities->dropdownFromTableWithCondition('bn_bdadminhierarchy', ' Select Thana ', 'BD_ADMINID', 'NAME', $condition = array("BDADMIN_TYPE" => 3));
//
//        $data['area'] = $this->utilities->findAllByAttribute('bn_navyadminhierarchy', array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => '2'));
//        $data['dis'] = $this->utilities->findAllByAttribute('bn_bdadminhierarchy', array("ACTIVE_STATUS" => 1, "BDADMIN_TYPE" => '2'));
//
//
//        $data['establishment'] = $this->utilities->dropdownFromTableWithCondition('bn_ship_establishment', ' Select Shift ', 'SHIP_ESTABLISHMENTID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
//
//        $data['rankData'] = $this->utilities->findAllByAttribute("bn_rank", array("ACTIVE_STATUS" => 1));
////    echo"<pre>";print_r($data['rankData']);exit;
//
//        $data['entryType1'] = $this->utilities->findAllByAttribute('bn_entrytype', array("ACTIVE_STATUS" => 1));
//
//        $data['zone'] = $this->utilities->findAllByAttribute('bn_navyadminhierarchy', array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => '1'));
//
//        $data['division'] = $this->utilities->findAllByAttribute('bn_bdadminhierarchy', array("ACTIVE_STATUS" => 1));

        $data ['relation1'] = $this->utilities->findAllByAttribute('bn_relation', array("ACTIVE_STATUS" => 1));
        $data['exam_name1'] = $this->utilities->findAllByAttribute('bn_govtexam_hierarchy', array("ACTIVE_STATUS" => 1));
        $data['subject_group1'] = $this->utilities->findAllByAttribute('bn_subject_group', array("ACTIVE_STATUS" => 1));
        $data['board_university1'] = $this->utilities->findAllByAttribute('bn_boardcenter', array("ACTIVE_STATUS" => 1));
        $data['results1'] = $this->utilities->findAllByAttribute('bn_examgrade', array("ACTIVE_STATUS" => 1));
        $data['thanapolice'] = $this->utilities->findAllByAttribute('bn_bdadminhierarchy', array("BDADMIN_TYPE" => 3, "ACTIVE_STATUS" => 1));

        $data['exam_name'] = $this->utilities->dropdownFromTableWithCondition('bn_govtexam_hierarchy', 'Select Exam Name', 'CODE', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['subject_group'] = $this->utilities->dropdownFromTableWithCondition('bn_subject_group', 'Select Group Name', 'SUBJECT_GROUPID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['board_university'] = $this->utilities->dropdownFromTableWithCondition('bn_boardcenter', 'Select Bard/University', 'BOARD_CENTERID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['result'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', 'Select Result', 'EXAM_GRADEID', 'NAME', $condition = array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'regularTransaction/AcademicInfo/create';
        $this->template->display($data);
    }

    //emran

    public function getEduData() {
        $examSystem = $this->input->post("examSystem");
        $examSystem_name = $this->input->post("examSystem_name");
        $examNameDesc = $this->input->post("examNameDesc");
        $examNameDesc_name = $this->input->post("examNameDesc_name");
        $subjectGroupDesc = $this->input->post("subjectGroupDesc");
        $subjectGroupDesc_name = $this->input->post("subjectGroupDesc_name");
        $passingYear = $this->input->post("passingYear");
        $baordUniversityDesc = $this->input->post("baordUniversityDesc");
        $baordUniversityDesc_name = $this->input->post("baordUniversityDesc_name");
        $result = $this->input->post("result");
        $result_n = $this->input->post("result_n");
        $resultdesc = $this->input->post("resultDesc");
        $eduStatusDesc = $this->input->post("eduStatusDesc");
        $eduStatusDesc_name = $this->input->post("eduStatusDesc_name");
        $percent = $this->input->post("percent");
        $traditional = $this->input->post("traditional");
        $gpa = $this->input->post("gpa");
        echo '<tr>            
        <td class="text-center">' . $examSystem_name . '<input type="hidden" name="examSystemTable[]" class="examSystem" value="' . $examSystem . '" /></td>
        <td class="text-center">' . $examNameDesc_name . '<input type="hidden" name="examNameDescTable[]" class="eximId" value="' . $examNameDesc . '" /></td>
        <td class="text-center">' . $subjectGroupDesc_name . '<input type="hidden" name="subjectGroupDescTable[]" class="subjectGroupDesc" value="' . $subjectGroupDesc . '" /></td>
        <td class="text-center">' . $passingYear . '<input type="hidden" name="passingYearTable[]" class="passingYear" value="' . $passingYear . '" /></td>
        <td class="text-center">' . $baordUniversityDesc_name . '<input type="hidden" name="baordUniversityDescTable[]" class="baordUniversityDesc" value="' . $baordUniversityDesc . '" /></td>
        <td class="text-center">' . $result_n . '<input type="hidden" name="resultTable[]" class="result" value="' . $result . '" /></td>
        <td class="text-center">' . $gpa . '<input type="hidden" name="gpaTable[]" class="gpa" value="' . $gpa . '" /></td>
        <td class="text-center">' . $traditional . '<input type="hidden" name="traditionTable[]" class="eximId" value="' . $traditional . '" /></td>  
        <td class="text-center">' . $percent . '<input type="hidden" name="percentTable[]" class="percent" value="' . $percent . '" /></td>
        <td class="text-center">' . $resultdesc . '<input type="hidden" name="resultdescTable[]" class="resultdesc" value="' . $resultdesc . '" /></td>
        <td class="text-center">' . $eduStatusDesc_name . '<input type="hidden" name="eduStatusDescTable[]" class="eduStatusDesc" value="' . $eduStatusDesc . '" /></td>
        <td class="text-center"><span class="removeEdu btn btn-danger  btn-xs">X</span></td>
    </tr>';
    }

    public function save() {
        /* Academic Information */
        $sailorId = $this->input->post('sailorId', true);
        $examSystem = $this->input->post('examSystemTable', true);
        $examName = $this->input->post('examNameDescTable', true);
        $subject = $this->input->post('subjectGroupDescTable', true);
        $passingYear = $this->input->post('passingYearTable', true);
        $boardUniversity = $this->input->post('baordUniversityDescTable', true);
        $result = $this->input->post('resultTable', true);
        $gpaName = $this->input->post('gpaTable', true);
        $percentage = $this->input->post('percentTable');
        $resultDescription = $this->input->post('resultdescTable', true);
        $educationStatus = $this->input->post('eduStatusDescTable', true);
        $traditional = $this->input->post('traditionTable', true);
        /* end Education insert part */
        for ($i = 0; $i < count($examName); $i++) {
            $academicInf = array(
                'SailorID' => $sailorId,
                'ExamID' => $examName[$i],
                'EvaluationSystem' => $examSystem[$i],
                'BoardID' => $boardUniversity[$i],
                'SubjectID' => $subject[$i],
                'PassingYear' => $passingYear[$i],
                'ExamGradeID' => $result[$i],
                'TotalMarks' => $traditional[$i],
                'Percentage' => $percentage[$i],
                'ResultDescription' => $resultDescription[$i],
                'EducationStatus' => $educationStatus[$i],
                'CRE_BY' => $this->user_session["USER_ID"],
                'CRE_DT' => date("Y-m-d h:i:s a")
            );
            if ($courseTranId = $this->utilities->insertData($academicInf, 'academic')) {
                /* end Academic insert part */
                $success = 1;
            }
        }
        if ($success == 1) {
            echo "<div class='alert alert-success'>Academic Information successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Academic Information successfully</div>";
        }
    }

    //emran

    public function edit($id) {
        $data['pageTitle'] = 'Academic Information';
        $data['results'] = $this->db->query("SELECT a.AcademicID,
                                            a.SailorID AS SailorID,
                                            s.OFFICIALNUMBER,
                                            s.FULLNAME,
                                            r.RANK_NAME,
                                            po.NAME AS postingUnit,       
                                            DATE_FORMAT(s.POSTINGDATE,'%d/%m/%Y') AS POSTINGDATE,
                                            a.ExamID,
                                            a.BoardID,
                                            a.SubjectID,
                                            a.ExamGradeID,
                                            a.PassingYear,
                                             a.EvaluationSystem,       
                                            a.ResultDescription,
                                            a.TotalMarks,
                                            a.Percentage,
                                            a.EducationStatus
                                       FROM academic a
                                            LEFT JOIN Sailor s ON a.SailorID = s.SAILORID
                                            LEFT JOIN bn_rank r ON s.RANKID = r.RANK_ID
                                            LEFT JOIN bn_posting_unit po ON s.POSTINGUNITID = po.POSTING_UNITID
                                             WHERE a.AcademicID = $id")->row();
        $data['exam_nameData'] = $this->utilities->findAllByAttribute("bn_govtexam_hierarchy", array("ACTIVE_STATUS" => 1));
        $data['subject_group'] = $this->utilities->findAllByAttribute("bn_subject_group", array("ACTIVE_STATUS" => 1));
        $data['board_university'] = $this->utilities->findAllByAttribute("bn_boardcenter", array("ACTIVE_STATUS" => 1));
        $data['result'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'regularTransaction/AcademicInfo/edit';
        $this->template->display($data);
    }
    
    
    
     public function update() {
        $id = $this->input->post('id', true);
        $examSystem = $this->input->post('examSystem', true);
        $examName = $this->input->post('examName', true);
        $subjectGroup = $this->input->post('subjectGroup', true);        
        $passingYear = $this->input->post('passingYear', true);
        $boardUniversity = $this->input->post('boardUniversity', true);
        $result = $this->input->post('result', true);        
        $percent = $this->input->post('percent', true);        
        $resultdesc = $this->input->post('resultdesc', true);
        $eduStatusDesc = $this->input->post('eduStatusDesc', true);
        if ($examSystem == 1){
            $tradition_1 = $this->input->post('tradition', true);
        } else {
            $tradition_1 = $this->input->post('gpa', true);
        }
        $update = array(
            'ExamID' => $examName,            
            'BoardID' => $boardUniversity,            
            'SubjectID' => $subjectGroup,            
            'PassingYear' => $passingYear,            
            'EvaluationSystem' => $examSystem,            
            'ExamGradeID' => $result,            
            'ResultDescription' => $resultdesc,            
            'TotalMarks' => $tradition_1,            
            'EducationStatus' => $eduStatusDesc,
            'Percentage' => $percent,
            'ACTIVE_STATUS'=> 1,
            'UPD_BY' => $this->user_session["USER_ID"],
            'UPD_DT' => date("Y-m-d h:i:s a")
        );
        echo '<pre>';
        print_r($update);
        //exit;
        if ($this->utilities->updateData('academic', $update, array('AcademicID' => $id))) {
            echo "<div class='alert alert-success'>Academic Information Update successfully</div>";
        } else {
            echo "<div class='alert alert-success'>Academic Information Update Failed</div>";
        }
    }

}
