<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Marriage
 * @package    Marriage Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class MarriageInfo extends CI_Controller {

	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Marriage Info entry';
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/marriage_info/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['daoNumber'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", "DESC");
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));        
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
      	$data['content_view_page'] = 'regularTransaction/marriage_info/create';
        $this->template->display($data);
    }
    
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {
        /*echo "<pre>";
        print_r($_POST);
        exit();*/
        $SAILOR_ID = $this->input->post('SAILOR_ID', true);
        /*start spouse part*/

        $spouseName = $this->input->post('spouseName', true);
        $spouseNameBN = $this->input->post('spouseNameBN', true);
        $marriageDate = date('Y-m-d', strtotime($this->input->post('marriageDate', true)));
        $marriageRate = (isset($_POST['marriageRate'])) ? 1 : 0 ;
        $marriageNextOfKin = (isset($_POST['marriageNextOfKin'])) ? 1 : 0 ;
        /*end spouse part*/

        /*start authority_info part*/
        $marriageNo = $this->utilities->get_max_value_by_attribute("marriage", "MarriageNo", array("SailorID" => $SAILOR_ID));
        if($marriageNo == ''){
        	$marriageNo = 1;
        }else{
        	$marriageNo += 1;
        }
        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID', true);
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $currentDate = date("Y-m-d");
        $DAO_ID = $this->input->post('DAO_ID', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        /*end authority_info part*/

        //foreignvisit insert part
        $insert = array(
            'SailorID' => $SAILOR_ID,
            'SpouseName' => $spouseName,
            'SpouseNameBangla' => $spouseNameBN,
            'MarriageNo' => $marriageNo,
            'MarriageDate' => $marriageDate,
            'IsThisMarriageNextOfKin' => $marriageNextOfKin,
            'MarriageAuthorityNumber' => $authorityNumber,
            'MarriageAuthorityDate' => $authorityDate,
            'MarriageDAOID' => $DAO_ID,
            'MarriageDAONo' => $DAO->DAO_NO,
            'MarriageShipEstablishmentID' => $SHIP_ESTABLISHMENT_ID,
            'IsMarriageRatesofPay' => $marriageRate,
            'Date' => $currentDate,
            'AuthorityDate' => $currentDate,
            'MaritalStatus' => 1,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        if ($this->utilities->insertData($insert,'marriage')) { // if examtran inserted successfully            
           	echo "<div class='alert alert-success'>Marriage Info Inserted successfully</div>";

        }else{
            echo "<div class='alert alert-success'>Marriage Info Inserted Failed</div>";
	    }       
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, mse.NAME MG_SHIP_EST, mse.AREA_ID,  se.NAME SHIP_EST
											FROM marriage m
											INNER JOIN sailor s on s.SAILORID = m.SailorID
											INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
											INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = s.POSTINGUNITID
											INNER JOIN bn_ship_establishment mse on mse.SHIP_ESTABLISHMENTID = m.MarriageShipEstablishmentID
											LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
											WHERE m.MarriageID = $id")->row();
        $data['daoNumber'] = $this->utilities->findAllByAttributeWithOrderBy("bn_dao", array("ACTIVE_STATUS" => 1), "DAO_ID", "DESC");
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));        
        $data['authorityShipEst'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
		$data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/marriage_info/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update() {
        $id= $this->input->post('id', true);
        $spouseName = $this->input->post('spouseName', true);
        $spouseNameBN = $this->input->post('spouseNameBN', true);        
        $marriageRate = (isset($_POST['marriageRate'])) ? 1 : 0 ;
        $marriageNextOfKin = (isset($_POST['marriageNextOfKin'])) ? 1 : 0 ;
        $currentDate = date("Y-m-d");
        /*common info*/
        $update1 = array(
        	'SpouseName' => $spouseName,
            'SpouseNameBangla' => $spouseNameBN,
            'IsMarriageRatesofPay' => $marriageRate,
            'IsThisMarriageNextOfKin' => $marriageNextOfKin,
        );

        /*end spouse part*/
        $marriageStatus = 0;
        $marriageStatus = $this->input->post('marriageStatus', true);
        if( $marriageStatus == 1){
        	$marriageDate = date('Y-m-d', strtotime($this->input->post('marriageDate', true)));
	        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID', true);
	        $authorityNumber = $this->input->post('authorityNumber', true);
	        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
	        $DAO_ID = $this->input->post('DAO_ID', true);
	        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
	        $update2 = array(
	        	'SpouseName' => $spouseName,
	            'SpouseNameBangla' => $spouseNameBN,
	            'MarriageDate' => $marriageDate,
	            'IsThisMarriageNextOfKin' => $marriageNextOfKin,
	            'MarriageAuthorityNumber' => $authorityNumber,
	            'MarriageAuthorityDate' => $authorityDate,
	            'MarriageDAOID' => $DAO_ID,
	            'MarriageDAONo' => $DAO->DAO_NO,
	            'MarriageShipEstablishmentID' => $SHIP_ESTABLISHMENT_ID,

	            'Date' => $currentDate,
	        	'AuthorityNumber' => '',
	        	'AuthorityDate' => $currentDate,
	        	'DAOID' => '',
	        	'DAONo' => '',
	        	'ShipEstablishmentID' => $SHIP_ESTABLISHMENT_ID,
	        	'MaritalStatus' => $marriageStatus,
	        	'SeparationCause' => '',
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
	        );
	        $update = array_merge($update1, $update2);
	        if($this->utilities->updateData('marriage',$update, array('MarriageID' => $id))){
	        	echo "<div class='alert alert-success'>Marriage Information Update successfully</div>";
	        }else{
	        	echo "<div class='alert alert-danger'>Marriage Information Update Failed</div>";
	        }

        }else if($marriageStatus == 2){
        	$separationDate = date('Y-m-d', strtotime($this->input->post('separationDate', true)));
        	$separationCause = $this->input->post('separationCause', true);
	        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID_SEP', true);
	        $authorityNumber = $this->input->post('authorityNumber_SEP', true);
	        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate_SEP', true)));
	        $DAO_ID = $this->input->post('DAO_ID_SEP', true);
	        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
	        $update2 = array(
	        	'Date' => $separationDate,
	        	'AuthorityNumber' => $authorityNumber,
	        	'AuthorityDate' => $authorityDate,
	        	'DAOID' => $DAO_ID,
	        	'DAONo' => $DAO->DAO_NO,
	        	'ShipEstablishmentID' => $SHIP_ESTABLISHMENT_ID,
	        	'MaritalStatus' => $marriageStatus,
	        	'SeparationCause' => $separationCause,
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
	        );
	        $update = array_merge($update1, $update2);
	        if($this->utilities->updateData('marriage',$update, array('MarriageID' => $id))){
	        	echo "<div class='alert alert-success'>Separation Information Update successfully</div>";
	        }else{
	        	echo "<div class='alert alert-danger'>Separation Information Update Failed</div>";
	        }

        }else if($marriageStatus == 3){
        	$deathDate = date('Y-m-d', strtotime($this->input->post('deathDate', true)));
	        $SHIP_ESTABLISHMENT_ID = $this->input->post('SHIP_ESTABLISHMENT_ID_D', true);
	        $authorityNumber = $this->input->post('authorityNumber_D', true);
	        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate_D', true)));
	        $DAO_ID = $this->input->post('DAO_ID_D', true);
	        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
	        $update2 = array(
	        	'Date' => $deathDate,
	        	'AuthorityNumber' => $authorityNumber,
	        	'AuthorityDate' => $authorityDate,
	        	'DAOID' => $DAO_ID,
	        	'DAONo' => $DAO->DAO_NO,
	        	'ShipEstablishmentID' => $SHIP_ESTABLISHMENT_ID,
	        	'MaritalStatus' => $marriageStatus,
	        	'SeparationCause' => '',
                'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
	        );
	        $update = array_merge($update1, $update2);
	        if($this->utilities->updateData('marriage',$update, array('MarriageID' => $id))){
	        	echo "<div class='alert alert-success'>Death Information Update successfully</div>";
	        }else{
	        	echo "<div class='alert alert-danger'>Death Information Update Failed</div>";
	        }
        }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function view($id)
    {
        $data['viewdetails'] = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, s.FULLNAME, r.RANK_NAME, pu.NAME POSTING_UNIT_NAME , s.POSTINGDATE, mse.NAME MG_SHIP_EST, mse.AREA_ID,  se.NAME SHIP_EST
                                            FROM marriage m
                                            INNER JOIN sailor s on s.SAILORID = m.SailorID
                                            INNER JOIN bn_rank r on s.RANKID = r.RANK_ID
                                            INNER JOIN bn_posting_unit pu on pu.POSTING_UNITID = s.POSTINGUNITID
                                            INNER JOIN bn_ship_establishment mse on mse.SHIP_ESTABLISHMENTID = m.MarriageShipEstablishmentID
                                            LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                            WHERE m.MarriageID = $id")->row();
        $this->load->view('regularTransaction/marriage_info/view',$data);
    }
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxMarriageList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 's.OFFICIALNUMBER', 1 => 'm.SpouseName', 2 => 'm.MarriageDate', 3 => 'm.MarriageNo', 4 => 'mse.NAME');

        // getting total number records without any search

        $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, mse.NAME MG_SHIP_EST, se.NAME SHIP_EST
									FROM marriage m
									INNER JOIN sailor s on s.SAILORID = m.SailorID
									INNER JOIN bn_ship_establishment mse on mse.SHIP_ESTABLISHMENTID = m.MarriageShipEstablishmentID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    WHERE s.SAILORSTATUS = 1")->num_rows();
       
        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, mse.NAME MG_SHIP_EST, se.NAME SHIP_EST
									FROM marriage m
									INNER JOIN sailor s on s.SAILORID = m.SailorID
									INNER JOIN bn_ship_establishment mse on mse.SHIP_ESTABLISHMENTID = m.MarriageShipEstablishmentID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    WHERE s.SAILORSTATUS = 1 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] ."%' OR m.SpouseName LIKE '" . $requestData['search']['value'] . "%' OR m.MarriageDate LIKE '" . $requestData['search']['value']. 
                                    "%' OR m.MarriageNo LIKE '" . $requestData['search']['value']."%' OR mse.NAME LIKE '" . $requestData['search']['value'].
                                    "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

        } else {

            $query = $this->db->query("SELECT m.*, s.OFFICIALNUMBER, mse.NAME MG_SHIP_EST, se.NAME SHIP_EST
									FROM marriage m
									INNER JOIN sailor s on s.SAILORID = m.SailorID
									INNER JOIN bn_ship_establishment mse on mse.SHIP_ESTABLISHMENTID = m.MarriageShipEstablishmentID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = m.ShipEstablishmentID
                                    WHERE s.SAILORSTATUS = 1
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->SpouseName;
            $nestedData[] = date("Y-m-d",strtotime($row->MarriageDate));
            $nestedData[] = $row->MarriageNo;
            $nestedData[] = $row->MG_SHIP_EST;
            $nestedData[] = '<a class="btn btn-success btn-xs modalLink" href="'.site_url('regularTransaction/marriageInfo/view/' . $row->MarriageID) .'" title="View Marriage Info" type="button"><span class="glyphicon glyphicon-eye-open"></span></a> '.
            				'<a class="btn btn-warning btn-xs" href="'.site_url('regularTransaction/marriageInfo/edit/' . $row->MarriageID) .'" title="Edit marriage Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->MarriageID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="MarriageID" data-tbl="marriage"><span class="glyphicon glyphicon-trash"></span></a>';
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }
}

/* End of file marriageInfo.php */
/* Location: ./application/controllers/regularTransaction/marriageInfo.php */