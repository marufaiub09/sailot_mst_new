<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * @category   Training Course
 * @package    Training Course Info
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class TrainingCourseInfo extends CI_Controller {
	
	private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Trainning Course entry';
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/training_course/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    public function create()
    {
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['trainingType'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 2));
        
        $data['batchNumber'] = $this->utilities->findAllByAttribute("bn_batchnumber", array("ACTIVE_STATUS" => 1));
        $data['trainingIns'] = $this->utilities->findAllByAttribute("bn_traininginstitute", array("ACTIVE_STATUS" => 1));
        $data['authorityZone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 1));
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));        

        $data['examGrade'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', '---Select---', 'EXAM_GRADEID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['examResult'] = $this->utilities->dropdownFromTableWithCondition('bn_exam_result', '---Select---', 'EXAM_RESULT_ID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['examGrade_one'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
        $data['examResul_one'] = $this->utilities->findAllByAttribute("bn_exam_result", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/training_course/create';
        $this->template->display($data);
    }
    
    
    /**
     * @access      public
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */

    public function save()
    {
        $SAILOR_NO = $this->input->post('OFFICIAL_NO', true);
        for ($i=0; $i < count($SAILOR_NO) ; $i++) { 
            $row = $this->utilities->findByAttribute("sailor", array('OFFICIALNUMBER' => $SAILOR_NO[$i]));
            $sailorId[] = $row->SAILORID;
        }
        /*start exam part*/

        $TRAINING_COURSE_TYPE = $this->input->post('TRAINING_COURSE_TYPE', true);
        $TRAINING_COURSE_NAME = $this->input->post('TRAINING_COURSE_NAME', true);        
        $BATCH_NO = $this->input->post('BATCH_NO', true);
        $batch = $this->utilities->findByAttribute("bn_batchnumber", array("BATCH_ID" => $BATCH_NO));
        $startingDate = date('Y-m-d', strtotime($this->input->post('startingDate', true)));
        $completionDate = date('Y-m-d', strtotime($this->input->post('completionDate', true)));
        $TRAINING_INS_ID = $this->input->post('TRAINING_INS_ID', true);
        $trainingOrg = $this->utilities->findByAttribute("bn_traininginstitute", array("Training_Institute_ID" => $TRAINING_INS_ID));
        $durationWeeks = $this->input->post('durationWeeks', true);
        /*end exam part*/

        /*start authority_info part*/

        if(isset($_POST['AUTHORITY_ZONE_ID'])){
	        $AUTHORITY_ZONE_ID = $this->input->post('AUTHORITY_ZONE_ID', true);
        	$examTrans_1 = array(
	        	'AuthorityAreaID' => $this->input->post('AUTHORITY_AREA_ID', true) 
	        );       	
        }elseif (isset($_POST['SHIP_AREA_ID'])) {
	        $SHIP_AREA_ID = $this->input->post('SHIP_AREA_ID', true);
	        $examTrans_1 = array(
	        	'AuthorityShipID' => $this->input->post('SHIP_ESTABLISHMENT_ID', true)
	        );   	
        }elseif (isset($_POST['authorityName'])) {
	        $examTrans_1 = array(
        		'AuthorityName' => $this->input->post('authorityName', true)
        	);
        }

        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_ID', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));
        /*end authority_info part*/

        /*start sailorsInfo part*/
        $GRADE = $this->input->post('GRADE', true);
        $MARKS = $this->input->post('MARKS', true);
        $PERCENTAGE = $this->input->post('PERCENTAGE', true);
        $RESULT = $this->input->post('RESULT', true);
        $SENIORITY = $this->input->post('SENIORITY', true);
        /*end sailorInfo part*/
        //CourseInformation insert part
        $insertSuccess = 0;
        for ($i=0; $i < count($SAILOR_NO) ; $i++) {
	        $examTrans_2 = array(
	            'CourseID' => $TRAINING_COURSE_NAME,
	            'SailorID' => $sailorId[$i],
	            'OrganizationID' => $trainingOrg->TRAINING_ORG_ID,
	            'ExamGradeID' => $GRADE[$i],
	            'ExamResultID' => $RESULT[$i],
	            'StartDate' => $startingDate,
	            'EndDate' => $completionDate,
	            'Duration' => $durationWeeks,
	            'BatchNumberID' => $BATCH_NO,
	            'BatchNumber' => $batch->BATCH_NUMBER,
	            'InstituteID' => $TRAINING_INS_ID,
	            'Mark' => $MARKS[$i],
	            'Percentage' => $PERCENTAGE[$i],
	            'Seniority' => $SENIORITY[$i],
	            'AuthorityNumber' => $authorityNumber,
	            'AuthorityDate' => $authorityDate,
	            'DAOID' => $DAO_ID,
	            'DAONumber' => $DAO->DAO_NO, /*object to value*/
	            'IsForeign' => 0,
	            'Remarks' => '',
	            'CRE_BY' => $this->user_session["USER_ID"]
	        );
	        $insert = array_merge($examTrans_1, $examTrans_2);
	        if ($this->utilities->insertData($insert,'coursetran')) { // if courseInfo inserted successfully            
	            $insertSuccess = 1;
	        }
	    }
	    if ($insertSuccess == 1) { // if courseInfo inserted successfully
	    	echo "<div class='alert alert-success'>Training/Course Inserted successfully</div>";

        }else{
            echo "<div class='alert alert-success'Training/Course Inserted Failed</div>";
	    }
    }
    /**
     * @access      public
     * @param       id
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      templete
     */
    public function edit($id)
    {
        $data['result'] = $this->db->query("SELECT e.*, na.PARENT_ID AUTHO_ZONE, se.AREA_ID SHIP_AREA,tro.Name ORG_NAME, ti.Name INSTITUTE_NAME, th.ParentID TRAINING_TYPE, th.Name TRAINING_COURSE, s.OFFICIALNUMBER,s.FULLNAME, r.RANK_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE, se.SHIP_ESTABLISHMENTID, se.NAME AUTHO_SHIP_EST, na.NAME AUTHO_AREA
											FROM coursetran e
											INNER JOIN sailor s on s.SAILORID = e.SailorID
											INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
											INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
											INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
											LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
											LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
											INNER JOIN bn_navytraininghierarchy th on th.NAVYTrainingID = e.CourseID
											INNER JOIN bn_traininginstitute ti on ti.Training_Institute_ID = e.InstituteID
											INNER JOIN bn_trainingorganization tro on tro.TrainingOrganizationID = e.OrganizationID
											WHERE e.CourseTranID= $id")->row();
        $data['daoNumber'] = $this->utilities->findAllByAttribute("bn_dao", array("ACTIVE_STATUS" => 1));
        $data['trainingType'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 2));
        $data['trainingName'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 3));
        
        $data['batchNumber'] = $this->utilities->findAllByAttribute("bn_batchnumber", array("ACTIVE_STATUS" => 1));
        $data['trainingIns'] = $this->utilities->findAllByAttribute("bn_traininginstitute", array("ACTIVE_STATUS" => 1));
        $data['authorityZone'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 1));
        $data['authorityArea'] = $this->utilities->findAllByAttribute("bn_navyadminhierarchy", array("ACTIVE_STATUS" => 1, "ADMIN_TYPE" => 2));        
        $data['shipEstablish'] = $this->utilities->findAllByAttribute("bn_ship_establishment", array("ACTIVE_STATUS" => 1));
        $data['examGrade'] = $this->utilities->dropdownFromTableWithCondition('bn_examgrade', '---Select---', 'EXAM_GRADEID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['examResult'] = $this->utilities->dropdownFromTableWithCondition('bn_exam_result', '---Select---', 'EXAM_RESULT_ID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['examGrade_one'] = $this->utilities->findAllByAttribute("bn_examgrade", array("ACTIVE_STATUS" => 1));
        $data['examResul_one'] = $this->utilities->findAllByAttribute("bn_exam_result", array("ACTIVE_STATUS" => 1));
        $data['flag'] = 0; /*flag status = 0 is Active sailor; status = 1 is retirement sailor;*/
        $data['content_view_page'] = 'regularTransaction/training_course/edit';
        $this->template->display($data);
    }

    /*
     * @methodName Update()
     * @access
     * @param  none
     * @return  //
     */
    public function update() {
        $id= $this->input->post('id', true);
        /*start exam part*/
        $SAILOR_NO = $this->input->post('OFFICIAL_NO', true);
        $TRAINING_COURSE_TYPE = $this->input->post('TRAINING_COURSE_TYPE', true);
        $TRAINING_COURSE_NAME = $this->input->post('TRAINING_COURSE_NAME', true);        
        $BATCH_NO = $this->input->post('BATCH_NO', true);
        $batch = $this->utilities->findByAttribute("bn_batchnumber", array("BATCH_ID" => $BATCH_NO));
        $startingDate = date('Y-m-d', strtotime($this->input->post('startingDate', true)));
        $completionDate = date('Y-m-d', strtotime($this->input->post('completionDate', true)));
        $TRAINING_INS_ID = $this->input->post('TRAINING_INS_ID', true);
        $trainingOrg = $this->utilities->findByAttribute("bn_traininginstitute", array("Training_Institute_ID" => $TRAINING_INS_ID));
        $durationWeeks = $this->input->post('durationWeeks', true);
        /*end exam part*/

        /*start authority_info part*/
        $authorityNumber = $this->input->post('authorityNumber', true);
        $authorityDate = date('Y-m-d', strtotime($this->input->post('authorityDate', true)));
        $DAO_ID = $this->input->post('DAO_ID', true);
        $DAO = $this->utilities->findByAttribute("bn_dao", array("DAO_ID" => $DAO_ID));

        if(isset($_POST['AUTHORITY_ZONE_ID'])){
	        $AUTHORITY_ZONE_ID = $this->input->post('AUTHORITY_ZONE_ID', true);
        	$examTrans_1 = array(
	        	'AuthorityAreaID' => $this->input->post('AUTHORITY_AREA_ID', true),
	        	'AuthorityShipID' => null,
	        	'AuthorityName' => ''

	        );       	
        }elseif (isset($_POST['SHIP_AREA_ID'])) {
	        $SHIP_AREA_ID = $this->input->post('SHIP_AREA_ID', true);
	        $examTrans_1 = array(
	        	'AuthorityShipID' => $this->input->post('SHIP_ESTABLISHMENT_ID', true),
	        	'AuthorityAreaID' => null,
	        	'AuthorityName' => ''
	        );   	
        }elseif (isset($_POST['authorityName'])) {
	        $examTrans_1 = array(
        		'AuthorityName' => $this->input->post('authorityName', true),
        		'AuthorityAreaID' => null,
	        	'AuthorityShipID' => null
        	);
        }
        /*start sailorsInfo part*/
        $GRADE = $this->input->post('GRADE', true);
        $MARKS = $this->input->post('MARKS', true);
        $PERCENTAGE = $this->input->post('PERCENTAGE', true);
        $RESULT = $this->input->post('RESULT', true);
        $SENIORITY = $this->input->post('SENIORITY', true);
        /*end sailorInfo part*/
        //CourseTran insert part
        $updateSuccess = 0;
        for ($i=0; $i < count($SAILOR_NO) ; $i++) {
	        $examTrans_2 = array(
	            'CourseID' => $TRAINING_COURSE_NAME,
	            'OrganizationID' => $trainingOrg->TRAINING_ORG_ID,
	            'ExamGradeID' => $GRADE[$i],
	            'ExamResultID' => $RESULT[$i],
	            'StartDate' => $startingDate,
                'EndDate' => $completionDate,
                'Duration' => $durationWeeks,
                'BatchNumberID' => $BATCH_NO,
                'BatchNumber' => $batch->BATCH_NUMBER,
                'InstituteID' => $TRAINING_INS_ID,
                'Mark' => $MARKS[$i],
                'Percentage' => $PERCENTAGE[$i],
                'Seniority' => $SENIORITY[$i],
                'AuthorityNumber' => $authorityNumber,
                'AuthorityDate' => $authorityDate,
                'DAOID' => $DAO_ID,
                'DAONumber' => $DAO->DAO_NO, /*object to value*/
	            'UPD_BY' => $this->user_session["USER_ID"],
                'UPD_DT' => date("Y-m-d h:i:s a")
	        );
	        $update = array_merge($examTrans_1, $examTrans_2);
	        if ($this->utilities->updateData('coursetran',$update, array('CourseTranID' => $id))) { // if examtran inserted successfully            
	            $updateSuccess = 1;
	        }
	    }
	    if ($updateSuccess == 1) { // if examtran inserted successfully
	    	echo "<div class='alert alert-success'>Training/Course Update successfully</div>";

        }else{
            echo "<div class='alert alert-danger'>Training/Course Update Failed</div>";
	    }
    }
    
    /**
     * @param       none
     * @author      Emdadul Huq <Emdadul@atilimited.net>
     * @return      View modal
     */
    function ajaxTrainningCourseList(){
        // storing  request (ie, get/post) global array to a variable
        $requestData = $_REQUEST;

        $columns = array(

            // datatable column index  => database column name
            0 => 'e.CourseTranID', 1 => 's.OFFICIALNUMBER', 2 => 'th.Name', 3 => 'e.StartDate', 4 => 'e.EndDate', 5 => 'e.Duration', 6 => 'er.NAME', 7 => 'e.CourseTranID');

        // getting total number records without any search

        $query = $this->db->query("SELECT e.*, na.PARENT_ID AUTHO_ZONE, se.AREA_ID SHIP_AREA,tro.Name ORG_NAME, ti.Name INSTITUTE_NAME, th.Name TRAINING_COURSE, s.OFFICIALNUMBER,s.FULLNAME, r.RANK_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE, se.SHIP_ESTABLISHMENTID, se.NAME AUTHO_SHIP_EST, na.NAME AUTHO_AREA
									FROM coursetran e
									INNER JOIN sailor s on s.SAILORID = e.SailorID
									INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
									INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
									INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
									LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
									INNER JOIN bn_navytraininghierarchy th on th.NAVYTrainingID = e.CourseID
									INNER JOIN bn_traininginstitute ti on ti.Training_Institute_ID = e.InstituteID
									INNER JOIN bn_trainingorganization tro on tro.TrainingOrganizationID = e.OrganizationID
									WHERE s.SAILORSTATUS = 1 AND e.IsForeign = 0")->num_rows();
       
        $totalData = $query;

        $totalFiltered = $totalData;
        // when there is no search parameter then total number rows = total number filtered rows.

        if (!empty($requestData['search']['value'])) {

            // if there is a search parameter

            $query = $this->db->query("SELECT e.*, na.PARENT_ID AUTHO_ZONE, se.AREA_ID SHIP_AREA,tro.Name ORG_NAME, ti.Name INSTITUTE_NAME, th.Name TRAINING_COURSE, s.OFFICIALNUMBER,s.FULLNAME, r.RANK_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE, se.SHIP_ESTABLISHMENTID, se.NAME AUTHO_SHIP_EST, na.NAME AUTHO_AREA
									FROM coursetran e
									INNER JOIN sailor s on s.SAILORID = e.SailorID
									INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
									INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
									INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
									LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
									INNER JOIN bn_navytraininghierarchy th on th.NAVYTrainingID = e.CourseID
									INNER JOIN bn_traininginstitute ti on ti.Training_Institute_ID = e.InstituteID
									INNER JOIN bn_trainingorganization tro on tro.TrainingOrganizationID = e.OrganizationID
									WHERE s.SAILORSTATUS = 1 AND e.IsForeign = 0 AND s.OFFICIALNUMBER LIKE '" . $requestData['search']['value'] ."%' OR th.NAME LIKE '" . $requestData['search']['value'] . "%' OR er.NAME LIKE '" . $requestData['search']['value']. 
                                    "%' OR eg.NAME LIKE '" . $requestData['search']['value']."%' OR e.Duration LIKE '" . $requestData['search']['value'].
                                    "%' ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
                    /* $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length. */

            $totalFiltered = $query;
            // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query

        } else {

            $query = $this->db->query("SELECT e.*, na.PARENT_ID AUTHO_ZONE, se.AREA_ID SHIP_AREA,tro.Name ORG_NAME, ti.Name INSTITUTE_NAME, th.Name TRAINING_COURSE, s.OFFICIALNUMBER,s.FULLNAME, r.RANK_NAME, er.NAME RESULT_NAME, eg.NAME EXAM_GRADE, se.SHIP_ESTABLISHMENTID, se.NAME AUTHO_SHIP_EST, na.NAME AUTHO_AREA
									FROM coursetran e
									INNER JOIN sailor s on s.SAILORID = e.SailorID
									INNER JOIN bn_rank r on r.RANK_ID = s.RANKID
									INNER JOIN bn_exam_result er on er.EXAM_RESULT_ID = e.ExamResultID
									INNER JOIN bn_examgrade eg on eg.EXAM_GRADEID = e.ExamGradeID
									LEFT JOIN bn_ship_establishment se on se.SHIP_ESTABLISHMENTID = e.AuthorityShipID
									LEFT JOIN bn_navyadminhierarchy na on na.ADMIN_ID = e.AuthorityAreaID
									INNER JOIN bn_navytraininghierarchy th on th.NAVYTrainingID = e.CourseID
									INNER JOIN bn_traininginstitute ti on ti.Training_Institute_ID = e.InstituteID
									INNER JOIN bn_trainingorganization tro on tro.TrainingOrganizationID = e.OrganizationID
									WHERE s.SAILORSTATUS = 1 AND e.IsForeign = 0
                                    ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ")->result();
        }
        $sn =1;
        $data = array();
        foreach ($query as $row) {
            // preparing an array
            $nestedData = array();
            $nestedData[] = $sn;
            $nestedData[] = $row->OFFICIALNUMBER;
            $nestedData[] = $row->TRAINING_COURSE;
            $nestedData[] = date('d-m-Y',strtotime($row->StartDate));
            $nestedData[] = date('d-m-Y',strtotime($row->EndDate));
            $nestedData[] = $row->Duration;
            $nestedData[] = $row->RESULT_NAME;
            $nestedData[] = '<a class="btn btn-warning btn-xs" href="'.site_url('regularTransaction/trainingCourseInfo/edit/' . $row->CourseTranID) .'" title="Edit Trainning Course Info" type="button"><span class="glyphicon glyphicon-edit"></span></a> '.
                            '<a class="btn btn-danger btn-xs deleteItemAjaxDataTable" id="'.$row->CourseTranID.'" sn="'.$sn++.'" title="Click For Delete" data-type="delete" data-field="CourseTranID" data-tbl="coursetran"><span class="glyphicon glyphicon-trash"></span></a>';
            $nestedData[] = $row->EXAM_GRADE;
            $nestedData[] = $row->Mark;
            $nestedData[] = $row->Percentage;
            $nestedData[] = $row->Seniority;
            $nestedData[] = $row->INSTITUTE_NAME;
            $nestedData[] = $row->AuthorityName." ".$row->AUTHO_SHIP_EST." ".$row->AUTHO_AREA;
            $nestedData[] = $row->DAONumber;
            $data[] = $nestedData;
        }
        $json_data = array("draw" => intval($requestData['draw']),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data

            // total data array
        );

        echo json_encode($json_data);
    }

}

/* End of file trainingCourseInfo.php */
/* Location: ./application/controllers/regularTransaction/trainingCourseInfo.php */