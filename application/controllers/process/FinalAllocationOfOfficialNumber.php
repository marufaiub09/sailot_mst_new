<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FinalAllocationOfOfficialNumber extends EXT_Controller {

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /*
     * @methodName index()
     * @access
     * @param  none
     * @return  //Load Sailor For Official Number
     */

    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Allocation of O Number';
        $data['batch'] = $this->utilities->dropdownFromTableWithCondition('bn_batchnumber', 'Select Batch', 'BATCH_ID', 'BATCH_NUMBER');
        $data['repotrType'] = $this->utilities->dropdownFromTableWithCondition('reportsubtype', ' Report Type ', 'ReportSubTypeID', 'ReportSubTypeName', $condition = array("ReportTypeID" => 1));
        $data['reportName'] = $this->utilities->dropdownFromTableWithCondition('reportsetup', 'Select Report Name', 'ReportSetupID', 'Name');
        $data['content_view_page'] = 'process/FinalAllocationOfOfficialNumber/index';
        $this->template->display($data);
    }

    /*
     * @methodName getSailorData()
     * @access
     * @param  none
     * @return  //Load Sailor For Official Number
     * Maruf
     */

    public function getSailorData() {
        $navi = $_POST['navi'];
        $batchid = $_POST['batchid'];
        $sequData = $_POST['sequData'];
        foreach ($sequData as $data1) {
            $data1;
        }
        foreach ($navi as $data2) {
            $data2;
        }
        $as = implode(",", $sequData);
        // echo $as;
        $returnVal = '<tr></tr>';
        if ($data2 == 'Navy') {

            $data['result'] = $this->db->query("SELECT
	    	sailor.*,
	    	DATE_FORMAT(sailor.BIRTHDATE,'%d-%m-%Y') BIRTHDATE,

	  		DATE_FORMAT(sailor.ENTRYDATE, '%d-%m-%Y') ENTRYDATE,
		r.RANK_NAME as RANKID
	    	FROM sailor
	   	INNER JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
       
	   	 WHERE sailor.BATCHNUMBER = $batchid AND  sailor.ISMODC = 0 ORDER BY $as");
        } else {

            $data['result'] = $this->db->query("SELECT
            sailor.*,
            DATE_FORMAT(sailor.BIRTHDATE,'%d-%m-%Y') BIRTHDATE,

            DATE_FORMAT(sailor.ENTRYDATE, '%d-%m-%Y') ENTRYDATE,
        r.RANK_NAME as RANKID
            FROM sailor
        INNER JOIN bn_rank r ON sailor.RANKID = r.RANK_ID
       
         WHERE sailor.BATCHNUMBER = $batchid AND  sailor.ISMODC = 1  ORDER BY $as");
        }

        foreach ($data['result']->result() as $key => $row) {

            $returnVal = '<tr>
        <td class="text-center copy" id="copy_' . $key . '">' . $row->OFFICIALNUMBER . '</td>
        <td class="text-center">' . $row->SAILORID . '</td>
        <td class="text-center">' . $row->LOCALNUMBER . '</td>
        <td class="text-center">' . $row->FULLNAME . '</td>
        <td class="text-center">' . $row->RANKID . '</td>
        <td class="text-center">' . $row->BIRTHDATE . '</td>
        <td class="text-center" id="data_' . $key . '">' . $row->ENTRYDATE . '</td>
        <input Name="sailor_id[]"  type ="hidden" value="' . $row->SAILORID . '">
        </tr>';

            echo $returnVal;
        }
    }

    //emran
    public function genONo() {
        $officealNo = $this->input->post("officialNumber");
        $sailorId = $this->input->post("sailor_id");
        for ($i = 0; $i < count($sailorId); $i++) {
            $id =$sailorId[$i];
            $ins = array(
              "OFFICIALNUMBER" => $officealNo[$i],
              "SAILORSTATUS" => 1
              );
              if ($this->utilities->updateData('sailor', $ins, array("SAILORID" => $id))) { // if Official Number successfully  update          
                $updateSuccess = 1;
            }
        }       
        if ($updateSuccess == 1) { // if Official Number Gegerate successfully
            echo "<div class='alert alert-success'>Official Number Generate successfully</div>";
        } else {
            echo "<div class='alert alert-danger'>Official Number Generate Failed</div>";
        }
    }

}
