<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReportMultiJoinSetup extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      templete
     */
    public function index() {

        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit Reprot Information';
        $CI = &get_instance();
        $CI->load->database();
        $a = $CI->db->dbdriver;
        //echo $a;
        if ($a == 'mysqli' || $a == 'mysql') {
            $data['result'] = $this->db->list_tables();
        } else {
            $data['result'] = $this->db->query("SELECT * FROM USER_TABLES")->result();
        }
        $data['content_view_page'] = 'multiReport/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Selected Column value
     */
    public function joinSetup() {
        error_reporting('0');
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit Reprot Information';
        $CI = &get_instance();
        $CI->load->database();
        $a = $CI->db->dbdriver;
        //echo $a;
        if ($a == 'mysqli' || $a == 'mysql') {
            $data['result'] = $this->db->list_tables();
        } else {
            $data['result'] = $this->db->query("SELECT * FROM USER_TABLES")->result();
        }

        if (isset($_POST['submit'])) {

            if (!empty($_POST['tableName'])) {
                $data['tableName'] = $_POST['tableName'];
                $data['columnName'] = $_POST['columnName'];
                $data['reportTitle'] = $_POST['reportTitle'];
            }
        }

        $data['pageTitle'] = 'visit Reprot Information';
        $data['content_view_page'] = 'multiReport/joinSetupView';
        $this->template->display($data);
        //$this->load->view('reportInfo/checkTables', $data);
    }

    public function checkColumn() {

        $Tablename = $_POST['Tablename'];
        $data['result'] = $this->db->field_data($Tablename);
        $returnVal = '<option value = "">Select Columns</option>';
        if (!empty($data['result'])) {

            foreach ($data['result'] as $row) {
                $returnVal .= '<option value = "' . $row->name . '">' . $row->name . '.' . $row->primary_key . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Selected Column value and type
     */
    public function checkTable() {

        $Tablename = $_POST['Tablename'];
        $data['result'] = $this->db->field_data($Tablename);
        $returnVal = '<option value = "">Select Columns</option>';
        if (!empty($data['result'])) {
            $returnVal .= '<option value = "none.limit">None</option>';
            foreach ($data['result'] as $row) {
                $returnVal .= '<option value = "' . $row->name . '.' . $row->type . '">' . $row->name . '.' . $row->primary_key . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Get Table
     */
    public function checkTableData() {

        $Tablename = $_POST['Tablename'];

        $data['result'] = $this->db->get($Tablename)->result();
        $returnVal = '<option value = "">Select Columns</option>';
        if (!empty($data['result'])) {
            foreach ($data['result'] as $row) {
                foreach ($row as $value) {

                    $returnVal .= '<option value = "' . $value . '">' . $value . '</option>';
                }
            }
        }
        echo $returnVal;
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Type
     */
    public function checkColumnType() {
        $likeOption = $_POST['likeOption'];
        //$str = "Name [varchar]";
        $str = explode(".", $likeOption);
        $type = $str[1];
        $returnVal = '<option value = "">Select Oparetor</option>';
        switch (!empty($type)) {
            case ($type == 'int'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option  value=">"> > </option>
				<option  value=">=">>=</option>
				<option  value="<"><</option>
				<option  value="<="><=</option>
				<option  value="Like">Like</option>
				<option  value="NotLike">Not Like</option>
				<option  value="limit">Limit</option>';
                echo $returnVal;
                break;

            case ($type == 'varchar'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value="Like">Like</option>
				<option value="NotLike">Not Like</option>
				<option value="limit">Limit</option>';
                echo $returnVal;
                break;
            case ($type == 'datetime'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
				<option value="<="><=</option>
				<option value="!=">!=</option>

				<option value="limit">Limit</option>
				<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'date'):
                $returnVal .= '<option value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
				<option value="<="><=</option>
				<option value="!=">!=</option>

				<option value="limit">Limit</option>
				<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'tinyint'):
                $returnVal .= '
				<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
				<option value="<="><=</option>

				<option value="Like">Like</option>
				<option value="limit">Limit</option>
				<option value="NotLike">Not Like</option>';
                echo $returnVal;
                break;
            case ($type == 'timestamp'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
			<option value="<="><=</option>
			<option value="!=">!=</option>

			<option value="limit">Limit</option>
			<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'year'):
                $returnVal .= '<option  value="=">=</option>
			<option value="!=">!=</option>
			<option value=">">></option>
			<option value=">=">>=</option>
			<option value="<"><</option>
			<option value="<="><=</option>
			<option value="!=">!=</option>

			<option value="limit">Limit</option>
			<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'limit'):
                $returnVal .= '
			<option value="limit">Limit</option>
			<option value="select">select</option>
			';

                echo $returnVal;
                break;
        }
    }

    public function express() {
        $operatorType = $_POST['operatorType'];
        switch (!empty($operatorType)) {
            case ($operatorType == '>'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]"></div>';
                echo $returnVal;
                break;

            case ($operatorType == '<'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '!='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '>='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '<='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == 'IsNull'):
                $returnVal = '';

                break;
            case ($operatorType == 'IsNotNull'):
                $returnVal = '';

                break;
            case ($operatorType == 'Like'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;

                break;

            case ($operatorType == 'NotLike'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;

                break;
            case ($operatorType == 'limit'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"> <input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;

                break;

            case ($operatorType == 'between'):
                $returnVal = '';
                $returnVal .= '
			<div class="form-group">

			<div class="col-sm-4 date">
			<input type="text"  name="startDate" value=""
			class="datePicker form-control required input-form-padding-right-empty"  placeholder="start date" data-fv-field="startDate">
			</div>
			<div class="col-sm-4 date">
			<input type="text"  name="endDate"
			class="datePicker2 form-control required input-form-padding-right-empty"  placeholder="end date" data-fv-field="startDate">
			</div>
			</div>
			<script type="text/javascript">
			$(document).ready(function() {

			$(".datePicker").datepicker(),
			$(".datePicker2").datepicker()});

			</script> ';

                echo $returnVal;

                break;
        }
    }

    public function createQuery() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $this->load->model('insert_model');
        if (isset($_POST['Create PDF'])) {

            if (!empty($_POST['tableName'])) {

                $data['tableName'] = $_POST['tableName'];
                $data['columnName'] = $_POST['columnName'];
                $data['reportTitle'] = $_POST['reportTitle'];
                $data['pageSize'] = $_POST['pageSize'];
                $data['orientation'] = $_POST['orientation'];
                $data['topMargin'] = $_POST['topMargin'];
                $data['bottomMargin'] = $_POST['bottomMargin'];
                $data['rightMargin'] = $_POST['rightMargin'];
                $data['leftMargin'] = $_POST['leftMargin'];
                $data['fontType'] = $_POST['fontType'];
                $data['fontSize'] = $_POST['fontSize'];
                $data['borderType'] = $_POST['borderType'];
                $data['borderColour'] = $_POST['borderColour'];
                $data['startDate'] = $_POST['startDate'];
                $data['endDate'] = $_POST['endDate'];
                $data['todayDate'] = $_POST['todayDate'];
                $data['joinType'] = $_POST['joinType'];
                $data['genQuery'] = $_POST['genQuery'];
            }
        }


        /* $data['pageTitle'] = 'visit Reprot Information';
          $data['content_view_page'] = 'reportInfo/viewQuery';
          $this->template->display($data); */
        $output = $this->load->view('multiReport/viewQuery', $data, TRUE);
        $this->load->library("mpdf_gen");
        $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
    }

    public function createHtml() {
        error_reporting('0');
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $this->load->model('insert_model');
        if (isset($_POST['Submit'])) {
            if (!empty($_POST['tableName'])) {

                $data['tableName'] = $_POST['tableName'];
                $data['columnName'] = $_POST['columnName'];
                $data['reportTitle'] = $_POST['reportTitle'];
                $data['joinType'] = $_POST['joinType'];
                $data['genQuery'] = $_POST['genQuery'];
            }
        }
        //error_reporting('0');
        foreach ($_POST['joinType'] as $joinType) {
            $joinType;
        }


        // exit();
        switch (!empty($joinType)) {
            case ($joinType == 'JOIN'):
                foreach ($_POST['tableName'] as $tableName) {
                    $tableName;
                }
                foreach ($_POST['columnName'] as $columnName) {
                    $columnName;
                }
                foreach ($_POST['reportTitle'] as $reportTitle) {
                    $reportTitle;
                }
                foreach ($_POST['genQuery'] as $genQuery) {
                    $genQuery;
                }
                 foreach ($_POST['columnName'] as $columnName) {
                        $columnName;
                    }
//                foreach ($_POST['columnName'] as $value790) {
//
//                    $value790;
//                    //$str = "Name [varchar]";
//                    $str = explode(".", $value790);
//                    $value790 = $str[0];
//                }
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $reportTitle,
                        'report_sql' => 'SELECT * FROM' . '  ' . $tableName . ' ' . $genQuery,
                        'table_name' => $tableName
                    );
                    $this->insert_model->form_insert($data1);
                } else {

                   $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $reportTitle,
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $tableName . ' ' . $genQuery,
                        'table_name' => $tableName,
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'multiReport/viewhtmlQuery';
                $this->template->display($data);
                break;
        }

    }

    public function submitQuery() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        if (isset($_POST['Submit'])) {

            if (!empty($_POST['check_list'])) {
                // Counting number of checked checkboxes.
                $data['checked_count'] = $_POST['check_list'];
                $data['tableName'] = $_POST['tableName'];
                $data['query'] = $_POST['query'];
            }
        }
        $data['pageTitle'] = 'visit Reprot Information';
        $data['content_view_page'] = 'reportInfo/viewSubmitQuery';
        $this->template->display($data);
    }

}
