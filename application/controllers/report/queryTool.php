<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @access      public
 * @param       none
 * @author      Maruf <ahasan@atilimited.net>
 * @return      Session Data and construct Class
 */
class QueryTool extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      main View Page With severel Parameters
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'QueryTool';
        $data['visit'] = $this->utilities->findAllByAttribute("bn_visitclassification", array("ACTIVE_STATUS" => 1));
        $data['visitInfo'] = $this->utilities->findAllByAttribute("bn_visitinformation", array("ACTIVE_STATUS" => 1));
        $data['exam'] = $this->utilities->findAllByAttribute("bn_navyexam_hierarchy", array("ACTIVE_STATUS" => 1, "EXAM_TYPE" => 3));
        $data['country'] = $this->utilities->findAllByAttribute("bn_country", array("ACTIVE_STATUS" => 1));
        $data['entryType'] = $this->utilities->findAllByAttribute("bn_entrytype", array("ACTIVE_STATUS" => 1));
        $data['batchNumber'] = $this->utilities->findAllByAttribute("bn_batchnumber", array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'reportPreparation/queryToolView';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Sailor
     */
    public function Sailor()
    {
        $qrValue = array($_POST['qrValue']);

        $a = $_POST['filterBirth'];
        $b = $_POST['fromDate'];
        $c = $_POST['endDate'];
        //$av = implode('&', $qrValue);
        $vowels = array("&");
        if($a == 1)
        {
            $abc = ('AND bithdate ='.$b);
        }
        if($a == 2)
        {
            $abc = ('AND bithdate <'.$b);
        }
        if($a == 3)
        {
            $abc = ('AND bithdate >'.$b);
        }
        if($a == 4)
        {
            $abc = ('AND birthdate BETWEEN' . $b .' AND '.$c);
        }
        $last = array($abc);
        $onlyconsonants = str_replace($vowels, " AND ", $qrValue);
        $lastValue = array_merge($onlyconsonants,$last);
        $lastValue1 = $onlyconsonants + $last;
           echo '<pre>';
           print_r($qrValue);
           print_r($onlyconsonants);
           print_r($a);
           print_r($b);
           print_r($c);
           print_r($abc);
            print_r($last);
            print_r($lastValue);
            print_r($lastValue1);

           exit();
    }


}
