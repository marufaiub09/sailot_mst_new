<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @access      public
 * @param       none
 * @author      Maruf <ahasan@atilimited.net>
 * @return      Session Data and construct Class
 */
class ReportDesigner extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      main View Page With severel Parameters
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Report Designer';
        $data['report'] = $this->utilities->findAllByAttribute("reportcolumn", array("ActualTableName !=" =>''));
        $data['reporttype'] = $this->utilities->findAllByAttribute("reporttype", array("ACTIVE_STATUS" => 1));
        $data['content_view_page'] = 'reportPreparation/reportDesignerView';
        $this->template->display($data);
    }
    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return     Sub Report Type DropDown
     */
    public function reportype(){
        $id = $_POST['id'];
        $query = $this->utilities->findAllByAttribute('reportsubtype', array("ACTIVE_STATUS" => 1, "ReportTypeID" => $id));
        $returnVal = '<option value = "">Select Sub Type</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->ReportSubTypeID . '">' . $row->ReportSubTypeName . '</option>';
            }
        }
        echo $returnVal;

    }
    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return     Insert Data into table Name: reportsetup,reportSetupFont
     */
    public function save()
    {
      // report setup--1st
      $reportName = $this->input->post('reportName', true);
      $reporttile = $this->input->post('reporttile', true);
      $reportSubtile = $this->input->post('reportSubtile', true);
      $reportHeader = $this->input->post('reportHeader', true);
      $reportFooter = $this->input->post('reportFooter', true);
      $reportType = $this->input->post('reportType', true);
      $subReportType = $this->input->post('subReportType', true);
      //table, column and alias
      $tableName = $this->input->post('positionBy', true);
      $tableOrderBy = $this->input->post('tableOrderBy', true);
      $columnOrderBy = $this->input->post('columnOrderBy', true);
      $aliasOrderBy = $this->input->post('aliasOrderBy', true);
      $tablePositionBy = $this->input->post('tablePositionBy', true);
      $columnPositionBy = $this->input->post('columnPositionBy', true);
      $aliasPositionBy = $this->input->post('aliasPositionBy', true);
      $groupBy = $this->input->post('groupBy', true);
      $orderBy = $this->input->post('orderBy', true);

      // page setup
      $pageSize = $this->input->post('pageSize',true);
      $topMargin = $this->input->post('topMargin',true);
      $bottomMargin = $this->input->post('bottomMargin',true);
      $rightMargin = $this->input->post('rightMargin',true);
      $leftMargin = $this->input->post('leftMargin',true);
      $rtfontName = $this->input->post('rtfontName',true);
      $rtfontSize = $this->input->post('rtfontSize',true);
      $rtbold = $this->input->post('rtbold',true);
      $rtItalic = $this->input->post('rtItalic',true);
      $rtUnderline = $this->input->post('rtUnderline',true);

      $rstfontName = $this->input->post('rstfontName',true);
      $rstfontSize = $this->input->post('rstfontSize',true);
      $rstbold = $this->input->post('rstbold',true);
      $rstItalic = $this->input->post('rstItalic',true);
      $rstUnderline = $this->input->post('rstUnderline',true);

      $chfontName = $this->input->post('chfontName',true);
      $chfontSize = $this->input->post('chfontSize',true);
      $chbold = $this->input->post('chbold',true);
      $chItalic = $this->input->post('chItalic',true);
      $chUnderline = $this->input->post('chUnderline',true);

      $cbbold = $this->input->post('cbbold',true);
      $cbItalic = $this->input->post('cbItalic',true);
      $cbUnderline = $this->input->post('cbUnderline',true);
      $cbfontName = $this->input->post('cbfontName',true);
      $cbfontSize = $this->input->post('cbfontSize',true);


      $tableQuery = $this->db->query("SELECT reportsubtype.tableName, reportsubtype.joinQuery FROM reportsubtype WHERE ReportTypeID = $reportType AND ReportSubTypeID = $subReportType")->row();

       $tbl = $tableQuery->tableName;
       $join = $tableQuery->joinQuery;

       if($tableQuery->joinQuery = null)
       {
         $join = '';
       }
      /* Start: for Table Column*/
      $tablename = implode(',', $tableName);
      /* End: for Table Column*/

      /* Start: for Order BY*/
      $order1 = implode(',', $orderBy);
      $order = explode(' ', $order1);

      if(isset($_POST['orderBy']))
      {
        $Finalorder = 'ORDER BY'.$order[0];
      }
      else {
        $Finalorder = '';
      }
      /* End: for Order BY*/
      /* Start: for Group BY*/
      $group1 = implode(',', $groupBy);
      $group = explode(' ', $group1);


      if(isset($_POST['groupBy']))
      {
        $Finalgroup = 'GROUP By'.$group[0];
      }
      else {
        $Finalgroup = '';
      }
      /* End: for Group BY*/

      //$orders = implode(',', $columnOrderBy);

      $position = implode(' ', $columnPositionBy);
      $whereClause = '$whereClause';


      $query = "SELECT $tablename FROM $tbl $join  WHERE $whereClause $Finalgroup $Finalorder";
      if($rtItalic = ' ')
      {
        $rtItalic = 0;
      }
      if($rtUnderline = ' ')
      {
        $rtUnderline = 0;
      }
      if($rtbold = ' ')
      {
        $rtbold = 0;
      }
      $fontType = 1;
      $reportSetup = array(

          'Name' => $reportName,
          'Title' => $reporttile,
          'Subtitle' => $reportSubtile,
          'reportHeader'=>$reportHeader,
          'reportFooter' =>$reportFooter,
          'PaperSize' => $pageSize,
          'TopMargin' => $topMargin,
          'BottomMargin' => $bottomMargin,
          'LeftMargin' => $leftMargin,
          'Rightmargin' => $rightMargin,
          'ReportTypeID' => $reportType,
          'ReportSubTypeID' => $subReportType,
          'reportQuery' => $query,
          // 'reportOrder' => $order,
          'reportPosition' => $position

      );
      if ($ReportSetupID = $this->utilities->insert('reportsetup', $reportSetup)) {
          $reportSetupFont = array(
              'ReportSetupID' =>$ReportSetupID,
              'FontType' => $fontType,
              'FontName' => $rtfontName,
              'FontSize' => $rtfontSize,
              'IsBold' => $rtbold,
              'IsItalic' => $rtItalic,
              'IsUnderLine' => $rtUnderline

          );
          $success = 0;
            if ($reportSetupFont = $this->utilities->insertData($reportSetupFont, 'reportsetupfont')) {
                      /* end Academic insert part */
                      $success = 1;
                  }

              if ($success == 1) {
                  echo "<div class='alert alert-success'>Report Setup  successfully</div>";
              } else {
                  echo "<div class='alert alert-danger'>Report Setup Not Inserted</div>";
              }
          }


      // echo "<pre>";
    //  print_r($position);
      // print_r($orderBy);
      // print_r($order);
      // print_r($tableOrderBy);
      // print_r($query);
      // print_r($aliasPositionBy);
       //print_r($position);
      // print_r($tablePositionBy);
      // print_r($reportName);
      // print_r($reporttile);
      // print_r($reportSubtile);
      // print_r($reportHeader);
      // print_r($reportFooter);
      // print_r($reportType);
      // print_r($subReportType);
      // print_r($cbbold);
      // print_r($cbItalic);
      // print_r($cbUnderline);
      // print_r($cbfontName);
      // print_r($cbfontSize);
      // print_r($groupBy);
    //   print_r($query);
      // exit();
    }
}
