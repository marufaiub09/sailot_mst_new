<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @category   Report Prepation
 * @package    Report
 * @author     Emdadul Huq <emdadul@atilimited.net>
 * @copyright  2016 ATI Limited Development Group
 */
class RpTrainCourSet extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <Emran@atilimited.net>
     * @return      View modal
     */
    public function index() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'Course Roster Setting';
        $data["partlist"] = $this->db->query("SELECT p.* FROM partii p WHERE p.ACTIVE_STATUS = 1")->result();
        $data['subjectType'] = $this->utilities->findAllByAttributeWithOrderBy("bn_navyexam_hierarchy", array("ACTIVE_STATUS" => 1, "EXAM_TYPE" => 1), "NAME");
        $data['branch'] = $this->utilities->findAllByAttributeWithOrderBy("bn_branch", array("ACTIVE_STATUS" => 1), "BRANCH_ID");
        $data['rank'] = $this->utilities->findAllByAttributeWithOrderBy("bn_rank", array("ACTIVE_STATUS" => 1), "RANK_NAME");
        //$data['partii'] = $this->utilities->findAllByAttributeWithOrderBy("partii", array("ACTIVE_STATUS" => 1), "Name");
        $data['code'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 3));
        $data['trainingType'] = $this->utilities->findAllByAttribute("bn_navytraininghierarchy", array("ACTIVE_STATUS" => 1, "NAVYTrainingType" => 2));
        $data['exam'] = $this->utilities->findAllByAttribute("bn_navyexam_hierarchy", array("ACTIVE_STATUS" => 1, "EXAM_TYPE" => 3));
        $data['codeExam'] = $this->utilities->dropdownFromTableWithCondition("bn_navyexam_hierarchy", '---Select---', 'EXAM_ID', 'NAME', $condition = array('ACTIVE_STATUS' => 1));
        $data['codeFirst'] = $this->utilities->dropdownFromTableWithCondition("bn_navytraininghierarchy", '---Select---', 'NAVYTrainingID', 'Name', $condition = array('ACTIVE_STATUS' => 1, "NAVYTrainingType" => 3));
        $data['content_view_page'] = 'reportPreparation/training_course';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <Emran@atilimited.net>
     * @return      View modal
     */
    function course_by_trainingType() {
        $TRAINING_TYPE = $_POST['TRAINING_TYPE'];
        $query = $this->utilities->findAllByAttribute('bn_navytraininghierarchy', array("ParentID" => $TRAINING_TYPE, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->NAVYTrainingID . '">' .'[ '.$row->Code.' ] '.$row->Name . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @access      public
     * @param       none
     * @author      Emran Hossen <Emran@atilimited.net>
     * @return      View modal
     */
    function course_by_trainingName() {
        $COURSE_TYPE = $_POST['COURSE_TYPE'];
        $query = $this->utilities->findAllByAttribute('bn_navyexam_hierarchy', array("PARENT_ID" => $COURSE_TYPE, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->EXAM_ID . '">' . $row->NAME . '</option>';
            }
        }
        echo $returnVal;
    }

    //emran
    
    function rankByBranch() {
        $branch_id = $_POST['branch_id'];
        $query = $this->utilities->findAllByAttribute('bn_rank', array("BRANCH_ID" => $branch_id, "ACTIVE_STATUS" => 1));
        $returnVal = '<option value = "">Select One</option>';
        if (!empty($query)) {
            foreach ($query as $row) {
                $returnVal .= '<option value = "' . $row->RANK_ID . '">' . $row->RANK_NAME . '</option>';
            }
        }
        echo $returnVal;
    }
    /**
     * @access      public
     * @param       none
     * @author      Md. Emdadul Huq<Emdadul@atilimited.net>
     * @return      if already exit then return CourseRoster
     */
    function courseDataSave(){
        /*echo "<pre>";
        print_r($_POST);
        exit;*/
        $COURSE_NAME= $this->input->post('COURSE_NAME', true);
        $Branch_ID= $this->input->post('Branch_ID', true);
        $rankId= $this->input->post('rankId', true);
        $courseData = array(
            'CourseID' => $COURSE_NAME,
            'CRE_BY' => $this->user_session["USER_ID"]
        );
        /*Data insert into CourseRoster table*/
        if ($courseRosterID = $this->utilities->insert('courseroster', $courseData)) { // if data inserted successfully
            
            $seniorityRecom= $this->input->post('seniorityRecom', true); /*Recommendation Counted from*/
            if($_POST['CalcRankSenio']){
                $calculeBase= $this->input->post('CalcRankSenio', true);
                if($calculeBase == 'Yes'){
                    $calculeBase = 2;
                }                
            }else{
                $calculeBase = 1;
            }
            $gainValue= $this->input->post('gainValue', true);
            $serviceLength= $this->input->post('seaPoint', true);
            $dueByTime= $this->input->post('dueByTime', true);
            $courseRosterBranch = array(
                'BranchID' => $Branch_ID,
                'CourseRosterID' => $courseRosterID,
                'RecomCountedFrom' => $seniorityRecom,
                'CalculationBase' => $calculeBase,
                'SeniorityGain' => $gainValue,
                'ServiceLength' => $serviceLength,
                'DueByTime' => $dueByTime,
                'CRE_BY' => $this->user_session["USER_ID"]
            );
            /*Data insert into CourseRosterBranch table*/
            if ($CourseRosterBranchID = $this->utilities->insert('courserosterbranch', $courseRosterBranch)) { // if data inserted successfully
                
                /*Course Roster Rank Section*/
                $rankId= $this->input->post('rankId', true);
                for ($i=0; $i < count($rankId) ; $i++) { 
                    $courseRosterRank = array(
                        'RankID' => $rankId[$i],
                        'CourseRosterBranchID' => $CourseRosterBranchID,
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $this->utilities->insertData($courseRosterRank, 'courserosterrank');
                }
                /*End course Roster Rank Section*/

                /*Course Roster Exam Section*/
                $codeExam= $this->input->post('codeExam', true);
                $examGain= $this->input->post('examGain', true);
                $examIsMandatory= $this->input->post('examIsMandatory', true);
                $examOutputColumn= $this->input->post('examOutputColumn', true);
                for ($i=0; $i < count($codeExam) ; $i++) { 
                    $courserosterexam = array(
                        'ExamID' => $codeExam[$i],
                        'CourseRosterBranchID' => $CourseRosterBranchID,
                        'GainValue' => $examGain[$i],
                        'IsMandatory' => $examIsMandatory[$i],
                        'ExamDateColumn' => $examOutputColumn[$i],
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $this->utilities->insertData($courserosterexam, 'courserosterexam');
                }
                /*End course Roster Exam Section*/

                /*Course Roster Training Section*/
                $trainingCode = $this->input->post('trainingCode', true);
                $trainingGain = $this->input->post('trainingGain', true);
                $trainingIsMandatory = $this->input->post('trainingIsMandatory', true);
                $trainingOutputColumn = $this->input->post('trainingOutputColumn', true);
                for ($i=0; $i < count($trainingCode) ; $i++) { 
                    $courserostertraining = array(
                        'TrainingID' => $trainingCode[$i],
                        'CourseRosterBranchID' => $CourseRosterBranchID,
                        'GainValue' => $trainingGain[$i],
                        'IsMandatory' => $trainingIsMandatory[$i],
                        'CourseDateColumn' => $trainingOutputColumn[$i],
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $this->utilities->insertData($courserostertraining, 'courserostertraining');
                }
                /*End course Roster Training Section*/

                /*Course Roster Part II Section*/
                $partII= $this->input->post('partII', true);
                for ($i=0; $i < count($partII) ; $i++) { 
                    $courseRosterPart = array(
                        'PartIIID' => $partII[$i],
                        'CourseRosterBranchID' => $CourseRosterBranchID,
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $this->utilities->insertData($courseRosterPart, 'courserosterpartii');
                }
                /*End course Roster Part II Section*/

                /*Course Roster Order By Section*/
                $orderBy = $this->input->post('orderBy');
                $recomnGain = $this->input->post('recomnGain');
                for ($i=0; $i < count($orderBy) ; $i++) { 
                    $orderBy = array(
                        'CourseRosterBranchID' => $CourseRosterBranchID,
                        'FieldType' => $recomnNo[$i],
                        'Name' => $recomnGain[$i],
                        'OrderByFieldID' => $recomnGain[$i],
                        'Position' => $recomnGain[$i],
                        'DataType' => $recomnGain[$i],
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $this->utilities->insertData($orderBy, 'courserosterorderby');
                }
                
                /*End course Roster Order By Section*/

                /*Course Roster Recommendation Section*/
                $recomnNo = $this->input->post('recomnNo');
                $recomnGain = $this->input->post('recomnGain');
                for ($i=0; $i < count($recomnNo) ; $i++) { 
                    $courseRosterComm = array(
                        'RecomNo' => $recomnNo[$i],
                        'GainValue' => $recomnGain[$i],
                        'CourseRosterBranchID' => $CourseRosterBranchID,
                        'CRE_BY' => $this->user_session["USER_ID"]
                    );
                    $this->utilities->insertData($courseRosterComm, 'courserosterrecom');
                }
                /*End course Roster Recommendation Section*/
            }
        }
    }
    /**
     * @access      public
     * @param       none
     * @author      Md. Emdadul Huq<Emdadul@atilimited.net>
     * @return      if already exit then return CourseRoster
     */
    function searchCourseRoster(){
        $courseId = $this->input->post('CourseId');
        $query = $this->db->query("SELECT cr.CourseRosterID, cb.CourseRosterBranchID ,bb.BRANCH_ID, bb.BRANCH_NAME, br.RANK_NAME, br.RANK_ID
                                        FROM courseroster cr
                                        INNER JOIN courserosterbranch cb on cr.CourseRosterID = cb.CourseRosterID
                                        INNER JOIN courserosterrank crr on crr.CourseRosterBranchID = cb.CourseRosterBranchID
                                        INNER JOIN bn_rank br on br.RANK_ID = crr.RankID
                                        INNER JOIN bn_branch bb on bb.BRANCH_ID = cb.BranchID
                                        WHERE cr.CourseID =  $courseId")->result();
        if($query != ''){
            echo json_encode($query);            
        }

    }

}

/* End of file examTestInfo.php */
/* Location: ./application/controllers/regularTransaction/examTestInfo.php */