<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReportView extends CI_Controller {

    private $now;

    public function __construct() {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      templete
     */
    public function index() {

        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit Reprot Information';

        $data['content_view_page'] = 'reportView/index';
        $this->template->display($data);
    }

    public function createHtml1() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $this->load->model('insert_model');
        if (isset($_POST['Submit'])) {
            if (!empty($_POST['reportName'])) {

                $data['reportName'] = $_POST['reportName'];
                $data['SAILOR_ID'] = $_POST['SAILOR_ID'];

            }
        }
        $data['pageTitle'] = 'visit Reprot Information';
        $data['content_view_page'] = 'reportView/viewhtmlQuery';
        $this->template->display($data);
    }

    public function createQuery() {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        if (isset($_POST['Create PDF'])) {

            if (!empty($_POST['reportName'])) {
                $data['SAILOR_ID'] = $_POST['SAILOR_ID'];
                $data['reportName'] = $_POST['reportName'];
                $data['pageSize'] = $_POST['pageSize'];
                $data['orientation'] = $_POST['orientation'];
                $data['topMargin'] = $_POST['topMargin'];
                $data['bottomMargin'] = $_POST['bottomMargin'];
                $data['rightMargin'] = $_POST['rightMargin'];
                $data['leftMargin'] = $_POST['leftMargin'];
                $data['fontType'] = $_POST['fontType'];
                $data['fontSize'] = $_POST['fontSize'];
                $data['borderType'] = $_POST['borderType'];
                $data['borderColour'] = $_POST['borderColour'];
                $data['startDate'] = $_POST['startDate'];
                $data['endDate'] = $_POST['endDate'];
                $data['todayDate'] = $_POST['todayDate'];
            }
        }


        /* $data['pageTitle'] = 'visit Reprot Information';
          $data['content_view_page'] = 'reportInfo/viewQuery';
          $this->template->display($data); */
        $output = $this->load->view('reportView/viewQuery', $data, TRUE);
        $this->load->library("mpdf_gen");
        $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
    }

}

?>