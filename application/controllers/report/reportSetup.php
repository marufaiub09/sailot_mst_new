<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ReportSetup extends CI_Controller
{

    private $now;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = $this->session->userdata('logged_in');
        if (!$this->user_session) {
            redirect('auth/index');
        }
        date_default_timezone_set("Asia/Dhaka");
        $this->now = date('Y-m-d H:i:s', time());
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      templete
     */
    public function index()
    {

        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $data['pageTitle'] = 'visit Reprot Information';
        $CI = &get_instance();
        $CI->load->database();
        $a = $CI->db->dbdriver;
        //echo $a;
        if ($a == 'mysqli' || $a == 'mysql') {
            $data['result'] = $this->db->list_tables();
        } else {
            $data['result'] = $this->db->query("SELECT * FROM USER_TABLES")->result();
        }
        $data['content_view_page'] = 'reportInfo/index';
        $this->template->display($data);
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Selected Column value
     */
    public function checkColumn()
    {

        $Tablename = $_POST['Tablename'];
        $data['result'] = $this->db->field_data($Tablename);
        $returnVal = '<option value = "">Select Columns</option>';
        if (!empty($data['result'])) {

            foreach ($data['result'] as $row) {
                $returnVal .= '<option value = "' . $row->name . '">' . $row->name . '.' . $row->primary_key . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Selected Column value and type
     */
    public function checkTable()
    {

        $Tablename = $_POST['Tablename'];
        $data['result'] = $this->db->field_data($Tablename);
        $returnVal = '<option value = "">Select Columns</option>';
        if (!empty($data['result'])) {
            $returnVal .= '<option value = "none.limit">None</option>';
            foreach ($data['result'] as $row) {
                $returnVal .= '<option value = "' . $row->name . '.' . $row->type . '">' . $row->name . '.' . $row->primary_key . '</option>';
            }
        }
        echo $returnVal;
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Get Table
     */
    public function checkTableData()
    {

        $Tablename = $_POST['Tablename'];

        $data['result'] = $this->db->get($Tablename)->result();
        $returnVal = '<option value = "">Select Columns</option>';
        if (!empty($data['result'])) {
            foreach ($data['result'] as $row) {
                foreach ($row as $value) {

                    $returnVal .= '<option value = "' . $value . '">' . $value . '</option>';
                }
            }
        }
        echo $returnVal;
    }

    /**
     * @access      public
     * @param       none
     * @author      Maruf <ahasan@atilimited.net>
     * @return      Type
     */
    public function checkColumnType()
    {
        $likeOption = $_POST['likeOption'];
        //$str = "Name [varchar]";
        $str = explode(".", $likeOption);
        $type = $str[1];
        $returnVal = '<option value = "">Select Oparetor</option>';
        switch (!empty($type)) {
            case ($type == 'int'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option  value=">"> > </option>
				<option  value=">=">>=</option>
				<option  value="<"><</option>
				<option  value="<="><=</option>
				<option  value="Like">Like</option>
				<option  value="NotLike">Not Like</option>
				<option  value="limit">Limit</option>';
                echo $returnVal;
                break;

            case ($type == 'varchar'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value="Like">Like</option>
				<option value="NotLike">Not Like</option>
				<option value="limit">Limit</option>';
                echo $returnVal;
                break;
            case ($type == 'datetime'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
				<option value="<="><=</option>
				<option value="!=">!=</option>
				
				<option value="limit">Limit</option>
				<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'date'):
                $returnVal .= '<option value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
				<option value="<="><=</option>
				<option value="!=">!=</option>
				
				<option value="limit">Limit</option>
				<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'tinyint'):
                $returnVal .= '
				<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
				<option value="<="><=</option>
				
				<option value="Like">Like</option>
				<option value="limit">Limit</option>
				<option value="NotLike">Not Like</option>';
                echo $returnVal;
                break;
            case ($type == 'timestamp'):
                $returnVal .= '<option  value="=">=</option>
				<option value="!=">!=</option>
				<option value=">">></option>
				<option value=">=">>=</option>
				<option value="<"><</option>
			<option value="<="><=</option>
			<option value="!=">!=</option>
			
			<option value="limit">Limit</option>
			<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'year'):
                $returnVal .= '<option  value="=">=</option>
			<option value="!=">!=</option>
			<option value=">">></option>
			<option value=">=">>=</option>
			<option value="<"><</option>
			<option value="<="><=</option>
			<option value="!=">!=</option>
			
			<option value="limit">Limit</option>
			<option value="between">Between</option>';

                echo $returnVal;
                break;
            case ($type == 'limit'):
                $returnVal .= '
			<option value="limit">Limit</option>
			<option value="select">select</option>
			';

                echo $returnVal;
                break;
        }
    }

    public function express()
    {
        $operatorType = $_POST['operatorType'];
        switch (!empty($operatorType)) {
            case ($operatorType == '>'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]"></div>';
                echo $returnVal;
                break;

            case ($operatorType == '<'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '!='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '>='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == '<='):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;
                break;
            case ($operatorType == 'IsNull'):
                $returnVal = '';

                break;
            case ($operatorType == 'IsNotNull'):
                $returnVal = '';

                break;
            case ($operatorType == 'Like'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;

                break;

            case ($operatorType == 'NotLike'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"><input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;

                break;
            case ($operatorType == 'limit'):
                $returnVal = '';
                $returnVal .= '<div class="col-sm-8"> <input type="text" class="form-control" name="options[]">
			</div>';
                echo $returnVal;

                break;

            case ($operatorType == 'between'):
                $returnVal = '';
                $returnVal .= '
			<div class="form-group">
			
			<div class="col-sm-4 date">
			<input type="text"  name="startDate" value=""
			class="datePicker form-control required input-form-padding-right-empty"  placeholder="start date" data-fv-field="startDate">
			</div>
			<div class="col-sm-4 date">
			<input type="text"  name="endDate" 
			class="datePicker2 form-control required input-form-padding-right-empty"  placeholder="end date" data-fv-field="startDate">
			</div>
			</div>
			<script type="text/javascript">
			$(document).ready(function() {
			
			$(".datePicker").datepicker(),
			$(".datePicker2").datepicker()});
			
			</script> ';

                echo $returnVal;

                break;
        }
    }

    public function createQuery()
    {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        if (isset($_POST['Create PDF'])) {

            if (!empty($_POST['tableName'])) {

                $data['tableName'] = $_POST['tableName'];
                $data['columnName'] = $_POST['columnName'];
                $data['options'] = $_POST['options'];
                $data['queryType'] = $_POST['operatortype'];
                $data['likeOption'] = $_POST['likeOption'];
                $data['reportTitle'] = $_POST['reportTitle'];
                $data['pageSize'] = $_POST['pageSize'];
                $data['orientation'] = $_POST['orientation'];
                $data['topMargin'] = $_POST['topMargin'];
                $data['bottomMargin'] = $_POST['bottomMargin'];
                $data['rightMargin'] = $_POST['rightMargin'];
                $data['leftMargin'] = $_POST['leftMargin'];
                $data['fontType'] = $_POST['fontType'];
                $data['fontSize'] = $_POST['fontSize'];
                $data['borderType'] = $_POST['borderType'];
                $data['borderColour'] = $_POST['borderColour'];
                $data['startDate'] = $_POST['startDate'];
                $data['endDate'] = $_POST['endDate'];
                $data['todayDate'] = $_POST['todayDate'];
            }
        }


        /* $data['pageTitle'] = 'visit Reprot Information';
          $data['content_view_page'] = 'reportInfo/viewQuery';
          $this->template->display($data); */
        $output = $this->load->view('reportInfo/viewQuery', $data, TRUE);
        $this->load->library("mpdf_gen");
        $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']);
    }

    public function createHtml()
    {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        $this->load->model('insert_model');
        if (isset($_POST['Submit'])) {
            if (!empty($_POST['tableName'])) {

                $data['tableName'] = $_POST['tableName'];
                $data['columnName'] = $_POST['columnName'];
                $data['options'] = $_POST['options'];
                $data['queryType'] = $_POST['operatortype'];
                $data['likeOption'] = $_POST['likeOption'];
                $data['reportTitle'] = $_POST['reportTitle'];
                $data['pageSize'] = $_POST['pageSize'];
                $data['orientation'] = $_POST['orientation'];
                $data['topMargin'] = $_POST['topMargin'];
                $data['bottomMargin'] = $_POST['bottomMargin'];
                $data['rightMargin'] = $_POST['rightMargin'];
                $data['leftMargin'] = $_POST['leftMargin'];
                $data['fontType'] = $_POST['fontType'];
                $data['fontSize'] = $_POST['fontSize'];
                $data['borderType'] = $_POST['borderType'];
                $data['borderColour'] = $_POST['borderColour'];
                $data['startDate'] = $_POST['startDate'];
                $data['endDate'] = $_POST['endDate'];
                $data['todayDate'] = $_POST['todayDate'];
                $data['ortableName'] = $_POST['ortableName'];
                $data['orclName'] = $_POST['orclName'];
                $data['join'] = $_POST['join'];
                $data['anothertlName'] = $_POST['anothertlName'];
                $data['anotherclName'] = $_POST['anotherclName'];
                $data['write_query'] = $_POST['write_query'];
                $data['write'] = $_POST['write'];
            }
        }
        error_reporting('0');
        foreach ($_POST['likeOption'] as $value3) {
            $value3;
            //$str = "Name [varchar]";
            $str = explode(".", $value3);
            $value3 = $str[0];
            $value3;
        }
        foreach ($_POST['operatortype'] as $value1) {
            // echo  $value1;
            $value1;
        }
        $value = array();
        foreach ($_POST['options'] as $key => $value2) {
            $value2;
            $value[$key] = $value2;
        }
        foreach ($_POST['join'] as $value9) {
            $value9;
        }
        foreach ($_POST['write'] as $value103) {
            $value103;
        }
        // exit();
        switch (!empty($value1)) {
            case ($value9 == 'join'):
                foreach ($_POST['ortableName'] as $value8) {
                    $value8;
                }
                foreach ($_POST['orclName'] as $value7) {

                    $value7;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value7);
                    $value7 = $str[0];
                }
                foreach ($_POST['anothertlName'] as $value78) {
                    $value78;
                }
                foreach ($_POST['anothertlName'] as $value78) {
                    $value78;
                }
                foreach ($_POST['anotherclName'] as $value79) {

                    $value79;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value79);
                    $value79 = $str[0];
                }
                foreach ($_POST['columnName'] as $value790) {

                    $value790;
                    //$str = "Name [varchar]";
                    $str = explode(".", $value790);
                    $value790 = $str[0];
                }
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $value8 . ' ' . 'JOIN' . ' ' . $value78 . '  ' . 'ON' . ' ' . $value8 . '.' . $value7 . '=' . $value78 . '.' . $value79,
                        'table_name' => $value8
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $value8 . ' ' . 'JOIN' . ' ' . $value78 . '  ' . 'ON' . ' ' . $value8 . '.' . $value7 . '=' . $value78 . '.' . $value79,
                        'table_name' => $value8,
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;

            case ($value1 == '<'):
                foreach ($_POST['tableName'] as $tablename) {
                    $tablename;
                }
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '<' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '<' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == '<='):
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '<=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '<=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == '>'):
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '>' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '>' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == '>='):
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '>=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '>=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == '!='):
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '!=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '!=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == '='):
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . '=' . ' ' . $value[$key],
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == 'Like'):
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . 'LIKE' . ' ' . "'" . '%' . $value[$key] . '%' . "'",
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . 'LIKE' . ' ' . "'" . '%' . $value[$key] . '%' . "'",
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == 'NotLike'):
                if (empty($_POST['columnName'])) {

                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . 'NOT LIKE' . ' ' . "'" . '%' . $value[$key] . '%' . "'",
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . '  ' . 'NOT LIKE' . ' ' . "'" . '%' . $value[$key] . '%' . "'",
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case($value1 == 'select'):
                if (empty($_POST['columnName'])) {


                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'],
                        'table_name' => $_POST['tableName']
                    );

                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'],
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;

            case ($value1 == 'limit'):
                if (empty($_POST['columnName'])) {
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'LIMIT' . ' ' . $value2,
                        'table_name' => $_POST['tableName']
                    );

                    $this->insert_model->form_insert($data1);
                } else {
                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'LIMIT' . ' ' . $value2,
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
            case ($value1 == 'between'):
                $start = $_POST['startDate'];
                $end = $_POST['endDate'];
                if (empty($_POST['columnName'])) {
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];

                        //$this->db->where($value3 . '>=', date("Y-m-d", strtotime($start)));
                        //$this->db->where($value3 . '<=', date("Y-m-d", strtotime($end)));
                    }
                    $date1 = date("Y-m-d", strtotime($start));
                    $date2 = date("Y-m-d", strtotime($end));
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT * FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . ' ' . 'BETWEEN' . ' ' . "'" . $date1 . "'" . ' ' . 'AND' . ' ' . "'" . $date2 . "'",
                        'table_name' => $_POST['tableName']
                    );
                    $this->insert_model->form_insert($data1);
                } else {
                    $date1 = date("Y-m-d", strtotime($start));
                    $date2 = date("Y-m-d", strtotime($end));
                    foreach ($_POST['likeOption'] as $key => $value3) {
                        $value3;
                        //$str = "Name [varchar]";
                        $str = explode(".", $value3);
                        $value3 = $str[0];

                        //$this->db->where($value3 . '>=', date("Y-m-d", strtotime($start)));
                        //$this->db->where($value3 . '<=', date("Y-m-d", strtotime($end)));
                    }

                    $column = implode(",", $_POST['columnName']);
                    $data1 = array(
                        'report_title' => $_POST['reportTitle'],
                        'report_sql' => 'SELECT' . '  ' . $column . '  ' . 'FROM' . '  ' . $_POST['tableName'] . ' ' . 'WHERE' . ' ' . $value3 . ' ' . 'BETWEEN' . ' ' . "'" . $date1 . "'" . ' ' . 'AND' . ' ' . "'" . $date2 . "'",
                        'table_name' => $_POST['tableName'],
                        'column_name' => $column
                    );
                    $this->insert_model->form_insert($data1);
                }
                $data['pageTitle'] = 'visit Reprot Information';
                $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
                $this->template->display($data);
                break;
        }


        //        $data['pageTitle'] = 'visit Reprot Information';
        //        $data['content_view_page'] = 'reportInfo/viewhtmlQuery';
        //        $this->template->display($data);
        /* $output = $this->load->view('reportInfo/viewQuery', $data, TRUE);
          $this->load->library("mpdf_gen");
          $this->mpdf_gen->gen_pdf($output, $_POST['pageSize']); */
    }

    public function submitQuery()
    {
        $data['breadcrumbs'] = array(
            'Modules' => '#'
        );
        if (isset($_POST['Submit'])) {

            if (!empty($_POST['check_list'])) {
                // Counting number of checked checkboxes.
                $data['checked_count'] = $_POST['check_list'];
                $data['tableName'] = $_POST['tableName'];
                $data['query'] = $_POST['query'];
            }
        }
        $data['pageTitle'] = 'visit Reprot Information';
        $data['content_view_page'] = 'reportInfo/viewSubmitQuery';
        $this->template->display($data);
    }

}
