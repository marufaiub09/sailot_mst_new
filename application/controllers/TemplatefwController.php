<?php
/**
 * Created by PhpStorm.
 * User: streetcoder
 * Date: 2/14/16
 * Time: 2:27 PM
 */

class TemplatefwController extends EXT_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['pageTitle']   = 'Template Full Width';
        $data['breadcrumbs'] = array(
            'Template Full Width' => '#'
        );

        $data['content_view_page'] = 'ui/dataTable';
        $this->templatefw->display($data);
    }

}